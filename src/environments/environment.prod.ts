// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  env: "production",
  apiEndPoint: "https://api.qurstore-test.vsolgmi.com/api",
  enableSSO: true,
  IdentityServerAPI: "https://api.qurstore-auth-test3.vsolgmi.com"
};
