import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AuthService } from './utils/auth.service';
import { Router } from '@angular/router';
import config from "./utils/config";
import { AuthenticationService } from './modules/authentication/services/authentication.service';
import { AlertService } from "./utils/alert/alert.service";
import { environment } from '../environments/environment';
import { NotificationService } from './modules/notification-management/services/notification.service';
import _ from 'lodash';
import moment from 'moment';
import { version } from '../../package.json';
import { NgxSmartModalService } from "ngx-smart-modal";
import { AppViewModel } from './app.viewmodel';
import { ToastrService } from 'ngx-toastr';
import { ValidationMessages } from "./utils/helper/validation-messages";
import { SessionTimerService } from "session-expiration-alert";
import { SYSTEM_CONFIGURATION_CODE } from "./utils/config";
import { from as observableFrom, Observable, Subscription } from "rxjs";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppViewModel],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  title = 'QurStore';
  isSystem: boolean;
  isAdvamedUser: boolean;
  isResearcher: boolean;
  organizationName: string;
  userName: string;
  navBarItems: any;
  menuItems = { rootLevel: [], grouped: [] };
  modalPoppedUp: boolean = false;
  loginErrorMessage: string;
  public isADUser = false;
  public version: string = version;
  public alertAt: number;
  public startTimer = true;
  subscribers = new Subscription();
  constructor(public authService: AuthService, private router: Router
    , public appViewModel: AppViewModel
    , public authenticationService: AuthenticationService, public alertService: AlertService
    , public notificationService: NotificationService, public ngxSmartModalService: NgxSmartModalService
    , private toastr: ToastrService,
    public sessionTimer: SessionTimerService
  ) { }

  public ngOnInit() {
    this.initViewModels();
    this.getMenuItems();
    this.setTimeOut_ErroMessage();
    this.initSubscription();
  }

  public initViewModels() {
    this.appViewModel.init();
  }
  public getMenuItemsInit(): void {
    if (this.menuItems.rootLevel === undefined ||
      (this.menuItems.rootLevel && this.menuItems.rootLevel.length === 0)) {
      this.getMenuItems();
      this.setTimeOut_ErroMessage();
    }
  }

  public getMenuItems() {
    this.navBarItems = this.authService.getMenuItems();
    this.menuItems.rootLevel = this.navBarItems.rootLevel;
    this.menuItems.grouped = this.navBarItems.grouped;
    this.checkUserIsADUser();
    this.setSessionAlertTime();
  }

  public showMenuBar(): boolean {
    this.userName = this.authService.getUserNameFromLocalStorage();
    this.getMenuItemsInit();
    if (window.location.href.indexOf("auth") >= 0) {
      if (this.router.url === "/auth/index") {
        this.menuItems = { rootLevel: [], grouped: [] };
      }
      return false;
    } else {
      return true;
    }
  }

  public goToDashboard() {
    this.router.navigate(["dashboard"]).then().catch();;
  }

  public get IsAdmin() {
    const isAdmin = this.authService.isAdmin();
    if (isAdmin) {
      this.isSystem = isAdmin;
      this.organizationName = this.authService.getOrganizationName();
      this.userName = this.authService.getUserNameFromLocalStorage();
      this.isAdvamedUser = this.authService.isAdvamedBoolean();
      this.isResearcher = this.authService.isResearcherBoolean();
    } else {
      this.isSystem = undefined;
      //  this.router.navigate(["auth/login"]);
    }
    return this.isSystem;
  }

  public logout(): void {
    this.ngxSmartModalService.getModal("logOutConfirmModal").close();
    localStorage.clear();
    this.userName = undefined;
    this.menuItems = { rootLevel: [], grouped: [] };
    this.navigateToIndex();
  }

  public sendChangePasswordNotificationEmail(): void {
    this.showInfo(ValidationMessages.RESET_PASSWORD_EMAIL_INITIATED, "Info");
    const decodedToken: any = this.authService.getDecodedAuthToken();
    const requestBody = { email: decodedToken.email };
    this.authenticationService.forgotPassword(requestBody)
      .subscribe(
        (data) => {
          if (data.isSuccess) {
            this.showSuccess(data.message, "Success");
          }
          else {
            this.showError(data.errorList[0], "Error");
          }
        }, (err) => { }
      );
  }


  public Login() {
    if (environment.enableSSO) {
      this.SSOLogin();
    } else {
      this.router.navigate(["auth/login"]);
    }
  }
  private SSOLogin() {
    this.authenticationService.SSOLogin()
      .subscribe(
        (data) => {
          if (data.isSuccess) {
            this.authService.setUserNameInLocalStorage();
            this.authService.getMenuItems();
            this.router.navigate(["dashboard"]).then().catch();
          }
          else {
            this.loginErrorMessage = data.errorList[0];
          }
        }, (err) => {
          if (err.status === 401) {
            this.loginErrorMessage = "User is not authorized";
          } else {
            this.loginErrorMessage = "API Communication error";
          }
        });
  }
  public get showLoginOption(): boolean {
    if (window.location.href.indexOf("login") >= 0) {
      return false;
    } else if (this.userName) {
      return false;
    }
    else {
      return true;
    }
  }

  public navigateToIndex(): void {
    this.router.navigate(["auth/index"]).then().catch();
  }

  public redirectToNotificationList(): void {
    this.router.navigate(["notification/list"]).then().catch();
  }

  private setTimeOut_ErroMessage() {
    setTimeout(() => {
      this.loginErrorMessage = undefined;
    }, 10000);
  }


  public checkUserIsADUser(): void {
    this.isADUser = this.authService.checkIsADUser();
  }

  public openLogOutConfirmationModal(): void {
    this.ngxSmartModalService.getModal("logOutConfirmModal").open();
  }

  public showInfo(message, title): void {
    this.toastr.info(message, title)

  }

  public showSuccess(message, title): void {
    this.toastr.success(message, title);
  }

  public showError(message, title): void {
    this.toastr.error(message, title);
  }

  public get showSessionTimeOutAlert(): boolean {
    if (this.router.url.includes("auth")) {
      return false;
    } else {
      return true;
    }
  }

  public setSessionAlertTime(): void {
    if (this.authService.hasToken()) {
      this.alertAt = this.authService.getSessionAlertTimeFromToken();
    }
  }

  private initSubscription(): Observable<any> {
    this.subscribers.add(this.authService.getPopUpStatusEventObs().subscribe(e => {
      this.modalPoppedUp = e.modalPoppedUp
    }));
    return observableFrom([]);
  }

  public navigateToDashboard(): void {
    if (this.router.url.includes("auth")) {
      this.router.navigate(["auth/index"]).then().catch();
    } else {
      this.router.navigate(["dashboard"]).then().catch();
    }
  }
}

