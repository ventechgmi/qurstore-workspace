import * as jwt_decode from "jwt-decode";
import { Injectable } from "@angular/core";
@Injectable()
export class AuthorizationTokenManager {
   KEY_TOKEN = "token"; 


  /**
   * Set the Authorization Token
   */
  setAuthToken(token: any): void {
    // const t = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIyMGM1ODgxNC0wOTBmLTQyNjMtYTRjNy0zMDJiYWNkZThjZmUiLCJ1c2VyTmFtZSI6IkhhcnNoIFRyaXZlZGkiLCJpc090cFZlcmlmaWVkIjp0cnVlLCJlbWFpbCI6ImhhcnNoLnRyaXZlZGlAdmVudGVjaHNvbHV0aW9ucy5jb20iLCJjbGFpbXMiOltdLCJpc1N5c3RlbSI6dHJ1ZSwiaWF0IjoxNTg4MTEzMDY4LCJleHAiOjE1ODgxMTQ4NjgsImlzcyI6IlZhbGhvbGxhIiwic3ViIjoiVmFsaG9sbGEifQ.dGo3EL76OIGmTQjIMU3Q9gOyhMvRsrz37xZGxR2nrO5Cm1HhkdsHhTEFX4DoxmGpvUxQwdIsmOx2D1btJ3vLx5FIyS4fojVNoqmU2B-7ny21lcRaw3KBdsdXPTAMi1zAHWi-qoeFOmqQYXZdc9_RhDQBwzzizgL4mXg4BsA-AQz-ZIevEXxO9IOQXOrLKD1htIl0goS9hghIGYDHT_QWN-xDRr8LR6JmUAkbLfLqhCjmdKvpCuxn6hpsbkjMPHyibHywYTrPpP-HlenkhX5kONRx6V5SCk5V9mibzibL5D0aTUfAUAvat8_Mt7jHYuxbnRGGMhtaN7jp8hY3FftuTg";
    localStorage.setItem(this.KEY_TOKEN, token);
  }

  /**
   * Get the encoded (raw) Authentication Token
   */
  getEncodedAuthToken(): any {
    // return "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIyMGM1ODgxNC0wOTBmLTQyNjMtYTRjNy0zMDJiYWNkZThjZmUiLCJ1c2VyTmFtZSI6IkhhcnNoIFRyaXZlZGkiLCJpc090cFZlcmlmaWVkIjp0cnVlLCJlbWFpbCI6ImhhcnNoLnRyaXZlZGlAdmVudGVjaHNvbHV0aW9ucy5jb20iLCJjbGFpbXMiOltdLCJpc1N5c3RlbSI6dHJ1ZSwiaWF0IjoxNTg4MTEzMDY4LCJleHAiOjE1ODgxMTQ4NjgsImlzcyI6IlZhbGhvbGxhIiwic3ViIjoiVmFsaG9sbGEifQ.dGo3EL76OIGmTQjIMU3Q9gOyhMvRsrz37xZGxR2nrO5Cm1HhkdsHhTEFX4DoxmGpvUxQwdIsmOx2D1btJ3vLx5FIyS4fojVNoqmU2B-7ny21lcRaw3KBdsdXPTAMi1zAHWi-qoeFOmqQYXZdc9_RhDQBwzzizgL4mXg4BsA-AQz-ZIevEXxO9IOQXOrLKD1htIl0goS9hghIGYDHT_QWN-xDRr8LR6JmUAkbLfLqhCjmdKvpCuxn6hpsbkjMPHyibHywYTrPpP-HlenkhX5kONRx6V5SCk5V9mibzibL5D0aTUfAUAvat8_Mt7jHYuxbnRGGMhtaN7jp8hY3FftuTg";
    return localStorage.getItem(this.KEY_TOKEN);
  }

  /**
   * Get the decoded Authentication Token
   */
  getDecodedAuthToken(): any {
    const token = this.getEncodedAuthToken();
    return token ? jwt_decode(token) : undefined;
  }

  /**
   * Clear the Authentication Token
   */
  clearAuthToken(): void {
    localStorage.removeItem(this.KEY_TOKEN);
  }


  // Additional functions for retrieving the organizations and module claims from the token


}