
import { throwError as observableThrowError, Observable } from "rxjs";

import { catchError, map } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import * as util from "util";
import { ROUTES_CONFIG_MAP, ERROR_TYPE_MAP } from "../config";
import { AuthService } from "../auth.service";

@Injectable({ providedIn: "root" })
export class HttpRequestResponseInterceptor implements HttpInterceptor {
  private readonly KEY_X_AUTH_TOKEN: string = "x-auth-token";
  private readonly KEY_AUTHORIZATION: string = "Authorization";
  private readonly KEY_BEARER: string = "Bearer";
  constructor(private router: Router, private authService: AuthService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    var ssoLogin = "/auth/sso-login";

    let clonedRequest;
    if (request.url.indexOf(ssoLogin) != -1) {
      clonedRequest = request.clone({ withCredentials: true });
    }
    else {
      let urls = this.authService.getUrlsWithoutHeaderAuth();
      var el = urls.find(a => request.url.includes(a));
      if (!el) {
        clonedRequest = request.clone({
          headers: request.headers.set(
            this.KEY_AUTHORIZATION,
            util.format("%s %s", this.KEY_BEARER, this.authService.getEncodedAuthToken())
          )
        });
      }
      else {
        clonedRequest = request.clone({ });
      }
    }

    return next
      .handle(clonedRequest)
      .pipe(
        map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            /**
             * If the response headers contains the authorization token, grab it and store it
             */
            if (event.headers.has(this.KEY_X_AUTH_TOKEN)) {
              this.authService.setAuthToken(event.headers.get(this.KEY_X_AUTH_TOKEN));
            }
          }
          return event;
        })
      ).pipe(
        catchError((err: any) => {
          // user don't have any module access
          const errorType = err.error && err.error.errorType;
          if (err.status === 401 && errorType === ERROR_TYPE_MAP.NO_MODULE_ACCESS) {
            this.router.navigate([ROUTES_CONFIG_MAP.UNAUTHORIZED.FULL_URL]).catch();
            return observableThrowError(err);
          }
          /**
           * If the error is a 401 (not authorized) or 440 (expired), then redirect to the login page
           */
          if (err instanceof HttpErrorResponse && (err.status === 401 || err.status === 440)) {
            this.router.navigate([ROUTES_CONFIG_MAP.AUTHENTICATION.FULL_URL]).catch();
            return observableThrowError(err);
          }
          return observableThrowError(err);
        }));
  }
}