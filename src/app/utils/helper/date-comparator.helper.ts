
export class DataComparatorHelper {


    public static DateComparator(date1: string, date2: string): number {
        const date1Number = this.MonthToComparableNumber(date1);
        const date2Number = this.MonthToComparableNumber(date2);
    
        if (date1Number === null && date2Number === null) {
          return 0;
        }
        if (date1Number === null) {
          return -1;
        }
        if (date2Number === null) {
          return 1;
        }
    
        return date1Number - date2Number;
      }
    
      public static MonthToComparableNumber(date: string): number {
        if (date === undefined || date === null || date.length !== 10) {
          return null;
        }
    
        const yearNumber = Number(date.substring(6, 10));
        const monthNumber = Number(date.substring(3, 5));
        const dayNumber = Number(date.substring(0, 2));
    
        const result = yearNumber * 10000 + monthNumber * 100 + dayNumber;
        return result;
      }
}