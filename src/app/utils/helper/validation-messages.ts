export class ValidationMessages {
    public static readonly RESET_PASSWORD_EMAIL_INITIATED = "Thank You! You'll shortly receive your password reset link!";
}
