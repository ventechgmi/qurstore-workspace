import { Injectable } from "@angular/core";
@Injectable()
export class LocalStorageManager {

  public  getLocalStorageItem(key: string): any {
    const data: string = localStorage.getItem(`__QS${key}`);
    if (data) {
      return JSON.parse(data);
    } else {
      return undefined;
    }
  }

  public setLocalStorageItem(key: string, data: any): void {
    localStorage.setItem(`__QS${key}`, JSON.stringify(data));
  }

  removeLocalStorageItem(key: string): void {
    localStorage.removeItem(`__QS${key}`);
  }
  public cleanUpSpecificEntity(startsWith: string): void {
    for (const key in localStorage) {
      if (key.startsWith(`__QS${startsWith}`)) {
        localStorage.removeItem(key);
      }
    }
  }
  public cleanUp(): void {
    for (const key in localStorage) {
      if (key.startsWith("__QS")) {
        // localStorage.removeItem(key);
      }
    }
  }
}