
import { of as observableOf, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot } from "@angular/router";
import { AuthService } from "./auth.service";
import { AuthenticationService } from "../modules/authentication/services/authentication.service";
import { ROUTES_CONFIG_MAP } from "./config";
import { LoaderService } from "../modules/core-ui-services/loader.service";

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(public auth: AuthService, public router: Router, private loaderService: LoaderService) { }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
        const routeData = route ? route.data : null;
        const moduleId: string = routeData ? routeData.moduleId : null;
        const url = "url";
        const routerState = "_routerState";
        const redirectUrl = "redirect_url";

        this.loaderService.clearLoaderExclusion();
        this.loaderService.setLoaderExclusion(route.routeConfig.path);

        // user trying to access Authorized URL without Token.
        if (!this.auth.hasToken()) {
            this.router.navigate([ROUTES_CONFIG_MAP.AUTHENTICATION.FULL_URL]).catch();
            localStorage.setItem(redirectUrl, route[routerState][url]);
            return observableOf(false);
        }
        // otp is not verified
        if (!this.auth.isOptVerified()) {
            this.router.navigate([ROUTES_CONFIG_MAP.AUTHENTICATION.FULL_URL]).catch();
            return observableOf(false);
        }
        // user token expired
        if (!this.auth.isAuthenticated()) {
            this.router.navigate([ROUTES_CONFIG_MAP.AUTHENTICATION.FULL_URL]).catch();
            localStorage.setItem(redirectUrl, route[routerState][url]);
            localStorage.clear(); // clearing the local storage as the login link show/hide is dependent on url and userName property in local storage.
            return observableOf(false);
        }
        // user has token and viewing unAuthorize screens(dashboard)
        if (!moduleId) { return observableOf(true); }
        return this.auth.isUserHasReadAccess(moduleId).pipe(
            map(isHasAccess => {
                if (!isHasAccess) { this.router.navigate([ROUTES_CONFIG_MAP.UNAUTHORIZED.FULL_URL]).catch(); }
                return isHasAccess;
            }));
    }
}
