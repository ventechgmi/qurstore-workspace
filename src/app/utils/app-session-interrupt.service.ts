import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SessionInteruptService } from 'session-expiration-alert'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AppSessionInterruptService extends SessionInteruptService {
  constructor(private readonly httpClient: HttpClient, private router: Router) {
    super();
  }
  continueSession() {
    window.location.reload()
  }
  stopSession() {
    localStorage.clear();
    this.router.navigate(["auth/index"]);
  }
}
