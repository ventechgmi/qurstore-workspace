
import { map } from "rxjs/operators";
import { Injectable, Inject } from "@angular/core";
import { AuthorizationTokenManager } from "./authentication-token-manager";
import { Router } from "@angular/router";
import _ from "underscore";
import config, { ROUTES_CONFIG_MAP, MODULE_SYMBOL_PERMISSIONS_MAP, NavBarRoutes, UrlsWithoutHeaderAuth } from "./config";
import { Subject, Observable, BehaviorSubject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { JwtHelperService } from "@auth0/angular-jwt";
import { Constants } from "../modules/authentication/helpers/constants";

@Injectable()
export class AuthService extends AuthorizationTokenManager {
  userName = new BehaviorSubject<string>("");
  moduleBaseUrlModule;
  baseUrlModuleIdMapPromise: Promise<any>;

  public popUpStatusEvent = new BehaviorSubject<any>({});
  public getPopUpStatusEventObs(): Observable<any> {
    return this.popUpStatusEvent.asObservable();
  }

  constructor(
    private router: Router,
    private http: HttpClient,
    public jwtHelper: JwtHelperService,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
    super();
  }

  public getRawApi(): string {
    const apiEndPoint = this.appConfig.apiEndPoint;
    return apiEndPoint;
  }

  public getUserId() {
    const decodedToken = super.getDecodedAuthToken();
    return decodedToken && decodedToken.userid;
  }

  public getMenuItems(): any {
    if (this.getDecodedAuthToken()) {
      const menuItems = { rootLevel: [], grouped: [] };
      if (this.isAdmin()) {
        menuItems.rootLevel.push(...NavBarRoutes.rootLevel);
        for (const item of NavBarRoutes.grouped) {
          const groupItem: any = {};
          groupItem.groupName = item.groupName;
          groupItem.groupItems = [];
          for (const navItem of item.groupItems) {
            if (navItem.grouped) {
              for (const subItem of navItem.grouped) {
                const subGroupItem: any = {};
                subGroupItem.groupName = subItem.groupName;
                subGroupItem.groupItems = [];
                for (const subNavItem of subItem.groupItems) {
                  subGroupItem.groupItems.push(subNavItem);
                }
                groupItem.groupItems.push(subGroupItem);
              }

            }
            else {
              groupItem.groupItems.push(navItem);
            }
          }
          menuItems.grouped.push(groupItem);
        }
        return menuItems;
      } else {
        const decodedToken = super.getDecodedAuthToken();
        for (const navItem of NavBarRoutes.rootLevel) {
          if (navItem.isDefault) {
            menuItems.rootLevel.push(navItem);
          } else {
            if (decodedToken && decodedToken.roleModules && decodedToken.roleModules.length) {
              const matchedObj = decodedToken.roleModules.find(e => e.ModuleId.toLowerCase() === navItem.moduleId.toLowerCase());
              if (matchedObj) {
                menuItems.rootLevel.push(navItem);
              }
            }
            else if (decodedToken && decodedToken.roleModules && decodedToken.roleModules.Id) {
              if (decodedToken.roleModules.ModuleId.toLowerCase() === navItem.moduleId.toLowerCase()) {
                menuItems.rootLevel.push(navItem);
              }
            }
          }
        }
        for (const item of NavBarRoutes.grouped) {
          const groupItem: any = {};
          groupItem.groupName = item.groupName;
          groupItem.groupItems = [];
          for (const navItem of item.groupItems) {
            if (navItem.grouped) {
              for (const subItem of navItem.grouped) {
                const subGroupItem: any = {};
                subGroupItem.groupName = subItem.groupName;
                subGroupItem.groupItems = [];
                if (decodedToken && decodedToken.roleModules && decodedToken.roleModules.length) {
                  const matchedObj = decodedToken.roleModules.find(e => e.ModuleId.toLowerCase() === subItem.moduleId.toLowerCase());
                  if (matchedObj) {
                    for (const subNavItem of subItem.groupItems) {
                      subGroupItem.groupItems.push(subNavItem);
                    }
                    groupItem.groupItems.push(subGroupItem);
                  }
                  
                }
                
              }
            }
            else {
              if (navItem.isDefault) {
                groupItem.groupItems.push(navItem);
              } else {
                if (decodedToken && decodedToken.roleModules && decodedToken.roleModules.length) {
                  const matchedObj = decodedToken.roleModules.find(e => e.ModuleId.toLowerCase() === navItem.moduleId.toLowerCase());
                  if (matchedObj) {
                    groupItem.groupItems.push(navItem);
                  }
                }
                else if (decodedToken && decodedToken.roleModules && decodedToken.roleModules.Id) {
                  if (decodedToken.roleModules.ModuleId.toLowerCase() === navItem.moduleId.toLowerCase()) {
                    menuItems.rootLevel.push(navItem);
                  }
                }
              }
            }
          }
          menuItems.grouped.push(groupItem);
        }
        return menuItems;
      }
    } else {
      return [];
    }
  }

  public getUserName() {
    const decodedToken = super.getDecodedAuthToken();
    if (decodedToken && decodedToken.firstName && decodedToken.lastName) {
      return decodedToken.firstName + " " + decodedToken.lastName;
    } else {
      return "";
    }
  }

  public getOrganizationId() {
    const decodedToken = super.getDecodedAuthToken();
    if (decodedToken && decodedToken.organizations) {
      if (Array.isArray(decodedToken.organizations) && decodedToken.organizations.length) {
        return decodedToken.organizations[0].Id;
      } else {
        return decodedToken.organizations.Id;
      }
    }
  }

  public getOrganizationName() {
    const decodedToken = super.getDecodedAuthToken();
    if (decodedToken && decodedToken.organizations) {
      return decodedToken.organizations.Name; // decodedToken.organizationName;
    } else {
      return "";
    }
  }


  public isAdmin() {
    const decodedToken = super.getDecodedAuthToken();
    const isAdmin = decodedToken && typeof decodedToken.isSystem === "string" ? decodedToken.isSystem.toLowerCase() === "true" : decodedToken && decodedToken.isSystem;
    return isAdmin;
  }
  public isAdminBoolean() {
    const decodedToken = super.getDecodedAuthToken();
    if (decodedToken && decodedToken.isSystem && decodedToken.isSystem.toLowerCase() === "true") {
      return true;
    } else {
      return false;
    }
  }
  public isAdvamedBoolean() {
    const decodedToken = super.getDecodedAuthToken();
    if (decodedToken && decodedToken.isAdvamed && decodedToken.isAdvamed.toLowerCase() === "true") {
      return true;
    } else {
      return false;
    }
  }
  public isResearcherBoolean() {
    const decodedToken = super.getDecodedAuthToken();
    if (decodedToken && decodedToken.isResearcher && decodedToken.isResearcher.toLowerCase() === "true") {
      return true;
    } else {
      return false;
    }
  }
  public isSysAdminBoolean() {
    const decodedToken = super.getDecodedAuthToken();
    if (decodedToken && decodedToken.isSystem && decodedToken.isSystem.toLowerCase() === "true") {
      return true;
    } else {
      return false;
    }
  }
  // public fetchBaseUrlModuleIdMap(): Observable<any> {
  //   return this.mainService.getModuleBaseUrlModuleIdMap();
  // }

  public profileUserName(): BehaviorSubject<string> {
    return this.userName;
  }

  // public getUserName() {
  //   const decodedToken = super.getDecodedAuthToken();
  //   return decodedToken && decodedToken.userName;
  // }

  public setModuleBaseUrlModuleIdMap(moduleBaseUrlModule) {
    this.moduleBaseUrlModule = moduleBaseUrlModule;
  }

  public logOut() {
    this.clearAuthToken();
    this.router.navigate([ROUTES_CONFIG_MAP.AUTHENTICATION.FULL_URL]).then().catch();
  }

  public getClaims() {
    const decodedToken = super.getDecodedAuthToken();
    return decodedToken ? decodedToken.claims : undefined;
  }

  public getModuleAccess(moduleId) {
    // const claims = this.getClaims();
    return this.buildModuleAccess(moduleId);
  }

  public buildModuleAccess(moduleId) {
    const decodedToken = super.getDecodedAuthToken();
    if (!decodedToken) {
      return {
        RolePermissions: {
          canRead: false,
          canUpdate: false,
          canDelete: false,
          canCreate: false,
          canExecute: false
        }
      }
    }
    if (decodedToken.isSystem === "True") {
      return {
        RolePermissions: {
          canRead: true,
          canUpdate: true,
          canDelete: true,
          canCreate: true,
          canExecute: true
        }
      };
    } else {
      if (decodedToken.roleModules) {
        const roleModulesArray = (decodedToken.roleModules.length) ? decodedToken.roleModules : [{ ...decodedToken.roleModules }]
        let moduleAccess = _.find(roleModulesArray, { ModuleId: moduleId.toLowerCase() });
        if (moduleAccess && moduleAccess.RolePermissions) {
          const rolePermission = {
            canRead: moduleAccess.RolePermissions.CanRead,
            canUpdate: moduleAccess.RolePermissions.CanUpdate,
            canDelete: moduleAccess.RolePermissions.CanDelete,
            canCreate: moduleAccess.RolePermissions.CanCreate,
            canExecute: moduleAccess.RolePermissions.CanExecute
          }
          moduleAccess.RolePermissions = rolePermission;
        }
        return moduleAccess;
      } else {
        return;
      }

    }
    // if (!moduleId) { return {}; }
    // if (!claims || !claims.modules) { return; }
    // let moduleAccess = _.find(claims.modules, { id: moduleId });

    // /**
    //  * * If the user role does not have minimal read access means , module Id does not part of the claims
    //  * so while fetching the claim for that specific moduleId its getting value as undefined
    //  * for that, here just giving all access are false
    //  */

  }

  public hasToken(): boolean {
    const token = super.getEncodedAuthToken();
    if (!token) { return false; }
    return true;
  }

  public isOptVerified(): boolean {
    const token = super.getDecodedAuthToken();
    if (!token) { return false; }
    return token.isOtpVerified;
  }

  public isAuthenticated(): boolean {
    const token = super.getEncodedAuthToken();
    if (!token) { return false; }
    return !this.jwtHelper.isTokenExpired(token);
  }

  public isUserHasReadAccess(moduleId): Observable<any> {
    return this.http
      .post(this.getRawApi() + "/authorize/user", { moduleId }).pipe(
        map((res: any) => (res ? res.isHasReadAccess : false)));
  }

  public checkIsADUser(): boolean {
    const decodedToken = super.getDecodedAuthToken();
    if (decodedToken && decodedToken.AuthenticationProviderId !== "") {
      return true;
    } else {
      return false;
    }

  }

  public setUserNameInLocalStorage() {
    localStorage.setItem(config.USER_NAME_LOCAL_STORAGE_KEY, this.getUserName())
  }
  public getUserNameFromLocalStorage(): string {
    return localStorage.getItem(config.USER_NAME_LOCAL_STORAGE_KEY);
  }

  public getSessionAlertTimeFromToken(): number {
    const decodedToken = super.getDecodedAuthToken();
    if (decodedToken && decodedToken.SessionAlertTime) {
      return Number(decodedToken.SessionAlertTime);
    }

  }

  public getUrlsWithoutHeaderAuth(): any {
    return UrlsWithoutHeaderAuth;
  }
}
