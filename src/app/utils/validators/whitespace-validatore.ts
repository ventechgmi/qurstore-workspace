import { AbstractControl, ValidatorFn } from "@angular/forms";

/*
This method validates the below
1. validates the control has only white spaces since most of the name fields should not allow just white spaces
2. validates the control whether it has only special characters.

*/
export function validateSpecialCharactersAndWhiteSpace(lengthMin: number, lengthMax: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        // const pattern = "[^A-Za-z0-9||||\.||_]";
        const pattern = "[A-Za-z0-9]";
        const whiteSpace = /^\s*$/;
        if ((control.value || "").length === 0) {
            return null;
        } else if ((control.value || "").trim().length < lengthMin) {
            return { minlength: true };
        }  else if ((control.value || "").trim().length > lengthMax) {
            return { maxlength: true };
        } else if (control.value.match(whiteSpace)) {
            return { hasOnlyWhiteSpace: true };
        } else if (!control.value.match(pattern)) { // check if the input doest not have any alphabets or in other words input has only special characters
            // return "Don't enter only special characters";
            return { hasOnlySpecialCharacters: true };
        }
    };
}