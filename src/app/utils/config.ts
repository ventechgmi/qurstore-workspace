import { SweetAlertOptions } from 'sweetalert2';
import { NgxUiLoaderConfig } from 'ngx-ui-loader';
import { CurrencyMaskConfig } from 'ngx-currency';

const config = {
    FULL_DATE_FORMAT: 'MM/DD/YYYY',
    REFRESH_DATE_FORMAT: 'MM/DD/YYYY hh:mm:ss A',
    AUDIT_LOG_DATE_FORMAT: 'MM/DD/YYYY h:mm:ss a',
    DB_DATE_FORMAT: 'YYYY-MM-DD',
    TIME_FORMAT: 'hh:mm A',
    UNIQUE_DATE: 'YYYYMMDDhhmmss',
    APPLICATION_DATE_TIME_FORMAT: 'MM-DD-YYYY hh:mm A',
    APPLICATION_DATE_FORMAT: 'MM-DD-YYYY',
    BATCH_DATE_FORMAT: 'MM/DD/YY hh:mm A',
    CLIENT_IP_FIND_URL: "https://ipinfo.io/json?token=",
    IP_INFO_TOKEN: "5da7f0e83e36e3",
    USER_NAME_LOCAL_STORAGE_KEY: "userName",
    AWS_AMORTIZED_COST: "AmortizedCost",
    NOTIFICATION_REFRESH_INTERVAL: 30000,
    FILE_UPLOAD_INTERVAL: 3000,
    CURRENCY_DOLLAR_SYMBOL: "$"
};

export default config;
export const reCaptchaSiteKey = '6LefCagZAAAAALFT9NxOCtuQEskWL6tUSvO_Vol-';

export const placeHolderImageSrc = 'assets/images/placeholder.png';
export const popupConfigurationObj: SweetAlertOptions = {
    title: 'Confirmation',
    text: 'Are you sure you want to remove this Role?',
    confirmButtonText: 'Confirm',
    buttonsStyling: false,
    position: 'top',
    customClass: {
        container: 'container-class',
        popup: 'popup-class',
        header: 'header-class',
        title: 'title-class',
        closeButton: 'close-button-class',
        icon: 'icon-class',
        image: 'image-class',
        content: 'content-class',
        input: 'input-class',
        actions: 'actions-class',
        confirmButton: 'confirm-button-class',
        cancelButton: 'cancel-button-class',
        footer: 'footer-class'
    }
};
/*
//showCancelButton: true,
 //   confirmButtonClass: 'btn btn-info',
    //cancelButtonClass: 'btn btn-warning',
*/

export const ngxUiLoaderConfig: NgxUiLoaderConfig = {
    bgsColor: 'rgba(255,255,255,0.8)',
    fgsColor: '#2F2F2F',
    bgsOpacity: 0.6,
    bgsPosition: 'bottom-right',
    bgsSize: 50,
    bgsType: 'ball-spin-clockwise',
    blur: 0,
    fgsPosition: 'center-center',
    fgsSize: 40,
    fgsType: 'ball-spin-clockwise',
    gap: 24,
    logoPosition: 'center-center',
    logoSize: 50,
    overlayBorderRadius: '0',
    overlayColor: 'transparent',
    pbColor: '#2F2F2F',
    pbDirection: 'ltr',
    pbThickness: 3,
    hasProgressBar: true,
    text: '',
    textColor: '#FFFFFF',
    textPosition: 'center-center'
};

export const UTILITY_CHART_COLORS = {
    APPROVED: "#5def09fc",
    CANCELLED: "#000",
    REJECTED: "#ff4000",
    AUTO_REJECTED: "#f44336c7",
    SUBMITTED_SCAN_IN_PROGRESS: "#ffff0082"
};
export const UTILITY_MAX_FILE_SIZE = 12288;
export const DATA_FILE_MAX_FILE_SIZE = 12288;
export const TARGET_FILE_MAX_FILE_SIZE = 12288;

export const MODULE_NAME_MAP = {
    USER_MANAGEMENT: 'User Management',
    ROLE_MANAGEMENT: 'Role Management'
};

// export const DEPENDENT_MODULES_MAP = {
//     'User Management': [MODULE_NAME_MAP.ROLE_MANAGEMENT, MODULE_NAME_MAP.ORGANIZATION_MANAGEMENT, MODULE_NAME_MAP.ORGANIZATION_TYPE_MANAGEMENT],
//     'Environment Management': [MODULE_NAME_MAP.ENVIRONMENT_TYPE_MANAGEMENT, MODULE_NAME_MAP.ORGANIZATION_MANAGEMENT]
// };

export const MODULE_NAME_DEFAULT_INFO_MAP = {
    'User Management': {
        class: 'card-header-success'
    },
    'Role Management': {
        class: 'card-header-warning'
    },
    'Task Module Configuration': {
        class: 'card-header-success'
    }
};


export const ROUTES_CONFIG_MAP = {
    AUTHENTICATION: {
        BASE_URL: 'auth',
        FULL_URL: '/auth/index'
    },
    UNAUTHORIZED: {
        BASE_URL: 'un-authorized',
        FULL_URL: 'un-authorized'
    },
    ROLE_MANAGEMENT: {
        BASE_URL: 'role-management',
        FULL_URL: '/role-management/list'
    },
    SUPPLIER_LANDING: {
        BASE_URL: 'supplier-landing',
        FULL_URL: '/supplier-landing/list'
    },
    EMPLOYEE_DASHBOARD: {
        BASE_URL: 'employee-dashboard',
        FULL_URL: '/employee-dashboard/list'
    },
    DASHBOARD: {
        BASE_URL: 'dashboard',
        FULL_URL: '/dashboard/list'
    },
    TARGET_DEFINITION: {
        BASE_URL: 'target-definition',
        FULL_URL: '/target-definition/list'
    },
    VENDOR_MANAGEMENT: {
        BASE_URL: 'vendor-management',
        FULL_URL: '/vendor-management/list'
    },
    WORKSPACE_MANAGEMENT: {
        BASE_URL: 'workspace',
        FULL_URL: '/workspace/list'
    },
    DATASET_TYPE_MANAGEMENT: {
        BASE_URL: 'dataset-type-management',
        FULL_URL: '/dataset-type-management/list'
    },
    USER_MANAGEMENT: {
        BASE_URL: 'user-management',
        FULL_URL: '/user-management/user-list'
    },
    REPORTS: {
        BASE_URL: 'reports',
        FULL_URL: '/reports/list'
    },
    SOURCE_TARGET_MAPPING: {
        BASE_URL: 'source-target-mapping',
        FULL_URL: '/source-target-mapping/mapping-definition-list'
    },
    DATA_SOURCE_CONFIGURATION: {
        BASE_URL: 'data-source-configuration',
        FULL_URL: '/data-source-configuration/list'
    },
    TAG_TYPE_MANAGEMENT: {
        BASE_URL: 'tag-type-management',
        FULL_URL: '/tag-type-management/list'
    },
    COST_MANAGEMENT: {
        BASE_URL: 'cost-reports',
        FULL_URL: '/cost-reports/list'
    },
    UTILITY_MANAGEMENT: {
        BASE_URL: 'utility-management',
        FULL_URL: '/utility-management/list'
    },
    DATA_UPLOAD: {
        BASE_URL: 'data-upload',
        FULL_URL: '/data-upload/list'
    },
    DATASET_EXTRACTION: {
        BASE_URL: 'dataset-extraction',
        FULL_URL: '/dataset-extraction/list'
    },
    ENVIRONMENT: {
        BASE_URL: 'environment',
        FULL_URL: '/environment/list'
    },
    NOTIFICATION_MANAGEMENT: {
        BASE_URL: 'notification',
        FULL_URL: '/notification/list'
    }
};

export const MODULE_BASE_URL_MODULE_ID_MAP = {
    'role-management': 'FA21B164-E9EA-423F-8728-057450395932',
    'user-management': '5FACEDA2-5F02-4DC9-8DA8-2526169A31A2',
    'supplier-landing': '8745d9ff-0bd9-4474-b736-5eaa277ef1ff',
    'reports': 'ef5b66c9-d930-4880-b7b5-e644d7bdb38c',
    'workspace': 'ef5b66c9-d930-4880-b7b5-e644d7bdb38c',
    'data-source-configuration': '6929deef-ea74-4f88-95bd-a0b51def412b',
    'tag-type-management': '197F7C73-A810-42B9-91D0-31E530F792EE',
    'utility-management': '3637423B-F149-4631-B93C-3F2C28FC841A',
    'data-upload': '025395B3-9736-45B7-947A-DF99F49AF51B', // Given Data Ingestion module ID
    'cost-reports': '937bda6b-fe97-4c04-a02f-45dc5132331b',
    'dataset-extraction': 'B22D018B-FC56-4AB1-A24F-71A88B3B4987',
    'environment': '861D7F23-5689-44B8-892A-CF6C17B77D53',
    'dataset-type-management': '656DD28E-197E-450C-A8AB-089F98D7E302'
};

export const MODULE_ID_MAP_FOR_AUDIT = [
    'audit-log-management',
    'notification-management',
    'logging-management'
];

export const ERROR_TYPE_MAP = {
    NO_MODULE_ACCESS: 'NoModuleAccess'
};

export const MODULE_SYMBOL_PERMISSIONS_MAP = {
    R: 'canRead',
    C: 'canCreate',
    D: 'canDelete',
    U: 'canUpdate',
    E: 'canExecute'
};

export const MESSAGE_TYPE_MAP = {
    success: 'Success',
    info: 'info',
    error: 'error'
};

export const CONSTANT_META_MAP = {
    months: [
        { name: 'January', value: 1 },
        { name: 'February', value: 2 },
        { name: 'March', value: 3 },
        { name: 'April', value: 4 },
        { name: 'May', value: 5 },
        { name: 'June', value: 6 },
        { name: 'July', value: 7 },
        { name: 'August', value: 8 },
        { name: 'September', value: 9 },
        { name: 'October', value: 10 },
        { name: 'November', value: 11 },
        { name: 'December', value: 12 }
    ]
};


export const GridConfig = {
    columnClicked: 'ColumnClicked',
    editClicked: 'EditClicked',
    copyClicked: 'CopyClicked',
    checkBoxClicked: 'CheckBoxClicked',
    roleCheckBoxClicked: 'RoleCheckBoxClicked',
    parentCheckBoxClicked: 'ParentCheckBoxClicked',
    deleteClicked: 'DeleteClicked',
    auditLogClicked: 'auditLogClicked',
    headerChanged: 'HeaderChanged',
    gridInit: 'gridInit',
    dataChanged: 'DataChanged',
    clearSearch: 'ClearSearch',
    actionIconClicked: 'actionIconClicked',
    acceptedValueIconClicked: 'acceptedValueIconClicked',
    previewTargetDefinitionClicked: 'previewTargetDefinitionClicked',
    downloadFileClicked: 'downloadFileClicked',
    downloadCleanFileClicked: 'downloadCleanFileClicked',
    viewDataUsageAgreement: 'viewDataUsageAgreement',
    viewErrorsClicked: 'viewErrorsClicked',
    approveRejectClicked: 'approveRejectClicked',
    cancelRequestClicked: 'cancelRequestClicked',
    terminateRequestClicked: 'terminateRequestClicked',
    mappingClicked: 'mappingClicked',
    toggleClicked: 'toggleClicked',
    splitClicked: 'splitClicked',
    mergeClicked: 'mergeClicked',
    onMouseHover: 'onMouseHover',
    dropDownChanged: 'dropDownChanged',
    viewCommentsClicked: 'viewCommentsClicked',
    rowSelected: 'rowSelected',
    requestMoreInfoClicked: 'requestMoreInfoClicked',
    beneficiaryClicked: 'beneficiaryClicked',
    requestMoreUtilityOrDataSetClicked: "requestMoreUtilityOrDataSetClicked",
    resubmitClicked: "resubmitClicked",
    refreshClicked: 'refreshClicked'
};

export const rowClickEnabledDataTablesIds = ['versionList'];

export const NOTES_MIN_LENGTH = 3;

export const CURRENCY_MODEL = {
    language: 'en-US',
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2
};


export const COMPARATOR_FUNCTION_MAP = {
    isEquals: (LHS, RHS) => LHS && RHS ? LHS === RHS : false,
    isGreaterThan: (LHS, RHS) => LHS && RHS ? LHS === RHS : false
};

export const UTCTimeZone = 'Europe/London';

export const PDFExportColumnMaximumLength = 20;


export const LocalStorageKeyCollection = {
    ReferenceDataValue: 'ReferenceDataValue',
    ReferenceDataMasking: 'ReferenceDataMasking',
    AcceptedValueReferenceData: 'AcceptedValueReferenceData',
    TargetColumnList: 'TargetColumnList',
    TargetDefinitionStep1: 'TargetDefinitionStep1',
    TargetDefinitionStep1Prefix: '_TDStep1',
    TargetDefinitionStep2Prefix: '_TDStep2',
    MappingColumnList: 'MappingColumnList'
};

export const DelimiterMapping = [
    {
        id: "C19180B0-499D-40F0-B7C2-31A790B3979A",
        name: "Space",
        symbol: "'  '"
    }, {
        id: "428B2660-680A-4D75-9D80-50D772F7E978",
        name: "Comma",
        symbol: ","
    },
    {
        id: "C1B4EE91-B8B6-494C-850F-7DCA9EE26006",
        name: "Dot",
        symbol: "."
    },
    {
        id: "B5F71778-6E03-4782-93A1-2A6CE0C15D4B",
        name: "Pipe",
        symbol: "|"
    }];

export const AWS_REGION = [
    {
        "id": "us-east-1",
        "itemName": "us-east-1"
    },
    {
        "id": "us-east-2",
        "itemName": "us-east-2"
    },
    {
        "id": "us-west-1",
        "itemName": "us-west-1"
    },
    {
        "id": "us-west-2",
        "itemName": "us-west-2"
    },
    {
        "id": "ap-south-1",
        "itemName": "ap-south-1"
    }
];

export const DataTypeCollection = {
    Varchar: '259EA267-F017-4176-9904-4DB06D475794',
    NVarchar: '1E9A7FD7-A1C8-4EC0-8D64-50374F5D127D',
    Char: '204F07B6-A339-4721-A64C-A47B40001976',
    Int: '5812EAE6-3CB4-4DE0-B9BB-DAD39BE4A0FB',
    Date: '470EFBC8-E750-48AD-B645-C9887CB9E43F'
};

export const ENVIRONMENT_CHART_COLORS = {
    PENDINGAPPROVAL: "#7cb5ec",
    APPROVED: "#434348",
    REJECTED: "#90ed7d",
    REQUESTEDCHANGES: "#f7a35c"
};

export const UTILITY_FILTER_TYPE = {
    MY_SUBMITTED_FORMS: "MY_SUBMITTED_FORMS",
    ALL_APPROVAL_WAITING_FORM: "ALL_APPROVAL_WAITING_FORM"
}

export const NavBarRoutes = {
    rootLevel: [
        // {
        //     moduleId: "",
        //     name: "Dashboard",
        //     order: 1,
        //     route_url: "/employee-dashboard/list",
        //     isDefault: true
        // },

        // {
        //     moduleId: "9C4280E4-CD0D-4851-8F9A-9376E0F975F3",
        //     name: "Reports",
        //     order: 2,
        //     route_url: "/reports/list",
        //     isDefault: false,
        //     isDataIngestionRequired: true,
        //     isDataTransformationRequired: true
        // },
        {
            moduleId: "861D7F23-5689-44B8-892A-CF6C17B77D53",
            name: "Environment Management",
            route_url: "/environment/list",
            order: 1,
            isDefault: false,
            isNotAvailableForAdmin: false
        },
        {
            moduleId: "937BDA6B-FE97-4C04-A02F-45DC5132331B",
            name: "Reports",
            route_url: "/cost-reports/list",
            order: 2,
            isDefault: false,
            isNotAvailableForAdmin: false
        }
        // TODO disabling it for now, need to confirm with Ananth
        // {
        //     moduleId: "58052B08-9EF5-49F0-9DA0-E9DC0AA08396",
        //     name: "Source Target Mapping Old",
        //     order: 4,
        //     route_url: "/supplier-mapping/list",
        //     isDefault: false
        // }
    ],
    grouped: [{
        groupName: "Administration",
        groupItems: [
            // {
            //     moduleId: "FA21B164-E9EA-423F-8728-057450395932",
            //     name: "Vendor Management",
            //     route_url: "/vendor-management/list",
            //     groupName: "Administration",
            //     groupOrder: 1,
            //     isDefault: false,
            //     isNotAvailableForAdmin: false
            // },
            {
                moduleId: "FA21B164-E9EA-423F-8728-057450395932",
                name: "Role Management",
                route_url: "/role-management/list",
                groupName: "Administration",
                groupOrder: 1,
                isDefault: false,
                isNotAvailableForAdmin: false
            },
            {
                moduleId: "5FACEDA2-5F02-4DC9-8DA8-2526169A31A2",
                name: "User Management",
                route_url: "/user-management/user-list",
                groupName: "Administration",
                groupOrder: 2,
                isDefault: false,
                isNotAvailableForAdmin: false
            },
            {
                moduleId: "197F7C73-A810-42B9-91D0-31E530F792EE",
                name: "Tag Type Management",
                route_url: "/tag-type-management/list",
                groupName: "Administration",
                groupOrder: 3,
                isDefault: false,
                isNotAvailableForAdmin: false
            },
            {
                moduleId: "3637423B-F149-4631-B93C-3F2C28FC841A",
                name: "Utility Management",
                groupName: "Administration",
                groupOrder: 4,
                route_url: "/utility-management/list",
                isDefault: false,
                isNotAvailableForAdmin: false
            }
            // {
            //     moduleId: "FA21B164-E9EA-423F-8728-057450395932",
            //     name: "My Workspace",
            //     route_url: "/workspace/list",
            //     groupName: "Administration",
            //     groupOrder: 4,
            //     isDefault: false,
            //     isNotAvailableForAdmin: false
            // },
            // {
            //     moduleId: "7F1FF09D-4F37-4178-B987-CA9D81D9703D",
            //     name: "Dataset Management",
            //     route_url: "/datasets/list",
            //     groupName: "Administration",
            //     groupOrder: 5,
            //     isDefault: false,
            //     isNotAvailableForAdmin: false
            // },
            // {
            //     moduleId: "2132E939-5A27-467C-A9AB-F364625CDB52",
            //     name: "Vendor Management",
            //     route_url: "/vendor-management/list",
            //     groupName: "Administration",
            //     groupOrder: 6,
            //     isDefault: false,
            //     isNotAvailableForAdmin: false
            // },
            // {
            //     moduleId: "F7BCAB49-2BBC-49CB-85A2-7BFF108E45D1",
            //     name: "Workspace Management",
            //     route_url: "/workspace/list",
            //     groupName: "Administration",
            //     groupOrder: 7,
            //     isDefault: false,
            //     isNotAvailableForAdmin: false
            // },

        ]
    },
    {
        groupName: "Data Management",
        groupItems: [
            {
                moduleId: "656DD28E-197E-450C-A8AB-089F98D7E302",
                name: "Dataset Type",
                route_url: "/dataset-type-management/list",
                groupName: "Dataset Type Management",
                groupOrder: 1,
                isDefault: false
            },
            {
                moduleId: "6929DEEF-EA74-4F88-95BD-A0B51DEF412B",
                name: "Data Source Configuration",
                route_url: "/data-source-configuration/list",
                groupName: "Data Management",
                groupOrder: 1,
                isDefault: false,
                isNotAvailableForAdmin: false
            },
            {
                grouped: [{
                    groupName: "Data Ingestion",
                    moduleId: "025395B3-9736-45B7-947A-DF99F49AF51B",
                    groupItems: [{
                        moduleId: "4FEF9828-282E-4939-94B2-0396255982D3",
                        name: "Data Uploads",
                        route_url: "/data-upload/list",
                        groupName: "Data Management",
                        groupOrder: 1,
                        isDefault: false
                    }]
                }],
                groupName: "Data Management",
                groupOrder: 3,
                isDefault: false
            },
            {
                moduleId: "BFC84F3B-F694-45EE-805E-CF2F28E77EC4",
                name: "Target Definitions",
                route_url: "target-definition/list",
                groupName: "Data Management",
                groupOrder: 3,
                isDefault: false,
                isNotAvailableForAdmin: true
            }
            ,
            {
                grouped: [{
                    groupName: "Data Transformation",
                    moduleId: "6CEBF9C5-66F3-47BD-8CE2-8B485072A0FF",
                    groupItems: [{
                        moduleId: "58052B08-9EF5-49F0-9DA0-E9DC0AA08396",
                        name: "Preview Target Definitions",
                        route_url: "source-target-mapping/preview-target-definition",
                        groupName: "Data Management",
                        groupOrder: 1,
                        isDefault: false,
                        isNotAvailableForAdmin: true
                    }, {
                        moduleId: "58052B08-9EF5-49F0-9DA0-E9DC0AA08396",
                        name: "Mapping Definitions",
                        route_url: "/source-target-mapping/mapping-definition-list",
                        groupName: "Data Management",
                        groupOrder: 2,
                        isDefault: false,
                        isNotAvailableForAdmin: true,
                    }]
                }],
                groupName: "Data Management",
                groupOrder: 4,
                isDefault: false
            },
            {
                moduleId: "B22D018B-FC56-4AB1-A24F-71A88B3B4987",
                name: "Dataset Extraction",
                route_url: "/dataset-extraction/list",
                groupName: "Data Management",
                groupOrder: 5,
                isDefault: false
            }

        ]
    }
    ]
};

export const SYSTEM_CONFIGURATION_CODE = "SESSION_ALERT_TIME";
export const SESSION_EXPIRY_MINUTES = 30;

export const UrlsWithoutHeaderAuth = [
    "auth/check-reset-password-link-valid/",
    "auth/changepassword"
];

export enum ModalType {
    CREATE = "Create",
    EDIT = "Edit",
    DETAIL = "Detail"
};
