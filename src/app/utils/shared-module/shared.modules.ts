// Angular core imports
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

// other libraries import
import { AngularEditorModule } from "@ventech/angular-editor";
import { NgxUiLoaderModule } from "ngx-ui-loader";

// custom files, components imports
import { ngxUiLoaderConfig } from "../config";
import { AlertComponent } from "../alert/alert.component";
import { AlertService } from "../alert/alert.service";
import { ClickOutsideDirective } from "../alert/click-outside-directive";
import { DataGridModule } from "../../modules/core-ui-components/data-grid/data-grid.module";
import { DataGridExtendedModule } from "../../modules/core-ui-components/data-grid-extended/data-grid-extended.module";

import { HttpRequestResponseInterceptor } from "../interceptors/http-request-response.interceptor";

import {MatNativeDateModule} from "@angular/material/core";
import {MatDatepickerModule} from "@angular/material/datepicker";
import { MultiSelectModule } from "@syncfusion/ej2-angular-dropdowns";
import { NgSelectModule } from '@ng-select/ng-select';
import { MatSelectModule } from "@angular/material/select";

const materialModules = [
    MatNativeDateModule,
    MatDatepickerModule,
    MultiSelectModule,
    NgSelectModule,
    MatSelectModule
];

const angularModules = [
    FormsModule,
    ReactiveFormsModule,
];

const importModules = [
    ...angularModules,
    ...materialModules,
    DataGridModule,
    DataGridExtendedModule,
    CommonModule,
    AngularEditorModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig)
];

const exportModules = [
    ...angularModules,
    ...materialModules,
    NgxUiLoaderModule,
    AlertComponent,
    CommonModule,
    ClickOutsideDirective,
    AngularEditorModule,
    DataGridModule,
    DataGridExtendedModule
];


@NgModule({
    declarations: [AlertComponent, ClickOutsideDirective],
    imports: importModules,
    exports: exportModules,
    providers: [
        AlertService,
        { provide: HTTP_INTERCEPTORS, useClass: HttpRequestResponseInterceptor, multi: true },
    ]
})
export class SharedModules {

}
