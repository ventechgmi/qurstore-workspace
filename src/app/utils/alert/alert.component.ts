import { Component, OnInit, OnDestroy, AfterViewInit, ElementRef } from "@angular/core";
import { Subscription, BehaviorSubject, Observable } from "rxjs";
import _ from "underscore";
import { AlertService } from "./alert.service";

@Component({
  selector: "app-alert-component",
  styleUrls: ["./alert.component.scss"],
  templateUrl: "alert.component.html"
})
export class AlertComponent implements OnInit, OnDestroy {
  public subscriptions = [];

  private readonly failureMessage = "Server failed to fetch the data";
  private readonly failureStatus = 500;
  private readonly failureStatusText = "OK";

  messageInfo: any = {
    text: "",
    toStable: false
  };

  constructor(private alertService: AlertService, public alertMsgElementRef: ElementRef) { }

  ngOnInit() {


    this.subscriptions.push(this.alertService.getMessage().subscribe(messageInfo => {
      let text = null;
      const textInfo = messageInfo.textInfo;
      if (_.isObject(textInfo)) {
        text = this.extractErrorMessage(textInfo);
      }
      if (typeof textInfo === "string" || textInfo instanceof String) {
        text = messageInfo.textInfo;
      }
      this.messageInfo = { ...messageInfo, text };
      if (!messageInfo.stopFocus) {
        setTimeout(() => {
          if (this.alertMsgElementRef.nativeElement.ownerDocument.getElementById("alert-info")) {
            this.alertMsgElementRef.nativeElement.ownerDocument.getElementById("alert-info").scrollIntoView({ block: "center", behavior: "smooth" });
          }
        }, 0);
      }

      // over all message should be less than 3 seconds
      setTimeout(() => {
        this.close();
      }, 10000);
    }));

    // this subscription will help to show error or success message in another screen(details screen delete success message showing in list screen)
    if (this.alertService.afterInitObs) {
      this.subscriptions.push(this.alertService.afterInitObs.subscribe(messageInfo => {
        if (messageInfo.type) {
          this.messageInfo = messageInfo;
        }
        setTimeout(() => {
          this.close();
        }, 10000);
      }));
    }


  }

  extractErrorMessage(error) {
    let message = "";
    const err = error.error;
    if(typeof err === 'string' && error.status === this.failureStatus && error.statusText === this.failureStatusText) {
      return this.failureMessage;
    }
    if (err && err.message) {
      message = err.message;
    }
    if (err && !err.message) {
      message = error.statusText;
    }
    if (err && err.diagnostics) {
      message = err.diagnostics;
    }
    return message;
  }

  close() {
    if (this.messageInfo.toStable) {
      this.messageInfo.text = "";
    }
  }

  ngOnDestroy() {
    _.each(this.subscriptions, subscription => {
      if (subscription) { subscription.unsubscribe(); }
    });
  }
}
