import { Directive, ElementRef, Output, EventEmitter, HostListener } from "@angular/core";

@Directive({
    selector: "[appClickOutside]"
})
export class ClickOutsideDirective {

    isInitialEvent = false;
    constructor(private elementRef: ElementRef) {
        this.isInitialEvent = true;
    }

    @Output()
    public clickOutside = new EventEmitter<MouseEvent>();

    @HostListener("document:click", ["$event", "$event.target"])
    public onClick(event: MouseEvent, targetElement: HTMLElement): void {

        // to skip initial event
        if (this.isInitialEvent) {
            this.isInitialEvent = false;
            return;
        }

        if (!targetElement || targetElement.nodeName === "BUTTON" || targetElement.nodeName === "INPUT") {
            return;
        }
        // don't check for checkbox
        if (targetElement.className === "check") {
            return;
        }
        const clickedInside = this.elementRef.nativeElement.contains(targetElement);
        if (!clickedInside) {
            this.clickOutside.emit(event);
        }
    }

}
