import { Injectable } from "@angular/core";
import { Router, NavigationStart } from "@angular/router";
import { Observable, Subject, BehaviorSubject } from "rxjs";

@Injectable()
export class AlertService {
  private subject = new Subject<any>();
  private keepAfterNavigationChange = false;
  public afterInitObs: Observable<any>;

  constructor(private router: Router) {
    // clear alert message on route change
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this.keepAfterNavigationChange) {
          // only keep for a single location change
          this.keepAfterNavigationChange = false;
        } else {
          // clear alert
          this.subject.next({});
        }
      }
    });
  }

  success(message: string, toStable?: boolean, stopFocus?: boolean, keepAfterNavigationChange?: boolean) {
    this.keepAfterNavigationChange = keepAfterNavigationChange ? keepAfterNavigationChange: false;
    this.subject.next({
      type: "success", textInfo: message, toStable: toStable ? toStable : false,
      stopFocus: stopFocus ? stopFocus : false
    });
  }

  info(message: string, toStable?: boolean, stopFocus?: boolean, keepAfterNavigationChange?: boolean) {
    this.keepAfterNavigationChange = keepAfterNavigationChange ? keepAfterNavigationChange: false;
    this.subject.next({ type: "info", textInfo: message, toStable: toStable ? toStable : false, stopFocus: stopFocus ? stopFocus : false });
  }

  error(message: any, toStable?: boolean, stopFocus?: boolean, keepAfterNavigationChange?: boolean) {
    this.keepAfterNavigationChange = keepAfterNavigationChange ? keepAfterNavigationChange: false;
    this.subject.next({ type: "error", textInfo: message, toStable: toStable ? toStable : false, stopFocus: stopFocus ? stopFocus : false });
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  clearSubject() {
    this.subject.next("");
  }
}
