
import { EntityBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { from as observableFrom,interval} from "rxjs";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ActivatedRoute } from "@angular/router";
import config from "./utils/config";
import _ from 'lodash';
import { NotificationService } from './modules/notification-management/services/notification.service';
import moment from "moment";
import { Injectable } from "@angular/core";
import { AuthService } from "./utils/auth.service";

@Injectable()
export class AppViewModel {
    public notificationList: any;
    public notificationCount: number = 0;

    public constructor(
        public notificationService: NotificationService,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute,
        public authService: AuthService,)
    {
      this.getNotifications();
      this.getUnreadNotificationCount();
    }

  public init() {
    interval(config.NOTIFICATION_REFRESH_INTERVAL).subscribe(() => {
      if (this.authService.hasToken()) {
        this.getNotifications(); this.getUnreadNotificationCount();
      }
    });
    return observableFrom([]);
  }
  
  public redirectToNotificationDetailsPage(url: string, id: string): void {
    const request = { Id: id }
    this.notificationService.updateNotificationAsRead(request).subscribe();
    if (url) {
      window.open(url);
    }
  }
    
      public updateAllUnreadNotificationAsRead(){
        this.notificationService.updateAllUnreadNotificationAsRead().subscribe();
      }

      
  private getNotifications(): void {
    this.notificationService.getLimitedNotification().subscribe(data => {
      if (data) {
        this.notificationList = data;
        _.each(this.notificationList, item => {
          item.nameInitials = item.sender.fullName.match(/\b(\w)/g).join('');
          item.notificationDate = moment(item.notificationDate).format(config.APPLICATION_DATE_TIME_FORMAT)
        });
      }
    });
  }

  private getUnreadNotificationCount(): void {
    this.notificationService.getUnreadNotificationCount().subscribe(data => {
      if (data) {
        this.notificationCount = data;
      }
    });
  }
}
