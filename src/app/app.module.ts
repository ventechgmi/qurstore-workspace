import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FastGenUIFrameworkModule, APP_CONFIG } from "@fastgen/ui-framework";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { APP_BASE_HREF, LocationStrategy, HashLocationStrategy } from "@angular/common";
import { environment } from "../environments/environment";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from "@angular/material/form-field";
import { JwtModule } from "@auth0/angular-jwt";
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';
import { AuthenticationModule } from './modules/authentication/authentication.module';
import { AuthService } from "./utils/auth.service";
import { AuthorizationTokenManager } from "./utils/authentication-token-manager";
import { AuthGuardService } from "./utils/auth-guard.service";
import { RoleManagementModule } from './modules/role-management/role-management.module';
import { TargetDefinitionModule } from './modules/target-definition/target-definition.module';
import { UserManagementModule } from './modules/user-management/user-management.module';
import { SourceTargetMappingModule } from './modules/source-target-mapping/source-target-mapping.modules';
import { SupplierLandingModule } from './modules/supplier-landing/supplier-landing.module';
import { EmployeeDashboardModule } from './modules/employee-dashboard/employee-dashboard.module';
import { ReportsModule } from './modules/reports/reports.module';
import { VendorManagementModule } from './modules/vendor-management/vendor-management.module';
import { WorkspaceModule } from './modules/workspace/workspace.module';
import { DatasetsModule } from './modules/datasets/datasets.module';
import { DataSourceConfigurationModule } from './modules/data-source-configuration/data-source-configuration.module';
import { TagtypeManagementModule } from './modules/tag-type-management/tag-type-management.module';
import { EnvironmentModule } from './modules/environment/environment.module';
import { UtilitiesManagementModule } from './modules/utility-management/utility-management.module';
import { DataFileUploadManagementModule } from './modules/data-upload-management/data-upload-management.module';
import { CostManagementModule } from './modules/cost-management/cost-management.module';
import { DatasetExtractionModule } from './modules/dataset-extraction/dataset-extraction.module';
import { BnNgIdleService } from 'bn-ng-idle';
import { MatBadgeModule } from '@angular/material/badge';
import { NotificationManagementModule } from './modules/notification-management/notification-management.module';
import { MatMenuModule } from '@angular/material/menu';
import { ToastrModule } from 'ngx-toastr';
import { SessionExpirationAlert, SessionInteruptService } from "session-expiration-alert"
import { AppSessionInterruptService } from "./utils/app-session-interrupt.service";
import { SESSION_EXPIRY_MINUTES } from "./utils/config";
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    MatInputModule,
    MatFormFieldModule,
    MatBadgeModule,
    MatMenuModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FastGenUIFrameworkModule,
    BrowserAnimationsModule,
    NgbModule,
    RoleManagementModule,
    ReportsModule,
    UserManagementModule,
    SourceTargetMappingModule,
    CostManagementModule,
    VendorManagementModule,
    WorkspaceModule,
    DatasetsModule,
    SupplierLandingModule,
    EmployeeDashboardModule,
    DataSourceConfigurationModule,
    TagtypeManagementModule,
    EnvironmentModule,
    UtilitiesManagementModule,
    NotificationManagementModule,
    DataFileUploadManagementModule,
    DatasetExtractionModule,
    TargetDefinitionModule,
    NgxSmartModalModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: AuthorizationTokenManager.prototype.getEncodedAuthToken
      }
    }),
    ToastrModule.forRoot(),
    MatPasswordStrengthModule.forRoot(),
    SessionExpirationAlert.forRoot({ totalMinutes: SESSION_EXPIRY_MINUTES })
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: "/" },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    {
      provide: APP_CONFIG,
      useValue: {
        apiEndPoint: environment.apiEndPoint,
        environment: environment.env
      }
    },
    NgxSmartModalService,
    AuthService,
    AuthGuardService,
    AuthorizationTokenManager,
    BnNgIdleService,
    {
      provide: SessionInteruptService,
      useClass: AppSessionInterruptService,
    },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
