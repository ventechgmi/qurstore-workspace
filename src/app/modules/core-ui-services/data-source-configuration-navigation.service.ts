import { HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { AppConfig, APP_CONFIG } from "@fastgen/ui-framework";
import { BaseProxyNavigationService } from "./base/base-proxy-navigation.service";

@Injectable({
  providedIn: "root"
})
export class DataSourceConfigurationNavigationService extends BaseProxyNavigationService {
  constructor(router: Router, http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
    super(router, http, appConfig);
  }

}
