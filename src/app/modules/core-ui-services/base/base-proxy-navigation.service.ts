
import { map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { Inject } from "@angular/core";
import { NavigationExtras, Router } from "@angular/router";
import { AppConfig, APP_CONFIG } from "@fastgen/ui-framework";
import { Observable } from "rxjs";
import * as uuid from "uuid";
const UUIDEncoder = require("uuid-encoder");

/**
 * @desc Provides the ability to pass multiple parameters into a specified URL under the alias of a single parameter.
 */
export abstract class BaseProxyNavigationService {
  private uuidEncoder = new UUIDEncoder("base62");

  constructor(protected router: Router, protected http: HttpClient, @Inject(APP_CONFIG) protected appConfig: AppConfig) { }

  /**
   * Navigate to the specified URL, masking the parameters
   * @param url The URL to navigate to.  This should only accept one parameter, which is the linkId which is derived from base62 encoding a random UUID
   *
   * The service will store the parameters in local storage, using the linkId as the key. If the **saveLink** parameter is true (default), the
   * linkId and the parameters will be stored in a shared location (i.e., database) so that the link may be shared. A receiving endpoint will
   * simply use this parameter in the call to **navigationParametersForLink** to retrieve the actual parameters.
   *
   * @param url The target URL
   * @param parameters The JSON structure containing the parameters to be stored under the derived linkId
   * @param openInNewWindow If true, then the specified link will be opened in a new window
   * @param extras The NavigationExtras for the Angular router (.navigate)
   */
  public async navigateUsingProxy({
    url,
    parameters,
    queryParams,
    isQueryParams,
    saveLink = true,
    openInNewWindow = false,
    extras
  }: {
    url: string;
    parameters: {};
    queryParams?: {};
    isQueryParams?: boolean;
    saveLink?: boolean;
    openInNewWindow?: boolean;
    extras?: NavigationExtras;
  }): Promise<boolean> {
    const linkId: string = this.encodeValue(uuid.v4());
    this.createLinkParametersAndShortcut(linkId, parameters, saveLink);
    if (openInNewWindow) {
      let handle: Window;
      if (isQueryParams) {
        handle = window.open(`#${this.router.createUrlTree([url], { queryParams: { ...queryParams, key: linkId } }).toString()}`, linkId);
      } else {
        handle = window.open(`#${this.router.createUrlTree([url, linkId], extras).toString()}`, linkId);
      }
      return Promise.resolve(!!handle);
    } else {
      return this.router.navigateByUrl(`${url}/${linkId}`);
    }
  }

  /**
   * Navigate without using a proxy (e.g., URL Shortcut)
   * @param url The target URL
   * @param parameters The URL parameters to be included with the URL 
   * @param queryParams The query parameters to be passed in (e.g., id="value",e tc.)
   * @param openInNewWindow If true, then the specified link will be opened in a new window
   * @param extras The NavigationExtras for teh Angular router (.navigate)
   */
  public async navigateWithoutProxy({
    url,
    parameterString,
    queryParams,
    openInNewWindow = false,
    extras
  }: {
    url: string;
    parameterString?: string;
    queryParams?: any;
    openInNewWindow?: boolean;
    extras?: NavigationExtras;
  }): Promise<boolean> {
    if (openInNewWindow) {
      let handle: Window;
      if (!!parameterString) {
        handle = window.open(`#${this.router.createUrlTree([url, parameterString], extras).toString()}`, uuid.v4());
      } else if (!!queryParams) {
        handle = window.open(`#${this.router.createUrlTree([url], { queryParams: queryParams }).toString()}`, uuid.v4());
      } else {
        handle = window.open(`#${this.router.createUrlTree([url]).toString()}`, uuid.v4());
      }
      return Promise.resolve(!!handle);
    } else {

      if (!!parameterString) {
        return this.router.navigateByUrl(`${url}/${parameterString}`);
      } else if (!!queryParams) {
        return this.router.navigate([url], { queryParams: queryParams }).then().catch();
      } else {
        return this.router.navigate([url]).then().catch();
      }
    }
  }

  /**
   * Navigate using a proxy, directly encoding the single parameter passed. This is mainly used when a single parameter specified (e.g., UUID)
   * is being passed and we do not want to directly show the raw value to the user.
   * @param url The target URL
   * @param parameter A string containing the parameter to be encoded
   * @param openInNewWindow If true, then the specified link will be opened in a new window
   * @param extras The NavigationExtras for teh Angular router (.navigate)

   */
  public async navigateUsingEncodedParameter({
    url,
    parameter,
    openInNewWindow = false,
    extras
  }: {
    url: string;
    parameter: string;
    openInNewWindow?: boolean;
    extras?: NavigationExtras;
  }): Promise<boolean> {
    const encodedParameter: string = this.encodeValue(parameter);
    if (openInNewWindow) {
      const handle = window.open(`#${this.router.createUrlTree([url, encodedParameter], extras).toString()}`, uuid.v4());
      return Promise.resolve(!!handle);
    } else {
      return this.router.navigateByUrl(`${url}/${encodedParameter}`);
    }
  }

  /**
   * This method is used in conjunction with the **navigateUsingEncodedParameter** method call to decode the encoded parameter value
   * @param encodedParameter The parameter that has been encoded (base62 by the **navigateUsingEncodedParameter** method) that must be decoded
   */
  public decodeProxyParameter(encodedParameter: string): string {
    if (!!encodedParameter) {
      try {
        return this.uuidEncoder.decode(encodedParameter);
      } catch (err) {
        /**
         * If the decode operation fails, return the raw parameter
         */
        return encodedParameter;
      }
    } else {
      return undefined;
    }
  }

  /**
   * Retrieve the parameters associated with the specified linkId (derived from the **navigateUsing** method)
   * @param linkId The value generated by the navigateUsing method
   * @returns any The parameters associated with the linkId, or **undefined** if not found
   */
  public async navigationParametersForLink(linkId: string): Promise<any> {
    return new Promise(resolve => {
      const data: string = localStorage.getItem(`__${linkId}`);
      if (!data) {
        this.getURLShortcut(linkId).subscribe({
          next: urlShortcutData => {
            if (!!urlShortcutData && !!urlShortcutData.payload) {
              resolve(JSON.parse(urlShortcutData.payload.payload));
            } else {
              resolve(undefined);
            }
          },
          error: error => {
            resolve(undefined);
          }
        });
      } else {
        if (data) {
          resolve(JSON.parse(data));
        } else {
          resolve(undefined);
        }
      }
    });
  }

  /**
   * Update the parameters associated with the specified linkId (derived from the **navigateUsing** method). Only the localStorage version will
   * be updated - allowing the user to customize the payload of the link without affecting the common shared payload
   * @param linkId The value generated by the navigateUsing method
   * @param parameters The JSON structure containing the parameters to be stored under the derived linkId
   */
  public updateLinkParametersForLink(linkId: string, parameters: {}): void {
    localStorage.setItem(`__${linkId}`, JSON.stringify(parameters));
  }

  /**
   * Remove the parameters associated with the specified linkId
   * @param linkId The value generated by the navigateUsing method
   */
  public removeLinkParameters(linkId: string): void {
    localStorage.removeItem(`__${linkId}`);
  }

  /**
   * Remove links from local storage
   */
  public cleanUp(): void {
    for (const key in localStorage) {
      if (key.startsWith("__")) {
        localStorage.removeItem(key);
      }
    }
  }

  /**
   * Encode the specified value as Base62
   * @param value The value to encode
   */
  public encodeValue(value: string): string {
    if (value !== undefined) {
      return this.uuidEncoder.encode(value);
    }
  }

  /**
   * Create a new URL Shortcut with the specified payload
   * @param linkId The value generated by the navigateUsing method
   * @param parameters The JSON structure containing the parameters to be stored under the derived linkId
   */
  private createLinkParametersAndShortcut(linkId: string, parameters: {}, saveLink?: boolean): void {
    localStorage.setItem(`__${linkId}`, JSON.stringify(parameters));
    if (saveLink) {
      this.createURLShortcut(linkId, parameters).catch();
    }
  }

  private getApi(): string {
    return this.appConfig.apiEndPoint + "/navigation-service";
  }

  /**
   * Create a URL Shortcut
   * @param linkId The URL Key for payload
   * @param payload The payload
   */
  private async createURLShortcut(linkId: string, payload: any): Promise<void> {
    this.http
      .post(this.getApi() + "/create", { urlKey: linkId, payload: JSON.stringify(payload) }).pipe(
        map((res: Response) => res))
      .subscribe(data => {
        // The result is not needed
      });
  }

  /**
   * Retrieve the payload for the specified linkId
   * @param linkId The linkId for the payload
   */
  private getURLShortcut(linkId: string): Observable<any> {
    return this.http.get(this.getApi() + "/key/" + linkId).pipe(map((res: Response) => res));
  }
}
