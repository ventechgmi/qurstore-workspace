import { Injectable, Inject } from "@angular/core";
import { Router } from "@angular/router";
import { BaseProxyNavigationService } from "./base/base-proxy-navigation.service";
import { HttpClient } from "@angular/common/http";
import { APP_CONFIG, AppConfig } from "@fastgen/ui-framework";

@Injectable({
  providedIn: "root"
})
/**
 * @desc Provides the ability to pass multiple parameters into a specified URL under the alias of a single parameter.
 */
export class NavigationProxyService extends BaseProxyNavigationService {
  constructor(router: Router, http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
    super(router, http, appConfig);
  }

  /**
   * Remove links from local storage
   */
  public cleanUp(): void {
    for (const key in localStorage) {
      if (key.startsWith("__")) {
        localStorage.removeItem(key);
      }
    }
  }
}
