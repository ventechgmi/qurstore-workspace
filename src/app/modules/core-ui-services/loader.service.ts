import { Injectable } from "@angular/core";
import _ from "lodash";


@Injectable({providedIn: "root"})
export class LoaderService {

    private isLoaderExcluded = false;
    private excludedRoutes = [];
    // module route path from app-routing.module.ts
    private excludedModules = [];

   public isLoaderExcludedInModule() {
        return this.isLoaderExcluded;
    }

    public setLoaderExclusion(baseUrl) {
        if (this.excludedModules.includes(baseUrl)) {
            this.isLoaderExcluded = true;
        }
    }

    public clearLoaderExclusion() {
        this.isLoaderExcluded = false;
    }

    public addLoaderExcludedAPIUrls(urls) {
        this.excludedRoutes.push(...urls);
    }

    public isUrlExcluded(url) {
        return _.some(this.excludedRoutes, (excludedUrl) => url.includes(excludedUrl));
    }
}