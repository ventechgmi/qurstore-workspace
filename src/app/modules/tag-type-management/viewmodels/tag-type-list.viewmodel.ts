import { from as observableFrom } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { TagtypeService } from "../services/tag-type.service";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { TagtypeViewModel } from "./tag-type.viewmodel";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AuthService } from "src/app/utils/auth.service";

@Injectable()
export class TagtypeListViewModel extends TagtypeViewModel {
  public userId: string;
  public tagTypeId: string;
  public tagTypeList: any;
  public alertService: AlertService;
  public userRolePermission: any = {};
  public viewModelDataGrid: DataGridViewModel;

  header = [{ headerName: "Tag Type Name", field: "name", isClickable: true }, { headerName: "Description", field: "description" }];
  // chartOptions: any;
  appliedFilters: any[];
  public constructor(
    public tagTypeService: TagtypeService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public authService: AuthService
  ) {
    super(tagTypeService, ViewModelType.Edit, ngxSmartModalService, route, authService);
  }

  public init(param?) {
    this.userId = param.userId;
    this.viewModelDataGrid = new DataGridViewModel();
    this.viewModelDataGrid.setPagination(true);
    this.userRolePermission = this.getUserClaims() || {};
    this.subscribeToCreateSuccessMessage();
    this.subscribers();
    this.viewModelDataGrid.setHeader(this.header, false, { canUpdate: this.userRolePermission.canUpdate });
    this.viewModelDataGrid.clickableColumnIndices = [1];
    this.setDataTableConfigurations();
    this.getTagTypeList();
    return observableFrom([]);
  }

  public redirectToCreatePage() {
    this.router
      .navigate(["/tag-type-management/create"])
      .then()
      .catch();
  }

  public addTagValue() {

  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  private subscribers() {
    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.columnClicked:
          this.tagTypeId = e.eventData.rowDetails.id;
          this.redirectToDetail();
          break;
        case GridConfig.editClicked:
          this.tagTypeId = e.eventData.rowDetails.id;
          this.redirectToEdit();
          break;
        default:
          if (e.eventData && e.eventData.rowDetails) {
            this.tagTypeId = e.eventData.rowDetails.id;
            // this.redirectToDetail();
          }
          break;
      }

    }, error => { });
  }

  // Getting all the details and redirecting to the details page
  private redirectToDetail() {
    this.tagTypeService.tagTypeIdSubject.next(this.tagTypeId);
    this.router
      .navigate(["/tag-type-management/" + this.tagTypeId])
      .then()
      .catch();
  }

  public redirectToEdit() {
    this.tagTypeService.tagTypeIdSubject.next(this.tagTypeId);
    this.router
      .navigate(["/tag-type-management/" + this.tagTypeId + "/" + true + "/" + false])
      .then()
      .catch();
  }

  private deleteUserFromList(tagTypeId) {
    this.router
      .navigate(["/tag-type-management/delete/" + tagTypeId + "/IsDeleteFromList"])
      .then()
      .catch();
  }





  // data-table configurations for project management
  public setDataTableConfigurations() {
    this.viewModelDataGrid.dataTableConfiguration = {
      destroy: true,
      columnDefs: [{ targets: [0], visible: false, searchable: false }, { type: "num-html", targets: [2] }, { orderable: false, targets: 2, searchable: false }],
      order: [[1, "asc"]]
    };
  }

  public getTagTypeList() {
    this.tagTypeService.getAllTagTypes().subscribe(
      data => {
        if (data) {
          this.tagTypeList = data;
          this.initGrid(this.tagTypeList);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public initGrid(data) {
    const tagTypes = data ? data : [];
    this.viewModelDataGrid.setRowsData(tagTypes, false);
  }

  public subscribeToCreateSuccessMessage() {
    this.tagTypeService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      if (!data.isCreateAnother) {
        this.alertService.success(message, toStable);
      }
    });
  }
}
