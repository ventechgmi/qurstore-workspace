import { from as observableFrom, Observable, Subject, forkJoin, Subscription } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { TagtypeService } from "../services/tag-type.service";
import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import * as _ from "underscore";
import { TagtypeViewModel } from "./tag-type.viewmodel";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ViewModelType } from "@fastgen/ui-framework";
import { GuidHelper } from '../../employee-dashboard/helpers/guid-helper';
import { AuthService } from 'src/app/utils/auth.service';
import { ValidationMessages, FormValidationMessages } from "../helpers/validation-messages";
import { UserService } from "../../user-management/services/user.service";
import { ExternalAuthService } from "../../user-management/services/external.authentication.service";
import { Interface } from "readline";
import { ModalName } from "../helpers/constants";
@Injectable()
export class TagtypeCreateEditViewModel extends TagtypeViewModel {
  public currentUserOrganizationId: string;
  public providerDetails;
  public adGroupsAndUsers = [];
  public adUnGroupedUsers = [];
  public userId: string;
  public tagTypeId: string;
  public tagTypeList: any;
  public alertService: AlertService;
  public allUsers: any;
  public userRolePermission: any = {};
  public selectedItems: any;
  public tagValueList = [];
  public pristineTagValueList = [];

  tagTypeDetailsForm: FormGroup;
  submitted = false;
  modalType = "Edit" || "Create" || "Detail";
  contexts = [];
  valueMethodTypes: any;
  tagTypeDetail: any = {};
  deleteTagtypeId: string;
  IsDeleteFromList: boolean;
  subscription: Subscription;
  arrValues = new Array();
  currentTagTypeContextMappingIds = [];
  editModeFromDetail: any;

  public constructor(
    public tagTypeService: TagtypeService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public authService: AuthService,
    public userService: UserService,
    public externalAuthService: ExternalAuthService
  ) {
    super(tagTypeService, ViewModelType.ReadOnly, ngxSmartModalService, route, authService);
  }

  public init(param?) {
    this.selectedItems = [];
    this.modalType = param.modalType;
    this.editModeFromDetail = param.isFromDetail;
    this.userRolePermission = this.getUserClaims() || {};
    this.getValueMethodTypes();
    this.getContexts();
    this.getTagValueList(param.id);
    this.initTagTypeDetailsForm();
    this.getAllUsers();
    this.currentUserOrganizationId = this.authService.getOrganizationId();
    this.getADListByOrganization();
    if (this.modalType === "Create") {
      this.enableAll();
    } else if (this.modalType === "Edit") {
      this.tagTypeId = param.id;
      this.getTagTypeById(param.id);
      this.enableAll();
    }
    else {
      this.tagTypeId = param.id;
      this.getTagTypeById(param.id);
      this.disableAll();
    }

    return observableFrom([]);
  }

  private enableAll(): void {
    if (!this.tagTypeDetailsForm) {
      return;
    }
    for (const control in this.tagTypeDetailsForm.controls) {
      this.tagTypeDetailsForm.get(control).enable();
    }
  }

  public disableAll(): void {
    if (!this.tagTypeDetailsForm) {
      return;
    }
    for (const control in this.tagTypeDetailsForm.controls) {
      this.tagTypeDetailsForm.get(control).disable();
    }
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  public editFromDetail() {
    this.editModeFromDetail = true;
    this.tagTypeService.tagTypeIdSubject.next(this.tagTypeId);
    this.router
      .navigate(["/tag-type-management/" + this.tagTypeId + "/" + true + "/" + true])
      .then()
      .catch();
    this.getTagTypeById(this.tagTypeId);
    this.enableAll();
  }
  public detailFromEdit() {

    this.tagTypeService.tagTypeIdSubject.next(this.tagTypeId);
    this.router
      .navigate(["/tag-type-management/" + this.tagTypeId])
      .then()
      .catch();
  }

  public deleteTagtype(deleteTagtypeId) {
    return this.tagTypeService.deleteTagTypeByTagTypeId(deleteTagtypeId)
      .subscribe((data) => {
        if (data && data.isSuccess) {
          this.tagTypeService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
          this.navigateToList();
        } else {
          alert(data.errorList);
        }
      });
  }

  public navigateToList() {
    if (this.editModeFromDetail == "true" && this.modalType === 'Edit') {
      this.detailFromEdit();
    } else {
      this.tagTypeDetailsForm.reset();
      this.router
        .navigate(["/tag-type-management/list/"])
        .then()
        .catch();
    }
  }

  public cancel(): void {
    let tagValueChanged = false;
    _.each(this.tagValueList, tagValue => {
      if (tagValue.isDeleted) {
        tagValueChanged = true;
      }
    });
    if (this.tagTypeDetailsForm.dirty || tagValueChanged) {
      this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
      this.ngxSmartModalService.open(ModalName.cancelConfirmModal);
    } else {
      this.navigateToList();
    }
  }

  public enableSaveButton(): boolean {
    let tagValueChanged = false;
    _.each(this.tagValueList, tagValue => {
      if (tagValue.isDeleted) {
        tagValueChanged = true;
      }
    });
    return (this.tagTypeDetailsForm.dirty || tagValueChanged) ? (this.submitted || !this.tagTypeDetailsForm.valid) : true;
  }

  public confirmCancel(): void {
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
    this.router.navigate(["/tag-type-management/list"]);
  }

  public deletePopup() {
    this.ngxSmartModalService.getModal("tagTypeDeleteConfirmModal").open();
  }
  public onConfirm(deleteTagtypeId) {
    this.ngxSmartModalService.getModal("tagTypeDeleteConfirmModal").close();
    this.deleteTagtype(deleteTagtypeId);
  }

  public close() {
    this.ngxSmartModalService.getModal("tagTypeDeleteConfirmModal").close();
  }
  private checkTagValuesForCurated(): boolean {
    let result = true;
    if (this.arrValues && this.arrValues.length > 0) {
      _.each(this.arrValues, item => {
        if (!item.isDeleted)
          result = false;
      });
    }
    else {
      result = true;
    }
    return result;
  }


  private validateTagValues(): boolean {
    let result = true;
    _.each(this.arrValues, item => {
      if (!item.value) {
        result = false;
        this.submitted = false;
        this.alertService.info(ValidationMessages.tagValueShouldNotBeEmpty, true);
      }
    });
    const tagValueArray = _.pluck(this.arrValues, "value").map(value => value.toLowerCase());;
    if (this.hasDuplicates(tagValueArray)) {
      result = false;
      this.submitted = false;
      this.alertService.error(FormValidationMessages.Duplicate_Tag_Value_Error, true);
    }
    return result;
  }

  private hasDuplicates(arr: string[]): boolean {
    return new Set(arr).size !== arr.length;
  }

  public async saveChanges() {
    if (!this.validateFormFields()) {
      return;
    }
    if (this.tagTypeDetailsForm.invalid || !(this.selectedItems.length || this.currentTagTypeContextMappingIds.length)) {
      return;
    } else {
      this.submitted = true;
      this.saveTagType().catch();
    }
  }

  public async saveTagType() {
    const tagTypeDetails = this.transformToEntityModel();
    this.tagTypeService.saveTagType(tagTypeDetails)
      .subscribe(
        (data) => {
          this.submitted = false;
          this.tagTypeService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
        }, (err) => {
          this.submitted = false;
          this.alertService.error(err.error.errorList[0], true);
          this.tagTypeService.showAfterIntMessage({ text: err.error.errorList[0], type: "error", toStable: true });
        },
        () => {
          this.navigateToList();
        });
  }

  public transformToEntityModel() {
    const tagTypeDetails = this.tagTypeDetailsForm.value;
    tagTypeDetails.id = GuidHelper.isValid(tagTypeDetails.id) ? tagTypeDetails.id : undefined;
    tagTypeDetails.contexts = this.selectedItems.length ? this.selectedItems : this.currentTagTypeContextMappingIds;
    tagTypeDetails.tagValues = this.getTagValuesToSave();
    tagTypeDetails.isDeleted = false;
    tagTypeDetails.lastModifiedBy = this.authService.getUserId();
    return tagTypeDetails;
  }

  private getTagValuesToSave(): any {
    const updatedTagValues = [...this.arrValues];
    updatedTagValues.forEach(tagValue => {
      const deletedTagValueUserMapping = [];
      const deletedADMapping = [];
      const oldTagValue = this.pristineTagValueList.find(e => e.id === tagValue.id);
      if (oldTagValue) {
        oldTagValue.tagValueUserMapping.forEach(user => {
          const newUser = tagValue.tagValueUserMapping.find(u => u.userId === user.userId);
          if (!newUser) {
            user.isDeleted = true;
            deletedTagValueUserMapping.push(user)
          }
        });
        oldTagValue.tagValueAuthenticationProviderMapping.forEach(adGroup => {
          const newGroup = tagValue.tagValueAuthenticationProviderMapping.find(e => e.groupCode === adGroup.groupCode);
          if (!newGroup) {
            adGroup.isDeleted = true;
            deletedADMapping.push(adGroup)
          }
        });
      }
      tagValue.tagValueUserMapping = [...tagValue.tagValueUserMapping, ...deletedTagValueUserMapping];
      tagValue.tagValueAuthenticationProviderMapping = [...tagValue.tagValueAuthenticationProviderMapping, ...deletedADMapping];
    });
    return updatedTagValues;
  }


  public resetForm() {
    this.tagTypeDetailsForm.reset();
    this.tagTypeDetailsForm.markAsPristine();
    this.tagTypeDetailsForm.markAsUntouched();
  }

  public getTagValueList(tagTypeId) {
    if (GuidHelper.isValid(tagTypeId)) {
      this.tagTypeService.getTagValuesForTagType(tagTypeId).subscribe(
        data => {
          if (data) {
            data.forEach(element => {
              element.tagValueUserMapping.map(userItem => {
                userItem.name = `${userItem.user.firstName} ${userItem.user.lastName}`;
                delete (userItem.user);
              });
            })
            this.pristineTagValueList = [...data];
            this.tagValueList = data;
          }
        },
        error => {
          this.alertService.error(error);
        }
      );
    }
  }

  public errorFor(propertyName: string): string | void {
    if (propertyName) {
      const property = propertyName.replace(/\w+/g, (w) => w[0].toUpperCase() + w.slice(1).toLowerCase());
      const splitProperty = property.split('.');
      const fieldProperty = splitProperty.length >= 2 ? splitProperty[1] : splitProperty[0];

      const control = this.tagTypeDetailsForm.get(propertyName);

      if (control) {
        if (control.hasError("required")) {
          return fieldProperty.concat(ValidationMessages.RequiredMessage);
        }
        else if (control.hasError("minlength")) {
          const requiredLengthCount = control.errors.minlength.requiredLength;
          const errorMessage = FormValidationMessages.MinLengthError.replace("{requiredLengthCount}", requiredLengthCount);
          return fieldProperty.concat(errorMessage);
        }
        else if (control.hasError("maxlength")) {
          const requiredLengthCount = control.errors.maxlength.requiredLength;
          const errorMessage = FormValidationMessages.MaxLengthError.replace("{requiredLengthCount}", requiredLengthCount);
          return fieldProperty.concat(errorMessage);
        }
        else if (control.hasError("email") || control.hasError("pattern")) {
          return fieldProperty.concat(ValidationMessages.InvalidMessage);
        }
        else {
          return null;
        }
      }
    }
  }

  private initTagTypeDetailsForm(): void {
    this.tagTypeDetailsForm = this.fb.group({
      id: [""],
      name: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      description: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(250)]],
      valueMethodTypeId: ["", [Validators.required]],
      isMultipleSelection: [null, [Validators.required]]
    });
  }

  private async getContexts() {
    await this.tagTypeService.getContexts().subscribe(
      data => {
        if (data) {
          this.contexts = [];
          data.map((value) => {
            this.contexts.push({
              id: value.id,
              name: value.name,
              isSelected: true
            });
          });
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  private getValueMethodTypes() {
    this.tagTypeService.getValueMethodTypes().subscribe(
      data => {
        if (data) {
          this.valueMethodTypes = data;
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  private getTagTypeById(tagTypeId) {
    setTimeout(() => {
      this.tagTypeService.getTagTypeById(tagTypeId).subscribe(
        data => {
          if (data) {
            const valueMethod = _.filter(this.valueMethodTypes, item => item.id === data.valueMethodTypeId);
            this.contexts.forEach((item1) => {
              item1.isSelected = false;
              _.map(data.tagTypeContextMapping, item => {
                if (item1.id === item.contextId) {
                  item1.isSelected = true;
                  item1.TagTypeContextMappingId = item.id;
                  this.currentTagTypeContextMappingIds.push(item);
                }
              });
            });
            this.tagTypeDetailsForm.patchValue({
              id: data.id,
              name: data.name,
              description: data.description,
              valueMethodTypeId: valueMethod[0].id,
              isMultipleSelection: data.isMultipleSelection
            });

          }
        },
        error => {
          this.alertService.error(error);
        }
      )
    }, 1000);
  }

  public onTagValueChange(event, index) {
    this.tagValueList[index].value = event.target.value;
    this.submitted = false;
  }

  public validateFormFields(): boolean {
    const tagTypeDetails = this.transformToEntityModel();
    const valueMethod = _.filter(this.valueMethodTypes, item => item.id === tagTypeDetails.valueMethodTypeId);
    if (!tagTypeDetails.name) {
      this.alertService.info(FormValidationMessages.Name_Required, true);
      this.submitted = false;
      return false;
    }
    if (!tagTypeDetails.description) {
      this.alertService.info(FormValidationMessages.Description_Required, true);
      this.submitted = false;
      return false;
    }
    if (this.selectedItems.length == 0) {
      this.alertService.info(FormValidationMessages.Context_Required, true);
      this.submitted = false;
      return false;
    }
    if (valueMethod.length == 0) {
      this.alertService.info(FormValidationMessages.Value_Method_Required, true);
      this.submitted = false;
      return false;
    }
    if (tagTypeDetails.isMultipleSelection == null) {
      this.alertService.info(FormValidationMessages.Value_Method_Restriction, true);
      this.submitted = false;
      return false;
    }

    if (valueMethod[0].isCurated && this.checkTagValuesForCurated()) {
      this.alertService.error(ValidationMessages.addAtleastOneTagValue, true);
      this.submitted = false;
      return false;
    }

    const validateTagValueResult = this.validateTagValues();
    if (!validateTagValueResult) {
      return;
    }

    for (const k in this.tagTypeDetailsForm.value) {
      const error = this.errorFor(k);
      if (error) {
        this.alertService.info(error, true);
        this.submitted = false;
        return false;
      }
    }
    return true;
  }

  public async getADGroupsAndUsers(authProviderId: string): Promise<any> {
    await this.externalAuthService.GetADUsersAndGroups(authProviderId).subscribe(
      data => {
        if (data) {
          data.payload.groupedUsers.map((e: any) => {
            e.authenticationProviderId = authProviderId;
          });
          data.payload.unGroupedUsers.map((e: any) => {
            e.authenticationProviderId = authProviderId;
          });
          this.adGroupsAndUsers = data.payload.groupedUsers;
          this.adUnGroupedUsers = data.payload.unGroupedUsers;
        }
      },
      error => {
        this.alertService.error(error);
        this.adGroupsAndUsers = [];
        this.adUnGroupedUsers = [];
      });
  }

  private async getADListByOrganization(): Promise<any> {
    await this.externalAuthService.GetProviderListByOrganization(this.currentUserOrganizationId).subscribe(
      data => {
        if (data) {
          const providerList = [];
          if (data.length) {
            data.forEach(element => {
              const obj: any = {};
              obj.id = element.id;
              obj.name = element.name;
              providerList.push(obj);
            });
            this.providerDetails = providerList[0]; // only one AD provider per Organization
            this.getADGroupsAndUsers(this.providerDetails.id);
          }
        }
      },
      error => {
        this.alertService.error(error);
      });
  }

  private async getAllUsers(): Promise<void> {
    await this.userService.getAllUsers().subscribe(async data => {
      const allUsers = [];
      if (data.length) {
        data.forEach(element => {
          const obj: any = {};
          obj.userId = element.id;
          obj.name = `${element.firstName} ${element.lastName}`;
          obj.userName = element.userName;
          obj.email = element.email;
          allUsers.push(obj);
        });
      }
      this.allUsers = allUsers;
    }, err => {
      this.allUsers = []
    })
  }

  public onPopUpClose(): void {
    this.ngxSmartModalService.getModal(ModalName.cancelConfirmModal).close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }


}
