import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { AlertService } from "../../../../../../utils/alert/alert.service";
import { TagtypeListViewModel } from "../../../../viewmodels/tag-type-list.viewmodel";
import { TagtypeCreateEditViewModel } from "../../../../viewmodels/tag-type-create-edit.viewmodel";
import { TagtypeService } from "../../../../services/tag-type.service";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AuthService } from 'src/app/utils/auth.service';
import { GuidHelper } from '../../../../../employee-dashboard/helpers/guid-helper';
import { Subscription } from 'rxjs';
import _ from "underscore";

@Component({
  selector: 'app-tagtype-create-edit-detail-view',
  templateUrl: './tagtype-create-edit-detail-view.component.html',
  styleUrls: ['./tagtype-create-edit-detail-view.component.css'],
  providers: [TagtypeCreateEditViewModel, TagtypeListViewModel, AlertService]
})
export class TagtypeCreateEditDetailViewComponent implements OnInit, OnDestroy {

  public tagList: any;
  public projectId: any;
  public subscription: Subscription;
  public tagValueDetailsForm: FormGroup;
  public noTagValue: boolean;
  public currentIndex: number = undefined;
  public userListPristine = [];
  public isADControlEnabled = false;
  public dynamicUserList = [];
  public unGroupedUserList = [];
  public extraADUsers = {
    total: 0,
    userNames: ""
  };

  constructor(private router: Router,
    private tagTypeService: TagtypeService,
    public tagTypeCreateEditViewModel: TagtypeCreateEditViewModel,
    public tagTypeListViewModel: TagtypeListViewModel,
    public alertService: AlertService,
    public authService: AuthService,
    private route: ActivatedRoute,
    public ngxSmartModalService: NgxSmartModalService,
    private fb: FormBuilder) {
    this.alertService.afterInitObs = this.tagTypeService.getAfterIntObs();
    // this.userService = new 
  }

  async ngOnInit() {
    this.route.params
      .subscribe(async params => {
        if (params.tagTypeId) {
          this.projectId = params.tagTypeId;
          this.tagTypeCreateEditViewModel.modalType = (params.isEditView) ? "Edit" : (GuidHelper.isValid(params.tagTypeId)) ? "Detail" : "Create";
          const tagTypeDetailParams = { id: params.tagTypeId, modalType: (params.isEditView) ? "Edit" : (GuidHelper.isValid(params.tagTypeId)) ? "Detail" : "Create", isFromDetail: params.isFromDetail };
          this.tagTypeCreateEditViewModel.init(tagTypeDetailParams);
        } else {
          this.tagTypeCreateEditViewModel.modalType = "Create";
          const tagTypeDetailParams = { id: "", modalType: "Create" };
          this.tagTypeCreateEditViewModel.init(tagTypeDetailParams);
        }
        if (params && params.IsDelete) {
          this.tagTypeCreateEditViewModel.deleteTagtypeId = params.linkId;
          this.tagTypeCreateEditViewModel.modalType = "Delete";
          this.tagTypeCreateEditViewModel.IsDeleteFromList = params.IsDelete === "IsDeleteFromList" ? true : false;
          const tagTypeDetailParams = { id: params.linkId, modalType: "Delete" };
          this.tagTypeCreateEditViewModel.init(tagTypeDetailParams);
        }
      });
    this.subscription = this.tagTypeCreateEditViewModel.subscription;
    this.tagTypeCreateEditViewModel.initServices(this.alertService);
    this.initTagValueDetailsForm();
  }

  public initTagValueDetailsForm() {
    this.tagValueDetailsForm = this.fb.group({
      id: [""],
      value: ["", [Validators.required]],
      tagValueAuthenticationProviderMapping: [],
      tagValueUserMapping: [[], [Validators.required]]
    });
  }

  public get hasSubmitted(): boolean {
    return this.tagTypeCreateEditViewModel.submitted;
  }

  ngOnDestroy() { }

  addRow() {
    const tagValueDetails = { value: "", tagValueAuthenticationProviderMapping: [], tagValueUserMapping: [] };
    if (this.tagTypeCreateEditViewModel.providerDetails) {
      this.isADControlEnabled = true;
      this.initiateUserList(this.tagTypeCreateEditViewModel.adGroupsAndUsers, this.tagTypeCreateEditViewModel.adUnGroupedUsers);
    } else {
      this.isADControlEnabled = false;
    }
    this.tagTypeCreateEditViewModel.tagValueList.push(tagValueDetails);
    this.tagValueDetailsForm.patchValue({ ...tagValueDetails });
    this.noTagValue = false;
    this.tagTypeCreateEditViewModel.tagTypeDetailsForm.markAsDirty();
    return true;
  }

  public onADGroupChange(event: any): void {
    if (!event.length && this.tagValueDetailsForm.value.tagValueUserMapping.length) {
      this.populateUserList([], this.tagTypeCreateEditViewModel.adUnGroupedUsers);
    } else if (event.length) {
      this.populateUserList(event);
    } else {
      this.populateUserList(this.tagTypeCreateEditViewModel.adGroupsAndUsers, this.tagTypeCreateEditViewModel.adUnGroupedUsers);
    }
  }

  public populateUserList(groups: any[], unGroupedUsers?: any[]): void {
    const commonUsers: ICommonUsers = this.getCommonUsers(groups, unGroupedUsers);
    this.dynamicUserList = _.uniq(commonUsers.dynamicUserList, "userId");

    const formUserListOld = this.tagValueDetailsForm.value.tagValueUserMapping;
    const formUserList = [];
    formUserListOld.forEach((user: any) => {
      if (!user.groupCode) {
        formUserList.push(user);
      } else {
        const matchedUser = groups.find(e => e.groupCode === user.groupCode);
        if (matchedUser) {
          formUserList.push(user);
        }
      }
    });

    this.tagValueDetailsForm.patchValue({
      tagValueUserMapping: formUserList
    });
  }

  public initiateUserList(groups: any[], unGroupedUsers: any[]): void {
    const commonUsers: ICommonUsers = this.getCommonUsers(groups, unGroupedUsers);
    this.dynamicUserList = _.uniq(commonUsers.dynamicUserList, "userId");
    this.unGroupedUserList = _.uniq(commonUsers.extraADUserList, "emailAddress");
    let uniqueExtraADUserList = _.pluck(this.unGroupedUserList, "displayName");
    uniqueExtraADUserList = uniqueExtraADUserList.filter(Boolean);
    this.extraADUsers = {
      total: uniqueExtraADUserList.length,
      userNames: uniqueExtraADUserList.length ? uniqueExtraADUserList.join(", ") : ""
    }
  }

  public getCommonUsers(groups: any[], unGroupedUsers?: any[]): ICommonUsers {
    let commonUsers: ICommonUsers = { dynamicUserList: [], extraADUserList: [] }
    if (groups && groups.length) {
      groups.forEach(element => {
        for (let user of element.groupUsers) {
          const userObj = _.find(this.tagTypeCreateEditViewModel.allUsers, e => {
            if (e.email && user.emailAddress) {
              return e.email.toLowerCase() === user.emailAddress.toLowerCase()
            }
          });
          if (userObj) {
            userObj.groupCode = element.groupCode;
            commonUsers.dynamicUserList.push(userObj);
          } else {
            commonUsers.extraADUserList.push(user);
          }
        }
      });
    }
    if (unGroupedUsers && unGroupedUsers.length) {
      unGroupedUsers.forEach(user => {
        const userObj = _.find(this.tagTypeCreateEditViewModel.allUsers, e => {
          if (e.email && user.emailAddress) {
            return e.email.toLowerCase() === user.emailAddress.toLowerCase()
          }
        });
        if (userObj) {
          commonUsers.dynamicUserList.push(userObj);
        } else {
          commonUsers.extraADUserList.push(user);
        }
      })
    }
    return commonUsers;
  }

  deleteRow(index) {
    if (!this.tagTypeCreateEditViewModel.tagValueList[index].id)
      this.tagTypeCreateEditViewModel.tagValueList.splice(index, 1);
    else {
      this.tagTypeCreateEditViewModel.tagValueList[index].isDeleted = true;
    }
    this.noTagValue = true;
    this.tagTypeCreateEditViewModel.tagValueList.forEach((data) => {
      if (!data.isDeleted)
        this.noTagValue = false;
    });
    return true;
  }

  saveTagType() {
    this.tagTypeCreateEditViewModel.arrValues = this.tagTypeCreateEditViewModel.tagValueList;
    this.tagTypeCreateEditViewModel.selectedItems = this.tagTypeCreateEditViewModel.contexts.filter(item => item.isSelected);
    this.tagTypeCreateEditViewModel.saveChanges();
  }

  public openTaggingDetailsPopUp(rowData: any, index: number): void {
    if (this.tagTypeCreateEditViewModel.providerDetails) {
      this.isADControlEnabled = true;
      this.initiateUserList(this.tagTypeCreateEditViewModel.adGroupsAndUsers, this.tagTypeCreateEditViewModel.adUnGroupedUsers);
    }
    this.currentIndex = index;
    this.userListPristine = rowData.tagValueUserMapping;
    this.tagValueDetailsForm.patchValue({
      id: rowData.id,
      value: rowData.value,
      tagValueUserMapping: rowData.tagValueUserMapping,
      tagValueAuthenticationProviderMapping: rowData.tagValueAuthenticationProviderMapping
    });
    this.ngxSmartModalService.getModal("taggingDetailsPopUp").open();
  }

  public saveTagDetails(): void {
    if (this.userListPristine.length && this.tagValueDetailsForm.value.tagValueUserMapping) {
      const updatedUserList = [];
      this.tagValueDetailsForm.value.tagValueUserMapping.forEach(element => {
        const detailsObj = this.userListPristine.find(e => e.userId === element.userId);
        if (detailsObj) {
          updatedUserList.push({ ...detailsObj });
        } else {
          updatedUserList.push(element);
        }
      });
      this.tagValueDetailsForm.patchValue({ tagValueUserMapping: updatedUserList });
    }
    this.tagTypeCreateEditViewModel.tagValueList[this.currentIndex] = this.tagValueDetailsForm.value;
    this.taggingDetailsPopUpClose();
  }

  public taggingDetailsPopUpClose(): void {
    this.tagValueDetailsForm.reset();
    this.extraADUsers = { total: 0, userNames: "" };
    this.ngxSmartModalService.getModal("taggingDetailsPopUp").close();
  }

  public omitSpecialChar(event) {
    let k = event.charCode;  //         k = event.keyCode;  (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
  }

}

export interface ICommonUsers {
  dynamicUserList: any[];
  extraADUserList: any[];
}