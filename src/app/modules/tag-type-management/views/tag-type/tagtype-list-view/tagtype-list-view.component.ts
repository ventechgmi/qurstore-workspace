import { Component, OnInit } from '@angular/core';
import { AlertService } from "../../../../../utils/alert/alert.service";
import { TagtypeListViewModel } from "../../../viewmodels/tag-type-list.viewmodel";
import {TagtypeService} from "../../../services/tag-type.service";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AuthService } from 'src/app/utils/auth.service';

@Component({
  selector: 'app-tagtype-list-view',
  templateUrl: './tagtype-list-view.component.html',
  styleUrls: ['./tagtype-list-view.component.css'],
  providers: [AlertService, TagtypeListViewModel]
})
export class TagtypeListViewComponent implements OnInit {

  public tagList: any;
  public dataTable: any;
  public tagCreateModalRef: any;
  projectId: any;
  constructor(private router: Router,
              private tagTypeService: TagtypeService,
              public tagTypeListViewModel: TagtypeListViewModel,
              public alertService: AlertService,
              public authService: AuthService,
              private route: ActivatedRoute,
              public ngxSmartModalService: NgxSmartModalService) {
                this.alertService.afterInitObs = this.tagTypeService.getAfterIntObs();
               }

  public ngOnInit() {
    this.route.params
      .subscribe(params => {
        const userId = this.authService.getUserId();
        this.tagTypeListViewModel.initServices(this.alertService);
        this.tagTypeListViewModel.init({ userId });
        this.tagTypeListViewModel.initServices(this.alertService);
      });

    this.route.queryParams.subscribe(res => {
      this.projectId = res.id;
    });
  }

  public ngOnDestroy() {
    this.tagTypeService.showAfterIntMessage({ text:"", type: "success", toStable: true});
  }
}

