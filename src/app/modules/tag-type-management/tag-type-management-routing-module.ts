import { Routes, RouterModule } from "@angular/router";
import { TagtypeListViewComponent } from "./views/tag-type/tagtype-list-view/tagtype-list-view.component";
import { NgModule } from "@angular/core";
import { TagtypeCreateEditDetailViewComponent } from './views/tag-type/tagtype-create-edit-detail-view/tagtype-create-edit-detail-view/tagtype-create-edit-detail-view.component';

export const routes: Routes = [
  {
    path: "tag-type-management/delete/:linkId/:IsDelete",
    component: TagtypeCreateEditDetailViewComponent
  },
  {
    path: "tag-type-management/list",
    component: TagtypeListViewComponent
  },
  {
    path: "tag-type-management/create",
    component: TagtypeCreateEditDetailViewComponent
  },
  {
    path: "tag-type-management/:tagTypeId",
    component: TagtypeCreateEditDetailViewComponent
  },
  {
    path: "tag-type-management/:tagTypeId/:isEditView/:isFromDetail",
    component: TagtypeCreateEditDetailViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TagtypeManagementRoutingModule { }
