
import { map } from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";

const appConfigLocal = require("../../../app-config.json");
@Injectable({
    providedIn: "root"
})
export class TagtypeService extends EntityService {
    appConfig: AppConfig;
    onUpdateListEvent = new EventEmitter();
    public tagTypeIdSubject = new ReplaySubject<any>();
    public showSuccessMessage = new Subject<any>();
    public alertInfo = { message: "", type: "" };
    private afterInitSubject = new BehaviorSubject(this.alertInfo);

    constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
        super(http, "tag-type", appConfig);
        this.appConfig = appConfig;

    }
    private getAPIURLUserManagement() {
        return this.getRawApi();
    }
    private getRawApi() {
        return this.getApi();
    }


    private getAPIURL() {
        return this.getApi();
    }

    public showAfterIntMessage(messageInfo) {
        this.afterInitSubject.next(messageInfo);
    }

    public getAfterIntObs(): Observable<any> {
        return this.afterInitSubject.asObservable();
    }

    public getShowSuccessMessage() {
        return this.showSuccessMessage.asObservable();
    }

    // public getUpdateRoleListEvent() {
    //     return this.onUpdateListEvent;
    // }

    private generateGetAPI(name, req): Observable<any> {
        return this.http
            .get(this.getAPIURL() + name + req).pipe(
                map(res => {
                    return res;
                }));
    }
    private generatePostAPI(name, data): Observable<any> {
        return this.http
            .post(this.getApi() + name, data).pipe(
                map((res: Response) => res));
    }

    private generateDeleteAPI(name, data): Observable<any> {
        return this.http
            .delete(this.getApi() + name + data).pipe(
                map(res => {
                    return res;
                }));
    }
    public getAllTagTypes(): Observable<any> {
        return this.generateGetAPI("/list", "");
    }

    public getAllTagValues(): Observable<any> {
        return this.generateGetAPI("/list/values", "");
    }

    public getTagValuesForTagType(tagTypeId): Observable<any> {
        return this.generateGetAPI("/tag-values/", tagTypeId);
    }

    public getTagTypeIdSubject() {
        return this.tagTypeIdSubject.asObservable();
    }

    public getContexts(): Observable<any> {
        return this.generateGetAPI("/contexts", "");
    }
    public getValueMethodTypes(): Observable<any> {
        return this.generateGetAPI("/value-method-types", "");
    }

    public getTagTypeById(req): Observable<any> {
        return this.generateGetAPI("/", req);
    }

    public deleteTagTypeByTagTypeId(tagTypeId): Observable<any> {
        return this.generateDeleteAPI("/delete/", tagTypeId);
    }
     public saveTagType(tagTypeDetails): Observable<any> {
        return this.generatePostAPI("/save/", tagTypeDetails);
    }
}

