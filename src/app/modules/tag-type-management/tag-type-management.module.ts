import { NgModule } from "@angular/core";
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { TagtypeListViewComponent } from "./views/tag-type/tagtype-list-view/tagtype-list-view.component";
import { DataGridModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import { TagtypeManagementRoutingModule } from "./tag-type-management-routing-module";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatButtonModule } from "@angular/material/button";
import { MatRadioModule } from '@angular/material/radio';
import { AlertService } from "src/app/utils/alert/alert.service";
import { TagtypeService } from './services/tag-type.service';
import { TagtypeCreateEditDetailViewComponent } from './views/tag-type/tagtype-create-edit-detail-view/tagtype-create-edit-detail-view/tagtype-create-edit-detail-view.component';
import { SharedModules } from 'src/app/utils/shared-module/shared.modules';

@NgModule(
    {
        declarations: [
            TagtypeListViewComponent,
            TagtypeCreateEditDetailViewComponent
        ],
        imports: [
            SharedModules,
            NgSelectModule,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            TagtypeManagementRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatButtonModule,
            MatRadioModule,
            DataGridModule,
            MatCheckboxModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [
            TagtypeListViewComponent,
            TagtypeCreateEditDetailViewComponent
        ],
        providers: [
            AlertService,
            TagtypeService,
            NgxSmartModalService
        ]
    }
)
export class TagtypeManagementModule {

}
