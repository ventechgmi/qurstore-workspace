export class ValidationMessages {
    public static readonly RequiredMessage = " is required.";
    public static readonly MinMaxLengthMessage = " must be between 3 and 20 characters..";
    public static readonly InvalidMessage = " is invalid.";

    public static readonly emailalreadyExists = "Email address is already in use. Please try with another one.";
    public static readonly saveSuccessMessage = "Tag type saved successfully.";
    public static readonly createSuccessMessage = "Tag type created successfully.";
    public static readonly updateSuccessMessage = "Tag type updated successfully.";
    public static readonly deleteSuccessMessage = "Tag type deleted successfully.";
    public static readonly somethingWentWrong = "Something went wrong. Please try again.";
    public static readonly enterTagTypenameError = "Please enter Tag type name";
    public static readonly addAtleastOneTagValue = "Add atleast one Tag value";
    public static readonly tagValueShouldNotBeEmpty = "Tag value should not be empty";


    // regex patter
    public static readonly EmailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    public static readonly PhoneNumberPattern = "^(\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$";
    public static readonly zipCodePattern = "^[0-9]{5}(?:-[0-9]{4})?$";
}

export class FormValidationMessages {
    public static readonly Name_Required = "Name is required";
    public static readonly Description_Required = "Description is required";
    public static readonly Context_Required = "Select at least one context";
    public static readonly Value_Method_Required = "Select at least one value method";
    public static readonly Value_Method_Restriction = "Select at least one value restriction";
    public static readonly MinLengthError = " must be at least {requiredLengthCount} characters";
    public static readonly MaxLengthError = " must be less than {requiredLengthCount} characters";
    public static readonly Duplicate_Tag_Value_Error = "No duplicates allowed in Tag Value(s)";
}
