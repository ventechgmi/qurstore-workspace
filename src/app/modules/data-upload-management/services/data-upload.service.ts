import { map } from 'rxjs/operators';
import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, BehaviorSubject } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class DataUploadService extends EntityService {
    public showSuccessMessage = new Subject<any>();
    public deleteSuccessMessage = new Subject<any>();
    public alertInfo = { message: "", type: "" };
    public appConfig: AppConfig;

    // this subscription will help to show error or success message in another screen(details screen delete success message showing in list screen)
    private afterInitSubject = new BehaviorSubject(this.alertInfo);

    constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
        super(http, "data-upload", appConfig);
        this.appConfig = appConfig;
    }

    private getAPIURL() {
        return this.getApi();
    }

    public showAfterIntMessage(messageInfo) {
        this.afterInitSubject.next(messageInfo);
    }

    public getAfterIntObs(): Observable<any> {
        return this.afterInitSubject.asObservable();
    }

    public getShowSuccessMessage() {
        return this.showSuccessMessage.asObservable();
    }

    private generateGetAPI(name, req): Observable<any> {
        return this.http.get(this.getAPIURL() + name + req).pipe(
            map(res => {
                return res;
            }));
    }

    private generatePostAPI(name, data): Observable<any> {
        return this.http.post(this.getAPIURL() + name, data).pipe(
            map((res: Response) => res));
    }

    public getDataUploadList(): Observable<any> {
        return this.http.get(this.getDashboardAPIURL() + "/GetUploadedDataFilesByUser").pipe(
            map(res => {
                return res;
            }));
    }

    public uploadDataFile(tdHeaderInformation: any, fileToUpload: Blob): Observable<any> {
        const formData: FormData = new FormData();
        const fileInformationJsonObject: string = JSON.stringify(tdHeaderInformation);
        formData.set('fileInformationJsonObject', fileInformationJsonObject);
        formData.set('formFile', fileToUpload);
        const endpoint = this.getFileServiceAPIURL() + "/saveUploadedFile";
        return this.http
            .post(endpoint, formData).pipe(
                map((res: Response) => res));
    }

    public deleteUploadedDataFile(uploadedFileId: string): Observable<any> {
        return this.http.post(this.getDashboardAPIURL() + "/deleteUploadedFileById/" + uploadedFileId, '').pipe(
            map(res => {
                return res;
            }));
    }

    public downloadFile(uploadedFileId: string, fileType: string): Observable<any> {
        const formData: FormData = new FormData();
        const endpoint = this.getFileServiceAPIURL() + "/downloadFile";
        formData.set('uploadedFileId', uploadedFileId);
        formData.set('fileType', fileType);
        return this.http.post<Blob>(endpoint, formData, { responseType: 'blob' as 'json' });
    }

    private getFileServiceAPIURL(): string {
        const fileServiceAPI = this.appConfig.apiEndPoint + "/FileService";
        return fileServiceAPI;
    }

    private getDashboardAPIURL(): string {
        const dashboardServiceAPI = this.appConfig.apiEndPoint + "/dashboard";
        return dashboardServiceAPI;
    }
}