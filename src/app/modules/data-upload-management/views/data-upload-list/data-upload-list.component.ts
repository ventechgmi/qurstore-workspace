import {
    Component,
    OnInit,
    AfterContentInit
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from "../../../../utils/alert/alert.service";
import { AuthService } from 'src/app/utils/auth.service';
import { DataUploadListViewModel } from '../../viewmodels/data-upload-list.viewmodel';
import { DataUploadService } from '../../services/data-upload.service';

@Component({
    selector: "app-data-upload-list",
    templateUrl: "./data-upload-list.component.html",
    styleUrls: ["./data-upload-list.component.scss"],
    providers: [DataUploadListViewModel, AlertService]
})
export class DataFileUploadListViewComponent implements OnInit, AfterContentInit {
    constructor(
        public dataUploadService: DataUploadService,
        public dataUploadListViewModel: DataUploadListViewModel,
        public alertService: AlertService,        
        public ngxSmartModalService: NgxSmartModalService,
        public authService: AuthService,
        private route: ActivatedRoute
    ) {
        // need to set the afterInitObs to show other screen success or error message in different screen
        alertService.afterInitObs = dataUploadService.getAfterIntObs();
    }

    public ngOnInit() {
        this.route.params
            .subscribe(params => {
                const userId = this.authService.getUserId();
                this.dataUploadListViewModel.initServices(this.alertService);
                this.dataUploadListViewModel.init({ userId });
            });
    }

    public ngAfterContentInit() { }
}
