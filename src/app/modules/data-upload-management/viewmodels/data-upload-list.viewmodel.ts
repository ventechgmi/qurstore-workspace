import { from as observableFrom } from "rxjs";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import { Router, ActivatedRoute } from "@angular/router";
import * as _ from "underscore";
import { DataGridExtendedViewModel } from "../../core-ui-components";
import config, { GridConfig, DATA_FILE_MAX_FILE_SIZE } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from "../../../utils/alert/alert.service";
import { DataUploadService } from "../services/data-upload.service";
import { DataUploadViewModel } from "./data-upload.viewmodel";
import { AuthService } from '../../../utils/auth.service';
import { ValidationMessages } from '../helpers/validation-messages';
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import moment from 'moment';

@Injectable()
export class DataUploadListViewModel extends DataUploadViewModel {
  public userId: string;
  public alertService: AlertService;
  public viewModelDataGrid: DataGridExtendedViewModel;
  public userRolePermission: any = {};
  public modalTitle: string;
  public isShowMessage: boolean = false;
  public messageData: any = {
    type: "",
    message: ""
  };
  public modalType: string = "Create";
  public dataUploadModelForm: FormGroup;
  public dataUploadFile: any;
  public dataUploadedListData: any[];
  public acceptedDataUploadFiles: string[] = ["xls", "xlsx", "csv", "txt", "zip"];
  public selectedUploadedFileId: any;
  public dataFileMaximumFileSize: number = DATA_FILE_MAX_FILE_SIZE;
  public dataFileMaximumFileSizeInMB: number = (DATA_FILE_MAX_FILE_SIZE / 1024);
  public userOrganizationId: string = "";
  private uploadDataFileModel: string = "uploadDataFileModel";
  private discardDataFileModel: string = "discardDataFileModel";
  private deleteUploadedDataFileModel: string = "deleteUploadedDataFileModel";
  private aWSStagingLandingZoneBucketName: string = "AWSStagingLandingZoneBucketName";
  private aWSDataFileFolder: string = "AWSDataFilesFolder";
  private dataFileDataType: string = "DATA";
  private isDownloadInProgress: boolean = false;

  public constructor(
    public dataUploadService: DataUploadService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    private fb: FormBuilder,
    private approveRejectFb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public authService: AuthService
  ) {
    super(dataUploadService, ViewModelType.Create, ngxSmartModalService, route, authService);
  }

  public init(param?) {
    this.userId = param.userId;
    this.userOrganizationId = this.authService.getOrganizationId();
    this.viewModelDataGrid = new DataGridExtendedViewModel();
    this.userRolePermission = this.getUserClaims() || {};
    this.initializeGridColDef();
    this.subscribeToCreateSuccessMessage();
    this.subscribers();
    this.setDataTableConfigurations();
    this.getDataUploadList();
    this.initDataUploadForm();
    return observableFrom([]);
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  public initDataUploadForm(): void {
    this.dataUploadModelForm = this.fb.group({
      dataFile: ["", [Validators.required]]
    });
  }

  public initializeGridColDef() {
    const fileName = {
      headerName: 'File Name',
      field: 'uploadedFileName',
      editable: false,
      sortable: true,
      cellRenderer: (params: any) => {
        if (params.value) {
          if (params.data.isEnabled && !params.data.isDeleted) {
            return `<u class="hyperlink" style ="color: blue; cursor: pointer;"  data-action-type="downloadCleanFileClicked">` + params.value + `</u>`;
          } else {
            return params.value;
          }
        }
      }
    };
    this.viewModelDataGrid.columnDefs.push(fileName);
    const fileStatus = {
      headerName: 'Status',
      sortable: true,
      field: 'uploadedFileStatus',
      editable: false
    };
    this.viewModelDataGrid.columnDefs.push(fileStatus);
    const uploadedOn = {
      headerName: 'Uploaded On',
      field: 'uploadedOn',
      sortable: true,
      editable: false,
      isDate: true,
      sort: 'desc',
      comparator: this.viewModelDataGrid.customDateSort
    };
    this.viewModelDataGrid.columnDefs.push(uploadedOn);
    const action = {
      headerName: 'Action',
      field: 'action',
      editable: false,
      cellRenderer: (params: any) => {
        return `<a routerLink="" class="data-grid-padding-right"><img alt="Delete"  title="Delete"  src="../../../assets/images/trash.png" class="data-grid-image-small" data-action-type="deleteClicked"/></a>`;
      }
    };
   // this.viewModelDataGrid.columnDefs.push(action); // Commented by Ananth - Dec-29-2020 : We do not need delete action right now - hence commented.
  }

  // data-table configurations for project management
  public setDataTableConfigurations(): void {
    this.viewModelDataGrid.dataTableConfiguration = {
      destroy: true,
      sortable: true,
      columnDefs: [{ targets: [0], visible: false, searchable: false }, { type: "num-html", targets: [2] }, { orderable: true, targets: 2, searchable: false }],
      order: [[3, "asc"]]
    };
  }


  public getDataUploadList(): void {
    this.dataUploadService.getDataUploadList().subscribe(
      data => {
        if (data) {
          this.dataUploadedListData = [];
          _.each(data, (uploadedFileDetail) => {
            let dataUploadObj: any = {};
            dataUploadObj.rowDetails = uploadedFileDetail;
            dataUploadObj.uploadedFileName = uploadedFileDetail.originalFileName;
            dataUploadObj.isEnabled = uploadedFileDetail.isEnabled;
            dataUploadObj.isDeleted = uploadedFileDetail.isDeleted;
            dataUploadObj.uploadedFileStatus = uploadedFileDetail.fileStatus;
            dataUploadObj.uploadedOn = moment(uploadedFileDetail.lastModifiedDate).format(config.APPLICATION_DATE_TIME_FORMAT);

            this.dataUploadedListData.push(dataUploadObj);
          });
          this.viewModelDataGrid.setRowsData(this.dataUploadedListData);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public subscribeToCreateSuccessMessage(): void {
    this.dataUploadService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      this.alertService.success(message, toStable);
    });
  }

  public uploadDataFile(): void {
    this.isShowMessage = false;
    if (!this.dataUploadFile) {
      this.updateMessageData("error", ValidationMessages.fileRequiredError, true);
      return;
    }
    this.updateMessageData("info", ValidationMessages.fileUploadProgress, false);
    const tdHeaderInformation = this.transformToDataModel();
    const formFile = this.dataUploadFile;
    this.dataUploadService.uploadDataFile(tdHeaderInformation, formFile).subscribe(data => {
      if (data && data.isSuccess) {
        this.dataUploadModelForm.reset();
        this.clearDataFileUploadControl();
        this.getDataUploadList();
        this.ngxSmartModalService.getModal(this.uploadDataFileModel).close();
        this.dataUploadService.showAfterIntMessage({ text: ValidationMessages.fileUploadSuccess, type: "success", toStable: true });
      } else {
        this.updateMessageData("error", ValidationMessages.fileUploadError, true);
      }
    }, (error) => {
      if (error.error && error.error.error) {
        this.updateMessageData("error", error.error.error, true);
      } else {
        this.updateMessageData("error", ValidationMessages.fileUploadError, true);
      }
    });
  }

  public deleteUploadedFile(): void {
    this.dataUploadService.deleteUploadedDataFile(this.selectedUploadedFileId).subscribe(data => {
      if (data && data.isSuccess) {
        this.selectedUploadedFileId = "";
        this.getDataUploadList();
        this.ngxSmartModalService.getModal(this.deleteUploadedDataFileModel).close();
        this.dataUploadService.showAfterIntMessage({ text: ValidationMessages.fileDeletedSuccess, type: "success", toStable: true });
      }
    }, (error) => {
      this.selectedUploadedFileId = "";
      this.alertService.success(error, true);
    });
  }

  public openDataFileUploadModel(): void {
    this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
    this.ngxSmartModalService.getModal(this.uploadDataFileModel).open();
  }

  public closeDataFileUploadModel(): void {
    if (this.dataUploadModelForm.dirty || this.dataUploadFile) {
      this.ngxSmartModalService.getModal(this.discardDataFileModel).open();
    } else {
      this.dataUploadModelForm.reset();
      this.clearDataFileUploadControl();
      this.ngxSmartModalService.getModal(this.uploadDataFileModel).close();
    }
  }

  public discardConfirmed(): void {
    this.dataUploadModelForm.reset();
    this.clearDataFileUploadControl();
    this.ngxSmartModalService.getModal(this.discardDataFileModel).close();
    this.ngxSmartModalService.getModal(this.uploadDataFileModel).close();
  }

  public closeDiscardConfirmation(): void {
    this.ngxSmartModalService.getModal(this.discardDataFileModel).close();
  }

  public closeDeleteDiscardConfirmation(): void {
    this.selectedUploadedFileId = "";
    this.ngxSmartModalService.getModal(this.deleteUploadedDataFileModel).close();
  }

  public fileChanged(event: EventTarget): void {
    this.isShowMessage = false;
    const eventObj: MSInputMethodContext = <MSInputMethodContext>event;
    const target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    const files: FileList = target.files;
    const fileData = files[0];
    const fileSize = Math.round((fileData.size / 1024));
    if (fileSize > this.dataFileMaximumFileSize) {
      this.updateMessageData("error", ValidationMessages.fileSizeError, true);
      (<HTMLInputElement>window.document.getElementById('dataFile')).value = "";
      return;
    }
    const fileType = fileData.name.split('.').pop();
    if (!this.acceptedDataUploadFiles.includes(fileType)) {
      const constructedMsg = (ValidationMessages.fileTypeInvalidError + this.acceptedDataUploadFiles);
      this.updateMessageData("error", constructedMsg, false);
      (<HTMLInputElement>window.document.getElementById('dataFile')).value = "";
      return;
    }
    this.dataUploadFile = fileData;
  }

  public transformToDataModel(): any {
    const data = {
      id: undefined,
      file_name: this.dataUploadFile.name,
      fileUploadId: undefined,
      file_type: this.dataUploadFile.type,
      lastModifiedBy: this.authService.getUserId(),
      isDataFileUpload: true,
      bucketNameKey: this.aWSStagingLandingZoneBucketName,
      uploadedFileDataType: this.dataFileDataType,
      folderPathKey: this.aWSDataFileFolder
    };
    return data;
  }

  private updateMessageData(type: string, message: string, toStable: boolean): void {
    this.isShowMessage = true;
    this.messageData = {
      type: type,
      message: message
    };
    if (toStable) {
      setTimeout(() => {
        this.isShowMessage = false;
      }, 10000);
    }
  }

  private clearDataFileUploadControl(): void {
    this.isShowMessage = false;
    this.dataUploadFile = "";
    (<HTMLInputElement>window.document.getElementById('dataFile')).value = "";
  }

  private subscribers(): void {
    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.columnClicked:
          break;
        case GridConfig.downloadCleanFileClicked:
          if (this.isDownloadInProgress) {
            this.alertService.error(ValidationMessages.fileDownloadProgress, true);
            return;
          }
          this.alertService.info(ValidationMessages.fileDownloadProgress, true);
          const selectedUploadedFileId = e.eventData.rowDetails.rowDetails.id;
          const fileName = e.eventData.rowDetails.rowDetails.originalFileName;
          const fileType = fileName.split('.').pop();
          this.downloadFile(selectedUploadedFileId, fileType, fileName);
          break;
        case GridConfig.deleteClicked:
          this.selectedUploadedFileId = e.eventData.rowDetails.rowDetails.id;
          this.ngxSmartModalService.getModal(this.deleteUploadedDataFileModel).open();
          break;
        default:
      }
    }, error => {
      this.alertService.error(error);
    });
  }

  private downloadFile(uploadedFileId: string, fileType: string, fileName: string): void {
    this.isDownloadInProgress = true;
    this.dataUploadService.downloadFile(uploadedFileId, fileType)
      .subscribe(
        blob => {
          const a = window.document.createElement("a");
          a.href = window.URL.createObjectURL(blob);
          a.download = fileName;
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
          this.isDownloadInProgress = false;
        },
        error => {
          this.alertService.error(error);
        });
  }

  public onPopUpClose(): void {
    this.ngxSmartModalService.getModal("uploadDataFileModel").close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }

}
