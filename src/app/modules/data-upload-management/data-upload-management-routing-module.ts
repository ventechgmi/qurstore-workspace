import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { DataFileUploadListViewComponent } from './views/data-upload-list/data-upload-list.component';

export const routes: Routes = [
  {
    path: "list",
    component: DataFileUploadListViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataFileUploadManagementRoutingModule { }
