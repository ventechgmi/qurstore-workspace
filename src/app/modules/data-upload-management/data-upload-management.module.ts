import { NgModule } from "@angular/core";
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { DataGridModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import { MatButtonModule } from "@angular/material/button";
import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";
import { DataFileUploadManagementRoutingModule } from './data-upload-management-routing-module';
import { DataFileUploadListViewComponent } from './views/data-upload-list/data-upload-list.component';
import { DataUploadService } from './services/data-upload.service';

@NgModule(
    {
        declarations: [
            DataFileUploadListViewComponent,
        ],
        imports: [
            SharedModules,
            NgSelectModule,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            DataFileUploadManagementRoutingModule,           
            MatButtonModule,
            DataGridModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [
            DataFileUploadListViewComponent
        ],
        providers: [
            AlertService,
            DataUploadService,
            NgxSmartModalService
        ]
    }
)
export class DataFileUploadManagementModule {

}
