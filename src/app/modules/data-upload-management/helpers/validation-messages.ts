export class ValidationMessages {
    public static readonly fileSizeError = "File size should not exceed 12MB";
    public static readonly fileUploadProgress = "File upload is in progress. Please wait...";
    public static readonly fileDownloadProgress = "File download is in progress. Please wait...";
    public static readonly fileUploadSuccess = "File has been uploaded successfully";
    public static readonly fileDeletedSuccess = "File has been deleted successfully";
    public static readonly fileUploadError = "Error with uploading the file";
    public static readonly fileRequiredError = "Data File is required";
    public static readonly fileTypeInvalidError = "File type is not valid. Kindly select file(s) with type - ";
}