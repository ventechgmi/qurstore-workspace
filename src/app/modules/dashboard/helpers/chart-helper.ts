enum STORAGE_UTILIZATION_CHART_COLORS {
  Low = "#2A9AEC",
  Medium = "#FFA500",
  High = "#EC332A"
};

export const STORAGE_UTILIZATION_CHART_OPTIONS = {
  chart: {
    type: "column"
  },
  title: {
    text: ""
  },
  xAxis: {
    type: "category",
    title: {
      text: "Threshold"
    }
  },
  yAxis: {
    title: {
      text: "# of Users"
    }
  },
  drilldown: {
    series: []
  },
  series: [],
  lang: {
    noData: "No Data Available"
  },
  noData: {
    style: {
      fontWeight: "bold",
      fontSize: "15px",
      color: "#303030"
    }
  },
}

enum STORAGE_UTILIZATION_SEGREGATION {
  "80-90" = "80-90 %",
  "91-100" = "91-100 %",
  ">100" = ">100 %"
}

export const STORAGE_UTILIZATION_CHART_SERIES = [{
  name: "# of Users",
  colorByPoint: true,
  data: [
    {
      name: STORAGE_UTILIZATION_SEGREGATION["80-90"],
      y: 0,
      drilldown: STORAGE_UTILIZATION_SEGREGATION["80-90"],
      color: STORAGE_UTILIZATION_CHART_COLORS.Low
    },
    {
      name: STORAGE_UTILIZATION_SEGREGATION["91-100"],
      y: 0,
      drilldown: STORAGE_UTILIZATION_SEGREGATION["91-100"],
      color: STORAGE_UTILIZATION_CHART_COLORS.Medium
    },
    {
      name: STORAGE_UTILIZATION_SEGREGATION[">100"],
      y: 0,
      drilldown: STORAGE_UTILIZATION_SEGREGATION[">100"],
      color: STORAGE_UTILIZATION_CHART_COLORS.High
    }
  ]
}]

export const STORAGE_UTILIZATION_CHART_DRILLDOWN_SERIES = [
  { name: "Utilization " + STORAGE_UTILIZATION_SEGREGATION["80-90"], id: STORAGE_UTILIZATION_SEGREGATION["80-90"], data: [] },
  { name: "Utilization " + STORAGE_UTILIZATION_SEGREGATION["91-100"], id: STORAGE_UTILIZATION_SEGREGATION["91-100"], data: [] },
  { name: "Utilization " + STORAGE_UTILIZATION_SEGREGATION[">100"], id: STORAGE_UTILIZATION_SEGREGATION[">100"], data: [] }
];

export const DRILLDOWN_FUNCTION = {
  yAxis: {
    title: {
      text: "% Utilized"
    },
    labels: {
      formatter: function () {
        return this.value + " %";
      }
    },
  },
  xAxis: {
    title: {
      text: "# of Users"
    },
    labels: {
      formatter: function () {
        return this.value;
      }
    }
  },
}

export const DRILLUP_FUNCTION = {
  yAxis: {
    title: {
      text: "# of Users"
    },
    labels: {
      formatter: function () {
        return this.value;
      }
    }
  },
  xAxis: {
    title: {
      text: "Threshold"
    },
    labels: {
      formatter: function () {
        return this.value;
      }
    }
  },
}
