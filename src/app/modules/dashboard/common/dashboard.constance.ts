

export enum UtilityStatus {
    SUBMITTED_WAITING_FOR_APPROVAL = "Submitted - Waiting for approval"
}
export enum WorkspaceStatus  {
    STOPPED = "Stopped",
    AVAILABLE = "Available",
    TERMINATED = "Terminated",
    PENDING = "Pending" 
}