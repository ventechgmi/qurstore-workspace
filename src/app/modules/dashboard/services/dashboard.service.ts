
import { map } from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpRequest } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
import config from "src/app/utils/config";
const appConfigLocal = require("../../../app-config.json");

@Injectable({
    providedIn: "root"
})
export class DashboardService extends EntityService {
    dashboardEvent = new BehaviorSubject("");

    constructor(public http: HttpClient, @Inject(APP_CONFIG) public appConfig: AppConfig) {
        super(http, "dashboard", appConfig);
        this.appConfig = appConfig;

    }

    getDashboardObs(): Observable<any> {
        return this.dashboardEvent.asObservable();
    }

    public getAllTagTypes(): Observable<any> {
        return this.generateGetAPI("/tag-type/list", "");
    }

    public getTagTypeValuesByTagIds(tagIds: string[]): Observable<any> {
        return this.generatePostAPI("/tag-type/list/values", tagIds);
    }

    public getApprovedUtilitiesDashboardData(): Observable<any> {
        return this.generateGetAPI("/utility/approved-utility-dashboard-data", "");
    }

    private generatePostAPI(name: string, data: any): Observable<any> {
        return this.http
            .post(this.getRawApi() + name, data).pipe(
                map((res: Response) => res));
    }

    private generateGetAPI(name, req): Observable<any> {
        return this.http
            .get(this.getRawApi() + name + req).pipe(
                map(res => {
                    return res;
                }));
    }

    public getCostData(filterCriteria): Observable<any> {
        return this.http.post(this.getApi() + "/get-cost-data", filterCriteria).pipe(map((res: Response) => res));
    }

    public getStorageUtilizationData(): Observable<any> {
        return this.generateGetAPI("/environment/dashboard-widgets-data/storage-utilization", "");
    }

    sendDashboardEvent(event) {
        this.dashboardEvent.next(event);
    }

    getRawApi(): string {
        return this.appConfig.apiEndPoint;
    }

    public getEnvironmentWidgetsData(): Observable<any> {
        return this.http
            .get(this.getRawApi() + "/environment/dashboard-widgets-data").pipe(
                map(res => {
                    return res;
                }));
    }

    public getUtilityWidgetsData(): Observable<any> {
        return this.http
            .get(this.getRawApi() + "/utility/dashboard-widgets-data").pipe(
                map(res => {
                    return res;
                }));
    }



}
