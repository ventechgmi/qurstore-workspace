import {
    Component,
    OnInit,
    ContentChild,
    AfterViewInit,
    ViewChild,
    ElementRef,
    AfterContentInit,
    OnDestroy,
    EventEmitter,
    Input
} from "@angular/core";


import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from "../../../../utils/alert/alert.service";
import { DashboardViewViewModel } from '../../viewmodels/dashboard-view.viewmodel';
import { DashboardService } from '../../services/dashboard.service';
import { AuthService } from "src/app/utils/auth.service";
import _ from "lodash";
import { CostEventType } from "src/app/modules/core-ui-components/cost-widget-component/helper/cost-widget.model";
import { Subject } from "rxjs";

@Component({
    selector: "app-dashboard-view",
    templateUrl: "./dashboard-view.component.html",
    styleUrls: ["./dashboard-view.component.scss"],
    providers: [DashboardViewViewModel, AlertService] // need separate alert instance
})
export class DashboardViewComponent implements OnInit, OnDestroy, AfterContentInit {

    constructor(
        private router: Router,
        private dashboardService: DashboardService,
        public dashboardViewViewModel: DashboardViewViewModel,
        public authService: AuthService,
        public alertService: AlertService,
        private route: ActivatedRoute,
        public ngxSmartModalService: NgxSmartModalService
    ) { }

    public ngOnInit() {
        this.dashboardViewViewModel.alertService = this.alertService;
        this.dashboardViewViewModel.init();
    }

    public ngOnDestroy() {
        this.dashboardViewViewModel.subscriptions.unsubscribe();
        this.dashboardService.sendDashboardEvent("");
    }

    public ngAfterContentInit() { }


}
