import {
    Component,
    OnInit,
    ContentChild,
    AfterViewInit,
    ViewChild,
    ElementRef,
    AfterContentInit,
    OnDestroy,
    EventEmitter,
    Input
} from "@angular/core";


import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from "../../../../utils/alert/alert.service";
import { DashboardViewViewModel } from '../../viewmodels/dashboard-view.viewmodel';
import { DashboardService } from '../../services/dashboard.service';
import { AuthService } from "src/app/utils/auth.service";
import _ from "lodash";
import { UserCostWidgetViewModel } from "../../viewmodels/user-cost-widget.viewmodel";

@Component({
    selector: "app-user-cost-widget",
    templateUrl: "./user-cost-widget.component.html",
    styleUrls: ["./user-cost-widget.component.scss"],
    providers: [UserCostWidgetViewModel, AlertService] // need separate alert instance
})
export class UserCostWidgetComponent implements OnInit, OnDestroy, AfterContentInit {

    public headerText = "My Usage Cost($): "
    constructor(
        private router: Router,
        private dashboardService: DashboardService,
        public userCostWidgetViewModel: UserCostWidgetViewModel,
        public authService: AuthService,
        public alertService: AlertService,
        private route: ActivatedRoute,
        public ngxSmartModalService: NgxSmartModalService
    ) { }

    public ngOnInit() {
        this.userCostWidgetViewModel.alertService = this.alertService;
        this.userCostWidgetViewModel.init();
    }

    public ngOnDestroy() {

    }

    public ngAfterContentInit() { }


}
