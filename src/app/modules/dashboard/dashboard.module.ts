import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DataGridModule } from "../core-ui-components";
import { DataGridExtendedModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import {MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatFormFieldModule} from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import {  MatButtonModule} from "@angular/material/button";
import {  MatFormFieldControl} from "@angular/material/form-field";
import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";
import { MatSelectModule } from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {LocalStorageManager} from "../../utils/local-storage-manager";

import { DashboardRoutingModule } from './dashboard-routing-module';
import { DashboardService } from './services/dashboard.service';
import { DashboardViewComponent } from "./views/dashboard-view/dashboard-view.component";
import { HighchartsChartModule } from "highcharts-angular";
import { MatMenuModule } from '@angular/material/menu';
import { NgSelectModule } from '@ng-select/ng-select';
import { CoreUiModule } from "../core-ui-components/core-ui.module";
import { UserCostWidgetComponent } from "./views/user-cost-widget/user-cost-widget.component";
@NgModule(
    {
        declarations: [
            DashboardViewComponent,
            UserCostWidgetComponent
        ],
        imports: [
            SharedModules,
            FormsModule,
            NgSelectModule,
            ReactiveFormsModule,
            CommonModule,
            DashboardRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatSelectModule,
            MatButtonModule,
            DataGridModule,
            DataGridExtendedModule,
            MatCheckboxModule,
            HighchartsChartModule,
            MatMenuModule,
            CoreUiModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [
            DashboardViewComponent
        ],
        providers: [
            AlertService,
            NgxSmartModalService,
            LocalStorageManager
        ]
    }
)
export class DashboardModule {

}
