import { from as observableFrom, Observable, Subject, forkJoin, Subscription } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { Injectable, OnInit, ChangeDetectorRef, ElementRef, EventEmitter } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "lodash";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormGroup, FormBuilder } from '@angular/forms';
import { DashboardViewModel } from './dashboard.viewmodel';
import { DashboardService } from '../services/dashboard.service';
import { EnvironmentService } from "../../environment/services/environment.service";
import { AuthService } from 'src/app/utils/auth.service';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { FormValidationMessages } from "../../cost-management/helpers/validation-messages";
import Highcharts from "highcharts";
import config, { GridConfig, UTILITY_FILTER_TYPE } from "src/app/utils/config";
import PieChartConfig from "../../../utils/chart-config-json/pie-chart.json";
import { ROUTES_CONFIG_MAP, MODULE_BASE_URL_MODULE_ID_MAP } from "src/app/utils/config";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { CostEventType, CostEventTypes } from "../../core-ui-components/cost-widget-component/helper/cost-widget.model";
import { DatasetExtractionService } from "../../../modules/dataset-extraction/services/dataset-extraction.service";
import { STATUS, EXTRACTION_JOB_STATUS } from "../../../modules/dataset-extraction/helpers/constants"
import moment from "moment";
import { UtilityManagementNavigationService } from "../../core-ui-services/utility-management-navigation.service";
import { EnvironmentRequestNavigationService } from "../../core-ui-services/environment-request-navigation.service";
import { DatasetExtractionNavigationService } from "../../core-ui-services/dataset-extraction-navigation.service";
import { UtilityStatus, WorkspaceStatus } from "../common/dashboard.constance";
import * as underscore from "underscore";
import { STORAGE_UTILIZATION_CHART_OPTIONS, STORAGE_UTILIZATION_CHART_SERIES, STORAGE_UTILIZATION_CHART_DRILLDOWN_SERIES,
  DRILLDOWN_FUNCTION, DRILLUP_FUNCTION } from "../helpers/chart-helper";

@Injectable()
export class DashboardViewViewModel extends DashboardViewModel {
  public userId: string;
  public alertService: AlertService;
  public isAdmin = false;
  public utilityModuleRolePermission: any = {};
  public costReportModuleAccess: any = {};
  public isSystemAdmin: boolean = false;
  public canShowAvailableUtilityWidget: boolean = false;
  public approvedUtilitiesListData: any;
  public canShowAvailableDatasetWidget: boolean = false;
  public datasetExtraction = {
    failedCount: 0,
    awaitingApproval: 0
  };
  public environmentListData = {
    reviewCount: 0,
    submittedCount: 0
  };
  public assignedResourcesHeader = [
    { headerName: "Environment Name", field: "environmentName", isClickable: true },
    { headerName: "Date Requested", field: "dateRequestedFormatted" },
    { headerName: "Resource Type", field: "resourceType" },
    { headerName: "Resource Name", field: "resourceName" },
  ];
  public userCostHeader = [
    { headerName: "Service Type", field: "serviceType" },
    { headerName: "Date", field: "Date" },
    { headerName: "Amount", field: "amount" }
  ];
  public assignedResourcesViewModelDataGrid: DataGridViewModel;
  public userCostViewModelDataGrid: DataGridViewModel;

  public UTILITY_FILTER_TYPE = UTILITY_FILTER_TYPE;
  public UtilityStatus = UtilityStatus;
  public environmentRolePermission;
  public assignedResources = [];
  private approvedUtilityList = [];
  private datasetCatalogList = [];
  private datasetResourceType = "Dataset";
  private utilityResourceType = "Utility";

  public userS3CostData = [];
  public Highcharts = Highcharts;
  public notifications = [];
  public subscriptions = new Subscription();
  public costDataEvent = new Subject<CostEventType>();
  public userCostChart: any;
  public tagTypeList: any[] = [];
  public tagTypeValues: any[] = [];
  public isShowUserCostGrid = false;
  public FormValidationMessages = FormValidationMessages;
  public environmentStatus = STATUS;
  public workspaceStatus = WorkspaceStatus;

  public availableDataSetHeader = [
    { headerName: "Name", field: "fileName", isClickable: false },
    { headerName: "Date Generated", field: "submissionDate" },
    { headerName: "Type", field: "datasetTypeName" },
  ];
  public availableUtilitiesHeader = [
    { headerName: "Name", field: "name", isClickable: false },
    { headerName: "Date Approved", field: "dateApproved" }
  ];
  public notificationHeader = [
    { headerName: "Notification", field: "notificationText", isClickable: true },
    { headerName: "Date", field: "notificationDate" }
  ];
  public availableDataSetViewModelDataGrid: DataGridViewModel;
  public availableUtilitiesViewModelDataGrid: DataGridViewModel;
  public infrastructureCostTitle = "Total Infrastructure cost($):";
  public storageUtilization = "Storage Utilization based on Threshold";
  public environmentModulePermission: any = {};
  public utilityModulePermission: any = {};
  public datasetExtractionModulePermission: any = {};
  public environmentWidgetsData: any;
  public storageUtilizationData: any;
  public utilityWidgetsData: any
  public moduleId: string;
  public moduleAccess;
  public moduleInfo;

  private assignedResourcesTableId = "assignedResources";
  private organizationId: string;
  storageUtilizationChart: any;
  storageUtilizationChartOptions: any;

  public constructor(
    public dashboardService: DashboardService,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    private router: Router,
    public authService: AuthService,
    public sanitizer: DomSanitizer,
    private ngxService: NgxUiLoaderService,
    public environmentService: EnvironmentService,
    public utilityManagementNavigationService: UtilityManagementNavigationService,
    public environmentRequestNavigationService: EnvironmentRequestNavigationService,
    public datasetExtractionNavigationService: DatasetExtractionNavigationService,
    public datasetExtractionService: DatasetExtractionService
  ) {
    super(dashboardService, ViewModelType.Edit, ngxSmartModalService, route, authService);
    this.initGrids();
  }


  public init() {
    this.userId = this.authService.getUserId();
    this.organizationId = this.authService.getOrganizationId();
    this.isSystemAdmin = this.authService.isAdminBoolean();
    this.utilityModuleRolePermission = this.getUtilityUserClaims() || {};
    this.costReportModuleAccess = this.getCostReportModuleAccess() || {};
    this.environmentRolePermission = this.getUserClaimsEnvironmentModule() || {};
    this.initSubscriptions();
    this.getDatasetExtractionWidgetsCount();
    this.initTags();
    this.getDataSetCatalogList();
    this.getApprovedUtilities();
    this.initUtilityWidgetData();
    this.initAvailableDatasetWidgetData();
    this.getEnvironmentWidgetsData();
    this.getEnvironmentListData();
    this.getAssignedResourcesEnvironmentData();
    this.getUtilityWidgetsData();
    this.getStorageUtilizationData();
    this.utilityModulePermission = this.getUserClaimsForUtilityModule() || {};
    return observableFrom([]);
  }

  public initSubscriptions() {

    this.subscriptions.add(this.costDataEvent.asObservable().subscribe(e => {
      switch (e.type) {
        case CostEventTypes.FETCH_COST_DATA:
        case CostEventTypes.DRILL_DOWN: {
          const tags = this.buildTagAndValues(e.tagTypeIds, this.tagTypeValues);
          this.getCost(e.range, e.parentTag, e.IsAllProjectTags, tags, true);
          break;
        }
        case CostEventTypes.TAG_CHANGE: {
          this.fetchTagValues(e.tagTypeIds, e.range, e.IsAllProjectTags, false);
          break;
        }
        default: { }
      }
    }));

    this.subscriptions.add(this.assignedResourcesViewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.columnClicked: {
          this.redirectToEnvironmentDetails(e.eventData.rowDetails);
          break;
        }
        default: { }
      }
    }));
  }

  public storageUtilizationChartCallback = (chart) => {
    this.storageUtilizationChart = chart;
  }


  public initStorageUtilizationChart() {
    this.storageUtilizationChartOptions = { ...STORAGE_UTILIZATION_CHART_OPTIONS }
    this.storageUtilizationChartOptions.chart.height = 400;
    this.storageUtilizationChartOptions.chart.events = {
      drilldown: () => {
        this.storageUtilizationChart.update(DRILLDOWN_FUNCTION)
      },
      drillupall: () => {
        this.storageUtilizationChart.update(DRILLUP_FUNCTION)
      }
    };
    if (this.storageUtilizationData && this.storageUtilizationData.length) {
      _.each(this.storageUtilizationData, item => {
        if (item.storagePercentage > 100) {
          STORAGE_UTILIZATION_CHART_SERIES[0].data[2].y++;
          STORAGE_UTILIZATION_CHART_DRILLDOWN_SERIES[2].data.push([item.users.firstName + " " + item.users.lastName, item.storagePercentage])
        } else if (item.storagePercentage >= 91 && item.storagePercentage <= 100) {
          STORAGE_UTILIZATION_CHART_SERIES[0].data[1].y++;
          STORAGE_UTILIZATION_CHART_DRILLDOWN_SERIES[1].data.push([item.users.firstName + " " + item.users.lastName, item.storagePercentage])
        } else if (item.storagePercentage >= 80 && item.storagePercentage < 91) {
          STORAGE_UTILIZATION_CHART_SERIES[0].data[0].y++;
          STORAGE_UTILIZATION_CHART_DRILLDOWN_SERIES[0].data.push([item.users.firstName + " " + item.users.lastName, item.storagePercentage])
        }
      });
      this.storageUtilizationChartOptions.series = STORAGE_UTILIZATION_CHART_SERIES;
      this.storageUtilizationChartOptions.drilldown.series = STORAGE_UTILIZATION_CHART_DRILLDOWN_SERIES;
    }
  }

  public routeToUtilityRequestList(status: string, filterType: string): void {
    this.utilityManagementNavigationService.navigateUsingProxy({
      url: "/utility-management/list",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { filterStatus: status, filterType }
    })
  }

  public userCostChartCallback = (chart) => {
    this.userCostChart = chart;
  }

  public getStorageUtilizationData(): any {
    this.dashboardService.getStorageUtilizationData().subscribe(data => {
      if (data) {
        this.storageUtilizationData = data.payload;
        this.initStorageUtilizationChart();
      }
    });
  }

  public getEnvironmentWidgetsData(): any {
    this.dashboardService.getEnvironmentWidgetsData().subscribe(data => {
      if (data) {
        this.environmentWidgetsData = data.payload;
      }
    });
  }

  public getUtilityWidgetsData(): any {
    this.dashboardService.getUtilityWidgetsData().subscribe(data => {
      if (data) {
        this.utilityWidgetsData = data.payload;
      }
    });
  }

  public gotoFailedDatasetExtractions(): void {
    this.datasetExtractionNavigationService.navigateUsingProxy({
      url: "dataset-extraction/list",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { status: EXTRACTION_JOB_STATUS.Failed }
    }).catch();
  }

  public gotoDatasetExtractionsAwaitingReview(): void {
    this.datasetExtractionNavigationService.navigateUsingProxy({
      url: "dataset-extraction/list",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { status: STATUS.Submitted }
    }).catch();
  }

  public gotoEnvironmentsAwaitingReview(status: string): void {
    this.datasetExtractionNavigationService.navigateUsingProxy({
      url: "environment/list",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { status: status }
    }).catch();
  }

  // private methods
  private initGrids(): void {
    this.assignedResourcesViewModelDataGrid = new DataGridViewModel();
    this.assignedResourcesViewModelDataGrid.dataTableId = this.assignedResourcesTableId;
    this.assignedResourcesViewModelDataGrid.setPagination(true);
    this.assignedResourcesViewModelDataGrid.setHeader(this.assignedResourcesHeader, false, { canUpdate: false, canDelete: false });


    this.userCostViewModelDataGrid = new DataGridViewModel();
    this.userCostViewModelDataGrid.dataTableId = "userCostHeader";
    this.userCostViewModelDataGrid.setPagination(true);
    this.userCostViewModelDataGrid.setHeader(this.userCostHeader, false, { canUpdate: false, canDelete: false });

    this.availableDataSetViewModelDataGrid = new DataGridViewModel();
    this.availableDataSetViewModelDataGrid.dataTableId = "availableDataSet";
    this.availableDataSetViewModelDataGrid.setPagination(true);
    this.availableDataSetViewModelDataGrid.setHeader(this.availableDataSetHeader, false, { canUpdate: false, canDelete: false });

    this.availableUtilitiesViewModelDataGrid = new DataGridViewModel();
    this.availableUtilitiesViewModelDataGrid.dataTableId = "availableUtilities";
    this.availableUtilitiesViewModelDataGrid.setPagination(true);
    this.availableUtilitiesViewModelDataGrid.setHeader(this.availableUtilitiesHeader, false, { canUpdate: false, canDelete: false });
  }

  private initTags(): void {
    this.dashboardService.getAllTagTypes()
      .subscribe(result => {
        if (result) {
          this.tagTypeList = _.map(result, tag => ({ ...tag, itemName: tag.name }));
          this.costDataEvent.next({ type: CostEventTypes.TAGS_LOADED, tagTypes: this.tagTypeList });
        }
      },
        error => {
          this.alertService.error(error);
        }
      );
  }

  private fetchTagValues(tagTypeIds: string[], range: any, isAllProject: boolean, isInit: boolean): void {
    this.dashboardService.getTagTypeValuesByTagIds(tagTypeIds)
      .subscribe(result => {
        if (result) {
          this.tagTypeValues = result;
          if (!isInit) {
            const tags = this.buildTagAndValues(tagTypeIds, result);
            this.getCost(range, null, isAllProject, tags, false);
          }
        }
      },
        error => {
          this.alertService.error(error);
        }
      );
  }

  private buildTagAndValues(tagTypeIds: string[], tagTypeValues: any[]): any {
    const tagIdInfoMap = {};
    const selectedTagIdInfoMap = {};
    const tagTypes = _.filter(this.tagTypeList, (tagType) => tagTypeIds.includes(tagType.id));
    _.each(tagTypes, tag => {
      tagIdInfoMap[tag.id] = tag;
    });
    _.each(tagTypes, tag => {
      selectedTagIdInfoMap[tag.id] = { tag: tagIdInfoMap[tag.id].name, tagValues: [] };
    });
    _.each(tagTypeValues, tagValue => {
      selectedTagIdInfoMap[tagValue.tagTypeId]?.tagValues?.push(tagValue.value);
    });
    return Object.values(selectedTagIdInfoMap);
  }

  private getEnvironmentListData(): void {
    this.environmentService.getEnvironmentList(null, null).subscribe(data => {
      if (data.length) {
        const environmentList = [];
        for (const environment of data) {
          let keyObject: any = {};
          keyObject.id = environment.id;
          keyObject.status = environment.currentStatus.name;
          keyObject.requestedById = environment.environmentRequestHeaderStatusAudits[0].actionBy;
          environmentList.push(keyObject);
        };
        const reviewCount = environmentList.filter(e => (e.status === STATUS.Submitted && e.requestedById !== this.userId));
        this.environmentListData.reviewCount = reviewCount.length;
      }
    }, error => {
      this.alertService.error(error.message, true);
    });
  }

  private getAssignedResourcesEnvironmentData(): void {

    this.environmentService.getAssignedResources().subscribe(data => {
      if (data.isSuccess) {
        const finalList = [];
        for (const environmentRequest of data.payload) {
          const headerObj: any = {};
          headerObj.environmentId = environmentRequest.id;
          headerObj.environmentName = environmentRequest.name;
          headerObj.dateRequestedFormatted = environmentRequest.requestedDateTime ? moment(environmentRequest.requestedDateTime).format(config.APPLICATION_DATE_FORMAT) : "";
          environmentRequest.environmentRequestDetails.forEach(details => {
            details.datasetRequests.forEach(datasetRequest => {
              datasetRequest.datasetRequestDetails.forEach(item => {
                if (!item.isDeleted) {
                  const dataSetObj: any = { ...headerObj };
                  dataSetObj.resourceType = this.datasetResourceType;
                  const datasetDetail = this.datasetCatalogList.find(e => e.id === item.datasetId);
                  if (datasetDetail) {
                    dataSetObj.resourceName = datasetDetail.fileName;
                    dataSetObj.id = environmentRequest.id + this.datasetResourceType + datasetDetail.fileName;
                    finalList.push(dataSetObj);
                  }
                }
              });
            });
            details.workspaceUtilityMappings.forEach(utilityMapping => {
              if (!utilityMapping.isDeleted) {
                const utilityObj: any = { ...headerObj };
                utilityObj.resourceType = this.utilityResourceType;
                const utilityDetail = this.approvedUtilityList.find(e => e.id === utilityMapping.utilityRequestId);
                if (utilityDetail) {
                  utilityObj.resourceName = utilityDetail.name;
                  utilityObj.id = environmentRequest.id + this.utilityResourceType + utilityDetail.name;
                  finalList.push(utilityObj);
                }
              }
            });
          });
          this.assignedResources = underscore.uniq(finalList, (e: any) => e.id);
        }
      } else {
        this.assignedResources = [];
      }
    }, error => {
      this.alertService.error(error.message, true);
    }, () => {
      this.assignedResourcesViewModelDataGrid.setRowsData(this.assignedResources);
    });
  }

  private getApprovedUtilities(): void {
    this.environmentService.getApprovedUtilities().subscribe(
      data => {
        if (data) {
          this.approvedUtilityList = data;
        }
      },
      error => {
        this.alertService.error(error.message, true);
      }
    );
  }

  private getDataSetCatalogList(): void {
    this.environmentService.getDataSetCatalogList().subscribe(
      data => {
        if (data) {
          this.datasetCatalogList = data;
        }
      },
      error => {
        this.alertService.error(error.message, true);
      }
    );
  }

  private redirectToEnvironmentDetails(rowDetails: any): void {
    const environmentId = rowDetails.environmentId;
    const data = { isFromRequestMoreUtility: false, isApproverFromList: false, isDetailFromList: true, environmentId: environmentId };
    localStorage.setItem("redirectData", JSON.stringify(data));

    this.environmentRequestNavigationService.navigateUsingProxy({
      url: "/environment/request",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { environmentId: environmentId, isRedirectDashboard: true }
    }).catch();
  }

  private initUtilityWidgetData(): void {
    if (this.isSystemAdmin || (this.utilityModuleRolePermission && this.utilityModuleRolePermission.canExecute)) {
      this.canShowAvailableUtilityWidget = true;
      this.dashboardService.getApprovedUtilitiesDashboardData()
        .subscribe(result => {
          if (result && result.isSuccess) {
            let approvedUtilities = [];
            if (result.payload) {
              _.each(result.payload, utilityData => {
                approvedUtilities.push({ name: utilityData.name, dateApproved: moment(utilityData.lastModifiedDate).format(config.APPLICATION_DATE_FORMAT) });
              });
            }
            this.approvedUtilitiesListData = approvedUtilities;
            this.availableUtilitiesViewModelDataGrid.setRowsData(this.approvedUtilitiesListData);
          }
        },
          error => {
            this.alertService.error(error);
          }
        );
    }
  }

  private getCost(range: any, parentTag?: any, isAllProjectTags?: boolean, tags?: any[], IsAllTags?: boolean): void {
    const dbObj = {
      parentTag,
      noOfDays: "",
      startDate: range.startDate,
      endDate: range.endDate,
      organizationId: this.organizationId,
      costType: config.AWS_AMORTIZED_COST,
      tags: tags || [],
      IsAllProjectTags: isAllProjectTags, // to fetch only all projects. exclude untags and common
      IsAllTags
    }
    if (dbObj.parentTag) {
      dbObj.IsAllTags = false;
    }
    this.ngxService.start();
    this.dashboardService.getCostData(dbObj).subscribe(
      data => {
        this.costDataEvent.next({
          type: CostEventTypes.LOAD_COST_DATA,
          data,
          parentTag,
          tags: dbObj.tags.length
        });
        this.ngxService.stop();
      },
      error => {
        this.alertService.error(error, true);
        this.ngxService.stop();
      }
    );
  }

  private initAvailableDatasetWidgetData(): void {
    if (this.isSystemAdmin || (this.environmentRolePermission && this.environmentRolePermission.canExecute)) {
      this.canShowAvailableDatasetWidget = true;
      this.environmentService.getDataSetCatalogList()
        .subscribe(data => {
          if (data) {
            _.each(data, (ds) => {
              ds.submissionDate = ds.submissionDate ? moment(ds.submissionDate).format(config.APPLICATION_DATE_FORMAT) : "-";;
            });
            this.availableDataSetViewModelDataGrid.setRowsData(data);
          }
        },
          error => {
            this.alertService.error(error);
          }
        );
    }
  }

  private getDatasetExtractionWidgetsCount(): void {
    this.datasetExtractionModulePermission = this.getUserClaimsForDataExtractionModule() || {};
    this.datasetExtractionService.getDatasetExtractionList().subscribe(data => {
      if (data) {
        let unfilteredList = [];
        _.each(data, item => {
          const obj: any = {
            id: item.id,
            createdBy: item.createdBy,
            status: item.status
          }
          if (item.dataExtractDetails.length) {
            const dataExtractDetail = item.dataExtractDetails[0];
            if (dataExtractDetail.dataExtractProcesses.length) {
              const dataExtractProcess = dataExtractDetail.dataExtractProcesses[0];
              obj.jobStatus = dataExtractProcess.processingStatus.name;
            }
          }
          if (item.createdBy === this.userId && obj.jobStatus) {
            obj.status = obj.jobStatus;
          }
          switch (item.status) {
            case STATUS.Configured:
              if (item.createdBy === this.userId) {
                obj.action = STATUS.Configured;
              }
              break;
            case EXTRACTION_JOB_STATUS.Completed:
            case STATUS.Submitted:
              if (this.datasetExtractionModulePermission?.canExecute && item.createdBy !== this.userId &&
                obj.jobStatus === EXTRACTION_JOB_STATUS.Completed) {
                obj.action = STATUS.Submitted;
              }
              break;
            case STATUS.Approved:
              obj.status = item.status;
              obj.action = STATUS.Approved;
              break;
            case STATUS.Rejected:
              obj.status = item.status;
              obj.action = STATUS.Rejected;
              break;
            default:
              obj.action = undefined;
              break;
          }
          unfilteredList.push(obj);
        });
        const datasetExtractionList = this.filterDataExtractionList(unfilteredList);

        const failedCount = datasetExtractionList.filter(e => (e.jobStatus === EXTRACTION_JOB_STATUS.Failed && e.status === EXTRACTION_JOB_STATUS.Failed));
        const awaitingApprovalCount = datasetExtractionList.filter(e => (e.jobStatus === EXTRACTION_JOB_STATUS.Completed && e.status === STATUS.Submitted));

        this.datasetExtraction.failedCount = failedCount.length;
        this.datasetExtraction.awaitingApproval = awaitingApprovalCount.length;
      }
    }, error => {
      this.alertService.error(error.message, true);
      return [];
    });
  }

  private filterDataExtractionList(data: any[]): any[] {
    const filteredList = [];
    if (data && data.length) {
      data.forEach((dataItem: any) => {
        if (this.datasetExtractionModulePermission.canExecute) {
          if (dataItem.status === STATUS.Configured) {
            if (dataItem.createdBy === this.userId) {
              filteredList.push(dataItem);
            }
          } else if (dataItem.status === STATUS.Submitted && dataItem.createdBy !== this.userId) {
            if (dataItem.jobStatus === EXTRACTION_JOB_STATUS.Completed) {
              filteredList.push(dataItem);
            }
          }
          else {
            filteredList.push(dataItem);
          }
        } else {
          if (dataItem.createdBy === this.userId) {
            filteredList.push(dataItem);
          }
        }
      });
    }
    return filteredList;
  }


}
