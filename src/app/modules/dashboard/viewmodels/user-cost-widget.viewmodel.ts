import { from as observableFrom, Observable, Subject, forkJoin, Subscription } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { Injectable, OnInit, ChangeDetectorRef, ElementRef, EventEmitter } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "lodash";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormGroup, FormBuilder } from '@angular/forms';
import { DashboardViewModel } from './dashboard.viewmodel';
import { DashboardService } from '../services/dashboard.service';
import { EnvironmentService } from "../../environment/services/environment.service";
import { AuthService } from 'src/app/utils/auth.service';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { FormValidationMessages } from "../../cost-management/helpers/validation-messages";
import Highcharts from "highcharts";
import config, { GridConfig } from "src/app/utils/config";
import PieChartConfig from "../../../utils/chart-config-json/pie-chart.json";
import { ROUTES_CONFIG_MAP, MODULE_BASE_URL_MODULE_ID_MAP } from "src/app/utils/config";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { CostEventType, CostEventTypes } from "../../core-ui-components/cost-widget-component/helper/cost-widget.model";
import moment from "moment";

@Injectable()
export class UserCostWidgetViewModel extends DashboardViewModel {

    public alertService: AlertService;
    public subscriptions = new Subscription();
    public costDataEvent = new Subject<CostEventType>();

    private organizationId: string;
    private userId: string;
    private isSystemAdmin: boolean;
    public costReportModuleAccess: any = {};
    public constructor(
        public dashboardService: DashboardService,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute,
        private router: Router,
        public authService: AuthService,
        public sanitizer: DomSanitizer,
        private ngxService: NgxUiLoaderService,
        public environmentService: EnvironmentService
    ) {
        super(dashboardService, ViewModelType.Edit, ngxSmartModalService, route, authService);
    }
    public init() {
        this.userId = this.authService.getUserId();
        this.organizationId = this.authService.getOrganizationId();
        this.isSystemAdmin = this.authService.isAdminBoolean();
        this.costReportModuleAccess = this.getCostReportModuleAccess() || {};
        this.initSubscriptions();
        if(this.isSystemAdmin || this.costReportModuleAccess.canReadModule) {
            this.getCost({ startDate: moment().startOf('week'), endDate: moment() }, null, true);
        }
        return observableFrom([]);
    }

    public initSubscriptions() {
        this.subscriptions.add(this.costDataEvent.asObservable().subscribe(e => {
            switch (e.type) {
                case CostEventTypes.FETCH_COST_DATA:
                case CostEventTypes.DRILL_DOWN: {
                    this.getCost(e.range, e.parentTag, e.IsOnlyUserTag);
                    break;
                }
                default: { }
            }
        }));
    }

    private getCost(range: any, parentTag?: any, IsOnlyUserTag?: boolean): void {
        const dbObj = {
            parentTag,
            noOfDays: "",
            startDate: range.startDate,
            endDate: range.endDate,
            organizationId: this.organizationId,
            costType: config.AWS_AMORTIZED_COST,
            tags: [],
            IsAllProjectTags: false, // to fetch only all projects. exclude untags and common
            IsAllTags: false,
            IsOnlyUserTag
        }
        if (dbObj.parentTag) {
            dbObj.IsAllTags = false;
        }
        this.ngxService.start();
        this.dashboardService.getCostData(dbObj).subscribe(
            data => {
                this.costDataEvent.next({
                    type: CostEventTypes.LOAD_COST_DATA,
                    data,
                    parentTag,
                    tags: dbObj.tags.length
                });
                this.ngxService.stop();
            },
            error => {
                this.alertService.error(error, true);
                this.ngxService.stop();
            }
        );
    }

}
