import { EntityBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { Subject, of } from "rxjs";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ActivatedRoute } from "@angular/router";
import { DashboardService } from '../services/dashboard.service';
import { ROUTES_CONFIG_MAP, MODULE_BASE_URL_MODULE_ID_MAP } from "src/app/utils/config";
import { AuthService } from 'src/app/utils/auth.service';
export class DashboardViewModel extends EntityBaseViewModel<"", DashboardService> {
    public utilityModuleId: string;
    public utilityModuleAccess;
    public utilityModuleInfo;
    public updateTargetDefinitionListSubject = new Subject<any>();
    public targetDefinitionDetailSelection = new Subject<any>();
    auditLogPermission: any = {};

    public constructor(
        public dashboardService: DashboardService,
        public viewModelType: ViewModelType,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute,
        public authService: AuthService
    ) {
        super(dashboardService, viewModelType);
    }

    public getUtilityUserClaims() {
        const baseUrl = ROUTES_CONFIG_MAP.UTILITY_MANAGEMENT.BASE_URL;
        this.utilityModuleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
        this.utilityModuleInfo = this.authService.getModuleAccess(this.utilityModuleId) || null;
        this.utilityModuleAccess = this.utilityModuleInfo ? this.utilityModuleInfo.RolePermissions : {};

        return this.utilityModuleAccess;
    }

    public getUserClaimsForUtilityModule(): any {
        const baseUrl = ROUTES_CONFIG_MAP.UTILITY_MANAGEMENT.BASE_URL;
        const moduleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
        const moduleInfo = this.authService.getModuleAccess(moduleId) || null;
        const moduleAccess = moduleInfo ? moduleInfo.RolePermissions : {};
        return moduleAccess;
    }

    public getUserClaimsForDataExtractionModule(): any {
        const baseUrl = ROUTES_CONFIG_MAP.DATASET_EXTRACTION.BASE_URL;
        const moduleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
        const moduleInfo = this.authService.getModuleAccess(moduleId) || null;
        const moduleAccess = moduleInfo ? moduleInfo.RolePermissions : {};
        return moduleAccess;
    }

    public getUserClaimsEnvironmentModule(): any {
        const baseUrl = ROUTES_CONFIG_MAP.ENVIRONMENT.BASE_URL;
        const moduleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
        const moduleInfo = this.authService.getModuleAccess(moduleId) || null;
        const moduleAccess = moduleInfo ? moduleInfo.RolePermissions : {};
        return moduleAccess;
    }

    public getCostReportModuleAccess(): any {
        const baseUrl = ROUTES_CONFIG_MAP.COST_MANAGEMENT.BASE_URL;
        const moduleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
        const moduleInfo = this.authService.getModuleAccess(moduleId) || null;
        const moduleAccess = moduleInfo ? moduleInfo.RolePermissions : {};
        return moduleAccess;
    }

}
