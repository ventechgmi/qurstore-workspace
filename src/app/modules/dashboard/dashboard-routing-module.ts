import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { DashboardViewComponent } from "./views/dashboard-view/dashboard-view.component";

export const routes: Routes = [
  {
    path: "",
    component: DashboardViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
