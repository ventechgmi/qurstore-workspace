import { map } from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient, HttpEvent, HttpRequest } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class DatasetExtractionService extends EntityService {
    appConfig: AppConfig;
    onUpdateListEvent = new EventEmitter();
    public datasetExtractionIdSubject = new ReplaySubject<any>();
    public showSuccessMessage = new Subject<any>();
    public alertInfo = { message: "", type: "" };
    private afterInitSubject = new BehaviorSubject(this.alertInfo);

    constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
        super(http, "dataset-extraction-service", appConfig);
        this.appConfig = appConfig;

    }

    public getDatasetExtractionList(): Observable<any> {
        return this.generateGetAPI("/list", "");
    }

    public getOriginalDatasetList(): Observable<any> {
        return this.generateGetAPI("/dataset/list", "");
    }

    public getDatasetColumns(originalDatasetId: string): Observable<any> {
        return this.generatePostAPI("/get-columns", { id: originalDatasetId });
    }

    public getDatasetRowsCount(originalDatasetId: string): Observable<any> {
        return this.generatePostAPI("/get-rows-count", { id: originalDatasetId });
    }

    public getQueryResult(virtualDatasetInfo: string, query: string): Observable<any> {
        return this.generatePostAPI("/preview-query-result", { query: query, virtualDatasetInfo: virtualDatasetInfo });
    }

    public deleteDataExtractionConfiguration(datasetExtractionId: string): Observable<any> {
        return this.generateDeleteAPI("/delete/", datasetExtractionId);
    }
    public saveDataExtractionConfiguration(dataExtractionConfigurationDetails: any): Observable<any> {
        return this.generatePostAPI("/save", dataExtractionConfigurationDetails);
    }

    public submitDataExtractionConfiguration(dataExtractionConfigurationDetails: any): Observable<any> {
        return this.generatePostAPI("/submit", dataExtractionConfigurationDetails);
    }

    public reviewDataExtractionConfiguration(dataExtractionConfigurationDetails: any): Observable<any> {
        return this.generatePostAPI("/review", dataExtractionConfigurationDetails);
    }

    public getDataExtractionConfigurationById(dataExtractionConfigurationId: string): Observable<any> {
        return this.generateGetAPI("/", dataExtractionConfigurationId);
    }

    public getRawApi(): string {
        return this.appConfig.apiEndPoint;
    }

    public getDatasetExtractionIdSubject() {
        return this.datasetExtractionIdSubject.asObservable();
    }

    public getDatasetExtractionServiceAPIURL() {
        return this.getApi();
    }

    public showAfterIntMessage(messageInfo) {
        this.afterInitSubject.next(messageInfo);
    }

    public getAfterIntObs(): Observable<any> {
        return this.afterInitSubject.asObservable();
    }

    public getShowSuccessMessage() {
        return this.showSuccessMessage.asObservable();
    }

    private generateGetAPI(name, req): Observable<any> {
        return this.http
            .get(this.getDatasetExtractionServiceAPIURL() + name + req).pipe(
                map(res => {
                    return res;
                }));
    }

    private generateDeleteAPI(name, data): Observable<any> {
        return this.http
            .delete(this.getDatasetExtractionServiceAPIURL() + name + data).pipe(
                map(res => {
                    return res;
                }));
    }


    private generatePostAPI(name, data): Observable<any> {
        return this.http
            .post(this.getDatasetExtractionServiceAPIURL() + name, data).pipe(
                map((res: Response) => res));
    }

}
