import { Routes, RouterModule } from "@angular/router";
import { DatasetExtractionListComponent } from "./views/dataset-extraction-list/dataset-extraction-list.component";
import { DatasetExtractionDetailComponent } from "./views/dataset-extraction-detail/dataset-extraction-detail.component";
import { NgModule } from "@angular/core";

export const routes: Routes = [
  {
    path: "dataset-extraction/list",
    component: DatasetExtractionListComponent
  },
  {
    path: "dataset-extraction/list/:key",
    component: DatasetExtractionListComponent
  },
  {
    path: "dataset-extraction/create",
    component: DatasetExtractionDetailComponent
  },
  {
    path: "dataset-extraction/:extractionConfigId/:modalType",
    component: DatasetExtractionDetailComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DatasetExtractionRoutingModule { }
