export const CHART_COLORS = {
  Configured: "#03b6fc",
  Submitted: "#5c5f63", // light grey
  Approved: "#80ff00",
  Rejected: "#ff6a00",
  Canceled: "#f21616",
  Failed: "#2b2a2a", //dark grey
  Completed: "#008c28",
  Inprogress: "#f2ca16"
};

export const ChartOptions = {
  chart: {
    type: "column"
  },
  title: {
    text: ""
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: "Count"
    }
  },
  credits: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true
      }
    }
  },
  tooltip: {
    enabled: false
  },
  series: []
}
