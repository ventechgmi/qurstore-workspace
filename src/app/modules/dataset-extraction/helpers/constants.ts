export enum STATUS {
  Configured = "Configured",
  Submitted = "Submitted - Waiting for approval",
  Approved = "Approved",
  Rejected = "Rejected",
  RequestChanges = "Request Changes"
}

export enum EXTRACTION_JOB_STATUS {
  Completed = "Completed",
  Canceled = "Canceled",
  Failed = "Failed",
  Inprogress = "In progress"
}

export enum ModalType {
  Create = "Create",
  Detail = "Detail",
  Edit = "Edit",
  Delete = "Delete",
  Approve = "Approve",
  Rejected = "Rejected"
}

export enum extractionType {
  percentage = "Percentage",
  rowsCount = "RowsCount"
}

export enum NgxSmartModelNames {
  RejectConfirmModal = "rejectConfirmModal"
}

