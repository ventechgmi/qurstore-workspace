export class ValidationMessages {

  public static readonly ReviewMessage = "Data Extract Configuration has been ";
  public static readonly Column_Empty = "There are no available columns in the dataset";
  public static readonly Rows_Empty = "There are no available rows in the dataset";
  public static readonly RequiredMessage = " is required";
  public static readonly MinMaxLengthMessage = " length should be between 3-100 ";
  public static readonly ExtractedRowsPercentErrorMessage = "Should be between 1-100%";
  public static readonly ExtractedRowsCountErrorMessage = `Row count {currentRowCount} should not exceed the actual row count {rowCount}`;
}
