import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "src/app/utils/auth.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { ModalType } from "../../helpers/constants";
import { DatasetExtractionService } from '../../services/dataset-extraction.service';
import { DatasetExtractionDetailViewModel } from '../../viewmodels/dataset-extraction-detail.viewmodel';

@Component({
  selector: 'dataset-extraction-detail.component',
  templateUrl: './dataset-extraction-detail.component.html',
  styleUrls: ['./dataset-extraction-detail.component.css'],
  providers: [DatasetExtractionDetailViewModel, AlertService]
})
export class DatasetExtractionDetailComponent implements OnInit {

  constructor(private router: Router,
    private authService: AuthService,
    private datasetExtractionService: DatasetExtractionService,
    public detailViewModel: DatasetExtractionDetailViewModel,
    public alertService: AlertService,
    private route: ActivatedRoute,
    public ngxSmartModalService: NgxSmartModalService
  ) {
    alertService.afterInitObs = datasetExtractionService.getAfterIntObs();
  }

  public ngOnInit() {
    this.route.params
      .subscribe(params => {
        const dataToPass = { dataSetConfigId: "", modalType: "" };
        this.detailViewModel.initServices(this.alertService);
        if (params.extractionConfigId) {
          this.detailViewModel.modalType = params.modalType;
          dataToPass.dataSetConfigId = params.extractionConfigId;
          dataToPass.modalType = params.modalType;
          this.detailViewModel.init(dataToPass);
        } else {
          this.detailViewModel.modalType = ModalType.Create;
          dataToPass.modalType = ModalType.Create;
          this.detailViewModel.init(dataToPass);
        }
      });
  }

  public ngAfterContentInit() { }


}

