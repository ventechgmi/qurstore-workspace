import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormBuilder } from "@angular/forms";
import { AuthService } from "src/app/utils/auth.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { DatasetExtractionListViewModel } from '../../viewmodels/dataset-extraction-list.viewmodel';
import { DatasetExtractionService } from '../../services/dataset-extraction.service';
import { DatasetExtractionNavigationService } from "../../../core-ui-services/dataset-extraction-navigation.service";

@Component({
  selector: 'app-dataset-extraction-list.component',
  templateUrl: './dataset-extraction-list.component.html',
  styleUrls: ['./dataset-extraction-list.component.css'],
  providers: [DatasetExtractionListViewModel, AlertService]
})
export class DatasetExtractionListComponent implements OnInit {

  public datasetExtractionList: any;
  public dataTable: any;
  constructor(private router: Router,
    private authService: AuthService,
    private datasetExtractionService: DatasetExtractionService,
    public listViewModel: DatasetExtractionListViewModel,
    public alertService: AlertService,
    private route: ActivatedRoute,
    public ngxSmartModalService: NgxSmartModalService,
    public datasetExtractionNavigationService: DatasetExtractionNavigationService
  ) {
    alertService.afterInitObs = datasetExtractionService.getAfterIntObs();
  }

  public ngOnInit() {
    this.route.params.subscribe(async params => {
      const userId = this.authService.getUserId();
      this.listViewModel.initServices(this.alertService);
      if (params.key) {
        const response = await this.datasetExtractionNavigationService.navigationParametersForLink(params["key"]);
        const status = response.status;
        this.listViewModel.init({ userId: userId, status: status });
      } else {
        this.listViewModel.init({ userId: userId });
      }
    });
  }

  public ngAfterContentInit() { }

}

