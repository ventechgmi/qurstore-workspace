import { from as observableFrom, Observable, Subject, forkJoin } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AuthService } from "src/app/utils/auth.service";
import { DatasetExtractionViewModel } from "./dataset-extraction.viewmodel";
import { DataGridExtendedViewModel, DataGridViewModel } from "../../core-ui-components";
import { DatasetExtractionService } from "../services/dataset-extraction.service";
import { ValidationMessages } from "../helpers/validation-messages";
import { STATUS, ModalType, extractionType, EXTRACTION_JOB_STATUS, NgxSmartModelNames } from "../helpers/constants";
import { GuidHelper } from "../../employee-dashboard/helpers/guid-helper";
import moment from "moment";
import { EventListenerFocusTrapInertStrategy } from "@angular/cdk/a11y";

@Injectable()
export class DatasetExtractionDetailViewModel extends DatasetExtractionViewModel {

  public modalType: string;
  public datasetColumnsGrid: DataGridExtendedViewModel;
  public previewDataGrid: DataGridViewModel;
  public originalDatasetList = [];
  public datasetExtractionForm: FormGroup;
  public enableApprovalControls = false;
  public comments = "";
  public SelectedColumnsString: string = "";
  public submitted: boolean = false;
  public showEditButton: boolean;
  public currentStatusName: any;
  public extractionJobStatus: string = "";
  public extractionJobDate: string = "";
  public failureDetails: string = "";
  public isExtractionJobCompleted: boolean = false;
  public isReviewButtonDisabled: boolean = false;
  public actionComments: any;
  public actionDate: any;
  public validationError = {
    extractedRowsCountError: "",
    extractedRowsPercentError: ""
  };
  public datasetTableName: any;
  public previewCount = 0;

  private selectedColumns = [];
  private datasetColumns = [];
  private dataSetConfigId: string;
  private alertService: AlertService;
  private queryResult = [];
  private detailEdit: boolean;
  private currentSetOfColumns = [];
  private nonSelectedColumns = [];
  private extractedVirtualDatasetInfo: any;
  private isColumnSelectionChanged = false;
  private pristineSelectedColumnString = "";
  private failureSortKey = "logDateTime";

  public constructor(
    public datasetExtractionService: DatasetExtractionService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public authService: AuthService) {
    super(datasetExtractionService, ViewModelType.Edit, ngxSmartModalService, route, authService);
  }

  public init(param?: any): Observable<any> {
    this.modalType = param.modalType;
    this.getOriginalDatasets();
    this.initDatasetExtractionDetailForm();
    this.datasetColumnsGrid = new DataGridExtendedViewModel();
    this.previewDataGrid = new DataGridViewModel();
    this.initializeDatasetGridColDef();
    this.subscribers();

    switch (this.modalType) {
      case ModalType.Create:
        this.dataSetConfigId = undefined;
        break;
      case ModalType.Detail:
        this.dataSetConfigId = param.dataSetConfigId
        this.datasetExtractionForm.disable();
        this.getDataExtractionConfigurationById(this.dataSetConfigId);
        break;
      case ModalType.Edit:
        this.dataSetConfigId = param.dataSetConfigId
        this.datasetExtractionForm.enable();
        this.getDataExtractionConfigurationById(this.dataSetConfigId);
        break;
      case ModalType.Approve:
      case ModalType.Rejected:
        this.dataSetConfigId = param.dataSetConfigId
        this.datasetExtractionForm.disable();
        this.getDataExtractionConfigurationById(this.dataSetConfigId);
        break;
      default:
        break;
    }
    return observableFrom([]);
  }

  public subscribers(): void {
    this.datasetColumnsGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.rowSelected:
          this.modifyQuery();
          break;
        default:
          break;
      }
    }, error => {
      this.alertService.error(error.message, true);
    });
  }

  public initServices(alertService: AlertService): void {
    this.alertService = alertService;
  }

  public getOriginalDatasets(): void {
    this.datasetExtractionService.getOriginalDatasetList().subscribe(data => {
      if (data && data.length) {
        this.originalDatasetList = data;
      } else {
        this.originalDatasetList = [];
      }
    }, error => {
      this.originalDatasetList = [];
      this.alertService.error(error);
    });
  }

  public getDataExtractionConfigurationById(dataExtractionConfigurationId: string): void {
    this.datasetExtractionService.getDataExtractionConfigurationById(dataExtractionConfigurationId).subscribe(data => {
      const queryObject = JSON.parse(data.dataExtractDetails.length ? data.dataExtractDetails[0].sqlquery : "");

      this.datasetExtractionForm.patchValue({
        id: data.id,
        name: data.name,
        originalDataset: this.originalDatasetList.find(dataset => (dataset.id === data.originalDatasetId)),
        sqlFilterCondition: queryObject.SqlFilterCondition,
        totalRows: 0,
        extractionType: data.dataExtractDetails.length ? data.dataExtractDetails[0].percentageOfRows ? extractionType.percentage : data.dataExtractDetails[0].numberOfRows ? extractionType.rowsCount : null : null,
        extractedRowsPercent: data.dataExtractDetails.length ? data.dataExtractDetails[0].percentageOfRows : null,
        extractedRowsCount: data.dataExtractDetails.length ? data.dataExtractDetails[0].numberOfRows : null
      });
      this.datasetTableName = this.datasetExtractionForm.value.originalDataset.name;
      this.SelectedColumnsString = queryObject.SelectedColumnsString;
      this.pristineSelectedColumnString = queryObject.SelectedColumnsString;
      this.extractedVirtualDatasetInfo = data.dataExtractDetails.length ? data.dataExtractDetails[0].virtualDatasetInfo : "";
      this.getExtractionJobStatus(data);

      this.datasetColumns = data.dataExtractDetails.length ? data.dataExtractDetails[0].dataExtractColumns : [];
      this.getRowsCount(this.datasetExtractionForm.value.originalDataset);

      if (this.modalType !== ModalType.Edit) {
        this.datasetColumnsGrid.gridOptions.api.setRowData(_.filter(data.dataExtractDetails[0].dataExtractColumns, column => column.isSelectedColumn === true));

        if ((data.currentStatus.name === STATUS.Approved) || (data.currentStatus.name === STATUS.Rejected)) {
          this.currentStatusName = data.currentStatus.name;
          this.actionComments = data.currentStatus.dataExtractHeaderStatusAudits.length ? data.currentStatus.dataExtractHeaderStatusAudits[0].actionComments : "";
          this.actionDate = data.currentStatus.dataExtractHeaderStatusAudits.length ? moment(data.currentStatus.dataExtractHeaderStatusAudits[0].actionDate).format(config.APPLICATION_DATE_TIME_FORMAT) : "";
        }
      } else {
        this.updateOnEdit(data);
      }
      this.showEditButton = (this.modalType === ModalType.Detail) && (data.currentStatus.name === STATUS.Configured)
    }, error => {
      this.alertService.error(error);
    });
  }

  public editFromDetail(): void {
    this.detailEdit = true;
    this.router.navigate([`dataset-extraction/${this.dataSetConfigId}/edit`]);
  }

  public updateOnEdit(data: any): void {
    this.datasetColumnsGrid.gridOptions.api.setRowData(data.dataExtractDetails[0].dataExtractColumns);
    this.datasetColumnsGrid.gridOptions.api.forEachNode(node => {
      node.setSelected(node.data.isSelectedColumn);
    });
  }

  public onOriginalDatasetChange(event: any): void {
    this.datasetColumnsGrid.gridApi.showLoadingOverlay();
    if (event && event.id) {
      this.getDatasetColumnsAndRowCount(event);
    }
  }

  public preview(): void {
    const query = JSON.stringify({
      SelectedColumnsString: this.SelectedColumnsString,
      sqlFilterCondition: this.datasetExtractionForm.value.sqlFilterCondition
    });
    this.ngxSmartModalService.getModal("previewData").open();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
    this.previewDataGrid.gridApi.showLoadingOverlay();
    this.datasetExtractionService.getQueryResult(this.extractedVirtualDatasetInfo, query).subscribe(data => {
      if (data.isSuccess) {
        this.queryResult = data.data;
        this.previewCount = data.data.length;
        this.previewDataGridHeaders(data.data[0]);
        this.previewDataGrid.setRowsData(this.queryResult);
      } else {
        this.previewDataGrid.gridOptions.api.hideOverlay();
        this.queryResult = [];
      }
    }, error => {
      this.queryResult = [];
      this.previewDataGrid.gridOptions.api.hideOverlay();
      this.alertService.error(error, true);
    });
  }

  public saveForm(): void {
    this.submitted = true;
    const dataExtractionConfigurationDetails = this.transformToEntity(STATUS.Configured);
    this.datasetExtractionService.saveDataExtractionConfiguration(dataExtractionConfigurationDetails)
      .subscribe(
        (data) => {
          this.enableSaveButton();
          this.datasetExtractionService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
          this.navigateToList();
        }, (err) => {
          this.alertService.error(err.error.errorList.length ? err.error.errorList[0] : "", true);
          this.datasetExtractionService.showAfterIntMessage({ text: err.error.errorList.length ? err.error.errorList[0] : "", type: "error", toStable: true });
          this.submitted = false;
          this.enableSaveButton();
        });
  }

  public submitForm(): void {
    this.submitted = true;
    const dataExtractionConfigurationDetails = this.transformToEntity(STATUS.Submitted);
    this.datasetExtractionService.submitDataExtractionConfiguration(dataExtractionConfigurationDetails)
      .subscribe(
        (data) => {
          this.datasetExtractionService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
        }, (err) => {
          this.submitted = false;
          this.alertService.error(err.error.errorList.length ? err.error.errorList[0] : "", true);
        }, () => {
          this.navigateToList()
        });
  }

  public transformToEntity(status: string): any {
    let sqlEditorQuery;
    sqlEditorQuery = JSON.stringify({
      SelectedColumnsString: this.SelectedColumnsString,
      sqlFilterCondition: this.datasetExtractionForm.value.sqlFilterCondition
    });
    this.datasetExtractionForm.patchValue({
      sqlEditorQuery: sqlEditorQuery
    });
    const dataExtractionConfigurationDetails = {
      dataExtractHeader: {
        id: GuidHelper.isValid(this.datasetExtractionForm.value.id) ? this.datasetExtractionForm.value.id : undefined,
        name: this.datasetExtractionForm.value.name,
        originalDatasetId: this.datasetExtractionForm.value.originalDataset.id
      },
      dataExtractDetail: {
        datasetName: this.datasetExtractionForm.value.originalDataset.name,
        percentageOfRows: this.datasetExtractionForm.value.extractedRowsPercent,
        numberOfRows: this.datasetExtractionForm.value.extractedRowsCount,
        sQLQuery: this.datasetExtractionForm.value.sqlEditorQuery,
      },
      dataExtractColumns: this.getDataExtractColumns(),
      status: status
    }
    return dataExtractionConfigurationDetails;
  }

  public getDataExtractColumns(): any[] {
    if (this.nonSelectedColumns.length) {
      this.nonSelectedColumns.forEach(e => e.isSelectedColumn = false);
      this.currentSetOfColumns.forEach(e => e.isSelectedColumn = true);
      this.currentSetOfColumns = [...this.nonSelectedColumns, ...this.currentSetOfColumns];
    } else {
      this.currentSetOfColumns.forEach(e => e.isSelectedColumn = true);
    }
    return this.currentSetOfColumns;
  }

  public approveDataSetConfig(): void {
    const dataExtractionConfigurationDetails = this.transformToEntity(STATUS.Approved);
    this.reviewDataExtractionConfiguration(dataExtractionConfigurationDetails, STATUS.Approved);
  }

  public rejectDataSetConfig(): void {
    this.ngxSmartModalService.getModal(NgxSmartModelNames.RejectConfirmModal).open();
  }

  public confirmRejectDataSetConfig(): void {
    const dataExtractionConfigurationDetails = this.transformToEntity(STATUS.Rejected);
    this.reviewDataExtractionConfiguration(dataExtractionConfigurationDetails, STATUS.Rejected);
  }

  public enableSaveButton(): boolean {
    if (this.datasetExtractionForm.valid) {
      if ((this.datasetExtractionForm.dirty || this.isColumnSelectionChanged) && !this.submitted) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  public enableSubmitButton(): boolean {
    if (this.modalType == ModalType.Create) {
      if (this.datasetExtractionForm.value.extractionType && (this.datasetExtractionForm.value.extractedRowsPercent || this.datasetExtractionForm.value.extractedRowsCount) && !this.errorForExtractionType() && this.enableSaveButton()) {
        return false
      } else {
        return true;
      }
    } else {
      if (this.datasetExtractionForm.value.extractionType && (this.datasetExtractionForm.value.extractedRowsPercent || this.datasetExtractionForm.value.extractedRowsCount) && !this.errorForExtractionType()) {
        return false
      } else {
        return true;
      }
    }
  }

  public cancelChanges(): void {
    if (this.datasetExtractionForm.dirty || this.isColumnSelectionChanged) {
      this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
      this.ngxSmartModalService.getModal("cancelConfirmModal").open();
    } else if (this.detailEdit) {
      this.router.navigate([`dataset-extraction/${this.dataSetConfigId}/detail`]);
      this.detailEdit = false;
    } else {
      this.navigateToList()
    }
  }

  public confirmCancel(): void {
    if (this.detailEdit) {
      this.router.navigate([`dataset-extraction/${this.dataSetConfigId}/detail`]);
      this.detailEdit = false;
    } else {
      this.navigateToList()
    }
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }

  public navigateToList(): void {
    this.router.navigate(["dataset-extraction/list"]).then().catch();
  }

  public modifyQuery(): void {
    this.currentSetOfColumns = this.datasetColumnsGrid.gridApi.getSelectedRows();
    this.nonSelectedColumns = _.difference([...this.datasetColumns], this.currentSetOfColumns);
    if (this.currentSetOfColumns.length) {
      this.selectedColumns = _.pluck(this.currentSetOfColumns, "name");
      this.SelectedColumnsString = this.selectedColumns.join(", ")
    }
    this.checkColumnSelectionChanged();
  }

  public showAllColumns(): void {
    const allRows = this.currentSetOfColumns.concat(this.nonSelectedColumns);
    this.datasetColumnsGrid.gridOptions.api.setRowData(allRows);
    this.datasetColumnsGrid.gridOptions.api.forEachNode(node => {
      _.each(this.currentSetOfColumns, column => {
        if (column.id === node.id) {
          node.setSelected(true);
        }
      });
    });
  }

  public showSelectedColumns(): void {
    this.datasetColumnsGrid.gridOptions.api.setRowData(this.currentSetOfColumns);
    this.datasetColumnsGrid.gridOptions.api.forEachNode(node => {
      node.setSelected(true);
    });
  }

  public showNonSelectedColumns(): void {
    this.datasetColumnsGrid.gridOptions.api.setRowData(this.nonSelectedColumns);
  }

  public omitSpecialChar(event: any): boolean {
    let k = event.charCode;  //         k = event.keyCode;  (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
  }

  public errorFor(propertyName: string): string | void {
    if (propertyName) {
      const property = propertyName.replace(/([A-Z])/g, ' $1').replace(/^./, (str) => str.toUpperCase());

      if (this.datasetExtractionForm.controls[propertyName]) {
        if (this.datasetExtractionForm.controls[propertyName].hasError("required")) {
          return property.concat(ValidationMessages.RequiredMessage);
        } else if (this.datasetExtractionForm.controls[propertyName].hasError("minlength")
          || this.datasetExtractionForm.controls[propertyName].hasError("maxlength")) {
          return property.concat(ValidationMessages.MinMaxLengthMessage);
        } else {
          return null;
        }
      }
    }
  }

  private getDatasetColumnsAndRowCount(datasetInfo: any): void {
    this.getColumns(datasetInfo);
    this.getRowsCount(datasetInfo);
  }

  private getColumns(datasetInfo: any): void {
    this.datasetExtractionService.getDatasetColumns(datasetInfo.id).subscribe(data => {
      if (data.isSuccess) {
        this.datasetColumns = data.data;
        this.currentSetOfColumns = [...this.datasetColumns];
        this.datasetColumnsGrid.gridOptions.api.setRowData(this.currentSetOfColumns);
        this.selectAllColumns();
        this.selectedColumns = _.pluck(this.currentSetOfColumns, "name");
        this.SelectedColumnsString = this.selectedColumns.join(", ");
        this.datasetTableName = datasetInfo.name;
      } else {
        this.datasetColumns = [];
        this.datasetColumnsGrid.gridOptions.api.hideOverlay();
        this.alertService.error(ValidationMessages.Column_Empty, true);
      }
    }, error => {
      this.datasetColumns = [];
      this.datasetColumnsGrid.gridOptions.api.hideOverlay();
      this.alertService.error(error, true);
    });
  }

  private getRowsCount(datasetInfo: any): void {
    this.datasetExtractionService.getDatasetRowsCount(datasetInfo.id).subscribe(data => {
      if (data.isSuccess) {
        this.datasetExtractionForm.patchValue({
          totalRows: data.data
        });
      } else {
        this.alertService.error(ValidationMessages.Rows_Empty, true);
      }
    }, error => {
      this.alertService.error(error, true);
    });
  }

  private reviewDataExtractionConfiguration(dataExtractionConfigurationDetails: any, status: string): void {
    this.isReviewButtonDisabled = true;
    dataExtractionConfigurationDetails.actionComments = this.comments;
    dataExtractionConfigurationDetails.status = status;
    this.ngxSmartModalService.getModal(NgxSmartModelNames.RejectConfirmModal).close();
    this.datasetExtractionService.reviewDataExtractionConfiguration(dataExtractionConfigurationDetails)
      .subscribe(data => {
        if (data && data.isSuccess) {
          this.datasetExtractionService.showAfterIntMessage({ text: `${ValidationMessages.ReviewMessage}${status}`, type: "success", toStable: true });
          this.navigateToList();
        } else {
          this.alertService.error(data.errorList.length ? data.errorList[0] : "", true);
        }
      }, (err) => {
        this.alertService.error(err.errorList.length ? err.errorList[0] : "", true);
      });
  }

  private selectAllColumns(): void {
    this.datasetColumnsGrid.gridApi.selectAll();
  }

  private initDatasetExtractionDetailForm(): void {
    this.datasetExtractionForm = this.fb.group({
      id: [""],
      name: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      originalDataset: ["", [Validators.required]],
      sqlEditorQuery: [""],
      sqlFilterCondition: [""],
      totalRows: [0, [Validators.min(1)]],
      extractionType: [""],
      extractedRowsPercent: [""],
      extractedRowsCount: [""]
    });
  }

  private initializeDatasetGridColDef(): void {
    this.datasetColumnsGrid.gridOptionRowSelectionToMultiple();
    if (this.modalType === ModalType.Create || this.modalType === ModalType.Edit) {
      const select = {
        headerName: 'Select',
        checkboxSelection: true,
        headerCheckboxSelection: true,
        isClickable: false
      };
      this.datasetColumnsGrid.columnDefs.push(select);
    }

    const columnName = {
      headerName: 'Column Name',
      field: 'name',
      editable: false,
      sortable: true
    };
    this.datasetColumnsGrid.columnDefs.push(columnName);

    const dataType = {
      headerName: 'Data Type',
      field: 'dataType',
      editable: false,
      sortable: true
    };
    this.datasetColumnsGrid.columnDefs.push(dataType);
    this.datasetColumnsGrid.defaultSortModel = [
      {
        colId: 'name',
        sort: 'asc',
      }
    ];
  }

  private errorForExtractionType(): string | void {
    switch (this.datasetExtractionForm.value.extractionType) {
      case extractionType.percentage: {
        this.datasetExtractionForm.patchValue({
          extractedRowsCount: ""
        });
        this.validationError.extractedRowsCountError = "";
        if (this.datasetExtractionForm.value.extractedRowsPercent > 100 || this.datasetExtractionForm.value.extractedRowsPercent < 1) {
          this.validationError.extractedRowsPercentError = ValidationMessages.ExtractedRowsPercentErrorMessage;
        } else {
          this.validationError.extractedRowsPercentError = null;
        }
        return this.validationError.extractedRowsPercentError;
      }
      case extractionType.rowsCount: {
        this.datasetExtractionForm.patchValue({
          extractedRowsPercent: ""
        });
        this.validationError.extractedRowsPercentError = "";
        if (this.datasetExtractionForm.value.extractedRowsCount > this.datasetExtractionForm.value.totalRows) {
          let errorMessage = ValidationMessages.ExtractedRowsCountErrorMessage.replace("{currentRowCount}", this.datasetExtractionForm.value.extractedRowsCount);
          errorMessage = errorMessage.replace("{rowCount}", this.datasetExtractionForm.value.totalRows);
          this.validationError.extractedRowsCountError = errorMessage;
        } else {
          this.validationError.extractedRowsCountError = null;
        }
        return this.validationError.extractedRowsCountError;
      }
      default:
        return null;
    }
  }

  private getExtractionJobStatus(data: any): void {
    if (data.dataExtractDetails.length) {
      const dataExtractDetails = data.dataExtractDetails.length ? data.dataExtractDetails[0] : {};
      if (dataExtractDetails.dataExtractProcesses.length) {
        const processDetail = dataExtractDetails.dataExtractProcesses[0];
        this.extractionJobStatus = processDetail.processingStatus.name;
        this.extractionJobDate = moment(processDetail.lastModifiedDate).format(config.APPLICATION_DATE_TIME_FORMAT);
        if (this.extractionJobStatus === EXTRACTION_JOB_STATUS.Completed && (data.currentStatus && data.currentStatus.name && data.currentStatus.name !== STATUS.Rejected)) {
          this.isExtractionJobCompleted = true;
          if (this.modalType === ModalType.Approve || this.modalType === ModalType.Rejected) {
            this.enableApprovalControls = true;
          }
        }
        if (this.extractionJobStatus === EXTRACTION_JOB_STATUS.Failed) {
          const sortedArray = _.sortBy(processDetail.dataExtractProcessLogs, this.failureSortKey);
          const failureObj = sortedArray[sortedArray.length - 1];
          this.failureDetails = JSON.parse(failureObj.logInfo);
        }
      }
    }
  }

  private previewDataGridHeaders(firstRowData: any): void {
    const headers = [];
    for (let field in firstRowData) {
      var headerObject = {
        headerName: field,
        field: field,
        isClickable: false,
        minWidth: 100,
        resizable: true
      }
      headers.push(headerObject);
    }
    this.previewDataGrid.gridOptions.api.sizeColumnsToFit();
    this.previewDataGrid.setHeader(headers, false);
  }

  private checkColumnSelectionChanged(): void {
    const pristineColumnsLength = this.pristineSelectedColumnString.length;
    const selectedColumnsLength = this.SelectedColumnsString.length;
    if (pristineColumnsLength !== selectedColumnsLength) {
      this.isColumnSelectionChanged = true;
    } else {
      this.isColumnSelectionChanged = false;
    }
  }

  public onPopUpClose(modalType: string): void {
    this.ngxSmartModalService.getModal(modalType).close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }



}
