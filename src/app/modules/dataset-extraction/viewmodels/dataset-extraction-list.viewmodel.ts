import { from as observableFrom, Observable, Subject, forkJoin } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridExtendedViewModel } from "../../core-ui-components";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AuthService } from "src/app/utils/auth.service";
import { DatasetExtractionViewModel } from "./dataset-extraction.viewmodel";
import { DatasetExtractionService } from "../services/dataset-extraction.service";
import moment from "moment";
import Highcharts from 'highcharts';
import { STATUS, EXTRACTION_JOB_STATUS } from "../helpers/constants";
import { CHART_COLORS, ChartOptions } from "../helpers/chart-helper";

@Injectable()
export class DatasetExtractionListViewModel extends DatasetExtractionViewModel {
  public userId: string;
  public datasetExtractionList = [];
  public alertService: AlertService;
  public viewModelDataGrid: DataGridExtendedViewModel;
  public appliedFilters: any[];
  public userRolePermission: any = {};
  public highcharts = Highcharts;
  public chartOptions;
  public comments: string;
  public enableClearFilterButton: boolean = false;
  public statusForFilter: string;
  private datasetExtractionId: string;
  private groupDataKeyForChart: string = "status";
  private keywordJob = "Data Extraction Job";
  private deleteConfirmModalFromList: string = "deleteConfirmModalFromList";
  private commentsPopUp: string = "commentsPopUp";


  public constructor(
    public datasetExtractionService: DatasetExtractionService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public authService: AuthService) {
    super(datasetExtractionService, ViewModelType.Edit, ngxSmartModalService, route, authService);
  }

  public init(params: any): Observable<any> {
    this.userId = params.userId;
    this.statusForFilter = params.status ? params.status : "";
    this.userRolePermission = this.getUserClaims() || {};
    this.viewModelDataGrid = new DataGridExtendedViewModel();
    this.initializeGridColDef();
    this.viewModelDataGrid.clickableColumnIndices = [1];
    this.subscribeToCreateSuccessMessage();
    this.subscribers();
    this.getDatasetExtractionList();
    return observableFrom([]);
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  private subscribers() {
    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.columnClicked:
          this.redirectToDetail(e.eventData.rowDetails.id);
          break;
        case GridConfig.editClicked:
          this.redirectToEditFromList(e.eventData.rowDetails.id);
          break;
        case GridConfig.deleteClicked:
          this.openDeleteConfirmationPopUp(e.eventData.rowDetails.id);
          break;
        case GridConfig.approveRejectClicked:
          this.redirectToApproveRejectView(e.eventData.rowDetails.id);
          break;
        case GridConfig.viewCommentsClicked:
          this.openCommentsPopUp(e.eventData.rowDetails.comments);
          break;
        default:
          break;
      }
    }, error => {
      this.alertService.error(error.message, true);
    });
  }

  public openDeleteConfirmationPopUp(id: string): void {
    this.datasetExtractionId = id;
    this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
    this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).open();
  }

  public openCommentsPopUp(comments: string): void {
    this.comments = comments;
    this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
    this.ngxSmartModalService.getModal(this.commentsPopUp).open();
  }

  public redirectToApproveRejectView(id: string): void {
    this.datasetExtractionId = id;
    this.datasetExtractionService.datasetExtractionIdSubject.next(id);
    this.router.navigate([`dataset-extraction/${this.datasetExtractionId}/Approve`])
  }

  public redirectToDetail(datasetExtractionId): void {
    this.datasetExtractionId = datasetExtractionId;
    this.datasetExtractionService.datasetExtractionIdSubject.next(datasetExtractionId);
    this.router.navigate([`dataset-extraction/${datasetExtractionId}/Detail`]).then().catch();
  }

  public redirectToEditFromList(datasetExtractionId): void {
    this.datasetExtractionId = datasetExtractionId;
    this.datasetExtractionService.datasetExtractionIdSubject.next(datasetExtractionId);
    this.router.navigate([`dataset-extraction/${datasetExtractionId}/Edit`]).then().catch();
  }

  public deleteDataExtractionConfiguration(): void {
    this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).close();
    this.datasetExtractionService.deleteDataExtractionConfiguration(this.datasetExtractionId)
      .subscribe(data => {
        if (data && data.isSuccess) {
          this.alertService.success(data.message, true);
        } else {
          this.alertService.error(data.errorList, true);
        }
        this.getDatasetExtractionList();
      }, error => {
        this.alertService.error(error.message, true);
      });
  }

  public clearFilter(): void {
    this.statusForFilter = "";
    this.enableClearFilterButton = false;
    this.getDatasetExtractionList();
  }

  public getDatasetExtractionList(): void {
    this.datasetExtractionService.getDatasetExtractionList().subscribe(data => {
      if (data) {
        let unfilteredList = [];
        _.each(data, item => {
          const obj: any = {
            id: item.id,
            name: item.name,
            originalDataset: item.originalDataset,
            createdBy: item.createdBy,
            createdByName: item.createdByName,
            createdDate: item.createdDate,
            status: item.status
          }
          if (item.dataExtractDetails.length) {
            const dataExtractDetail = item.dataExtractDetails[0];
            if (dataExtractDetail.dataExtractProcesses.length) {
              const dataExtractProcess = dataExtractDetail.dataExtractProcesses[0];
              obj.jobStatus = dataExtractProcess.processingStatus.name;
            }
          }
          if (item.dataExtractHeaderStatusAudits.length) {
            const statusObj = item.dataExtractHeaderStatusAudits[0];
            obj.comments = statusObj.actionComments;
            obj.approvedDate = statusObj.actionDate;
            if (statusObj.actionByNavigation) {
              const user = statusObj.actionByNavigation;
              obj.approverName = `${user.firstName} ${user.lastName}`
            }
          }
          if (item.createdBy === this.userId && obj.jobStatus) {
            switch (obj.jobStatus) {
              case EXTRACTION_JOB_STATUS.Completed:
              case EXTRACTION_JOB_STATUS.Canceled:
              case EXTRACTION_JOB_STATUS.Failed:
              case EXTRACTION_JOB_STATUS.Inprogress:
                obj.status = `${obj.jobStatus} ${this.keywordJob}`;
                break;
              default:
                obj.status = obj.jobStatus;
                break;
            }
          }
          switch (item.status) {
            case STATUS.Configured:
              if (item.createdBy === this.userId) {
                obj.action = STATUS.Configured;
              }
              break;
            case EXTRACTION_JOB_STATUS.Completed:
            case STATUS.Submitted:
              if (this.userRolePermission?.canExecute && item.createdBy !== this.userId &&
                obj.jobStatus === EXTRACTION_JOB_STATUS.Completed) {
                obj.action = STATUS.Submitted;
              }
              break;
            case STATUS.Approved:
              obj.status = item.status;
              obj.action = STATUS.Approved;
              break;
            case STATUS.Rejected:
              obj.status = item.status;
              obj.action = STATUS.Rejected;
              break;
            default:
              obj.action = undefined;
              break;
          }
          unfilteredList.push(obj);
        });
        this.datasetExtractionList = this.filterList(unfilteredList);
        this.viewModelDataGrid.setRowsData(this.datasetExtractionList);
        this.bindChartData(this.datasetExtractionList);
      }
    }, error => {
      this.alertService.error(error.message, true);
    });
  }

  public subscribeToCreateSuccessMessage(): void {
    this.datasetExtractionService.showSuccessMessage.subscribe(data => {
      const { message, toastable } = data;
      if (!data.isCreateAnother) {
        this.alertService.success(message, toastable);
      }
    });
  }

  public navigateToCreate(): void {
    this.router.navigate(["/dataset-extraction/create"]).then().catch();
  }

  public initializeGridColDef(): void {
    const name = {
      headerName: 'Name',
      field: 'name',
      editable: false,
      sortable: true,
      filter: true,

      cellRenderer: (params: any) => {
        return `<u class="hyperlink" data-action-type="columnClicked">` + params.value + `</u>`;
      }
    };
    this.viewModelDataGrid.columnDefs.push(name);

    const originalDataset = {
      headerName: 'Original Dataset',
      field: 'originalDataset',
      editable: false,
      sortable: true,
      filter: true,
    };
    this.viewModelDataGrid.columnDefs.push(originalDataset);

    const createdBy = {
      headerName: 'Created By',
      field: 'createdByName',
      sortable: true,
      editable: false
    };
    this.viewModelDataGrid.columnDefs.push(createdBy);

    const createdDate = {
      headerName: 'Created On',
      field: 'createdDate',
      editable: false,
      sortable: true,
      isDate: true,
      cellRenderer: (params: any) => {
        return moment(params.value).format(config.APPLICATION_DATE_TIME_FORMAT)
      }
    };
    this.viewModelDataGrid.columnDefs.push(createdDate);

    const status = {
      headerName: 'Status',
      field: 'status',
      editable: false,
      sortable: true,
      filter: true,
    };
    this.viewModelDataGrid.columnDefs.push(status);

    const approverName = {
      headerName: 'Action By',
      field: 'approverName',
      sortable: true,
      editable: false
    };
    this.viewModelDataGrid.columnDefs.push(approverName);

    const approvedDate = {
      headerName: 'Action Date',
      field: 'approvedDate',
      editable: false,
      isDate: true,
      sortable: true,
      filter: true,
      sort: 'desc',
      comparator: this.viewModelDataGrid.customDateSort,
      cellRenderer: (params: any) => {
        return params.value ? moment(params.value).format(config.APPLICATION_DATE_TIME_FORMAT) : ""
      }
    };
    this.viewModelDataGrid.columnDefs.push(approvedDate);

    const action = {
      headerName: 'Action',
      field: 'action',
      editable: false,
      cellRenderer: (params: any) => {
        let template = ``;
        let icon = ``;
        let deleteIcon = ``;
        switch (params.value) {
          case STATUS.Configured:
            icon = `<a routerLink="" class="data-grid-padding-right" style = "cursor:pointer;"><img alt="Edit" title="Edit" src="../../../assets/images/edit.png"  class="data-grid-image"  data-action-type="editClicked"/></a>`;
            deleteIcon = `<a  routerLink="" class="data-grid-padding-right" style = "cursor:pointer;"><img alt="Delete"  title="Delete"  src="../../../assets/images/trash.png" class="data-grid-image-small" data-action-type="deleteClicked"/></a>`;
            template += icon;
            template += deleteIcon;
            break;
          case STATUS.Submitted:
            template = `<a routerLink="" class="data-grid-padding-right"><img alt="Approve/Reject" title="Approve/Reject" src="../../../assets/images/icons8-clipboard-approve-32.png" class="data-grid-image-medium" data-action-type="approveRejectClicked"/></a>`;
            break;
          case STATUS.Approved:
          case STATUS.Rejected:
            template = `<i title="View Comments" class="icon material-icons cursor text-primary" data-action-type="viewCommentsClicked">visibility</i>`;
            break;
          default:
            break;
        }
        return template;
      }
    };
    this.viewModelDataGrid.columnDefs.push(action);
  }

  private bindChartData(listData: any): void {
    let groupedData = _.groupBy(listData, this.groupDataKeyForChart);
    const chartData = [];
    _.each(groupedData, (value, key) => {
      let objChartData: any = {};
      objChartData.name = key;
      objChartData.y = value.length;
      switch (key) {
        case STATUS.Configured:
          objChartData.color = CHART_COLORS.Configured;
          break;
        case STATUS.Submitted:
          objChartData.color = CHART_COLORS.Submitted;
          break;
        case STATUS.Approved:
          objChartData.color = CHART_COLORS.Approved;
          break;
        case STATUS.Rejected:
          objChartData.color = CHART_COLORS.Rejected;
          break;
        case `${EXTRACTION_JOB_STATUS.Canceled} ${this.keywordJob}`:
        case `${EXTRACTION_JOB_STATUS.Failed} ${this.keywordJob}`:
          objChartData.color = CHART_COLORS.Failed;
          break;
        case `${EXTRACTION_JOB_STATUS.Completed} ${this.keywordJob}`:
          objChartData.color = CHART_COLORS.Completed;
          break;
        default:
          objChartData.color = CHART_COLORS.Inprogress;
          break;
      }
      chartData.push(objChartData);
    });
    this.chartOptions = { ...ChartOptions };
    this.chartOptions.series = [{ data: chartData, showInLegend: false }];
  }

  private filterList(data: any[]): any[] {
    const filteredList = [];
    if (data && data.length) {
      data.forEach((dataItem: any) => {
        if (this.userRolePermission.canExecute) {
          if (dataItem.status === STATUS.Configured) {
            if (dataItem.createdBy === this.userId) {
              filteredList.push(dataItem);
            }
          } else if (dataItem.status === STATUS.Submitted && dataItem.createdBy !== this.userId) {
            if (dataItem.jobStatus === EXTRACTION_JOB_STATUS.Completed) {
              filteredList.push(dataItem);
            }
          }
          else {
            filteredList.push(dataItem);
          }
        } else {
          if (dataItem.createdBy === this.userId) {
            filteredList.push(dataItem);
          }
        }
      });
    }
    if (this.statusForFilter && this.statusForFilter !== "") {
      this.enableClearFilterButton = true;
      let filteredListStatus;
      if (this.statusForFilter === EXTRACTION_JOB_STATUS.Failed) {
        filteredListStatus = filteredList.filter(e => e.jobStatus === this.statusForFilter);
      } else {
        filteredListStatus = filteredList.filter(e => e.status === this.statusForFilter);
      }
      return filteredListStatus;
    } else {
      return filteredList;
    }
  }

  public onPopUpClose(modal: string): void {
    this.ngxSmartModalService.getModal(modal).close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }

}
