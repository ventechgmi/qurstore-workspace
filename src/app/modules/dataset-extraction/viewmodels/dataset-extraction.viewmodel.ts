import { map } from "rxjs/operators";
import { EntityBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { Observable, Subject, of } from "rxjs";
import { AbstractControl, ValidatorFn } from "@angular/forms";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ROUTES_CONFIG_MAP, MODULE_BASE_URL_MODULE_ID_MAP } from "src/app/utils/config";
import { ActivatedRoute } from "@angular/router";
import { AuthService } from 'src/app/utils/auth.service';
import { DatasetExtractionService } from "../services/dataset-extraction.service";

export class DatasetExtractionViewModel extends EntityBaseViewModel<"", DatasetExtractionService> {
    public entityName: string;
    public moduleId: string;
    public moduleAccess;
    public moduleInfo;

    public constructor(
        public datasetExtractionService: DatasetExtractionService,
        public viewModelType: ViewModelType,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute,
        public authService: AuthService) {
        super(datasetExtractionService, viewModelType);
        this.entityName = "datasetExtraction";
    }

    public create(): Observable<any> {
        return super.create().pipe(map(data => data));
    }

    public getUserClaims(): any {
        const baseUrl = ROUTES_CONFIG_MAP.DATASET_EXTRACTION.BASE_URL;
        this.moduleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
        this.moduleInfo = this.authService.getModuleAccess(this.moduleId) || null;
        this.moduleAccess = this.moduleInfo ? this.moduleInfo.RolePermissions : {};
        return this.moduleAccess;
    }

    /*
    This method validates the below
    1. validates the control has only white spaces since most of the name fields should not allow just white spaces
    2. validates the control whether it has only special characters.
    */
    public validateSpecialCharactersAndWhiteSpace(lengthMin: number): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            const pattern = "[A-Za-z0-9]";
            const whiteSpace = /^\s*$/;
            if ((control.value || "").length === 0) {
                return null;
            } else if ((control.value || "").trim().length < lengthMin) {
                return { minlength: true };
            } else if (control.value.match(whiteSpace)) {
                return { hasOnlyWhiteSpace: true };
            } else if (!control.value.match(pattern)) {
                return { hasOnlySpecialCharacters: true };
            }
        };
    }
}
