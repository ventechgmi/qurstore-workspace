import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DataGridModule } from "../core-ui-components";
import { DataGridExtendedModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldControl } from "@angular/material/form-field";
import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";
import { DatasetExtractionRoutingModule } from "./dataset-extraction-routing-module";
import { DatasetExtractionService } from "./services/dataset-extraction.service";
import { DatasetExtractionListComponent } from "./views/dataset-extraction-list/dataset-extraction-list.component";
import { DatasetExtractionDetailComponent } from "./views/dataset-extraction-detail/dataset-extraction-detail.component";
import { MatSelectModule } from "@angular/material/select";
import { MatInputModule } from "@angular/material/input";
import { LocalStorageManager } from "../../utils/local-storage-manager";
import { HighchartsChartModule } from 'highcharts-angular';

const modules = [MatSelectModule];
@NgModule(
    {
        declarations: [
            DatasetExtractionListComponent,
            DatasetExtractionDetailComponent
        ],
        imports: [
            SharedModules,
            NgSelectModule,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            DatasetExtractionRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatSelectModule,
            MatButtonModule,
            DataGridModule,
            DataGridExtendedModule,
            MatCheckboxModule,
            HighchartsChartModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [
            DatasetExtractionListComponent,
            DatasetExtractionDetailComponent
        ],
        providers: [
            AlertService,
            DatasetExtractionService,
            NgxSmartModalService,
            LocalStorageManager
        ]
    }
)
export class DatasetExtractionModule {

}
