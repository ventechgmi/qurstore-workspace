import { NgModule } from "@angular/core";
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { DataGridModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";

import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatButtonModule } from "@angular/material/button";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';

import { AuthenticationService } from "./services/authentication.service";
import { LoginComponent } from "./views/authentication/login/login.component";
import { LoginVerificationComponent } from "./views/authentication/login-verification/login-verification.component";
import { ChangePasswordComponent } from "./views/authentication/changepassword/changepassword.component";
import { AuthenticationRoutingModule } from "./authentication-routing-module";
import { SetPasswordComponent } from './views/authentication/set-password/set-password.component';

import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";
import { from } from 'rxjs';
import { IndexComponent } from './views/authentication/index/index.component';
import { ForgotPasswordComponent } from './views/authentication/forgot-password/forgot-password.component';

@NgModule(
    {
        declarations: [
            LoginComponent,
            LoginVerificationComponent,
            ChangePasswordComponent,
            SetPasswordComponent,
            IndexComponent,
            ForgotPasswordComponent
        ],
        imports: [
            SharedModules,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            AuthenticationRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatButtonModule,
            DataGridModule,
            MatCheckboxModule,
            MatSlideToggleModule,
            NgxSmartModalModule.forRoot(),
            // BrowserAnimationsModule,
            MatProgressBarModule,
            MatPasswordStrengthModule
        ],
        exports: [
            LoginComponent,
            LoginVerificationComponent,
            ChangePasswordComponent
        ],
        providers: [
            AlertService,
            AuthenticationService,
            NgxSmartModalService
        ]
    }
)
export class AuthenticationModule { }
