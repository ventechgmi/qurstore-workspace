export class ValidationMessages {
    public static readonly somethingWentWrong = "Something went wrong. Please try again.";
    public static readonly RequiredMessage = " is required.";
    public static readonly VerificationCodeLengthValidationMessage = "Enter your 6 digit verification code.";
    public static readonly InvalidMessage = " is invalid.";
    public static readonly NotMatchMessage = " must be matched with Password.";
    public static readonly PasswordCriteriaMessage = " must contain atleast one uppercase, one lowercase, one number and one special character."
    public static readonly MinMaxLengthMessage = " should be between 8 and 20 characters.";
}
