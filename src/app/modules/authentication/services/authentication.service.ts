
import { map } from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { EntityService, EntityQuery, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
import { of as observableOf } from "rxjs";
import * as util from "util";
import { AuthService } from "../../../utils/auth.service";
import { environment } from "src/environments/environment";
@Injectable({
    providedIn: "root"
})
export class AuthenticationService extends EntityService {
    appConfig: AppConfig;
    onUpdateListEvent = new EventEmitter();
    public showSuccessMessage = new Subject<any>();
    public alertInfo = { message: "", type: "" };
    private readonly KEY_AUTHORIZATION: string = "Authorization";
    private readonly KEY_BEARER: string = "Bearer";

    // this subscription will help to show error or success message in another screen(details screen delete success message showing in list screen)
    private afterInitSubject = new BehaviorSubject(this.alertInfo);

    constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig, private authService: AuthService) {
        super(http, "auth", appConfig);
        this.appConfig = appConfig;
    }

    private getAPIURL() {
        return this.getApi();
    }

    public showAfterIntMessage(messageInfo) {
        this.afterInitSubject.next(messageInfo);
    }

    public getAfterIntObs(): Observable<any> {
        return this.afterInitSubject.asObservable();
    }

    public getShowSuccessMessage() {
        return this.showSuccessMessage.asObservable();
    }

    private generateGetAPI(name, req): Observable<any> {
        return this.http.get(this.getAPIURL() + name + req).pipe(
            map(res => {
                return res;
            }));
    }

    private generatePostAPI(name, data): Observable<any> {
        return this.http.post(this.getAPIURL() + name, data).pipe(
            map((res: Response) => res));
    }

    public setAuthToken(authToken) {
        localStorage.setItem("token", authToken);
    }

    public getAuthToken(): string {
        return localStorage.getItem("token");
    }

    public authenticate(loginCred): Observable<any> {
        return this.http
            .post(this.getAPIURL() + "/login", loginCred).pipe(
                map((res: any) => {
                    if (res.token) {
                        this.authService.setAuthToken(res.token);
                    }
                    return res;
                }));
    }

    public verifyOtp(verificationCode): Observable<any> {
        return this.http
            .get(this.getAPIURL() + "/verifyotp/" + verificationCode).pipe(
                map((res: any) => {
                    if (res.data && res.data.token) {
                        this.authService.setAuthToken(res.data.token);
                        this.authService.setUserNameInLocalStorage();
                    }
                    return res;
                }));
    }

    public resendOtp(): Observable<any> {
        return this.http
            .get(this.getAPIURL() + "/resendotp/").pipe(
                map((res: any) => {
                    return res;
                }));
    }

    public checkResetPasswordLinkIsValid(token): Observable<any> {
        var header = {
            headers: new HttpHeaders()
                .set(this.KEY_AUTHORIZATION,
                    util.format("%s %s", this.KEY_BEARER, token))
        }

        return this.http
            .get(this.getAPIURL() + "/check-reset-password-link-valid/", header).pipe(
                map((res: any) => {
                    return res;
                }));
    }

    public changePassword(model, token): Observable<any> {
        var header = {
            headers: new HttpHeaders()
                .set(this.KEY_AUTHORIZATION,
                    util.format("%s %s", this.KEY_BEARER, token))
        }

        return this.http.post(this.getAPIURL() + "/changepassword", model, header).pipe(
            map((res: any) => {
                return res;
            }));
    }

    public SSOLogin(): Observable<any> {
        return this.http
            .get(environment.IdentityServerAPI + "/auth/sso-login").pipe(
                map((res: any) => {
                    this.authService.setAuthToken(res.data.token);
                    return res;
                }));
    }

    public forgotPassword(email): Observable<any> {
        return this.http
            .post(this.getAPIURL() + "/forgot-password/", email).pipe(
                map((res: any) => {
                    return res;
                }));
    }

    private getRawApi(): string {
        return this.appConfig.apiEndPoint;
    }
    
    private getConfigurationServiceAPIURL(): any {
        return this.getRawApi() + "/configuration";        
    }

    public getOrganizationPropertyByName(organizationId: string, names: string): Observable<any>  {

        const endpoint = this.getConfigurationServiceAPIURL() + "/organization-property/list/" + organizationId +"/"+names;
        return this.http.get(endpoint).pipe(map(res => {
            return res;
        }));
    }
}
