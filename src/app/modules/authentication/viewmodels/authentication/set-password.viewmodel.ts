import { Router } from "@angular/router";
// import { AuthenticationService } from "../services/authentication.service";
import { FormBuilder, Validators, FormGroup, AbstractControl, ValidatorFn } from "@angular/forms";
// import { ROUTES_CONFIG_MAP } from "../../../utils/config";
import { AlertService } from "src/app/utils/alert/alert.service";

export class SetPasswordViewModel {
    currentUserEmail: any;
    errMessage: string;
    isSubmitted = false;
    isEmailExists = false;
    public isError = false;
    setPwdForm: any;
    validationMessage: boolean;
    public userDetails: any;
    public formMode: any;
  isDecrypted: any;
  userEmail: any;
    public constructor(
        private formBuilder: FormBuilder, private router: Router,
        private alertService: AlertService) {

        this.setPwdForm = this.formBuilder.group({
            oldPassword: ["", [Validators.required]],
            NewPassword: ["", [this.validatePassword(6, 20)]],
            ConfirmPassword: [""]
        });

    }


validatePassword(lengthMin: number, lengthMax: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        const password = control.value || "";
        if (password.trim().length === 0) {
            return { required: true };
        } else if (password.trim().length < lengthMin && password.trim().length !== 0) {
            return { minlength: true };
        } else if (password.trim().length > lengthMax) {
            return { maxlength: true };
        } else { return null; }


    };
}

}
