
import { map } from "rxjs/operators";
import { EntityBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { Observable, Subject ,  of } from "rxjs";
import { AbstractControl, ValidatorFn, FormGroup } from "@angular/forms";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ActivatedRoute } from "@angular/router";
import { ROUTES_CONFIG_MAP, MODULE_BASE_URL_MODULE_ID_MAP } from "src/app/utils/config";

import { AuthenticationService } from "../../services/authentication.service";

export class AuthenticationViewModel extends EntityBaseViewModel<"", AuthenticationService> {
    public moduleId: string;
    public moduleAccess;
    public moduleInfo;
    // public updateUserListSubject = new Subject<any>();
    // public userDetailSelection = new Subject<any>();

    public constructor(
        public authenticationService: AuthenticationService,
        public viewModelType: ViewModelType,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute)
    {
        super(authenticationService, viewModelType);
        this.entityName = "authentication";
    }

    public noWhitespaceValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            const isWhitespace = (control.value || "").trim().length === 0;
            const isValid = !isWhitespace;
            return isValid ? null : { hasOnlyWhiteSpace: true };
        };
    }

    /*
    This method validates the below
    1. validates the control has only white spaces since most of the name fields should not allow just white spaces
    2. validates the control whether it has only special characters.

    */
    public validateSpecialCharactersandWhiteSpace(lengthMin: number): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            const pattern = "[A-Za-z0-9]";
            const whiteSpace = /^\s*$/;
            if ((control.value || "").length === 0) {
                return null;
            } else if ((control.value || "").trim().length < lengthMin) {
                return { minlength: true };
            } else if (control.value.match(whiteSpace)) {
                return { hasOnlyWhiteSpace: true };
            } else if (!control.value.match(pattern)) {
                // check if the input doest not have any alphabets or in other words input has only special characters
                // return "Don't enter only special characters";
                return { hasOnlySpecialCharaters: true };
            }
        };
    }

    // public confirmedValidator(matchingControlName: AbstractControl): ValidatorFn {
    //     return (control: AbstractControl): { [key: string]: any } => {
    //         if ((control.value || "").length === 0 || (matchingControlName.value || "").length === 0) {
    //             return null;
    //         } else if (control.value === matchingControlName.value) {
    //             return { matchedField: true };
    //         }
    //     };
    // }
}
