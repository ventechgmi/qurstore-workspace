
import { from as observableFrom, Observable } from "rxjs";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ViewModelType } from "@fastgen/ui-framework";
import _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import config from "../../../../utils/config";
import { AuthenticationService } from "../../services/authentication.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { AuthenticationViewModel } from "./authentication.viewmodel";
import { ValidationMessages } from "../../helpers/validation-messages";
import { AuthService } from 'src/app/utils/auth.service';

@Injectable()
export class LoginVerificationViewModel extends AuthenticationViewModel {
  public alertService: AlertService;

  loginVerificationForm: FormGroup;
  submitted = false;
  isFormValueChange = false;

  loginVerificationDetail: any = {
    verificationCode: ""
  };

  public constructor(
    public authenticationService: AuthenticationService,
    private router: Router,
    private fb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService,
    private chRef: ChangeDetectorRef,
    public route: ActivatedRoute,
    private authService: AuthService
  ) {
    super(authenticationService, ViewModelType.ReadOnly, ngxSmartModalService, route);
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  public init() {
    this.initLoginVerificationForm();
    this.initSubscribers();
    return observableFrom([]);
  }

  private initLoginVerificationForm() {
    this.loginVerificationForm = this.fb.group({
      verificationCode: ["", [Validators.required, Validators.minLength(6), Validators.maxLength(6)]],
    });
  }

  private get loginVerificationFormControls() {
    return this.loginVerificationForm.controls;
  }

  public get detailForm() {
    return this.loginVerificationForm.controls;
  }

  public saveChanges() {
    this.submitted = true;
    if (this.loginVerificationForm.invalid) { return; }
    this.loginVerificationForm.controls.verificationCode.setValue(this.loginVerificationForm.controls.verificationCode.value.trim());
    this.verifyOtp();
  }

  public verifyOtp() {
    const loginVerificationDetails = this.transformToEntityModel();
    this.authenticationService.verifyOtp(loginVerificationDetails.verificationCode)
      .subscribe(
        (data) => {
          this.submitted = false;
          if (data.isSuccess) {
            this.authService.getMenuItems();
            this.alertService.success(data.message, true);
            this.navigateToDashboard().then().catch();
          }
          else {
            this.authenticationService.showAfterIntMessage({ text: data.errorList[0], type: "error", toStable: true });
          }
        }, (err) => {
          this.submitted = false;
          this.alertService.error(err.error.errorList[0], true);
          this.authenticationService.showAfterIntMessage({ text: err.error.errorList[0], type: "error", toStable: true });
        });
  }

  public resendVerificationCode() {
    this.authenticationService.resendOtp()
      .subscribe(
        (data) => {
          this.submitted = false;
          if (data.isSuccess) {
            this.alertService.success(data.message, true);
            this.authenticationService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
          }
          else {
            this.alertService.error(data.errorList[0], true);
            this.authenticationService.showAfterIntMessage({ text: data.errorList[0], type: "error", toStable: true });
          }
        }, (err) => {
          this.submitted = false;
          this.alertService.error(err.error.errorList[0], true);
          this.authenticationService.showAfterIntMessage({ text: err.error.errorList[0], type: "error", toStable: true });
        });
  }

  public transformToEntityModel() {
    const {
      verificationCode
    } = this.loginVerificationFormControls;

    const loginVerificationDetailEntity: any = {
      verificationCode: verificationCode.value
    };

    return loginVerificationDetailEntity;
  }

  public isFormValueChanged() {
    const { verificationCode } = this.loginVerificationFormControls;

    const refLoginVerification: any = {
      verificationCode: verificationCode.value
    };
    return _.isEqual(refLoginVerification, this.loginVerificationDetail);
  }

  public onCancel() {
    this.authenticationService.showAfterIntMessage({ text: "" });
    this.navigateToLogin();
  }

  public navigateToSupplierDashboard() {
    return this.router.navigate(["supplier-landing/list"]);
  }

  public navigateToAdvamedDashboard() {
    return this.router.navigate(["reports/list"]);
  }

  public navigateToResearchDashboard() {
    return this.router.navigate(["workspace/list"]);
  }

  public navigateToEmployeeDashboard() {
    return this.router.navigate(["employee-dashboard/list"]);
  }

  public navigateToDashboard() {
    return this.router.navigate(["dashboard"]);
  }

  public navigateToLogin() {
    return this.router.navigate(["auth/login"]);
  }

  public initSubscribers() {
    this.loginVerificationForm.valueChanges.subscribe(() => {
      this.isFormValueChange = !this.isFormValueChanged();
    });
  }

  public errorFor(propertyName: string): string | void {
    if (propertyName) {
      const property = propertyName.replace(/\w+/g, (w) => w[0].toUpperCase() + w.slice(1, 12).toLowerCase() + ' ' + w.slice(12).toLowerCase());

      if (this.loginVerificationForm.controls[propertyName]) {
        if (this.loginVerificationForm.controls[propertyName].hasError("required")) {
          return property.concat(ValidationMessages.RequiredMessage);
        }
        else if (this.loginVerificationForm.controls[propertyName].hasError("minlength")
          || this.loginVerificationForm.controls[propertyName].hasError("maxlength")) {
          return ValidationMessages.VerificationCodeLengthValidationMessage;
        }
        else {
          return null;
        }
      }
    }
  }
}
