
import { from as observableFrom, Observable } from "rxjs";
import { QuestionableBooleanPipe } from "src/app/utils/questionable-boolean-pipe";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ViewModelType } from "@fastgen/ui-framework";
import _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";

import { AuthenticationService } from "../../services/authentication.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { AuthenticationViewModel } from "./authentication.viewmodel";
import { ValidationMessages } from "../../helpers/validation-messages";
import { AuthService } from 'src/app/utils/auth.service';
@Injectable()
export class ForgotPasswordViewModel extends AuthenticationViewModel {
    public alertService: AlertService;

    public forgotPasswordForm: FormGroup;
    submitted = false;
    isFormValueChange = false;
    emailObject: any = { email: "" };

    public constructor(
        public authenticationService: AuthenticationService,
        public router: Router,
        private fb: FormBuilder,
        public ngxSmartModalService: NgxSmartModalService,
        private chRef: ChangeDetectorRef,
        public route: ActivatedRoute,
        private authService: AuthService
    ) {
        super(authenticationService, ViewModelType.ReadOnly, ngxSmartModalService, route);
    }

    public initServices(alertService: AlertService) {
        this.alertService = alertService;
    }

    public init() {
        this.initLoginForm();
        this.initSubscribers();
        return observableFrom([]);
    }

    private initLoginForm() {
        this.forgotPasswordForm = this.fb.group({
            email: ["", [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]]
        });
    }

    private get forgotPasswordFormControls() {
        return this.forgotPasswordForm.controls;
    }



    public saveChanges() {
        this.submitted = true;
        if (this.forgotPasswordForm.invalid) { return; }
        this.forgotPasswordForm.controls.email.setValue(this.forgotPasswordForm.controls.email.value.trim());
        this.forgotPassword()
    }

    public forgotPassword() {
        const requestBody = { email: this.forgotPasswordFormControls.email.value };
        this.authenticationService.forgotPassword(requestBody)
            .subscribe(
                (data) => {
                    this.submitted = false;
                    if (data.isSuccess) {
                        this.forgotPasswordForm.reset();
                        this.authenticationService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
                        this.navigateToLogin()
                    }
                    else {
                        this.authenticationService.showAfterIntMessage({ text: data.errorList[0], type: "error", toStable: true });
                    }
                }, (err) => {
                    this.submitted = false;
                    this.authenticationService.showAfterIntMessage({ text: err.error.errorList[0], type: "error", toStable: true });
                }
            );
    }

    public onCancel() {
        this.navigateToLogin();
    }

    public navigateToLogin() {
        return this.router.navigate(["auth/login"]).then().catch();
    }

    public initSubscribers() {
        this.forgotPasswordForm.valueChanges.subscribe(() => {
            this.isFormValueChange = !this.isFormValueChanged();
        });
    }

    public isFormValueChanged() {
        const { email } = this.forgotPasswordFormControls;

        const refLogin: any = {
            email: email.value,
        };
        return _.isEqual(refLogin, this.emailObject);
    }

    public errorFor(propertyName: string): string | void {
        if (propertyName) {
            const property = propertyName.replace(/\w+/g, (w) => w[0].toUpperCase() + w.slice(1).toLowerCase());

            if (this.forgotPasswordForm.controls[propertyName]) {
                if (this.forgotPasswordForm.controls[propertyName].hasError("required")) {
                    return property.concat(ValidationMessages.RequiredMessage);
                }

                else if (this.forgotPasswordForm.controls[propertyName].hasError("email")) {
                    return property.concat(ValidationMessages.InvalidMessage);
                }
                else if (this.forgotPasswordForm.controls[propertyName].hasError("pattern")) {
                    return property.concat(ValidationMessages.InvalidMessage);
                }
                else {
                    return null;
                }
            }
        }
    }


}
