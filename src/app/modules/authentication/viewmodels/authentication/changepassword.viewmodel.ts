
import { from as observableFrom, Observable } from "rxjs";
import { QuestionableBooleanPipe } from "src/app/utils/questionable-boolean-pipe";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ViewModelType } from "@fastgen/ui-framework";
import _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";

import { AuthenticationService } from "../../services/authentication.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { AuthenticationViewModel } from "./authentication.viewmodel";
import { ValidationMessages } from "../../helpers/validation-messages";
import { ConfirmedValidator } from "../../../../utils/validators/confirmed.validator";
import { AuthService } from "../../../../utils/auth.service";
import config from "../../../../utils/config";


@Injectable()
export class ChangePasswordViewModel extends AuthenticationViewModel {
  public alertService: AlertService;

  changePasswordForm: FormGroup;
  submitted = false;
  public componentChangeDetection: ChangeDetectorRef;
  isFormValueChange = false;
  showPasswordDetails: boolean;
  token = "";
  passwordChangedSuccess: boolean = false;

  changePasswordDetail: any = {
    password: "",
    confirmPassword: ""
  };

  public resetPasswordLinkExpired: boolean;
  public constructor(
    public authenticationService: AuthenticationService,
    private authService: AuthService,
    public router: Router,
    private fb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService,
    private chRef: ChangeDetectorRef,
    public route: ActivatedRoute) {
    super(authenticationService, ViewModelType.ReadOnly, ngxSmartModalService, route);
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  public init(params?) {
    if (params.token) {
      this.token = params.token;
      this.checkResetPasswordLinkIsValid(params.token);
      // this.authService.setAuthToken(params.token); // commented this line as it is replacing the token in local storage and facing authentication issues when the change password link is loaded.
    }
    this.initChangePasswordForm();
    this.initSubscribers();
    return observableFrom([]);
  }

  private initChangePasswordForm() {
    this.changePasswordForm = this.fb.group({
      password: ["", [Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/)]],
      confirmPassword: ["", [Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/)]],
    }, {
      validator: ConfirmedValidator('password', 'confirmPassword')
    }
    );
  }

  private get changePasswordFormControls() {
    return this.changePasswordForm.controls;
  }

  public get detailForm() {
    return this.changePasswordForm.controls;
  }

  public saveChanges() {
    this.submitted = true;
    if (this.changePasswordForm.invalid) { return; }
    this.changePasswordForm.controls.password.setValue(this.changePasswordForm.controls.password.value.trim());
    this.changePasswordForm.controls.confirmPassword.setValue(this.changePasswordForm.controls.confirmPassword.value.trim());
    this.changePassword();
  }

  public changePassword() {
    const changePasswordDetails = this.transformToEntityModel();
    this.authenticationService.changePassword(changePasswordDetails, this.token)
      .subscribe(
        (data) => {
          this.passwordChangedSuccess = true;
          this.submitted = false;
          if (data.isSuccess) {
            this.alertService.success(data.message, true);
            this.changePasswordForm.disable();
            this.navigateToLoginFromChangePassword();
          }
          else {
            this.alertService.error(data.errorList[0], true);
            this.authenticationService.showAfterIntMessage({ text: data.errorList[0], type: "error", toStable: true });
          }
        }, (err) => {
          this.submitted = false;
          this.alertService.error(err.error.errorList[0], true);
        });
  }

  public transformToEntityModel() {
    const {
      password,
      confirmPassword
    } = this.changePasswordFormControls;

    const changePasswordDetailEntity: any = {
      password: password.value,
      confirmPassword: confirmPassword.value
    };
    return changePasswordDetailEntity;
  }

  public isFormValueChanged() {
    const { password, confirmPassword } = this.changePasswordFormControls;

    const refChangePassword: any = {
      password: password.value,
      confirmPassword: confirmPassword.value
    };
    return _.isEqual(refChangePassword, this.changePasswordDetail);
  }

  public onCancel() {
    this.navigateToLoginFromChangePassword();
  }

  public navigateToHome() {
    return this.router.navigate([""]);
  }

  public navigateToLogin(): Promise<boolean> {
    return this.router.navigate(["auth/login"]).then().catch();
  }

  public initSubscribers() {
    this.changePasswordForm.valueChanges.subscribe(() => {
      this.isFormValueChange = !this.isFormValueChanged();
    });
  }

  public checkResetPasswordLinkIsValid(token) {
    this.authenticationService.checkResetPasswordLinkIsValid(token).subscribe(
      data => {
        if (data) {
          this.resetPasswordLinkExpired = data.isSuccess;
          this.componentChangeDetection.detectChanges();
        }
      },
      error => {
        if (error.status === 401) {
          this.resetPasswordLinkExpired = true;
          return this.router.navigate([""]);
        }
        else {
          this.alertService.error(error);
          this.authenticationService.showAfterIntMessage({ text: error, type: "error", toStable: true });
          return this.router.navigate([""]);
        }
      }
    );
  }

  public errorFor(propertyName: string): string | void {
    if (propertyName) {
      const property = propertyName.replace(/([A-Z])/g, ' $1').replace(/^./, (str) => str.toUpperCase());

      if (this.changePasswordForm.controls[propertyName]) {
        if (this.changePasswordForm.controls[propertyName].hasError("required")) {
          return property.concat(ValidationMessages.RequiredMessage);
        }
        else if (this.changePasswordForm.controls[propertyName].hasError("minlength")
          || this.changePasswordForm.controls[propertyName].hasError("maxlength")) {
          return property.concat(ValidationMessages.MinMaxLengthMessage);
        }
        else if (this.changePasswordForm.controls[propertyName].hasError("matchedField")) {
          return property.concat(ValidationMessages.NotMatchMessage);
        }
        else if (this.changePasswordForm.controls[propertyName].hasError("pattern")) {
          return property.concat(ValidationMessages.PasswordCriteriaMessage);
        }
        else if (this.changePasswordForm.controls[propertyName].hasError("confirmedValidator")) {
          return "New password and confirm password should match.";
        }
        else {
          return null;
        }
      }
    }
  }

  public navigateToLoginFromChangePassword() {
    localStorage.clear();
    setTimeout(() => {
      return this.router.navigate(["auth/login"]).then(() => { window.location.reload(); }).catch();
    }, 3000);
  }

}
