
import { from as observableFrom, Observable } from "rxjs";
import { QuestionableBooleanPipe } from "src/app/utils/questionable-boolean-pipe";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ViewModelType } from "@fastgen/ui-framework";
import _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";

import { AuthenticationService } from "../../services/authentication.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { AuthenticationViewModel } from "./authentication.viewmodel";
import { ValidationMessages } from "../../helpers/validation-messages";
import { AuthService } from 'src/app/utils/auth.service';
@Injectable()
export class LoginViewModel extends AuthenticationViewModel {
  public alertService: AlertService;

  loginForm: FormGroup;
  submitted = false;
  isFormValueChange = false;
  loginDetail: any = {
    email: "",
    password: ""
  };

  public constructor(
    public authenticationService: AuthenticationService,
    public router: Router,
    private fb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService,
    private chRef: ChangeDetectorRef,
    public route: ActivatedRoute,
    private authService: AuthService
  ) {
    super(authenticationService, ViewModelType.ReadOnly, ngxSmartModalService, route);
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  public init() {
    this.initLoginForm();
    this.initSubscribers();
    return observableFrom([]);
  }

  private initLoginForm() {
    // this.authService.clearAuthToken(); //commented this line as token is getting removed from local storage when the login page is loading.
    this.loginForm = this.fb.group({
      email: ["", [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      password: ["", [Validators.required, Validators.minLength(3)]],
    });
  }

  private get loginFormControls() {
    return this.loginForm.controls;
  }

  public get detailForm() {
    return this.loginForm.controls;
  }

  public saveChanges() {
    this.submitted = true;
    if (this.loginForm.invalid) { return; }
    this.loginForm.controls.email.setValue(this.loginForm.controls.email.value.trim());
    this.loginForm.controls.password.setValue(this.loginForm.controls.password.value.trim());
    this.authenticate();
  }

  public authenticate() {
    //  this.navigateToLoginVerification().then().catch();

    const loginDetails = this.transformToEntityModel();
    this.authenticationService.authenticate(loginDetails)
      .subscribe(
        (data) => {
          this.submitted = false;
          if (data.isSuccess) {
            this.alertService.success(data.message, true);
            this.authenticationService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
            this.authenticationService.setAuthToken(data.data.token);
            this.navigateToLoginVerification();
            // this.router.navigate(["auth/login-verification"]).then().catch();
          }
          else {
            this.alertService.error(data.errorList[0], true);
            this.authenticationService.showAfterIntMessage({ text: data.errorList[0], type: "error", toStable: true });
          }
        }, (err) => {
          this.submitted = false;
          this.alertService.error(err.error.errorList[0], true);
          this.authenticationService.showAfterIntMessage({ text: err.error.errorList[0], type: "error", toStable: true });
        }
      );
  }

  public transformToEntityModel() {
    const {
      email,
      password
    } = this.loginFormControls;

    const loginDetailEntity: any = {
      email: email.value,
      password: password.value
    };
    return loginDetailEntity;
  }

  public isFormValueChanged() {
    const { email, password } = this.loginFormControls;

    const refLogin: any = {
      email: email.value,
      password: password.value
    };
    return _.isEqual(refLogin, this.loginDetail);
  }

  public onCancel() {
    this.navigateToHome();
  }

  public navigateToHome() {
    return this.router.navigate(["auth/index"]);
  }

  public navigateToLoginVerification() {
    return this.router.navigate(["auth/login-verification"]).then().catch();
  }

  public initSubscribers() {
    this.loginForm.valueChanges.subscribe(() => {
      this.isFormValueChange = !this.isFormValueChanged();
    });
  }

  public errorFor(propertyName: string): string | void {
    if (propertyName) {
      const property = propertyName.replace(/\w+/g, (w) => w[0].toUpperCase() + w.slice(1).toLowerCase());

      if (this.loginForm.controls[propertyName]) {
        if (this.loginForm.controls[propertyName].hasError("required")) {
          return property.concat(ValidationMessages.RequiredMessage);
        }
        else if (this.loginForm.controls[propertyName].hasError("minlength")
          || this.loginForm.controls[propertyName].hasError("maxlength")) {
          return property.concat(ValidationMessages.MinMaxLengthMessage);
        }
        else if (this.loginForm.controls[propertyName].hasError("email")) {
          return property.concat(ValidationMessages.InvalidMessage);
        }
        else if (this.loginForm.controls[propertyName].hasError("pattern")) {
          return property.concat(ValidationMessages.InvalidMessage);
        }
        else {
          return null;
        }
      }
    }
  }

  public navigateToForgotPassword() {
    return this.router.navigate(["auth/forgot-password"]);
  }
}
