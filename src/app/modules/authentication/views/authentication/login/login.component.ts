import { FormGroup, Validators } from "@angular/forms";
import { Component, OnInit, OnDestroy, OnChanges, AfterContentInit } from "@angular/core";
import { Observable, Subject, Subscription } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";

import { LoginViewModel } from "../../../viewmodels/authentication/login.viewmodel";
import { AuthenticationService } from "../../../services/authentication.service";
import config from "../../../../../utils/config";
import { AlertService } from "../../../../../utils/alert/alert.service";
import { environment } from "src/environments/environment";

export interface User {
  name: string;
}

@Component({
  selector: "app-user-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  providers: [LoginViewModel, AlertService]
})

export class LoginComponent implements OnInit, AfterContentInit {
  public hideForgotPasswordButton: boolean = false;
  constructor(public loginViewModel: LoginViewModel,
    private router: Router,
    private alertService: AlertService,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService) {
    // need to set the afterInitObs to show other screen success or error message in different screen
    alertService.afterInitObs = authenticationService.getAfterIntObs();
  }
  public ngOnInit() {

    if (environment.enableSSO) {
      this.router.navigate(["auth/index"]);
    }
    this.route.params
      .subscribe(params => {
        this.initViewModels();
      });
  }

  public displayFn(user?: User): string | undefined {
    return user ? user.name : undefined;
  }

  public initViewModels() {
    this.loginViewModel.initServices(this.alertService);
    this.loginViewModel.init();
  }


  public ngAfterContentInit() { }


}
