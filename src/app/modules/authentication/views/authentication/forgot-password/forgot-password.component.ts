import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ForgotPasswordViewModel } from "../../../viewmodels/authentication/forgot-password.viewmodel";
import { AuthenticationService } from "../../../services/authentication.service";
import config from "../../../../../utils/config";
import { AlertService } from "../../../../../utils/alert/alert.service";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
  providers: [ForgotPasswordViewModel, AlertService]
})
export class ForgotPasswordComponent implements OnInit {

  constructor(public forgotPasswordViewModel: ForgotPasswordViewModel,
    private router: Router,
    private alertService: AlertService,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService) {
    // need to set the afterInitObs to show other screen success or error message in different screen
    alertService.afterInitObs = authenticationService.getAfterIntObs();
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(params => {
        this.initViewModels();
      });
  }

  public initViewModels() {
    this.forgotPasswordViewModel.initServices(this.alertService);
    this.forgotPasswordViewModel.init();
  }

}
