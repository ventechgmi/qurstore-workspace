import { FormGroup, Validators } from "@angular/forms";
import { Component, OnInit, OnDestroy, OnChanges, AfterContentInit, ChangeDetectionStrategy, ViewEncapsulation, ChangeDetectorRef } from "@angular/core";
import { Observable, Subject, Subscription } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { Title } from '@angular/platform-browser';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatPasswordStrengthComponent } from '@angular-material-extensions/password-strength';

import { ChangePasswordViewModel } from "../../../viewmodels/authentication/changepassword.viewmodel";
import { AuthenticationService } from "../../../services/authentication.service";
import config from "../../../../../utils/config";
import { AlertService } from "../../../../../utils/alert/alert.service";

export interface User {
  name: string;
}

@Component({
  selector: "app-user-change-password",
  templateUrl: "./changepassword.component.html",
  // styleUrls: ["./changepassword.component.scss", "../../../../../../assets/css/mat-password-policy.css"],
  styleUrls: ["./changepassword.component.scss", "../../../../../../assets/css/mat-password-policy.css"],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ChangePasswordViewModel, AlertService]
})

export class ChangePasswordComponent implements OnInit, AfterContentInit {
  constructor(public changePasswordViewModel: ChangePasswordViewModel,
    private router: ActivatedRoute,
    private alertService: AlertService,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private authenticationService: AuthenticationService) {
    // need to set the afterInitObs to show other screen success or error message in different screen
    alertService.afterInitObs = authenticationService.getAfterIntObs();
  }

  showDetails: true;

  public ngOnInit() {
    this.router.params
      .subscribe(params => {
        this.initViewModels(params);
      });
  }

  onStrengthChanged(strength: number) {
    console.log('password strength = ', strength);
  }

  public initViewModels(params: any) {
    this.changePasswordViewModel.componentChangeDetection = this.cdr;
    this.changePasswordViewModel.initServices(this.alertService);
    this.changePasswordViewModel.init(params);
  }

  public ngAfterContentInit() { }
}
