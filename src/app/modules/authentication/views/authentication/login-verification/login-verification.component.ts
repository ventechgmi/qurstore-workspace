import { FormGroup, Validators } from "@angular/forms";
import { Component, OnInit, OnDestroy, OnChanges, AfterContentInit } from "@angular/core";
import { Observable, Subject, Subscription } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";

import { LoginVerificationViewModel } from "../../../viewmodels/authentication/login-verification.viewmodel";
import { AuthenticationService } from "../../../services/authentication.service";
import config from "../../../../../utils/config";
import { AlertService } from "../../../../../utils/alert/alert.service";

export interface User {
  name: string;
}

@Component({
  selector: "app-user-login-verification",
  templateUrl: "./login-verification.component.html",
  styleUrls: ["./login-verification.component.scss"],
  providers: [LoginVerificationViewModel, AlertService]
})

export class LoginVerificationComponent implements OnInit, AfterContentInit, OnDestroy {
  constructor(public loginVerificationViewModel: LoginVerificationViewModel,
    private router: Router,
    private alertService: AlertService,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService) {
    // need to set the afterInitObs to show other screen success or error message in different screen
    alertService.afterInitObs = authenticationService.getAfterIntObs();
  }
  public ngAfterContentInit() { }

  public ngOnInit() {
    this.route.params
      .subscribe(params => {
        this.initViewModels();
      });
  }
  public displayFn(user?: User): string | undefined {
    return user ? user.name : undefined;
  }
  public initViewModels() {
    this.loginVerificationViewModel.initServices(this.alertService);
    this.loginVerificationViewModel.init();
  }

  public ngOnDestroy() {
    this.authenticationService.showAfterIntMessage("");
  }

}
