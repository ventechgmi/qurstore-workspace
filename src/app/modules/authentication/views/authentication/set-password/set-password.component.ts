import { Component, OnInit, ElementRef, OnDestroy, ComponentFactoryResolver, ComponentFactory, ViewChild, AfterViewInit } from "@angular/core";
import { FormBuilder, FormGroup, FormControl, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { SetPasswordViewModel } from "../../../viewmodels/authentication/set-password.viewmodel";
import { AlertService } from "src/app/utils/alert/alert.service";
import config from "../../../../../utils/config";
@Component({
  selector: 'app-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.css']
})
export class SetPasswordComponent implements OnInit {

  viewModel: SetPasswordViewModel;
  public componentFactory: ComponentFactory<any>;
  public isHavingCapitalValidation = true;
  public isHavingSmallValidation = true;
  public isHavingNumericValidation = true;
  public isHavingSpecialValidation = true;

  constructor(private element: ElementRef, private formBuild: FormBuilder, private alertService: AlertService, private router: Router, private activatedRoute: ActivatedRoute) {
    // alertService.afterInitObs = authenticationService.getSuccessMessageAfterInit();

  }

  ngOnInit(): void {
    this.viewModel = new SetPasswordViewModel(this.formBuild, this.router, this.alertService);
  }

  public get isMinLength() {
    return (this.viewModel.setPwdForm.controls.NewPassword.value.length > 6) ? true : false;
  }
  public get isHavingCapitalLetter() {
    let result = false;
    const regexPatter = new RegExp("^(?=.*[A-Z])");
    result = (regexPatter.test(this.viewModel.setPwdForm.controls.NewPassword.value)) ? true : false;

    return result;
  }
  public get isHavingSmallLetter() {
    let result = false;
    const regexPatter = new RegExp("^(?=.*[a-z])");
    result = (regexPatter.test(this.viewModel.setPwdForm.controls.NewPassword.value)) ? true : false;
    return result;
  }
  public get isHavingNumerical() {
    let result = false;
    const regexPatter = new RegExp("^(?=.*\\d)");
    result = (regexPatter.test(this.viewModel.setPwdForm.controls.NewPassword.value)) ? true : false;
    return result;
  }
  public get isHavingSpecialCharacter() {
    let result = false;
    const regexPatter = new RegExp("^(?=.*[!@#$%^&*])");
    result = (regexPatter.test(this.viewModel.setPwdForm.controls.NewPassword.value)) ? true : false;
    return result;
  }

}
