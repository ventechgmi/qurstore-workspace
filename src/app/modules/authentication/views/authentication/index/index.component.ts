import { Component, OnInit } from '@angular/core';
import config from "../../../../../utils/config";
import { Router } from "@angular/router";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor(
    private router: Router) { }

  ngOnInit(): void {
    localStorage.clear() // QS-571 clearing the local storage due to login link display/hide is dependent on the userName property in the local storage.
  }

  public navigateToLogin() {
    this.router.navigate(["auth/login"]);
  }


}
