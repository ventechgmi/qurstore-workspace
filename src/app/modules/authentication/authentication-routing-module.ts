import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./views/authentication/login/login.component";
import { LoginVerificationComponent } from "./views/authentication/login-verification/login-verification.component";
import { ChangePasswordComponent } from "./views/authentication/changepassword/changepassword.component";
import { NgModule } from "@angular/core";
import { SetPasswordComponent } from './views/authentication/set-password/set-password.component';
import { IndexComponent } from "./views/authentication/index/index.component";
import { ForgotPasswordComponent } from "./views/authentication/forgot-password/forgot-password.component";

export const routes: Routes = [
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "index",
    component: IndexComponent
  },
  {
    path: "login-verification",
    component: LoginVerificationComponent
  },
  {
    path: "changepassword/:token",
    component: ChangePasswordComponent
  },
  {
    path: "setpassword",
    pathMatch: "full",
    component: SetPasswordComponent
  },
  {
    path: "forgot-password",
    pathMatch: "full",
    component: ForgotPasswordComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
