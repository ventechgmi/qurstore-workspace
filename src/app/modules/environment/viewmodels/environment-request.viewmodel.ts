import { from as observableFrom, Observable, Subject, forkJoin } from "rxjs";
import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel, DataGridExtendedViewModel } from "../../core-ui-components";
import config, { GridConfig, ROUTES_CONFIG_MAP } from "../../../utils/config";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import moment from 'moment';
import { NgxSmartModalService } from "ngx-smart-modal";

import { ValidationMessages } from '../helpers/validation-messages';
import { AlertService } from 'src/app/utils/alert/alert.service';
import { EnvironmentService } from '../services/environment.service';
import { EnvironmentViewModel } from './environment.viewmodel';
import { AuthService } from "src/app/utils/auth.service";
import { WorkspaceRequestStatus, PageModeType, AWSKeyValues, PricingModel, BundleOption, TabIndexKeys } from "../helpers/constants";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { EnvironmentRequestNavigationService } from "../../core-ui-services/environment-request-navigation.service";

@Injectable()
export class EnvironmentRequestViewModel extends EnvironmentViewModel {

  public hardwareTabModelForm: FormGroup;
  public environmentForm: FormGroup;
  public datasetForm: FormGroup;

  public environmentId: string;
  public isApproverFromList: boolean;
  public isFromRequestMoreUtility: boolean;
  public isFromResubmit: boolean;
  public isDetailFromList: boolean;
  public vendorId: string;
  public workspaceList: any;
  public workspaceCatalogList: any;
  public viewModelDataGrid: DataGridViewModel;
  public auditDataGrid: DataGridViewModel;
  public datasetviewModelDataGrid: DataGridViewModel;
  public requestedDatasetDataGrid: DataGridViewModel;
  public historyViewModelDataGrid: DataGridViewModel;
  public usersViewModelDataGrid: DataGridExtendedViewModel;
  private workspaceCatalogId: string;
  public workspaceTagTypeDetailsList: any;
  public workspaceTagTypeSelectedDetailsList: any;
  public approvedUtilityList: any;

  public requestNewWorkSpace = false;
  public workspaceRequestId: string;
  public todayDate: string;
  public minDate: string;
  public appliedFilters: any[];
  public vendorWithoutUserList: any;
  public workSpaceTypeLabel: string;
  public diskSpaceRequestedLabel: string;
  public workspaceNameLabel: string;
  public reasonForRequestLabel: string;
  public workspaceRequestStatusAuditLabel: any;
  public rootVolumeSizeLabel: string;
  public workspaceDescriptionLabel: string;
  public userNameLabel: string;
  public neededByDateLabel: string;
  public currentStatusNameLabel: string;
  public isMonthlyPricing: boolean;
  public isHourlyPricing: boolean;
  public isRegionSelected = false;
  public isBundleOptionSelected = false;
  public userVolume: string;
  public OriginalUserVolume: string;
  public canShowDetailLabel = false;
  public modeType: string = "Create";
  public title: string;
  public metadata = ["one", "two", "three", "four"];
  public blankReasonForTermination = false;
  public blankReasonForCancellation = false;
  public selectedUserIds: any = [];
  public availableCatalogId: string;
  public selectedAWSBundleId: string;
  public selectedUserId: string;
  public header = [
    { headerName: "Bundle", field: "awsbundleId", isClickable: true, width: 100 },
    { headerName: "Configuration", field: "workspaceDescription", isClickable: false, width: 400 },
    { headerName: "Workspace Name", field: "workspaceName", isClickable: false },
    { headerName: "Last Modified", field: "lastModifiedDateDisplay", width: 100 },
    { headerName: "Status", field: "currentStatusName", width: 75 }
  ];
  public auditHeader = [
    { headerName: "Action Date", field: "actionDateDisplay" },
    { headerName: "User Name", field: "actionByUserName" },
    { headerName: "Status", field: "statusName" },
    { headerName: "Comments", field: "actionComments" }
  ];

  public headerCatalog = [
    { headerName: "Name", field: "fileName", checkboxSelection: true, width: 150, headerCheckboxSelection: true, headerCheckboxSelectionFilteredOnly: true },
    { headerName: "Description", field: "fileDescription", width: 190 },
    { headerName: "Type", field: "datasetTypeName", isClickable: false, width: 75 },
  ];
  public headerRequest = [
    { headerName: "Name", field: "fileName", checkboxSelection: true, width: 150, headerCheckboxSelection: true, headerCheckboxSelectionFilteredOnly: true },
    { headerName: "Description", field: "fileDescription", checkboxSelection: false, width: 250, headerCheckboxSelection: false, headerCheckboxSelectionFilteredOnly: false },
    { headerName: "Type", field: "datasetTypeName", isClickable: false, },
  ];
  public headerRequestDetail = [
    { headerName: "Name", field: "fileName", checkboxSelection: true, width: 150, headerCheckboxSelection: true, headerCheckboxSelectionFilteredOnly: true },
    { headerName: "Description", field: "fileDescription", checkboxSelection: false, width: 250, headerCheckboxSelection: false, headerCheckboxSelectionFilteredOnly: false },
    { headerName: "Type", field: "datasetTypeName", isClickable: false, },
  ];
  public historyHeader = [
    { headerName: "Status", field: "status", isClickable: false, width: 300, sortable: false },
    { headerName: "Comments", field: "comments", width: 400, sortable: false },
    { headerName: "Date", field: "date", width: 200, sortable: true },
    { headerName: "User", field: "user", width: 300, sortable: false },
  ];

  public selectedTags: any[] = [];
  // map the appropriate columns to fields property
  public fields: object = { text: 'itemName', value: 'id' };
  public userFields: object = { text: 'name', value: 'id' };
  // set the placeholder to MultiSelect input element
  public waterMark: string = '-- Select--';
  // set the type of mode for how to visualized the selected items in input element.
  public box: string = 'Box';
  public modifiedData;
  public filteredData: object;
  public additionalPrice: number = 0;
  public calculatedAdditionalPrice: number = 0;
  public filterstrDataForWindow: object;
  public filterstrDataForPlus: object;
  public filterstrDataForLinux: object;
  public bundleOptions = ["Linux Bundle Options", "Windows Bundle Options"]
  public usageType = ["AlwaysOn", "AutoStop"]
  public filterstrDataForWindowHeader;
  public filterstrDataForPlusHeader;
  public filterstrDataForLinuxHeader;
  public selectedspaceUsageOptions: string;
  public costDataDetails: object;
  public costData: object;
  public awsBundleName = [];
  public awsSelectedBundle = [];
  public WindowHeaderData = [];
  public PlusHeader = [];
  public LinuxHeader = [];
  public selectedSpecificationBundleOptions: string;
  public filterstrDataForLinuxHeaderData: object;
  public filterstrDataForPlusHeaderData: object;
  public filterstrDataForWindowHeaderData: object;
  public monthValue: number;
  public hourValue: number;
  public monthValueAlwaysOn: number;
  public hourUsagecount: number;
  public dayUsagecount: number;
  public totalCost: number;
  public monthlySavings: number;
  public enableCostCalculation: boolean = false;
  private cancelConfirmModalFromList: string = "cancelConfirmModalFromList";
  private terminateConfirmModalFromList: string = "terminateConfirmModalFromList";
  private cancelConfirmModal: string = "cancelConfirmModal";
  private selectedBundleOptions: string;
  public userList: any = [];
  public originalTagTypeDetails: any;
  public softwaresList: any;
  public utilitiesList: any;
  public alertService: AlertService;
  public softwaresViewModelDataGrid: DataGridViewModel;
  public utilitiesViewModelDataGrid: DataGridViewModel;
  public environmentRequestDetails: any;
  public workspaceRequestViewModelForm: FormGroup;
  public tabIndex: number = 0;

  public tabIndexKeys = TabIndexKeys;

  public headerForSoftware = [
    {
      headerName: "Select All", field: "selectAll", minWidth: 250, headerCheckboxSelection: true, headerCheckboxSelectionFilteredOnly: true, checkboxSelection: true,
      rowSelection: 'multiple', groupSelectsChildren: true, suppressRowClickSelection: true
    },
    { headerName: "Bundle", field: "bundleName", minWidth: 500, isClickable: false, filter: false, noFilter: true },
    { headerName: "Available Software", field: "softwareList", minWidth: 550, isClickable: false, filter: false, noFilter: true },
  ];
  public headerForSoftwareDetail = [

    { headerName: "Bundle", field: "bundleName", minWidth: 500, isClickable: false, filter: false, noFilter: true },
    { headerName: "Available Software", field: "softwareList", minWidth: 550, isClickable: false, filter: false, noFilter: true },
  ];
  public headerForUtilities = [
    {
      headerName: "Select All", field: "selectAll", minWidth: 250, headerCheckboxSelection: true, headerCheckboxSelectionFilteredOnly: true, checkboxSelection: true,
      rowSelection: 'multiple', groupSelectsChildren: true, suppressRowClickSelection: true
    },
    { headerName: "Name", field: "name", minWidth: 500, isClickable: false, filter: false, noFilter: true },
    { headerName: "Description", field: "description", minWidth: 550, isClickable: false, filter: false, noFilter: true },
  ];
  public headerForUtilitiesDetail = [

    { headerName: "Name", field: "name", minWidth: 500, isClickable: false, filter: false, noFilter: true },
    { headerName: "Description", field: "description", minWidth: 550, isClickable: false, filter: false, noFilter: true },
  ];

  public datasetCatalogList: any[] = [];
  public message: any = "";
  public selectedSoftWares: any;
  public selectedUtilities: any;
  public isRedirectDashboard: boolean = false;
  public isHideDataSetGrid: boolean = false;
  public selectedUserValues: any;
  public isHavingUserError: any = false;
  public isHavingUserVolumeError: any = false;

  public onSelect(args: any, workspaceTagDetail: any): void {
    if (args && (args.itemData.value === undefined)) {
      args.itemData.isCustomCreated = true;
      args.itemData.tagTypeId = workspaceTagDetail.id;
    }
    workspaceTagDetail.selectedTags.push(args.itemData);
    this.selectedTags.push(args.itemData);
    if (this.selectedTags && this.selectedTags.length > 0) {
      _.each(this.selectedTags, tagValue => {
        if (tagValue && tagValue.tagValueUserMapping && tagValue.tagValueUserMapping.length > 0) {
          _.each(tagValue.tagValueUserMapping, user => {
            const userData = user.user;
            let objUser: any = {};
            objUser.name = userData.firstName.concat(" ", userData.lastName);
            objUser.id = userData.id;
            this.userList.push(objUser);
          });
        }
      });
      this.userList = _.uniq(this.userList, "id");
      console.log(this.userList);
    }
  }

  public get isValidComment() {
    return (this.message.trim().length > 2) ? false : true;
  }

  public getUserList(tagValue) {

    if (tagValue && tagValue.tagValueUserMapping && tagValue.tagValueUserMapping.length > 0) {
      _.each(tagValue.tagValueUserMapping, user => {
        const userData = user.user;
        const exisitingUser = this.userList
          .find(m => m.id === userData.id);
        if (!exisitingUser) {
          let objUser: any = {};
          objUser.name = userData.firstName.concat(" ", userData.lastName);
          objUser.id = userData.id;
          this.userList.push(objUser);
        }
      });
    }

  }

  public onRemove(args: any, workspaceTagDetail: any): void {
    const tempSelectedTags = [];
    _.each(workspaceTagDetail.selectedTags, selectedTags => {
      if (args.itemData.itemName !== selectedTags.itemName) {
        tempSelectedTags.push(selectedTags);
      }
    });
    workspaceTagDetail.selectedTags = tempSelectedTags;

    this.selectedTags = _.without(this.selectedTags, _.findWhere(this.selectedTags, {
      id: args.itemData.id
    }));

    this.userList = [];
    if (this.selectedTags?.length > 0) {
      _.each(this.selectedTags, tagValue => {
        if (tagValue && tagValue.tagValueUserMapping?.length > 0) {
          _.each(tagValue.tagValueUserMapping, user => {
            const userData = user.user;
            let objUser: any = {};
            objUser.name = userData.firstName.concat(" ", userData.lastName);
            objUser.id = userData.id;
            this.userList.push(objUser);
          });
        }
      });

      this.userList = _.uniq(this.userList, "id");
    }
  }

  public onUserSelect(args: any): void {
    this.selectedUserIds.push(args.itemData.id);
    if (this.modeType !== PageModeType.Resubmit)
      this.updateUtilitiesGridBasedOnUser();
  }

  public onUserRemove(args: any): void {
    const tempUserList = _.filter(this.selectedUserIds, item => item !== args.itemData.id);
    this.selectedUserIds = tempUserList;
    if (this.modeType !== PageModeType.Resubmit)
      this.updateUtilitiesGridBasedOnUser();
  }

  private updateUtilitiesGridBasedOnUser(): void {
    const utilitiesList = (this.selectedUserIds.length === 1) ? _.filter(this.approvedUtilityList, item => item.userId === this.selectedUserIds[0] || item.isPublic === true) :
      this.selectedUserIds.length > 1 ? _.filter(this.approvedUtilityList, item => item.isPublic === true) : this.approvedUtilityList;
    this.initUtilitiesGrid(utilitiesList);
  }

  public get CanShowDetails() {
    return this.canShowDetailLabel;
  }
  public hideDetails() {
    this.canShowDetailLabel = false;
  }

  public initToday() {
    this.todayDate = (new Date()).toISOString().split('T')[0];
    this.minDate = this.todayDate;
    this.hardwareTabModelForm.patchValue({
      neededBy: this.todayDate
    });

    this.environmentForm.patchValue({
      neededBy: this.todayDate
    });

    this.datasetForm.patchValue({
      neededBy: this.todayDate
    });

  }
  public constructor(
    public environmentService: EnvironmentService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    private fb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public authService: AuthService,
    private ngxService: NgxUiLoaderService,
    public environmentRequestNavigationService: EnvironmentRequestNavigationService
  ) {
    super(environmentService, ViewModelType.Edit, ngxSmartModalService, route, authService);
    this.initForms();
  }

  public init(param?) {
    this.environmentId = param.environmentId;
    this.isRedirectDashboard = param.isRedirectDashboard;
    if (param.isFromEmail) {
      this.isApproverFromList = param.mode === "0" ? true : false; // 0 Approve
      this.isFromResubmit = param.mode === "1" ? true : false; // 1 Resubmit
      this.isDetailFromList = param.mode === "2" ? true : false; // 2 Detail
    }
    else {
      const redirectData = JSON.parse(localStorage.getItem("redirectData"));

      this.isApproverFromList = redirectData.isApproverFromList;
      this.isFromRequestMoreUtility = redirectData.isFromRequestMoreUtility;
      this.isFromResubmit = redirectData.isFromResubmit;
      this.selectedUserId = redirectData.selectedUserId;
      this.isDetailFromList = redirectData.isDetailFromList;
    }
    this.viewModelDataGrid = new DataGridViewModel();

    this.softwaresViewModelDataGrid = new DataGridViewModel();
    this.softwaresViewModelDataGrid.gridOptionRowSelectionToMultiple();
    this.utilitiesViewModelDataGrid = new DataGridViewModel();
    this.utilitiesViewModelDataGrid.gridOptionRowSelectionToMultiple();
    this.softwaresViewModelDataGrid.setPagination(true);
    this.utilitiesViewModelDataGrid.setPagination(true);
    this.subscribeToCreateSuccessMessage();

    this.viewModelDataGrid.setHeader(this.header, false, { canCancelRequest: true, canTerminate: true });


    this.getWorkspaceCatalogList();
    this.getAWSWorkspaceCatalogList();
    this.getRegionList();
    this.getWorkspaceTagTypeDetails();
    this.viewModelDataGrid.defaultSortModel = [
      {
        colId: 'lastModifiedDateDisplay',
        sort: 'desc',
      }
    ];

    this.softwaresViewModelDataGrid.setHeader((this.isApproverFromList || this.isFromRequestMoreUtility || this.isDetailFromList) ? this.headerForSoftwareDetail : this.headerForSoftware, false);
    this.softwaresViewModelDataGrid.defaultSortModel = [{ colId: 'bundleName', sort: 'asc' }];
    this.utilitiesViewModelDataGrid.setHeader((this.isApproverFromList || this.isDetailFromList) ? this.headerForUtilitiesDetail : this.headerForUtilities, false);
    this.utilitiesViewModelDataGrid.defaultSortModel = [{ colId: 'name', sort: 'asc' }];

    this.datasetviewModelDataGrid = new DataGridViewModel();
    this.datasetviewModelDataGrid.dataTableId = "datasetviewModelDataGrid";
    this.datasetviewModelDataGrid.actionColumnName = "Data Usage Agreement";
    this.datasetviewModelDataGrid.setHeader(this.headerCatalog, false, { canViewDataUsageAgreement: true });
    this.datasetviewModelDataGrid.setPagination(true);
    this.datasetviewModelDataGrid.gridOptionRowSelectionToMultiple();
    this.datasetviewModelDataGrid.setPagination(true);

    this.requestedDatasetDataGrid = new DataGridViewModel();
    this.requestedDatasetDataGrid.dataTableId = "requestedDatasetDataGrid";
    this.requestedDatasetDataGrid.setHeader((this.isApproverFromList) ? this.headerRequestDetail : this.headerRequest, false);
    this.requestedDatasetDataGrid.gridOptionRowSelectionToMultiple();
    this.requestedDatasetDataGrid.setPagination(true);
    this.historyViewModelDataGrid = new DataGridViewModel();
    this.historyViewModelDataGrid.setHeader(this.historyHeader, false);
    this.historyViewModelDataGrid.defaultSortModel = [
      {
        colId: 'actionDate',
        sort: 'desc',
      }
    ];
    this.usersViewModelDataGrid = new DataGridExtendedViewModel();
    this.getSoftwaresList();
    this.getApprovedUtilities();
    this.getDataSetCatalogList();

    this.initToday();
    this.auditDataGrid = new DataGridViewModel();
    this.auditDataGrid.setHeader(this.auditHeader, false);
    this.auditDataGrid.setPagination(false);
    this.auditDataGrid.defaultSortModel = [
      {
        colId: 'actionDateDisplay',
        sort: 'desc',
      }
    ];
    this.workspaceTagTypeDetailsList = [];
    this.workspaceTagTypeSelectedDetailsList = [];
    this.subscribers();
    this.title = this.isApproverFromList ? "Approve/Reject New Environment" : (this.isFromResubmit) ? "Resubmit Environment Request" : (this.isDetailFromList) ? "Environment Request Details" : "New Environment Request";
    this.modeType = this.isApproverFromList ? "Approve" : this.isFromRequestMoreUtility ? "RequestMoreUtility" : this.isFromResubmit ? "Resubmit" : this.isDetailFromList ? "Detail" : "Create";
    if (this.isApproverFromList || this.isFromRequestMoreUtility || this.isDetailFromList) { this.disableFormControls(); }
    this.initializeGridColDef();
    return observableFrom([]);
  }

  public initForms(): void {
    this.workspaceRequestViewModelForm = this.fb.group({
      workspaceCatalog: ""
    });

    this.environmentForm = this.fb.group({
      name: ["", [Validators.required, super.validateSpecialCharactersAndWhiteSpace(3, 100)]],
      neededBy: ["", [Validators.required]],
      reasonForRequest: ["", [Validators.required, super.validateSpecialCharactersAndWhiteSpace(3, 4000)]],
      userData: [null, [Validators.required]]
    });

    this.hardwareTabModelForm = this.fb.group({
      workspaceType: [null, [Validators.required]],
      bundleOptions: [null, [Validators.required]],
      region: [null, [Validators.required]],
      userWorkspaceName: ["",],
      pricingModel: [null, [Validators.required]],
      terminateDecisionComment: "",
      cancelDecisionComment: "",
      hourUsage: ["",],
      dayUsage: ["",],
      userVolume: ["", [Validators.required]]
    });

    this.datasetForm = this.fb.group({
      name: ["", [Validators.required]],
      neededBy: ["", [Validators.required]],
      reasonForRequest: ["", [Validators.required]]
    });
  }

  public initializeGridColDef() {

    const id = {
      headerName: "id",
      field: "id",
      width: 100,
      hide: true,
      suppressToolPanel: true
    };
    this.usersViewModelDataGrid.columnDefs.push(id);

    const name = {
      headerName: "Name",
      field: "name",
      editable: false,
      sortable: true,
      filter: true
    };
    this.usersViewModelDataGrid.columnDefs.push(name);

    const userName = {
      headerName: "AWS Workspace Username",
      field: "workspaceUserName",
      editable: this.modeType === "Approve" ? true : false
    };
    this.usersViewModelDataGrid.columnDefs.push(userName);

  }
  private disableFormControls() {
    if (!this.isFromRequestMoreUtility) {
      this.environmentForm.get("name").disable();
      this.environmentForm.get("neededBy").disable();
      this.environmentForm.get("reasonForRequest").disable();
    }
    this.hardwareTabModelForm.get("bundleOptions").disable();
    this.hardwareTabModelForm.get("region").disable();
    this.hardwareTabModelForm.get("workspaceType").disable();
    this.hardwareTabModelForm.get("userVolume").disable();
    this.hardwareTabModelForm.get("pricingModel").disable();

  }

  private getEnvironmentDetails() {
    this.ngxService.start();
    this.environmentService.getEnvironmentRequest(this.environmentId)
      .subscribe(

        (data) => {
          this.ngxService.stop();
          this.environmentRequestDetails = data;
          this.bindValuesToControls(this.environmentRequestDetails)

        },
        (err) => {
          this.alertService.error(err.error.errorList[0], true);
          this.ngxService.stop();
        });
  }

  private bindValuesToControls(data: any) {

    this.environmentForm.patchValue({
      name: data.name,
      neededBy: data.neededByDate || this.todayDate,
      reasonForRequest: data.comments
    });

    _.each(this.workspaceTagTypeDetailsList, item => {
      var selected = _.find(item.tagValues, tagValue => {
        return data.environmentRequestHeaderUserMappings.find(t => t.tagValueUserMapping.tagValueId == tagValue.id)
      });
      if (selected) {
        this.getUserList(selected);
        item.selectedTags = [selected];
        item.selectedTagValues = [selected.id]
      }
    });
    const awsbundleOptionData = data.environmentRequestDetails[0].userEnvironment.workspaceRequests[0].awsbundleOption;
    const awsregionData = data.environmentRequestDetails[0].userEnvironment.workspaceRequests[0].awsregion;
    const workspaceCatalogIdData = data.environmentRequestDetails[0].userEnvironment.workspaceRequests[0].workspaceCatalogId;
    const userVolumeSizeData = data.environmentRequestDetails[0].userEnvironment.workspaceRequests[0].userVolumeSize;
    const pricingModelData = data.environmentRequestDetails[0].userEnvironment.workspaceRequests[0].pricingModel;

    this.changeWorkspaceBundleOptions(awsbundleOptionData);
    this.changeWorkspaceBundle({ name: awsregionData });
    const selectedSpecificationForAwsBundle = this.awsBundleName.find(item => item.id === workspaceCatalogIdData);
    this.changeSpecificationForBundleOptions(selectedSpecificationForAwsBundle);

    this.pricingModelValueBinding(pricingModelData);

    this.hardwareTabModelForm.patchValue({
      bundleOptions: awsbundleOptionData,
      region: { name: awsregionData },
      workspaceType: workspaceCatalogIdData,
      userVolume: userVolumeSizeData,
      pricingModel: pricingModelData
    });


    let userDataGrid: any = [];
    if (data.environmentRequestDetails && data.environmentRequestDetails.length > 0) {
      let environmentRequestDetails = data.environmentRequestDetails;

      if (this.selectedUserId) {
        environmentRequestDetails = environmentRequestDetails.find(m => m.userEnvironment.user.id === this.selectedUserId);
        let objUser: any = {};
        objUser.name = environmentRequestDetails.userEnvironment.user.fullName;
        objUser.workspaceId = "";
        objUser.id = environmentRequestDetails.userEnvironment.workspaceRequests[0].id;
        objUser.workspaceUserName = environmentRequestDetails.userEnvironment.workspaceRequests[0].workspaceUserName ? environmentRequestDetails.userEnvironment.workspaceRequests[0].workspaceUserName
          : environmentRequestDetails.userEnvironment.user.authenticationProviderId ? environmentRequestDetails.userEnvironment.user.userName : "";
        objUser.userId = environmentRequestDetails.userEnvironment.user.id;
        userDataGrid.push(objUser);
        this.selectedUserIds.push(environmentRequestDetails.userEnvironment.user.id);
      }
      else {
        _.each(environmentRequestDetails, environmentRequestDetail => {
          let objUser: any = {};
          objUser.name = environmentRequestDetail.userEnvironment.user.fullName;
          objUser.workspaceId = "";
          objUser.id = environmentRequestDetail.userEnvironment.workspaceRequests[0].id;
          objUser.workspaceUserName = environmentRequestDetail.userEnvironment.workspaceRequests[0].workspaceUserName ? environmentRequestDetail.userEnvironment.workspaceRequests[0].workspaceUserName
            : environmentRequestDetail.userEnvironment.user.authenticationProviderId ? environmentRequestDetail.userEnvironment.user.userName : "";
          objUser.userId = environmentRequestDetail.userEnvironment.user.id;
          userDataGrid.push(objUser);
          this.selectedUserIds.push(environmentRequestDetail.userEnvironment.user.id);
        });
      }
      this.usersViewModelDataGrid.setRowsData(userDataGrid);
      this.selectedUserValues = this.selectedUserIds;
    }
    this.updateUtilitiesGridBasedOnUser();
    let historyDataGrid: any = [];
    if (data.environmentRequestHeaderStatusAudits && data.environmentRequestHeaderStatusAudits.length > 0) {
      _.each(data.environmentRequestHeaderStatusAudits, auditData => {
        let objauditData: any = {};
        objauditData.status = auditData.status.name ? auditData.status.name : data.currentStatus.name;
        objauditData.comments = auditData.actionComments;
        objauditData.actionDate = auditData.actionDate;
        objauditData.date = moment(auditData.actionDate).format(config.APPLICATION_DATE_TIME_FORMAT);
        objauditData.user = auditData.actionByNavigation.fullName;
        historyDataGrid.push(objauditData);
      });
      this.historyViewModelDataGrid.setRowsData(historyDataGrid);

      _.each(data.environmentRequestDetails[0].userEnvironment.workspaceRequestBundleMappings, workspaceRequestBundleMapping => {
        this.softwaresViewModelDataGrid.gridOptions.api.forEachNode(node => node.data.id == workspaceRequestBundleMapping.bundleId ? node.setSelected(true) : 0);
      });
      if (this.modeType === PageModeType.Approve || this.modeType === PageModeType.Resubmit || this.modeType === PageModeType.Detail) {

        var updatedNodes = [];
        _.each(data.environmentRequestDetails[0].userEnvironment.datasetRequests, datasetRequest => {
          _.each(datasetRequest.datasetRequestDetails, datasetRequestDetail => {
            this.datasetviewModelDataGrid.gridOptions.api.forEachNode(node => {
              if (node.data.id == datasetRequestDetail.datasetId) {
                if(!updatedNodes.includes(node.data)) {
                  updatedNodes.push(node.data);
                }
              }
            });
          })

        })
        this.datasetviewModelDataGrid.gridApi.applyTransactionAsync({ remove: updatedNodes });
        this.requestedDatasetDataGrid.gridApi.applyTransactionAsync({ add: updatedNodes });
      }

      if (this.modeType === PageModeType.Resubmit) {
        setTimeout(() => {
          _.each(data.environmentRequestDetails[0].userEnvironment.workspaceUtilityMappings, workSpaceUtility => {
            this.utilitiesViewModelDataGrid.gridOptions.api.forEachNode(node => node.data.id == workSpaceUtility.utilityRequestId ? node.setSelected(true) : 0);
          });
        }, 1000);
      }
      else {
        _.each(data.environmentRequestDetails[0].userEnvironment.workspaceUtilityMappings, workSpaceUtility => {
          this.utilitiesViewModelDataGrid.gridOptions.api.forEachNode(node => node.data.id == workSpaceUtility.utilityRequestId ? node.setSelected(true) : 0);
        });
      }
    }
    if (this.isApproverFromList || this.isFromRequestMoreUtility || this.isDetailFromList) { this.bindSelectedValuesBinding(); }

    this.environmentForm.markAsDirty();
    this.hardwareTabModelForm.markAsDirty();

  }

  public updateStatusForEnvironment(value: string): void {
    this.usersViewModelDataGrid.gridApi.stopEditing();
    let workspaceRequest: any = [];
    this.usersViewModelDataGrid.gridApi.forEachNode(node => workspaceRequest.push(node.data));
    if (value === WorkspaceRequestStatus.APPROVED_STATUS) {
      this.isHavingUserError = workspaceRequest.filter(t => t.workspaceUserName === null || t.workspaceUserName === "").length > 0;
      if (this.isHavingUserError) { this.alertService.error(ValidationMessages.UserNameRequired, true); return; }

      let duplicateWorkspaces = workspaceRequest.map(e => e['workspaceUserName'])
        .map((e, i, final) => final.indexOf(e) !== i && i)
        .filter(obj => workspaceRequest[obj])
        .map(e => workspaceRequest[e]["workspaceUserName"]);

      if (duplicateWorkspaces.length > 0) {
        this.alertService.error(duplicateWorkspaces[0] + ValidationMessages.DuplicateWorkspaceUserNameMessage, true);
        return;
      }
      const workspaceUserNames = [];
      _.each(workspaceRequest, (e) => {
        workspaceUserNames.push(e.workspaceUserName);
      });
      const obj = {
        userNames: workspaceUserNames
      }

      this.environmentService.validateWorkSpaceUsernames(obj)
        .subscribe((data) => {
          if (data.isSuccess) {
            const updateStatusObject = {
              EnvironmentRequestHeaderDetailId: this.environmentId,
              ActionComments: this.message,
              Id: this.environmentId,
              StatusName: value,
              workspaceRequest: workspaceRequest
            }
            this.updateStatusAsync(updateStatusObject);
          }
          else {
            this.alertService.error(data.message, true);
          }
        },
          (err) => {
            this.alertService.error(err.error.errorList[0], true);
          });
    }
    else {

      let updateStatusObject = {
        EnvironmentRequestHeaderDetailId: this.environmentId,
        ActionComments: this.message,
        Id: this.environmentId,
        StatusName: value,
        workspaceRequest: workspaceRequest
      }
      this.updateStatusAsync(updateStatusObject);
    }
  }

  public updateStatusAsync(requestObject: any): void {
    this.environmentService.UpdateEnvironmentRequestHeaderStatus(requestObject)
      .subscribe((data) => {
        this.alertService.success(data.message, true);
        this.environmentService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
        this.redirectToList();
      },
        (err) => {
          this.alertService.error(err.error.errorList[0], true);
        });
  }

  public get environmentDetailForm() {
    return this.environmentForm.controls;
  }

  public initGridAuditLog(data) {
    const auditLogs = data ? data : [];
    auditLogs.map(auditLog => {
      if (auditLog.actionDate) {
        auditLog.actionDateDisplay = moment(auditLog.actionDate).format(config.APPLICATION_DATE_TIME_FORMAT);
      }
    });
    this.auditDataGrid.setRowsData(auditLogs, false);
  }
  private validateForm() {
    const { neededBy } = this.workspaceListViewModelFormControls;
    let isInputsValid = false;
    if (new Date(neededBy.value).getTime() >= new Date().getTime()) {
      isInputsValid = true;
    }
    return isInputsValid;
  }

  private resetFormControls() {
    this.hardwareTabModelForm.patchValue({
      workspaceType: null,
      bundleOptions: null,
      region: null,
      pricingModel: "",
      reasonForRequest: " ",
      terminateDecisionComment: "",
      cancelDecisionComment: "",
      project: "",
      hourUsage: "",
      dayUsage: "",
      userVolume: ""
    });
  }

  public terminateWorkspace() {
    this.ngxSmartModalService.getModal(this.terminateConfirmModalFromList).open();
  }
  public transformToEntityModelTerminateRequest() {
    const { terminateDecisionComment } = this.workspaceListViewModelFormControls;
    return {
      id: this.workspaceRequestId,
      decisionReason: terminateDecisionComment.value,
    };
  }

  get workspaceListViewModelFormControls() {
    return this.hardwareTabModelForm.controls;
  }

  public get detailForm() {
    return this.hardwareTabModelForm.controls;
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  private subscribers() {
    this.datasetviewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.viewDataUsageAgreement:
          this.availableCatalogId = e.eventData.rowDetails.id;
          this.downloadFile();
          break;
        default:
          break;
      }
    }, error => {
      console.log("Error: " + error);
    });
  }


  private downloadFile() {
    const row = this.datasetCatalogList.filter(x => x.id === this.availableCatalogId);
    if (row && row.length > 0) {
      const dataUserAgreementId = row[0].dataUsageAgreementId;
      const fileName = row[0].dataUsageAgreementFileName.replace(/ /g, '_');
      this.environmentService.downloadDataUsageAgreementDocument(dataUserAgreementId)
        .subscribe(
          blob => {
            const a = window.document.createElement("a");
            a.href = window.URL.createObjectURL(blob);
            a.download = fileName;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
          },
          error => {
            this.alertService.error(error);
          }
        );
    }
  }

  public cancelWorkspacePopUp() {
    this.ngxSmartModalService.getModal(this.cancelConfirmModalFromList).open();
  }

  public transformToEntityModelCancelRequest() {
    const { cancelDecisionComment } = this.workspaceListViewModelFormControls;
    return {
      id: this.workspaceRequestId,
      decisionReason: cancelDecisionComment.value,
    };
  }

  public changeSpecificationForBundleOptions(e) {
    this.isHavingUserVolumeError = false;
    this.OriginalUserVolume = "";
    if (e) {
      this.hourValue = null;
      this.monthValue = null;
      this.monthValueAlwaysOn = null;
      this.selectedSpecificationBundleOptions = e.id;
      const costDataDetails = this.filterstrDataForWindow ? this.filterstrDataForWindow : this.filterstrDataForPlus ? this.filterstrDataForPlus : this.filterstrDataForLinux;

      //with selected bundle specification filter the data
      this.costData = Object.keys(costDataDetails).filter(function (key) {
        return (key.startsWith(e.name));
      }).reduce(function (newData, key) {
        newData[key] = costDataDetails[key];
        return newData;
      }, {});
      const costData2 = this.costData;
      //filter hour value object from data
      const autoStopCostHourData: any = Object.keys(costData2).filter(function (key) {
        return (key.includes("hour"));
      }).reduce(function (newData, key) {
        newData[key] = costData2[key];
        return newData[key];
      }, {});
      this.hourValue = Object.keys(autoStopCostHourData).length > 0 ? (autoStopCostHourData.price).slice(0, -8) : null;

      //filter autostop cost value for month object from data
      const autoStopCostMonthData: any = Object.keys(costData2).filter(function (key) {
        return ((key.includes("month") && (key.includes("AutoStop"))));
      }).reduce(function (newData, key) {
        newData[key] = costData2[key];
        return newData[key];
      }, {});
      //filter autostop cost value for hour object from data
      this.monthValue = Object.keys(autoStopCostMonthData).length > 0 ? (autoStopCostMonthData.price).slice(0, -8) : null;

      const alwaysOnCostMonthData: any = Object.keys(costData2).filter(function (key) {
        return ((key.includes("month") && (key.includes("AlwaysOn"))));
      }).reduce(function (newData, key) {
        newData[key] = costData2[key];
        return newData[key];
      }, {});
      this.monthValueAlwaysOn = Object.keys(alwaysOnCostMonthData).length > 0 ? (alwaysOnCostMonthData.price).slice(0, -8) : null;
      const regex = new RegExp('(\\d+)"*$').exec(e.name);
      this.userVolume = regex[0].toString();
      this.OriginalUserVolume = regex[0].toString();
    } else {
      this.hardwareTabModelForm.patchValue({
        userVolume: ""
      });
      this.userVolume = "";
      this.hourValue = null;
      this.monthValue = null;
      this.monthValueAlwaysOn = null;
    }
  }


  public changeWorkspaceBundleOptions(e) {
    if (e) {
      this.isBundleOptionSelected = true;
      this.selectedBundleOptions = e;
    }
    else {
      this.isBundleOptionSelected = false;
      this.selectedBundleOptions = "";
    }
    this.hardwareTabModelForm.patchValue({
      workspaceType: null,
      region: null,
    });
    this.hourValue = null;
    this.monthValue = null;
    this.monthValueAlwaysOn = null;
    this.isHavingUserVolumeError = false;
    this.userVolume = "";
    this.PlusHeader = [];
    this.WindowHeaderData = [];
    this.LinuxHeader = [];
  }

  public calculatePrice() {
    if (this.enableCostCalculation)
      this.enableCostCalculation = false;
    else
      this.enableCostCalculation = true;
  }

  public onPricingModelChange(value) {
    const pricingModel = value.srcElement.defaultValue;
    this.pricingModelValueBinding(pricingModel);
  }
  private pricingModelValueBinding(value) {
    if (value === PricingModel.Monthly) {
      this.isHourlyPricing = false;
      this.isMonthlyPricing = true;
    }
    if (value === PricingModel.Hourly) {
      this.isMonthlyPricing = false;
      this.isHourlyPricing = true;
    }
  }

  public onValueChangeCalculateTotalPrice() {
    this.dayUsagecount = this.hardwareTabModelForm.get('dayUsage').value;
    this.hourUsagecount = this.hardwareTabModelForm.get('hourUsage').value;
    if (this.dayUsagecount && this.hourUsagecount) {
      this.totalCost = parseFloat(this.monthValue.toString()) + parseFloat((this.hourValue * this.hourUsagecount * this.dayUsagecount).toString());
      this.monthlySavings = parseFloat(this.totalCost.toString()) - parseFloat(this.monthValueAlwaysOn.toString());
    } else {
      this.totalCost = null;
    }
  }
  public onValueChangeCalculateWithAdditionalPrice() {
    this.calculatedAdditionalPrice = 0;
    if (parseFloat(this.OriginalUserVolume) < parseFloat(this.userVolume)) {
      const additionalUserVolume = parseFloat(this.userVolume) - parseFloat(this.OriginalUserVolume);
      this.calculatedAdditionalPrice = parseFloat(this.additionalPrice.toString()) * additionalUserVolume;
    }

  }
  public get hasMonthValueAlwaysOn(): number {
    const result = parseFloat(this.monthValueAlwaysOn.toString()) + parseFloat(this.calculatedAdditionalPrice.toString());
    return result;
  }

  public get hasMonthValue(): number {
    const result = parseFloat(this.monthValue.toString()) + parseFloat(this.calculatedAdditionalPrice.toString());
    return result;
  }

  public get hasHourValue(): number {
    const result = parseFloat(this.hourValue.toString()) + parseFloat(this.calculatedAdditionalPrice.toString());
    return result;
  }

  public get hasTotalCost(): number {
    const result = parseFloat(this.totalCost.toString()) + parseFloat(this.calculatedAdditionalPrice.toString());
    return result;
  }

  public get hasMonthlySavings(): number {
    const result = parseFloat(this.monthlySavings.toString()) + parseFloat(this.calculatedAdditionalPrice.toString());
    return result;
  }

  public onUserVolumeBlur() {
    if (this.userVolume) {
      if (parseInt(this.userVolume) < parseInt(this.OriginalUserVolume)) {
        this.userVolume = this.OriginalUserVolume;
        this.isHavingUserVolumeError = true;
      }
    }
    else {
      this.userVolume = this.OriginalUserVolume;
      this.isHavingUserVolumeError = true;
    }
  }

  public changeWorkspaceBundle(e) {
    let valueSelected: string = "";
    if (e) {
      this.isRegionSelected = true;
      valueSelected = e.name;
    }
    else {
      this.isRegionSelected = false;
      valueSelected = "";
    }

    this.hardwareTabModelForm.patchValue({
      workspaceType: null,
    });
    this.awsSelectedBundle = [];
    this.isHavingUserVolumeError = false;
    this.hourValue = null;
    this.monthValue = null;
    this.monthValueAlwaysOn = null;
    this.userVolume = "";
    this.PlusHeader = [];
    this.WindowHeaderData = [];
    this.LinuxHeader = [];
    this.filterstrDataForPlus = null;
    this.filterstrDataForWindow = null;
    this.filterstrDataForLinux = null;
    this.filterstrDataForWindowHeader = null;
    this.filterstrDataForPlusHeader = null;
    this.filterstrDataForLinuxHeader = null;


    //REGION DROPDOWN
    //filter region list from the Data
    const filtered: any = Object.keys(this.modifiedData.regions)
      .filter(key => valueSelected.includes(key))
      .reduce((obj, key) => {
        obj[key] = this.modifiedData.regions[key];
        return obj[key];
      }, {});

    // ADDITIONAL PRICING
    const objAdditionalPrice = Object.keys(filtered).filter(function (key) {
      return key.indexOf(AWSKeyValues.AdditionalPriceKey) == 0
    }).reduce(function (newData, key) {
      newData[key] = filtered[key];
      return newData;
    }, {});
    this.additionalPrice = objAdditionalPrice[AWSKeyValues.AdditionalPriceKey].price ? objAdditionalPrice[AWSKeyValues.AdditionalPriceKey].price : 0;
    //SPECIFICATION FOR BUNDLE SELECTION DROPDOWN
    if (this.selectedBundleOptions === BundleOption.Windows) {
      //filters data respective to Window bundle
      this.filterstrDataForWindow = Object.keys(filtered).filter(function (key) {
        return key.indexOf('Plus') == 0 || (key.indexOf('Windows') == 0 && !key.endsWith("BYOL"));
      }).reduce(function (newData, key) {
        newData[key] = filtered[key];
        return newData;
      }, {});
      //To avoid repeation of month License and hour license filtering one set of data
      this.filterstrDataForWindowHeaderData = Object.keys(this.filterstrDataForWindow).filter(function (key) {
        return key.endsWith("the month License Included")
      }).reduce(function (newData, key) {
        newData[key] = filtered[key];
        return newData;
      }, {});
      //Crop unwanted details from the Key. This value is to bind in the Specification Dropdown
      this.filterstrDataForWindowHeader = Object.keys(this.filterstrDataForWindowHeaderData);
      this.filterstrDataForWindowHeader.forEach(element => {
        this.WindowHeaderData.push(element.split("GB", 3).join("GB"));
      });

      _.each(Object.keys(this.filterstrDataForWindow), key => {
        _.each(this.awsBundleName, awsbundle => {
          if (key.startsWith(awsbundle.name)) {
            const exisitingbundle = this.awsSelectedBundle
              .find(m => m.name === awsbundle.name);
            if (!exisitingbundle) {
              this.awsSelectedBundle.push(awsbundle);
            }
          }

        });
      });

    } else if (this.selectedBundleOptions === BundleOption.Linux) {
      //filters data respective to Window(BYOUL) bundle
      this.filterstrDataForLinux = Object.keys(filtered).filter(function (key) {
        return key.indexOf('Linux') == 0;
      }).reduce(function (newData, key) {
        newData[key] = filtered[key];
        return newData;
      }, {});
      //To avoid repeation of month License and hour license filtering on set of data
      this.filterstrDataForLinuxHeaderData = Object.keys(this.filterstrDataForLinux).filter(function (key) {
        return key.endsWith("by the month License None")
      }).reduce(function (newData, key) {
        newData[key] = filtered[key];
        return newData;
      }, {});
      //Crop unwanted details from the Key. This value is to bind in the Specification Dropdown
      this.filterstrDataForLinuxHeader = Object.keys(this.filterstrDataForLinuxHeaderData);
      this.filterstrDataForLinuxHeader.forEach(element => {
        this.LinuxHeader.push(element.split("GB", 3).join("GB"));
      });
      _.each(Object.keys(this.filterstrDataForLinux), key => {
        _.each(this.awsBundleName, awsbundle => {
          if (key.startsWith(awsbundle.name)) {
            const exisitingbundle = this.awsSelectedBundle
              .find(m => m.name === awsbundle.name);
            if (!exisitingbundle) {
              this.awsSelectedBundle.push(awsbundle);
            }
          }

        });
      });
    }
  }

  public getRegionList() {
    this.environmentService.getRegionList().subscribe(
      data => {
        if (data) {
          this.workspaceCatalogList = data;
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public getAWSWorkspaceCatalogList() {
    this.environmentService.getAWSWorkspaceCatalogList().subscribe(
      data => {
        if (data) {
          this.modifiedData = JSON.parse(data);
          if (this.isApproverFromList || this.isFromRequestMoreUtility || this.isFromResubmit || this.isDetailFromList) {
            this.modeType = this.isApproverFromList ? PageModeType.Approve : this.isFromRequestMoreUtility ? PageModeType.RequestMoreUtility : this.isFromResubmit ? PageModeType.Resubmit : this.isDetailFromList ? PageModeType.Detail : PageModeType.Create;
            this.getEnvironmentDetails();
            if (this.isApproverFromList || this.isFromRequestMoreUtility) { this.disableFormControls(); }
          }
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public getWorkspaceCatalogList() {
    this.environmentService.getWorkspaceCatalogList().subscribe(
      data => {
        if (data) {
          this.awsBundleName = data;
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public getSoftwaresList() {
    this.environmentService.getSoftwaresList().subscribe(
      data => {
        if (data) {
          this.softwaresList = data;
          this.initSoftwaresGrid(this.softwaresList);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );

  }

  public initSoftwaresGrid(data) {
    var softwareArray = [];
    var list: any = Object.values(_.groupBy(data, 'bundleId'))
    list.forEach(element => {
      var softwareObj: any = {};
      softwareObj.id = element[0].bundleId;
      softwareObj.bundleName = element[0].bundle.name;
      softwareObj.softwareList = _.map(element, function (num) { return num.software.name }).join(",  ");
      softwareArray.push(softwareObj);
    });
    this.softwaresViewModelDataGrid.setRowsData(_.sortBy(softwareArray, 'bundleName'), false);
  }

  private getApprovedUtilities(): void {
    this.approvedUtilityList = [];
    this.environmentService.getApprovedUtilities().subscribe(
      data => {
        if (data) {
          this.approvedUtilityList = data;
          this.initUtilitiesGrid(this.approvedUtilityList);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public initUtilitiesGrid(data) {
    this.utilitiesViewModelDataGrid.setRowsData(data, false);
  }

  public sizeToFit() {
    this.utilitiesViewModelDataGrid.sizeToFit();
    this.softwaresViewModelDataGrid.sizeToFit();
    this.requestedDatasetDataGrid.sizeToFit();
    this.datasetviewModelDataGrid.sizeToFit();
    this.historyViewModelDataGrid.sizeToFit();
  }

  public subscribeToCreateSuccessMessage() {
    this.environmentService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      this.alertService.success(message, toStable);
    });
  }

  public getSettingsDetails(workspaceTagDetail: any): any {
    let settings = {
      singleSelection: workspaceTagDetail.isSingleSelection,
      text: "Select Value",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
    return settings;
  }

  public datasetInitGrid(data) {
    const datasetCatalogs = data ? data : [];
    datasetCatalogs.map(datasetCatalog => {
      if (datasetCatalog.lastModifiedDate) {
        datasetCatalog.lastModifiedDateDisplay = this.getFormattedDate(new Date(datasetCatalog.lastModifiedDate)) + " " + new Date(datasetCatalog.lastModifiedDate).toLocaleTimeString();
      }
    });
    this.datasetviewModelDataGrid.setRowsData(data);
  }

  public getDataSetCatalogList() {
    this.environmentService.getDataSetCatalogList().subscribe(
      data => {
        if (data) {
          this.datasetCatalogList = data;
          this.datasetInitGrid(data);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public addToCart() {
    const selectedRows = this.datasetviewModelDataGrid.getSelectedRows();
    if (selectedRows.length <= 0) {
      this.alertService.error(ValidationMessages.DatasetRequiredMessage, true);
      return;
    }
    else {
      this.datasetviewModelDataGrid.onRemoveSelected(selectedRows);
      this.requestedDatasetDataGrid.onAddSelected(selectedRows);
    }
  }

  public removeFromCart(): void {
    const selectedRows = this.requestedDatasetDataGrid.getSelectedRows();
    if (selectedRows.length <= 0) {
      this.alertService.error(ValidationMessages.DatasetRequiredMessage, true);
      return;
    }
    else {
      this.requestedDatasetDataGrid.onRemoveSelected(selectedRows);
      this.datasetviewModelDataGrid.onAddSelected(selectedRows);
    }
  }

  public submitRequest(): void {
    if (this.softwaresViewModelDataGrid.getSelectedRows().length == 0) {
      this.alertService.error(ValidationMessages.SoftwareRequiredMessage, true);
      return;
    }
    if (this.isFromRequestMoreUtility) {
      if (this.utilitiesViewModelDataGrid.getSelectedRows().length == 0 && this.requestedDatasetDataGrid.getAllRowsId().length == 0) {
        this.alertService.error(ValidationMessages.ForMoreUtilityDataset, true);
        return;
      }
    }
    const objEnvironmentRequest = this.transformToEnvironmentRequestModel();
    this.environmentService.saveEnvironmentRequest(objEnvironmentRequest)
      .subscribe(
        (data) => {
          this.alertService.success(data.message, true);
          this.environmentService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
          this.redirectToList();
        },
        (err) => {
          this.alertService.error(err.error.errorList[0], false);
        });

  }

  private transformToEnvironmentRequestModel(): any {
    let objEnvironmentRequestModel: any = {};
    const environmentTabDetails = this.environmentForm.value;
    if (this.isFromResubmit) {
      objEnvironmentRequestModel.Id = this.environmentId;
    }
    objEnvironmentRequestModel.Name = environmentTabDetails.name;
    objEnvironmentRequestModel.NeededByDate = moment(environmentTabDetails.neededBy).format(config.APPLICATION_DATE_TIME_FORMAT);
    objEnvironmentRequestModel.Comments = environmentTabDetails.reasonForRequest;
    const tempWorkspaceTagTypeDetailsList = this.workspaceTagTypeDetailsList;
    let Tags = [];

    _.each(tempWorkspaceTagTypeDetailsList, workspaceTagTypeDetail => {
      let objTags: any = {};
      if (workspaceTagTypeDetail.selectedTags && workspaceTagTypeDetail.selectedTags.length > 0) {
        _.each(workspaceTagTypeDetail.selectedTags, selectedTag => {
          let tempTagValueUserMapping = [];
          _.each(this.selectedUserIds, selectedUser => {
            const objTaggedUser = _.find(selectedTag.tagValueUserMapping, taggedUser => taggedUser.userId == selectedUser);
            if (objTaggedUser)
              tempTagValueUserMapping.push(objTaggedUser);
          });
          selectedTag.tagValueUserMapping = tempTagValueUserMapping;
        });
        objTags.id = workspaceTagTypeDetail.id;
        objTags.tagValues = workspaceTagTypeDetail.selectedTags;
        objTags.tagTypeContextMappings = workspaceTagTypeDetail.tagTypeContextMapping;
        Tags.push(objTags);
      }

    });
    objEnvironmentRequestModel.TagType = Tags;
    objEnvironmentRequestModel.AwsbundleOption = this.hardwareTabModelForm.controls.bundleOptions.value;
    objEnvironmentRequestModel.Awsregion = this.hardwareTabModelForm.controls.region.value.name;
    objEnvironmentRequestModel.WorkspaceCatalogID = this.hardwareTabModelForm.controls.workspaceType.value;
    objEnvironmentRequestModel.UserVolumeSize = this.hardwareTabModelForm.controls.userVolume.value;
    objEnvironmentRequestModel.PricingModel = this.hardwareTabModelForm.controls.pricingModel.value;
    objEnvironmentRequestModel.WorkspaceUserName = null;
    let arrSoftwareId = [];
    const selectedSoftwareRow = this.softwaresViewModelDataGrid.getSelectedRows();
    _.each(selectedSoftwareRow, selectedSoftware => {
      arrSoftwareId.push(selectedSoftware.id)
    });
    let arrUtilitiesId = [];
    const selectedUtilitiesRow = this.utilitiesViewModelDataGrid.getSelectedRows();
    _.each(selectedUtilitiesRow, selectedUtility => {
      arrUtilitiesId.push(selectedUtility.id)
    });

    objEnvironmentRequestModel.Softwares = arrSoftwareId;
    objEnvironmentRequestModel.Utilities = arrUtilitiesId;
    objEnvironmentRequestModel.Datasets = this.requestedDatasetDataGrid.getAllRowsId();
    objEnvironmentRequestModel.Users = this.selectedUserIds;

    return objEnvironmentRequestModel;
  }

  public onCancel(): void {
    if ((this.environmentForm.dirty || this.hardwareTabModelForm.dirty) && this.modeType !== "Detail") {
      this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
      this.ngxSmartModalService.open(this.cancelConfirmModal);
    }
    else if(this.isRedirectDashboard) {
      this.router.navigateByUrl(ROUTES_CONFIG_MAP.DASHBOARD.BASE_URL);
    } else {
      this.navigateToList();
    }
  }

  public onCancelConfirm(): void {
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
    this.ngxSmartModalService.close(this.cancelConfirmModal);
    if(this.isRedirectDashboard) {
      this.router.navigateByUrl(ROUTES_CONFIG_MAP.DASHBOARD.BASE_URL);
    } else {
      this.navigateToList();
    }
  }


  private async navigateToList() {
    return this.router.navigate(["environment/list"]);
  }

  public showSelectedSoftwares() {
    this.selectedSoftWares = this.softwaresViewModelDataGrid.getSelectedRows();
    this.softwaresViewModelDataGrid.setRowsData(this.selectedSoftWares, false);
    var selectedSoftwareId: [] = _.map(this.selectedSoftWares, (item) => {
      return item.id;
    })
    setTimeout(() => {
      this.softwaresViewModelDataGrid.gridOptions.api.forEachNode(node => _.contains(selectedSoftwareId, node.data.id) ? node.setSelected(true) : 0);
    }, 1000)
  }

  public clearSoftwareFilter() {
    this.selectedSoftWares = this.softwaresViewModelDataGrid.getSelectedRows();
    this.initSoftwaresGrid(this.softwaresList);
    var selectedSoftwareId: [] = _.map(this.selectedSoftWares, (item) => {
      return item.id;
    })
    setTimeout(() => {
      this.softwaresViewModelDataGrid.gridOptions.api.forEachNode(node => _.contains(selectedSoftwareId, node.data.id) ? node.setSelected(true) : 0);
    }, 1000)
  }

  public showSelectedUtilities() {
    this.selectedUtilities = this.utilitiesViewModelDataGrid.getSelectedRows();
    this.utilitiesViewModelDataGrid.setRowsData(this.selectedUtilities, false);
    var selectedUtilityId: [] = _.map(this.selectedUtilities, (item) => {
      return item.id;
    })
    setTimeout(() => {
      this.utilitiesViewModelDataGrid.gridOptions.api.forEachNode(node => _.contains(selectedUtilityId, node.data.id) ? node.setSelected(true) : 0);
    }, 1000)
  }

  public clearUtilityFilter() {
    this.utilitiesViewModelDataGrid.setRowsData(this.approvedUtilityList, false);
    var selectedUtilityId: [] = _.map(this.selectedUtilities, (item) => {
      return item.id;
    })
    setTimeout(() => {
      this.utilitiesViewModelDataGrid.gridOptions.api.forEachNode(node => _.contains(selectedUtilityId, node.data.id) ? node.setSelected(true) : 0);
    }, 1000)
  }

  public bindSelectedValuesBinding() {
    if (this.isApproverFromList || this.isDetailFromList) { this.showSelectedUtilities(); }
    this.showSelectedSoftwares();
  }

  private getWorkspaceTagTypeDetails(): void {
    this.environmentService.getWorkspaceTagTypeDetails("Environment").subscribe(
      data => {
        if (data) {
          this.originalTagTypeDetails = data;
          _.each(data, item => {
            item.selectedTags = [];
            item.selectedTagValues = [];
            item.tagValues = _.filter(item.tagValues, function (tag) { return tag.tagValueUserMapping.length > 0; });
            _.each(item.tagValues, tagValue => {
              tagValue.itemName = tagValue.value;
            });

            if (item.tagValues.length > 0)
              this.workspaceTagTypeDetailsList.push(item);
          });
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  private getFormattedDate(date) {
    const year = date.getFullYear();
    let month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    let day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;
  }

  public errorForEnvironment(propertyName: string): string | void {
    if (propertyName) {
      const property = propertyName.replace(/\w+/g, (w) => w[0].toUpperCase() + w.slice(1).toLowerCase());

      if (this.environmentForm.controls[propertyName]) {
        if (this.environmentForm.controls[propertyName].hasError("required")) {
          return ValidationMessages.RequiredMessage;
        }
        else if (this.environmentForm.controls[propertyName].hasError("minlength")
          || this.environmentForm.controls[propertyName].hasError("maxlength")) {
          return property.concat(ValidationMessages.MinMaxLengthMessage);
        }
        else {
          return null;
        }
      }
    }
  }

  public errorForHardware(propertyName: string): string | void {
    if (propertyName) {
      const property = propertyName.replace(/\w+/g, (w) => w[0].toUpperCase() + w.slice(1).toLowerCase());

      if (this.hardwareTabModelForm.controls[propertyName]) {
        if (this.hardwareTabModelForm.controls[propertyName].hasError("required")) {
          return ValidationMessages.RequiredMessage;
        }
        else if (this.hardwareTabModelForm.controls[propertyName].hasError("minlength")
          || this.hardwareTabModelForm.controls[propertyName].hasError("maxlength")) {
          return property.concat(ValidationMessages.MinMaxLengthMessage);
        }
        else {
          return null;
        }
      }
    }
  }

  private redirectToList() {
    this.environmentService.environmentIdSubject.next();
    this.router.navigate(["environment/list"]);
  }

  public get envDetailForm() {
    return this.environmentForm.controls;
  }

  public get hardwareDetailForm() {
    return this.hardwareTabModelForm.controls;
  }

  public validateCurrentTabForm(tabIndex: number) {
    if (this.modeType !== PageModeType.Create) {
      return true;
    }
    var isValid = false;
    const tabName = this.tabIndexKeys[tabIndex];
    switch (tabName) {
      case 'Environment': isValid = !this.environmentForm.invalid; this.environmentForm.markAllAsTouched(); break;
      case 'Hardware': isValid = !this.hardwareTabModelForm.invalid; this.hardwareTabModelForm.markAllAsTouched(); break;
      case 'Datasets': isValid = !this.datasetForm.invalid; this.datasetForm.markAllAsTouched(); break;
      case 'Software': isValid = this.softwaresViewModelDataGrid.getSelectedRows().length > 0; break;
      default: isValid = true;
    }

    return isValid;
  }

  getTabIndex(tabName: string): number {
    switch (tabName) {
      case 'Environment': return 0;
      case 'Hardware': return 1;
      case 'Software': return 2;
      case 'Utilities': return 3;
      case 'Datasets': return 4;
      case 'History': return 5;
      default: return -1; // return -1 if clicked text is not a tab label text
    }
  }

  public isEnableSubmitButton() {
    return this.modeType === 'Create' ? !(this.environmentForm.valid && this.environmentForm.dirty && this.hardwareTabModelForm.valid && this.hardwareTabModelForm.dirty && this.softwaresViewModelDataGrid.getSelectedRows().length > 0) : false;
  }

  public onPopUpClose(): void {
    this.ngxSmartModalService.getModal(this.cancelConfirmModal).close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }

  public trimEnvironmentName(): void {
    this.environmentForm.controls['name'].setValue(this.environmentForm.controls.name.value.trim());
  }

}
