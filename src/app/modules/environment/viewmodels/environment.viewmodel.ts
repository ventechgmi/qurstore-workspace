
import { map } from "rxjs/operators";
import { EntityBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { Observable, Subject, of } from "rxjs";
import { AbstractControl, FormControl, ValidatorFn } from "@angular/forms";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ActivatedRoute } from "@angular/router";
import { EnvironmentService } from '../services/environment.service';
import { AuthService } from 'src/app/utils/auth.service';
import { ROUTES_CONFIG_MAP, MODULE_BASE_URL_MODULE_ID_MAP } from "src/app/utils/config";

export class EnvironmentViewModel extends EntityBaseViewModel<"", EnvironmentService> {
    public moduleId: string;
    public moduleAccess;
    public moduleInfo;
    public updateVendorListSubject = new Subject<any>();
    public vendorDetailSelection = new Subject<any>();
    auditLogPermission: any = {};

    public constructor(
        public environmentService: EnvironmentService,
        public viewModelType: ViewModelType,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute,
        public authService: AuthService
    ) {
        super(environmentService, viewModelType);
        this.entityName = "environment";
    }

    public create(): Observable<any> {
        // add any customization logic for create
        return super.create().pipe(map(data => data));
    }

    // custom methods
    public getUpdateVendorListObservable(): Observable<any> {
        return this.updateVendorListSubject.asObservable();
    }

    public getUpdateVendorListSubject(): Subject<any> {
        return this.updateVendorListSubject;
    }

    public getVendorDetailObservable(): Observable<any> {
        return this.vendorDetailSelection.asObservable();
    }

    public getVendorDetailSelectionSubject(): Subject<any> {
        return this.vendorDetailSelection;
    }

    // public nameUniqueValidator(vendorName): Observable<any> {
    //     return this.workspaceService.getvendorByName(vendorName).pipe(map(res => res));
    // }

    public noWhitespaceValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            // messy but you get the idea
            const isWhitespace = (control.value || "").trim().length === 0;
            const isValid = !isWhitespace;
            return isValid ? null : { hasOnlyWhiteSpace: true };
        };
    }


    /*
    This method validates the below
    1. validates the control has only white spaces since most of the name fields should not allow just white spaces
    2. validates the control whether it has only special characters.

    */
    public validateSpecialCharactersAndWhiteSpace(lengthMin: number, lengthMax: number): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            // const pattern = "[^A-Za-z0-9||||\.||_]";
            const pattern = "[A-Za-z0-9]";
            const whiteSpace = /^\s*$/;
            if ((control.value || "").length === 0) {
                return null;
            } else if ((control.value || "").trim().length < lengthMin) {
                return { minLength: true };
            } else if (control.value.match(whiteSpace)) {
                return { hasOnlyWhiteSpace: true };
            } else if (!control.value.match(pattern)) { // check if the input doest not have any alphabets or in other words input has only special characters
                // return "Don't enter only special characters";
                return { hasOnlySpecialCharacters: true };
            }
        };
    }

    public getUserClaims() {
        const baseUrl = ROUTES_CONFIG_MAP.ENVIRONMENT.BASE_URL;
        this.moduleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
        this.moduleInfo = this.authService.getModuleAccess(this.moduleId) || null;
        this.moduleAccess = this.moduleInfo ? this.moduleInfo.RolePermissions : {};

        return this.moduleAccess;
    }
}
