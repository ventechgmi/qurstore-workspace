import { from as observableFrom, Observable, Subject, forkJoin } from "rxjs";
import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ListBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { Router, ActivatedRoute } from "@angular/router";
import * as _ from "underscore";
import moment from 'moment';
import { DataGridViewModel } from "../../core-ui-components";
import { NgxSmartModalService } from "ngx-smart-modal";
import Highcharts, { color } from 'highcharts';
import { ClientSideRowModelModule } from '@ag-grid-community/client-side-row-model';
import { MasterDetailModule } from '@ag-grid-enterprise/master-detail';
import { MenuModule } from '@ag-grid-enterprise/menu';
import { ColumnsToolPanelModule } from '@ag-grid-enterprise/column-tool-panel';

import { AlertService } from "../../../utils/alert/alert.service";
import { EnvironmentService } from "../services/environment.service";
import { EnvironmentViewModel } from "./environment.viewmodel";
import { AuthService } from "src/app/utils/auth.service";
import config, { GridConfig, ROUTES_CONFIG_MAP, MODULE_BASE_URL_MODULE_ID_MAP, ENVIRONMENT_CHART_COLORS } from "../../../utils/config";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { EnvironmentRequestNavigationService } from "../../core-ui-services/environment-request-navigation.service";
import { DashboardWidgetStatus } from "../helpers/constants";

@Injectable()
export class EnvironmentListViewModel extends EnvironmentViewModel {
  public environmentId: string;
  public environmentRequestHeaderDetailId: string;
  public userEnvironmentId: string;
  public selectedUserId: string;
  public environmentRequestList: any;
  public alertService: AlertService;
  public viewModelDataGrid: DataGridViewModel;
  public beneficiaryViewModelDataGrid: DataGridViewModel;
  public bundleOptions = ["Linux Bundle Options", "Windows Bundle Options"]
  public workspaceCatalogList: any;
  public modifiedData;
  public userRolePermission: any = {};
  public selectedBundleOption: string = "";
  public selectedRegion: string = "";
  private statusPendingApproval: string = "Submitted - Waiting for approval";
  private statusApproved: string = "Approved";
  private statusRejected: string = "Rejected";
  private statusTerminated: string = "Terminated";
  private statusPending: string = 'Not Started';
  private statusAvailable: string = "Available";
  private statusStopped: string = "Stopped";
  private statusRequestedChanges: string = "Requested for Information";
  private statusRequestChanges: string = "Request Changes";
  private actionApproveReject: string = "Approve/Reject";
  private actionCancel: string = "CancelRequest";
  private actionRequestMoreUtilities: string = "RequestMoreUtilities";

  private actionViewComments: string = "ViewComments";
  private beneficiaryPopup: string = "beneficiarymModal";
  private terminateConfirmationModal: string = "terminateConfirmationModal";

  public isBundleOptionSelected = false;
  private selectedBundleOptions: string;
  public message: string;
  public xAxisValues: any = [];
  public chartData: any = [];
  public highcharts = Highcharts;
  public isRevokingFileAccess: boolean = false;
  public chart: any;
  public data = [{
    name: "",
    showInLegend: false,
    data: []
  }];
  public chartOptions = {
    chart: {
      type: "column"
    },
    title: {
      text: ""
    },
    xAxis: {
      categories: []
    },
    yAxis: {
      title: {
        text: ""
      },
      labels: {
        overflow: 'justify'
      }
    },
    plotOptions: {
      column: {
        dataLabels: {
          enabled: true
        }
      }
    },
    series: this.data
  };
  public columnDefs;
  public defaultColDef;
  public detailCellRendererParams;
  public rowData: any;
  public gridApi;
  public gridColumnApi;
  public beneficiaryHeader = [
    { headerName: "Name", field: "name" },
    { headerName: "User Name", field: "userName" },
    { headerName: "Action", field: "action" }
  ];
  public enableClearFilterButton: boolean = false;
  public statusForFilter: string;
  public userId: string;

  public constructor(
    public environmentService: EnvironmentService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public authService: AuthService,
    private ngxService: NgxUiLoaderService,
    public environmentRequestNavigationService: EnvironmentRequestNavigationService
  ) {
    super(environmentService, ViewModelType.Edit, ngxSmartModalService, route, authService);
    this.viewModelDataGrid = new DataGridViewModel();
    this.viewModelDataGrid.setPagination(true);
    this.beneficiaryViewModelDataGrid = new DataGridViewModel();
    this.initializeGridColDef();
    this.viewModelDataGrid.setPagination(true);
    this.initializeBeneficiaryGridColDef();
  }

  public init(param?) {
    this.statusForFilter = param?.status ? param.status : ""; // param.status is throwing error while list component is loading. So I have changed the param.status to param?.status
    this.environmentId = param?.environmentId;
    this.userId = this.authService.getUserId();
    this.userRolePermission = this.getUserClaims() || {};
    this.subscribeToCreateSuccessMessage();
    this.subscribers();
    this.setDataTableConfigurations();
    this.getEnvironmentList();
    this.getRegionList();
    return observableFrom([]);
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  public initializeGridColDef() {

    const name = {
      headerName: "Environment Name",
      field: "name",
      editable: false,
      sortable: true,
      filter: true,
      cellRenderer: (param: any) => {
        const loggedUserId = this.userId;
        if (this.userRolePermission?.canExecute && param.data.status === this.statusPendingApproval && param.data.requestedById != loggedUserId)
          return `<a routerLink="" class ="hyperLink" data-action-type="approveRejectClicked" aria-hidden="true" title = ` + param.value + `>` + param.value + `</a>`;
        else if (this.userRolePermission?.canUpdate && param.data.status === this.statusRequestChanges && loggedUserId === param.data.requestedById)
          return `<a routerLink="" class ="hyperLink" data-action-type="resubmitClicked" aria-hidden="true" title = ` + param.value + `>` + param.value + `</a>`;
        else
          return `<a routerLink="" class ="hyperLink" data-action-type="columnClicked" aria-hidden="true" title = ` + param.value + `>` + param.value + `</a>`;
      }
    };
    this.viewModelDataGrid.columnDefs.push(name);

    const neededBy = {
      headerName: "Needed By",
      field: "neededByDate",
      editable: false,
      isDate: true
    };
    this.viewModelDataGrid.columnDefs.push(neededBy);

    const requestedBy = {
      headerName: "Requested By",
      field: "requestedBy",
      editable: false
    };
    this.viewModelDataGrid.columnDefs.push(requestedBy);

    // const awsBundleOption = {
    //   headerName: "Requested By",
    //   field: "awsBundleOption",
    //   editable: false
    // };
    // this.viewModelDataGrid.columnDefs.push(awsBundleOption);

    // const awsRegion = {
    //   headerName: "Requested By",
    //   field: "awsRegion",
    //   editable: false
    // };
    // this.viewModelDataGrid.columnDefs.push(awsRegion);


    const requestedDateTime = {
      headerName: "Requested Date",
      field: "requestedDateTime",
      sortable: true,
      editable: false,
      isDate: true,
      sort: 'desc',
      comparator: this.viewModelDataGrid.customDateSort
    };
    this.viewModelDataGrid.columnDefs.push(requestedDateTime);

    const beneficiary = {
      headerName: "Beneficiary",
      field: "beneficiary",
      editable: false,
      cellRenderer: (params: any) => {
        return `<i class="fa fa-users" class="data-grid-padding-right" data-action-type="beneficiaryClicked" aria-hidden="true"></i>`;
      }
    }
    this.viewModelDataGrid.columnDefs.push(beneficiary);

    const status = {
      headerName: "Status",
      field: "status",
      editable: false
    };
    this.viewModelDataGrid.columnDefs.push(status);

    const actionBy = {
      headerName: "Action By",
      field: "actionBy",
      editable: false
    };
    this.viewModelDataGrid.columnDefs.push(actionBy);

    const actionDate = {
      headerName: "Action Date",
      field: "actionDate",
      editable: false,
      isDate: true
    };
    this.viewModelDataGrid.columnDefs.push(actionDate);

    const action = {
      headerName: 'Action',
      field: 'action',
      editable: false,
      cellRenderer: (params: any) => {
        let template = ``;
        if (params.value === this.actionCancel)
          template = `<a routerLink="" class="data-grid-padding-right" ><img alt="Cancel Request"  title="Cancel Request"  src="../../../assets/images/cancel.png" class="data-grid-image-medium" data-action-type="cancelRequestClicked"/></a>`;
        else if (params.value === this.actionApproveReject)
          template = `<a routerLink="" class="data-grid-padding-right" ><img alt="Approve/Reject" title="Approve/Reject" src="../../../assets/images/icons8-clipboard-approve-32.png" class="data-grid-image-medium" data-action-type="approveRejectClicked"/></a>`;
        else if (params.value === this.actionRequestMoreUtilities)
          template = `<a routerLink=""  class="data-grid-padding-right"><img alt="Request More Utilities/Datasets"  title="Request More Utilities/Datasets"  src="../../../assets/images/Request.png" class="data-grid-image-medium" data-action-type="requestMoreUtilityOrDataSetClicked"/></a>`;
        else if (params.value === this.statusRequestChanges)
          template = `<a routerLink=""  class="data-grid-padding-right" ><img alt="Resubmit Request"  title="Resubmit Request"  src="../../../assets/images/resubmit.png" class="data-grid-image-medium" data-action-type="resubmitClicked"/></a>`;

        return template;

      }
    };
    this.viewModelDataGrid.columnDefs.push(action);

    this.viewModelDataGrid.defaultSortModel = [
      {
        colId: 'requestedDateTime',
        sort: 'desc',
      }
    ];
  }

  public initializeBeneficiaryGridColDef() {

    const name = {
      headerName: "Name",
      field: "name",
      editable: false,
      sortable: true,
      filter: true
    };
    this.beneficiaryViewModelDataGrid.columnDefs.push(name);

    const userName = {
      headerName: "User Name",
      field: "userName",
      editable: false
    };
    this.beneficiaryViewModelDataGrid.columnDefs.push(userName);

    const workspaceStatus = {
      headerName: "Workspace Status",
      field: "workspaceStatus",
      editable: false,
      cellRenderer: (params: any) => {
        return params.data.currentStatus
      }
    };
    this.beneficiaryViewModelDataGrid.columnDefs.push(workspaceStatus);

    const action = {
      headerName: "Action",
      field: "action",
      editable: false,
      cellRenderer: (params: any) => {
        const loggedUserId = this.userId;
        if (params.data.currentStatus !== this.statusTerminated && (params.data.currentStatus === this.statusAvailable || params.data.currentStatus === this.statusStopped) && (params.data.EnvironmentDetails.currentStatus.name === this.statusApproved || params.data.currentStatus === this.statusStopped) && loggedUserId === params.data.requestedById) {
          return `<a routerLink="" class="data-grid-padding-right" ><img alt="Request More Utilities/Dataset"  title="Request More Utilities/Datasets"  src="../../../assets/images/Request.png" class="data-grid-image-medium" data-action-type="requestMoreUtilityOrDataSetClicked"/></a>
        <a routerLink="" class="data-grid-padding-right"><img alt="Terminate Workspace"  title="Terminate Workspace"  src="../../../assets/images/terminate.png" class="data-grid-image-medium" data-action-type="terminateRequestClicked"/></a>`;
        }
      }
    }
    this.beneficiaryViewModelDataGrid.columnDefs.push(action);

  }

  onFirstDataRendered(params) {
    setTimeout(function () {
      params.api.getDisplayedRowAtIndex(1).setExpanded(true);
    }, 0);
  }

  private subscribers() {
    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.columnClicked:
          this.environmentId = e.eventData.rowDetails.id;
          this.redirectToDetail();
          break;
        case GridConfig.deleteClicked:
          this.environmentId = e.eventData.rowDetails.id;
          break;
        case GridConfig.approveRejectClicked:
          this.environmentId = e.eventData.rowDetails.id;
          this.redirectToApprove();
          break;
        case GridConfig.cancelRequestClicked:
          this.environmentId = e.eventData.rowDetails.id;
          this.ngxSmartModalService.open("cancelConfirmModal");
          this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
          break;
        case GridConfig.requestMoreInfoClicked:
          this.environmentId = e.eventData.rowDetails.id;
          this.redirectToCreateToAddMoreutilityAndDatasets(true);
          break;
        case GridConfig.beneficiaryClicked:
          this.environmentId = e.eventData.rowDetails.id;
          this.openBeneficiaryPopup(e.eventData.rowDetails);
          break;
        case GridConfig.requestMoreUtilityOrDataSetClicked:
          this.environmentId = e.eventData.rowDetails.id;
          this.redirectToCreateToAddMoreutilityAndDatasets(false);
          break;
        case GridConfig.resubmitClicked:
          this.environmentId = e.eventData.rowDetails.id;
          this.redirectToResubmit();
          break;
        default:
          if (e.eventData && e.eventData.rowDetails) {
            this.environmentId = e.eventData.rowDetails.id;
          }
      }
    }, error => { });


    this.beneficiaryViewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.terminateRequestClicked:
          this.environmentId = e.eventData.rowDetails.EnvironmentDetails.id;
          this.environmentRequestHeaderDetailId = e.eventData.rowDetails.EnvironmentDetails.environmentRequestDetails[0].id;
          this.userEnvironmentId = e.eventData.rowDetails.EnvironmentDetails.environmentRequestDetails[0].userEnvironment.id;
          this.openTerminateConfirmationPopup();
          break;
        case GridConfig.requestMoreUtilityOrDataSetClicked:
          this.environmentId = e.eventData.rowDetails.EnvironmentDetails.id;
          this.environmentRequestHeaderDetailId = e.eventData.rowDetails.EnvironmentDetails.environmentRequestDetails[0].id;
          this.userEnvironmentId = e.eventData.rowDetails.id;
          this.selectedUserId = e.eventData.rowDetails.userId;
          this.redirectToCreateToAddMoreutilityAndDatasets(false);
          break;
        case GridConfig.resubmitClicked:
          this.environmentId = e.eventData.rowDetails.EnvironmentDetails.id;
          this.redirectToResubmit();
          break;
        default:
      }
    }, error => { });
  }

  public redirectToResubmit(): void {
    const data = { isFromRequestMoreUtility: false, isApproverFromList: false, environmentId: this.environmentId, isFromResubmit: true };
    localStorage.setItem("redirectData", JSON.stringify(data));
    this.environmentRequestNavigationService.navigateUsingProxy({
      url: "/environment/request",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { environmentId: this.environmentId }
    }).catch();
  }

  public redirectToCreateToAddMoreutilityAndDatasets(isForAllUsers: boolean): void {
    this.selectedUserId = isForAllUsers ? "" : this.selectedUserId;
    const data = { isFromRequestMoreUtility: true, isApproverFromList: false, environmentId: this.environmentId, selectedUserId: this.selectedUserId };
    localStorage.setItem("redirectData", JSON.stringify(data));
    this.environmentRequestNavigationService.navigateUsingProxy({
      url: "/environment/request",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { environmentId: this.environmentId }
    }).catch();
  }

  public redirectToApprove() {
    const data = { isFromRequestMoreUtility: false, isApproverFromList: true, environmentId: "" };
    localStorage.setItem("redirectData", JSON.stringify(data));
    this.environmentService.environmentIdSubject.next(this.environmentId);
    this.environmentRequestNavigationService.navigateUsingProxy({
      url: "/environment/request",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { environmentId: this.environmentId }
    }).catch();
  }

  public redirectToDetail() {
    const data = { isFromRequestMoreUtility: false, isApproverFromList: false, isDetailFromList: true, environmentId: "" };
    localStorage.setItem("redirectData", JSON.stringify(data));
    this.environmentRequestNavigationService.navigateUsingProxy({
      url: "/environment/request",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { environmentId: this.environmentId }
    }).catch();
  }

  public redirectToCreate() {
    const data = { isFromRequestMoreUtility: false, isApproverFromList: false, environmentId: "" };
    localStorage.setItem("redirectData", JSON.stringify(data));
    this.environmentService.environmentIdSubject.next();
    this.environmentRequestNavigationService.navigateUsingProxy({
      url: "/environment/request",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: {}
    }).catch();
  }

  public setDataTableConfigurations() {
    this.viewModelDataGrid.dataTableConfiguration = {
      destroy: true,
      columnDefs: [{ targets: [0], visible: false, searchable: false }, { type: "num-html", targets: [2] }, { orderable: false, targets: 2, searchable: false }],
      order: [[1, "asc"]]
    };
  }

  public getEnvironmentList(bundleOption = null, region = null) {
    this.ngxService.start();
    const loggedUserId = this.userId;
    this.environmentService.getEnvironmentList(bundleOption, region).subscribe(
      data => {
        if (data) {
          this.ngxService.stop();
          this.environmentRequestList = [];
          _.each(data, (env) => {
            let objEnvironment: any = {};
            objEnvironment.rowDetails = env;
            objEnvironment.id = env.id;
            objEnvironment.name = env.name;
            objEnvironment.neededByDate = env.neededByDate ? moment(env.neededByDate).format(config.APPLICATION_DATE_FORMAT) : "-";
            objEnvironment.requestedById = env.environmentRequestHeaderStatusAudits[0].actionBy;
            objEnvironment.requestedBy = env.environmentRequestHeaderStatusAudits[0].actionByNavigation?.fullName;
            objEnvironment.requestedDateTime = env.requestedDateTime ? moment(env.requestedDateTime).format(config.APPLICATION_DATE_TIME_FORMAT) : "-";
            objEnvironment.beneficiary = env.environmentRequestDetails;
            objEnvironment.status = env.currentStatus.name;
            // objEnvironment.awsBundleOption = env.environmentRequestDetails[0].workspaceRequests[0]?.awsbundleOption;
            // objEnvironment.awsRegion = env.environmentRequestDetails[0].workspaceRequests[0]?.awsregion;
            const lastEnvObj = _.last(env.environmentRequestHeaderStatusAudits);
            objEnvironment.actionById = lastEnvObj?.actionBy;
            objEnvironment.actionBy = lastEnvObj?.actionByNavigation?.fullName;
            objEnvironment.actionDate = lastEnvObj?.actionDate ? moment(lastEnvObj?.actionDate).format(config.APPLICATION_DATE_TIME_FORMAT) : "-";

            if (loggedUserId == objEnvironment.requestedById) {
              if (this.userRolePermission?.canUpdate && objEnvironment.status === this.statusPendingApproval)
                objEnvironment.action = this.actionCancel;
              else if (this.userRolePermission?.canUpdate && objEnvironment.status === this.statusApproved)
                objEnvironment.action = this.actionRequestMoreUtilities;
              else if (this.userRolePermission?.canUpdate && objEnvironment.status === this.statusRequestChanges)
                objEnvironment.action = this.statusRequestChanges;
            }
            else {
              if (this.userRolePermission?.canExecute && objEnvironment.status === this.statusPendingApproval)
                objEnvironment.action = this.actionApproveReject;
            }

            this.environmentRequestList.push(objEnvironment);
          });

          if (this.statusForFilter && this.statusForFilter !== "") {
            this.enableClearFilterButton = true;
            let filteredListStatus = [];
            if(this.statusForFilter == DashboardWidgetStatus.Submitted_Waiting_For_Approval) {
              filteredListStatus = this.environmentRequestList.filter(e => e.status === this.statusForFilter && e.requestedById !== this.userId);
            } else {
              filteredListStatus = this.environmentRequestList.filter(e => e.status === this.statusForFilter);
            }
            this.initGrid(filteredListStatus);
            this.bindChartData(filteredListStatus);
          } else {
            this.initGrid(this.environmentRequestList);
            this.bindChartData(this.environmentRequestList);
          }

        }
      },
      error => {
        this.alertService.error(error);
        this.ngxService.stop();
      }
    );
  }

  public initGrid(data) {
    const environmentRequests = data ? data : [];
    this.viewModelDataGrid.setRowsData(environmentRequests);
  }

  public clearFilter(): void {
    this.statusForFilter = "";
    this.enableClearFilterButton = false;
    this.getEnvironmentList();
  }

  public subscribeToCreateSuccessMessage() {
    this.environmentService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      this.alertService.success(message, toStable);
    });
  }

  public changeWorkspaceBundleOptions(e) {
    if (e) {
      this.isBundleOptionSelected = true;
      this.selectedBundleOptions = e;
    }
    else {
      this.isBundleOptionSelected = false;
      this.selectedBundleOptions = "";
    }
  }

  public getRegionList() {
    this.environmentService.getRegionList().subscribe(
      data => {
        if (data) {
          this.workspaceCatalogList = data;
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  private bindChartData(data): void {
    let grouped_data = _.groupBy(data, "status");
    this.xAxisValues = [];
    this.chartData = [];
    _.each(grouped_data, (value, key) => {
      let objChartData: any = {};
      objChartData.name = key;
      objChartData.y = value.length;

      switch (key) {
        case this.statusApproved:
          objChartData.color = ENVIRONMENT_CHART_COLORS.APPROVED;
          break;
        case this.statusPendingApproval:
          objChartData.color = ENVIRONMENT_CHART_COLORS.PENDINGAPPROVAL;
          break;
        case this.statusRejected:
          objChartData.color = ENVIRONMENT_CHART_COLORS.REJECTED;
          break;
        case this.statusRequestedChanges:
          objChartData.color = ENVIRONMENT_CHART_COLORS.REQUESTEDCHANGES;
          break;
        default:
      }
      this.xAxisValues.push(key);
      this.chartData.push(objChartData);
    });
    this.chartOptions = {
      chart: {
        type: "column"
      },
      title: {
        text: ""
      },
      xAxis: {
        categories: this.xAxisValues
      },
      yAxis: {
        title: {
          text: "Count"
        },
        labels: {
          overflow: 'justify'
        }

      },
      plotOptions: {
        column: {
          dataLabels: {
            enabled: true
          }
        }
      },
      series: [{ name: "Environment Requests", data: this.chartData, showInLegend: false }]
    };
  }

  private openBeneficiaryPopup(data: any): void {
    let arrBeneficiary: any = [];
    _.each(data.rowDetails.environmentRequestDetails, (value, key) => {

      let objBeneficiary: any = {};
      objBeneficiary.rowDetails = value;
      objBeneficiary.name = value.userEnvironment.user.fullName;
      objBeneficiary.userName = value.userEnvironment?.workspaceRequests[0].workspaceUserName;
      objBeneficiary.EnvironmentDetails = data.rowDetails;
      objBeneficiary.requestedById = data.requestedById;
      objBeneficiary.userId = value.userEnvironment.userId;
      objBeneficiary.userEnvironmentId = value.userEnvironment.id;
      objBeneficiary.currentStatus = value.userEnvironment.status?.name;
      arrBeneficiary.push(objBeneficiary);

    });
    this.beneficiaryViewModelDataGrid.gridOptions.rowHeight = 80;
    this.beneficiaryViewModelDataGrid.setRowsData(arrBeneficiary);
    this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
    this.ngxSmartModalService.open(this.beneficiaryPopup);
  }

  public onCancelConfirm(value: string): void {
    this.ngxSmartModalService.close("cancelConfirmModal");
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
    let updateStatusObject = {
      EnvironmentRequestHeaderDetailId: this.environmentId,
      ActionComments: "",
      Id: this.environmentId,
      StatusName: value,
      workspaceRequest: []
    }
    this.environmentService.UpdateEnvironmentRequestHeaderStatus(updateStatusObject)
      .subscribe((data) => {
        this.alertService.success(data.message, false);
        this.environmentService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
        this.getEnvironmentList();
      },
        (err) => {
          this.alertService.error(err.error.errorList[0], false);
        });
  }
  private openTerminateConfirmationPopup(): void {
    this.ngxSmartModalService.open(this.terminateConfirmationModal);
    this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
    this.isRevokingFileAccess = false;
  }
  public onTerminateConfirm(value: string): void {

    this.ngxSmartModalService.close(this.terminateConfirmationModal);
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
    let terminateRequest = {
      EnvironmentRequestHeaderDetailId: this.environmentRequestHeaderDetailId,
      Id: this.environmentId,
      StatusName: value,
      userEnvironmentId: this.userEnvironmentId,
      isRevokingFileAccess: this.isRevokingFileAccess
    }
    this.environmentService.TerminateWorkspace(terminateRequest)
      .subscribe((data) => {
        this.alertService.success(data.message, false);
        this.environmentService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
        this.getEnvironmentList();
      }, (err) => {
        this.alertService.error(err.error.errorList[0], false);
      });
    this.ngxSmartModalService.close(this.beneficiaryPopup);
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }
  public getEnvironmentRequestByBundleOption(bundleOption) {
    this.selectedBundleOption = bundleOption;
    this.getEnvironmentList(this.selectedBundleOption, this.selectedRegion);
  }

  public getEnvironmentRequestByRegion(region) {
    this.selectedRegion = (region) ? region.name : null;
    this.getEnvironmentList(this.selectedBundleOption, this.selectedRegion);
  }

  public onPointSelect(event) {
    let envList = _.filter(this.environmentRequestList, item => item.status.toLowerCase() == event.point.name.toLowerCase());
    this.enableClearFilterButton = true;
    this.statusForFilter = event.point.name;
    this.initGrid(envList);
    this.bindChartData(envList);
  }


  public onPopUpClose(modal: string): void {
    this.ngxSmartModalService.getModal(modal).close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }

}
