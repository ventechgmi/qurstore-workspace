import { map } from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
@Injectable({
    providedIn: "root"
})
export class EnvironmentService extends EntityService {
    appConfig: AppConfig;
    onUpdateListEvent = new EventEmitter();
    public environmentIdSubject = new ReplaySubject<any>();
    public showSuccessMessage = new Subject<any>();
    public alertInfo = { message: "", type: "" };
    private afterInitSubject = new BehaviorSubject(this.alertInfo);
    public isApproverFromList = new BehaviorSubject(false);
    public isAddingBeneficiary = new BehaviorSubject(false);

    // public isEditFromList = false;
    // public isApproverFromList = false;
    // public isAddingBeneficiary = false;


    constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
        super(http, "environment", appConfig);
        this.appConfig = appConfig;
    }
    private getRawApi(): string {
        return this.appConfig.apiEndPoint;
    }

    private getAPIURL() {

        return this.getApi();
    }

    private generateGetAPI(name, req): Observable<any> {
        const requestURL = this.getAPIURL() + name + req;
        return this.http
            .get(requestURL).pipe(
                map(res => {
                    return res;
                }));
    }

    private generatePostAPI(name, data): Observable<any> {
        return this.http
            .post(this.getAPIURL() + name, data).pipe(
                map((res: Response) => res));
    }

    private generateDeleteAPI(name, data): Observable<any> {
        return this.http
            .delete(this.getAPIURL() + name, data).pipe(
                map(res => {
                    return res;
                }));
    }

    public getSoftwaresList() {
        return this.generateGetAPI("/software/list", "");
    }

    public getUtilitiesList() {
        return this.generateGetAPI("/utilities/list", "");
    }

    public getWorkspaceCatalogList(): Observable<any> {
        return this.generateGetAPI("/catalog/list", "");
    }

    public getDataSetCatalogList(): Observable<any> {
        return this.http
            .get(this.getRawApi() + "/FileService/getDatasetCatalog").pipe(
                map(res => {
                    return res;
                }));
    }

    public showAfterIntMessage(messageInfo) {
        this.afterInitSubject.next(messageInfo);
    }

    public getAfterIntObs(): Observable<any> {
        return this.afterInitSubject.asObservable();
    }

    public getEnvironmentIdSubject() {
        return this.environmentIdSubject.asObservable();
    }

    public getShowSuccessMessage() {
        return this.showSuccessMessage.asObservable();
    }

    public getUpdateVendorListEvent() {
        return this.onUpdateListEvent;
    }

    public getRegionList() {
        return this.generateGetAPI("/region", "");
    }

    public getAWSWorkspaceCatalogList() {
        return this.generateGetAPI("/aws-region", "");
    }


    //hardware
    public getWorkspaceTagTypeDetails(data): Observable<any> {
        return this.http
            .get(this.getRawApi() + "/tag-type/tag-types-for-context/" + data).pipe(
                map(res => {
                    return res;
                }));
    }

    public getWorkspaceTagTypeDetailById(data) {
        return this.http
            .get(this.getRawApi() + "/tag-type/tag-types-for-context-by-id/" + data).pipe(
                map(res => {
                    return res;
                }));
    }

    //utility
    public getApprovedUtilities(): Observable<any> {
        return this.http
            .get(this.getRawApi() + "/utility/approved-utility").pipe(
                map(res => {
                    return res;
                }));
    }
    public saveEnvironmentRequest(data): Observable<any> {
        return this.generatePostAPI("/save", data);
    }

    public getEnvironmentList(bundleOption: string = null, region: string = null, isRequestor = false) {
        let apiEndpoint = "/list/";
        apiEndpoint += (bundleOption) ? bundleOption : "All";
        apiEndpoint += "/";
        apiEndpoint += (region) ? region : "All";
        apiEndpoint += "/";
        return this.generateGetAPI(apiEndpoint, "");
    }

    public getAssignedResources() {
        return this.generateGetAPI("/assigned-resources", "");
    }

    public UpdateEnvironmentRequestHeaderStatus(data): Observable<any> {
        return this.generatePostAPI("/update-status", data)
    }

    public TerminateWorkspace(data): Observable<any> {
        return this.generatePostAPI("/terminate-workspace", data)
    }

    public downloadDataUsageAgreementDocument(dataUsageAgreementId: string): Observable<any> {
        const formData: FormData = new FormData();
        const endpoint = this.getRawApi() + "/FileService/viewDataUsageAgreement";
        formData.set('dataUsageAgreementId', dataUsageAgreementId);
        return this.http.post<Blob>(endpoint, formData, { responseType: 'blob' as 'json' });
    }

    public getEnvironmentRequest(id): Observable<any> {
        return this.generateGetAPI("/listById/", id);
    }

    public validateWorkSpaceUsernames(data:any): Observable<any> {
        return this.http
        .post(this.getRawApi() + "/authentication-provider/users-by-names", data).pipe(
            map(res => {
                return res;
            }));
    }


}