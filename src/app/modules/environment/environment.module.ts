import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DataGridModule } from "../core-ui-components";
import { DataGridExtendedModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import { AngularMultiSelectModule } from "src/app/modules/core-ui-components/angular2-multiselect-component/src/lib";
import { MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns';
import { NgSelectModule } from '@ng-select/ng-select';
import { HighchartsChartModule } from 'highcharts-angular';
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatButtonModule } from "@angular/material/button";
import { MatSelectModule } from "@angular/material/select";
import { MatInputModule } from "@angular/material/input";
import { MatTabsModule } from '@angular/material/tabs';

import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";
import { LocalStorageManager } from "../../utils/local-storage-manager";
import { EnvironmentRoutingModule } from './environment-routing-module';
import { EnvironmentRequestViewComponent } from './views/environment-request-view/environment-request-view.component';
import { EnvironmentRequestListComponent } from './views/environment-request-list/environment-request-list.component';

import { NgxUiLoaderModule } from "ngx-ui-loader";
import { ngxUiLoaderConfig } from "src/app/utils/config";

const modules = [MatSelectModule];

@NgModule(
    {
        declarations: [
            EnvironmentRequestViewComponent,
            EnvironmentRequestListComponent
        ],
        imports: [
            MultiSelectModule,
            NgSelectModule,
            SharedModules,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            EnvironmentRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatSelectModule,
            MatButtonModule,
            DataGridModule,
            DataGridExtendedModule,
            MatCheckboxModule,
            AngularMultiSelectModule,
            MatTabsModule,
            NgxSmartModalModule.forRoot(),
            NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
            HighchartsChartModule
        ],
        exports: [
            EnvironmentRequestViewComponent
        ],
        providers: [
            AlertService,
            NgxSmartModalService,
            LocalStorageManager
           
        ]
    }
)
export class EnvironmentModule {
}
