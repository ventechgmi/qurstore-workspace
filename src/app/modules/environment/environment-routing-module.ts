import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { EnvironmentRequestListComponent } from './views/environment-request-list/environment-request-list.component';
import { EnvironmentRequestViewComponent } from "./views/environment-request-view/environment-request-view.component";

export const routes: Routes = [
  {
    path: "environment/list",
    component: EnvironmentRequestListComponent
  },
  {
    path: "environment/list/:key",
    component: EnvironmentRequestListComponent
  },
  {
    path: "environment/request",
    component: EnvironmentRequestViewComponent
  },
  {
    path: "environment/request/:key",
    component: EnvironmentRequestViewComponent
  },
  {
    path: "environment/request/:environmentId/:isFromEmail/:mode",
    component: EnvironmentRequestViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class EnvironmentRoutingModule { }
