export class ValidationMessages {
    public static readonly RequiredMessage = "This field is required.";
    public static readonly MinMaxLengthMessage = " must be between 3 and 100 characters..";
    public static readonly deleteSuccessMessage = "Workspace deactivated successfully.";
    public static readonly UserNameRequired = "Provide AWS workspace User Name for each user before approve";
    public static readonly ForMoreUtilityDataset = "Select atleast one Utilities/Datasets for requesting more Utilities/Datasets";
    public static readonly SoftwareRequiredMessage = "Select atleast one Software Bundle for requesting New Environment";
    public static readonly DatasetRequiredMessage = "Select atleast one dataset to add/remove to cart";
    public static readonly DuplicateWorkspaceUserNameMessage = " is already in use. Please try another one";
}
