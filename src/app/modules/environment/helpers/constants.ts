export class WorkspaceRequestStatus {
    public static readonly APPROVED_STATUS = "Approved";
    public static readonly PENDING_FOR_APPROVAL_STATUS = "Pending for approval";
    public static readonly TERMINATED_STATUS = "Terminated";
    public static readonly REJECTED_STATUS = "Rejected";
}

export enum PageModeType {
    Approve = "Approve",
    RequestMoreUtility = "RequestMoreUtility",
    Resubmit = "Resubmit",
    Detail = "Detail",
    Create = "Create"
}

export enum AWSKeyValues {
    AdditionalPriceKey = "Bundle Storage License User Volume Usage Custom",
}

export enum PricingModel {
    Monthly = "Monthly",
    Hourly = "Hourly"
}

export enum BundleOption {
    Windows = "Windows Bundle Options",
    Linux = "Linux Bundle Options"
}

export let TabIndexKeys = {};
TabIndexKeys[0] = 'Environment';
TabIndexKeys[1] = 'Hardware'
TabIndexKeys[2] = 'Software';
TabIndexKeys[3] = 'Utilities';
TabIndexKeys[4] = 'Datasets';
TabIndexKeys[5] = 'History';

export enum DashboardWidgetStatus {
   Submitted_Waiting_For_Approval = "Submitted - Waiting for approval",
   Request_Changes = "Request Changes"
}