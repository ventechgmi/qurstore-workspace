import {
    Component,
    OnInit,
    AfterContentInit,
    ViewEncapsulation,
    ViewChild
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { DropDownListComponent } from '@syncfusion/ej2-angular-dropdowns';
import * as _ from "underscore";

import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from 'src/app/utils/alert/alert.service';
import { EnvironmentService } from '../../services/environment.service';
import { EnvironmentRequestViewModel } from '../../viewmodels/environment-request.viewmodel';
import { EnvironmentRequestNavigationService } from "../../../core-ui-services/environment-request-navigation.service";

export interface SoftwareRequest {
    name: string;
    value: string;
    selected: boolean;
}

@Component({
    selector: "environment-request-view",
    templateUrl: "./environment-request-view.component.html",
    styleUrls: ["./environment-request-view.component.css"],
    providers: [EnvironmentRequestViewModel, AlertService], // need separate alert instance
    encapsulation: ViewEncapsulation.None
})

export class EnvironmentRequestViewComponent implements OnInit, AfterContentInit {
    public softwareRequests: SoftwareRequest[] = [];
    @ViewChild('userList')
    public dropDownListObject: DropDownListComponent;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private environmentService: EnvironmentService,
        public environmentRequestViewModel: EnvironmentRequestViewModel,
        public alertService: AlertService,
        public ngxSmartModalService: NgxSmartModalService,
        public environmentRequestNavigationService: EnvironmentRequestNavigationService
    ) {
        alertService.afterInitObs = environmentService.getAfterIntObs();
    }

    public ngOnInit() {
        this.route.params
            .subscribe(async params => {
                let environmentId, isFromEmail, mode, isRedirectDashboard;
                if (params.key) {
                    const response = await this.environmentRequestNavigationService.navigationParametersForLink(params["key"]);

                    environmentId = response.environmentId;
                    isRedirectDashboard = response.isRedirectDashboard;
                    isFromEmail = response.isFromEmail;
                    mode = response.mode;
                    this.environmentRequestViewModel.initServices(this.alertService);
                    this.environmentRequestViewModel.init({ environmentId, isFromEmail, mode, isRedirectDashboard });
                } else {
                    if (params.environmentId)
                        this.environmentRequestViewModel.init({ environmentId:params.environmentId, isFromEmail:params.isFromEmail, mode:params.mode, isRedirectDashboard: false });
                }
            });
    }

    public ngAfterContentInit() {
    }

    public moveToSelectedTabByIndex(index: number) {
        const tabName = this.environmentRequestViewModel.tabIndexKeys[index];
        let isValid = this.environmentRequestViewModel.validateCurrentTabForm(index - 1);
        if (tabName && isValid) {
            this.environmentRequestViewModel.tabIndex = index;
            for (let i = 0; i < document.querySelectorAll('.mat-tab-label-content').length; i++) {
                if ((<HTMLElement>document.querySelectorAll('.mat-tab-label-content')[i]).innerText == tabName) {
                    (<HTMLElement>document.querySelectorAll('.mat-tab-label')[i]).click();
                }
            }
        }
    }

    public tabClick(clickEvent: any): void {
        //I have changed the clickEvent.toElement to clickEvent.target to fix the ag-grid resize issue in (chrome,firefox,edge).
        if (clickEvent.target) {
            const clickedTabIndex = this.environmentRequestViewModel.getTabIndex(clickEvent.target.innerText);

            if (clickedTabIndex === -1) {
                return;
            }

            if (!(this.environmentRequestViewModel.tabIndex === clickedTabIndex)) {
                if (clickedTabIndex <= this.environmentRequestViewModel.tabIndex) {
                    this.environmentRequestViewModel.tabIndex = clickedTabIndex;
                }
                else {
                    let isValid = false;
                    for (let i = 0; i < clickedTabIndex; i++) {
                        isValid = this.environmentRequestViewModel.validateCurrentTabForm(i);
                        if (!isValid) {
                            this.environmentRequestViewModel.tabIndex = i;
                            break;
                        }
                    }
                    if (isValid) {
                        this.environmentRequestViewModel.tabIndex = clickedTabIndex;
                    }

                }
            }
        }
    }

    public onTabChanged(): void {
        this.environmentRequestViewModel.sizeToFit();
    }

    public onTagRemove(args: any, workspaceTagDetail: any): void {
        let selectedUsers: any = this.dropDownListObject.value;
        this.environmentRequestViewModel.onRemove(args, workspaceTagDetail);
        let selectedTagUsers = _.map(this.environmentRequestViewModel.userList, 'id')
        this.environmentRequestViewModel.userList.filter(e => !selectedUsers.includes(e.id));
        this.dropDownListObject.value = _.intersection(selectedUsers, selectedTagUsers);
    }
}
