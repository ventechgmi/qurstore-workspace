import { Component, OnInit, AfterContentInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormBuilder } from "@angular/forms";
import config from "../../../../utils/config";

import { AlertComponent } from "src/app/utils/alert/alert.component";
import { AlertService } from "../../../../utils/alert/alert.service";
import { EnvironmentListViewModel } from "../../viewmodels/environment-list.viewmodel";
import { EnvironmentService } from "../../services/environment.service";
import { EnvironmentRequestNavigationService } from 'src/app/modules/core-ui-services/environment-request-navigation.service';

@Component({
  selector: 'app-environment-request-list',
  templateUrl: './environment-request-list.component.html',
  styleUrls: ['./environment-request-list.component.css'],
  providers: [EnvironmentListViewModel, AlertService]
})
export class EnvironmentRequestListComponent implements OnInit, AfterContentInit, OnDestroy {

  public dataTable: any;
  public userCreateModalRef: any;
  projectId: any;

  constructor(private router: Router,
    private environmentService: EnvironmentService,
    public environmentListViewModel: EnvironmentListViewModel,
    public alertService: AlertService,
    private route: ActivatedRoute,
    public ngxSmartModalService: NgxSmartModalService,
    public environmentRequestNavigationService: EnvironmentRequestNavigationService
  ) {
    // need to set the afterInitObs to show other screen success or error message in different screen
    alertService.afterInitObs = environmentService.getAfterIntObs();
  }

  public ngOnInit() {
    this.route.params.subscribe(async params => {
      this.environmentListViewModel.initServices(this.alertService);
      if (params.key) {
        const response = await this.environmentRequestNavigationService.navigationParametersForLink(params["key"]);
        const status = response.status;
        this.environmentListViewModel.init({ status: status });
      } else {
        this.environmentListViewModel.init({});
      }
    });
  }

  public ngOnDestroy() {
    this.environmentService.showAfterIntMessage({ text: "", type: "success", toStable: true });
  }

  public ngAfterContentInit() { }
}
