import { Routes, RouterModule } from "@angular/router";
import { DataSourceConfigurationListComponent } from "./views/data-source-configuration-list/data-source-configuration-list.component";
import { DataSourceConfigurationCreateEditDetailComponent } from "./views/data-source-configuration-create-edit-detail/data-source-configuration-create-edit-detail.component";
import { NgModule } from "@angular/core";

export const routes: Routes = [
  {
    path: "data-source-configuration/list",
    component: DataSourceConfigurationListComponent
  },
  {
    path: "data-source-configuration/:key",
    component: DataSourceConfigurationCreateEditDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataSourceConfigurationRoutingModule { }
