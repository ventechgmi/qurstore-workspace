import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DataGridModule } from "../core-ui-components";
import { DataGridExtendedModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import {MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatFormFieldModule} from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import {  MatButtonModule} from "@angular/material/button";
import {  MatFormFieldControl} from "@angular/material/form-field";
import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";
import { DataSourceConfigurationRoutingModule } from "./data-source-configuration-routing-module";
import { DataSourceConfigurationService } from "./services/data-source-configuration.service";
import { DataSourceConfigurationListComponent } from "./views/data-source-configuration-list/data-source-configuration-list.component";
import { MatSelectModule } from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {LocalStorageManager} from "../../utils/local-storage-manager";
import { DataSourceConfigurationCreateEditDetailComponent } from './views/data-source-configuration-create-edit-detail/data-source-configuration-create-edit-detail.component';
const modules = [MatSelectModule];
@NgModule(
    {
        declarations: [
            DataSourceConfigurationListComponent,
            DataSourceConfigurationCreateEditDetailComponent
        ],
        imports: [
            SharedModules,
            NgSelectModule,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            DataSourceConfigurationRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatSelectModule,
            MatButtonModule,
            DataGridModule,
            DataGridExtendedModule,
            MatCheckboxModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [
            DataSourceConfigurationListComponent,
            DataSourceConfigurationCreateEditDetailComponent
        ],
        providers: [
            AlertService,
            DataSourceConfigurationService,
            NgxSmartModalService,
            LocalStorageManager
        ]
    }
)
export class DataSourceConfigurationModule {

}
