export enum DataSourceType {
  AWS_S3 = "AWS_S3",
  SFTP = "SFTP",
  FTPS = "FTPS",
  FTP = "FTP",
  AWS_S3_Bucket = "AWS S3 Bucket"
}

export enum ModalType {
  Create = "create",
  Detail = "detail",
  Edit = "edit",
  Delete = "delete"
}

export enum AmazonS3Keys {
  AccessKey = "accessKey",
  SecretKey = "secretKey",
  Bucket = "bucket",
  Region = "region",
  FolderName = "folderName",
  DatasetType = "datasetType"
}

export enum AmazonS3Types {
  Password = "password",
  String = "string"
}
