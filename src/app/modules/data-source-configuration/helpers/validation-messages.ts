export default class ValidationMessages {
    public static readonly Delete_Success = "Data Source Deleted Successfully";
    public static readonly Save_Success = "Data Source Details Saved Successfully";
    public static readonly Copied_Success = "Configuration details are copied into Clipboard";
    public static readonly Copied_Fail = "Copy command failed";
    public static readonly Connection_Success = "Connection Verified Successfully";
}
