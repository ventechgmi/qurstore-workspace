import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormBuilder } from "@angular/forms";
import { AuthService } from "src/app/utils/auth.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { DataSourceConfigurationListViewModel } from '../../viewmodels/data-source-configuration-list.viewmodel';
import { DataSourceConfigurationService } from '../../services/data-source-configuration.service';

@Component({
  selector: 'app-data-source-configuration-list',
  templateUrl: './data-source-configuration-list.component.html',
  styleUrls: ['./data-source-configuration-list.component.css'],
  providers: [DataSourceConfigurationListViewModel, AlertService]
})
export class DataSourceConfigurationListComponent implements OnInit {

  public dataSourceConfigurationList: any;
  public dataTable: any;
  constructor(private router: Router,
    private authService: AuthService,
    private dataSourceConfigurationService: DataSourceConfigurationService,
    public dataSourceConfigurationListViewModel: DataSourceConfigurationListViewModel,
    public alertService: AlertService,
    private route: ActivatedRoute,
    public ngxSmartModalService: NgxSmartModalService
  ) {
    alertService.afterInitObs = dataSourceConfigurationService.getAfterIntObs();
  }

  public ngOnInit() {
    this.route.params
      .subscribe(params => {
        const userId = this.authService.getUserId();
        this.dataSourceConfigurationListViewModel.initServices(this.alertService);
        this.dataSourceConfigurationListViewModel.init({ userId });
      });
  }

  public ngAfterContentInit() { }

}

