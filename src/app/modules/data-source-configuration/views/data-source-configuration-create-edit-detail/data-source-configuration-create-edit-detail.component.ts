import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/utils/alert/alert.service';
import { AuthService } from 'src/app/utils/auth.service';
import { DataSourceConfigurationService } from '../../services/data-source-configuration.service';
import { DataSourceConfigurationCreateEditDetailViewModel } from "../../viewmodels/data-source-configuration-create-edit-detail.viewmodel"
import { NgxSmartModalService } from "ngx-smart-modal";
import { DataSourceConfigurationNavigationService } from "../../../core-ui-services/data-source-configuration-navigation.service";

@Component({
  selector: 'app-data-source-configuration-create-edit-detail',
  templateUrl: './data-source-configuration-create-edit-detail.component.html',
  styleUrls: ['./data-source-configuration-create-edit-detail.component.css'],
  providers: [DataSourceConfigurationCreateEditDetailViewModel, AlertService]
})
export class DataSourceConfigurationCreateEditDetailComponent implements OnInit {

  constructor(
    public dataSourceConfigurationCreateEditDetailViewModel: DataSourceConfigurationCreateEditDetailViewModel,
    private router: Router,
    private authService: AuthService,
    private dataSourceConfigurationService: DataSourceConfigurationService,
    public alertService: AlertService,
    private route: ActivatedRoute,
    public ngxSmartModalService: NgxSmartModalService,
    public dataSourceConfigurationNavigationService: DataSourceConfigurationNavigationService
  ) {
  }

  public ngOnInit() {
    this.route.params.subscribe(async params => {
      if (params.key) {
        const response = await this.dataSourceConfigurationNavigationService.navigationParametersForLink(params["key"]);
        this.dataSourceConfigurationCreateEditDetailViewModel.alertService = this.alertService;
        this.dataSourceConfigurationCreateEditDetailViewModel.init({
          dataSourceConfigurationId: response.dataSourceConfigurationId,
          mode: response.mode
        });
      } else {
        this.dataSourceConfigurationCreateEditDetailViewModel.init({});
      }
    });
  }
}
