
import { map } from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient, HttpEvent, HttpRequest } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
@Injectable({
    providedIn: "root"
})
export class DataSourceConfigurationService extends EntityService {
    appConfig: AppConfig;
    onUpdateListEvent = new EventEmitter();
    public showSuccessMessage = new Subject<any>();
    public alertInfo = { message: "", type: "" };
    private afterInitSubject = new BehaviorSubject(this.alertInfo);


    private test: any[];
    constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
        super(http, "data-source-configuration", appConfig);
        this.appConfig = appConfig;

    }
    getRawApi(): string {
        return this.appConfig.apiEndPoint;
    }

    private getDataSourceConfigurationServiceAPIURL() {
        return this.getApi();
    }
    public showAfterIntMessage(messageInfo) {
        this.afterInitSubject.next(messageInfo);
    }

    public getAfterIntObs(): Observable<any> {
        return this.afterInitSubject.asObservable();
    }

    public getShowSuccessMessage() {
        return this.showSuccessMessage.asObservable();
    }



    private generateGetAPI(name, req): Observable<any> {
        return this.http
            .get(this.getDataSourceConfigurationServiceAPIURL() + name + req).pipe(
                map(res => {
                    return res;
                }));
    }
    private generateDeleteAPI(name, data): Observable<any> {
        return this.http
            .delete(this.getDataSourceConfigurationServiceAPIURL() + name + data).pipe(
                map(res => {
                    return res;
                }));
    }


    private generatePostAPI(name, data): Observable<any> {
        return this.http
            .post(this.getDataSourceConfigurationServiceAPIURL() + name, data).pipe(
                map((res: Response) => res));
    }


    public getAllDataSourceConfigurations(): Observable<any> {
        return this.generateGetAPI("/list", "");
    }

    public saveDataSourceConfiguration(dataSourceConfigurationDetails): Observable<any> {
        return this.generatePostAPI("/save", dataSourceConfigurationDetails);
    }

    public verifyConnection(connectionDetails, dataSourceType): Observable<any> {
        return this.generatePostAPI("/verify-connection", { connectionDetails: connectionDetails, dataSourceType: dataSourceType });
    }

    public getDataSourceConfiguration(dataSourceConfigurationId): Observable<any> {
        return this.generateGetAPI("/", dataSourceConfigurationId);
    }

    public deleteDataSourceConfiguration(dataSourceConfigurationId): Observable<any> {
        return this.generateDeleteAPI("/delete/", dataSourceConfigurationId);
    }

    public getDatasetTypeList(organizationId: string): Observable<any> {
        return this.http.get(this.getRawApi() + "/dataset-type/organization/" + organizationId)
            .pipe(map(res => { return res; }));
    }

    public getReferenceDataValue(referenceDataCode): Observable<any> {
        const endpoint = this.getRawApi() + "/ReferenceDataValue/list/" + referenceDataCode;
        return this.http
            .get(endpoint).pipe(
                map(res => {
                    return res;
                }));
    }
}
