
import { from as observableFrom, Observable } from "rxjs";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import {
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";
import ValidationMessages  from "../helpers/validation-messages";
import { AmazonS3Keys, AmazonS3Types, DataSourceType, ModalType } from "../helpers/constants";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from "../../../utils/alert/alert.service";
import { DataSourceConfigurationService } from "../services/data-source-configuration.service";
import { DataSourceConfigurationViewModel } from "./data-source-configuration.viewmodel";
import { LocalStorageManager } from 'src/app/utils/local-storage-manager';
import { AWS_REGION } from 'src/app/utils/config';
import { AuthService } from 'src/app/utils/auth.service';
import { GuidHelper } from "../../employee-dashboard/helpers/guid-helper";
import { DataSourceConfigurationNavigationService } from "../../core-ui-services/data-source-configuration-navigation.service";

@Injectable()
export class DataSourceConfigurationCreateEditDetailViewModel extends DataSourceConfigurationViewModel {
  public modalType: string;
  public refTypeList = [];
  public dataSourceConfigurationForm: FormGroup;
  public amazonS3Form: FormGroup;
  public SFTPForm: FormGroup;
  public roleList = [];
  public datasetTypeList = [];
  public selectedRefType: string;
  public regionList = AWS_REGION;
  public alertService: AlertService;
  public submitted: boolean;
  public dataSourceConfigurationId: string;
  public detailEdit: boolean;
  public isConnectionVerified = false;
  public userRolePermission: any = {};
  public filePattern: string;
  private datasetTypeId: string;
  private cancelConfirmModal: string = "cancelConfirmModal";
  private deleteConfirmModalFromList: string = "deleteConfirmModalFromList";
  private deleteConfirmModal: string = "deleteConfirmModal";
  private organizationId: string;

  public constructor(
    public dataSourceConfigurationService: DataSourceConfigurationService,
    private router: Router,
    private fb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService,
    private chRef: ChangeDetectorRef,
    public route: ActivatedRoute,
    public authService: AuthService,
    public dataSourceConfigurationNavigationService: DataSourceConfigurationNavigationService,
  ) {
    super(dataSourceConfigurationService, ViewModelType.ReadOnly, ngxSmartModalService, route, authService);
    this.initForm();
  }


  public init(params?): Observable<any> {
    this.userRolePermission = this.getUserClaims() || {};
    this.organizationId = this.authService.getOrganizationId();
    this.dataSourceConfigurationId = params.dataSourceConfigurationId ? params.dataSourceConfigurationId : "";
    const mode = params.mode ? params.mode : "";
    this.getDatasetTypeList();
    switch (mode) {
      case ModalType.Edit:
        this.getDataSourceConfiguration();
        this.modalType = ModalType.Edit;
        this.enableForms();
        break;
      case ModalType.Detail:
        this.getDataSourceConfiguration();
        this.modalType = ModalType.Detail
        this.disableForms();
        break;
      default:
        this.modalType = ModalType.Create;
        break;
    }
    this.getRefTypes();
    return observableFrom([]);
  }

  private initForm(): void {
    this.dataSourceConfigurationForm = this.fb.group({
      id: [""],
      name: ["", [Validators.required]],
      refTypeId: ["", [Validators.required]]
    });

    this.amazonS3Form = this.fb.group({
      bucket: ["", [Validators.required]],
      folderName: [""],
      region: ["", [Validators.required]],
      datasetType: ["", [Validators.required]]
    });
    this.SFTPForm = this.fb.group({
      endpoint: ["", [Validators.required]],
      username: [""],
      datasetType: ["", [Validators.required]]
    });
  }

  public enableForms(): void {
    this.dataSourceConfigurationForm.enable();
    this.amazonS3Form.enable();
  }
  public disableForms(): void {
    this.dataSourceConfigurationForm.disable();
    this.amazonS3Form.disable();
    this.SFTPForm.disable();
  }

  public getRefTypes(): void {
    this.dataSourceConfigurationService.getReferenceDataValue("DSCTYP").subscribe(
      data => {
        this.refTypeList = JSON.parse(data);
        if (this.modalType === ModalType.Create) {
          var index = this.refTypeList.findIndex(x => x.Name === DataSourceType.AWS_S3_Bucket);
          this.dataSourceConfigurationForm.patchValue({
            refTypeId: this.refTypeList[index].Id
          });
          this.selectRefType({ name: DataSourceType.AWS_S3_Bucket })
        }

      });
  }

  public selectRefType(selectedRefType): void {
    this.selectedRefType = selectedRefType ? selectedRefType.name : "";
    if (!selectedRefType) {
      this.amazonS3Form.reset();
      this.SFTPForm.reset();
    }
  }

  public enableSaveConfigurationButton(): boolean {
    if (this.selectedRefType === DataSourceType.AWS_S3_Bucket) {
      if (this.dataSourceConfigurationForm.valid && this.amazonS3Form.valid && this.isConnectionVerified) {
        return true
      } else {
        return false;
      }
    } else {
      return true;
    }

  }

  public enableSaveButton(): boolean {
    if (this.dataSourceConfigurationForm.dirty && !this.amazonS3Form.dirty) {
      return true;
    } else if (this.amazonS3Form.dirty && this.isConnectionVerified) {
      return true;
    } else {
      return false;
    }
  }

  public enableVerifyButton(): boolean {
    if (this.selectedRefType === DataSourceType.AWS_S3_Bucket) {
      if (this.dataSourceConfigurationForm.valid && this.amazonS3Form.valid && this.amazonS3Form.dirty &&
        this.modalType !== ModalType.Detail) {
        return true
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  public cancelCreate(): void {
    if (this.dataSourceConfigurationForm.dirty || this.amazonS3Form.dirty || this.SFTPForm.dirty) {
      this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
      this.ngxSmartModalService.open(this.cancelConfirmModal);
    } else if (this.detailEdit) {
      this.dataSourceConfigurationNavigationService.navigateUsingProxy({
        url: "/data-source-configuration",
        saveLink: true,
        openInNewWindow: false,
        isQueryParams: false,
        parameters: { dataSourceConfigurationId: this.dataSourceConfigurationId, mode: ModalType.Detail }
      }).catch();
      this.detailEdit = false;
    } else {
      this.router.navigate(["data-source-configuration/list"]);
    }
  }

  public confirmCancel(): void {
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
    this.router.navigate(["data-source-configuration/list"]);
  }

  public saveDataSourceConfiguration(): void {
    this.submitted = true;
    const dataSourceConfigurationDetails = this.transformToEntityModel();
    this.dataSourceConfigurationService.saveDataSourceConfiguration(dataSourceConfigurationDetails)
      .subscribe(
        (data) => {
          this.submitted = false;
          this.dataSourceConfigurationService.showAfterIntMessage({ text: ValidationMessages.Save_Success, type: "success", toStable: true });
        }, (err) => {
          this.submitted = false;
          this.alertService.error(err.error.errorList.length ? err.error.errorList[0] : "", true);
        },
        () => {
          if (this.detailEdit) {
            this.dataSourceConfigurationNavigationService.navigateUsingProxy({
              url: "/data-source-configuration",
              saveLink: true,
              openInNewWindow: false,
              isQueryParams: false,
              parameters: { dataSourceConfigurationId: this.dataSourceConfigurationId, mode: ModalType.Detail }
            }).catch();
            this.detailEdit = false;
            this.dataSourceConfigurationForm.reset();
            this.amazonS3Form.reset();
            this.alertService.success(ValidationMessages.Save_Success, true);
          } else {
            this.navigateToList();
          }
        });
  }

  public transformToEntityModel(): any {
    const dataSourceConfigurationDetails = this.dataSourceConfigurationForm.value;
    dataSourceConfigurationDetails.id = GuidHelper.isValid(dataSourceConfigurationDetails.id) ? dataSourceConfigurationDetails.id : undefined;
    dataSourceConfigurationDetails.configData = this.getConfigData();
    dataSourceConfigurationDetails.isDeleted = false;
    dataSourceConfigurationDetails.datasetTypeId = this.datasetTypeId;
    return dataSourceConfigurationDetails;
  }

  public getConfigData(): string {
    const configData = [];
    const keys = Object.keys(this.amazonS3Form.value);
    this.amazonS3Form.value.datasetType = this.amazonS3Form.value.datasetType.name;
    _.each(keys, item => {
      configData.push({
        name: item,
        type: ((item === AmazonS3Keys.AccessKey) || (item === AmazonS3Keys.SecretKey)) ? AmazonS3Types.Password : AmazonS3Types.String,
        value: this.amazonS3Form.value[item]
      });
    });
    return (JSON.stringify(configData));
  }

  public navigateToList(): void {
    this.dataSourceConfigurationForm.reset();
    this.amazonS3Form.reset();
    this.SFTPForm.reset();
    this.router.navigate(["/data-source-configuration/list/"]).then().catch();
  }

  public verifyConnection(): void {
    const connectionDetails = this.amazonS3Form.value;
    const dataSourceType = this.selectedRefType;
    this.dataSourceConfigurationService.verifyConnection(connectionDetails, dataSourceType).subscribe(
      (data) => {
        if (data.isSuccess) {
          this.alertService.success(ValidationMessages.Connection_Success, true);
          this.isConnectionVerified = true;
        } else {
          this.alertService.error(data.errorList[0], true);
        }

      }, (err) => {
        this.submitted = false;
        this.alertService.error(err.error.errorList.length ? err.error.errorList[0] : "", true);
      });
  }

  public getDataSourceConfiguration(): void {
    this.dataSourceConfigurationService.getDataSourceConfiguration(this.dataSourceConfigurationId).subscribe(
      (data) => {
        const jsonConfigData = JSON.parse(data.configData)
        this.dataSourceConfigurationForm.patchValue({
          id: data.id,
          name: data.name,
          refTypeId: data.refType.id
        });
        this.selectRefType(data.refType);
        this.filePattern = data.datasetType.filePattern;
        this.datasetTypeId = data.datasetType.id;

        if (data.refType.code === DataSourceType.AWS_S3) {
          _.each(jsonConfigData, data => {
            this.amazonS3Form.get(data.name).setValue(data.value)
          });
          this.amazonS3Form.patchValue({
            datasetType: data.datasetType.name
          });
        } else {
          const jsonConfigObject: any = {};
          jsonConfigData.forEach(item => {
            jsonConfigObject[item.name] = item.value
          });
          this.SFTPForm.patchValue({
            datasetType: data.datasetType.name,
            endpoint: jsonConfigObject.endpoint,
            username: jsonConfigObject.username
          });
        }
      });
  }

  public editFormDetail(): void {
    this.detailEdit = true;
    this.dataSourceConfigurationNavigationService.navigateUsingProxy({
      url: "/data-source-configuration",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { dataSourceConfigurationId: this.dataSourceConfigurationId, mode: ModalType.Edit }
    }).catch();
  }

  public deletePopup(): void {
    this.ngxSmartModalService.open(this.deleteConfirmModal);
  }

  public deleteDataSourceConfiguration(): void {
    this.dataSourceConfigurationService.deleteDataSourceConfiguration(this.dataSourceConfigurationId).subscribe(data => {
      this.ngxSmartModalService.getModal(this.deleteConfirmModal).close();
      if (data.isSuccess) {
        this.alertService.success(ValidationMessages.Delete_Success, true);
        this.router.navigate(["data-source-configuration/list"]);
      } else {
        this.alertService.error(data.message, true);
      }
    }, (requestFailed) => {
      this.ngxSmartModalService.getModal(this.deleteConfirmModal).close();
      (requestFailed.error && requestFailed.error.validationErrors) ?
        this.alertService.error(requestFailed.error.validationErrors.message, true) :
        this.alertService.error(requestFailed.error.message, true);
    });
  }

  public onPopUpClose(modal: string): void {
    this.ngxSmartModalService.getModal(modal).close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }

  public selectDatasetTypeS3(event: any): void {
    if(event) {
      this.datasetTypeId = event.id;
      this.filePattern = event.filePattern ? event.filePattern : "";
    } else {
      this.datasetTypeId = undefined;
      this.filePattern = undefined;
    }
  }

  private getDatasetTypeList(): void {
    this.dataSourceConfigurationService.getDatasetTypeList(this.organizationId).subscribe(data => {
      if (data.isSuccess && data.payload.length) {
        this.datasetTypeList = data.payload;
      } else {
        this.datasetTypeList = [];
      }
    }, (err) => {
      this.alertService.error(err.error.message, true);
    });
  }

}
