import { from as observableFrom, Observable, Subject, forkJoin } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridExtendedViewModel } from "../../core-ui-components";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AuthService } from "src/app/utils/auth.service";
import { DataSourceConfigurationViewModel } from "./data-source-configuration.viewmodel";
import { DataSourceConfigurationService } from "../services/data-source-configuration.service";
import { DataSourceType, ModalType } from "../helpers/constants";
import moment from "moment";
import { ClipboardService } from "ngx-clipboard";
import  ValidationMessages from "../helpers/validation-messages";
import { DataSourceConfigurationNavigationService } from "../../core-ui-services/data-source-configuration-navigation.service";

@Injectable()
export class DataSourceConfigurationListViewModel extends DataSourceConfigurationViewModel {
  public userId: string;
  public dataSourceConfigurationList = [];
  public alertService: AlertService;
  public viewModelDataGrid: DataGridExtendedViewModel;
  public appliedFilters: any[];
  public dataSourceConfigurationId: any;
  public userRolePermission: any = {};
  private deleteConfirmModalFromList: string = "deleteConfirmModalFromList";
  public constructor(
    public dataSourceConfigurationService: DataSourceConfigurationService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    private clipboardService: ClipboardService,
    public dataSourceConfigurationNavigationService: DataSourceConfigurationNavigationService,
    public authService: AuthService) {
    super(dataSourceConfigurationService, ViewModelType.Edit, ngxSmartModalService, route, authService);
  }

  public init(param?) {
    this.userId = param.userId;
    this.userRolePermission = this.getUserClaims() || {};
    this.viewModelDataGrid = new DataGridExtendedViewModel();
    this.initializeGridColDef();
    this.viewModelDataGrid.clickableColumnIndices = [1];
    this.subscribeToCreateSuccessMessage();
    this.subscribers();
    this.setDataTableConfigurations();
    this.getDataSourceConfigurationsList();
    return observableFrom([]);
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  private subscribers() {
    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.columnClicked:
          this.redirectToDetail(e.eventData.rowDetails.id);
          break;
        case GridConfig.editClicked:
          this.redirectToEditFromList(e.eventData.rowDetails.id);
          break;
        case GridConfig.deleteClicked:
          this.openDeleteConfirmationPopUp(e.eventData.rowDetails.id);
          break;
        case GridConfig.copyClicked:
          this.copyToClipboard(e.eventData.rowDetails);
          break;
        default:
      }
    });
  }

  public openDeleteConfirmationPopUp(id: string): void {
    this.dataSourceConfigurationId = id;
    this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
    this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).open();
  }


  public redirectToDetail(dataSourceConfigurationId): void {
    this.dataSourceConfigurationNavigationService.navigateUsingProxy({
      url: "/data-source-configuration",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { dataSourceConfigurationId: dataSourceConfigurationId, mode: ModalType.Detail }
    }).catch();
  }

  public redirectToEditFromList(dataSourceConfigurationId): void {
    this.dataSourceConfigurationNavigationService.navigateUsingProxy({
      url: "/data-source-configuration",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { dataSourceConfigurationId: dataSourceConfigurationId, mode: ModalType.Edit }
    }).catch();
  }

  public deleteDataSourceConfiguration(): void {
    this.dataSourceConfigurationService.deleteDataSourceConfiguration(this.dataSourceConfigurationId)
      .subscribe(data => {
        this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).close();
        this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
        if (data.isSuccess) {
          this.alertService.success(data.message, true);
          this.getDataSourceConfigurationsList();
        } else {
          this.alertService.error(data.message, true);
        }
      }, (requestFailed) => {
        this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).close();
        (requestFailed.error && requestFailed.error.validationErrors) ?
          this.alertService.error(requestFailed.error.validationErrors.message, true) :
          this.alertService.error(requestFailed.error.message, true);
      });
  }

  // data-table configurations for project
  public setDataTableConfigurations() {
    this.viewModelDataGrid.dataTableConfiguration = {
      destroy: true,
      columnDefs: [{ targets: [0], visible: false, searchable: false }, { type: "num-html", targets: [2] }, { orderable: false, targets: 2, searchable: false }],
      order: [[1, "asc"]]
    };
  }

  public getDataSourceConfigurationsList(): void {
    this.dataSourceConfigurationService.getAllDataSourceConfigurations().subscribe(
      data => {
        if (data) {
          this.dataSourceConfigurationList = data;
          this.viewModelDataGrid.setRowsData(this.dataSourceConfigurationList);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public subscribeToCreateSuccessMessage() {
    this.dataSourceConfigurationService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      if (!data.isCreateAnother) {
        this.alertService.success(message, toStable);
      }
    });
  }

  public navigateToCreate() {
    this.dataSourceConfigurationNavigationService.navigateUsingProxy({
      url: "/data-source-configuration",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { dataSourceConfigurationId: "", mode: ModalType.Create }
    }).catch();
  }

  public initializeGridColDef() {
    const name = {
      headerName: 'Name',
      field: 'name',
      editable: false,
      sortable: true,
      filter: true,
      sort: 'asc',
      cellRenderer: (params: any) => {
        return `<u class="hyperlink" data-action-type="columnClicked">` + params.value + `</u>`;
      }
    };
    this.viewModelDataGrid.columnDefs.push(name);

    const typeName = {
      headerName: 'Type',
      field: 'typeName',
      editable: false
    };
    this.viewModelDataGrid.columnDefs.push(typeName);

    const datasetType = {
      headerName: 'Dataset Type',
      field: 'datasetTypeName',
      editable: false
    };
    this.viewModelDataGrid.columnDefs.push(datasetType);

    const description = {
      headerName: 'Description',
      field: 'description',
      editable: false,
      cellStyle: { 'white-space': 'normal' },
      autoHeight: true,
      cellRenderer: (params: any) => {
        let template = ``;
        let bucket = ``;
        let endpoint = ``;
        let username = ``;
        if (params.data.bucket) {
          bucket = '<b>Bucket Name: </b>' + params.data.bucket;
          template += bucket;
        } else if (params.data.endpoint) {
          endpoint = '<b>Endpoint: </b>' + params.data.endpoint;
          username = params.data.username ? '<br /><b>Username: </b>' + params.data.username : '';
          template += endpoint;
          template += username;
        }
        return template;
      }
    };
    this.viewModelDataGrid.columnDefs.push(description);


    const lastModifiedBy = {
      headerName: 'Last Modified By',
      field: 'lastModifiedBy',
      editable: false
    };
    this.viewModelDataGrid.columnDefs.push(lastModifiedBy);

    const lastModifiedDate = {
      headerName: 'Last Modified On',
      field: 'lastModifiedDate',
      editable: false,
      isDate: true,
      cellRenderer: (params: any) => {
        return moment(params.value).format(config.APPLICATION_DATE_TIME_FORMAT)
      }
    };
    this.viewModelDataGrid.columnDefs.push(lastModifiedDate);

    const action = {
      headerName: 'Action',
      field: 'action',
      editable: false,
      cellRenderer: (params: any) => {
        let template = ``;
        let icon = ``;
        let deleteIcon = ``;
        switch (params.data.typeName) {
          case DataSourceType.AWS_S3_Bucket:
            if (this.userRolePermission.canCreate || this.userRolePermission.canUpdate) {
              icon = `<a routerLink="" class="data-grid-padding-right" style = "cursor:pointer;"><img alt="Edit" title="Edit" src="../../../assets/images/edit.png"  class="data-grid-image"  data-action-type="editClicked"/></a>`;
            }
            if (this.userRolePermission.canDelete) {
              deleteIcon = `<a routerLink="" class="data-grid-padding-right" style = "cursor:pointer;"><img alt="Delete"  title="Delete"  src="../../../assets/images/trash.png" class="data-grid-image-small" data-action-type="deleteClicked"/></a>`;
            }
            template += icon;
            template += deleteIcon;
            break;
          case DataSourceType.SFTP:
          case DataSourceType.FTP:
          case DataSourceType.FTPS:
            icon = `<a routerLink="" class="data-grid-padding-right" style = "cursor:pointer;"><img alt="Copy" title="Copy Configuration" src="../../../assets/images/copy.png"  class="data-grid-image"  data-action-type="copyClicked"/></a>`;
            template += icon;
        }


        return template;
      }
    };
    this.viewModelDataGrid.columnDefs.push(action);
  }

  public copyToClipboard(event: any): void {
    if (event.endpoint && event.username) {
      const data = `Endpoint: ${event.endpoint}, Username: ${event.username}`;
      if (this.clipboardService.copyFromContent(data)) {
        this.dataSourceConfigurationService.showAfterIntMessage({ text: ValidationMessages.Copied_Success, type: "success", toStable: true });
      } else {
        this.dataSourceConfigurationService.showAfterIntMessage({ text: ValidationMessages.Copied_Fail, type: "error", toStable: true });
      }
    } else {
      this.dataSourceConfigurationService.showAfterIntMessage({ text: ValidationMessages.Copied_Fail, type: "error", toStable: true });
    }
  }

  public onPopUpClose(): void {
    this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }


}
