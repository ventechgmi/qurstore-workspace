
import {map} from "rxjs/operators";
import { EntityBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { Observable, Subject ,  of } from "rxjs";
import { AbstractControl, ValidatorFn } from "@angular/forms";
import { NgxSmartModalService } from "ngx-smart-modal";
// import { AuthService } from "src/app/utils/auth.service";
import { ActivatedRoute } from "@angular/router";
import { VendorService } from '../services/vendor-management.service';

export class VendorViewModel extends EntityBaseViewModel<"", VendorService> {
    public moduleId: string;
    public moduleAccess;
    public moduleInfo;
    public updateVendorListSubject = new Subject<any>();
    public vendorDetailSelection = new Subject<any>();
    auditLogPermission: any = {};

    public constructor(
        public vendorService: VendorService,
        public viewModelType: ViewModelType,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute
    ) {
        super(vendorService, viewModelType);
        this.entityName = "vendor";
    }


    public create(): Observable<any> {
        // add any customization logic for create
        return super.create().pipe(map(data => data));
    }

    // initUserClaims() {
    //     const baseUrl = ROUTES_CONFIG_MAP.vendor_MANAGEMENT.BASE_URL;
    //     this.moduleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
    //     this.moduleInfo = this.authService.getModuleAccess(this.moduleId) || null;
    //     this.moduleAccess = this.moduleInfo ? this.moduleInfo.rights : {};
    //     this.getAuditModulePermissions();
    // }
    // getAuditModulePermissions() {
    //     const baseUrl = ROUTES_CONFIG_MAP.AUDIT_LOG_MANAGEMENT.BASE_URL;
    //     const moduleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
    //     const moduleInfo: any = this.authService.getModuleAccess(moduleId) || null;
    //     const moduleAccess = moduleInfo ? moduleInfo.rights : {};
    //     const permissions = { moduleId: moduleId, moduleInfo: moduleInfo, moduleAccess: moduleAccess };
    //     this.auditLogPermission = permissions;
    // }

    // custom methods
    public getUpdateVendorListObservable(): Observable<any> {
        return this.updateVendorListSubject.asObservable();
    }

    public getUpdateVendorListSubject(): Subject<any> {
        return this.updateVendorListSubject;
    }

    public getVendorDetailObservable(): Observable<any> {
        return this.vendorDetailSelection.asObservable();
    }

    public getVendorDetailSelectionSubject(): Subject<any> {
        return this.vendorDetailSelection;
    }

    // public nameUniqueValidator(vendorName): Observable<any> {
    //     return this.vendorService.getvendorByName(vendorName).pipe(map(res => res));
    // }

    public noWhitespaceValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            // messy but you get the idea
            const isWhitespace = (control.value || "").trim().length === 0;
            const isValid = !isWhitespace;
            return isValid ? null : { hasOnlyWhiteSpace: true };
        };
    }

    validWebsite(): ValidatorFn {
      return (control: AbstractControl): { [key: string]: any } => {
        const pattern = "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?";
        const whiteSpace = /^\s*$/;
        if ((control.value || "").match(whiteSpace)) {
          return null;
        } else if (!control.value.match(pattern)) {
          return { invalidTime: true };
        }
      };
    }
    /*
    This method validates the below
    1. validates the control has only white spaces since most of the name fields should not allow just white spaces
    2. validates the control whether it has only special characters.

    */
   public validateSpecialCharactersAndWhiteSpace(lengthMin: number): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            // const pattern = "[^A-Za-z0-9||||\.||_]";
            const pattern = "[A-Za-z0-9]";
            const whiteSpace = /^\s*$/;
            if ((control.value || "").length === 0) {
                return null;
            } else if ((control.value || "").trim().length < lengthMin) {
                return { minLength: true };
            } else if (control.value.match(whiteSpace)) {
                return { hasOnlyWhiteSpace: true };
            } else if (!control.value.match(pattern)) { // check if the input doest not have any alphabets or in other words input has only special characters
                // return "Don't enter only special characters";
                return { hasOnlySpecialCharacters: true };
            }
        };
    }
}
