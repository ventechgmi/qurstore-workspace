
import {from as observableFrom,  Observable } from "rxjs";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import {
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ValidationMessages } from '../helpers/validation-messages';
import { AlertService } from "../../../utils/alert/alert.service";
// import { AuthService } from "src/app/utils/auth.service";

import { VendorService } from '../services/vendor-management.service';
import { VendorViewModel } from './vendor-management.viewmodel';
import { AuthService } from 'src/app/utils/auth.service';
import { GuidHelper } from '../../employee-dashboard/helpers/guid-helper';


@Injectable()
export class VendorManagementDetailViewModel extends VendorViewModel {

  public vendorDetailsForm: FormGroup;
  private targetDetailEntity: any;
  submitted = false;
  modalType = "Edit" || "Create" || "Detail";
  isFormValueChange = false; // to disable save btn if form fields not changed
  isExist: boolean;
  vendorId: any;
  vendorName: any;
  textBasedOnModalType: string;
  addressInformation: any;
  file: any;
  public constructor(
    public vendorService: VendorService,
    private router: Router,
    private fb: FormBuilder,
    private alertService: AlertService,
    public ngxSmartModalService: NgxSmartModalService,
    private chRef: ChangeDetectorRef,
    public authService: AuthService,
    public route: ActivatedRoute
  ) {
    super(vendorService, ViewModelType.ReadOnly, ngxSmartModalService, route);

  }
  public init(params?) {
      this.modalType = params.modalType;
      this.vendorDetailsForm = this.fb.group({
        name:  ["", [Validators.required]],
        website:  ["", [this.validWebsite()]],
        address1:  "",
        address2: "",
        city:  ["", [Validators.required]],
        state:   ["", [Validators.required]],
        zip:  ["", [Validators.minLength(5)]]
      });
      if (this.modalType === "Create") {
        this.textBasedOnModalType = "Create New Vendor";
      } else {
          this.textBasedOnModalType = "Edit Vendor Details";
          // super.init(params)
          // .subscribe((data: any) => {
          //   const data1 = data;
          // });
          this.getFormData();
          if (params.id) {
            this.vendorId = params.id; // this.targetDefinitionDetail.id;
          } else {
            this.alertService.error(ValidationMessages.idNotFound, false);
          }
      }
      return observableFrom([]);
    }

    private loadFormData(data){
      this.vendorDetailsForm.patchValue({
        name: data.name,
        website: data.webUrl
      });
      this.vendorName = data.name;
      this.addressInformation = data.organizationAddress;
      if (this.addressInformation && this.addressInformation.length > 0) {
          this.vendorDetailsForm.patchValue({
            address1: data.organizationAddress[0].addressLine1,
            address2: data.organizationAddress[0].addressLine2,
            city: data.organizationAddress[0].city,
            state: data.organizationAddress[0].state,
            zip: data.organizationAddress[0].zip
          });
      }
    }

    public errorFor(propertyName: string): string | void {
      if (propertyName) {
        const property = propertyName.replace(/\w+/g, (w) => w[0].toUpperCase() + w.slice(1).toLowerCase());
        const splittedProperty = property.split('.');
        const fieldProperty = splittedProperty.length >= 2 ? splittedProperty[1] : splittedProperty[0];
  
        const control = this.vendorDetailsForm.get(propertyName);
  
        if (control) {
          if (control.hasError("required")) {
            return fieldProperty.concat(ValidationMessages.RequiredMessage);
          }
          else if (control.hasError("minlength")
                    || control.hasError("maxlength")) {
            return fieldProperty.concat(ValidationMessages.MinMaxLengthMessage);
          }
          else if (control.hasError("email") || control.hasError("pattern")) {
            return fieldProperty.concat(ValidationMessages.InvalidMessage);
          }
          else if (control.hasError("confirmedValidator")) {
            return ValidationMessages.EmailMatchMessage;
          }
          else {
            return null;
          }
        }
      }
    }


    private  getFormData(){
      const request = { id: this.vendorId};
        this.vendorService.getVendorById(this.vendorId).subscribe(data => {
         if (data) {
            this.loadFormData(data);
         } else {
             this.alertService.error(data.message, true);
         }
      }, (requestFailed) => {
          (requestFailed.error && requestFailed.error.validationErrors) ?
              this.alertService.error(requestFailed.error.validationErrors.message, true) :
              this.alertService.error(requestFailed.error.message, true);
      //    this.ngxSmartModalService.getModal("deleteConfirmModalFromList").close();
      }, () => {
         // this.ngxSmartModalService.getModal("deleteConfirmModalFromList").close();
     });
    }
  get vendorDetailsFormControls() {
    return this.vendorDetailsForm.controls;
  }

  public get detailForm() {
    return this.vendorDetailsForm.controls;
  }
   public saveChanges() {
     this.submitted = true;
    if (this.vendorDetailsForm.invalid) { return; }
    // if (!this.isValidInput()) {
     //   return;
     // }
     this.transformToEntityModel();

      this.vendorService.saveVendor(this.targetDetailEntity)
     .subscribe(
       (data) => {
         this.submitted = false;
         this.alertService.success(data.message, false);
         this.vendorService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
       },
       (err) => {
         this.submitted = false;
         
         this.alertService.error(err.error.errorList[0], false);
       },
       () => {
          this.navigateToList().then().catch();
       });
 
   }

  public transformToEntityModel() {
    const { name, website, address1, address2, city, state, zip  } = this.vendorDetailsFormControls;
    this.targetDetailEntity = {
      id: this.vendorId,
      name: name.value,
      isEnabled: true,
      webUrl : website.value,
      lastModifiedBy: this.authService.getUserId(),
      organizationAddress : this.transformModuleToEntityModel()
    };
    return this.targetDetailEntity;
  }
  public transformModuleToEntityModel() {
    const orgAddressList: any[] = [];
    const {  address1, address2, city, state, zip  } = this.vendorDetailsFormControls;
    let addressId ;
    if(this.addressInformation && this.addressInformation.length > 0) {
      addressId = this.addressInformation[0].id;
    } else {
      addressId = new GuidHelper();
    }
    const orgAddressInfo: any = {
            organizationId: this.vendorId,
            addressLine1 : address1.value,
            addressLine2 : address2.value,
            city: city.value,
            state: state.value,
            zip: zip.value,
            lastModifiedBy: this.authService.getUserId(),
            id: addressId
          };
    orgAddressList.push(orgAddressInfo);
    return orgAddressList;
  }
  public onCancel() {
    if (this.modalType === "Edit" || this.modalType === "Create") {
      this.vendorDetailsForm.dirty ? this.ngxSmartModalService.getModal("vendorCancelConfirmModal").open() :  this.navigateToList().then().catch();
    } else {
      this.navigateToList().then().catch();
    }
  }
  

  public async navigateToList() {
    return this.router.navigate(["vendor-management/list"]);
  }
  public onConfirm() {
    this.navigateToList().then().catch();
   }

}
