import {from as observableFrom,  Observable, Subject, forkJoin } from "rxjs";

import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import {  ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from 'src/app/utils/alert/alert.service';
import { VendorService } from '../services/vendor-management.service';
import { VendorViewModel } from './vendor-management.viewmodel';
// import { AuthService } from "src/app/utils/auth.service";


@Injectable()
export class VendorListViewModel extends VendorViewModel {
  public userId: string;
  public vendorId: string;
  public vendorList: any;
  public alertService: AlertService;
  viewModelDataGrid: DataGridViewModel;

  header = [
    {       headerName: "Name", field: "name", isClickable: false     },
    {       headerName: "Website", field: "webUrl"     }];

 // chartOptions: any;
  appliedFilters: any[];
  vendorWithoutUserList: any;
  public constructor(
    public vendorService: VendorService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute
  ) {
   super(vendorService, ViewModelType.Edit, ngxSmartModalService,  route);
  }

  public init(param?) {
  //  super.initUserClaims();
    this.userId = param.userId;
    this.viewModelDataGrid = new DataGridViewModel();
    this.subscribeToCreateSuccessMessage();
   // this.initSubscribeMethods();
    this.subscribers();
    this.viewModelDataGrid.setHeader(this.header, false, { canUpdate: true, canDelete: true});

    // this.setDataTableConfigurations();
    this.getVendorsList();
    return observableFrom([]);
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }
   
  private subscribers() {
    this.viewModelDataGrid.events.subscribe(e => {
        switch (e.eventType) {
            case GridConfig.editClicked:
                this.vendorId = e.eventData.rowDetails[0];
                this.redirectToDetail();
                break;
            case GridConfig.deleteClicked:
                this.vendorId = e.eventData.rowDetails[0];
                this.deleteUserFromList();
                break;
            default:
                if (e.eventData && e.eventData.rowDetails) {
                  this.vendorId = e.eventData.rowDetails[0];
                  this.redirectToDetail();
                }
                break;
        }

    }, error => { });
}
  // Getting all the vendor details and redirecting to the vendor details page
  private redirectToDetail() {
    this.vendorService.vendorIdSubject.next(this.vendorId);
    this.router
      .navigate(["/vendor-management/edit/" + this.vendorId])
      .then()
      .catch();
  }
  public redirectToCreate() {
    this.router
      .navigate(["/vendor-management/new"])
      .then()
      .catch();
  }

  public deleteUserFromList() {
      this.ngxSmartModalService.getModal("deleteConfirmModalFromList").open();
  }

  public async  deleteVendor() {
    this.ngxSmartModalService.getModal("deleteConfirmModalFromList").close();
      // const request = { vendorId: this.vendorId, currentUserId: this.currentUserId };  // Once Auth is implemented
      const request = { id: this.vendorId};
      await this.vendorService.deleteVendor(request).subscribe(data => {
         if (data.isSuccess) {
            this.alertService.success(data.message, true);
            this.getVendorsList();
         } else {
             this.alertService.error(data.message, true);
         }
      }, (requestFailed) => {
          (requestFailed.error) ?
              this.alertService.error(requestFailed.error.errorList[0], true) :
              // this.alertService.error(requestFailed.error.message, true);
          this.ngxSmartModalService.getModal("deleteConfirmModalFromList").close();
      }, () => {
          this.ngxSmartModalService.getModal("deleteConfirmModalFromList").close();
     });
    }

  // // data-table configurations for project management
  // public setDataTableConfigurations() {
  //   this.viewModelDataGrid.dataTableConfiguration = {
  //     destroy: true,
  //     columnDefs: [{ targets: [0], visible: false, searchable: false }, { type: "num-html", targets: [2] }, { orderable: false, targets: 2, searchable: false }],
  //     order: [[1, "asc"]]
  //   };
  // }

  public getVendorsList() {
    this.vendorService.getAllVendors().subscribe(
      data => {
        if (data) {
          this.vendorList = data;
          this.initGrid(this.vendorList);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public initGrid(data) {
    const vendors = data ? data : [];
    vendors.map(vendor => {
      vendor.moduleCount = vendor.moduleCount === undefined ? "0" : vendor.moduleCount;
    });
    this.viewModelDataGrid.setRowsData(vendors, false);
  }

  public subscribeToCreateSuccessMessage() {
    this.vendorService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      if (!data.isCreateAnother) {
        this.alertService.success(message, toStable);
      }
    });
  }
}
