import {
    Component,
    OnInit,
    AfterContentInit
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { NgxSmartModalService } from "ngx-smart-modal";
import { VendorListViewModel } from '../../viewmodels/vendor-management-list.viewmodel';
import { AlertService } from 'src/app/utils/alert/alert.service';
import { VendorService } from '../../services/vendor-management.service';
@Component({
    selector: "app-vendor-management-list-view",
    templateUrl: "./vendor-management-list-view.component.html",
    styleUrls: ["./vendor-management-list-view.component.scss"],
    providers: [ VendorListViewModel, AlertService] // need separate alert instance
})
export class VendorManagementListViewComponent implements OnInit, AfterContentInit {

    constructor(private router: Router,
                private route: ActivatedRoute,
                private vendorService: VendorService,
                public vendorListViewModel: VendorListViewModel,
                public alertService: AlertService,
                public ngxSmartModalService: NgxSmartModalService
    ) {
        alertService.afterInitObs = vendorService.getAfterIntObs();
    }

    public  ngOnInit() {
        this.route.params
            .subscribe(params => {
               this.vendorListViewModel.initServices(this.alertService);
               this.vendorListViewModel.init();
            });

        this.route.queryParams.subscribe(res => {
          //  this.projectId = res.id;
        });
    }
    public ngAfterContentInit() {
    }

}
