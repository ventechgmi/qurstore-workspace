import { Component, OnInit} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
 import { AuthService } from "src/app/utils/auth.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { VendorManagementDetailViewModel } from '../../viewmodels/vendor-management-detail.viewmodel';

import { VendorService } from '../../services/vendor-management.service';
@Component({
  selector: "app-vendor-management-create-edit-detail-view",
  templateUrl: "./vendor-management-create-edit-detail-view.component.html",
  styleUrls: ["./vendor-management-create-edit-detail-view.component.scss"],
  providers: [VendorManagementDetailViewModel, AlertService, VendorService]
})
export class VendorManagementCreateEditDetailViewComponent implements OnInit {
  // userId: string;
  constructor(
    public vendorManagementDetailViewModel: VendorManagementDetailViewModel,
    public vendorService: VendorService,
    private router: ActivatedRoute,
    private alertService: AlertService,
    private authService: AuthService,
  ) {
    // /this.userId = this.authService.getUserId();
  }

  public  ngOnInit() {
    this.router.params
      .subscribe(params => {
        this.initViewModels(params.action, params);
      });
  }

  public initViewModels(action: string, params: any) {
    const detailParams = {
       id: params.vendorId,
       modalType: (params.action === "new") ? "Create" :  "Edit"
    };
    if(detailParams.modalType === "Edit") {
      this.vendorManagementDetailViewModel.vendorId = params.vendorId;
    }
    this.vendorManagementDetailViewModel.init(detailParams);
  }
}
