import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { VendorManagementListViewComponent } from './views/vendor-management-list-view/vendor-management-list-view.component';
import { VendorManagementCreateEditDetailViewComponent } from './views/vendor-management-create-edit-detail-view/vendor-management-create-edit-detail-view.component';

export const routes: Routes = [
  {
    path: "vendor-management/list",
    component: VendorManagementListViewComponent
  },
  {
    path: "vendor-management/:action/:vendorId",
    component: VendorManagementCreateEditDetailViewComponent
  },
  {
    path: "vendor-management/:action",
    component: VendorManagementCreateEditDetailViewComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorManagementRoutingModule { }
