
import {map} from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";

const appConfigLocal = require("../../../app-config.json");
@Injectable({
    providedIn: "root"
})
export class VendorService extends EntityService {
        appConfig: AppConfig;
        onUpdateListEvent = new EventEmitter();
        public vendorIdSubject = new ReplaySubject<any>();
        public showSuccessMessage = new Subject<any>();
        public alertInfo = { message: "", type: "" };
        private afterInitSubject = new BehaviorSubject(this.alertInfo);

        constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
            super(http, "vendor", appConfig);
            this.appConfig = appConfig;

        }
        


        private getAPIURL(){
            let vendorServiceAPI = "";
            const  serverDeployment = appConfigLocal.serverDeployment;
            if (serverDeployment === "0") {
                vendorServiceAPI = "https://localhost:4001/vendor";
            } else if (serverDeployment === "1") {
                vendorServiceAPI = "https://api.qurstore-qa-vendor.vsolgmi.com/vendor";
            }  else {
                vendorServiceAPI = this.getApi();
            }
            return vendorServiceAPI;
        }
        private generateGetAPI(name, req): Observable<any> {
            return this.http
            .get(this.getAPIURL() + name + req).pipe(
            map(res => {
                return res;
            }));
        }
        private generatePostAPI(name, data): Observable<any> {
            return this.http
            .post(this.getAPIURL() + name, data).pipe(
            map((res: Response) => res));
        }

        private generateDeleteAPI(name, data): Observable<any> {
            return this.http
            .delete(this.getAPIURL() + name, data).pipe(
                map(res => {
                    return res;
                }));
        }
        public  getAllVendors(): Observable<any> {
         return  this.generateGetAPI("/list", "");
        }

        public  getVendorById(vendorId): Observable<any> {
            return  this.generateGetAPI("/getVendorById/" + vendorId, "");
           }
        public  saveVendor(vendorDetails): Observable<any> {
            return  this.generatePostAPI("/save", vendorDetails);
        }
        public  deleteVendor(vendorDetails): Observable<any> {
            return  this.generateDeleteAPI("/delete/" +  vendorDetails.id, vendorDetails );
        }
        public  getVendorModules(): Observable<any> {
            return  this.generateGetAPI("/module-list", "");
        }

        public getVendorByName(req): Observable<any> {
            return  this.generateGetAPI("/vendor-detail/by-name/", req);
        }

        public canDeleteVendor(req): Observable<any> {
            return  this.generateGetAPI("/can/delete/vendor/", req);
        }


        public showAfterIntMessage(messageInfo) {
            this.afterInitSubject.next(messageInfo);
        }

        public  getAfterIntObs(): Observable<any> {
            return this.afterInitSubject.asObservable();
        }

        public getVendorIdSubject() {
            return this.vendorIdSubject.asObservable();
        }

        public getShowSuccessMessage() {
            return this.showSuccessMessage.asObservable();
        }

        public getUpdateVendorListEvent() {
            return this.onUpdateListEvent;
        }
}
