import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DataGridModule } from "../core-ui-components";
import { DataGridExtendedModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import {MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatFormFieldModule} from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import {  MatButtonModule} from "@angular/material/button";
import {  MatFormFieldControl} from "@angular/material/form-field";
import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";

import { MatSelectModule } from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {LocalStorageManager} from "../../utils/local-storage-manager";
import { VendorManagementRoutingModule } from './vendor-management-routing-module';
import { VendorManagementListViewComponent } from './views/vendor-management-list-view/vendor-management-list-view.component';
import { VendorManagementCreateEditDetailViewComponent } from './views/vendor-management-create-edit-detail-view/vendor-management-create-edit-detail-view.component';
const modules = [MatSelectModule];

@NgModule(
    {
        declarations: [
            VendorManagementListViewComponent,
            VendorManagementCreateEditDetailViewComponent
        ],
        imports: [
            SharedModules,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            VendorManagementRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatSelectModule,
            MatButtonModule,
            DataGridModule,
            DataGridExtendedModule,
            MatCheckboxModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [
            VendorManagementListViewComponent,
            VendorManagementCreateEditDetailViewComponent

        ],
        providers: [
            AlertService,
            NgxSmartModalService,
            LocalStorageManager
        ]
    }
)
export class VendorManagementModule {

}
