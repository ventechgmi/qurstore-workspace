
export enum REPORT_NAMES {
    REPORTS = 'Reports',
    ADMISSION_DIAGNOSIS_COST = 'Admission Diagnosis Cost',
    CHRONIC_DISEASE_COST = 'Chronic Disease Cost',
}