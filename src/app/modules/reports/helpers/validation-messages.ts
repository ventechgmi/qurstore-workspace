export class ValidationMessages {
    public static readonly idNotFound = "Error occurred: please navigate back to List screen. ";
    public static readonly NoModuleSelected = "Select at least one module to continue.";
}
