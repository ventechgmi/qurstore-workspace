import {
    Component,
    OnInit
} from "@angular/core";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from "../../../../../utils/alert/alert.service";
import { REPORT_NAMES } from "../../../common/report-management.constants";

@Component({
    selector: "app-reports-list-view",
    templateUrl: "./reports-list-view.component.html",
    styleUrls: ["./reports-list-view.component.scss"],
    providers: [AlertService] // need separate alert instance
})
export class ReportsListViewComponent implements OnInit {

    nationalChart: any;
    stateChart: any;
    name: string;
    activeChart: string;
    REPORT_NAMES = REPORT_NAMES;
    constructor(
        public alertService: AlertService,
        public ngxSmartModalService: NgxSmartModalService
    ) {}

    public ngOnInit() {
        this.activeChart = REPORT_NAMES.REPORTS;
    }


}
