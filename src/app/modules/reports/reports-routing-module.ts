import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { ReportsListViewComponent } from './views/reports/reports-list-view/reports-list-view.component';

export const routes: Routes = [
  {
    path: "list",
    component: ReportsListViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsManagementRoutingModule { }
