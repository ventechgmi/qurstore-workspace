import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { DataGridModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import {MatAutocompleteModule } from "@angular/material/autocomplete";
import {MatInputModule} from "@angular/material/input";
import { MatFormFieldModule} from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import {  MatButtonModule} from "@angular/material/button";

// import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";
import { ReportsListViewComponent } from './views/reports/reports-list-view/reports-list-view.component';
import { ReportsManagementRoutingModule } from './reports-routing-module';

@NgModule(
    {
        declarations: [
            ReportsListViewComponent
        ],
        imports: [
            // SharedModules,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            ReportsManagementRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatButtonModule,
            DataGridModule,
            MatCheckboxModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [
            ReportsListViewComponent
        ],
        providers: [
            AlertService,
            NgxSmartModalService
        ]
    }
)
export class ReportsModule {

}
