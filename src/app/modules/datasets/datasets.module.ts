import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DataGridModule } from "../core-ui-components";
import { DataGridExtendedModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import {MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatFormFieldModule} from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import {  MatButtonModule} from "@angular/material/button";
import {  MatFormFieldControl} from "@angular/material/form-field";
import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";

import { MatSelectModule } from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {LocalStorageManager} from "../../utils/local-storage-manager";
import { DatasetsRoutingModule } from "./datasets-routing-module";
import { DatasetsListViewComponent } from "./views/datasets-list-view/datasets-list-view.component";
import { DatasetsRequestViewComponent } from "./views/datasets-request-view/datasets-request-view.component";
import { MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns';
const modules = [MatSelectModule];

@NgModule(
    {
        declarations: [
            DatasetsListViewComponent,
            DatasetsRequestViewComponent
        ],
        imports: [
            SharedModules,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            DatasetsRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatSelectModule,
            MatButtonModule,
            DataGridModule,
            DataGridExtendedModule,
            MatCheckboxModule,
            MultiSelectModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [
            DatasetsListViewComponent,
            DatasetsRequestViewComponent
        ],
        providers: [
            AlertService,
            NgxSmartModalService,
            LocalStorageManager
        ]
    }
)
export class DatasetsModule {
}
