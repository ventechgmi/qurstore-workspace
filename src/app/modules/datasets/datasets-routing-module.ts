import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { DatasetsListViewComponent } from "./views/datasets-list-view/datasets-list-view.component";
import { DatasetsRequestViewComponent } from "./views/datasets-request-view/datasets-request-view.component";

export const routes: Routes = [
  {
    path: "datasets/list",
    component: DatasetsListViewComponent
  }
   ,
  {
     path: "datasets/request",
     component: DatasetsRequestViewComponent
  }
  //,
  // {
  //   path: "vendor-management/:action/:vendorId",
  //   component: VendorManagementCreateEditDetailViewComponent
  // },
  // {
  //   path: "vendor-management/:action",
  //   component: VendorManagementCreateEditDetailViewComponent
  // }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DatasetsRoutingModule { }
