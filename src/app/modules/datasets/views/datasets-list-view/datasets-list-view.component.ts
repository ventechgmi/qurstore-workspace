import {
    Component,
    OnInit,
    AfterContentInit
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from 'src/app/utils/alert/alert.service';
import { DatasetsService } from '../../services/datasets.service';
import { DatasetsListViewModel } from '../../viewmodels/datasets-list.viewmodel';
@Component({
    selector: "app-datasets-list-view",
    templateUrl: "./datasets-list-view.component.html",
    styleUrls: ["./datasets-list-view.component.scss"],
    providers: [ DatasetsListViewModel, AlertService] // need separate alert instance
})
export class DatasetsListViewComponent implements OnInit, AfterContentInit {

    constructor(private router: Router,
                private route: ActivatedRoute,
                private datasetsService: DatasetsService,
                public datasetsListViewModel: DatasetsListViewModel,
                public alertService: AlertService,
                public ngxSmartModalService: NgxSmartModalService
    ) {
        alertService.afterInitObs = datasetsService.getAfterIntObs();
    }

    public  ngOnInit() {
        this.route.params
            .subscribe(params => {
               this.datasetsListViewModel.initServices(this.alertService);
               this.datasetsListViewModel.init();
            });

        this.route.queryParams.subscribe(res => {
          //  this.projectId = res.id;
        });
    }
    public ngAfterContentInit() {
    }

}
