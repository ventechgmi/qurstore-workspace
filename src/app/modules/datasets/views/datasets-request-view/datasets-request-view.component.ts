import {
    Component,
    OnInit,
    AfterContentInit
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from 'src/app/utils/alert/alert.service';
import { DatasetsService } from '../../services/datasets.service';
import { DatasetsRequestViewModel } from '../../viewmodels/datasets-request.viewmodel';

export interface DatasetRequests {
    value: string;
    name: string;
    type: string;
    fileName: string;
   size: string;
}
@Component({
    selector: "app-datasets-request-view",
    templateUrl: "./datasets-request-view.component.html",
    styleUrls: ["./datasets-request-view.component.scss"],
    providers: [ DatasetsRequestViewModel, AlertService] // need separate alert instance
})
export class DatasetsRequestViewComponent implements OnInit, AfterContentInit {
    public datasetRequests : DatasetRequests [] = [];
    constructor(private router: Router,
                private route: ActivatedRoute,
                private datasetsService: DatasetsService,
                public datasetsRequestViewModel: DatasetsRequestViewModel,
                public alertService: AlertService,
                public ngxSmartModalService: NgxSmartModalService
    ) {
        alertService.afterInitObs = datasetsService.getAfterIntObs();
    }

    public  ngOnInit() {
        this.route.params
            .subscribe(params => {
               this.datasetsRequestViewModel.initServices(this.alertService);
               this.datasetsRequestViewModel.init();
            });

        this.route.queryParams.subscribe(res => {
          //  this.projectId = res.id;
        });
        this.fillItems();
    }

    private fillItems(){

        this.datasetRequests = [
            { value: "1", name: "Beneficiary", type: "Beneficiary_Summary", fileName: "de1_beneficiary.csv", size: "100 MB"},
            { value: "2", name: "Medical", type: "Medical_Claims", fileName: "EFA1_beneficiary.csv", size: "200 MB"},
            { value: "3", name: "Carrier", type: "Carrier_Claims", fileName: "FE1_beneficiary.csv", size: "300 MB"}
        ];
    }
    public showOptions(e) {
        const temp = e;
    }


    public ngAfterContentInit() {
    }

}
