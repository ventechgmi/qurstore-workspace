import {from as observableFrom,  Observable, Subject, forkJoin } from "rxjs";

import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import {  ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from 'src/app/utils/alert/alert.service';
import { DatasetsService } from '../services/datasets.service';
import { DatasetsViewModel } from './datasets.viewmodel';
import { ValidationMessages } from '../helpers/validation-messages';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TagTypeService } from '../services/tag-type.service';
// import { AuthService } from "src/app/utils/auth.service";


@Injectable()
export class DatasetsRequestViewModel extends DatasetsViewModel {
  public userId: string;
  public vendorId: string;
  public dataUserAgreementId:  string ;
  public availableCatalogId:  string ;
  public datasetCatalogList:  any[] = [];

  public requestedDatasetList: any[] = [];
  public alertService: AlertService;
  viewModelDataGrid: DataGridViewModel;
  requestedDatasetDataGrid: DataGridViewModel;
  public datasetsRequestViewModelForm: FormGroup;
  public isNeededByInvalid = false;
  public workspaceTagTypeDetailsList: any;
  public pleaseRemoveMeIfSeen = 1;
  headerCatalog = [
    {       headerName: "Name", field: "fileName",   checkboxSelection: true, width: 150 , headerCheckboxSelection: true, headerCheckboxSelectionFilteredOnly: true    },
    {       headerName: "Description", field: "fileDescription", width: 190 },
    {       headerName: "Type", field: "datasetTypeName", isClickable: false, width: 75  },
  ];
  headerRequest = [
    {       headerName: "Description", field: "fileDescription",   checkboxSelection: true,   width: 250, headerCheckboxSelection: true, headerCheckboxSelectionFilteredOnly: true    },
    {       headerName: "Type", field: "datasetTypeName", isClickable: false ,  },
  ];
  appliedFilters: any[];
  vendorWithoutUserList: any;
  public modalType = "Edit" || "Create" || "Detail";
  public selectedTags: string[] = [];
  // map the appropriate columns to fields property
  public fields: object = {text: 'itemName', value: 'id'};
  // set the placeholder to MultiSelect input element
  public waterMark: string = '-- Select--';
  // set the type of mode for how to visualized the selected items in input element.
  public box : string = 'Box';
  public constructor(
    public datasetsService: DatasetsService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    private fb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public tagTypeService: TagTypeService
  ) {
   super(datasetsService, ViewModelType.Edit, ngxSmartModalService,  route);
  }

  public init(param?) {
      this.userId = param.userId;
      this.viewModelDataGrid = new DataGridViewModel();
      this.viewModelDataGrid.actionColumnSize = 80;
      this.viewModelDataGrid.actionColumnName = "Data Usage Agreement";
      this.requestedDatasetDataGrid = new DataGridViewModel();
      // this.viewModelDataGrid.setPagination(false);
      this.subscribeToCreateSuccessMessage();
     // this.initSubscribeMethods();
      this.subscribers();
      this.viewModelDataGrid.setHeader(this.headerCatalog, false, { canViewDataUsageAgreement: true});
      this.viewModelDataGrid.gridOptionRowSelectionToMultiple();
      this.requestedDatasetDataGrid.setHeader(this.headerRequest, false);
      this.requestedDatasetDataGrid.gridOptionRowSelectionToMultiple();
      this.getWorkspaceCatalogList();
      this.getWorkspaceTagTypeDetails();
      this.datasetsRequestViewModelForm = this.fb.group({
        requestName: ["", Validators.required],
        neededBy: ["", Validators.required],
        reasonForRequest: ["", Validators.required],
        selectedDatasetHidden: ["", Validators.required],
      });
      return observableFrom([]);
    }

    private resetControls() {
      this.datasetsRequestViewModelForm.patchValue({
        requestName: "",
        neededBy: "",
        reasonForRequest: "",
        selectedDatasetHidden: ""
      });
    }

    public onSelect(args: any, workspaceTagDetail: any): void{
      if(args && (args.itemData.value === undefined)) {
        args.itemData.isCustomCreated = true;
        args.itemData.tagTypeId = workspaceTagDetail.id;
      }
      workspaceTagDetail.selectedTags.push(args.itemData);
    }

    public onRemove(args: any, workspaceTagDetail: any): void{
      const tempSelectedTags = [];
      _.each(this.selectedTags, selectedTags => {
          if(args.itemData.itemName !== selectedTags.itemName) {
            tempSelectedTags.push(selectedTags);
          }
      });
      workspaceTagDetail.selectedTags = tempSelectedTags;
    }

    public transformToEntityModel() {
      const { requestName, neededBy, reasonForRequest} = this.datasetsRequestViewModelFormControls;
      const tempWorkspaceTagTypeDetailsList = this.workspaceTagTypeDetailsList;
      _.each(tempWorkspaceTagTypeDetailsList, workspaceTagTypeDetail => {
        workspaceTagTypeDetail.lastModifiedBy = this.userId;
        _.each(workspaceTagTypeDetail.selectedTags, selectedTagDetail => {
            if(selectedTagDetail && selectedTagDetail.isCustomCreated) {
                selectedTagDetail.value = selectedTagDetail.itemName;
                delete selectedTagDetail.id;
            }
        });
      });
      return    {
        name : requestName.value,
        neededByDateTime : neededBy.value,
        comments : reasonForRequest.value,
        datasetId: this.requestedDatasetDataGrid.getAllRowsId(),
        workspaceTagMappingDetails: tempWorkspaceTagTypeDetailsList
      };
    }

    public addToCart() {
      const selectedRows = this.viewModelDataGrid.getSelectedRows();
      this.viewModelDataGrid.onRemoveSelected(selectedRows);
      this.requestedDatasetDataGrid.onAddSelected(selectedRows);
      if (selectedRows) {
        this.datasetsRequestViewModelForm.patchValue({
          selectedDatasetHidden: 1
        });
      }
    }

    public removeFromCart() {
      const selectedRows = this.requestedDatasetDataGrid.getSelectedRows();
      this.requestedDatasetDataGrid.onRemoveSelected(selectedRows);
      this.viewModelDataGrid.onAddSelected(selectedRows);
      const hasRows = this.requestedDatasetDataGrid.getAllRowsId();
      if(hasRows?.length === 0) {
        this.datasetsRequestViewModelForm.patchValue({
          selectedDatasetHidden: ""
        });
      }
    }

    public  cancelChanges() {
      this.resetControls();
      this.redirectToList();
    }
    public  saveChanges() {
      if (this.datasetsRequestViewModelForm.invalid) { return; }
      if (!this.validateForm()) { return; }
      const saveRequestData = this.transformToEntityModel();
      this.datasetsService.createDatasetRequest(saveRequestData)
        .subscribe(
          (data) => {
            // this.alertService.success(data.message, false);
            this.datasetsService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
            this.resetControls();
            this.redirectToList();
          },
          (err) => {
            this.alertService.error(err.error.errorList[0], false);
          },
          () => {
        });
    }
    public redirectToList() {
      this.router
        .navigate(["/datasets/list"])
        .then()
        .catch();
    }
  private validateForm() {
        const {  neededBy  } = this.datasetsRequestViewModelFormControls;
        if (new Date(neededBy.value).getTime() >= new Date().getTime()) {
          this.isNeededByInvalid =  false;
        } else {
          this.isNeededByInvalid =  true;
        }
        return !this.isNeededByInvalid;
  }

  public initGrid(data) {
    const datasetCatalogs = data ? data : [];
    datasetCatalogs.map(datasetCatalog => {
      if (datasetCatalog.lastModifiedDate) {
        datasetCatalog.lastModifiedDateDisplay = this.getFormattedDate(new Date(datasetCatalog.lastModifiedDate)) + " " + new Date(datasetCatalog.lastModifiedDate).toLocaleTimeString();
      }
    });
    this.viewModelDataGrid.setRowsData(data);
    // this.viewModelDataGrid.setPageSize (5);

  }
  public onCancel() {

  }
  public requestNewWorkspace() {
    this.ngxSmartModalService.getModal("requestNewWorkspaceFromCatalog").open();
  }
  public deleteUserFromList() {
      this.ngxSmartModalService.getModal("deleteConfirmModalFromList").open();
  }

  public async  deleteWorkspace() {
    this.ngxSmartModalService.getModal("deleteConfirmModalFromList").close();
    this.alertService.success(ValidationMessages.deleteSuccessMessage, true);
  }

  get datasetsRequestViewModelFormControls() {
    return this.datasetsRequestViewModelForm.controls;
  }

  clearData() {
    this.requestedDatasetDataGrid.setRowsData([]);
  }
  public get detailForm() {
    return this.datasetsRequestViewModelForm.controls;
  }
  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }
  private subscribers() {
      this.viewModelDataGrid.events.subscribe(e => {
        switch (e.eventType) {
          case GridConfig.viewDataUsageAgreement:
            this.availableCatalogId = e.eventData.rowDetails[0];
            this.downloadFile();
            break;
          default:
                break;
        }
    },  error => {
    });

  }
 private downloadFile() {
   const row = this.datasetCatalogList.filter(x => x.id === this.availableCatalogId);
   if (row && row.length > 0) {
     this.dataUserAgreementId  = row[0].dataUsageAgreementId;
     const fileName = row[0].dataUsageAgreementFileName.replace(/ /g, '_');
     this.datasetsService.downloadDataUsageAgreementDocument(this.dataUserAgreementId)
     .subscribe(
      blob => {
        const a = window.document.createElement("a");
        a.href = window.URL.createObjectURL(blob);
        a.download = fileName;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);

        // let url = window.URL.createObjectURL(blob);
        // let pwa = window.open(url);
        // if (!pwa || pwa.closed || typeof pwa.closed == 'undefined') {
        //     alert( 'Please disable your Pop-up blocker and try again.');
        // }

       },
      error => {
        this.alertService.error(error);
      }
    );


    // this.employeeDashboardService.downloadFile(uploadedFileId, fileType)
    //  .subscribe(
    //   blob => {
    //     const a = window.document.createElement("a");
    //     a.href = window.URL.createObjectURL(blob);
    //     a.download = fileName;
    //     document.body.appendChild(a);
    //     a.click();
    //     document.body.removeChild(a);
    //    },
    //    error => {
    //      console.log(error);
    //      // test
    //    });



   }
 }


public subscribeToCreateSuccessMessage() {
  this.datasetsService.showSuccessMessage.subscribe(data => {
    const { message, toStable } = data;
    if (!data.isCreateAnother) {
      this.alertService.success(message, toStable);
    }
  });
}


  public getWorkspaceCatalogList() {
    this.datasetsService.getCatalogList().subscribe(
      data => {
        if (data) {
          this.datasetCatalogList =  data;
          this.initGrid(data);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public initRequestedDatasetGrid(data) {
    this.requestedDatasetDataGrid.setRowsData(data);
  }
  private getFormattedDate(date) {
    const year = date.getFullYear();
    let month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    let day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;
  }

  private getWorkspaceTagTypeDetails(): void {
    this.workspaceTagTypeDetailsList = [];
    this.tagTypeService.getWorkspaceTagTypeDetails("Dataset").subscribe(
      data => {
        if (data) {
          _.each(data, item => {
            item.selectedTags = [];
            _.each(item.tagValues, tagValue => {
                tagValue.itemName = tagValue.value;
            });
          });
          this.workspaceTagTypeDetailsList = data;
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
}
