import {from as observableFrom,  Observable, Subject, forkJoin } from "rxjs";

import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import {  ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from 'src/app/utils/alert/alert.service';
import { DatasetsService } from '../services/datasets.service';
import { DatasetsViewModel } from './datasets.viewmodel';
// import { AuthService } from "src/app/utils/auth.service";
import { debug } from 'console';
import { ValidationMessages } from '../helpers/validation-messages';
import { FormGroup, FormBuilder } from '@angular/forms';
import moment from 'moment';
import { AuthService } from '../../../utils/auth.service';
import { TagTypeService } from '../services/tag-type.service';


@Injectable()
export class DatasetsListViewModel extends DatasetsViewModel {
  public datasetRequestId: any;
  public alertService: AlertService;
  viewModelDataGrid: DataGridViewModel;
  datasetsRequestDataGrid: DataGridViewModel;
  auditDataGrid: DataGridViewModel;
  public isSystemAdmin = false;
  public pageHeading = "Dataset";
  public datasetsListViewModelForm: FormGroup;
  datasetRequestDetailsHeader : string;
  // private datasetRequestList: any;
  private datasetRequestDetails: any;

  blankReasonForTermination = false;
  modalType = "Edit"  || "Detail";
  header: any  []  = [];
  dataRequestHeader =  [
    {       headerName: "Name", field: "fileName" , width: 200   },
    {       headerName: "Type", field: "type"  }
  ];
  auditHeader =  [
    {       headerName: "Action Date", field: "actionDateDisplay"    },
    {       headerName: "User Name", field: "actionBy.name"    },
    {       headerName: "Status", field: "actionStatus"  },
    {       headerName: "Comments", field: "comments"  }
  ];
  // -------------------------------------------------------------------
  public requestedByNameLabel: string;
  public neededByLabel: string;
  public nameLabel: string;
  public requestDateTimeLabel: string;
  public requestReasonLabel: string;
  public statusLabel: string;
  public selectedTags: string[] = [];
  // map the appropriate columns to fields property
  public fields: object = {text: 'itemName', value: 'id'};
  // set the placeholder to MultiSelect input element
  public waterMark: string = '-- Select--';
  // set the type of mode for how to visualized the selected items in input element.
  public box : string = 'Box';
  public datasetTagTypeSelectedDetailsList: any;
  public datasetTagTypeDetailsList: any;
  private requestNewDatasetFromCatalog:string = "requestNewDatasetFromCatalog";
  private dateTimeFormat:string = "MM/DD/YYYY hh:mm A";
  private dateFormat:string = "MM/DD/YYYY";
  private terminateConfirmModalFromList:string = "terminateConfirmModalFromList";
  // -------------------------------------------------------------------

  appliedFilters: any[];
  vendorWithoutUserList: any;
  public constructor(
    public datasetsService: DatasetsService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    private fb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public authService: AuthService,
    public tagTypeService: TagTypeService
  ) {
   super(datasetsService, ViewModelType.Edit, ngxSmartModalService,  route);
  }

  public init() {
    this.isSystemAdmin = this.authService.isSysAdminBoolean();
    this.viewModelDataGrid = new DataGridViewModel();
    this.datasetsRequestDataGrid = new DataGridViewModel();
    this.auditDataGrid = new DataGridViewModel();
    this.subscribeToCreateSuccessMessage();
    this.subscribers();
    this.buildHeader();
    this.setHeader();
    // this.setDataTableConfigurations();
    this.loadDataGrid();

    this.datasetsListViewModelForm = this.fb.group({
      decisionComment: "",
      terminateDecisionComment: ""
    });
    this.datasetTagTypeDetailsList = [];
    this.datasetTagTypeSelectedDetailsList = [];
    return observableFrom([]);
  }
  private resetControls() {
    this.datasetsListViewModelForm.patchValue({
      decisionComment: "",
      terminateDecisionComment: ""
    });

  }

  private setHeader(){
    if (this.isSystemAdmin) {
        this.viewModelDataGrid.setHeader(this.header, false, { canCancelRequest: false, canTerminate: true, canApproveReject: true });
    } else {
      this.viewModelDataGrid.setHeader(this.header, false, {canCancelRequest: true, canTerminate: true});
    }
    this.datasetsRequestDataGrid.setHeader(this.dataRequestHeader, false);
    this.datasetsRequestDataGrid.setPagination(false);

    this.auditDataGrid.setHeader(this.auditHeader, false);
    this.auditDataGrid.setPagination(false);
    this.auditDataGrid.defaultSortModel = [
      {
        colId: 'actionDateDisplay',
        sort: 'desc',
      }
    ];

    this.viewModelDataGrid.defaultSortModel = [
      {
        colId: 'requestDateTimeDisplay',
        sort: 'desc',
      }
    ];
  }
  private buildHeader(){
      this.header.push ({ headerName: "Request Name", field: "name", isClickable: true     } ) ;
      if (this.isSystemAdmin) {
        this.header.push( { headerName: "Requested By", field: "requestedBy.name" } );
      }
      this.header.push (  { headerName: "Needed by", field: "neededByDateDisplay", isClickable: false     });
      this.header.push ({ headerName: "Requested Date", field: "requestDateTimeDisplay", isClickable: false     } );
      this.header.push ({ headerName: "Status", field: "status"     } );
  }
  get datasetsListViewModelFormControls() {
    return this.datasetsListViewModelForm.controls;
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  private subscribers() {
    this.viewModelDataGrid.events.subscribe(e => {
        switch (e.eventType) {
            case GridConfig.columnClicked:
              this.datasetRequestId = e.eventData.rowDetails[0];
              this.modalType = "Detail";
              this.datasetRequestDetailsHeader = "Dataset Request Details";
              this.showDetailsInPopup();
              break;
            case GridConfig.approveRejectClicked:
                this.datasetRequestId = e.eventData.rowDetails[0];
                this.modalType = "Edit";
                this.datasetRequestDetailsHeader = "Approve / Reject Dataset Request";
                this.showDetailsInPopup();
                break;
            case GridConfig.terminateRequestClicked:
                this.datasetRequestId = e.eventData.rowDetails[0];
                this.terminateWorkspacePopUp();
                break;
            case GridConfig.cancelRequestClicked:
                this.datasetRequestId = e.eventData.rowDetails[0];
                this.cancelWorkspacePopUp();
                break;
            default:
                break;
        }
    }, error => { });
  }

  public requestNewWorkspace() {
    // this.ngxSmartModalService.getModal("requestNewWorkspaceFromCatalog").open();
    this.redirectToCreate();
   }

  public showDetailsInPopup() {
    this.getDetailsById();
    this.ngxSmartModalService.getModal(this.requestNewDatasetFromCatalog).open();
  }

  private getDetailsById() {
    this.resetControls();
    this.datasetsService.getDatasetRequestById(this.datasetRequestId).subscribe(
      data => {
        if (data) {
          this.datasetRequestDetails = data;
          this.showDetails(data);
          this.getWorkspaceTagTypeDetailById(this.datasetRequestId);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
  private showDetails(data) {
    this.requestedByNameLabel =  data.requestedBy.name;
    this.neededByLabel  = moment(data.neededByDate).format(this.dateFormat);
    this.nameLabel = data.name;
    this.requestDateTimeLabel = moment(data.requestDateTime).format(this.dateTimeFormat);
    this.requestReasonLabel = data.requestReason;
    this.statusLabel = data.status;
    this.datasetsRequestDataGrid.setRowsData(data.datasets);
    this.initGridAuditLog(data.auditLog);
  }


  public transformToEntityModel() {
    const { decisionComment} = this.datasetsListViewModelFormControls;
    return    {
      datasetRequestId: this.datasetRequestId,
      reason : decisionComment.value,
    };
  }
  public approveRequest() {
      // if (this.datasetsListViewModelForm.invalid) { return; }
      const saveRequestData = this.transformToEntityModel();
      // if (this.validateInputForApproval(saveRequestData)) { return; }
      this.ngxSmartModalService.getModal(this.requestNewDatasetFromCatalog).close();
      this.datasetsService.approveRequest(saveRequestData)
        .subscribe(
          (data) => {
            this.alertService.success(data.message, false);
            this.datasetsService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
            this.datasetRequestId = "";
            this.resetControls();
          },
          (err) => {
            this.alertService.error(`Error while Saving: ${err.error.errorList[0]}`, false);
          },
          () => {
            this.loadDataGrid();
        });
    }


    public terminateWorkspacePopUp() {
      this.ngxSmartModalService.getModal(this.terminateConfirmModalFromList).open();
    }
    public terminateRequest() {
      const { terminateDecisionComment} = this.datasetsListViewModelFormControls;
      return    {
        datasetRequestId: this.datasetRequestId,
        reason : terminateDecisionComment.value,
      };
    }

    public terminateDatasetAction() {
      const saveRequestData = this.terminateRequest();
      if (saveRequestData?.reason.length === 0) {
        this.blankReasonForTermination = true;
        return;
      } else {
        this.blankReasonForTermination = false;
      }
    this.ngxSmartModalService.getModal(this.terminateConfirmModalFromList).close();
    this.datasetsService.terminateRequest(saveRequestData)
      .subscribe(
        (data) => {
          // this.alertService.success(ValidationMessages.deleteSuccessMessage, true);
          this.alertService.success(data.message, false);
          this.datasetRequestId = "";
        },
        (err) => {
          this.alertService.error(err.error.errorList[0], false);
        },
        () => {
          this.loadDataGrid();
      });
    }

    public cancelWorkspacePopUp() {
    }
    public rejectRequest() {
      const saveRequestData = this.transformToEntityModel();
      // if (this.validateInputForRejection(saveRequestData)) { return; }
      this.ngxSmartModalService.getModal(this.requestNewDatasetFromCatalog).close();
      this.datasetsService.rejectRequest(saveRequestData)
        .subscribe(
          (data) => {
            this.alertService.success(data.message, false);
            this.datasetRequestId = "";
            this.resetControls();
          },
          (err) => {
            this.alertService.error(err.error.errorList[0], false);
          },
          () => {
            this.loadDataGrid();
        });
    }

    private loadDataGrid() {
      if (this.isSystemAdmin) {
        this.pageHeading = "Dataset Requests";
        this.getSysAdminDatasetRequest();
      } else {
        this.pageHeading = "My Dataset";
        this.getMyDatasetRequest();
      }
    }

    private getMyDatasetRequest() {
      this.datasetsService.getMyDatasetRequest().subscribe(
        data => {
          if (data) {
          //  this.datasetRequestList = data;
            this.initGrid(data);
          }
        },
        error => {
          this.alertService.error(error);
        }
      );
    }
    private getSysAdminDatasetRequest() {
        this.datasetsService.getSysAdminDatasetRequest().subscribe(
          data => {
            if (data) {
            //  this.datasetRequestList = data;
              this.initGrid(data);
            }
          },
          error => {
            this.alertService.error(error);
          }
        );
      }

    public redirectToCreate() {
      this.router
        .navigate(["/datasets/request"])
        .then()
        .catch();
    }

  public initGrid(data) {
    const datasetRequests = data ? data : [];
    datasetRequests.map(datasetRequest => {
      if (datasetRequest.requestDateTime) {
        datasetRequest.requestDateTimeDisplay = moment(datasetRequest.requestDateTime).format(this.dateTimeFormat);
      }
      if (datasetRequest.neededByDate) {
        datasetRequest.neededByDateDisplay = moment(datasetRequest.neededByDate).format(this.dateFormat);
      }
    });
    this.viewModelDataGrid.setRowsData(datasetRequests, false);
  }
  public initGridAuditLog(data) {
    const auditLogs = data ? data : [];
    auditLogs.map(auditLog => {
      if (auditLog.actionDate) {
        auditLog.actionDateDisplay = moment(auditLog.actionDate).format(this.dateTimeFormat);
      }
    });
    this.auditDataGrid.setRowsData(auditLogs, false);
  }
  public subscribeToCreateSuccessMessage() {
    this.datasetsService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      if (!data.isCreateAnother) {
        this.alertService.success(message, toStable);
      }
    });
  }

  private getWorkspaceTagTypeDetailById(contextId): void {
    this.datasetTagTypeSelectedDetailsList = [];
    this.tagTypeService.getWorkspaceTagTypeDetailById(contextId).subscribe(
      data => {
        if (data) {
          const tagGropedValue = _.groupBy(data, item => {
            return item.tagValue.tagTypeId;
          });
          const formattedData = [];
          _.each(tagGropedValue, groupData => {
              const data = {
                name: groupData[0].tagValue.tagType.name,
                selectedTagValues: this.getSelectedTagValue(groupData)
              };
              formattedData.push(data);
          });
          this.datasetTagTypeSelectedDetailsList = formattedData;
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  private getSelectedTagValue(groupData): any {
    const values = [];
    _.each(groupData, groupData => {
      values.push(groupData.tagValue.value)
    });
    return values.join(", ");
  }
}
