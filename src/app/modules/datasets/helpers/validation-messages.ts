export class ValidationMessages {
  
        public static readonly deleteSuccessMessage = "Workstation deactivated successfully.";
        public static readonly atLeastOneRoleRequiredMsg = "Please select at least one role for this user";
        public static readonly somethingWentWrong = "Something went wrong. Please try again.";
 
}
