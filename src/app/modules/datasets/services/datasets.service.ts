
import {map} from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
// import { downloadFile } from 'file-saver';
const appConfigLocal = require("../../../app-config.json");

const mockUserDatasetsData = require("../services/mock-user-datasets-data-model.json");
const mockAdminWorkspaceData = require("../services/mock-admin-user-workspace-data-model.json");

@Injectable({
    providedIn: "root"
})
export class DatasetsService extends EntityService {
        appConfig: AppConfig;
        onUpdateListEvent = new EventEmitter();
        public vendorIdSubject = new ReplaySubject<any>();
        public showSuccessMessage = new Subject<any>();
        public alertInfo = { message: "", type: "" };
        private afterInitSubject = new BehaviorSubject(this.alertInfo);

        constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
            super(http, "dataset-request", appConfig);
            this.appConfig = appConfig;
        }
        getRawApi(): string {
            return this.appConfig.apiEndPoint;
        }
        private getAPIURL(){
            return this.getApi();
        }
        private generateGetAPI(name, req): Observable<any> {
            return this.http
            .get(this.getAPIURL() + name + req).pipe(
            map(res => {
                return res;
            }));
        }
        private generatePostAPI(name, data): Observable<any> {
            return this.http
            .post(this.getAPIURL() + name, data).pipe(
            map((res: Response) => res));
        }
        private generatePutAPI(name, data): Observable<any> {
            return this.http
            .put(this.getAPIURL() + name, data).pipe(
            map((res: Response) => res));
        }
        private generateDeleteAPI(name, data): Observable<any> {
            return this.http
            .delete(this.getAPIURL() + name, data).pipe(
                map(res => {
                    return res;
                }));
        }
        public  getAssignedWorkspace(): Observable<any> {
           return  this.generateGetAPI("/list", "");
        }
        public  getCatalogList(): Observable<any> {
            return this.http
                .get(this.getRawApi() + "/FileService/getDatasetCatalog").pipe(
                    map(res => {
                        return res;
                    }));
         }

         public   downloadDataUsageAgreementDocument(dataUsageAgreementId: string): Observable<any> {
            const formData: FormData = new FormData();
            const endpoint = this.getRawApi() + "/FileService/viewDataUsageAgreement";
            formData.set('dataUsageAgreementId',  dataUsageAgreementId);
            return   this.http.post<Blob>(endpoint, formData, {responseType: 'blob' as 'json' });
        }



        public  getMyDatasetRequest(): Observable<any> {
            return  this.generateGetAPI("/my-requests", "");
         }
         public  getSysAdminDatasetRequest(): Observable<any> {
            return  this.generateGetAPI("/admin-dataset", "");
         }
         public  getDatasetRequestById(datasetRequestId): Observable<any> {
            return  this.generateGetAPI("/" + datasetRequestId, "");
         }
        public  getAssignedWorkspaceMock() {
            return  mockUserDatasetsData.userDatasetsData;
        }
        public  getAssignedWorkspaceMockHalf() {
            return  mockUserDatasetsData.userDatasetsData;
        }
        public  getAllWorkspaceMock() {
            return  mockAdminWorkspaceData.adminWorkspaceData;
        }

        public  deleteVendor(vendorDetails): Observable<any> {
            return  this.generateDeleteAPI("/delete/" +  vendorDetails.id, vendorDetails );
        }
        public  createDatasetRequest(data): Observable<any> {
            return  this.generatePostAPI("/create", data);
        }
        public  approveRequest(data): Observable<any> {
            return  this.generatePutAPI("/approve", data);
        }
        public  terminateRequest(data): Observable<any> {
            return  this.generatePutAPI("/delete",  data );
        }
        // public  cancelWorkspaceRequest(data): Observable<any> {
        //     return  this.generatePostAPI("/cancel",  data );
        // }
        public  rejectRequest(data): Observable<any> {
            return  this.generatePutAPI("/reject", data );
        }

        public showAfterIntMessage(messageInfo) {
            this.afterInitSubject.next(messageInfo);
        }

        public  getAfterIntObs(): Observable<any> {
            return this.afterInitSubject.asObservable();
        }

        public getVendorIdSubject() {
            return this.vendorIdSubject.asObservable();
        }

        public getShowSuccessMessage() {
            return this.showSuccessMessage.asObservable();
        }

        public getUpdateVendorListEvent() {
            return this.onUpdateListEvent;
        }
}
