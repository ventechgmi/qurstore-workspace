export class ValidationMessages {
    public static readonly idNotFound = "Error occurred: please navigate back to List screen. ";
    public static readonly ErrorOccurred = "Error occurred: please navigate back to List screen. ";
    public static readonly saveSuccessMessage = "Target Definition has been activated successfully";
    public static readonly draftSuccessMessage = "Target Definition has been saved successfully";
    public static readonly FileSizeError = "File size should not exceed 12MB";
    public static readonly FileNotUploaded = "Invalid File Type: Please upload either XLSX or CSV file.";
    public static readonly ColumnNameMandatory = "Data element name is required";
    public static readonly ColumnNameUnique = "Data element with name '{DuplicateColumnName}' is duplicate. It should be unique";
}
