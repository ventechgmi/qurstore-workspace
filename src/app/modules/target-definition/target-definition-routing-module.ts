import { Routes, RouterModule } from "@angular/router";
import { TargetDefinitionListViewComponent } from "./views/target-definition-list-view/target-definition-list-view.component";
import { NgModule } from "@angular/core";
import { TargetDefinitionCreateEditDetailViewComponent } from './views/target-definition-create-edit-detail-view/target-definition-create-edit-detail-view.component';
import { DefineDataCreateEditDetailViewComponent } from './views/define-data/define-data-create-edit-detail-view.component';
import { TargetDefinitionPreviewViewComponent } from './views/target-definition-preview/target-definition-preview.component';

export const routes: Routes = [
  {
    path: "list",
    component: TargetDefinitionListViewComponent
  },
  {
    path: "upload/:action",
    component: TargetDefinitionCreateEditDetailViewComponent
  },
  {
    path: "upload/:targetDefinitionId",
    component: TargetDefinitionCreateEditDetailViewComponent
  },
  {
    path: "define-data/:key", 
    component: DefineDataCreateEditDetailViewComponent
  },
  {
    path: "preview/:targetDefinitionId/:source",
    component: TargetDefinitionPreviewViewComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TargetDefinitionRoutingModule { }
