import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DataGridModule } from "../core-ui-components";
import { DataGridExtendedModule,ProgressBarModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import {MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatFormFieldModule} from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import {  MatButtonModule} from "@angular/material/button";
import {  MatFormFieldControl} from "@angular/material/form-field";
import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";
import { TargetDefinitionRoutingModule } from "./target-definition-routing-module";
import { TargetDefinitionService } from "./services/target-definition.service";

import { TargetDefinitionListViewComponent } from "./views/target-definition-list-view/target-definition-list-view.component";
import { TargetDefinitionCreateEditDetailViewComponent } from "./views/target-definition-create-edit-detail-view/target-definition-create-edit-detail-view.component";
import { MatSelectModule } from "@angular/material/select";
import { NgSelectModule } from '@ng-select/ng-select';
import {MatInputModule} from "@angular/material/input";
import { DefineDataCreateEditDetailViewComponent } from './views/define-data/define-data-create-edit-detail-view.component';
import {LocalStorageManager} from "../../utils/local-storage-manager";
import { TargetDefinitionPreviewViewComponent } from './views/target-definition-preview/target-definition-preview.component';
const modules = [MatSelectModule];
@NgModule(
    {
        declarations: [
            TargetDefinitionListViewComponent,
            TargetDefinitionCreateEditDetailViewComponent,
            DefineDataCreateEditDetailViewComponent,
            TargetDefinitionPreviewViewComponent,
        ],
        imports: [
            SharedModules,
            NgSelectModule,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            TargetDefinitionRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatSelectModule,
            MatButtonModule,
            DataGridModule,
            DataGridExtendedModule,
            ProgressBarModule,
            MatCheckboxModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [
            TargetDefinitionListViewComponent,
            TargetDefinitionCreateEditDetailViewComponent,
            DefineDataCreateEditDetailViewComponent,
            TargetDefinitionPreviewViewComponent
        ],
        providers: [
            AlertService,
            TargetDefinitionService,
            NgxSmartModalService,
            LocalStorageManager
        ]
    }
)
export class TargetDefinitionModule {

}
