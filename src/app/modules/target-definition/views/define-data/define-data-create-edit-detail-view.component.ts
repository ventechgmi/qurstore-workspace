import { Component, OnInit} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
// import { AuthService } from "src/app/utils/auth.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { DefineDataDetailViewModel } from '../../viewmodels/define-data-detail.viewmodel';
import { TargetDefinitionService } from '../../services/target-definition.service';
import { TargetDefinitionNavigationService } from "src/app/modules/core-ui-services/target-definition-navigation.service";

@Component({
  selector: "app-define-data-create-edit-detail-view",
  templateUrl: "./define-data-create-edit-detail-view.component.html",
  styleUrls: ["./define-data-create-edit-detail-view.component.scss"],
  providers: [DefineDataDetailViewModel, AlertService, TargetDefinitionService]
})
export class DefineDataCreateEditDetailViewComponent implements OnInit {
  // userId: string;
  constructor(
    public defineDataDetailViewModel: DefineDataDetailViewModel,
    public targetDefinitionService: TargetDefinitionService,
    private router: ActivatedRoute,
    public targetDefinitionNavigationService: TargetDefinitionNavigationService
  ) {
   // this.userId = this.authService.getUserId();
  }

  public  ngOnInit() {
    this.router.params
      .subscribe(async params => {
        const data = await this.targetDefinitionNavigationService.navigationParametersForLink(params["key"]);
        if (data && data.targetDefinitionId) {
          this.initViewModels(data.targetDefinitionId, data);
        }
      });
    }
  public initViewModels(targetDefinitionId: string, params: any) {
    const defineDataDetailParams = {
      id: targetDefinitionId,
      ...params
    };
    this.defineDataDetailViewModel.init(defineDataDetailParams);
  }
}
