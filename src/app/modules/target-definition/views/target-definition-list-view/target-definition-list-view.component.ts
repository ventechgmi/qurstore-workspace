import {
    Component,
    OnInit,
    ContentChild,
    AfterViewInit,
    ViewChild,
    ElementRef,
    AfterContentInit,
    OnDestroy
} from "@angular/core";


import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormBuilder } from "@angular/forms";
// import { AuthService } from "src/app/utils/auth.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { TargetDefinitionListViewModel } from '../../viewmodels/target-definition-list.viewmodel';
import { TargetDefinitionService } from "../../services/target-definition.service";


@Component({
    selector: "app-target-definition-list-view",
    templateUrl: "./target-definition-list-view.component.html",
    styleUrls: ["./target-definition-list-view.component.scss"],
    providers: [ TargetDefinitionListViewModel, AlertService] // need separate alert instance
})
export class TargetDefinitionListViewComponent implements OnInit, AfterContentInit,OnDestroy {

    public targetDefinitionList: any;
    public dataTable: any;
    public targetDefinitionCreateModalRef: any;
    projectId: any;
    constructor(private router: Router,
                private targetDefinitionService: TargetDefinitionService,
                public targetDefinitionListViewModel: TargetDefinitionListViewModel,
                public alertService: AlertService,
                private route: ActivatedRoute,
                public ngxSmartModalService: NgxSmartModalService
    ) {
        alertService.afterInitObs = targetDefinitionService.getAfterIntObs();
    }

    public  ngOnInit() {
        this.route.params
            .subscribe(params => {
               this.targetDefinitionListViewModel.initServices(this.alertService);
               this.targetDefinitionListViewModel.init();
            });

        this.route.queryParams.subscribe(res => {
            this.projectId = res.id;
        });
    }

    public ngAfterContentInit() {
    }

    public ngOnDestroy(){
        this.targetDefinitionService.showAfterIntMessage({ text: "", type: "success", toStable: true });
    }

}
