import { Component, OnInit} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
// import { AuthService } from "src/app/utils/auth.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { TargetDefinitionDetailViewModel } from '../../viewmodels/target-definition-detail.viewmodel';
import { TargetDefinitionService } from '../../services/target-definition.service';

@Component({
  selector: "app-target-definition-create-edit-detail-view",
  templateUrl: "./target-definition-create-edit-detail-view.component.html",
  styleUrls: ["./target-definition-create-edit-detail-view.component.scss"],
  providers: [TargetDefinitionDetailViewModel, AlertService, TargetDefinitionService]
})
export class TargetDefinitionCreateEditDetailViewComponent implements OnInit {
  // userId: string;
  constructor(
    public targetDefinitionDetailViewModel: TargetDefinitionDetailViewModel,
    public targetDefinitionService: TargetDefinitionService,
    private router: ActivatedRoute,
    private alertService: AlertService
  ) {
   // this.userId = this.authService.getUserId();
  } 

  public  ngOnInit() {
    this.router.params
      .subscribe(params => {
        this.initViewModels(params.action, params);
      });
    }

  public initViewModels(action: string, params: any) {
    const targetDefinitionDetailParams = {
       id: params.action,
       modalType: (params.action === "new") ? "Create" :  "Edit"
    };
    // modalType: (params.action === "new") ? "Create" : (params.type === "2") ? "Detail" : "Edit"
    this.targetDefinitionDetailViewModel.init(targetDefinitionDetailParams);
  }
}
