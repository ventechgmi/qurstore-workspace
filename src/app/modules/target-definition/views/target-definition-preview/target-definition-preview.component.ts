import {
    Component,
    OnInit,
    ContentChild,
    AfterViewInit,
    ViewChild,
    ElementRef,
    AfterContentInit
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormBuilder } from "@angular/forms";
// import { AuthService } from "src/app/utils/auth.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { TargetDefinitionService } from '../../services/target-definition.service';
import { TargetDefinitionPreviewViewModel } from '../../viewmodels/target-definition-preview';

@Component({
    selector: "app-target-definition-preview",
    templateUrl: "./target-definition-preview.component.html",
    styleUrls: ["./target-definition-preview.component.scss"],
    providers: [ TargetDefinitionPreviewViewModel, AlertService] // need separate alert instance
})
export class TargetDefinitionPreviewViewComponent implements OnInit, AfterContentInit {

    public targetDefinitionList: any;
    public dataTable: any;
    public targetDefinitionCreateModalRef: any;
    projectId: any;
    constructor(private router: Router,
                private targetDefinitionService: TargetDefinitionService,
                public targetDefinitionPreviewViewModel: TargetDefinitionPreviewViewModel,
                public alertService: AlertService,
                private route: ActivatedRoute,
                public ngxSmartModalService: NgxSmartModalService
    ) {
        
    }

    public  ngOnInit() {
      this.route.params
          .subscribe(params => {
             const targetDefinitionPreviewParams = {
              targetDefinitionId: params.targetDefinitionId,
              source: params.source
            };
             this.targetDefinitionPreviewViewModel.initServices(this.alertService);
             this.targetDefinitionPreviewViewModel.init(targetDefinitionPreviewParams);
          });
    }

    public ngAfterContentInit() {
    }
}
