
import {map} from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient, HttpEvent, HttpRequest } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
import { AnyARecord } from 'dns';
import { element } from 'protractor';

const appConfigLocal = require("../../../app-config.json");
@Injectable({
    providedIn: "root"
})
export class TargetDefinitionService extends EntityService {
        appConfig: AppConfig;
        onUpdateListEvent = new EventEmitter();
        public showSuccessMessage = new Subject<any>();
        public alertInfo = { message: "", type: "" };
        private afterInitSubject = new BehaviorSubject(this.alertInfo);
       

        private test: any[];
        constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
            super(http, "TargetDefinition", appConfig);
            this.appConfig = appConfig;

        }
        getRawApi(): string {
            return this.appConfig.apiEndPoint;
        }

        private getFileServiceAPIURL(){
            let fileServiceAPI = "";
            const  serverDeployment = appConfigLocal.serverDeployment;
            if (serverDeployment === "0") {
                fileServiceAPI = "https://localhost:5051/FileService";
            } else if (serverDeployment === "1") {
                fileServiceAPI = "https://api.qurstore-qa-file.vsolgmi.com/FileService";
            }
            else {
                fileServiceAPI = this.getRawApi() + "/FileService";
            }
            return fileServiceAPI;
        }

        private getTargetDefinitionServiceAPIURL(){
            return  this.getApi();
        }
        private getReferenceDataServiceAPIURL(){
            let referenceDataAPI = "";
            const  serverDeployment = appConfigLocal.serverDeployment;
            if (serverDeployment === "0") {
                referenceDataAPI = "https://localhost:5501";
            } else if (serverDeployment === "1") {
                referenceDataAPI = "https://api.qurstore-qa-refdata.vsolgmi.com";
            }
            else {
                referenceDataAPI = this.getRawApi() ;
            }
            return referenceDataAPI;
        }

        public showAfterIntMessage(messageInfo) {
            this.afterInitSubject.next(messageInfo);
        }

        public  getAfterIntObs(): Observable<any> {
            return this.afterInitSubject.asObservable();
        }

        public getShowSuccessMessage() {
            return this.showSuccessMessage.asObservable();
        }

        public getUpdateTargetDefinitionListEvent() {
            return this.onUpdateListEvent;
        }

        private generateGetAPI(name, req): Observable<any> {
            return this.http
            .get(this.getTargetDefinitionServiceAPIURL() + name + req).pipe(
            map(res => {
                return res;
            }));
        }
        private generateDeleteAPI(name, data): Observable<any> {
            return this.http
            .delete(this.getTargetDefinitionServiceAPIURL() + name, data).pipe(
                map(res => {
                    return res;
                }));
        }
        public  getReferenceDataValue(referenceDataCode): Observable<any> {
            const endpoint = this.getReferenceDataServiceAPIURL() + "/ReferenceDataValue/list/" + referenceDataCode;
            return this.http
                .get(endpoint).pipe(
                    map(res => {
                        return res;
                    }));
        }
        public  getAcceptedValueReferenceData(referenceDataCode): Observable<any> {
            const endpoint = this.getReferenceDataServiceAPIURL() + "/ReferenceDataValue/listValues/" + referenceDataCode;
            return this.http
                .get(endpoint).pipe(
                    map(res => {
                        return res;
                    }));
        }
 
        public  getTargetDefinitionStep1(targetDefinitionId): Observable<any> {
            return  this.generateGetAPI("/getTargetDefinitionStep1/" + targetDefinitionId, "");
        }
        public  getAllTargetDefinitions(): Observable<any> {
         return  this.generateGetAPI("/list", "");
        }
        public  getColumnList(targetDefinitionId): Observable<any> {
            return  this.generateGetAPI("/column-list/" + targetDefinitionId, "");
        }

        private generatePostAPI(name, data): Observable<any> {
            return this.http
            .post(this.getTargetDefinitionServiceAPIURL() + name, data).pipe(
            map((res: Response) => res));
        }
        public  getFileInformation(uploadedFileId): Observable<any> {
            const endpoint = this.getFileServiceAPIURL() + "/getFileInformation/" + uploadedFileId;
            return this.http
                .get(endpoint).pipe(
                    map(res => {
                        return res;
                    }));
        }

        public saveFileContent(tdHeaderInformation: any, fileToUpload: Blob): Observable<any> {
            const formData: FormData = new FormData();
            const fileInformationJsonObject: string = JSON.stringify(tdHeaderInformation);
            const endpoint = this.getFileServiceAPIURL() + "/saveUploadedFile";
            formData.set('fileInformationJsonObject',  fileInformationJsonObject);
            formData.set('formFile', fileToUpload);
            return this.http
                .post(endpoint, formData).pipe(
                map((res: Response) => res));
        }

        public  saveTargetDefinition(targetDefinitionData): Observable<any> {
            return  this.generatePostAPI("/saveTargetDefinition", targetDefinitionData);
        }
        public  activateTargetDefinition(targetDefinitionId): Observable<any> {
            return  this.generatePostAPI("/activateTargetDefinition", targetDefinitionId);
        }
        public  saveTargetColumns(targetColumnData): Observable<any> {
            return  this.generatePostAPI("/saveTargetColumns", targetColumnData);
        }
        public  deleteTargetDefinition(targetDefinition): Observable<any> {
            return  this.generateDeleteAPI("/delete/" +  targetDefinition.id, targetDefinition );
        }
        public  canEditActivatedTargetDefinition(targetDefinitionId): Observable<any> {
            return  this.generateDeleteAPI("/canEditActivatedTargetDefinition/" + targetDefinitionId, "");
        }

}
