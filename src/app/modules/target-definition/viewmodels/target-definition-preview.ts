import { from as observableFrom, Observable, Subject, forkJoin } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import { NgxSmartModalService } from "ngx-smart-modal";
// import { AuthService } from "src/app/utils/auth.service";
import { TargetDefinitionViewModel } from "./target-definition.viewmodel";
import { TargetDefinitionService } from "../services/target-definition.service";
import { LocalStorageManager } from 'src/app/utils/local-storage-manager';
import { LocalStorageKeyCollection, GridConfig, ModalType } from 'src/app/utils/config';
import { TargetDefinitionNavigationService } from "../../core-ui-services/target-definition-navigation.service";
import { ValidationMessages } from "../helpers/validation-messages";


@Injectable()
export class TargetDefinitionPreviewViewModel extends TargetDefinitionViewModel {
  public userId: string;
  public targetDefinitionId: string;
  public targetDefinitionList: any;
  public alertService: AlertService;
  public viewModelDataGrid: DataGridViewModel;
  public targetColumnsList: any;
  source = "list" || "supplier" || "create";

  header = [
    { headerName: "Data Element Name", field: "columnName", isClickable: false },
    { headerName: "Type", field: "refColumnDataTypeName", isClickable: false },
    { headerName: "Required", field: "isMandatoryDisplay", isClickable: false },
    { headerName: "Max Length", field: "maxLength", isClickable: false },
    { headerName: "Format", field: "format", isClickable: false },
    { headerName: "Data Masking", field: "refDataMaskTypeName", isClickable: false },
    { headerName: "Accepted Values", field: "refDataLookupName", isClickable: false },
    { headerName: "Default Value", field: "defaultValue", isClickable: false }
  ];

  // chartOptions: any;
  public appliedFilters: any[];
  public targetDefinitionWithoutUserList: any;
  public targetDefinitionName: string;
  public progressValue: number = 66;

  public constructor(
    public targetDefinitionService: TargetDefinitionService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    private targetColumnsLocalStorageManager: LocalStorageManager,
    public targetDefinitionNavigationService: TargetDefinitionNavigationService,
    public route: ActivatedRoute
  ) {
    super(targetDefinitionService, ViewModelType.Edit, ngxSmartModalService, route);
  }

  public initGrid(data) {
    const targetDefinitions = data ? data : [];
    targetDefinitions.map(targetDefinition => {
      targetDefinition.isEnabledDisplay = targetDefinition.isEnabled === true ? "Yes" : "No";
      targetDefinition.isMandatoryDisplay = targetDefinition.isMandatory === true ? "Yes" : "No";
    });
    this.viewModelDataGrid.setRowsData(targetDefinitions, false);
  }
  public showReferenceValueDetails() {
    this.targetDefinitionService.getColumnList(this.targetDefinitionId).subscribe(
      data => {
        if (data) {
          this.targetColumnsList = data;
        }
      },
      error => {
        this.alertService.error(error);
        return;
      }
    );
  }
  public async navigateToList() {
    return this.router.navigate(["target-definition/list"]);
  }

  public saveAndActivateTargetDefinition() {
    const target = { id: this.targetDefinitionId };
    // const targetDetailEntity: any = {
    //   id: this.targetDefinitionId,

    this.targetDefinitionService.activateTargetDefinition(target).subscribe(tdData => {
      if (tdData?.id && tdData.isSuccess) {
        this.progressValue = 100;
        this.targetDefinitionService.showAfterIntMessage({ text: ValidationMessages.saveSuccessMessage, type: "success", toStable: true })
        this.redirectToList();
      }
    }, error => {
      this.alertService.error(error.errors.Description[0], false);
    });
  }


  public async getColumnListData() {
    this.targetDefinitionService.getColumnList(this.targetDefinitionId).subscribe(
      data => {
        if (data) {
          this.targetColumnsList = data;
          this.initGrid(this.targetColumnsList);
        }
      },
      error => {
        this.alertService.error(error);
        return;
      }
    );
  }
  public onPrevious() {
    this.redirectToDefineData();
  }

  public redirectToDefineData() {
    this.targetDefinitionNavigationService.navigateUsingProxy({
      url: "/target-definition/define-data",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { targetDefinitionId: this.targetDefinitionId, modelType: ModalType.CREATE }
    }).catch();
  }
  public saveDraftChanges() {
    this.ngxSmartModalService.getModal("targetDefinitionPreviewSaveDraft").open();
  }
  public saveDraft() {
    this.progressValue = 100;
    this.targetDefinitionService.showAfterIntMessage({ text: ValidationMessages.draftSuccessMessage, type: "success", toStable: true })
    this.router
      .navigate(["/target-definition/list"])
      .then()
      .catch();
  }

  public activateTargetDefinition() {
    this.ngxSmartModalService.getModal("targetDefinitionPreviewPublishConfirm").open();
  }
  private async loadFormData() {
    const data = this.targetColumnsLocalStorageManager.getLocalStorageItem(LocalStorageKeyCollection.TargetDefinitionStep1Prefix + this.targetDefinitionId);
    if (data?.targetDefinitionName) {
      this.targetDefinitionName = data.targetDefinitionName;

    }
  }

  public init(param?) {
    //  super.initUserClaims();
    //start
    // 
    this.targetDefinitionId = param.targetDefinitionId;
    this.source = param.source;
    // if (this.source === "list") {

    // }
    this.viewModelDataGrid = new DataGridViewModel();
    this.subscribers();
    this.viewModelDataGrid.setHeader(this.header, false);
    this.getColumnListData();
    this.loadFormData();
    return observableFrom([]);
  }

  private subscribers() {

    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.columnClicked:
          this.targetDefinitionId = e.eventData.rowDetails[0];
          this.redirectToList();
          break;
        default:
          if (e.eventData && e.eventData.rowDetails) {
            this.targetDefinitionId = e.eventData.rowDetails[0];
            this.redirectToList();
          }
          break;
      }
    }, error => { });
  }
  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  // // Getting all the targetDefinition details and redirecting to the targetDefinition details page
  public redirectToList() {
    this.router
      .navigate(["target-definition/list"])
      .then()
      .catch();
  }



  public subscribeToCreateSuccessMessage() {
    this.targetDefinitionService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      if (!data.isCreateAnother) {
        this.alertService.success(message, toStable);
      }
    });
  }
}
