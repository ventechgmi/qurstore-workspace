import {from as observableFrom,  Observable } from "rxjs";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import {
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ValidationMessages } from '../helpers/validation-messages';
import { AlertService } from "../../../utils/alert/alert.service";
// import { AuthService } from "src/app/utils/auth.service";
import { TargetDefinitionService } from "../services/target-definition.service";
import { TargetDefinitionViewModel } from "./target-definition.viewmodel";
import { DataGridExtendedComponent } from '../../core-ui-components';
import { DataGridExtendedViewModel } from '../../core-ui-components/data-grid-extended/view-models/data-grid-extended.viewmodel';
import { DataGridViewModel } from '../../core-ui-components/data-grid/view-models/data-grid.viewmodel';
import { GridConfig, LocalStorageKeyCollection, DataTypeCollection } from 'src/app/utils/config';
import { debounceTime } from 'rxjs/operators';
import { LocalStorageManager } from 'src/app/utils/local-storage-manager';
import { debug } from 'console';

@Injectable()
export class DefineDataDetailViewModel extends TargetDefinitionViewModel {
  defineDataId: any;
  isDeleteButtonEnable = true;
  public defineDataDetailsForm: FormGroup;

  submitted = false;
  modalType = "Edit" || "Create" || "Detail";
  isFormValueChange = false; // to disable save btn if form fields not changed
  isExist: boolean;
  textBasedOnModalType: string;
  file: any;
  viewModelDataGridExtendedComponent: DataGridExtendedViewModel;
  viewModelAcceptedValueGridComponent: DataGridViewModel;
  columnName: any;
  private targetDefinitionId;
  private tdColumnId;
  private acceptedValueReferenceDataRow: any[];
  public targetColumnsList: any;
  private dataTypeValueList: any[];
  private dataMaskingValueList: any[];
  private allReferenceDataAcceptedValue: any[];
  public targetDefinitionName: string;
  public progressValue: number = 33;
  public constructor(
    public targetDefinitionService: TargetDefinitionService,
    private router: Router,
    private fb: FormBuilder,
    private alertService: AlertService,
    public ngxSmartModalService: NgxSmartModalService,
    private chRef: ChangeDetectorRef,
    private targetColumnsLocalStorageManager: LocalStorageManager,
    // public authService: AuthService,
    public route: ActivatedRoute
  ) {
    super(targetDefinitionService, ViewModelType.ReadOnly, ngxSmartModalService, route);
    this.initGridViewModal();
    this.defineDataDetailsForm = this.fb.group({});

  }
  columnDefs = [];
  private referenceDataTypeCode =  "DTYPE";
  private referenceDataMaskingCode =  "DMTYP";
  private allReferenceDataAcceptedValues =  "ALL";
  header = [
    { headerName: "Accepted Value", field: "name", isClickable: false },
    { headerName: "Values", field: "referenceValueString", isClickable: false },
    { headerName: "Select", field: "isSelected", isRadioButton: true,  isClickable: true  }
  ];

  public init(params?) {
      //  super.initUserClaims();
        this.modalType = params.modalType;
        this.initializeReferenceData();
        this.targetDefinitionId = params.id;

        if (this.modalType === "Create") {
          this.textBasedOnModalType = "Create: Define Target Definition";
        } else {
            this.textBasedOnModalType = "Edit: Define Target Definition";
            if (params.id) {
              this.targetDefinitionId = params.id;
            } else {
              this.alertService.error(ValidationMessages.idNotFound, false);
            }
        }
        this.initializeGridColDef();
        this.getColumnListData();
        this.loadFormData();
        this.subscribers();
        return observableFrom([]);
  }

  private initGridViewModal(): void {
    this.viewModelDataGridExtendedComponent = new DataGridExtendedViewModel();
    this.viewModelAcceptedValueGridComponent = new DataGridViewModel();
    this.viewModelAcceptedValueGridComponent.setHeader(this.header, false, { canUpdate: false, canDelete: false});
  }

  private async loadFormData() {
    const data = this.targetColumnsLocalStorageManager.getLocalStorageItem(LocalStorageKeyCollection.TargetDefinitionStep1Prefix + this.targetDefinitionId);
    if(data) {
      this.targetDefinitionName = data.targetDefinitionName;
    }

  }
  private subscribers() {
    this.viewModelDataGridExtendedComponent.events.subscribe(e => {
        switch (e.eventType) {
          case GridConfig.checkBoxClicked:
            this.targetColumnsList.forEach(element => {
              if (e.eventData.rowDetails && element.id === e.eventData.rowDetails.id) {
                if (element.isMandatory) {
                  element.isMandatory = false;
                } else {
                  element.isMandatory = true;
                }
              }
            });
            this.defineDataDetailsForm.markAsDirty();
            this.viewModelDataGridExtendedComponent.setRowsData(this.targetColumnsList);
            break;
            default:
                break;
        }

    }, error => { });
  }

  public  initializeReferenceData() {
       this.getReferenceDataValue();
       this.getAcceptedValueReferenceData();
       this.getReferenceDataMasking();
  }
  public  getColumnListData() {
    let tempHolder = this.targetColumnsLocalStorageManager.getLocalStorageItem(LocalStorageKeyCollection.TargetDefinitionStep2Prefix + this.targetDefinitionId);
    tempHolder = null;
    if (tempHolder) {
        this.targetColumnsList = tempHolder;
        this.initGrid(tempHolder);
     } else {
    this.targetDefinitionService.getColumnList(this.targetDefinitionId).subscribe(
        data => {
          if (data) {
            this.targetColumnsList = data;
            this.initGrid(data);
            this.targetColumnsLocalStorageManager.setLocalStorageItem(LocalStorageKeyCollection.TargetDefinitionStep2Prefix + this.targetDefinitionId, data);
          }
        },
        error => {
          this.alertService.error(error);
          this.targetColumnsLocalStorageManager.removeLocalStorageItem(LocalStorageKeyCollection.TargetDefinitionStep2Prefix + this.targetDefinitionId);
          return;
        }
      );
      }
  }
  public   getAcceptedValueReferenceData() {
     const tempHolder = this.targetColumnsLocalStorageManager.getLocalStorageItem(LocalStorageKeyCollection.AcceptedValueReferenceData);
     if (tempHolder) {
        this.allReferenceDataAcceptedValue = JSON.parse(tempHolder);
     } else {
      this.targetDefinitionService.getAcceptedValueReferenceData(this.allReferenceDataAcceptedValues).subscribe(
        data => {
          if (data) {
            this.allReferenceDataAcceptedValue = JSON.parse(data);
            this.targetColumnsLocalStorageManager.setLocalStorageItem(LocalStorageKeyCollection.AcceptedValueReferenceData, data);
          }
        },
        error => {
          this.alertService.error(error);
          this.targetColumnsLocalStorageManager.removeLocalStorageItem(LocalStorageKeyCollection.AcceptedValueReferenceData);
        }
      );
    }
  }

  public async getReferenceDataValueold() {
    const tempHolder = this.targetColumnsLocalStorageManager.getLocalStorageItem(LocalStorageKeyCollection.ReferenceDataValue);
    if (tempHolder) {
       this.dataTypeValueList = tempHolder;
    } else {
        this.targetDefinitionService.getReferenceDataValue(this.referenceDataTypeCode).subscribe(
          data => {
            if (data) {
              this.dataTypeValueList = data;
              this.targetColumnsLocalStorageManager.setLocalStorageItem(LocalStorageKeyCollection.ReferenceDataValue, data);
            }
          },
          error => {
            this.alertService.error(error);
            this.targetColumnsLocalStorageManager.removeLocalStorageItem(LocalStorageKeyCollection.ReferenceDataValue);
          }
        );
     }
    }

    public async  getReferenceDataValue() {
      const tempHolder = this.targetColumnsLocalStorageManager.getLocalStorageItem(LocalStorageKeyCollection.ReferenceDataValue);
      if (tempHolder) {
        const parsedData = JSON.parse(tempHolder);
        let dict : any = [] ;
        parsedData.forEach(element => {
          dict[element.Id] = element.Name;
          });
         this.dataTypeValueList = dict;
      } else {
            this.targetDefinitionService.getReferenceDataValue(this.referenceDataTypeCode).subscribe(
            data => {
              if (data) {
                const parsedData = JSON.parse(data);
                  let dict : any = [] ;
                  parsedData.forEach(element => {
                  dict[element.Id] = element.Name;
                  });
                this.dataTypeValueList = dict;
                this.targetColumnsLocalStorageManager.setLocalStorageItem(LocalStorageKeyCollection.ReferenceDataValue, data);
                // this.initializeGridColDef();
              }
            },
            error => {
              this.alertService.error(error);
              this.targetColumnsLocalStorageManager.removeLocalStorageItem(LocalStorageKeyCollection.ReferenceDataValue);
            }
          );

   }
 }


  public async getReferenceDataMasking() {

    const tempHolder = this.targetColumnsLocalStorageManager.getLocalStorageItem(LocalStorageKeyCollection.ReferenceDataMasking);
    if (tempHolder) {
      const parsedData = JSON.parse(tempHolder);
       let dict : any = [] ;
       parsedData.forEach(element => {
        dict[element.Id] = element.Name;
        });
       this.dataMaskingValueList = dict;
    } else {
      this.targetDefinitionService.getReferenceDataValue(this.referenceDataMaskingCode).subscribe(
        data => {
          if (data) {
            const parsedData = JSON.parse(data);
            let dict : any = [] ;
            parsedData.forEach(element => {
              dict[element.Id] = element.Name;
              });
            this.dataMaskingValueList = dict;
            this.targetColumnsLocalStorageManager.setLocalStorageItem(LocalStorageKeyCollection.ReferenceDataMasking, data);
          }
        },
        error => {
          this.alertService.error(error);
          this.targetColumnsLocalStorageManager.removeLocalStorageItem(LocalStorageKeyCollection.ReferenceDataMasking);
        }
      );
      }
  }

  public initGrid(data) {
      this.targetColumnsList.map(targetColumns => {
        if (targetColumns.lastModifiedDate) {
          targetColumns.lastModifiedDateDisplay = new Date(targetColumns.lastModifiedDate).toLocaleDateString() + " " + new Date(targetColumns.lastModifiedDate).toLocaleTimeString();
          }
      });
      this.viewModelDataGridExtendedComponent.rowData = this.targetColumnsList;
  }

  private validateData(gridData: any) {
    const tempColumnNameArray = [];
    for (let i = 0; i < gridData.length; i++) {
      const element = gridData[i];
      if (!element.columnName || element.columnName === "") {
        this.alertService.error(ValidationMessages.ColumnNameMandatory, false);
        return false;
      } else {
        if (tempColumnNameArray.length && tempColumnNameArray.includes(element.columnName.toLowerCase())) {
          const errorMsg = ValidationMessages.ColumnNameUnique.replace("{DuplicateColumnName}", element.columnName);
          this.alertService.error(errorMsg, false);
          return false;
        } else {
          tempColumnNameArray.push(element.columnName.toLowerCase());
        }
      }
    }
    return true;
  }

  public saveChanges() {
    const gridData = this.viewModelDataGridExtendedComponent.rowData;
    if (this.validateData(gridData)) {
      const tempHolder = this.transformModuleToEntityModel(gridData);
      this.targetDefinitionService.saveTargetColumns(tempHolder).subscribe(
        data => {
          if (data && data.isSuccess) {
            // success
            // navagate to Preview Definition
            this.targetColumnsLocalStorageManager.removeLocalStorageItem(LocalStorageKeyCollection.TargetDefinitionStep2Prefix + this.targetDefinitionId);
            this.addTargetDefinitionStep2ToLocalStorage(gridData);
            this.redirectToPreview();
          }
        },
        error => {
          this.alertService.error(error);
          return;
        }
      );
    }
  }
  public redirectToPreview() {
    this.router
      .navigate(["target-definition/preview/" + this.targetDefinitionId + "/create"])
      .then()
      .catch();
  }
  private  transformModuleToEntityModel(gridData) {
    const targetColumns: any[] = [];
    _.each(gridData, (node) => {

      const { id, targetDefinitionId, recordType, columnName, refColumnDataTypeId, maxLength, isMandatory,
        format, refDataLookupId, lastModifiedBy, refDataMaskTypeId, defaultValue } = node;

      const targetColumnInfo: any = {
            id,
            targetDefinitionId,
            recordType,
            columnName,
            refColumnDataTypeId,
            maxLength,
            isMandatory,
            format,
            refDataLookupId,
            lastModifiedBy,
            refDataMaskTypeId,
            defaultValue
      };
      targetColumns.push(targetColumnInfo);
    });
    return targetColumns;
  }

  private async redirectToStep1() {
    this.router
      .navigate(["/target-definition/upload/" + this.targetDefinitionId])
      .then()
      .catch();
  }

  public onPrevious() {
    this.targetColumnsLocalStorageManager.removeLocalStorageItem(LocalStorageKeyCollection.TargetDefinitionStep2Prefix + this.targetDefinitionId);
    this.addTargetDefinitionStep2ToLocalStorage(this.viewModelDataGridExtendedComponent.rowData);
    if (this.modalType === "Edit" || this.modalType === "Create") {
        this.defineDataDetailsForm.dirty ? this.ngxSmartModalService.getModal("defineDataCancelConfirmModal").open() :      this.redirectToStep1().then().catch();
    } else {
      this.redirectToStep1().then().catch();
    }
  }
  public async navigateToUploadFile() {
    if (this.targetDefinitionId) {
          return this.router.navigate(["target-definition/upload/new"]); // TODO : open upload in edit mode
    } else {
      return this.router.navigate(["target-definition/new-target-definition"]); // TODO open in new mode
    }
  }
  private addTargetDefinitionStep2ToLocalStorage(data)  {
    this.targetColumnsLocalStorageManager.setLocalStorageItem(LocalStorageKeyCollection.TargetDefinitionStep2Prefix + this.targetDefinitionId, data);
  }

  private isHeaderInformationAvailable(){
    const getHeaderInformation =  this.targetColumnsLocalStorageManager.getLocalStorageItem(LocalStorageKeyCollection.TargetDefinitionStep1Prefix + this.targetDefinitionId);
    if (getHeaderInformation) {
      return getHeaderInformation.file_header_row_indicator === "1" ? true : false;
    } else {
      return false;
    }
  }
  public initializeGridColDef() {
    if (this.isHeaderInformationAvailable()) {
       this.columnName =  {
        headerName: 'Data Element Name',
        field: 'columnName',
        sortable: false,
        editable: false
      };
    } else {
        this.columnName =  {
          headerName: 'Data Element Name',
          field: 'columnName',
          sortable: false,
          editable: true
        };
    }

    this.viewModelDataGridExtendedComponent.columnDefs.push(this.columnName);

    const refColumnDataTypeId  =    {
      headerName: 'Type',
      field: 'refColumnDataTypeId',
      cellEditor: 'agSelectCellEditor',
      cellEditorParams: {
          values: this.extractValues(this.dataTypeValueList)
      },
      refData: this.dataTypeValueList,
    };
    this.viewModelDataGridExtendedComponent.columnDefs.push(refColumnDataTypeId);

    const refColumnDataTypeName  =    {
      headerName: 'Type Name',
      field: 'refColumnDataTypeName',
      hide: true
    };
    this.viewModelDataGridExtendedComponent.columnDefs.push(refColumnDataTypeName);
    const isMandatory  =   {
      headerName: 'Required',
      editable: false,
      field: 'isMandatory',
      cellRenderer: (params: any) => {
            if (params.value) {
                return `<input type="checkbox" checked data-action-type="checkBoxClicked">`;
            } else {
                return `<input type="checkbox" data-action-type="checkBoxClicked">`;
            }
     }
    };
    this.viewModelDataGridExtendedComponent.columnDefs.push(isMandatory);
    const maxLength  =    {
      headerName: 'Max Length',
      field: 'maxLength',
      sortable: false,
      valueParser: this.numberParser,
      editable: (params: any) => {
        return this.isEditableRow(params);
      },
    } ;
    this.viewModelDataGridExtendedComponent.columnDefs.push(maxLength);
    const format  =    {
      headerName: 'Format',
      field: 'format',
      editable: true,
      cellEditorSelector: params => {
        if (params.node.data.refColumnDataTypeId.toUpperCase()  === DataTypeCollection.Date) {
          return {
            component: 'agSelectCellEditor',
            params: { values: ['DD/MM/YYYY', 'MM/DD/YYYY'] }
          };
        }
        else {
          return null;
        }

    }
    };
    this.viewModelDataGridExtendedComponent.columnDefs.push(format);

    const refDataMaskTypeId  =    {
      headerName: 'Data Masking',
      field: 'refDataMaskTypeId',
      cellEditor: 'agSelectCellEditor',
      cellEditorParams: {
          values: this.extractValues(this.dataMaskingValueList)
      },
      refData: this.dataMaskingValueList
    };
    this.viewModelDataGridExtendedComponent.columnDefs.push(refDataMaskTypeId);


    const refDataLookupId  =     {
      headerName: 'Accepted Values',
      field: 'refDataLookupId',
      cellEditor: 'agSelectCellEditor',
      cellEditorParams: {
          values: this.extractValues(this.allReferenceDataAcceptedValue)
      },
      refData: this.allReferenceDataAcceptedValue
    };
    this.viewModelDataGridExtendedComponent.columnDefs.push(refDataLookupId);

    const defaultValue =  {
      headerName: 'Default Value',
      field: 'defaultValue',
      sortable: false,
      editable: true
    };

this.viewModelDataGridExtendedComponent.columnDefs.push(defaultValue);

    this.viewModelDataGridExtendedComponent.defaultSortModel = [
      {
        colId: 'name',
        sort: 'asc',
      }
    ];
    // this.viewModelDataGridExtendedComponent.moveColumnToSpecificIndex();
  }
  // https://www.ag-grid.com/javascript-grid-change-detection/

  public canHaveMaxLength(para) {
    return "false";
  }

  public isEditableRow(params) {
    const dataType = params.node.data.refColumnDataTypeId;
// https://www.ag-grid.com/javascript-grid-cell-editing/
    if (dataType.toUpperCase()  === DataTypeCollection.Varchar || dataType.toUpperCase() === DataTypeCollection.NVarchar || dataType.toUpperCase() === DataTypeCollection.Char || dataType.toUpperCase() === DataTypeCollection.Int) {
      return true;
    }
    return false;
  }

  public extractValues(mappings) {
    if(mappings) {
    return Object.keys(mappings);
    }
  }
  public numberParser(params) {
    const tempHolder =  Number(params.newValue);
    if (tempHolder) {
        return tempHolder <= 8192 ? tempHolder : "";
    } else {
      return "";
    }
  }
}
