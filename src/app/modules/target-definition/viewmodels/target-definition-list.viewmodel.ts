import { from as observableFrom, Observable, Subject, forkJoin } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import config, { GridConfig, ModalType } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { TargetDefinitionViewModel } from "./target-definition.viewmodel";
import { TargetDefinitionService } from "../services/target-definition.service";
import { TargetDefinitionNavigationService } from "../../core-ui-services/target-definition-navigation.service";
import moment from 'moment';


@Injectable()
export class TargetDefinitionListViewModel extends TargetDefinitionViewModel {
  public userId: string;
  public targetDefinitionId: string;
  public targetDefinitionList: any;
  public alertService: AlertService;
  viewModelDataGrid: DataGridViewModel;

  header = [
    { headerName: "Target Name", field: "name", isClickable: true },
    { headerName: "Version #", field: "sequenceNumber", isClickable: false },
    { headerName: "File Name", field: "uploadedFileName", isClickable: false },
    { headerName: "Status", field: "targetStatus", isClickable: false },
    { headerName: "Last Modified On", field: "lastModifiedDateDisplay", isClickable: false }
  ];
  appliedFilters: any[];
  targetDefinitionWithoutUserList: any;
  public constructor(
    public targetDefinitionService: TargetDefinitionService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public targetDefinitionNavigationService: TargetDefinitionNavigationService
  ) {
    super(targetDefinitionService, ViewModelType.Edit, ngxSmartModalService, route);
  }

  public init(param?) {
    this.viewModelDataGrid = new DataGridViewModel();
    this.subscribeToCreateSuccessMessage();
    this.subscribers();

    this.viewModelDataGrid.setPagination(true);
    this.viewModelDataGrid.setHeader(this.header, false, { canUpdate: true, canDelete: true, canDownloadTargetDefinition: false });
    this.viewModelDataGrid.clickableColumnIndices = [1];

    this.setDataTableConfigurations();
    this.getTargetDefinitionsList();
    return observableFrom([]);
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  private subscribers() {
    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.columnClicked:
          this.targetDefinitionId = e.eventData.rowDetails.id;
          this.redirectToPreview();
          break;
        case GridConfig.editClicked:
          this.targetDefinitionId = e.eventData.rowDetails.id;
          if (e.eventData.rowDetails[8] === "Completed") {
            this.ngxSmartModalService.getModal("editActivatedFromList").open();
          } else {
            this.redirectToDefineData(ModalType.EDIT);
          }
          break;
        case GridConfig.previewTargetDefinitionClicked:
          this.targetDefinitionId = e.eventData.rowDetails.id;
          this.redirectToPreview();
          break;
        case GridConfig.deleteClicked:
          this.targetDefinitionId = e.eventData.rowDetails.id;
          this.ngxSmartModalService.getModal("deleteConfirmModalFromList").open();
          break;
        default:
          if (e.eventData && e.eventData.rowDetails) {
            this.targetDefinitionId = e.eventData.rowDetails.id;
            this.redirectToDetail();
          }
          break;
      }

    }, error => { });

  }

  public redirectToPreview() {
    this.router
      .navigate(["target-definition/preview/" + this.targetDefinitionId + "/list"])
      .then()
      .catch();
  }

  // Getting all the targetDefinition details and redirecting to the targetDefinition details page
  private redirectToDetail() {
    this.router
      .navigate(["/target-definition/targetDefinition-detail/" + this.targetDefinitionId])
      .then()
      .catch();
  }

  public checkAndRedirectToDefineData() {
    this.ngxSmartModalService.getModal("editActivatedFromList").close();
    if (this.canEditActivateRecord()) {
      this.targetDefinitionNavigationService.navigateUsingProxy({
        url: "/target-definition/define-data",
        saveLink: true,
        openInNewWindow: false,
        isQueryParams: false,
        parameters: { targetDefinitionId: this.targetDefinitionId, modelType: ModalType.EDIT }
      }).catch();
    } else {
      this.alertService.error("Activated Target Definition cannot be edited.", true);
    }
  }

  public redirectToDefineData(modelType) {
    this.ngxSmartModalService.getModal("editActivatedFromList").close();
    this.targetDefinitionNavigationService.navigateUsingProxy({
      url: "/target-definition/define-data",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { targetDefinitionId: this.targetDefinitionId, modelType }
    }).catch();
  }

  private canEditActivateRecord() {
    this.targetDefinitionService.canEditActivatedTargetDefinition(this.targetDefinitionId).subscribe(
      data => {
        if (data) {
          return data.canEditResult;
        }
      },
      error => {
        return false;
      }
    );
    return false;
  }

  public redirectToCreate() {
    this.router
      .navigate(["target-definition/upload/new"])
      .then()
      .catch();
  }

  public async deleteTargetDefinition() {
    const request = { id: this.targetDefinitionId };
    await this.targetDefinitionService.deleteTargetDefinition(request).subscribe(data => {
      if (data.isSuccess) {
        this.alertService.success(data.message, true);
        this.getTargetDefinitionsList();
      } else {
        this.alertService.error(data.message, true);
      }
    }, (requestFailed) => {
      (requestFailed.error && requestFailed.error.validationErrors) ?
        this.alertService.error(requestFailed.error.validationErrors.message, true) :
        this.alertService.error(requestFailed.error.message, true);
      this.ngxSmartModalService.getModal("deleteConfirmModalFromList").close();
    }, () => {
      this.ngxSmartModalService.getModal("deleteConfirmModalFromList").close();
    });
  }

  // data-table configurations for project
  public setDataTableConfigurations() {
    this.viewModelDataGrid.dataTableConfiguration = {
      destroy: true,
      columnDefs: [{ targets: [0], visible: false, searchable: false }, { type: "num-html", targets: [2] }, { orderable: false, targets: 2, searchable: false }],
      order: [[1, "asc"]]
    };
  }

  public getTargetDefinitionsList() {
    this.targetDefinitionService.getAllTargetDefinitions().subscribe(
      data => {
        if (data) {
          this.targetDefinitionList = data;
          this.initGrid(this.targetDefinitionList);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public initGrid(data) {
    const showEditIcon = new Map();
    const targetDefinitions = data ? data : [];
    targetDefinitions.map(targetDefinition => {
      if (targetDefinition.lastModifiedDate) {
        targetDefinition.lastModifiedDateDisplay = moment(targetDefinition.lastModifiedDate).format(config.APPLICATION_DATE_TIME_FORMAT);
      }
      if (targetDefinition.isEnabled) {
        targetDefinition.isEnabledDisplay = targetDefinition.isEnabled === undefined ? "No" : "Yes";
      }
      if (targetDefinition.status) {
        targetDefinition.targetStatus = (targetDefinition.status === 'Completed') ? "Active" : targetDefinition.status;
      }
      showEditIcon.set(targetDefinition.id, targetDefinition.status !== 'Completed');
    });
    this.viewModelDataGrid.setRowsData(targetDefinitions, false, false, showEditIcon);
  }

  public subscribeToCreateSuccessMessage() {
    this.targetDefinitionService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      if (!data.isCreateAnother) {
        this.alertService.success(message, toStable);
      }
    });
    
  }

}
