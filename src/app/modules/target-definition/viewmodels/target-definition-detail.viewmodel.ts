
import { from as observableFrom, Observable } from "rxjs";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import {
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";

import { NgxSmartModalService } from "ngx-smart-modal";
import { ValidationMessages } from '../helpers/validation-messages';

import { AlertService } from "../../../utils/alert/alert.service";
// import { AuthService } from "src/app/utils/auth.service";

import { TargetDefinitionService } from "../services/target-definition.service";
import { TargetDefinitionViewModel } from "./target-definition.viewmodel";
import { LocalStorageManager } from 'src/app/utils/local-storage-manager';
import { LocalStorageKeyCollection, ModalType, TARGET_FILE_MAX_FILE_SIZE } from 'src/app/utils/config';
import { AuthService } from 'src/app/utils/auth.service';
import { TargetDefinitionNavigationService } from "../../core-ui-services/target-definition-navigation.service";
// const appConfig = require("../../../app-config.json");
@Injectable()
export class TargetDefinitionDetailViewModel extends TargetDefinitionViewModel {

  private targetDefinitionInProgressGuid = "EF40287A-F299-4B9B-81B2-739994825C90";
  private targetColumnsVarcharDataTypeGuid = "259EA267-F017-4176-9904-4DB06D475794";
  private targetColumnsDataMaskingNoneValueGuid = "C7FAA8C7-7C8A-4800-8A93-FEE03C034097";
  private targetDefinitionS3Folder = "TargetDefinitionS3Folder";
  private targetDefinitionS3bucketNameKey = "AWSStagingLandingZoneBucketName";
  public targetDefinitionName: string;
  public targetDefinitionId: any;
  public fileUploadId: any;
  public fileName: string;
  public isDeleteButtonEnable = true;
  public targetDefinitionDetailsForm: FormGroup;
  private targetDetailEntity: any;
  public submitted = false;
  public modalType = "Edit" || "Create" || "Detail";
  isFormValueChange = false; // to disable save btn if form fields not changed
  isExist: boolean;
  textBasedOnModalType: string;
  file: any;
  public progressValue: number = 0;

  public constructor(
    public targetDefinitionService: TargetDefinitionService,
    private router: Router,
    private fb: FormBuilder,
    private alertService: AlertService,
    public ngxSmartModalService: NgxSmartModalService,
    private chRef: ChangeDetectorRef,
    private targetColumnsLocalStorageManager: LocalStorageManager,
    public authService: AuthService,
    public route: ActivatedRoute,
    public targetDefinitionNavigationService: TargetDefinitionNavigationService
  ) {
    super(targetDefinitionService, ViewModelType.ReadOnly, ngxSmartModalService, route);

  }
  public init(params?) {
    //  super.initUserClaims();.
    this.modalType = params.modalType;
    this.targetDefinitionDetailsForm = this.fb.group({
      file_type: "",
      fileUpload: [],
      isHeaderInformationAvailable: ["", [Validators.required, , ,]],
      delimiterInformation: ["", [Validators.required, , ,]],
      targetDefinitionName: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(50), super.validateSpecialCharactersAndWhiteSpace(3)]]
    });
    if (this.modalType === "Create") {
      this.textBasedOnModalType = "Create Target Definition";
      this.removeTargetDefinitionFromLocalStorage();
    } else {
      this.textBasedOnModalType = "Edit Target Definition";
      if (params.id) {
        this.targetDefinitionId = params.id; // this.targetDefinitionDetail.id;
        this.loadFormData();
      } else {
        this.alertService.error(ValidationMessages.idNotFound, false);
      }
    }

    return observableFrom([]);
  }
  private async loadFormData() {
    this.getFormLoadDataAPI()
      .then((data: any) => {
        if (data) {
          this.targetDefinitionName = data.targetDefinitionName;
          this.targetDefinitionId = data.id;
          this.fileUploadId = data.fileUploadId;
          this.fileName = data.file_name;
          this.targetDefinitionDetailsForm.patchValue({
            file_type: data.file_type,
            isHeaderInformationAvailable: data.file_header_row_indicator,
            delimiterInformation: data.delimiter,
            targetDefinitionName: data.targetDefinitionName
          });
          this.targetDefinitionDetailsForm.markAsDirty();
        }
      });
  }
  private async getFormLoadDataAPI() {
    let tempHolder = this.targetColumnsLocalStorageManager.getLocalStorageItem(LocalStorageKeyCollection.TargetDefinitionStep1Prefix + this.targetDefinitionId);
    if (!tempHolder) {
      return new Promise((resolve, reject) => {
        this.targetDefinitionService.getTargetDefinitionStep1(this.targetDefinitionId).subscribe(
          data => {
            if (data) {
              this.fileUploadId = data.uploadedFileId;
              this.getFileInfo(data, resolve);
            } else {
              resolve(null);
            }
          },
          error => {
            this.alertService.error(error);
            reject(null);
          }
        );
      })
    } else {
      return tempHolder;
    }
  }

  private getFileInfo(data, resolve) {
    this.targetDefinitionService.getFileInformation(this.fileUploadId).subscribe(
      fileData => {
        if (fileData) {
          const loadConfigData = JSON.parse(fileData.configData);
          this.targetDetailEntity = {
            id: this.targetDefinitionId,
            file_name: fileData.name,
            fileUploadId: data.uploadedFileId,
            targetDefinitionName: data.name,
            file_type: loadConfigData.file_type,
            file_header_row_indicator: loadConfigData.file_header_row_indicator,
            delimiter: loadConfigData.delimiter,
            lastModifiedBy: data.lastModifiedBy,
            targetDefinitionInProgressGuid: this.targetDefinitionInProgressGuid,
            targetColumnsVarcharDataTypeGuid: this.targetColumnsVarcharDataTypeGuid,
            targetColumnsDataMaskingNoneValueGuid: this.targetColumnsDataMaskingNoneValueGuid
          };
          if (this.targetDetailEntity) {
            this.targetColumnsLocalStorageManager.setLocalStorageItem(LocalStorageKeyCollection.TargetDefinitionStep1Prefix + this.targetDefinitionId, this.targetDetailEntity);
            resolve(this.targetDetailEntity);
          }
        } else {
          resolve(null);
        }
      }
    );
  }

  public transformToEntityModel() {
    const { file_type, fileUpload, isHeaderInformationAvailable, delimiterInformation, targetDefinitionName } = this.targetDefinitionDetailsFormControls;

    this.targetDetailEntity = {
      id: this.targetDefinitionId,
      fileUploadId: this.fileUploadId,
      file_type: file_type.value,
      file_header_row_indicator: isHeaderInformationAvailable.value,
      delimiter: delimiterInformation.value,
      targetDefinitionName: targetDefinitionName.value,
      lastModifiedBy: this.authService.getUserId(),
      targetDefinitionInProgressGuid: this.targetDefinitionInProgressGuid,
      targetColumnsVarcharDataTypeGuid: this.targetColumnsVarcharDataTypeGuid,
      targetColumnsDataMaskingNoneValueGuid: this.targetColumnsDataMaskingNoneValueGuid,
      uploadedFileDataType: "TGT"
    };
    if (this.file) {
      this.targetDetailEntity.file_name = this.file.name;
    }
    return this.targetDetailEntity;
  }

  private removeTargetDefinitionFromLocalStorage() {
    this.targetColumnsLocalStorageManager.cleanUp();
  }
  private addTargetDefinitionToLocalStorage(data) {
    this.targetColumnsLocalStorageManager.setLocalStorageItem(LocalStorageKeyCollection.TargetDefinitionStep1Prefix + this.targetDefinitionId, data);
  }
  private isValidInput() {
    let isValid = true;
    if (!!!this.targetDefinitionId && !!!this.file) {
      isValid = false;
      this.alertService.error(ValidationMessages.FileNotUploaded, false);
    }

    return isValid;
  }
  public async saveChanges() {
    this.submitted = true;
    if (this.targetDefinitionDetailsForm.invalid) { return; }
    if (!this.isValidInput()) {
      return;
    }
    this.transformToEntityModel();
    if (this.modalType === "Create") {
      this.targetDetailEntity.fileChanges = true;
      this.uploadFileToActivity();
    } else if (this.modalType === "Edit" && this.file) {
      this.ngxSmartModalService.getModal("uploadNewFile").open();
    }
    else if (this.modalType === "Edit") {
      this.targetDetailEntity.fileChanges = false;
      this.saveTargetDefinition(this.targetDetailEntity);
    }
    else {
      this.redirectToDefineData();
    }
  }
  public reloadPage() {
    window.location.reload();
  }
  public uploadNewFileConfirm() {
    this.ngxSmartModalService.getModal("uploadNewFile").close();
    this.targetDetailEntity.fileChanges = true;
    this.uploadFileToActivity();
  }
  private saveTargetDefinition(data: any) {
    this.targetDefinitionService.saveTargetDefinition(this.targetDetailEntity).subscribe(tdData => {
      if (tdData?.id && tdData.isSuccess) {
        this.targetDefinitionId = tdData.id;
        this.targetDetailEntity.targetDefinitionId = tdData.id;
        // this.removeTargetDefinitionFromLocalStorage();
        this.targetColumnsLocalStorageManager.removeLocalStorageItem(LocalStorageKeyCollection.TargetDefinitionStep1Prefix + this.targetDefinitionId);
        if (this.file) {
          this.targetColumnsLocalStorageManager.removeLocalStorageItem(LocalStorageKeyCollection.TargetDefinitionStep2Prefix + this.targetDefinitionId);
        }
        this.addTargetDefinitionToLocalStorage(this.targetDetailEntity);
      }
      this.redirectToDefineData();
    }, error => {
      this.alertService.error(error.errors.Description[0], false);
    });
  }
  public uploadFileToActivity() {
    this.targetDetailEntity.isTargetDefinitionFileUpload = true;
    this.targetDetailEntity.folderPathKey = this.targetDefinitionS3Folder;
    this.targetDetailEntity.bucketNameKey = this.targetDefinitionS3bucketNameKey;
    this.targetDefinitionService.saveFileContent(this.targetDetailEntity, this.file).subscribe(data => {
      if (data?.id && data.isSuccess) {
        this.targetDetailEntity.uploadId = data.id;
        this.targetDetailEntity.fileUploadId = data.id;
        this.targetDetailEntity.columnList = data.columnList;
        this.fileUploadId = data.id;
        this.saveTargetDefinition(this.targetDetailEntity);
      } else {
        this.alertService.error(ValidationMessages.ErrorOccurred, false);
      }
    }, error => {
      if (error?.error?.errorList && error.error.errorList.length > 0) {
        this.alertService.error(error.error.errorList[0], false);
      } else if (error.error) {
        this.alertService.error(error.error, false);
      }
      else {
        this.alertService.error(ValidationMessages.ErrorOccurred, false);
      }
    });
  }

  public redirectToDefineData() {
    this.targetDefinitionNavigationService.navigateUsingProxy({
      url: "/target-definition/define-data",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { targetDefinitionId: this.targetDefinitionId, modelType: ModalType.EDIT }
    }).catch();
  }


  public fileChanged(e) {
    const fileSize = Math.round((e.target.files[0].size / 1024));
    if (fileSize > TARGET_FILE_MAX_FILE_SIZE) {
      this.alertService.error(ValidationMessages.FileSizeError, false);
      (<HTMLInputElement>window.document.getElementById('fileUpload')).value = "";
      return;
    }
    this.file = e.target.files[0];
    this.changefile_typeDropdown();
  }

  private updateFormFields(targetDefinitionDetail) {
    this.targetDefinitionDetailsForm.patchValue({
      //  name: targetDefinitionDetail.name
    });
    // this.existingTargetDefinitionModules = targetDefinitionDetail.targetDefinitionModules;
  }

  get targetDefinitionDetailsFormControls() {
    return this.targetDefinitionDetailsForm.controls;
  }

  public get detailForm() {
    return this.targetDefinitionDetailsForm.controls;
  }

  public onCancel() {
    if (this.modalType === "Edit" || this.modalType === "Create") {
      this.targetDefinitionDetailsForm.dirty ? this.ngxSmartModalService.getModal("targetDefinitionCancelConfirmModal").open() : this.navigateToList().then().catch();
    } else {
      this.navigateToList().then().catch();
    }
  }

  public async navigateToList() {
    return this.router.navigate(["target-definition/list"]);
  }
  // public async backToListScreen(){
  //   return this.router.navigate(["target-definition/list"]);
  // }
  private changefile_typeDropdown() {
    const fileNameArray = this.file.name.split(".");
    const extensionName = fileNameArray[fileNameArray.length - 1];
    if (extensionName.toUpperCase() === "CSV") {
      this.targetDefinitionDetailsForm.patchValue({
        file_type: "CSV",
        delimiterInformation: ","
      });
      this.targetDefinitionDetailsForm.get("file_type").disable();
    } else if (extensionName.toUpperCase() === "XLSX") {
      this.targetDefinitionDetailsForm.patchValue({
        file_type: "XLSX",
        delimiterInformation: "none"
      });
      this.targetDefinitionDetailsForm.get("file_type").disable();
      this.targetDefinitionDetailsForm.get("delimiterInformation").disable();

    }
    else if (extensionName.toUpperCase() === "TXT") {
      this.targetDefinitionDetailsForm.patchValue({
        file_type: "TEXT"
      });
    }
    else {
      this.alertService.error(ValidationMessages.FileNotUploaded, false);
      this.targetDefinitionDetailsForm.patchValue({
        targetDefinitionName: "",
        file_type: "SELECT",
        isHeaderInformationAvailable: "",
        delimiterInformation: ""
      });
    }
  }


  public onConfirm() {
  }


}
