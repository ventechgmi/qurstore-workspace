
import {map} from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
const appConfigLocal = require("../../../app-config.json");

@Injectable({
    providedIn: "root"
})
export class TagTypeService extends EntityService {
        appConfig: AppConfig;
        public alertInfo = { message: "", type: "" };
        private afterInitSubject = new BehaviorSubject(this.alertInfo);

        constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
            super(http, "tag-type", appConfig);
            this.appConfig = appConfig;
        }

        private getAPIURL() {
            return this.getApi();
        }

        private generateGetAPI(name, req): Observable<any> {
            const requestURL = this.getAPIURL() + name + req;
            return this.http
            .get(requestURL).pipe(
            map(res => {
                return res;
            }));
        }
        
        private generatePostAPI(name, data): Observable<any> {
            return this.http
            .post(this.getAPIURL() + name, data).pipe(
            map((res: Response) => res));
        }

        private generateDeleteAPI(name, data): Observable<any> {
            return this.http
            .delete(this.getAPIURL() + name, data).pipe(
                map(res => {
                    return res;
                }));
        }
        
        public  getWorkspaceTagTypeDetails(data) {
            return this.generateGetAPI("/tag-types-for-context/", data);
        }

        public  getWorkspaceTagTypeDetailById(data) {
            return this.generateGetAPI("/tag-types-for-context-by-id/", data);
        }

}
