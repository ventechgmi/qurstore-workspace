
import {map} from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
const appConfigLocal = require("../../../app-config.json");

const mockUserWorkspaceData = require("../services/mock-user-workspace-data-model.json");
const mockAdminWorkspaceData = require("../services/mock-admin-user-workspace-data-model.json");

@Injectable({
    providedIn: "root"
})
export class WorkspaceService extends EntityService {
        appConfig: AppConfig;
        onUpdateListEvent = new EventEmitter();
        public vendorIdSubject = new ReplaySubject<any>();
        public showSuccessMessage = new Subject<any>();
        public alertInfo = { message: "", type: "" };
        private afterInitSubject = new BehaviorSubject(this.alertInfo);

        constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
            super(http, "workspace", appConfig);
            this.appConfig = appConfig;
        }

        private getAPIURL(){

            return this.getApi();
        }
        private generateGetAPI(name, req): Observable<any> {
            const requestURL = this.getAPIURL() + name + req;
            return this.http
            .get(requestURL).pipe(
            map(res => {
                return res;
            }));
        }

        private generatePostAPI(name, data): Observable<any> {
            return this.http
            .post(this.getAPIURL() + name, data).pipe(
            map((res: Response) => res));
        }

        private generateDeleteAPI(name, data): Observable<any> {
            return this.http
            .delete(this.getAPIURL() + name, data).pipe(
                map(res => {
                    return res;
                }));
        }
        // public  getAssignedWorkspace(): Observable<any> {
        //    return  this.generateGetAPI("/list", "");
        // }
        public  getWorkspaceCatalogList()    : Observable<any> {
            return this.generateGetAPI("/catalog/list", "");
        }

        public  getAssignedWorkspace() {
            return this.generateGetAPI("/loggedInUser", "");
        }

        public  getAllWorkspace() {
            return this.generateGetAPI("/list", "");
        }

        // public  terminateWorkspaceRequest1(data): Observable<any> {
        //     return  this.generatePostAPI("/Terminated", data);
        // }
        public  saveWorkspaceRequest(data): Observable<any> {
            return  this.generatePostAPI("/create", data);
        }
        public  approveWorkspaceRequest(data): Observable<any> {
            return  this.generatePostAPI("/Approved", data);
        }
        public  terminateWorkspaceRequest(data): Observable<any> {
            return  this.generatePostAPI("/terminated",  data );
        }
        public  cancelWorkspaceRequest(data): Observable<any> {
            return  this.generatePostAPI("/cancel",  data );
        }
        public  rejectWorkspaceRequest(data): Observable<any> {
            return  this.generatePostAPI("/rejected/", data );
        }


        public  getAllWorkspaceMock() {
            return  mockAdminWorkspaceData.adminWorkspaceData;
        }

        public  deleteVendor(vendorDetails): Observable<any> {
            return  this.generateDeleteAPI("/delete/" +  vendorDetails.id, vendorDetails );
        }
        // public  getVendorModules(): Observable<any> {
        //     return  this.generateGetAPI("/module-list", "");
        // }
        // public  getVendorById(vendorId): Observable<any> {
        //     return  this.generateGetAPI("/getVendorById/" + vendorId, "");
        //    }
        // public  saveVendor(vendorDetails): Observable<any> {
        //     return  this.generatePostAPI("/save", vendorDetails);
        // }
        // public getVendorByName(req): Observable<any> {
        //     return  this.generateGetAPI("/vendor-detail/by-name/", req);
        // }

        public showAfterIntMessage(messageInfo) {
            this.afterInitSubject.next(messageInfo);
        }

        public  getAfterIntObs(): Observable<any> {
            return this.afterInitSubject.asObservable();
        }

        public getVendorIdSubject() {
            return this.vendorIdSubject.asObservable();
        }

        public getShowSuccessMessage() {
            return this.showSuccessMessage.asObservable();
        }

        public getUpdateVendorListEvent() {
            return this.onUpdateListEvent;
        }

       public getAWSWorkspaceCatalogList() {
        return this.generateGetAPI("/region","");    
        }
}
