import {
    Component,
    OnInit,
    AfterContentInit
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from 'src/app/utils/alert/alert.service';
import { WorkspaceService } from '../../services/workspace.service';
import { WorkspaceRequestViewModel } from '../../viewmodels/workspace-request.viewmodel';

export interface SoftwareRequest {
    name: string;
    value: string;
    selected: boolean;
}

@Component({
    selector: "app-workspace-request-view",
    templateUrl: "./workspace-request-view.component.html",
    styleUrls: ["./workspace-request-view.component.scss"],
    providers: [ WorkspaceRequestViewModel, AlertService] // need separate alert instance
})

export class WorkspaceRequestViewComponent implements OnInit, AfterContentInit {
    public softwareRequests : SoftwareRequest []= [];
    constructor(private router: Router,
                private route: ActivatedRoute,
                private workspaceService: WorkspaceService,
                public workspaceRequestViewModel: WorkspaceRequestViewModel,
                public alertService: AlertService,
                public ngxSmartModalService: NgxSmartModalService
    ) {
        alertService.afterInitObs = workspaceService.getAfterIntObs();
    }

    public  ngOnInit() {
        this.route.params
            .subscribe(params => {
               this.workspaceRequestViewModel.initServices(this.alertService);
               this.workspaceRequestViewModel.init();
            });

        this.route.queryParams.subscribe(res => {
          //  this.projectId = res.id;
        });
        this.fillItems();
    }
    private fillItems(){

        this.softwareRequests = [
            { value: "1", name: "R Studio", selected: true},
            { value: "2", name: "Jupyter", selected: false}

        ];
    }
    public showOptions(e) {
        const temp = e;
    }
    public ngAfterContentInit() {
    }

}
