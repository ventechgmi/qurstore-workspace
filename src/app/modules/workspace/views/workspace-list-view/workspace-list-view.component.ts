import {
    Component,
    OnInit,
    AfterContentInit
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ViewEncapsulation } from '@angular/core';
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from 'src/app/utils/alert/alert.service';
import { WorkspaceService } from '../../services/workspace.service';
import { WorkspaceListViewModel } from '../../viewmodels/workspace-list.viewmodel';

export interface SoftwareRequest {
    name: string;
    value: string;
    selected: boolean;
}
@Component({
    selector: "app-workspace-list-view",
    templateUrl: "./workspace-list-view.component.html",
    styleUrls: ["./workspace-list-view.component.scss"],
    encapsulation: ViewEncapsulation.None,
    providers: [ WorkspaceListViewModel, AlertService] // need separate alert instance
})
export class WorkspaceListViewComponent implements OnInit, AfterContentInit {
    public softwareRequests : SoftwareRequest []= [];




    constructor(private router: Router,
                private route: ActivatedRoute,
                private workspaceService: WorkspaceService,
                public workspaceListViewModel: WorkspaceListViewModel,
                public alertService: AlertService,
                public ngxSmartModalService: NgxSmartModalService
    ) {
        alertService.afterInitObs = workspaceService.getAfterIntObs();
    }

    public  ngOnInit() {
        this.route.params
            .subscribe(params => {
               this.workspaceListViewModel.initServices(this.alertService);
               this.workspaceListViewModel.init();
            });

        this.route.queryParams.subscribe(res => {
          //  this.projectId = res.id;
        });
        // this.fillItems();
    }
    public ngAfterContentInit() {
    }
    // private fillItems(){

    //     this.softwareRequests = [
    //         { value: "1", name: "R Studio", selected: true},
    //         { value: "2", name: "Jupyter", selected: false}

    //     ];
    // }
    public showOptions(e) {
        const temp = e;
      //  debugger;
    }
}
