import {
    Component,
    OnInit,
    AfterContentInit
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from 'src/app/utils/alert/alert.service';
import { WorkspaceService } from '../../services/workspace.service';
import { WorkspaceAdminListViewModel } from '../../viewmodels/workspace-admin-list.viewmodel';
@Component({
    selector: "app-workspace-admin-list-view",
    templateUrl: "./workspace-admin-list-view.component.html",
    styleUrls: ["./workspace-admin-list-view.component.scss"],
    providers: [ WorkspaceAdminListViewModel, AlertService]
})
export class WorkspaceAdminListViewComponent implements OnInit, AfterContentInit {

    constructor(private router: Router,
                private route: ActivatedRoute,
                private workspaceService: WorkspaceService,
                public workspaceAdminListViewModel: WorkspaceAdminListViewModel,
                public alertService: AlertService,
                public ngxSmartModalService: NgxSmartModalService
    ) {
        alertService.afterInitObs = workspaceService.getAfterIntObs();
    }

    public  ngOnInit() {
        this.route.params
            .subscribe(params => {
               this.workspaceAdminListViewModel.initServices(this.alertService);
               this.workspaceAdminListViewModel.init();
            });

        this.route.queryParams.subscribe(res => {
          //  this.projectId = res.id;
        });
    }
    public ngAfterContentInit() {
    }

}
