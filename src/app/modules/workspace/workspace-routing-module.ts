import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { WorkspaceListViewComponent } from "./views/workspace-list-view/workspace-list-view.component";
import { WorkspaceAdminListViewComponent } from "./views/workspace-admin-list-view/workspace-admin-list-view.component";
import { WorkspaceRequestViewComponent } from "./views/workspace-request-view/workspace-request-view.component";

export const routes: Routes = [
  {
    path: "workspace/list",
    component: WorkspaceListViewComponent
  },
  {
    path: "workspace/admin-list",
    component: WorkspaceAdminListViewComponent
  },
  {
    path: "workspace/request",
    component: WorkspaceRequestViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkspaceRoutingModule { }
