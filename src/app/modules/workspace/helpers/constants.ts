export class WorkspaceRequestStatus {
    public static readonly APPROVED_STATUS = "Approved";
    public static readonly PENDING_FOR_APPROVAL_STATUS = "Pending for approval";
    public static readonly TERMINATED_STATUS = "Terminated";
    public static readonly REJECTED_STATUS = "Rejected";
}

export class WorkspaceStatusProperties {
    public static readonly IS_OPEN = "isOpen";
    public static readonly IS_COMPLETED = "isComlpeted";
    public static readonly IS_CLOSED = "isClosed";
}
