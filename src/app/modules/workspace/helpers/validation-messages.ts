export class ValidationMessages {
    public static readonly idNotFound = "Error occurred: please navigate back to List screen. ";
    public static readonly NoModuleSelected = "Select at least one module to continue.";

        public static readonly RequiredMessage = " is required.";
        public static readonly MinMaxLengthMessage = " must be between 3 and 20 characters..";
        public static readonly InvalidMessage = " is invalid.";
        public static readonly EmailMatchMessage = "Email and ConfirmEmail must be matched.";

        public static readonly vendorAlreadyExists = "Vendor name is already in use. Please try with another one.";
        public static readonly saveSuccessMessage = "User saved successfully.";
        public static readonly createSuccessMessage = "User created successfully.";
        public static readonly updateSuccessMessage = "User updated successfully.";
        public static readonly deleteSuccessMessage = "Workspace deactivated successfully.";
        public static readonly atLeastOneRoleRequiredMsg = "Please select at least one role for this user";
        public static readonly somethingWentWrong = "Something went wrong. Please try again.";

        public static readonly ApproveSuccessMessage = "Workspace request Approved successfully.";
        public static readonly RejectSuccessMessage = "Workspace request Rejected successfully.";

        // regex patter
        public static readonly WebsitePattern = "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?";
        public static readonly EmailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
        public static readonly PhoneNumberPattern = "^(\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$";
        public static readonly zipCodePattern = "^[0-9]{5}(?:-[0-9]{4})?$";
}
