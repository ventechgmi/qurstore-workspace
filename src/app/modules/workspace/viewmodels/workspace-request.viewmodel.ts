import {from as observableFrom,  Observable, Subject, forkJoin } from "rxjs";

import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import {  ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from 'src/app/utils/alert/alert.service';
import { WorkspaceService } from '../services/workspace.service';
import { WorkspaceViewModel } from './workspace.viewmodel';
// import { AuthService } from "src/app/utils/auth.service";
import { debug } from 'console';
import { ValidationMessages } from '../helpers/validation-messages';
import { FormGroup, FormBuilder } from '@angular/forms';



@Injectable()
export class WorkspaceRequestViewModel extends WorkspaceViewModel {
  public userId: string;
  public vendorId: string;
  public workspaceList: any;
  public alertService: AlertService;
  viewModelDataGrid: DataGridViewModel;

  public workspaceRequestViewModelForm: FormGroup;
  header = [
    {       headerName: "Workspace Id", field: "workspaceId", isClickable: false     },
    {       headerName: "Name", field: "workspaceName", isClickable: false     },
    {       headerName: "Configuration", field: "workspaceConfig", isClickable: false     },
    {       headerName: "Type", field: "workspaceType"     },
    {       headerName: "Status", field: "workspaceStatus"     }
  ];



 // chartOptions: any;
  appliedFilters: any[];
  vendorWithoutUserList: any;
  public constructor(
    public workspaceService: WorkspaceService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    private fb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute
  ) {
   super(workspaceService, ViewModelType.Edit, ngxSmartModalService,  route);
  }

  public init(param?) {
    this.userId = param.userId;
    this.viewModelDataGrid = new DataGridViewModel();
    this.viewModelDataGrid.setPagination(false);
    this.subscribeToCreateSuccessMessage();
   // this.initSubscribeMethods();
    this.subscribers();
    this.viewModelDataGrid.setHeader(this.header, false, { canUpdate: false, canDelete: true});

    // this.setDataTableConfigurations();
    this.getWorkspaceListMock();

    this.workspaceRequestViewModelForm = this.fb.group({
      workspaceCatalog: ""
    });

    return observableFrom([]);
  }
  get workspaceRequestViewModelFormControls() {
    return this.workspaceRequestViewModelForm.controls;
  }

  public get detailForm() {
    return this.workspaceRequestViewModelForm.controls;
  }


  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  private subscribers() {
    this.viewModelDataGrid.events.subscribe(e => {
        switch (e.eventType) {
            case GridConfig.editClicked:
                this.vendorId = e.eventData.rowDetails[0];
                this.redirectToDetail();
                break;
            case GridConfig.deleteClicked:
                this.vendorId = e.eventData.rowDetails[0];
                this.deleteUserFromList();
                break;
            default:
                if (e.eventData && e.eventData.rowDetails) {
                  this.vendorId = e.eventData.rowDetails[0];
                  this.redirectToDetail();
                }
                break;
        }

    }, error => { });
}
  // Getting all the vendor details and redirecting to the vendor details page
  private redirectToDetail() {
    this.workspaceService.vendorIdSubject.next(this.vendorId);
    this.router
      .navigate(["/vendor-management/edit/" + this.vendorId])
      .then()
      .catch();
  }
  public redirectToCreate() {
    this.router
      .navigate(["/vendor-management/new"])
      .then()
      .catch();
  }

  public requestNewWorkspace() {
    this.ngxSmartModalService.getModal("requestNewWorkspaceFromCatalog").open();
  }
  public deleteUserFromList() {
      this.ngxSmartModalService.getModal("deleteConfirmModalFromList").open();
  }

  public async  deleteWorkspace() {
    this.ngxSmartModalService.getModal("deleteConfirmModalFromList").close();
    this.alertService.success(ValidationMessages.deleteSuccessMessage, true);
  }

  public getWorkspaceListMock() {
    // const resultHolder =  this.workspaceService.getAssignedWorkspaceMock();
    //     if (resultHolder) {
    //       this.workspaceList = resultHolder;
    //       this.initGrid(this.workspaceList);
    //     }
  }


  public initGrid(data) {
    const vendors = data ? data : [];
    vendors.map(vendor => {
      vendor.moduleCount = vendor.moduleCount === undefined ? "0" : vendor.moduleCount;
    });
    this.viewModelDataGrid.setRowsData(vendors, false);
  }

  public subscribeToCreateSuccessMessage() {
    this.workspaceService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      if (!data.isCreateAnother) {
        this.alertService.success(message, toStable);
      }
    });
  }


}
