import { from as observableFrom, Observable, Subject, forkJoin } from "rxjs";
import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from 'src/app/utils/alert/alert.service';
import { WorkspaceService } from '../services/workspace.service';
import { TagTypeService } from '../services/tag-type.service';
import { WorkspaceViewModel } from './workspace.viewmodel';
import { ValidationMessages } from '../helpers/validation-messages';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import moment from 'moment';


@Injectable()
export class WorkspaceListViewModel extends WorkspaceViewModel {
  public userId: string;
  public vendorId: string;
  public workspaceList: any;
  public workspaceCatalogList: any;
  public alertService: AlertService;
  viewModelDataGrid: DataGridViewModel;
  auditDataGrid: DataGridViewModel;
  private workspaceCatalogId: string;
  public workspaceTagTypeDetailsList: any;
  public workspaceTagTypeSelectedDetailsList: any;

  requestNewWorkSpace = false;
  workspaceRequestId: string;
  todayDate: string;
  public workspaceListViewModelForm: FormGroup;
  appliedFilters: any[];
  vendorWithoutUserList: any;
  public workSpaceTypeLabel: string;
  public diskSpaceRequestedLabel: string;
  public workspaceNameLabel: string;
  public reasonForRequestLabel: string;
  public workspaceRequestStatusAuditLabel: any;
  public rootVolumeSizeLabel: string;
  public workspaceDescriptionLabel: string;
  public userNameLabel: string;
  public neededByDateLabel: string;
  public currentStatusNameLabel: string;
  public isMonthlyPricing: boolean;
  public isHourlyPricing: boolean;
  public isRegionSelected = false;
  public isBundleOptionSelected = false;

  public canShowDetailLabel = false;

  public metadata = ["one", "two", "three", "four"];
  blankReasonForTermination = false;
  blankReasonForCancellation = false;
  header = [
    { headerName: "Bundle", field: "awsbundleId", isClickable: true, width: 100 },
    { headerName: "Configuration", field: "workspaceDescription", isClickable: false, width: 400 },
    { headerName: "Workspace Name", field: "workspaceName", isClickable: false },
    { headerName: "Last Modified", field: "lastModifiedDateDisplay", width: 100 },
    { headerName: "Status", field: "currentStatusName", width: 75 }
  ];
  auditHeader = [
    { headerName: "Action Date", field: "actionDateDisplay" },
    { headerName: "User Name", field: "actionByUserName" },
    { headerName: "Status", field: "statusName" },
    { headerName: "Comments", field: "actionComments" }
  ];

  public selectedTags: string[] = [];
  // map the appropriate columns to fields property
  public fields: object = { text: 'itemName', value: 'id' };
  // set the placeholder to MultiSelect input element
  public waterMark: string = '-- Select--';
  // set the type of mode for how to visualized the selected items in input element.
  public box: string = 'Box';
  public modifiedData;
  public filteredData: object;
  public filterstrDataForWindow: object;
  public filterstrDataForPlus: object;
  public filterstrDataForLinux: object;
  public bundleOptions = ["Linux Bundle Options", "Windows Bundle Options", "Windows Bundle Options- Brings your own License(BYOUL)"]
  public usageType = ["AlwaysOn", "AutoStop"]
  public filterstrDataForWindowHeader;
  public filterstrDataForPlusHeader;
  public filterstrDataForLinuxHeader;
  public selectedspaceUsageOptions: string;
  public costDataDetails: object;
  public costData: object;
  public awsBundleName = [];
  public WindowHeaderData = [];
  public PlusHeader = [];
  public LinuxHeader = [];
  public selectedSpecificationBundleOptions: string;
  public filterstrDataForLinuxHeaderData: object;
  public filterstrDataForPlusHeaderData: object;
  public filterstrDataForWindowHeaderData: object;
  public monthValue: number;
  public hourValue: number;
  public monthValueAlwaysOn: number;
  public hourUsagecount: number;
  public dayUsagecount: number;
  public totalCost: number;
  public monthlySavings: number;
  public enableCostCalculation:boolean = false;
  private dateTimeFormat: string = 'MM/DD/YYYY hh:mm A';
  private dateFormat: string = 'MM/DD/YYYY';
  private requestNewWorkspaceFromCatalog: string = "requestNewWorkspaceFromCatalog";
  private requestNewWorkspaceFromCatalogDetails: string = "requestNewWorkspaceFromCatalogDetails";
  private cancelConfirmModalFromList: string = "cancelConfirmModalFromList";
  private terminateConfirmModalFromList: string = "terminateConfirmModalFromList";
  private selectedBundleOptions: string;


  public onSelect(args: any, workspaceTagDetail: any): void {
    if (args && (args.itemData.value === undefined)) {
      args.itemData.isCustomCreated = true;
      args.itemData.tagTypeId = workspaceTagDetail.id;
    }
    workspaceTagDetail.selectedTags.push(args.itemData);
  }

  public onRemove(args: any, workspaceTagDetail: any): void {
    const tempSelectedTags = [];
    _.each(this.selectedTags, selectedTags => {
      if (args.itemData.itemName !== selectedTags.itemName) {
        tempSelectedTags.push(selectedTags);
      }
    });
    workspaceTagDetail.selectedTags = tempSelectedTags;
  }


  public get CanShowDetails() {
    return this.canShowDetailLabel;
  }
  public hideDetails() {
    this.canShowDetailLabel = false;
  }

  public initToday() {
    this.todayDate = (new Date()).toISOString().split('T')[0];
    this.workspaceListViewModelForm.patchValue({
      neededBy: this.todayDate
    });

  }
  public constructor(
    public workspaceService: WorkspaceService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    private fb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public tagTypeService: TagTypeService
  ) {
    super(workspaceService, ViewModelType.Edit, ngxSmartModalService, route);
  }

  public init(param?) {
    this.userId = param.userId;
    this.viewModelDataGrid = new DataGridViewModel();

    this.subscribeToCreateSuccessMessage();
    this.subscribers();
    this.viewModelDataGrid.setHeader(this.header, false, { canCancelRequest: true, canTerminate: true });

    this.getAssignedWorkspace();
    this.getWorkspaceCatalogList();
    this.getAWSWorkspaceCatalogList();
    this.workspaceListViewModelForm = this.fb.group({
      workspaceType: "",
      // requestedDiskSpace: ["", [Validators.required]],
      userWorkspaceName: ["", [Validators.required]],
      pricingModel: [null, [Validators.required]],
      neededBy: ["", [Validators.required]],
      reasonForRequest: ["", [Validators.required]],
      terminateDecisionComment: "",
      cancelDecisionComment: "",
      hourUsage: ["",[Validators.max(24)]],
      dayUsage: ["",[Validators.min(1), Validators.max(31)]]
    });
    this.viewModelDataGrid.defaultSortModel = [
      {
        colId: 'lastModifiedDateDisplay',
        sort: 'desc',
      }
    ];
    this.initToday();
    this.auditDataGrid = new DataGridViewModel();
    this.auditDataGrid.setHeader(this.auditHeader, false);
    this.auditDataGrid.setPagination(false);
    this.auditDataGrid.defaultSortModel = [
      {
        colId: 'actionDateDisplay',
        sort: 'desc',
      }
    ];
    this.workspaceTagTypeDetailsList = [];
    this.workspaceTagTypeSelectedDetailsList = [];
    return observableFrom([]);
  }
  public initGridAuditLog(data) {
    const auditLogs = data ? data : [];
    auditLogs.map(auditLog => {
      if (auditLog.actionDate) {
        auditLog.actionDateDisplay = moment(auditLog.actionDate).format(this.dateTimeFormat);
      }
    });
    this.auditDataGrid.setRowsData(auditLogs, false);
  }
  private validateForm() {
    const { neededBy } = this.workspaceListViewModelFormControls;
    let isInputsValid = false;
    if (new Date(neededBy.value).getTime() >= new Date().getTime()) {
      isInputsValid = true;
    }
    return isInputsValid;
  }

  private resetFormControls() {
    this.workspaceListViewModelForm.patchValue({
      workspaceType: "",
      userWorkspaceName: " ",
      pricingModel: " ",
      reasonForRequest: " ",
      terminateDecisionComment: "",
      cancelDecisionComment: "",
      project: "",
      hourUsage: "",
      dayUsage: ""
    });
  }
  public saveRequest() {
    if (this.workspaceListViewModelForm.invalid) { return; }
    if (!this.validateForm()) { return; }
    const saveRequestData = this.transformToEntityModel();
    this.workspaceService.saveWorkspaceRequest(saveRequestData)
      .subscribe(
        (data) => {
          this.alertService.success(data.message, false);
          this.workspaceService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
          this.workspaceRequestId = "";
          this.resetForm();
          this.resetFormControls();
          this.workspaceCatalogId = "";
          this.initToday();
        },
        (err) => {
          this.alertService.error(err.error.errorList[0], false);
        },
        () => {
          this.getAssignedWorkspace();
          this.ngxSmartModalService.getModal(this.requestNewWorkspaceFromCatalog).close();
        });
  }

  public terminateWorkspace() {
    this.ngxSmartModalService.getModal(this.terminateConfirmModalFromList).open();
  }
  public transformToEntityModelTerminateRequest() {
    const { terminateDecisionComment } = this.workspaceListViewModelFormControls;
    return {
      id: this.workspaceRequestId,
      decisionReason: terminateDecisionComment.value,
    };
  }
  public async terminateWorkspaceAction() {
    const saveRequestData = this.transformToEntityModelTerminateRequest();
    if (saveRequestData?.decisionReason.length === 0) {
      this.blankReasonForTermination = true;
      return;
    } else {
      this.blankReasonForTermination = false;
    }
    this.ngxSmartModalService.getModal(this.terminateConfirmModalFromList).close();

    this.workspaceService.terminateWorkspaceRequest(saveRequestData)
      .subscribe(
        (data) => {
          this.alertService.success(data.message, false);
          this.workspaceRequestId = "";
          this.resetFormControls();
          this.workspaceService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
        },
        (err) => {
          this.alertService.error(err.error.errorList[0], false);
        },
        () => {
          this.getAssignedWorkspace();
          this.alertService.success(ValidationMessages.deleteSuccessMessage, true);
        });
  }

  public transformToEntityModel() {
    const { workspaceType,  userWorkspaceName, pricingModel, neededBy, reasonForRequest } = this.workspaceListViewModelFormControls;
    // const rootVolume: number = +this.rootVolumeSizeLabel;
    // const userVolume: number = +requestedDiskSpace.value;
    const tempWorkspaceTagTypeDetailsList = this.workspaceTagTypeDetailsList;
    _.each(tempWorkspaceTagTypeDetailsList, workspaceTagTypeDetail => {
      workspaceTagTypeDetail.lastModifiedBy = this.userId;
      _.each(workspaceTagTypeDetail.selectedTags, selectedTagDetail => {
        if (selectedTagDetail && selectedTagDetail.isCustomCreated) {
          selectedTagDetail.value = selectedTagDetail.itemName;
          delete selectedTagDetail.id;
        }
      });
    });
    return {
      workspaceName: userWorkspaceName.value,
      pricingModel: pricingModel.value,
      workspaceCatalogId: "D616CFC7-5CA0-457F-904B-E47E78D62A10",
      neededByDate: neededBy.value,
      Comments: reasonForRequest.value
      // RootVolumeSize: rootVolume,
      // UserVolumeSize: userVolume,
      // workspaceTagMappingDetails: tempWorkspaceTagTypeDetailsList
    };
  }

  public get CanRequestNewWorkspace() {
    return this.requestNewWorkSpace;
  }

  get workspaceListViewModelFormControls() {
    return this.workspaceListViewModelForm.controls;
  }

  public get detailForm() {
    return this.workspaceListViewModelForm.controls;
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  private subscribers() {
    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.editClicked:
          this.vendorId = e.eventData.rowDetails[0];
          break;
        case GridConfig.terminateRequestClicked:
          this.workspaceRequestId = e.eventData.rowDetails[0];
          this.terminateWorkspace();
          break;
        case GridConfig.cancelRequestClicked:
          this.workspaceRequestId = e.eventData.rowDetails[0];
          this.cancelWorkspacePopUp();
          break;
        case GridConfig.columnClicked:
          if (e.eventData.columnId === "awsbundleId") {
            this.workspaceRequestId = e.eventData.rowDetails[0];
            this.showDetails();
          }
          break;
        default:
          if (e.eventData && e.eventData.rowDetails) {
            this.workspaceRequestId = e.eventData.rowDetails[0];
          }
          break;
      }

    }, error => { });
  }

  public cancelWorkspacePopUp() {
    this.ngxSmartModalService.getModal(this.cancelConfirmModalFromList).open();
  }

  public transformToEntityModelCancelRequest() {
    const { cancelDecisionComment } = this.workspaceListViewModelFormControls;
    return {
      id: this.workspaceRequestId,
      decisionReason: cancelDecisionComment.value,
    };
  }
  public cancelWorkspaceAction() {
    const saveRequestData = this.transformToEntityModelCancelRequest();
    if (saveRequestData?.decisionReason.length === 0) {
      this.blankReasonForCancellation = true;
      return;
    } else {
      this.blankReasonForCancellation = false;
    }
    this.ngxSmartModalService.getModal(this.cancelConfirmModalFromList).close();
    this.workspaceService.cancelWorkspaceRequest(saveRequestData)
      .subscribe(
        (data) => {
          this.alertService.success(data.message, false);
          this.workspaceRequestId = "";
          this.workspaceService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
        },
        (err) => {
          this.alertService.error(err.error.errorList[0], false);
        },
        () => {
          this.getAssignedWorkspace();
          this.alertService.success(ValidationMessages.deleteSuccessMessage, true);
        });
  }

  private showDetails() {

    const rowSelected = this.workspaceList.find(x => x.id === this.workspaceRequestId);
    this.workspaceDescriptionLabel = rowSelected.workspaceDescription;
    this.rootVolumeSizeLabel = rowSelected.rootVolumeSize;
    this.diskSpaceRequestedLabel = rowSelected.userVolumeSize;
    this.workspaceNameLabel = rowSelected.workspaceName;
    this.userNameLabel = rowSelected.userName;
    this.currentStatusNameLabel = rowSelected.currentStatusName;

    this.neededByDateLabel = rowSelected.neededByDate; // moment(rowSelected.neededByDate).format(this.dateFormat);
    this.initGridAuditLog(rowSelected.workspaceRequestStatusAudit);
    this.getWorkspaceTagTypeDetailById(this.workspaceRequestId);
    this.ngxSmartModalService.getModal(this.requestNewWorkspaceFromCatalogDetails).open();

  }

  public changeSpecificationForBundleOptions(e) {
    this.hourValue = null;
    this.monthValue = null;
    this.monthValueAlwaysOn = null;
    this.selectedSpecificationBundleOptions = e.target.value;
    const costDataDetails = this.filterstrDataForWindow ? this.filterstrDataForWindow : this.filterstrDataForPlus ? this.filterstrDataForPlus : this.filterstrDataForLinux;

    //with selected bundle specification filter the data
    this.costData = Object.keys(costDataDetails).filter(function (key) {
      return (key.startsWith(e.target.value));
    }).reduce(function (newData, key) {
      newData[key] = costDataDetails[key];
      return newData;
    }, {});
    const costData2 = this.costData;
    //filter hour value object from data
    const autoStopCostHourData: any = Object.keys(costData2).filter(function (key) {
      return (key.includes("hour"));
    }).reduce(function (newData, key) {
      newData[key] = costData2[key];
      return newData[key];
    }, {});
    this.hourValue = Object.keys(autoStopCostHourData).length > 0 ? (autoStopCostHourData.price).slice(0, -8) : null;

    //filter autostop cost value for month object from data
    const autoStopCostMonthData: any = Object.keys(costData2).filter(function (key) {
      return ((key.includes("month") && (key.includes("AutoStop"))));
    }).reduce(function (newData, key) {
      newData[key] = costData2[key];
      return newData[key];
    }, {});
    //filter autostop cost value for hour object from data
    this.monthValue = Object.keys(autoStopCostMonthData).length > 0 ? (autoStopCostMonthData.price).slice(0, -8) : null;

    const alwaysOnCostMonthData: any = Object.keys(costData2).filter(function (key) {
      return ((key.includes("month") && (key.includes("AlwaysOn"))));
    }).reduce(function (newData, key) {
      newData[key] = costData2[key];
      return newData[key];
    }, {});
    this.monthValueAlwaysOn = Object.keys(alwaysOnCostMonthData).length > 0 ? (alwaysOnCostMonthData.price).slice(0, -8) : null;
  }


  public changeWorkspaceBundleOptions(e) {
    this.isBundleOptionSelected = true;
    this.selectedBundleOptions = e.target.value;
    this.PlusHeader = [];
    this.WindowHeaderData = [];
    this.LinuxHeader = [];
  }

  public calculatePrice() {
    this.enableCostCalculation = true;
    }

  public onPricingModelChange(value) {
    const pricingModel = value.srcElement.defaultValue;
    if (pricingModel === "Monthly") {
      this.isHourlyPricing = false;
      this.isMonthlyPricing = true;
    }
    if (pricingModel === "Hourly") {
      this.isMonthlyPricing = false;
      this.isHourlyPricing = true;
    }
  }

  public onValueChangeCalculateTotalPrice() {
    this.dayUsagecount = this.workspaceListViewModelForm.get('dayUsage').value;
    this.hourUsagecount = this.workspaceListViewModelForm.get('hourUsage').value;
    if(this.dayUsagecount && this.hourUsagecount){
      this.totalCost = parseFloat(this.monthValue.toString()) + parseFloat((this.hourValue * this.hourUsagecount * this.dayUsagecount).toString());
      this.monthlySavings = parseFloat(this.totalCost.toString()) - parseFloat(this.monthValueAlwaysOn.toString());
    } else {
      this.totalCost = null;
    }
  }

  public changeWorkspaceBundle(e) {
    this.isRegionSelected = true;
    this.filterstrDataForPlus = null;
    this.filterstrDataForWindow = null;
    this.filterstrDataForLinux = null;
    this.filterstrDataForWindowHeader = null;
    this.filterstrDataForPlusHeader = null;
    this.filterstrDataForLinuxHeader = null;
    const valueSelected = e.target.value;

    //REGION DROPDOWN
    //filter region list from the Data
    const filtered: any = Object.keys(this.modifiedData.regions)
      .filter(key => valueSelected.includes(key))
      .reduce((obj, key) => {
        obj[key] = this.modifiedData.regions[key];
        return obj[key];
      }, {});

    //SPECIFICATION FOR BUNDLE SELECTION DROPDOWN
    if (this.selectedBundleOptions === "Windows Bundle Options") {
      //filters data respective to Window bundle
      this.filterstrDataForWindow = Object.keys(filtered).filter(function (key) {
        return key.indexOf('Plus') == 0 || (key.indexOf('Windows') == 0 && !key.endsWith("BYOL"));
      }).reduce(function (newData, key) {
        newData[key] = filtered[key];
        return newData;
      }, {});
      //To avoid repeation of month License and hour license filtering one set of data
      this.filterstrDataForWindowHeaderData = Object.keys(this.filterstrDataForWindow).filter(function (key) {
        return key.endsWith("the month License Included")
      }).reduce(function (newData, key) {
        newData[key] = filtered[key];
        return newData;
      }, {});
      //Crop unwanted details from the Key. This value is to bind in the Specification Dropdown
      this.filterstrDataForWindowHeader = Object.keys(this.filterstrDataForWindowHeaderData);
      this.filterstrDataForWindowHeader.forEach(element => {
        this.WindowHeaderData.push(element.split("GB", 3).join("GB"));
      });

    } else if (this.selectedBundleOptions === "Windows Bundle Options- Brings your own License(BYOUL)") {
      //filters data respective to Window(BYOUL) bundle
      this.filterstrDataForPlus = Object.keys(filtered).filter(function (key) {
        return (key.indexOf('Windows') == 0 && key.endsWith("BYOL"));
      }).reduce(function (newData, key) {
        newData[key] = filtered[key];
        return newData;
      }, {});
      //To avoid repeation of month License and hour license filtering on set of data
      this.filterstrDataForPlusHeaderData = Object.keys(this.filterstrDataForPlus).filter(function (key) {
        return key.endsWith("the month License BYOL")
      }).reduce(function (newData, key) {
        newData[key] = filtered[key];
        return newData;
      }, {});
      //Crop unwanted details from the Key. This value is to bind in the Specification Dropdown
      this.filterstrDataForPlusHeader = Object.keys(this.filterstrDataForPlusHeaderData);
      this.filterstrDataForPlusHeader.forEach(element => {
        this.PlusHeader.push(element.split("GB", 3).join("GB"));
      });

    } else if (this.selectedBundleOptions === "Linux Bundle Options") {
      //filters data respective to Window(BYOUL) bundle
      this.filterstrDataForLinux = Object.keys(filtered).filter(function (key) {
        return key.indexOf('Linux') == 0;
      }).reduce(function (newData, key) {
        newData[key] = filtered[key];
        return newData;
      }, {});
      //To avoid repeation of month License and hour license filtering on set of data
      this.filterstrDataForLinuxHeaderData = Object.keys(this.filterstrDataForLinux).filter(function (key) {
        return key.endsWith("by the month License None")
      }).reduce(function (newData, key) {
        newData[key] = filtered[key];
        return newData;
      }, {});
      //Crop unwanted details from the Key. This value is to bind in the Specification Dropdown
      this.filterstrDataForLinuxHeader = Object.keys(this.filterstrDataForLinuxHeaderData);
      this.filterstrDataForLinuxHeader.forEach(element => {
        this.LinuxHeader.push(element.split("GB", 3).join("GB"));
      });

    }

    const rowSelected = this.workspaceCatalogList.find(x => x.id === e.target.value);
    if (rowSelected) {
      // this.workspaceListViewModelForm.patchValue({
      //   requestedDiskSpace: rowSelected.userCapacity,
      // });
      this.workspaceCatalogId = "D616CFC7-5CA0-457F-904B-E47E78D62A10";
      this.rootVolumeSizeLabel = rowSelected.rootCapacity;
    } else {
      // this.workspaceListViewModelForm.patchValue({
      //   requestedDiskSpace: "",
      // });
      this.workspaceCatalogId = "";
      this.rootVolumeSizeLabel = "";
    }
  }

  public getAssignedWorkspace() {
    this.workspaceService.getAssignedWorkspace().subscribe(
      data => {
        if (data) {
          this.workspaceList = data;
          this.initGrid(this.workspaceList);
          this.setRequestNewWorkSpace(data);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public getAWSWorkspaceCatalogList() {
    this.workspaceService.getAWSWorkspaceCatalogList().subscribe(
      data => {
        if (data) {
          this.modifiedData = JSON.parse(data);
          this.workspaceCatalogList = Object.keys(this.modifiedData.regions);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public getWorkspaceCatalogList() {
    this.workspaceService.getWorkspaceCatalogList().subscribe(
      data => {
        if (data) {
          // this.workspaceCatalogList = data;
          this.awsBundleName = data;
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  private setRequestNewWorkSpace(data) {
    let isAtleastOneEligible = true;
    data.forEach(element => {
      if (element.isOpen === true || element.isCompleted === true) {
        isAtleastOneEligible = false;
      }
    });
    this.requestNewWorkSpace = isAtleastOneEligible;
  }
  private resetForm() {
    this.hideDetails();
    this.workspaceRequestId = "";
    this.workspaceCatalogId = "";
    this.workSpaceTypeLabel = "";
    this.diskSpaceRequestedLabel = "";
    this.workspaceNameLabel = "";
    this.reasonForRequestLabel = "";
    this.workspaceTagTypeDetailsList = [];
    this.selectedTags = [];
    this.initToday();
  }
  public requestNewWorkspace() {
    this.getAWSWorkspaceCatalogList();
    this.resetForm();
    this.getWorkspaceTagTypeDetails();
    this.ngxSmartModalService.getModal(this.requestNewWorkspaceFromCatalog).open();
  }


  public initGrid(data) {
    const workspaces = data ? data : [];
    workspaces.map(workspace => {
      if (workspace.lastModifiedDate) {

        workspace.lastModifiedDateDisplay = moment(workspace.lastModifiedDate).format(this.dateTimeFormat);
      }
    });

    this.viewModelDataGrid.setRowsData(workspaces, false);
    this.viewModelDataGrid.paginationPageSize = 5;
  }

  public subscribeToCreateSuccessMessage() {
    this.workspaceService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      if (!data.isCreateAnother) {
        this.alertService.success(message, toStable);
      }
    });
  }

  public getSettingsDetails(workspaceTagDetail: any): any {
    let settings = {
      singleSelection: workspaceTagDetail.isSingleSelection,
      text: "Select Value",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
    return settings;
  }

  private getWorkspaceTagTypeDetails(): void {
    this.tagTypeService.getWorkspaceTagTypeDetails("Workspace").subscribe(
      data => {
        if (data) {
          _.each(data, item => {
            item.selectedTags = [];
            _.each(item.tagValues, tagValue => {
              tagValue.itemName = tagValue.value;
            });
          });
          this.workspaceTagTypeDetailsList = data;
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  private getWorkspaceTagTypeDetailById(contextId): void {
    this.workspaceTagTypeSelectedDetailsList = [];
    this.tagTypeService.getWorkspaceTagTypeDetailById(contextId).subscribe(
      data => {
        if (data) {
          const tagGropedValue = _.groupBy(data, item => {
            return item.tagValue.tagTypeId;
          });
          const formattedData = [];
          _.each(tagGropedValue, groupData => {
            const data = {
              name: groupData[0].tagValue.tagType.name,
              selectedTagValues: this.getSelectedTagValue(groupData)
            };
            formattedData.push(data);
          });
          this.workspaceTagTypeSelectedDetailsList = formattedData;
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  private getSelectedTagValue(groupData): any {
    const values = [];
    _.each(groupData, data => {
      values.push(data.tagValue.value);
    });
    return values.join(", ");
  }
}
