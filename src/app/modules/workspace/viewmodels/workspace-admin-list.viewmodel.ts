import {from as observableFrom,  Observable, Subject, forkJoin } from "rxjs";

import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import {  ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from 'src/app/utils/alert/alert.service';
import { WorkspaceService } from '../services/workspace.service';
import { WorkspaceViewModel } from './workspace.viewmodel';
// import { AuthService } from "src/app/utils/auth.service";
import { debug } from 'console';
import { ValidationMessages } from '../helpers/validation-messages';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { debugOutputAstAsTypeScript } from '@angular/compiler';
import moment from 'moment';
import { TagTypeService } from '../services/tag-type.service';



@Injectable()
export class WorkspaceAdminListViewModel extends WorkspaceViewModel {
  public userId: string;
  // public workspaceId: string;
  public workspaceList: any;
  public alertService: AlertService;
  viewModelDataGrid: DataGridViewModel;
  auditDataGrid: DataGridViewModel;
  public workSpaceTypeLabel: string;
  public diskSpaceRequestedLabel: string;
  public neededByLabel: string;
  public workspaceNameLabel: string;
  // public workspaceDescription: string;
  public reasonForRequestLabel: string;
  public workspaceListViewModelForm: FormGroup;
  workspaceRequestId: string;
  blankReasonForTermination = false;
  blankReasonForCancellation = false;
  blankReasonForApproval = false;
  blankUserName = false;


  public workspaceRequestStatusAuditLabel: any;
  public rootVolumeSizeLabel: string;
  public workspaceDescriptionLabel: string;
  public userNameLabel: string;
  public neededByDateLabel: string;
  public currentStatusNameLabel: string;
  public workspaceTagTypeSelectedDetailsList: any;

  header = [
    // {       headerName: "Workspace Id", field: "workspaceId", isClickable: false     },
    {       headerName: "Bundle", field: "awsbundleId", isClickable: true , width: 100    },
    {       headerName: "Configuration", field: "workspaceDescription", isClickable: false, width: 520     },
    {       headerName: "Requested By", field: "userName", isClickable: false  , width: 100      },
    {       headerName: "User Assigned Name", field: "workspaceName", isClickable: false     },
    {       headerName: "Last Modified On", field: "lastModifiedDateDisplay", isClickable: false  , width: 150},
    {       headerName: "Status", field: "currentStatusName"  , width: 100   }
  ];
  auditHeader =  [
    {       headerName: "Action Date", field: "actionDateDisplay"    },
    {       headerName: "User Name", field: "actionByUserName"    },
    {       headerName: "Status", field: "statusName"  },
    {       headerName: "Comments", field: "actionComments"  }
  ];
  // {       headerName: "Reason for Request", field: "reasonForRequest"     }

 // chartOptions: any;
  appliedFilters: any[];
  vendorWithoutUserList: any;
  private PendingApproval:string = "Pending Approval";
  private dateFormat:string = "MM/DD/YYYY";
  private dateTimeFormat: string = "MM/DD/YYYY hh:mm A";
  private requestNewWorkspaceFromCatalogDetails: string = "requestNewWorkspaceFromCatalogDetails";
  private deleteConfirmModalFromList:string = "deleteConfirmModalFromList";
  private requestNewWorkspaceFromCatalog:string = "requestNewWorkspaceFromCatalog";
  private cancelConfirmModalFromList:string = "cancelConfirmModalFromList";
  public constructor(
    public workspaceService: WorkspaceService,
    private router: Router,
    private fb: FormBuilder,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public tagTypeService: TagTypeService
  ) {
   super(workspaceService, ViewModelType.Edit, ngxSmartModalService,  route);
  }

  public init(param?) {
    this.userId = param.userId;
    this.viewModelDataGrid = new DataGridViewModel();
    this.subscribeToCreateSuccessMessage();
    this.subscribers();
    this.viewModelDataGrid.setHeader(this.header, false, { canCancelRequest: false, canTerminate: true, canApproveReject: true });
    this.viewModelDataGrid.defaultSortModel = [
      {
        colId: 'lastModifiedDateDisplay',
        sort: 'desc',
      }
    ];
    this.workspaceListViewModelForm = this.fb.group({
      decisionComment: "",
      userName: "",
      terminateDecisionComment: "",
      cancelDecisionComment: ""
    });

    this.getAllWorkspace();
    this.auditDataGrid = new DataGridViewModel();
      this.auditDataGrid.setHeader(this.auditHeader, false);
      this.auditDataGrid.setPagination(false);
      this.auditDataGrid.defaultSortModel = [
        {
          colId: 'actionDateDisplay',
          sort: 'desc',
        }
      ];
    this.workspaceTagTypeSelectedDetailsList = [];
    return observableFrom([]);
  }

  // private getFormattedDate(date) {
  //   const year = date.getFullYear();
  //   let month = (1 + date.getMonth()).toString();
  //   month = month.length > 1 ? month : '0' + month;
  //   let day = date.getDate().toString();
  //   day = day.length > 1 ? day : '0' + day;
  //   return month + '/' + day + '/' + year;
  // }
  private resetControls() {
    this.workspaceListViewModelForm.patchValue({
      decisionComment: "",
      userName: "",
      terminateDecisionComment: "",
      cancelDecisionComment: ""
    });
  }
  private showDetails() {
    this.resetControls();
    const rowSelected = this.workspaceList.find(x => x.id === this.workspaceRequestId);
    if (rowSelected) {
      this.workspaceDescriptionLabel = rowSelected.workspaceDescription;
      this.diskSpaceRequestedLabel = rowSelected.userVolumeSize;
      this.workspaceNameLabel = rowSelected.workspaceName;
      this.reasonForRequestLabel = (rowSelected.workspaceRequestStatusAudit.find(x => x.statusName === this.PendingApproval)).actionComments;
      this.neededByLabel = moment(rowSelected.neededByDate).format(this.dateFormat);
      this.getWorkspaceTagTypeDetailById(this.workspaceRequestId);
    }
  }
  private showDetailsReadOnly() {
    this.resetControls();
    const rowSelected = this.workspaceList.find(x => x.id === this.workspaceRequestId);
    if (rowSelected) {
        this.workspaceDescriptionLabel = rowSelected.workspaceDescription;
        this.rootVolumeSizeLabel = rowSelected.rootVolumeSize;
        this.diskSpaceRequestedLabel = rowSelected.userVolumeSize;
        this.workspaceNameLabel = rowSelected.workspaceName;
        this.userNameLabel = rowSelected.userName;
        this.currentStatusNameLabel = rowSelected.currentStatusName;
        this.reasonForRequestLabel = (rowSelected.workspaceRequestStatusAudit.find(x => x.statusName === this.PendingApproval)) ? (rowSelected.workspaceRequestStatusAudit.find(x => x.statusName === this.PendingApproval)).actionComments : null;
      // this.workspaceRequestStatusAuditLabel = rowSelected.workspaceRequestStatusAudit;
        // this.neededByLabel =  this.getFormattedDate(new Date(rowSelected.neededByDate));
        this.neededByDateLabel = moment(rowSelected.neededByDate).format(this.dateFormat);
        this.initGridAuditLog(rowSelected.workspaceRequestStatusAudit);
        this.getWorkspaceTagTypeDetailById(this.workspaceRequestId);
        this.ngxSmartModalService.getModal(this.requestNewWorkspaceFromCatalogDetails).open();
    }
  }
  public initGridAuditLog(data) {
    const auditLogs = data ? data : [];
    auditLogs.map(auditLog => {
      if (auditLog.actionDate) {
        auditLog.actionDateDisplay = moment(auditLog.actionDate).format(this.dateTimeFormat);
      }
    });
    this.auditDataGrid.setRowsData(auditLogs, false);
  }

  // public fillData() {
  //    this.workSpaceTypeLabel = "test";
  //    this.diskSpaceRequestedLabel = "disk";
  //    this.neededByLabel = "needed";
  //    this.workspaceNameLabel = "work name";
  //    this.reasonForRequestLabel = "reason";

  // }
  public getAllWorkspace() {
    this.workspaceService.getAllWorkspace().subscribe(
      data => {
        if (data) {
          this.workspaceList =  data;
          this.initGrid(this.workspaceList);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public getAllWorkspaceMock() {
    const resultHolder =  this.workspaceService.getAllWorkspaceMock();
        if (resultHolder) {
          this.workspaceList = resultHolder;
          this.initGrid(this.workspaceList);
        }
  }

  public initGrid(data) {
    const workspaces = data ? data : [];
    workspaces.map(workspace => {
      if (workspace.lastModifiedDate) {
        workspace.lastModifiedDateDisplay = moment(workspace.lastModifiedDate).format(this.dateTimeFormat);
      }
    });

    this.viewModelDataGrid.setRowsData(workspaces, false);
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  private subscribers() {
    this.viewModelDataGrid.events.subscribe(e => {
        switch (e.eventType) {
            case GridConfig.approveRejectClicked:
              this.workspaceRequestId = e.eventData.rowDetails[0];
              this.approveRejectClicked();
              break;
            case GridConfig.terminateRequestClicked:
                this.workspaceRequestId = e.eventData.rowDetails[0];
                this.terminateWorkspacePopUp();
                break;
            case GridConfig.cancelRequestClicked:
              this.workspaceRequestId = e.eventData.rowDetails[0];
              this.cancelWorkspacePopUp();
              break;
            case GridConfig.columnClicked:
              this.workspaceRequestId = e.eventData.rowDetails[0];
              this.showDetailsReadOnly();
              break;
            default:
                if (e.eventData && e.eventData.rowDetails) {
                  this.workspaceRequestId = e.eventData.rowDetails[0];
                  this.redirectToDetail();
                }
                break;
        }

    }, error => { });
  }
  public cancelWorkspacePopUp() {
    this.ngxSmartModalService.getModal(this.cancelConfirmModalFromList).open();
  }

  public    cancelWorkspaceAction() {
    const saveRequestData = this.transformToEntityModelCancelRequest();
    if (saveRequestData?.decisionReason.length === 0) {
      this.blankReasonForCancellation = true;
      return;
    } else {
      this.blankReasonForCancellation = false;
    }
    this.ngxSmartModalService.getModal(this.cancelConfirmModalFromList).close();
    this.workspaceService.cancelWorkspaceRequest(saveRequestData)
      .subscribe(
        (data) => {
          this.alertService.success(data.message, false);
          this.workspaceRequestId = "";
          this.workspaceService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
        },
        (err) => {
          this.alertService.error(err.error.errorList[0], false);
        },
        () => {
          this.getAllWorkspace();
          this.alertService.success(ValidationMessages.deleteSuccessMessage, true);
      });
  }



  public terminateWorkspacePopUp() {
    this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).open();
  }

  public   terminateWorkspaceAction() {
    const saveRequestData = this.transformToEntityModelTerminateRequest();
    if (saveRequestData?.decisionReason.length === 0) {
      this.blankReasonForTermination = true;
      return;
    } else {
      this.blankReasonForTermination = false;
    }
    this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).close();
    this.workspaceService.terminateWorkspaceRequest(saveRequestData)
      .subscribe(
        (data) => {
          this.alertService.success(data.message, false);
          this.workspaceRequestId = "";
          this.workspaceService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
        },
        (err) => {
          this.alertService.error(err.error.errorList[0], false);
        },
        () => {
          this.getAllWorkspace();
          this.alertService.success(ValidationMessages.deleteSuccessMessage, true);
      });
  }



  private validateInputForApproval(data) {
    let invalidInput = false;
    if (data?.decisionReason.length === 0) {
      this.blankReasonForApproval = true;
      invalidInput = true;
    } else {
      this.blankReasonForApproval = false;
    }
    if (data?.AWSUserName.length === 0) {
      this.blankUserName = true;
      invalidInput = true;
    } else {
      this.blankUserName = false;
    }
    return invalidInput;
  }
  private approveRejectClicked() {
    this.showDetails();
    this.ngxSmartModalService.getModal(this.requestNewWorkspaceFromCatalog).open();
  }


  public approveRequest() {
    if (this.workspaceListViewModelForm.invalid) { return; }
    const saveRequestData = this.transformToEntityModel();
    if (this.validateInputForApproval(saveRequestData)) { return; }
    this.ngxSmartModalService.getModal(this.requestNewWorkspaceFromCatalog).close();
    this.workspaceService.approveWorkspaceRequest(saveRequestData)
      .subscribe(
        (data) => {
          this.alertService.success(data.message, false);
          this.workspaceService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
          this.workspaceRequestId = "";
          this.resetControls();
          this.alertService.success(ValidationMessages.ApproveSuccessMessage, true);
        },
        (err) => {
          this.alertService.error(`Error while Saving: ${err.error.errorList[0]}`, false);
        },
        () => {
          this.getAllWorkspace();
      });
  }


  private validateInputForRejection(data) {
    let invalidInput = false;
    if (data?.decisionReason.length === 0) {
      this.blankReasonForApproval = true;
      invalidInput = true;
    } else {
      this.blankReasonForApproval = false;
    }
    return invalidInput;
  }

  public rejectRequest() {
    const saveRequestData = this.transformToEntityModel();
    if (this.validateInputForRejection(saveRequestData)) { return; }
    this.ngxSmartModalService.getModal(this.requestNewWorkspaceFromCatalog).close();

    this.workspaceService.rejectWorkspaceRequest(saveRequestData)
      .subscribe(
        (data) => {
          this.alertService.success(data.message, false);
          this.workspaceService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
          this.workspaceRequestId = "";
          this.resetControls();
          this.alertService.success(ValidationMessages.RejectSuccessMessage, true);

        },
        (err) => {
          this.alertService.error(err.error.errorList[0], false);
        },
        () => {
          this.getAllWorkspace();
      });
  }

  get workspaceListViewModelFormControls() {
    return this.workspaceListViewModelForm.controls;
  }
  public transformToEntityModel() {
    const { decisionComment, userName} = this.workspaceListViewModelFormControls;
    return    {
      id: this.workspaceRequestId,
      decisionReason : decisionComment.value,
      AWSUserName : userName.value
    };
  }
  public transformToEntityModelTerminateRequest() {
    const { terminateDecisionComment} = this.workspaceListViewModelFormControls;
    return    {
      id: this.workspaceRequestId,
      decisionReason : terminateDecisionComment.value,
    };
  }

  public transformToEntityModelCancelRequest() {
    const { cancelDecisionComment} = this.workspaceListViewModelFormControls;
    return    {
      id: this.workspaceRequestId,
      decisionReason : cancelDecisionComment.value,
    };
  }

  public requestNewWorkspace() {
    this.ngxSmartModalService.getModal(this.requestNewWorkspaceFromCatalog).open();
  }

  public deleteUserFromList() {
      this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).open();
  }

  public subscribeToCreateSuccessMessage() {
    this.workspaceService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      if (!data.isCreateAnother) {
        this.alertService.success(message, toStable);
      }
    });
  }

  private getWorkspaceTagTypeDetailById(contextId): void {
    this.workspaceTagTypeSelectedDetailsList = [];
    this.tagTypeService.getWorkspaceTagTypeDetailById(contextId).subscribe(
      data => {
        if (data) {
          const tagGropedValue = _.groupBy(data, item => {
            return item.tagValue.tagTypeId;
          });
          const formattedData = [];
          _.each(tagGropedValue, groupData => {
              const data = {
                name: groupData[0].tagValue.tagType.name,
                selectedTagValues: this.getSelectedTagValue(groupData)
              };
              formattedData.push(data);
          });
          this.workspaceTagTypeSelectedDetailsList = formattedData;
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  private getSelectedTagValue(groupData): any {
    const values = [];
    _.each(groupData, groupData => {
      values.push(groupData.tagValue.value)
    });
    return values.join(", ");
  }

  // Getting all the vendor details and redirecting to the vendor details page
  private redirectToDetail() {
    // this.workspaceService.workspaceIdSubject.next(this.workspaceId);
    // this.router
    //   .navigate(["/vendor-management/edit/" + this.workspaceId])
    //   .then()
    //   .catch();
  }
  public redirectToCreate() {
    // this.router
    //   .navigate(["/vendor-management/new"])
    //   .then()
    //   .catch();
  }
}
