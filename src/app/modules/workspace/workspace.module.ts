import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DataGridModule } from "../core-ui-components";
import { DataGridExtendedModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import {MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatFormFieldModule} from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import {  MatButtonModule} from "@angular/material/button";
import {  MatFormFieldControl} from "@angular/material/form-field";
import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";
import { AngularMultiSelectModule } from "src/app/modules/core-ui-components/angular2-multiselect-component/src/lib";

import { MatSelectModule } from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {LocalStorageManager} from "../../utils/local-storage-manager";
import { WorkspaceRoutingModule } from './workspace-routing-module';
import { WorkspaceListViewComponent } from './views/workspace-list-view/workspace-list-view.component';
import { WorkspaceAdminListViewComponent } from './views/workspace-admin-list-view/workspace-admin-list-view.component';
import { WorkspaceRequestViewComponent } from './views/workspace-request-view/workspace-request-view.component';
import { MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns';
const modules = [MatSelectModule];

@NgModule(
    {
        declarations: [
            WorkspaceListViewComponent,
            WorkspaceAdminListViewComponent,
            WorkspaceRequestViewComponent
        ],
        imports: [
            MultiSelectModule,
            SharedModules,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            WorkspaceRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatSelectModule,
            MatButtonModule,
            DataGridModule,
            DataGridExtendedModule,
            MatCheckboxModule,
            AngularMultiSelectModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [
            WorkspaceListViewComponent,
            WorkspaceAdminListViewComponent,
            WorkspaceRequestViewComponent

        ],
        providers: [
            AlertService,
            NgxSmartModalService,
            LocalStorageManager
        ]
    }
)
export class WorkspaceModule {
}
