import { Routes, RouterModule } from "@angular/router";
import { DatasetTypeListViewComponent } from "./views/role/dataset-type-list-view/dataset-type-list-view.component";
import { DatasetTypeEditDetailViewComponent } from "./views/role/dataset-type-edit-detail-view/dataset-type-edit-detail-view.component";
import { NgModule } from "@angular/core";

export const routes: Routes = [
  {
    path: "list",
    component: DatasetTypeListViewComponent
  },
  {
    path: "dataset-type/:key",
    component: DatasetTypeEditDetailViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DatasetTypeManagementRoutingModule { }
