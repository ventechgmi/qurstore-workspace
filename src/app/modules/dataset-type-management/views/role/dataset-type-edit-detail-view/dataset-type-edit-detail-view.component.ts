import { Component, OnInit } from "@angular/core";
import { DatasetTypeDetailViewModel } from "../../../viewmodels/role/dataset-type-detail.viewmodel";
import { ActivatedRoute } from "@angular/router";
// import { AuthService } from "src/app/utils/auth.service";
import { AlertService } from "../../../../../utils/alert/alert.service";
import { RoleManagementNavigationService } from "src/app/modules/core-ui-services/role-management-navigation.service";
import { ModalType } from "src/app/utils/config";

@Component({
  selector: "app-dataset-type-edit-detail-view",
  templateUrl: "./dataset-type-edit-detail-view.component.html",
  styleUrls: ["./dataset-type-edit-detail-view.component.scss"],
  providers: [DatasetTypeDetailViewModel, AlertService]
})
export class DatasetTypeEditDetailViewComponent implements OnInit {
  constructor(
    public roleManagementNavigationService: RoleManagementNavigationService,
    public datasetTypeDetailViewModel: DatasetTypeDetailViewModel,
    private router: ActivatedRoute,
    private alertService: AlertService
  ) { }

  public ngOnInit() {
    this.router.params
      .subscribe(async params => {
        if (params["key"]) {
          const data = await this.roleManagementNavigationService.navigationParametersForLink(params["key"]);
          this.datasetTypeDetailViewModel.init({ id: data.id, modalType: data.modalType });
        }
      });
  }
}
