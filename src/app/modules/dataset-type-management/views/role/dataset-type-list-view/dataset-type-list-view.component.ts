import {
    Component,
    OnInit
} from "@angular/core";
import { DatasetTypeListViewModel } from "../../../viewmodels/role/dataset-type-list.viewmodel";
import { DatasetTypeService } from "../../../services/dataset-type.service";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from "../../../../../utils/alert/alert.service";
@Component({
    selector: "app-dataset-type-list-view",
    templateUrl: "./dataset-type-list-view.component.html",
    styleUrls: ["./dataset-type-list-view.component.scss"],
    providers: [DatasetTypeListViewModel, AlertService] // need separate alert instance
})
export class DatasetTypeListViewComponent implements OnInit {

    constructor(private router: Router,
        private datasetTypeService: DatasetTypeService,
        public datasetTypeListViewModel: DatasetTypeListViewModel,
        public alertService: AlertService,
        private route: ActivatedRoute,
        public ngxSmartModalService: NgxSmartModalService
    ) {
        alertService.afterInitObs = datasetTypeService.getAfterIntObs();
    }

    public ngOnInit() {
        this.route.params
            .subscribe(params => {
                this.datasetTypeListViewModel.init();
            });
    }

    public ngOnDestroy() {
        this.datasetTypeService.showAfterIntMessage("");
    }

}
