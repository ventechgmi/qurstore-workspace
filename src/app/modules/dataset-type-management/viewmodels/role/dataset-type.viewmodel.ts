import { map } from "rxjs/operators";
import { EntityBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { DatasetTypeService } from "../../services/dataset-type.service";
import { Observable, Subject, of } from "rxjs";
import { AbstractControl, ValidatorFn } from "@angular/forms";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AuthService } from "src/app/utils/auth.service";
import { ActivatedRoute } from "@angular/router";
import { ROUTES_CONFIG_MAP, MODULE_BASE_URL_MODULE_ID_MAP } from "src/app/utils/config";
export class DatasetTypeViewModel extends EntityBaseViewModel<"", DatasetTypeService> {
    public moduleId: string;
    public moduleAccess;
    public moduleInfo;
    public updateRoleListSubject = new Subject<any>();
    public roleDetailSelection = new Subject<any>();
    auditLogPermission: any = {};

    public constructor(
        public datasetTypeService: DatasetTypeService,
        public viewModelType: ViewModelType,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute,
        public authService: AuthService
    ) {
        super(datasetTypeService, viewModelType);
        this.entityName = "dataset-type";
    }

    public getUserClaims(): any {
        const baseUrl = ROUTES_CONFIG_MAP.DATASET_TYPE_MANAGEMENT.BASE_URL;
        this.moduleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
        this.moduleInfo = this.authService.getModuleAccess(this.moduleId) || null;
        this.moduleAccess = this.moduleInfo ? this.moduleInfo.RolePermissions : {};
        return this.moduleAccess;
    }

    
}
