import { from as observableFrom, Observable } from "rxjs";
import { DatasetTypeService } from "../../services/dataset-type.service";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { AlertService } from "../../../../utils/alert/alert.service";
import {
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";
import { DataGridExtendedViewModel } from "../../../core-ui-components";
import { GridConfig, ModalType, ROUTES_CONFIG_MAP } from "../../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ValidationMessages } from '../../helpers/validation-messages';
import { AuthService } from 'src/app/utils/auth.service';
import { DatasetTypeViewModel } from './dataset-type.viewmodel';
import { GuidHelper } from 'src/app/modules/employee-dashboard/helpers/guid-helper';
import { validateSpecialCharactersAndWhiteSpace } from "src/app/utils/validators/whitespace-validatore";
import { NgxUiLoaderService } from "ngx-ui-loader";

@Injectable()
export class DatasetTypeDetailViewModel extends DatasetTypeViewModel {

  public headerTitle: string;
  public datasetTypeId: string;
  public datasetTypeForm: FormGroup;
  public datasetTypePermission: any;
  public modalType: ModalType;
  public isEditFromList = false;
  public datasetTypePristineValue: any;
  public cancelConfirmModal = "cancelConfirmModal";


  private errorDictionary: Map<string, any[]> = new Map();
  public constructor(
    public datasetTypeService: DatasetTypeService,
    private router: Router,
    private fb: FormBuilder,
    private alertService: AlertService,
    public ngxSmartModalService: NgxSmartModalService,
    public authService: AuthService,
    public route: ActivatedRoute,
    private ngxService: NgxUiLoaderService
  ) {
    super(datasetTypeService, ViewModelType.ReadOnly, ngxSmartModalService, route, authService);
    this.initForm();
  }

  public init(params?): Observable<any> {
    this.modalType = params.modalType;
    this.datasetTypePermission = this.getUserClaims() || {};
    this.initErrorDirectoryDetails();

    switch (this.modalType) {
      case ModalType.CREATE: {
        this.headerTitle = ValidationMessages.CREATE_TITLE;
        break;
      }
      case ModalType.EDIT: {
        this.headerTitle = ValidationMessages.EDIT_TITLE;
        this.isEditFromList = true;
        this.datasetTypeId = params.id;
        this.fetchDatasetType(this.datasetTypeId);
        break;
      }
      case ModalType.DETAIL: {
        this.headerTitle = ValidationMessages.DETAIL_TITLE;
        this.toggleFieldVisibility(true);
        this.datasetTypeId = params.id;
        this.fetchDatasetType(this.datasetTypeId);
        break;
      }
      default: { }
    }
    return observableFrom([]);
  }

  public onCancel(): void {
    if (this.modalType === ModalType.EDIT || this.modalType === ModalType.CREATE) {
      if (this.datasetTypeForm.dirty) {
        this.ngxSmartModalService.getModal(this.cancelConfirmModal).open();
      } else if (this.isEditFromList) {
        this.router.navigateByUrl(ROUTES_CONFIG_MAP.DATASET_TYPE_MANAGEMENT.FULL_URL);
      } else if (this.modalType === ModalType.EDIT) {
        this.detailMode();
      } else {
        this.router.navigateByUrl(ROUTES_CONFIG_MAP.DATASET_TYPE_MANAGEMENT.FULL_URL);
      }
    } else {
      this.router.navigateByUrl(ROUTES_CONFIG_MAP.DATASET_TYPE_MANAGEMENT.FULL_URL);
    }
  }

  public onEdit(): void {
    this.isEditFromList = false;
    this.modalType = ModalType.EDIT;
    this.toggleFieldVisibility(false);
    this.headerTitle = ValidationMessages.EDIT_TITLE;
  }

  public saveChanges(): void {
    if (this.datasetTypeForm.invalid) {
      return;
    }
    const controls = this.datasetTypeForm.controls;
    if(controls.securedFolderPath.value[0] != "/") {
      controls.securedFolderPath.setErrors({shouldHaveSlash: true});
      return;
    }
    const dbObj: any = {
      Name: controls.name.value,
      SecuredFolderPath: controls.securedFolderPath.value,
      Description: controls.description.value,
      FilePattern: controls.filePattern.value,
    }
    if (this.datasetTypeId) {
      dbObj.Id = this.datasetTypeId;
    }
    this.ngxService.start();
    this.datasetTypeService.saveDatasetType(dbObj)
      .subscribe(data => {
        this.datasetTypeService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
        this.router.navigateByUrl(ROUTES_CONFIG_MAP.DATASET_TYPE_MANAGEMENT.FULL_URL);
        this.ngxService.stop();
      }, error => {
        this.alertService.error(error);
        this.ngxService.stop();
      })
  }

  public onConfirm(): void {
    if (this.modalType === ModalType.CREATE || this.modalType === ModalType.EDIT && this.isEditFromList) {
      this.router.navigateByUrl(ROUTES_CONFIG_MAP.DATASET_TYPE_MANAGEMENT.FULL_URL);
      return;
    }
    this.detailMode()
  }

  public onPopUpClose(): void {
    this.ngxSmartModalService.getModal(this.cancelConfirmModal).close();
  }

  public verifyConnection(): void {
    if (!this.datasetTypeForm.controls.securedFolderPath.value) { return; }
    if(!this.datasetTypeForm.controls.securedFolderPath.value.trim()) { return; }
    if(this.datasetTypeForm.controls.securedFolderPath.value[0] != "/") {
      this.datasetTypeForm.controls.securedFolderPath.setErrors({shouldHaveSlash: true});
      return;
    }
    this.ngxService.start();
    this.datasetTypeService.verifyFolderPath(this.datasetTypeForm.controls.securedFolderPath.value)
      .subscribe(data => {
        if (data.payload) {
          this.alertService.success(data.message, true);
        } else {
          this.alertService.info(data.message, true);
        }
        this.ngxService.stop();
      }, error => {
        this.alertService.error(error, true);
        this.ngxService.stop();
      })
  }

  public errorFor(propertyName: string): string | void {
    if (propertyName) {
      if (this.datasetTypeForm.controls[propertyName] && this.datasetTypeForm.controls[propertyName].errors) {
        // Let's just show the first error message for now
        return this.makeErrorMessageFor(propertyName, Object.keys(this.datasetTypeForm.controls[propertyName].errors)[0]);
      }
    }
  }

  //private methods
  private toggleFieldVisibility(isDisabled: boolean): void {
    if (isDisabled) {
      this.datasetTypeForm.disable();
    } else {
      this.datasetTypeForm.enable();
    }
  }

  private detailMode() {
    this.ngxSmartModalService.getModal(this.cancelConfirmModal).close();
    this.datasetTypeForm.patchValue(this.datasetTypePristineValue);
    this.toggleFieldVisibility(true);
    this.modalType = ModalType.DETAIL;
    this.headerTitle = ValidationMessages.DETAIL_TITLE;
  }

  private makeErrorMessageFor(propertyName: string, errorKey: string): string {
    return this.errorDictionary.get(propertyName).filter(data => data.key === errorKey)[0].message;
  }

  private initForm(): void {
    this.datasetTypeForm = this.fb.group({
      name: ["", [Validators.required, validateSpecialCharactersAndWhiteSpace(3, 100)]],
      description: ["", []],
      securedFolderPath: ["", [Validators.required, validateSpecialCharactersAndWhiteSpace(0, 250)]],
      filePattern: ["", [Validators.required, validateSpecialCharactersAndWhiteSpace(0, 250)]]
    });
  }

  private fetchDatasetType(id: string): void {
    this.ngxService.start();
    this.datasetTypeService.getDatasetTypeById(id)
      .subscribe(data => {
        this.datasetTypePristineValue = data.payload;
        this.datasetTypeForm.patchValue(data.payload);
        this.ngxService.stop();
      },
        error => {
          this.alertService.error(error);
          this.ngxService.stop();
        })
  }

  private initErrorDirectoryDetails(): void {
    this.errorDictionary.set("name", [
      { key: "required", message: ValidationMessages.NAME_REQUIRED },
      { key: "minlength", message: ValidationMessages.NAME_MIN_LENGTH },
      { key: "maxlength", message: ValidationMessages.NAME_MAX_LENGTH },
      { key: "hasOnlyWhiteSpace", message: ValidationMessages.NAME_WHITESPACE_ERROR },
      { key: "hasOnlySpecialCharacters", message: ValidationMessages.NAME_SPECIAL_CHARACTER_ERROR }
    ]);

    this.errorDictionary.set("securedFolderPath", [
      { key: "required", message: ValidationMessages.S3_FOLDER_PATH_REQUIRED },
      { key: "maxlength", message: ValidationMessages.FOLDER_PATH_MAX_LENGTH },
      { key: "hasOnlyWhiteSpace", message: ValidationMessages.FOLDER_PATH_WHITESPACE_ERROR },
      { key: "shouldHaveSlash", message: ValidationMessages.FOLDER_PATH_NO_SLASH_ERROR },
      { key: "hasOnlySpecialCharacters", message: ValidationMessages.FOLDER_PATH_SPECIAL_CHARACTER_ERROR }
    ]);

    this.errorDictionary.set("filePattern", [
      { key: "required", message: ValidationMessages.FILE_PATTERN_REQUIRED },
      { key: "maxlength", message: ValidationMessages.FILE_PATTERN_MAX_LENGTH },
      { key: "hasOnlyWhiteSpace", message: ValidationMessages.FILE_PATTERN_WHITESPACE_ERROR },
      { key: "hasOnlySpecialCharacters", message: ValidationMessages.FILE_PATTERN_SPECIAL_CHARACTER_ERROR }
    ]);
  }


}

