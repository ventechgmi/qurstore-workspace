import { from as observableFrom, Observable } from "rxjs";
import { AlertService } from "../../../../utils/alert/alert.service";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { DatasetTypeViewModel } from "./dataset-type.viewmodel";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../../core-ui-components";
import config, { GridConfig, ModalType } from "../../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AuthService } from "src/app/utils/auth.service";
import { RoleManagementNavigationService } from "src/app/modules/core-ui-services/role-management-navigation.service";
import { DatasetTypeService } from "../../services/dataset-type.service";
import { DataComparatorHelper } from "src/app/utils/helper/date-comparator.helper";
import moment from "moment";
import { DatasetTypeDetailsRoute } from "../../helpers/dataset-type.constance";

@Injectable()
export class DatasetTypeListViewModel extends DatasetTypeViewModel {

  public ModalType = ModalType;
  public datasetTypePermission: any;
  public viewModelDataGrid: DataGridViewModel;
  public deleteConfirmModalFromList = "deleteConfirmModalFromList";
  public datasetTypeHeader = [
    { headerName: "Name", field: "name", isClickable: true },
    { headerName: "Description", field: "description" },
    { headerName: "Folder", field: "securedFolderPath" },
    { headerName: "File Pattern", field: "filePattern" },
    { headerName: "Last Modified By", field: "lastModifiedByNavigation.fullName" },
    { headerName: "Last Modified On", field: "lastModifiedDateFormatted", isDate: true, sort: "desc" }
  ];


  private organizationId: string;
  private datasetTypeId: string;

  public constructor(
    public datasetTypeService: DatasetTypeService,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public roleManagementNavigationService: RoleManagementNavigationService,
    public authService: AuthService,
    public alertService: AlertService
  ) {
    super(datasetTypeService, ViewModelType.Edit, ngxSmartModalService, route, authService);
    this.viewModelDataGrid = new DataGridViewModel();
  }

  public init(): Observable<any> {
    this.organizationId = this.authService.getOrganizationId()  ;
    this.datasetTypePermission = this.getUserClaims() || {};
    this.viewModelDataGrid.setPagination(true);
    this.viewModelDataGrid.setHeader(this.datasetTypeHeader, false, { canUpdate: this.datasetTypePermission.canUpdate, canDelete: this.datasetTypePermission.canDelete });
    this.setDataTableConfigurations();
    this.fetchDatasetTypesList();
    this.subscribers();
    return observableFrom([]);
  }

  public deleteDatasetType(): void {
    this.datasetTypeService.deleteDatasetType(this.datasetTypeId)
    .subscribe(data => {
      this.onPopUpClose();
      if(data.isSuccess) {
        this.alertService.success(data.message, true);
        this.fetchDatasetTypesList();
      } else {
        this.alertService.error(data.message, true);
      }
    }, error => this.alertService.error(error, true));
  }

  public onPopUpClose(): void {
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
    this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).close();
  }
  
  public redirectToDetail(modalType: string): void {
    this.roleManagementNavigationService.navigateUsingProxy({
      url: DatasetTypeDetailsRoute,
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { id: this.datasetTypeId, modalType }
    }).catch();
  }

  // private methods
  private setDataTableConfigurations(): void {
    this.viewModelDataGrid.dataTableConfiguration = {
      destroy: true,
      columnDefs: [{ targets: [0], visible: false, searchable: false }, { type: "num-html", targets: [2] }, { orderable: false, targets: 2, searchable: false }],
      order: [[5, "asc"]]
    };
  }
  private fetchDatasetTypesList(): void {
    this.datasetTypeService.getDatasetTypes(this.organizationId)
      .subscribe(data => {
        this.viewModelDataGrid.setRowsData(this.format(data.payload), false);
      });
  }

  private subscribers(): void {
    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.columnClicked:
          this.datasetTypeId = e.eventData.rowDetails.id;
          this.redirectToDetail(ModalType.DETAIL);
          break;
        case GridConfig.editClicked:
          this.datasetTypeId = e.eventData.rowDetails.id;
          this.redirectToDetail(ModalType.EDIT);
          break;
        case GridConfig.deleteClicked:
          this.datasetTypeId = e.eventData.rowDetails.id;
          this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
          this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).open();
          break;
        default:
          if (e.eventData && e.eventData.rowDetails) {

          }
          break;
      }
    }, error => {
      this.alertService.error(error, true);
    });
  }

  private format(datasetTypeList: any[]): any[] {
    _.each(datasetTypeList, (datasetType) => {
      datasetType.lastModifiedDateFormatted = moment(datasetType.lastModifiedDate).format(config.FULL_DATE_FORMAT);
    });
    return datasetTypeList;
  }


}
