export class ValidationMessages {

    public static readonly CREATE_TITLE = "Create Dataset Type";
    public static readonly EDIT_TITLE = "Edit Dataset Type";
    public static readonly DETAIL_TITLE = "Dataset Type Detail";
    public static readonly NAME_REQUIRED = "Name is required";
    public static readonly NAME_MIN_LENGTH = "Name should have at least 3 character.";

    public static readonly NAME_MAX_LENGTH = "Name should not exceed 100 characters.";
    public static readonly NAME_WHITESPACE_ERROR = "Name has only whitespace.";
    public static readonly NAME_SPECIAL_CHARACTER_ERROR = "Name has only special characters.";

    public static readonly FOLDER_PATH_MAX_LENGTH = "Folder path should not exceed 100 characters.";
    public static readonly FOLDER_PATH_WHITESPACE_ERROR = "Folder path has only whitespace.";
    public static readonly FOLDER_PATH_NO_SLASH_ERROR = "Folder path should start with forward slash(/).";
    public static readonly FOLDER_PATH_SPECIAL_CHARACTER_ERROR = "Folder path has only special characters.";

    public static readonly FILE_PATTERN_MAX_LENGTH = "File pattern should not exceed 100 characters.";
    public static readonly FILE_PATTERN_WHITESPACE_ERROR = "File pattern has only whitespace.";
    public static readonly FILE_PATTERN_SPECIAL_CHARACTER_ERROR = "File pattern has only special characters.";

    public static readonly S3_FOLDER_PATH_REQUIRED = "S3 Folder path is required";
    public static readonly FILE_PATTERN_REQUIRED = "File pattern is required";
   
   
}
