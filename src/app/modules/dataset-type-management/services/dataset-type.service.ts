
import { map } from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";

const appConfigLocal = require("../../../app-config.json");
@Injectable({
    providedIn: "root"
})
export class DatasetTypeService extends EntityService {

    public alertInfo = { message: "", type: "" };
    private afterInitSubject = new BehaviorSubject(this.alertInfo);

    constructor(
        http: HttpClient,
        @Inject(APP_CONFIG) appConfig: AppConfig
    ) {
        super(http, "dataset-type", appConfig);
        this.appConfig = appConfig;
    }

    public showAfterIntMessage(messageInfo): void {
        this.afterInitSubject.next(messageInfo);
    }

    public getAfterIntObs(): Observable<any> {
        return this.afterInitSubject.asObservable();
    }

    public deleteDatasetType(id: string): Observable<any> {
        const requestURL = this.getApi() + "/" + id;
        return this.http.delete(requestURL);
    }

    public getDatasetTypes(organizationId: string): Observable<any> {
        return this.generateGetAPI("/organization/", organizationId);
    }

    public getDatasetTypeById(id: string): Observable<any> {
        return this.generateGetAPI("/", id);
    }

    public saveDatasetType(dbObj: any): Observable<any> {
        return this.generatePostAPI("/save/", dbObj);
    }

    public verifyFolderPath(folderPath: string): Observable<any> {
        return this.generatePostAPI("/verify-connection/", { SecuredFolderPath: folderPath });
    }

    private generateGetAPI(name: string, req: any): Observable<any> {
        const requestURL = this.getApi() + name + req;
        return this.http
            .get(requestURL).pipe(
                map(res => {
                    return res;
                }));
    }

    private generatePostAPI(name: string, data: any): Observable<any> {
        return this.http
            .post(this.getApi() + name, data).pipe(
                map((res: Response) => res));
    }

}
