import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { DatasetTypeListViewComponent } from "./views/role/dataset-type-list-view/dataset-type-list-view.component";
import { DatasetTypeEditDetailViewComponent } from "./views/role/dataset-type-edit-detail-view/dataset-type-edit-detail-view.component";
import { DataGridModule, DataGridExtendedModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import { DatasetTypeManagementRoutingModule } from "./dataset-type-management-routing-module";
import {MatAutocompleteModule } from "@angular/material/autocomplete";
import {MatInputModule} from "@angular/material/input";
import { MatFormFieldModule} from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import {  MatButtonModule} from "@angular/material/button";
import { SharedModules } from "../../utils/shared-module/shared.modules";

@NgModule(
    {
        declarations: [
            DatasetTypeListViewComponent,
            DatasetTypeEditDetailViewComponent
        ],
        imports: [
            SharedModules,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            DatasetTypeManagementRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatButtonModule,
            DataGridModule,
            DataGridExtendedModule,
            MatCheckboxModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [],
        providers: [
            NgxSmartModalService
        ]
    }
)
export class DatasetTypeManagementModule {

}
