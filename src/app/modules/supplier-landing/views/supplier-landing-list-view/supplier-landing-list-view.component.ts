import {
    Component,
    OnInit,
    ContentChild,
    AfterViewInit,
    ViewChild,
    ElementRef,
    AfterContentInit
} from "@angular/core";


import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormBuilder } from "@angular/forms";
// import { AuthService } from "src/app/utils/auth.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { SupplierLandingListViewModel } from '../../viewmodels/supplier-landing-list.viewmodel';
import { SupplierLandingService } from '../../services/supplier-landing.service';
import { debug } from 'console';


import * as HighCharts from "highcharts";



@Component({
    selector: "app-supplier-landing-list-view",
    templateUrl: "./supplier-landing-list-view.component.html",
    styleUrls: ["./supplier-landing-list-view.component.scss"],
    providers: [ SupplierLandingListViewModel, AlertService] // need separate alert instance
})
export class SupplierLandingListViewComponent implements OnInit, AfterContentInit {
//    title = 'Angular 9 HighCharts';
    @ViewChild('fileUploader') fileUploader : ElementRef;
    public dataTable: any;
    projectId: any;
    constructor(private router: Router,
                private supplierLandingService: SupplierLandingService,
                public supplierLandingListViewModel: SupplierLandingListViewModel,
                public alertService: AlertService,
                private route: ActivatedRoute,
                public ngxSmartModalService: NgxSmartModalService
    ) {
        alertService.afterInitObs = supplierLandingService.getAfterIntObs();
    }

    public uploadFilePopupTS() {
        this.fileUploader.nativeElement.value = null;
        this.supplierLandingListViewModel.uploadFilePopup();
      }

    public  ngOnInit() {
        this.route.params
            .subscribe(params => {
               this.supplierLandingListViewModel.initServices(this.alertService);
               this.supplierLandingListViewModel.init();
            });

        this.route.queryParams.subscribe(res => {
            this.projectId = res.id;
        });
        this.supplierLandingListViewModel.getYTDUploadedFiles();
        this.supplierLandingListViewModel.getThreeMonthsUploadedFiles();
    }

    public ngAfterContentInit() {
    }

}
