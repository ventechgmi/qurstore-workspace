import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DataGridModule } from "../core-ui-components";
import { DataGridExtendedModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import {MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatFormFieldModule} from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import {  MatButtonModule} from "@angular/material/button";
import {  MatFormFieldControl} from "@angular/material/form-field";
import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";
import { MatSelectModule } from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {LocalStorageManager} from "../../utils/local-storage-manager";

import { SupplierLandingRoutingModule } from './supplier-landing-routing-module';
import { SupplierLandingService } from './services/supplier-landing.service';
import { SupplierLandingListViewComponent } from './views/supplier-landing-list-view/supplier-landing-list-view.component';
import { SupplierMappingListViewComponent } from './views/supplier-mapping-list-view/supplier-mapping-list-view.component';
//import {MatDatepickerModule} from "@angular/material/datepicker";

const modules = [MatSelectModule];
@NgModule(
    {
        declarations: [
            SupplierLandingListViewComponent,
            SupplierMappingListViewComponent
        ],
        imports: [
            SharedModules,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            SupplierLandingRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatSelectModule,
            MatButtonModule,
            DataGridModule,
            DataGridExtendedModule,
            MatCheckboxModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [
            SupplierLandingListViewComponent,
            SupplierMappingListViewComponent
        ],
        providers: [
            AlertService,
            SupplierLandingService,
            NgxSmartModalService,
            LocalStorageManager
        ]
    }
)
export class SupplierLandingModule {

}
