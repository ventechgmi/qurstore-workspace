import {from as observableFrom,  Observable, Subject, forkJoin } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { Injectable, OnInit, ChangeDetectorRef, ElementRef } from "@angular/core";
import {  ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
// import { AuthService } from "src/app/utils/auth.service";
import { SupplierLandingViewModel } from './supplier-landing.viewmodel';
import { SupplierLandingService } from '../services/supplier-landing.service';
import { ValidationMessages } from '../helpers/validation-messages';
import { Component, ViewChild } from '@angular/core';
import * as HighCharts from 'highcharts';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/utils/auth.service';

@Injectable()
export class SupplierLandingListViewModel extends SupplierLandingViewModel {
  public userId: string;
  public supplierMappingId: string;
  organizationId = '';
  public supplierMappingDefinition: any;
  public uploadedFilesList: any;
  private dashboardWidgetYTD: any;
  private dashboardWidgetLast3Months: any;
  public alertService: AlertService;
  viewModelDataGrid: DataGridViewModel;
  viewModelDataGridSupplierMapping: DataGridViewModel;
  private uploadFileEntity: any;
  private dataProcessEntity: any;
  private mappingDefinitionId: any;
  private datasetTypeId: any;
  private dauID: any;
  appliedFilters: any[];
  targetDefinitionWithoutUserList: any;

  public mappingName: string;
  public dataUsageAgreementList: any;
  public targetMappingList: any;
  submissionDate : Date;
  header = [
    { headerName: "File Name", field: "uploadedFileName", isClickable: false },
    { headerName: "Target Name", field: "targetDefinitionName", isClickable: false },
    { headerName: "Mapping Definition", field: "mappingDefinitionName", isClickable: false },
    { headerName: "Submission On", field: "submissionOnDisplay", isClickable: false },
    { headerName: "Uploaded On", field: "uploadedOnDisplay", isClickable: false },
    { headerName: "Data Process Status", field: "uploadedFileStatus", isClickable: false },
    { headerName: "Processed On", field: "processedOnDisplay", isClickable: false },
  ];
  // { headerName: "# of Total Records", field: "totalRecordCount", isClickable: false },
  // { headerName: "# of Records Processed", field: "processedRecordCount", isClickable: false },
  // { headerName: "# of Records Failed", field: "errorRecordCount", isClickable: false },
  //  { headerName: "Approval Status", field: "approvalStatus", isClickable: false },
  //  { headerName: "Approval On", field: "aprvalRejectDateDisplay", isClickable: false }

  headerSupplierMapping = [
    { headerName: "Source Columns", field: "sourceColumnName", isClickable: false },
    { headerName: "Split/Merge", field: "targetDefinitionName", isClickable: false },
    { headerName: "Data Mapping", field: "mappingDefinitionName", isClickable: false },
    { headerName: "Transformation", field: "uploadedOnDisplay", isClickable: false },
    { headerName: "Target Columns", field: "targetColumnName", isClickable: false },
    { headerName: "Format", field: "totalRecordCount", isClickable: false },
    { headerName: "Accepted Values", field: "defaultValue", isClickable: true },
    { headerName: "Sequence Number", field: "sequenceNumber", isHide: true }
  ];
  file: any;
  errorFileUpload = false;
  public supplierLandingForm: FormGroup;

  public constructor(
    public supplierLandingService: SupplierLandingService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public authService: AuthService
  ) {
   super(supplierLandingService, ViewModelType.Edit, ngxSmartModalService,  route);
  }

  public init() {
    this.userId = this.authService.getUserId();
    this.viewModelDataGrid = new DataGridViewModel();
    this.viewModelDataGrid.setHeader(this.header, false, { canUpdate: false, canDelete: false});

    this.organizationId = this.authService.getOrganizationId();
    this. viewModelDataGridSupplierMapping = new DataGridViewModel();
    this.viewModelDataGridSupplierMapping.setHeader(this.headerSupplierMapping, false, { canUpdate: false, canDelete: false});
    this.viewModelDataGridSupplierMapping.defaultSortModel = [
      {
        colId: 'sequenceNumber',
        sort: 'asc',
      }
    ];
    this.subscribers();
    this.getAllUploadedFilesForGrid();
    this.getSupplierMappingDefinition();
   // this.getMappingDefinitionDetails();
    this.submissionDate = new Date();
    this.supplierLandingForm = this.fb.group({
      submissionDate: '',
      description: ''
    });
    this.initDropDown();
    return observableFrom([]);
  }

  public initDropDown() {
   this.getDataForDataUsageAgreement();
   this.getDataForTargetMappingList();


 }
 private getDataForDataUsageAgreement() {
  this.supplierLandingService.getDUAList().subscribe(
    data => {
      if (data) {
        this.dataUsageAgreementList = data;
      }
    },
    error => {
      this.alertService.error(error);
    }
  );
 }
 private getDataForTargetMappingList() {
 this.supplierLandingService.getTargetMappingList().subscribe(
   data => {
     if (data) {
       this.targetMappingList = data;
     }
   },
   error => {
     this.alertService.error(error);
   }
 );
}


 get supplierLandingFormControls() {
    return this.supplierLandingForm.controls;
  }

  public async getAllUploadedFilesForGrid() {
    setInterval(() => {
      this.getAllUploadedFiles();
    }, 10000);
  }
  public async getAllUploadedFiles() {
    this.supplierLandingService.getAllUploadedFiles(this.organizationId).subscribe(
      data => {
        if (data) {
          this.uploadedFilesList = data;
          this.initGrid(this.uploadedFilesList);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }


  private subscribers() {
    this.viewModelDataGridSupplierMapping.events.subscribe(e => {
        switch (e.eventType) {
          case GridConfig.columnClicked:
            this.supplierMappingId = e.eventData.rowDetails[0];
            this.viewAcceptedValueClicked();
            break;
          default:
                break;
        }
    },  error => {
      console.log("Error: " + error );
    });
  }
  public showStateCode() {
    this.ngxSmartModalService.getModal("stateCodeReferenceValue").open();
  }

  public showTestName() {
    this.ngxSmartModalService.getModal("testNameReferenceValue").open();
  }
  public showFacilityType() {
    this.ngxSmartModalService.getModal("showFacilityTypeReferenceValue").open();
  }

  private viewAcceptedValueClicked() {
    const row = this.supplierMappingDefinition.filter(x => x.id === this.supplierMappingId);
    if (row && row.length > 0) {
      const targetColumnName  = row[0].targetColumnName;
      if (targetColumnName === "state_code") {
          this.showStateCode();
      } else if (targetColumnName === "test_name") {
        this.showTestName();
      }  else if (targetColumnName === "facility_type") {
        this.showFacilityType();
      }
    }
  }
  public getMappingDefinitionDetails() {
    // this.mappingDefinitionId = "4BB9E4DB-6E17-4DCB-91D5-37C7B12A1913";
    this.supplierLandingService.getMappingDataInformation(this.organizationId).subscribe(
      data => {
        if (data) {
          this.mappingDefinitionId = data.id;
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }



  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }
  public uploadFilePopup() {
    // this.myInputVariable.nativeElement.value = "";
    // this.myInputVariable.nativeElement.value = null;
    this.ngxSmartModalService.getModal("uploadFile").open();
  }

  public fileChanged(e) {
    this.file = e.target.files[0];
    this.changeFileTypeDropdown();
  }
  private changeFileTypeDropdown() {
    const fileNameArray = this.file.name.split(".");
    const extensionName = fileNameArray[fileNameArray.length - 1];
    if (extensionName.toUpperCase() !== "CSV" && extensionName.toUpperCase() !== "XLSX") {
      this.file = "";
      this.errorFileUpload = true;
      // this.alertService.error(ValidationMessages.FileNotUploaded, false);
    } else {
      this.errorFileUpload = false;
    }
  }

  ytdColumnChart(noData) {
    HighCharts.chart('ytdTotalSubmission', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      credits: {
         enabled: false
       },
       colors: ['#2f7ed8', '#0d233a', '#FF0000'],
      xAxis: {
        categories: this.getYTDXAxis(), // ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number of Records',
          align: 'middle'
        },
      },
      tooltip: {
        valueSuffix: ''
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      series: this.buildYTD(noData)
    } );
  }
//

  private buildYTD(noData) {
    if(noData) {
      return [];
    } else {
      const columnDataBuilder =  [{
        type: undefined,
        name: 'Total',
        data: this.getYTDTotalStatistics()
      }, {
        type: undefined,
        name: 'Processed',
        data: this.getYTDProcessStatistics()
      }, {
        type: undefined,
        name: 'Failed',
        data: this.getYTDFailedStatistics()
      }
    ];
    return columnDataBuilder;
    }

  }

  private getYTDTotalStatistics() {
    const ytdTotal: number[] = [];
    this.dashboardWidgetYTD?.forEach(element => {
      ytdTotal.push(element.totalRecordCount);
    });
    return ytdTotal;
  }
  private getYTDProcessStatistics() {
    const ytdProcess: number[] = [];
    this.dashboardWidgetYTD?.forEach(element => {
      ytdProcess.push(element.processedRecordCount);
    });
    return ytdProcess;
  }
  private getYTDFailedStatistics() {
    const ytdFailed: number[] = [];
    this.dashboardWidgetYTD?.forEach(element => {
      ytdFailed.push(element.errorRecordCount);
    });
    return ytdFailed;
  }
  public getYTDXAxis() {
    const ytdFailed: string[] = [];
    this.dashboardWidgetYTD?.forEach(element => {
      ytdFailed.push(element.monthName + " - " + element.yearNumber);
    });
    return ytdFailed;
  }

  private get3MonthsTotalStatistics() {
    const ytdTotal: number[] = [];
    this.dashboardWidgetLast3Months?.forEach(element => {
      ytdTotal.push(element.totalRecordCount);
    });
    return ytdTotal;
  }
  private get3MonthsProcessStatistics() {
    const ytdProcess: number[] = [];
    this.dashboardWidgetLast3Months?.forEach(element => {
      ytdProcess.push(element.processedRecordCount);
    });
    return ytdProcess;
  }
  private get3MonthsFailedStatistics() {
    const ytdFailed: number[] = [];
    this.dashboardWidgetLast3Months?.forEach(element => {
      ytdFailed.push(element.errorRecordCount);
    });
    return ytdFailed;
  }
  private get3MonthsXAxis() {
    const ytdFailed: string[] = [];
    this.dashboardWidgetLast3Months?.forEach(element => {
      ytdFailed.push(element.monthName + " - " + element.yearNumber);
    });
    return ytdFailed;
  }

  public mappingDefinitionChanged(e) {
    this.mappingDefinitionId = e.target.value;
    this.mappingName = e.target.options[e.target.options.selectedIndex].text;
  }
  public datasetTypeChanged(e) {
    this.datasetTypeId = e.target.value;
  }
  public dataUsageAgreementChanged(e) {
    this.dauID = e.target.value;
  }

  lastThreeMonths(noDataForLastThreeMonths) {
    HighCharts.chart('last3Months', {
      chart: {
        type: 'line'
      },
      title: {
        text: ''
      },
      accessibility: {
        announceNewData: {
            enabled: true
        }
      },
      credits: {
         enabled: false
       },
       colors: ['#2f7ed8', '#0d233a', '#FF0000'],
       xAxis: {
        categories: this.get3MonthsXAxis()
    },
      yAxis: {
        title: {
          text: 'Number of Records'
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },
      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
      },
      plotOptions: {
        series: {
          label: {
            connectorAllowed: false
          }
        }
      },
      series: this.buildLastThreeMonths(noDataForLastThreeMonths)
    }
  );
  }

  private buildLastThreeMonths(noData) {
    if (noData) {
      return [];
    } else {
      const columnDataBuilder = [{
        type: undefined,
        name: 'Total',
        data: this.get3MonthsTotalStatistics()
      }, {
        type: undefined,
        name: 'Processed',
        data: this.get3MonthsProcessStatistics()
      }, {
        type: undefined,
        name: 'Failed',
        data: this.get3MonthsFailedStatistics()
      }];
    return columnDataBuilder;
    }
  }

  private getLast3MonthsName() {
    const currentDate = new Date();
    currentDate.setMonth(currentDate.getMonth() - 2);
    const results: string[] = [];
    for (let i = 0 ; i < 3; i++) {
        const tempHolder = currentDate.toLocaleString('en-us', { month: 'short' }); /* Jun */
        const year = currentDate.getFullYear();
        results.push(tempHolder + '-' + year);
        currentDate.setMonth(currentDate.getMonth() + 1 );
    }
    return  results;
  }
  public saveAndUploadFile() {
    if (this.file) {
      this.transformToEntityModel();
      this.uploadFileToActivity();
      this.getAllUploadedFiles();
    }
  }

  public uploadFileToActivity() {
    this.supplierLandingService.saveFileContent(this.uploadFileEntity, this.file).subscribe(data => {
        if (data?.id && data.isSuccess) {
          this.uploadFileEntity.fileUploadId = data.id;

          this.saveDataProcess(data.id);
          this.ngxSmartModalService.getModal("uploadFile").close();
        } else {
          this.alertService.error(ValidationMessages.ErrorOccurred, false);
        }
    }, error => {
      if (error?.error?.errorList && error.error.errorList.length > 0) {
        this.alertService.error(error.error.errorList[0], false);
      } else if (error.error) {
        this.alertService.error(error.error, false);
      }
       else  {
        this.alertService.error(ValidationMessages.ErrorOccurred, false);
      }
    });
  }

  public saveDataProcess(fileUploadId) {
    const { submissionDate } = this.supplierLandingFormControls;
    this.dataProcessEntity = {
      fileUploadId,
      submissionDate: submissionDate.value,
      MappingDefinitionId:  this.mappingDefinitionId, // 'EC5ABFA6-6B0F-42E1-B8BA-81962BABDE2A',
      RefFileProcessStatusId: '245EFA44-AD25-494F-90F5-6AB9AB111964',
      DataUsageAgreementId: this.dauID,
      lastModifiedBy: this.authService.getUserId()
    };

    this.supplierLandingService.saveDataProcess(this.dataProcessEntity).subscribe(result => {
      if (result?.id && result.isSuccess) {
        this.alertService.success(ValidationMessages.SuccessFileUpload, false);
      } else {
        this.alertService.error(ValidationMessages.ErrorOccurred, false);
      }
      this.getAllUploadedFiles();
    }, error => {
      if (error?.error?.errorList && error.error.errorList.length > 0) {
        this.alertService.error(error.error.errorList[0], false);
      } else if (error.error) {
        this.alertService.error(error.error, false);
      }
      else  {
        this.alertService.error(ValidationMessages.ErrorOccurred, false);
      }
    });
  }


  public transformToEntityModel() {
    const fileNameArray = this.file.name.split(".");
    const extensionName = fileNameArray[fileNameArray.length - 1].toUpperCase();
    // if (extensionName.toUpperCase() !== "CSV" && extensionName.toUpperCase() !== "XLSX") {
    const { description } = this.supplierLandingFormControls;
    this.uploadFileEntity = {
      file_type: extensionName,
      file_header_row_indicator: '1',
      delimiter: ',',
      lastModifiedBy: this.authService.getUserId(),
      uploadedFileDataType: "DATA",
      organizationName :  this.authService.getOrganizationName(),
      fileDescription: description.value,
      datasetTypeId: this.datasetTypeId,
      dataUsageAgreementId: this.dauID
    };
    if (this.file) {
      this.uploadFileEntity.file_name = this.file.name;
    }
    return this.uploadFileEntity;
  }



  public getSupplierMappingDefinition() {
    this.supplierLandingService.getSupplierMappingDefinition(this.organizationId).subscribe(
      data => {
        if (data) {
          this.supplierMappingDefinition = data;
          this.initGridSupplierMapping(this.supplierMappingDefinition);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public getYTDUploadedFiles() {
    const endDate = new Date();
    const currentYear = endDate.getFullYear();
    endDate.setDate(endDate.getDate() + 1);
    const startDate = new Date(currentYear, 0, 1);

    const filterCriteriaForDashboard = {organizationId: this.organizationId, startDateRange: startDate, endDateRange: endDate };
    this.supplierLandingService.getUploadedFilesByRange(filterCriteriaForDashboard).subscribe(
      data => {
        if (data) {
          this.dashboardWidgetYTD = data;
          let noData;
          if (this.dashboardWidgetYTD?.length > 0){
            noData = false ;
          } else {
            noData = true;
          }
          this.ytdColumnChart(noData);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
  public getThreeMonthsUploadedFiles() {
    const startDate = new Date();
    const endDate = new Date();
    const currentYear = endDate.getFullYear();
    endDate.setDate(endDate.getDate() + 1);
    startDate.setMonth(endDate.getMonth() - 3);

    const filterCriteriaForDashboard = {organizationId: this.organizationId, startDateRange: startDate, endDateRange: endDate };
    // const dateRange = {startDateRange: startDate, endDateRange: endDate };
    this.supplierLandingService.getUploadedFilesByRange(filterCriteriaForDashboard).subscribe(
      data => {
        if (data) {
          let noDataForLastThreeMonths = true;
          this.dashboardWidgetLast3Months = data;
          if (this.dashboardWidgetLast3Months && this.dashboardWidgetLast3Months.lenght > 0) {
            noDataForLastThreeMonths = false;
          } else {
             noDataForLastThreeMonths = true;
          }
          this.lastThreeMonths(noDataForLastThreeMonths);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }


  public initGridSupplierMapping(data) {
    const supplierMapping = data ? data : [];
    supplierMapping.map(uploadedFile => {
    });
     this.viewModelDataGridSupplierMapping.setRowsData(supplierMapping, false);
    //    this.viewModelDataGridSupplierMapping.setRowsData(supplierMapping, false,  {isPreviewTargetDefinitionIconClaimList: false},  {isDownloadTargetDefinitionIconClaimList: false});
  }

  public initGrid(data) {
    const uploadedFiles = data ? data : [];
    uploadedFiles.map(uploadedFile => {
      if (uploadedFile.uploadedOn) {
        uploadedFile.uploadedOnDisplay = new Date(uploadedFile.uploadedOn).toLocaleDateString() + " " + new Date(uploadedFile.uploadedOn).toLocaleTimeString();
      }
      if (uploadedFile.submissionOn) {
        uploadedFile.submissionOnDisplay = new Date(uploadedFile.submissionOn).toLocaleDateString() + " " + new Date(uploadedFile.submissionOn).toLocaleTimeString();
      }
      if (uploadedFile.aprvalRejectDate) {
        uploadedFile.aprvalRejectDateDisplay = new Date(uploadedFile.aprvalRejectDate).toLocaleDateString() + " " + new Date(uploadedFile.aprvalRejectDate).toLocaleTimeString();
      }

      if (uploadedFile.endDate) {
        uploadedFile.processedOnDisplay = new Date(uploadedFile.endDate).toLocaleDateString() + " " + new Date(uploadedFile.endDate).toLocaleTimeString();
      }
    });
  this.viewModelDataGrid.setRowsData(uploadedFiles, false); // ,  {isPreviewTargetDefinitionIconClaimList: false},  {isDownloadTargetDefinitionIconClaimList: false}
   // this.viewModelDataGrid.setRowsData(uploadedFiles, false,  {isPreviewTargetDefinitionIconClaimList: false},  {isDownloadTargetDefinitionIconClaimList: false});

  }
}
interface DropDownValueList {
  value: string;
  text: string;
}
