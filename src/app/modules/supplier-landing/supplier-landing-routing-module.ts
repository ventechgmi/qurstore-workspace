import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { SupplierLandingListViewComponent } from './views/supplier-landing-list-view/supplier-landing-list-view.component';
import { SupplierMappingListViewComponent } from './views/supplier-mapping-list-view/supplier-mapping-list-view.component';

export const routes: Routes = [
  {
    path: "supplier-landing/list",
    component: SupplierLandingListViewComponent
  },
  {
    path: "supplier-mapping/list",
    component: SupplierMappingListViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupplierLandingRoutingModule { }
