
import {map} from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient, HttpEvent, HttpRequest } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";

const appConfigLocal = require("../../../app-config.json");
@Injectable({
    providedIn: "root"
})
export class SupplierLandingService extends EntityService {
        appConfig: AppConfig;
        onUpdateListEvent = new EventEmitter();
        public showSuccessMessage = new Subject<any>();
        public alertInfo = { message: "", type: "" };
        private afterInitSubject = new BehaviorSubject(this.alertInfo);
        public supplierLandingIdSubject = new ReplaySubject<any>();
        private test: any[];
        constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
            super(http, "dashboard", appConfig);
            this.appConfig = appConfig;

        }
        public getSupplierLandingIdSubject() {
            return this.supplierLandingIdSubject.asObservable();
        }

        public showAfterIntMessage(messageInfo) {
            this.afterInitSubject.next(messageInfo);
        }

        public  getAfterIntObs(): Observable<any> {
            return this.afterInitSubject.asObservable();
        }

        public getShowSuccessMessage() {
            return this.showSuccessMessage.asObservable();
        }

        public getUpdateTargetDefinitionListEvent() {
            return this.onUpdateListEvent;
        }
        getRawApi(): string {
            return this.appConfig.apiEndPoint;
       }

        private getSupplierDashboardAPIURL(){
            return this.getApi();
        }


        private getFileServiceAPIURL(){
            return this.getRawApi() + "/FileService";
        }



        public  getUploadedFilesByRange(filterCriteriaForDashboard): Observable<any> {
            const endpoint =this.getSupplierDashboardAPIURL() + "/GetUploadedFilesByRange";
            return this.http
            .post(endpoint, filterCriteriaForDashboard).pipe(
            map((res: Response) => res));
        }


        public  getMappingDataInformation(orgId: string): Observable<any> {
            const endpoint = this.getSupplierDashboardAPIURL() + "/getMappingDefinition/";
            return this.http
            .get(endpoint + orgId).pipe(
            map(res => {
                return res;
            }));

        }
        public  saveDataProcess(data): Observable<any> {
            return this.http
            .post(this.getSupplierDashboardAPIURL() + "/saveDataProcess" , data).pipe(
            map((res: Response) => res));

        }

        public  getAllUploadedFiles(orgId): Observable<any> {
            const endpoint = this.getSupplierDashboardAPIURL() + "/GetUploadedFilesByOrganization/";
            return this.http
            .get(endpoint + orgId).pipe(
            map(res => {
                return res;
            }));
        }

        public  getDUAList(): Observable<any> {
            const endpoint = this.getFileServiceAPIURL() + "/getDataUsageAgreements";
            return this.http
            .get(endpoint).pipe(
            map(res => {
                return res;
            }));
        }
        public  getTargetMappingList(): Observable<any> {
            const endpoint = this.getFileServiceAPIURL() + "/getMappingDefinitions";
            return this.http
            .get(endpoint).pipe(
            map(res => {
                return res;
            }));
        }

        public  getSupplierMappingDefinition(orgId): Observable<any> {
            const endpoint = this.getSupplierDashboardAPIURL() + "/GetSupplierMappingDefinition/";
            return this.http
            .get(endpoint + orgId).pipe(
            map(res => {
                return res;
            }));
        }


        public saveFileContent(configData: any, fileToUpload: Blob): Observable<any> {
            const formData: FormData = new FormData();
            const fileInformationJsonObject: string = JSON.stringify(configData);
            const endpoint = this.getFileServiceAPIURL() + "/saveUploadedFile";
            formData.set('fileInformationJsonObject',  fileInformationJsonObject);
            formData.set('formFile', fileToUpload);
            return this.http
                .post(endpoint, formData).pipe(
                map((res: Response) => res));
        }

        // private generateGetAPI(name, req): Observable<any> {
        //     return this.http
        //     .get("https://localhost:9001/targetDefinition" + name + req).pipe(
        //     map(res => {
        //         return res;
        //     }));
        // }
        // private generateDeleteAPI(name, data): Observable<any> {
        //     return this.http
        //     .delete("https://localhost:9001/targetDefinition" + name, data).pipe(
        //         map(res => {
        //             return res;
        //         }));
        // }
        // private generatePostAPI(name, data): Observable<any> {
        //     return this.http
        //     .post("https://localhost:9001/targetDefinition" + name, data).pipe(
        //     map((res: Response) => res));
        // }

}
