import { Component, OnInit } from "@angular/core";
import { RoleDetailViewModel } from "../../../viewmodels/role/role-detail.viewmodel";
import { ActivatedRoute } from "@angular/router";
// import { AuthService } from "src/app/utils/auth.service";
import { AlertService } from "../../../../../utils/alert/alert.service";
import { RoleManagementNavigationService } from "src/app/modules/core-ui-services/role-management-navigation.service";

export interface User {
  name: string;
}

@Component({
  selector: "app-role-edit-detail-view",
  templateUrl: "./role-edit-detail-view.component.html",
  styleUrls: ["./role-edit-detail-view.component.scss"],
  providers: [RoleDetailViewModel, AlertService]
})
export class RoleEditDetailViewComponent implements OnInit {
  // userId: string;
  constructor(
    public roleManagementNavigationService: RoleManagementNavigationService,
    public roleViewModel: RoleDetailViewModel,
    private router: ActivatedRoute,
    private alertService: AlertService
    // ,    private jQuery: JQueryService
  ) {
    // this.userId = this.authService.getUserId();
  }

  public ngOnInit() {
    this.router.params
      .subscribe(async params => {
        if(params["key"] && params["key"] !== "new-role") {
          const data = await this.roleManagementNavigationService.navigationParametersForLink(params["key"]);
          this.initViewModels(data.roleId, data);
        } else {
          this.initViewModels(params["key"], {roleId: params["key"]});
        }
      });
  }

  public displayFn(user?: User): string | undefined {
    return user ? user.name : undefined;
  }

  public initViewModels(roleId: string, params: any) {
    const roleDetailParams = {
      id: roleId,
      modalType: (params.roleId === "new-role") ? "Create" : (params.type === "2") ? "Detail" : "Edit",
      relations: ["RoleModules"]
    };
    this.roleViewModel.init(roleDetailParams);
  }
}
