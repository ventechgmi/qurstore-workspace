import {
    Component,
    OnInit
} from "@angular/core";
import { RoleListViewModel } from "../../../viewmodels/role/role-list.viewmodel";
import { RoleService } from "../../../services/role.service";
import { Router, ActivatedRoute } from "@angular/router";

import { NgxSmartModalService } from "ngx-smart-modal";
import { FormBuilder } from "@angular/forms";
// import { AuthService } from "src/app/utils/auth.service";
import { AlertService } from "../../../../../utils/alert/alert.service";
@Component({
    selector: "app-role-list-view",
    templateUrl: "./role-list-view.component.html",
    styleUrls: ["./role-list-view.component.scss"],
    providers: [RoleListViewModel, AlertService] // need separate alert instance
})
export class RoleListViewComponent implements OnInit {

    public roleList: any;
    public dataTable: any;
    public roleCreateModalRef: any;
    projectId: any;
    constructor(private router: Router,
        private roleService: RoleService,
        public roleListViewModel: RoleListViewModel,
        public alertService: AlertService,
        private route: ActivatedRoute,
        public ngxSmartModalService: NgxSmartModalService
    ) {
        alertService.afterInitObs = roleService.getAfterIntObs();
    }

    public ngOnInit() {
        this.route.params
            .subscribe(params => {
                this.roleListViewModel.initServices(this.alertService);
                this.roleListViewModel.init();
            });

        this.route.queryParams.subscribe(res => {
            this.projectId = res.id;
        });
    }

    public ngOnDestroy() {
        this.roleService.showAfterIntMessage({ text: "", type: "success", toStable: true });
    }

}
