
import { map } from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";

const appConfigLocal = require("../../../app-config.json");
@Injectable({
    providedIn: "root"
})
export class RoleService extends EntityService {
    appConfig: AppConfig;
    onUpdateListEvent = new EventEmitter();
    public roleIdSubject = new ReplaySubject<any>();
    public showSuccessMessage = new Subject<any>();

    public alertInfo = { message: "", type: "" };
    private afterInitSubject = new BehaviorSubject(this.alertInfo);

    constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
        super(http, "role", appConfig);
        this.appConfig = appConfig;

    }
    private getAPIURLUserManagement() {
        return this.getAPIURL();
    }

    private getAPIURL() {
        return this.getApi(); // 10/19/2020 07/30/2020 -> HT : No need of the block below
    }
    private generateGetAPIUser(name, req): Observable<any> {

        return this.http
            .get(this.getAPIURLUserManagement() + name + req).pipe(
                map(res => {
                    return res;
                }));
    }

    public showAfterIntMessage(messageInfo) {
        this.afterInitSubject.next(messageInfo);
    }

    public getAfterIntObs(): Observable<any> {
        return this.afterInitSubject.asObservable();
    }

    public getRoleIdSubject() {
        return this.roleIdSubject.asObservable();
    }

    public getShowSuccessMessage() {
        return this.showSuccessMessage.asObservable();
    }

    public getUpdateRoleListEvent() {
        return this.onUpdateListEvent;
    }

    private generateGetAPI(name, req): Observable<any> {
        return this.http
            .get(this.getAPIURL() + name + req).pipe(
                map(res => {
                    return res;
                }));
    }

    private generatePostAPI(name, data): Observable<any> {
        return this.http
            .post(this.getApi() + name, data).pipe(
                map((res: Response) => res));
    }

    private generateDeleteAPI(name, data): Observable<any> {
        return this.http
            .delete(this.getApi() + name, data).pipe(
                map(res => {
                    return res;
                }));
    }
    public getAllRoles(): Observable<any> {
        return this.generateGetAPI("/list", "");
    }


    public getAllRolesForUserModule(): Observable<any> {
        return this.generateGetAPIUser("/list", "");
    }


    public saveRole(roleDetails): Observable<any> {
        return this.generatePostAPI("/save", roleDetails);
    }

    public deleteRole(roleDetails): Observable<any> {
        return this.generateDeleteAPI("/delete/" + roleDetails.id, roleDetails);
    }
    public getRoleModules(): Observable<any> {
        return this.generateGetAPI("/module-list", "");
    }

    public getRoleByName(req): Observable<any> {
        return this.generateGetAPI("/role-detail/by-name/", req);
    }

    public canDeleteRole(req): Observable<any> {
        return this.generateGetAPI("/can/delete/role/", req);
    }
}
