import { map } from "rxjs/operators";
import { EntityBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { RoleService } from "../../services/role.service";
import { Observable, Subject, of } from "rxjs";
import { AbstractControl, ValidatorFn } from "@angular/forms";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AuthService } from "src/app/utils/auth.service";
import { ActivatedRoute } from "@angular/router";
import { ROUTES_CONFIG_MAP, MODULE_BASE_URL_MODULE_ID_MAP } from "src/app/utils/config";
export class RoleViewModel extends EntityBaseViewModel<"", RoleService> {
    public moduleId: string;
    public moduleAccess;
    public moduleInfo;
    public updateRoleListSubject = new Subject<any>();
    public roleDetailSelection = new Subject<any>();
    auditLogPermission: any = {};

    public constructor(
        public roleService: RoleService,
        public viewModelType: ViewModelType,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute,
        public authService: AuthService
    ) {
        super(roleService, viewModelType);
        this.entityName = "role";
    }


    public create(): Observable<any> {
        // add any customization logic for create
        return super.create().pipe(map(data => data));
    }

    public getUserClaims(): any {
        const baseUrl = ROUTES_CONFIG_MAP.ROLE_MANAGEMENT.BASE_URL;
        this.moduleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
        this.moduleInfo = this.authService.getModuleAccess(this.moduleId) || null;
        this.moduleAccess = this.moduleInfo ? this.moduleInfo.RolePermissions : {};

        return this.moduleAccess;
    }

    // custom methods
    public getUpdateRoleListObservable(): Observable<any> {
        return this.updateRoleListSubject.asObservable();
    }

    public getUpdateRoleListSubject(): Subject<any> {
        return this.updateRoleListSubject;
    }

    public getroleDetailObservable(): Observable<any> {
        return this.roleDetailSelection.asObservable();
    }

    public getroleDetailSelectionSubject(): Subject<any> {
        return this.roleDetailSelection;
    }

    public nameUniqueValidator(roleName): Observable<any> {
        return this.roleService.getRoleByName(roleName).pipe(map(res => res));
    }

    public noWhitespaceValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            // messy but you get the idea
            const isWhitespace = (control.value || "").trim().length === 0;
            const isValid = !isWhitespace;
            return isValid ? null : { hasOnlyWhiteSpace: true };
        };
    }

    /*
    This method validates the below
    1. validates the control has only white spaces since most of the name fields should not allow just white spaces
    2. validates the control whether it has only special characters.

    */
    public validateSpecialCharactersandWhiteSpace(lengthMin: number): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            // const pattern = "[^A-Za-z0-9||||\.||_]";
            const pattern = "[A-Za-z0-9]";
            const whiteSpace = /^\s*$/;
            if ((control.value || "").length === 0) {
                return null;
            } else if ((control.value || "").trim().length < lengthMin) {
                return { minlength: true };
            } else if (control.value.match(whiteSpace)) {
                return { hasOnlyWhiteSpace: true };
            } else if (!control.value.match(pattern)) { // check if the input doest not have any alphabets or in other words input has only special characters
                // return "Don't enter only special characters";
                return { hasOnlySpecialCharaters: true };
            }
        };
    }
}
