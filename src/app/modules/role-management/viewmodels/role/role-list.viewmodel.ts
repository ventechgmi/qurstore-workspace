import { from as observableFrom } from "rxjs";
import { AlertService } from "../../../../utils/alert/alert.service";
import { RoleService } from "../../services/role.service";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { RoleViewModel } from "./role.viewmodel";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../../core-ui-components";
import config, { GridConfig } from "../../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AuthService } from "src/app/utils/auth.service";
import { RoleManagementNavigationService } from "src/app/modules/core-ui-services/role-management-navigation.service";

@Injectable()
export class RoleListViewModel extends RoleViewModel {
  public userId: string;
  public roleId: string;
  public roleList: any;
  public alertService: AlertService;
  public viewModelDataGrid: DataGridViewModel;
  public header = [{ headerName: "Role", field: "name", isClickable: false }, { headerName: "Number of Modules Associated", field: "moduleCount", isNumber: true }];
  public appliedFilters: any[];
  public roleWithoutUserList: any;
  public userRolePermission: any = {};

  public constructor(
    public roleService: RoleService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public roleManagementNavigationService: RoleManagementNavigationService,
    public authService: AuthService
  ) {
    super(roleService, ViewModelType.Edit, ngxSmartModalService, route, authService);
  }

  public init() {
    this.userId = this.authService.getUserId();
    this.viewModelDataGrid = new DataGridViewModel();
    this.viewModelDataGrid.setPagination(true);
    this.subscribeToCreateSuccessMessage();
    this.userRolePermission = this.getUserClaims() || {};
    this.subscribers();
    this.viewModelDataGrid.setHeader(this.header, false, { canUpdate: this.userRolePermission.canUpdate, canDelete: this.userRolePermission.canDelete });
    this.viewModelDataGrid.clickableColumnIndices = [1];
    this.setDataTableConfigurations();
    this.getRolesList();
    return observableFrom([]);
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  public redirectToCreate(): any {
    this.router
      .navigate(["/role-management/role-detail/new-role"])
      .then()
      .catch();
  }

  public deleteUserFromList(): void {
    this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
    this.ngxSmartModalService.getModal("deleteConfirmModalFromList").open();
  }

  public async deleteRole(): Promise<void> {
    // const request = { roleId: this.roleId, currentUserId: this.currentUserId };  // Once Auth is implemented
    const request = { id: this.roleId };
    await this.roleService.deleteRole(request).subscribe(data => {
      if (data.isSuccess) {
        this.alertService.success(data.message, true);
        this.getRolesList();
      } else {
        this.alertService.error(data.message, true);
      }
    }, (requestFailed) => {
      (requestFailed.error && requestFailed.error.validationErrors) ?
        this.alertService.error(requestFailed.error.validationErrors.message, true) :
        this.alertService.error(requestFailed.error.errorList[0], true);
      this.ngxSmartModalService.getModal("deleteConfirmModalFromList").close();
    }, () => {
      this.ngxSmartModalService.getModal("deleteConfirmModalFromList").close();
    });
  }

  // data-table configurations for project management
  public setDataTableConfigurations(): void {
    this.viewModelDataGrid.dataTableConfiguration = {
      destroy: true,
      columnDefs: [{ targets: [0], visible: false, searchable: false }, { type: "num-html", targets: [2] }, { orderable: false, targets: 2, searchable: false }],
      order: [[1, "asc"]]
    };
  }

  public getRolesList(): void {
    this.roleService.getAllRoles().subscribe(
      data => {
        if (data) {
          this.roleList = data;
          this.initGrid(this.roleList);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public initGrid(data: any): void {
    const roles = data ? data : [];
    roles.map(role => {
      role.moduleCount = role.moduleCount === undefined ? "0" : role.moduleCount;
    });
    this.viewModelDataGrid.setRowsData(roles, false);
  }

  public subscribeToCreateSuccessMessage(): void {
    this.roleService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      if (!data.isCreateAnother) {
        this.alertService.success(message, toStable);
      }
    });
  }

  private subscribers(): void {
    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.columnClicked:
          this.roleId = e.eventData.rowDetails.id;
          this.redirectToDetail();
          break;
        case GridConfig.editClicked:
          this.roleId = e.eventData.rowDetails.id;
          this.redirectToDetail();
          break;
        case GridConfig.deleteClicked:
          this.roleId = e.eventData.rowDetails.id;
          this.deleteUserFromList();
          break;
        default:
          if (e.eventData && e.eventData.rowDetails) {
            this.roleId = e.eventData.rowDetails.id;
            this.redirectToDetail();
          }
          break;
      }
    }, error => {
      this.alertService.error(error, true);
    });
  }

  // Getting all the role details and redirecting to the Role details page
  private redirectToDetail(): any {
    this.roleService.roleIdSubject.next(this.roleId);
    this.roleManagementNavigationService.navigateUsingProxy({
      url: "/role-management/role-detail",
      saveLink: true,
      openInNewWindow: false,
      isQueryParams: false,
      parameters: { roleId: this.roleId }
    }).catch();

  }

  public onPopUpClose(): void {
    this.ngxSmartModalService.getModal("deleteConfirmModalFromList").close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }

}
