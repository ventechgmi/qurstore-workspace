import { from as observableFrom } from "rxjs";
import { RoleService } from "../../services/role.service";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { AlertService } from "../../../../utils/alert/alert.service";
import { RoleViewModel } from "./role.viewmodel";
import {
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";
import { DataGridExtendedViewModel } from "../../../core-ui-components";
import { GridConfig } from "../../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ValidationMessages } from '../../helpers/validation-messages';
import { AuthService } from 'src/app/utils/auth.service';
import { GuidHelper } from 'src/app/modules/employee-dashboard/helpers/guid-helper';

@Injectable()
export class RoleDetailViewModel extends RoleViewModel {
  public roleId: any;
  public isDeleteButtonEnable = true;
  public roleDetailsForm: FormGroup;
  public submitted = false;
  public modalType = "Edit" || "Create" || "Detail";
  public isFormValueChange = false; // to disable save btn if form fields not changed
  public roleOrgTypesCount = 0;
  public roleModulesCount = 0;
  public viewModelDataGrid: DataGridExtendedViewModel;
  public existingRoleModules: any[] = [];
  public allRoleModules: any[] = [];
  public originalAllRoleModules: any[] = [];
  public roleDetail: any = {
    id: "",
    name: "",
    description: ""
  };
  public isExist: boolean;
  public userRolePermission: any = {};
  public isAllRead = false;
  public isAllUpdate = false;
  public isAllDelete = false;
  public isAllApprove = false;

  public constructor(
    public roleService: RoleService,
    private router: Router,
    private fb: FormBuilder,
    private alertService: AlertService,
    public ngxSmartModalService: NgxSmartModalService,
    private chRef: ChangeDetectorRef,
    public authService: AuthService,
    public route: ActivatedRoute
  ) {
    super(roleService, ViewModelType.ReadOnly, ngxSmartModalService, route, authService);
    this.initRoleDetailsForm();
  }

  public init(params?) {
    this.modalType = params.modalType;
    this.userRolePermission = this.getUserClaims() || {};
    this.roleService.showAfterIntMessage({ text: "", type: "success", toStable: true });
    if (this.modalType === "Create") {
      this.getRoleModules();
    } else {
      super.init(params)
        .subscribe((data: any) => {
          this.roleDetail = data;
          this.updateFormFields(this.roleDetail);
          this.getRoleModules();
        });
      if (params.id) {
        this.roleId = params.id;
      } else {
        this.alertService.error(ValidationMessages.idNotFound, false);
      }
    }
    return observableFrom([]);
  }

  public get detailForm(): any {
    return this.roleDetailsForm.controls;
  }

  public isAtLeastOneModuleSelected(): boolean {
    let isValidModuleList = false;
    _.each(this.allRoleModules, (roleModule) => {
      const { canReadModule, canUpdateModule, canDeleteModule, id } = roleModule;
      if (canReadModule || canUpdateModule || canDeleteModule) {
        isValidModuleList = true;
      }
    });
    return isValidModuleList;
  }

  public async saveChanges(): Promise<void> {
    this.submitted = true;
    if (this.roleDetailsForm.invalid) { return; }
    if (!this.isAtLeastOneModuleSelected()) {
      this.alertService.error(ValidationMessages.NoModuleSelected, false);
      return;
    }

    this.roleDetailsForm.controls.name.setValue(this.roleDetailsForm.controls.name.value.trim());
    this.roleDetailsForm.controls.description.setValue(this.roleDetailsForm.controls.description.value.trim());
    this.updateRole().catch();
  }

  public async updateRole(): Promise<any> {
    const roleDetails = this.transformToEntityModel();
    return this.roleService.saveRole(roleDetails)
      .subscribe(
        (data) => {
          this.submitted = false;
          this.roleDetail.name = data.name;
          this.roleDetail.description = data.description;
          this.alertService.success(data.message, false);
          this.roleService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
        },
        (err) => {
          this.submitted = false;
          this.alertService.error(err.error.errorList[0], false);
        },
        () => {
          this.navigateToList().then().catch();
        });
  }

  public transformToEntityModel(): any {
    const { name, description } = this.roleDetailsFormControls;
    const roleDetailEntity: any = {
      id: this.roleId,
      name: name.value,
      description: description.value,
      isEnabled: true,
      lastModifiedBy: this.authService.getUserId(),
      roleModules: this.transformModuleToEntityModel()
    };
    return roleDetailEntity;
  }

  public onCancel(): void {
    if (this.modalType === "Edit" || this.modalType === "Create") {
      if (this.roleDetailsForm.dirty) {
        this.ngxSmartModalService.getModal("roleCancelConfirmModal").open();
        this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
      } else {
        this.navigateToList().then().catch();
      }
    } else {
      this.navigateToList().then().catch();
    }
  }

  public async navigateToList(): Promise<any> {
    return this.router.navigate(["role-management/list"]);
  }

  public onConfirm(): void {
    if (this.modalType === "Create" || this.modalType === "Edit") {
      this.navigateToList();
    }
    this.roleDetailsForm.reset();
    this.updateFormFields(this.roleDetail);
    this.ngxSmartModalService.getModal("roleCancelConfirmModal").close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }

  public onPopUpClose(): void {
    this.ngxSmartModalService.getModal("roleCancelConfirmModal").close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }

  public toggleAllRead(): void {
    if (!this.isAllRead) {
      this.allRoleModules.forEach(e => {
        e.canReadModule = false;
        e.canUpdateModule = false;
        e.canDeleteModule = false;
        e.canExecuteModule = false;
      });
      this.isAllUpdate = false;
      this.isAllDelete = false;
      this.isAllApprove = false;
    } else {
      this.allRoleModules.forEach(e => e.canReadModule = true);
    }
    this.roleDetailsForm.markAsDirty();
  }

  public toggleRead(moduleId: string, canReadModule: boolean): void {
    const moduleObj = this.allRoleModules.find(module => module.moduleId === moduleId);
    if (canReadModule) {
      moduleObj.canReadModule = true;
    } else {
      moduleObj.canReadModule = false;
      moduleObj.canUpdateModule = false;
      moduleObj.canDeleteModule = false;
      moduleObj.canExecuteModule = false;
    }
    this.checkAllSelected();
    this.roleDetailsForm.markAsDirty();
  }

  public toggleAllUpdate(): void {
    if (!this.isAllUpdate) {
      this.allRoleModules.forEach(e => e.canUpdateModule = false);
    } else {
      this.allRoleModules.forEach(e => {
        e.canUpdateModule = true;
        e.canReadModule = true;
      });
    }
    this.checkAllSelected();
    this.roleDetailsForm.markAsDirty();
  }

  public toggleUpdate(moduleId: string, canUpdate: boolean): void {
    const moduleObj = this.allRoleModules.find(module => module.moduleId === moduleId);
    if (canUpdate) {
      moduleObj.canReadModule = true;
      moduleObj.canUpdateModule = true;
    } else {
      moduleObj.canUpdateModule = false;
    }
    this.checkAllSelected();
    this.roleDetailsForm.markAsDirty();
  }

  public toggleAllDelete(): void {
    if (!this.isAllDelete) {
      this.allRoleModules.forEach(e => e.canDeleteModule = false);
    } else {
      this.allRoleModules.forEach(e => {
        e.canReadModule = true
        e.canDeleteModule = true
      });
    }
    this.checkAllSelected();
    this.roleDetailsForm.markAsDirty();
  }

  public toggleDelete(moduleId: string, canDelete: boolean): void {
    const moduleObj = this.allRoleModules.find(module => module.moduleId === moduleId);
    if (canDelete) {
      moduleObj.canReadModule = true;
      moduleObj.canDeleteModule = true;
    } else {
      moduleObj.canDeleteModule = false;
    }
    this.checkAllSelected();
    this.roleDetailsForm.markAsDirty();
  }

  public toggleAllApprove(): void {
    this.allRoleModules.forEach(module => {
      if (module.moduleType.isExecutable) {
        if (!this.isAllApprove) {
          module.canExecuteModule = false;
        } else {
          module.canReadModule = true;
          module.canExecuteModule = true;
        }
      }
    });
    this.checkAllSelected();
    this.roleDetailsForm.markAsDirty();
  }

  public toggleApprove(moduleId: string, canExecute: boolean): void {
    const moduleObj = this.allRoleModules.find(module => module.moduleId === moduleId);
    if (canExecute) {
      moduleObj.canReadModule = true;
      moduleObj.canExecuteModule = true;
    } else {
      moduleObj.canExecuteModule = false;
    }
    this.checkAllSelected();
    this.roleDetailsForm.markAsDirty();
  }

  private initRoleDetailsForm(): void {
    this.roleDetailsForm = this.fb.group({
      name: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(25), Validators.pattern("^[a-zA-Z0-9 _-]+$"), super.validateSpecialCharactersandWhiteSpace(3)]],
      description: ["", [, Validators.maxLength(500), super.validateSpecialCharactersandWhiteSpace(1)]]
    });
  }

  private transformModuleToEntityModel(): any {
    const roleModules: any[] = [];
    _.each(this.allRoleModules, (roleModule) => {
      const { canReadModule, canUpdateModule, canDeleteModule, canExecuteModule, moduleId, id } = roleModule;
      if (canReadModule || canUpdateModule || id != GuidHelper.empty) {
        const roleModuleInfo: any = {
          id,
          moduleId,
          roleId: this.roleId,
          canRead: canReadModule,
          canUpdate: canUpdateModule,
          canDelete: canDeleteModule,
          canExecute: canExecuteModule,
          canCreate: canUpdateModule,
          lastModifiedBy: this.authService.getUserId()
        };
        roleModules.push(roleModuleInfo);
      }
    });
    return roleModules;
  }

  private async getRoleModules(): Promise<any> {
    return this.roleService.getRoleModules()
      .subscribe((data) => {
        this.allRoleModules = data ? data : [];
        this.mergeResults();
        this.checkAllSelected();
      });
  }

  private mergeResults(): void {
    this.allRoleModules.forEach(element => {
      const row = this.existingRoleModules.filter(x => x.moduleId === element.id && x.isDeleted === false);
      element.moduleId = element.id;
      if (row?.length > 0) {
        element.id = row[0].id;
        element.canReadModule = row[0].canRead;
        element.canUpdateModule = row[0].canUpdate;
        element.canDeleteModule = row[0].canDelete;
        element.canExecuteModule = row[0].canExecute;
      } else {
        element.id = GuidHelper.empty;
        element.canReadModule = false;
        element.canUpdateModule = false;
        element.canDeleteModule = false;
        element.canExecuteModule = false;
      }
    });
    this.allRoleModules.forEach(val => this.originalAllRoleModules.push(Object.assign({}, val)));
  }

  private updateFormFields(roleDetail: any): void {
    this.roleDetailsForm.patchValue({
      name: roleDetail.name,
      description: roleDetail.description
    });
    this.existingRoleModules = roleDetail.roleModules;
  }

  private get roleDetailsFormControls(): any {
    return this.roleDetailsForm.controls;
  }

  private checkAllSelected(): void {
    this.isAllRead = this.allRoleModules.every(e => e.canReadModule === true);
    const updatableModules = [];
    const removableModule = [];
    const executableModules = [];
    this.allRoleModules.forEach(module => {
      if (module.moduleType.isManagement) {
        updatableModules.push(module);
        removableModule.push(module);
        if (module.moduleType.isExecutable) {
          executableModules.push(module);
        }
      }
    });
    this.isAllUpdate = updatableModules.every(e => e.canUpdateModule === true);
    this.isAllDelete = removableModule.every(e => e.canDeleteModule === true);
    this.isAllApprove = executableModules.every(e => e.canExecuteModule === true);
  }
}
