import { Routes, RouterModule } from "@angular/router";
import { RoleListViewComponent } from "./views/role/role-list-view/role-list-view.component";
import { RoleEditDetailViewComponent } from "./views/role/role-edit-detail-view/role-edit-detail-view.component";
import { NgModule } from "@angular/core";

export const routes: Routes = [
  {
    path: "list",
    component: RoleListViewComponent
  },
  {
    path: "role-detail/:key",
    component: RoleEditDetailViewComponent
  },
  {
    path: "role-detail/new-role",
    component: RoleEditDetailViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleManagementRoutingModule { }
