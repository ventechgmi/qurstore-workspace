import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { RoleService } from "./services/role.service";
import { RoleListViewComponent } from "./views/role/role-list-view/role-list-view.component";
import { RoleEditDetailViewComponent } from "./views/role/role-edit-detail-view/role-edit-detail-view.component";
import { DataGridModule, DataGridExtendedModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import { RoleManagementRoutingModule } from "./role-management-routing-module";
import {MatAutocompleteModule } from "@angular/material/autocomplete";
import {MatInputModule} from "@angular/material/input";
import { MatFormFieldModule} from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import {  MatButtonModule} from "@angular/material/button";

import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";

@NgModule(
    {
        declarations: [
            RoleListViewComponent,
            RoleEditDetailViewComponent
        ],
        imports: [
            SharedModules,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            RoleManagementRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatButtonModule,
            DataGridModule,
            DataGridExtendedModule,
            MatCheckboxModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [
            RoleListViewComponent,
            RoleEditDetailViewComponent
        ],
        providers: [
            AlertService,
            RoleService,
            NgxSmartModalService
        ]
    }
)
export class RoleManagementModule {

}
