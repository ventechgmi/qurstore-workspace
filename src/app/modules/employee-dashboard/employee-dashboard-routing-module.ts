import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { EmployeeDashboardListViewComponent } from './views/employee-dashboard-landing-list-view/employee-dashboard-list-view.component';

export const routes: Routes = [
  {
    path: "employee-dashboard/list",
    component: EmployeeDashboardListViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeDashboardRoutingModule { }
