import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DataGridModule } from "../core-ui-components";
import { DataGridExtendedModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import {MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatFormFieldModule} from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import {  MatButtonModule} from "@angular/material/button";
import {  MatFormFieldControl} from "@angular/material/form-field";
import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";
import { MatSelectModule } from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {LocalStorageManager} from "../../utils/local-storage-manager";

import { EmployeeDashboardRoutingModule } from './employee-dashboard-routing-module';
import { EmployeeDashboardService } from './services/employee-dashboard.service';
import { EmployeeDashboardListViewComponent } from './views/employee-dashboard-landing-list-view/employee-dashboard-list-view.component';
@NgModule(
    {
        declarations: [
            EmployeeDashboardListViewComponent
        ],
        imports: [
            SharedModules,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            EmployeeDashboardRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatButtonModule,
            DataGridModule,
            DataGridExtendedModule,
            MatCheckboxModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [
            EmployeeDashboardListViewComponent
        ],
        providers: [
            AlertService,
            EmployeeDashboardService,
            NgxSmartModalService,
            LocalStorageManager
        ]
    }
)
export class EmployeeDashboardModule {

}
