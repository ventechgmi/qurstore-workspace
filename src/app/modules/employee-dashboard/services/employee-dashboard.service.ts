
import { map } from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpRequest } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
// import { downloadFile } from 'file-saver';

import { Component, OnInit } from '@angular/core';
import config from "src/app/utils/config";


const appConfigLocal = require("../../../app-config.json");
@Injectable({
    providedIn: "root"
})
export class EmployeeDashboardService extends EntityService {
    appConfig: AppConfig;
    onUpdateListEvent = new EventEmitter();
    public showSuccessMessage = new Subject<any>();
    public alertInfo = { message: "", type: "" };
    private afterInitSubject = new BehaviorSubject(this.alertInfo);
    public supplierLandingIdSubject = new ReplaySubject<any>();
    private test: any[];
    constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
        super(http, "dashboard", appConfig);
        this.appConfig = appConfig;

    }
    getRawApi(): string {
        return this.appConfig.apiEndPoint;
    }
    private getSupplierDashboardAPIURL() {
        return this.getApi();
    }

    private getFileServiceAPIURL() {
        let fileServiceAPI = "";
        const serverDeployment = appConfigLocal.serverDeployment;
        if (serverDeployment === "0") {
            fileServiceAPI = "https://localhost:5051/FileService";
        } else if (serverDeployment === "1") {
            fileServiceAPI = "https://api.qurstore-qa-file.vsolgmi.com/FileService";
        }
        else {
            fileServiceAPI = this.getRawApi() + "/FileService";
        }
        return fileServiceAPI;
    }

    public getSupplierLandingIdSubject() {
        return this.supplierLandingIdSubject.asObservable();
    }

    public showAfterIntMessage(messageInfo) {
        this.afterInitSubject.next(messageInfo);
    }

    public getAfterIntObs(): Observable<any> {
        return this.afterInitSubject.asObservable();
    }

    public getShowSuccessMessage() {
        return this.showSuccessMessage.asObservable();
    }

    public getUpdateTargetDefinitionListEvent() {
        return this.onUpdateListEvent;
    }
    public getUploadedFilesByRange(filterCriteriaForDashboard): Observable<any> {
        const endpoint = this.getSupplierDashboardAPIURL() + "/GetUploadedFilesByRange";
        return this.http
            .post(endpoint, filterCriteriaForDashboard).pipe(
                map((res: Response) => res));
    }

    public getAllUploadedFiles(organizationId): Observable<any> {
        const endpoint = this.getSupplierDashboardAPIURL() + "/GetUploadedFilesByOrganization/";
        return this.http
            .get(endpoint + organizationId).pipe(
                map(res => {
                    return res;
                }));
    }
    // public  getAllUploadedFilesBySupplierByDuration(organizationId, duration): Observable<any> {
    //     const endpoint = this.getSupplierDashboardAPIURL() + "GetUploadedFilesByOrganization/";
    //     return this.http
    //     .get(endpoint + organizationId).pipe(
    //     map(res => {
    //         return res;
    //     }));
    // }

    public getAllUploadedFilesBySupplierByDuration(filterCriteriaForDashboard): Observable<any> {
        const endpoint = this.getSupplierDashboardAPIURL() + "/getAllUploadedFilesBySupplierByDuration";
        return this.http
            .post(endpoint, filterCriteriaForDashboard).pipe(
                map((res: Response) => res));
    }

    public saveDataProcess(data): Observable<any> {
        return this.http
            .post(this.getSupplierDashboardAPIURL() + "/saveDataProcess", data).pipe(
                map((res: Response) => res));

    }

    public getMappingDataInformation(orgId: string): Observable<any> {
        const endpoint = this.getSupplierDashboardAPIURL() + "/getMappingDefinition/";
        return this.http
            .get(endpoint + orgId).pipe(
                map(res => {
                    return res;
                }));
    }

    public saveFileContent(configData: any, fileToUpload: Blob): Observable<any> {
        const formData: FormData = new FormData();
        const fileInformationJsonObject: string = JSON.stringify(configData);
        const endpoint = this.getFileServiceAPIURL() + "/saveUploadedFile";
        formData.set('fileInformationJsonObject', fileInformationJsonObject);
        formData.set('formFile', fileToUpload);
        return this.http
            .post(endpoint, formData).pipe(
                map((res: Response) => res));
    }

    public getDataErrorsStatistics(filterCriteriaForDashboard): Observable<any> {
        const endpoint = this.getSupplierDashboardAPIURL() + "/getRowwiseValidationErrors";
        return this.http
            .post(endpoint, filterCriteriaForDashboard).pipe(
                map((res: Response) => res));
    }



    public crateApproveRejectRequest(approveRejectStatusForProcessedData: any): Observable<any> {

        const endpoint = this.getSupplierDashboardAPIURL() + "/approveRejectFile";
        return this.http
            .post(endpoint, approveRejectStatusForProcessedData).pipe(
                map((res: Response) => res));
    }

    public downloadFile(uploadedFileId: string, fileType: string): Observable<any> {
        const formData: FormData = new FormData();
        const endpoint = this.getFileServiceAPIURL() + "/downloadFile";
        formData.set('uploadedFileId', uploadedFileId);
        formData.set('fileType', fileType);
        return this.http.post<Blob>(endpoint, formData, { responseType: 'blob' as 'json' });
    }
  
    public getClientIP() {​​
        return fetch(config.CLIENT_IP_FIND_URL + config.IP_INFO_TOKEN).then((response) => response.json());
    }​​

    public getDashboardUrl(ClientIp, orgId): Observable<any> {
        return this.http.get(this.getRawApi() + "/dashboard/get-current-user-dashboard/" + ClientIp + "/" + orgId)
            .pipe(map((res: Response) => res));
    }

    // public  downloadFile1(filePath: string): Observable<Blob> {
    //     const endpoint = this.getFileServiceAPIURL() + "downloadFileGetTest";
    //     return this.http.get<Blob>(endpoint + "", { responseType: 'blob' as 'json' });
    // }
    // public  getDownloadFileLink(dataProcessId): Observable<any> {
    //     const endpoint = this.getFileServiceAPIURL() + "getDownloadFileLink/";
    //     return this.http
    //     .get(endpoint + dataProcessId).pipe(
    //     map(res => {
    //         return res;
    //     }));
    // }
    // // data-extract.service.ts
    // downloadFileoldie(filePath: string): Observable<Blob> {
    //     const endpoint = this.getFileServiceAPIURL() + "downloadFileGetTest";
    //     return this.http.post<Blob>(`${endpoint}`, filePath,
    //     { responseType: 'blob' as 'json' });
    // }
    // public  downloadFile11(filePath: string): Observable<any> {
    //     const endpoint = this.getFileServiceAPIURL() + "downloadFileGetTest";
    //     return this.http.get(endpoint + "") ;
    // }

    // private generateGetAPI(name, req): Observable<any> {
    //     return this.http
    //     .get("https://localhost:9001/targetDefinition" + name + req).pipe(
    //     map(res => {
    //         return res;
    //     }));
    // }
    // private generateDeleteAPI(name, data): Observable<any> {
    //     return this.http
    //     .delete("https://localhost:9001/targetDefinition" + name, data).pipe(
    //         map(res => {
    //             return res;
    //         }));
    // }
    // private generatePostAPI(name, data): Observable<any> {
    //     return this.http
    //     .post("https://localhost:9001/targetDefinition" + name, data).pipe(
    //     map((res: Response) => res));
    // }


}
