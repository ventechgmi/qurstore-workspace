import {
    Component,
    OnInit,
    ContentChild,
    AfterViewInit,
    ViewChild,
    ElementRef,
    AfterContentInit,
    OnDestroy
} from "@angular/core";


import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormBuilder } from "@angular/forms";
// import { AuthService } from "src/app/utils/auth.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import * as HighCharts from "highcharts";
import { EmployeeDashboardListViewModel } from '../../viewmodels/employee-dashboard-list.viewmodel';
import { EmployeeDashboardService } from '../../services/employee-dashboard.service';
import { SafeResourceUrl ,DomSanitizer } from "@angular/platform-browser";

declare var require: any
const FileSaver = require('file-saver');


@Component({
    selector: "app-employee-dashboard-list-view",
    templateUrl: "./employee-dashboard-list-view.component.html",
    styleUrls: ["./employee-dashboard-list-view.component.scss"],
    providers: [ EmployeeDashboardListViewModel, AlertService] // need separate alert instance
})
export class EmployeeDashboardListViewComponent implements OnInit, OnDestroy, AfterContentInit {

    @ViewChild('fileUploader') fileUploader : ElementRef;
    public dataTable: any;
    projectId: any;
    constructor(private router: Router,
                private employeeDashboardService: EmployeeDashboardService,
                public employeeDashboardListViewModel: EmployeeDashboardListViewModel,
                public alertService: AlertService,
                private route: ActivatedRoute,
                public ngxSmartModalService: NgxSmartModalService
    ) {
        alertService.afterInitObs = employeeDashboardService.getAfterIntObs();
    }

    public  ngOnInit() {
        this.route.params
            .subscribe(params => {
               this.employeeDashboardListViewModel.initServices(this.alertService);
               this.employeeDashboardListViewModel.init();
            });

        this.route.queryParams.subscribe(res => {
            this.projectId = res.id;
        });
    }

    public ngOnDestroy() {
        this.employeeDashboardService.showAfterIntMessage({ message: "", type: "" });
    }

    public ngAfterContentInit() {
    }

}
