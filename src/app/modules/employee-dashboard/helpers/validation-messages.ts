export class ValidationMessages {
    public static readonly idNotFound = "Error occurred: please navigate back to List screen. ";
    public static readonly ErrorOccurred = "Error occurred: please navigate back to List screen. ";

    public static readonly SuccessFileUpload = "File uploaded successfully. ";

    public static readonly FileNotUploaded = "Error occurred: please upload either CSV or XLSX file while creating new record";
    public static readonly FAILED_TO_FETCH_DATA_STATISTICS = "Failed to fetch data statistics";
}
