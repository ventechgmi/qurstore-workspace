
import {map} from "rxjs/operators";
import { EntityBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { Observable, Subject ,  of } from "rxjs";
import { AbstractControl, ValidatorFn } from "@angular/forms";
import { NgxSmartModalService } from "ngx-smart-modal";
// import { AuthService } from "src/app/utils/auth.service";
import { ActivatedRoute } from "@angular/router";
import { EmployeeDashboardService } from '../services/employee-dashboard.service';

export class  EmployeeDashboardViewModel extends EntityBaseViewModel<"", EmployeeDashboardService> {
    public moduleId: string;
    public moduleAccess;
    public moduleInfo;
    public updateTargetDefinitionListSubject = new Subject<any>();
    public targetDefinitionDetailSelection = new Subject<any>();
    auditLogPermission: any = {};

    public constructor(
        public employeeDashboardService: EmployeeDashboardService,
        public viewModelType: ViewModelType,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute
    ) {
        super(employeeDashboardService, viewModelType);
        this.entityName = "supplierLanding";
    }
    public create(): Observable<any> {
        // add any customization logic for create
        return super.create().pipe(map(data => data));
    }
}
