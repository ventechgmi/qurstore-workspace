import { from as observableFrom, Observable, Subject, forkJoin } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { Injectable, OnInit, ChangeDetectorRef, ElementRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
// import { AuthService } from "src/app/utils/auth.service";
import { ValidationMessages } from '../helpers/validation-messages';
import { Component, ViewChild } from '@angular/core';
import * as HighCharts from 'highcharts';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EmployeeDashboardViewModel } from './employee-dashboard.viewmodel';
import { EmployeeDashboardService } from '../services/employee-dashboard.service';
import { rest } from 'lodash';
import { toBase64String } from '@angular/compiler/src/output/source_map';
// import { downloadFile } from 'file-saver';
import { debounceTime, filter } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { GuidHelper } from '../helpers/guid-helper';
import { debug } from 'console';
import { AuthService } from 'src/app/utils/auth.service';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
const FileSaver = require('file-saver');
@Injectable()
export class EmployeeDashboardListViewModel extends EmployeeDashboardViewModel {
  public userId: string;
  public uploadedFilesList: any;
  private mappingDefinitionId: any;
  public alertService: AlertService;
  viewModelDataGrid: DataGridViewModel;
  viewModelDataGridErrors: DataGridViewModel;
  private uploadFileEntity: any;
  private dataProcessEntity: any;
  private dataProcessId: any;
  appliedFilters: any[];
  targetDefinitionWithoutUserList: any;
  private dashboardWidgetYTD: any;
  private dataErrorsYTD: any;
  private xAxis: any[] = [];
  private isFilterActivated = false;
  public selectedSupplierName: string;
  public srcWithToken: SafeResourceUrl;


  // { headerName: "Target Name", field: "targetDefinitionName", isClickable: false },
  // { headerName: "Mapping Definition", field: "mappingDefinitionName", isClickable: false },
  header = [
    { headerName: "Supplier", field: "organizationName", isClickable: false },
    { headerName: "File Name", field: "uploadedFileName", isClickable: false },
    { headerName: "Status", field: "uploadedFileStatus", isClickable: false },
    { headerName: "Uploaded On", field: "uploadedOnDisplay", isClickable: false },
    // { headerName: "Approval Status", field: "approvalStatus", isClickable: false },
    // { headerName: "Approval On", field: "aprvalRejectDateDisplay", isClickable: false }
  ];

  errHeader = [
    { headerName: "Row Number", field: "recNum", width: 75, isClickable: false },
    { headerName: "Notes", field: "notes", width: 200, isClickable: false }
  ];
  file: any;
  errorFileUpload = false;
  todayDate: any;
  public supplierLandingForm: FormGroup;
  uploadedFileName: any;
  supplierName: any;
  totalRecords: any;
  processedRecords: any;
  failedRecords: any;
  organizationId: string;
  public constructor(
    public employeeDashboardService: EmployeeDashboardService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    private http: HttpClient,
    public authService: AuthService,
    public sanitizer: DomSanitizer,
  ) {
    super(employeeDashboardService, ViewModelType.Edit, ngxSmartModalService, route);
  }

  public init() {
    this.userId = this.authService.getUserId();
    this.viewModelDataGrid = new DataGridViewModel();
    this.viewModelDataGridErrors = new DataGridViewModel();
    this.organizationId = this.authService.getOrganizationId();
    this.viewModelDataGrid.setHeader(this.header, false, {
      canDownloadTargetDefinition: true,
      canDownloadCleanFile: true
    });

    this.viewModelDataGridErrors.setHeader(this.errHeader, false);
    this.viewModelDataGridErrors.defaultSortModel = [
      {
        colId: 'recNum',
        sort: 'asc',
      }
    ];


    this.todayDate = new Date().toDateString();
    this.supplierLandingForm = this.fb.group({
      uploadedFileName: "",
      supplierName: ""
    });
    this.viewModelDataGrid.gridSideBarOption();
    this.fetchTrustedToken();
    return observableFrom([]);
  }
  get supplierLandingFormControls() {
    return this.supplierLandingForm.controls;
  }

  public getMappingDefinitionDetails() {
    this.employeeDashboardService.getMappingDataInformation(this.organizationId).subscribe(
      data => {
        if (data) {
          this.mappingDefinitionId = data;
        }
      },
      error => {
        this.alertService.error(error, true);
      }
    );
  }

  public initGrid(data) {
    const uploadedFiles = data ? data : [];
    uploadedFiles.map(uploadedFile => {
      if (uploadedFile.uploadedOn) {
        uploadedFile.uploadedOnDisplay = new Date(uploadedFile.uploadedOn).toLocaleDateString() + " " + new Date(uploadedFile.uploadedOn).toLocaleTimeString();
      }
      if (uploadedFile.endDate) {
        uploadedFile.processedOnDisplay = new Date(uploadedFile.endDate).toLocaleDateString() + " " + new Date(uploadedFile.endDate).toLocaleTimeString();
      }
      if (uploadedFile.aprvalRejectDate) {
        uploadedFile.aprvalRejectDateDisplay = new Date(uploadedFile.aprvalRejectDate).toLocaleDateString() + " " + new Date(uploadedFile.aprvalRejectDate).toLocaleTimeString();
      }
    });
    //  this.viewModelDataGrid.setRowsData(uploadedFiles, false,  {isPreviewTargetDefinitionIconClaimList: false},  {isDownloadTargetDefinitionIconClaimList: false});
    this.viewModelDataGrid.setRowsData(uploadedFiles, false);
  }
  private viewErrorsClicked() {
    const row = this.uploadedFilesList.filter(x => x.id === this.dataProcessId);
    if (row && row.length > 0) {
      this.viewModelDataGridErrors.setRowsData(row[0].rawShipmentData, false);
      this.uploadedFileName = row[0].uploadedFileName;
      this.supplierName = row[0].supplierName;

      this.supplierLandingForm.patchValue({
        uploadedFileName: this.uploadedFileName
      });
    }
    this.ngxSmartModalService.getModal("viewErrors").open();
  }
  public approveProcessRecord() {
    this.crateApproveRejectRequest(true);
    this.ngxSmartModalService.getModal("approveReject").close();
  }
  public rejectProcessRecord() {
    this.crateApproveRejectRequest(false);
    this.ngxSmartModalService.getModal("approveReject").close();
  }
  private crateApproveRejectRequest(isApprovedInput: boolean) {
    const approveRejectStatusForProcessedData = {
      dataProcessId: this.dataProcessId,
      isApproved: isApprovedInput,
      lastModifiedBy: this.authService.getUserId()
    };
    this.employeeDashboardService.crateApproveRejectRequest(approveRejectStatusForProcessedData)
      .subscribe(
        data => {
          this.getAllUploadedFiles();
        },
        error => {
          console.log(error);
          // test
        });

  }
  private approveRejectClicked() {
    const row = this.uploadedFilesList.filter(x => x.id === this.dataProcessId);
    if (row && row.length > 0) {
      // this.viewModelDataGridErrors.setRowsData(row[0].rawShipmentData, false );
      this.uploadedFileName = row[0].uploadedFileName;
      this.supplierName = row[0].organizationName;
      this.totalRecords = row[0].totalRecordCount;
      this.processedRecords = row[0].processedRecordCount;
      this.failedRecords = row[0].errorRecordCount;

      // this.supplierLandingForm.patchValue({
      //   uploadedFileName: this.uploadedFileName
      // });
    }
    this.ngxSmartModalService.getModal("approveReject").open();
  }

  private subscribers() {
    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.viewErrorsClicked:
          this.dataProcessId = e.eventData.rowDetails[0];
          this.viewErrorsClicked();
          break;
        case GridConfig.approveRejectClicked:
          this.dataProcessId = e.eventData.rowDetails[0];
          this.approveRejectClicked();
          break;
        case GridConfig.downloadFileClicked:
          this.dataProcessId = e.eventData.rowDetails[0];
          this.getOriginalFile();
          break;
        case GridConfig.downloadCleanFileClicked:
          this.dataProcessId = e.eventData.rowDetails[0];
          this.getCleanFile();
          break;
        default:
          break;
      }
    }, error => {
      console.log("Error: " + error);
    });
  }

  private getOriginalFile() {
    const row = this.uploadedFilesList.filter(x => x.id === this.dataProcessId);
    if (row && row[0]?.configData) {
      const tempHolder = JSON.parse(row[0].configData);
      this.viewFileDownload(row[0].uploadedFileId, "raw", tempHolder.file_name); // originalFileName
    }
  }

  private getCleanFile() {
    const row = this.uploadedFilesList.filter(x => x.id === this.dataProcessId);
    if (row && row[0]) {
      const fileName = row[0].fileName;
      const tempArray = fileName.split('/');
      const fileNameNew = tempArray[tempArray.length - 1];
      this.viewFileDownload(row[0].uploadedFileId, "clean", fileNameNew);
    }
  }

  private viewFileDownload(uploadedFileId: string, fileType: string, fileName: string) {
    this.employeeDashboardService.downloadFile(uploadedFileId, fileType)
      .subscribe(
        blob => {
          const a = window.document.createElement("a");
          a.href = window.URL.createObjectURL(blob);
          a.download = fileName;
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
        },
        error => {
          console.log(error);
          // test
        });
  }


  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }
  showAllRecords() {
    this.isFilterActivated = false;
    this.selectedSupplierName = "";
    this.getAllUploadedFiles();

  }
  public getAllUploadedFiles() {
    this.employeeDashboardService.getAllUploadedFiles(new GuidHelper()).subscribe(
      data => {
        if (data) {
          this.uploadedFilesList = data;
          this.initGrid(this.uploadedFilesList);
        }
      },
      error => {
        this.alertService.error(error, true);
      }
    );
  }


  public getAllUploadedFilesBySupplierByDuration(organizationIdInput: string, supplierName: string, timeDuration: string) {
    this.selectedSupplierName = supplierName;
    this.isFilterActivated = true;

    const monthYear = timeDuration.split('-');
    const d = Date.parse(monthYear[0] + "1," + monthYear[1]);
    const startDate = new Date(d);
    const endDate = new Date(d);
    endDate.setMonth(endDate.getMonth() + 1);

    const filterCriteriaForDashboard = { organizationId: organizationIdInput, startDateRange: startDate, endDateRange: endDate };
    this.employeeDashboardService.getAllUploadedFilesBySupplierByDuration(filterCriteriaForDashboard).subscribe(
      data => {
        if (data) {
          this.uploadedFilesList = data;
          this.initGrid(this.uploadedFilesList);
        }
      },
      error => {
        this.alertService.error(error, true);
      }
    );
  }
  public get getIsFilterActivated() {
    return this.isFilterActivated;
  }
  public get getSelectedSupplierName() {
    return this.selectedSupplierName;
  }


  public getAllUploadedFilesBySupplier(organizationId: string) {
    this.employeeDashboardService.getAllUploadedFiles(organizationId).subscribe(
      data => {
        if (data) {
          this.uploadedFilesList = data;
          this.initGrid(this.uploadedFilesList);
        }
      },
      error => {
        this.alertService.error(error, true);
      }
    );
  }

  public async fetchTrustedToken() {
    const data: any = await this.employeeDashboardService.getClientIP();
    this.employeeDashboardService.getDashboardUrl(data.ip, this.organizationId)
      .subscribe(data => {
        if (!data.isSuccess) {
          this.alertService.error(data.message);
        } else {
          this.srcWithToken = this.sanitizer.bypassSecurityTrustResourceUrl(data.payload);
        }
      }, error => {
      })
  }

}
