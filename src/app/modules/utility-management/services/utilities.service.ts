
import { map, debounceTime } from 'rxjs/operators';
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { EntityService, EntityQuery, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
import { of as observableOf } from "rxjs";

const appConfigLocal = require("../../../app-config.json");
@Injectable({
    providedIn: "root"
})
export class UtilitiesService extends EntityService {
    appConfig: AppConfig;
    onUpdateListEvent = new EventEmitter();
    public userIdSubject = new ReplaySubject<any>();
    public showSuccessMessage = new Subject<any>();
    public deleteSuccessMessage = new Subject<any>();
    public userRolesCountSubject = new Subject<any>();
    public isEditModeSubject = new Subject<any>();
    public alertInfo = { message: "", type: "" };
    public isUserEditFromList = false;

    // this subscription will help to show error or success message in another screen(details screen delete success message showing in list screen)
    private afterInitSubject = new BehaviorSubject(this.alertInfo);

    constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
        super(http, "utility", appConfig);
        this.appConfig = appConfig;
    }
    private getAPIURL() {
        return this.getApi();
    }


    public showAfterIntMessage(messageInfo) {
        this.afterInitSubject.next(messageInfo);
    }

    public getAfterIntObs(): Observable<any> {
        return this.afterInitSubject.asObservable();
    }

    public getUserIdSubject() {
        return this.userIdSubject.asObservable();
    }

    public getShowSuccessMessage() {
        return this.showSuccessMessage.asObservable();
    }

    public getUpdateUserListEvent() {
        return this.onUpdateListEvent;
    }

    public getUserRolesCountObs() {
        return this.userRolesCountSubject;
    }

    private generateGetAPI(name, req): Observable<any> {
        return this.http.get(this.getAPIURL() + name + req).pipe(
            map(res => {
                return res;
            }));
    }

    private generatePostAPI(name, data): Observable<any> {
        return this.http.post(this.getAPIURL() + name, data).pipe(
            map((res: Response) => res));
    }

    private generateDeleteAPI(name, req): Observable<any> {
        return this.http.delete(this.getAPIURL() + name + req).pipe(
            map(res => {
                return res;
            }));
    }

    public getUtilities(isUserHasApproverAccess: boolean): Observable<any> {
        return this.generateGetAPI("/list/", isUserHasApproverAccess);
    }

    public uploadUtilityOrHelpFile(tdHeaderInformation: any, utilityFile: Blob, helperFile: Blob): Observable<any> {
        const formData: FormData = new FormData();
        const fileInformationJsonObject: string = JSON.stringify(tdHeaderInformation);
        formData.set('fileInformationJsonObject',  fileInformationJsonObject);
        formData.set('utilityFile', utilityFile);
        formData.set('helperFile', helperFile);
        const endpoint = this.getFileServiceAPIURL() + "/saveUploadedUtilityAndHelperFile";
        return this.http
                .post(endpoint, formData).pipe(
                map((res: Response) => res));
    }

    public downloadFile(uploadedFileId: string, fileType: string): Observable<any> {
        const formData: FormData = new FormData();
        const endpoint = this.getFileServiceAPIURL() + "/downloadFile";
        formData.set('uploadedFileId',  uploadedFileId);
        formData.set('fileType',  fileType);
        return this.http.post<Blob>(endpoint, formData, {responseType: 'blob' as 'json' });
    }

    public submitUtilityRequest(requestData: any): Observable<any> {
        return this.generatePostAPI("/save", requestData);
    }

    public checkUtilityRequestNameUnique(utilityName: string): Observable<any> {
      return this.generateGetAPI("/check-utility-request-name-unique/", utilityName);
    }

    public approveOrRejectOrCancel(requestData: any): Observable<any> {
        return this.generatePostAPI("/updateStatus", requestData);
    }

    private getFileServiceAPIURL(): string {
        const fileServiceAPI = this.appConfig.apiEndPoint + "/FileService";
        return fileServiceAPI;
    }

}
