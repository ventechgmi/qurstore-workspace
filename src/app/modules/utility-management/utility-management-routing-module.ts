import { Routes, RouterModule } from "@angular/router";
import { UtilitiesListViewComponent } from "./views/utilities/utilities-list/utilities-list.component";

import { NgModule } from "@angular/core";

export const routes: Routes = [
  {
    path: "list/:key",
    component: UtilitiesListViewComponent
  },
  {
    path: "list",
    component: UtilitiesListViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class utilitiesManagementRoutingModule { }
