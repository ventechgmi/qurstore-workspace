import {
    Component,
    OnInit,
    ContentChild,
    AfterViewInit,
    ViewChild,
    ElementRef,
    AfterContentInit
} from "@angular/core";

import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormBuilder } from "@angular/forms";
import config from "../../../../../utils/config";

import { UtilitiesListViewModel } from "../../../viewmodels/utilities/utilities-list.viewmodel";
import { UtilitiesService } from "../../../services/utilities.service";
import { AlertComponent } from "src/app/utils/alert/alert.component";
import { AlertService } from "../../../../../utils/alert/alert.service";
import { AuthService } from 'src/app/utils/auth.service';
import { UtilityManagementNavigationService } from "src/app/modules/core-ui-services/utility-management-navigation.service";

@Component({
    selector: "app-utilities-list",
    templateUrl: "./utilities-list.component.html",
    styleUrls: ["./utilities-list.component.scss"],
    providers: [ UtilitiesListViewModel, AlertService]
})
export class UtilitiesListViewComponent implements OnInit, AfterContentInit {

    public roleList: any;
    public dataTable: any;
    public userCreateModalRef: any;
    projectId: any;

    constructor(private router: Router,
                private utilitiesService: UtilitiesService,
                public utilitiesListViewModel: UtilitiesListViewModel,
                public alertService: AlertService,
                private route: ActivatedRoute,
                public ngxSmartModalService: NgxSmartModalService,
                public authService: AuthService,
                public utilityManagementNavigationService: UtilityManagementNavigationService,
    ) {
        // need to set the afterInitObs to show other screen success or error message in different screen
        alertService.afterInitObs = utilitiesService.getAfterIntObs();
    }

    public ngOnInit() {
        this.route.params
            .subscribe(async params => {
                const userId = this.authService.getUserId();
                this.utilitiesListViewModel.initServices(this.alertService);
                if(params.key) {
                    const response = await this.utilityManagementNavigationService.navigationParametersForLink(params["key"]);
                    this.utilitiesListViewModel.init({ userId, filterStatus: response.filterStatus, filterType: response.filterType  });
                } else {
                    this.utilitiesListViewModel.init({ userId, filterStatus: "", filterType: "" });
                }
            });

        this.route.queryParams.subscribe(res => {
            this.projectId = res.id;
        });
    }

    public ngAfterContentInit() { }
}
