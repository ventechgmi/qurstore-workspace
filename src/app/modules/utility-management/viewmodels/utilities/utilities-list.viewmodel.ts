import { from as observableFrom } from "rxjs";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import { Router, ActivatedRoute } from "@angular/router";
import * as _ from "underscore";
import { DataGridExtendedViewModel } from "../../../core-ui-components";
import config, { GridConfig, UTILITY_CHART_COLORS, UTILITY_FILTER_TYPE, UTILITY_MAX_FILE_SIZE } from "../../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from "../../../../utils/alert/alert.service";
import { UtilitiesService } from "../../services/utilities.service";
import { UtilitiesViewModel } from "./utilities.viewmodel";
import { AuthService } from "../../../../utils/auth.service";
import { ValidationMessages } from "../../helpers/validation-messages";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import Highcharts, { color } from "highcharts";
import moment from "moment";

@Injectable()
export class UtilitiesListViewModel extends UtilitiesViewModel {
  public userId: string;
  public utilities: any = [];
  public utilitiesOriginalData: any = [];
  public isApproveOrReject: boolean = false;
  public isViewComment: boolean = false;
  public approveOrRejectComment: string = "";
  public alertService: AlertService;
  public viewModelDataGrid: DataGridExtendedViewModel;
  public userRolePermission: any = {};
  public modalTitle: string;
  public utilitiesRequestModelForm: FormGroup;
  public utilitiesApproveRejectForm: FormGroup;
  public utilityFile: any;
  public helpFile: any;
  public isShowMessage: boolean = false;
  public messageData: any = {
    type: "",
    message: ""
  };
  public modalType: string = "Create | Edit | Detail";
  public xAxisValues: any = [];
  public chartData: any = [];
  public highcharts = Highcharts;
  public chart: any;
  public data = [{
    name: "",
    showInLegend: false,
    data: []
  }];
  public chartOptions = {
    chart: {
      type: "column"
    },
    title: {
      text: ""
    },
    xAxis: {
      categories: []
    },
    yAxis: {
      title: {
        text: ""
      }
    }    ,
    plotOptions: {
      column: {
        dataLabels: {
          enabled: true
        }
      }
    },
    series: this.data
  };
  public filterInfo = {
    filterStatus: "",
    filterType: ""
  }

  private requestUtilityModel: string = "requestUtilityModel";
  private discardUtilityModel: string = "discardConfirmationUtilityModel";
  private approveRejectModal: string = "approveModalFromList";
  private utilityId: any;
  private actionApproveReject: string = "ApproveReject";
  private actionCancel: string = "Cancel";
  private actionViewComments: string = "ViewComments";
  private statusPendingApproval: string = "Submitted - Waiting for approval";
  private statusApproved: string = "Approved";
  private statusRejected: string = "Rejected";
  private statusCancelled: string = "Cancelled";
  private submittedScanInProgress: string = "Submitted - Scan In Progress";
  private autoRejected: string = "Auto-Rejected";
  private aWSUtilityManagementBucketName: string = "AWSUtilityManagementBucketName";
  private aWSUtilityManagementUtilityFolder: string = "AWSUtilityManagementUtilityFolder";
  private aWSUtilityManagementHelperFolder: string = "AWSUtilityManagementHelperFolder";
  private utilityFileDataType: string = "UTILITY";
  private helperFileDataType: string = "HELPER";
  private modalTitleForApproveReject: string = "Approve/Reject Utility";
  private modalTitleForCancel: string = "Cancel Utility";
  private modalTitleForComments: string = "Comments";
  private acceptedHelperFiles: string[] = ["doc", "docx", "xls", "xlsx", "ppt", "pptx", "pdf", "csv", "txt", "html", "zip", "jpg", "jpeg", "png"];
  private notAcceptedUtilityFiles: string[] = ["exe", "dll"];
  private isDownloadInProgress: boolean = false;
  public constructor(
    public utilitiesService: UtilitiesService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    private fb: FormBuilder,
    private approveRejectFb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public authService: AuthService
  ) {
    super(utilitiesService, ViewModelType.Edit, ngxSmartModalService, route, authService);
    this.viewModelDataGrid = new DataGridExtendedViewModel();
    this.initializeGridColDef();
    this.initUtilityRequestForm();
    this.initUtilityApproveRejectForm();
  }

  public init(param?) {
    this.filterInfo.filterType = param.filterType;
    this.filterInfo.filterStatus = param.filterStatus;
    this.userId = this.authService.getUserId();
    this.userRolePermission = this.getUserClaims() || {};
    this.subscribeToCreateSuccessMessage();
    this.subscribers();
    this.setDataTableConfigurations();
    this.getUtilitiesList();
    return observableFrom([]);
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  public initUtilityRequestForm(): void {
    this.utilitiesRequestModelForm = this.fb.group({
      name: ["", [Validators.required, super.validateSpecialCharactersandWhiteSpace(3, 100)]],
      description: ["", [Validators.required, super.validateSpecialCharactersandWhiteSpace(3, 1000)]],
      isPublic: [true, [Validators.required]],
      utilityFileID: [""],
      helperFileID: [""]
    });
  }
  public initUtilityApproveRejectForm(): void {
    this.utilitiesApproveRejectForm = this.approveRejectFb.group({
      comments: ["", [Validators.required, super.validateSpecialCharactersandWhiteSpace(3, 1000)]],
    });
  }

  public initializeGridColDef() {
    const name = {
      headerName: 'Name',
      field: 'name',
      sort: 'asc',
      editable: false,
      sortable: true,
      filter: true
    };
    this.viewModelDataGrid.columnDefs.push(name);
    const description = {
      headerName: 'Description',
      field: 'description',
      sortable: true,
      editable: false
    };
    this.viewModelDataGrid.columnDefs.push(description);
    const availableForAllUsers = {
      headerName: 'Available For All Users',
      field: 'availableForAllUsers',
      sortable: true,
      editable: false
    };
    this.viewModelDataGrid.columnDefs.push(availableForAllUsers);

    const utilityFileName = {
      headerName: 'Utility File',
      field: 'utilityFileName',
      editable: false,
      sortable: true,
      cellRenderer: (params: any) => {
        if (params.value != null && params.data.rowDetails.utilityFile != null && params.data.rowDetails.currentStatus.name != this.submittedScanInProgress && params.data.rowDetails.currentStatus.name != this.autoRejected && params.data.rowDetails.currentStatus.name != this.statusCancelled) {
          return `<u class="hyperlink" style ="color: blue; cursor: pointer;"  data-action-type="downloadCleanFileClicked">` + params.value + `</u>`;
        } else {
          return params.value;
        }
      }
    };
    this.viewModelDataGrid.columnDefs.push(utilityFileName);

    const helperFileName = {
      headerName: 'Helper File',
      field: 'helperFileName',
      editable: false,
      sortable: true,
      cellRenderer: (params: any) => {
        if (params.value != null && params.data.rowDetails.helperFile != null && params.data.rowDetails.currentStatus.name != this.submittedScanInProgress && params.data.rowDetails.currentStatus.name != this.autoRejected && params.data.rowDetails.currentStatus.name != this.statusCancelled) {
          return `<u class="hyperlink" style ="color: blue; cursor: pointer;"  data-action-type="downloadCleanFileClicked">` + params.value + `</u>`;
        } else {
          return params.value;
        }
      }
    };
    this.viewModelDataGrid.columnDefs.push(helperFileName);

    const status = {
      headerName: 'Status',
      field: 'status',
      sortable: true,
      editable: false
    };
    this.viewModelDataGrid.columnDefs.push(status);

    const requestedBy = {
      headerName: 'Requested By',
      field: 'requestedBy',
      sortable: true,
      editable: false
    };
    this.viewModelDataGrid.columnDefs.push(requestedBy);

    const requestedOn = {
      headerName: 'Requested On',
      field: 'requestedOn',
      editable: false,
      sortable: true,
      isDate: true,
      cellRenderer: (params: any) => {
        return moment(params.value).format(config.APPLICATION_DATE_TIME_FORMAT)
      }
    };
    this.viewModelDataGrid.columnDefs.push(requestedOn);

    const reviewedBy = {
      headerName: 'Reviewed By',
      field: 'reviewedBy',
      sortable: true,
      editable: false
    };
    this.viewModelDataGrid.columnDefs.push(reviewedBy);

    const reviewedOn = {
      headerName: 'Reviewed On',
      field: 'reviewedOn',
      editable: false,
      sortable: true,
      isDate: true,
      cellRenderer: (params: any) => {
        if (params.value != null) {
          return moment(params.value).format(config.APPLICATION_DATE_TIME_FORMAT)
        } else {
          return null;
        }

      }
    };
    this.viewModelDataGrid.columnDefs.push(reviewedOn);

    const action = {
      headerName: 'Action',
      field: 'action',
      editable: false,
      cellRenderer: (params: any) => {
        let template = ``;
        if (params.value === this.actionApproveReject)
          template = `<a routerLink="" class="data-grid-padding-right"><img alt="Approve/Reject" title="Approve/Reject" src="../../../assets/images/icons8-clipboard-approve-32.png" class="data-grid-image-medium" data-action-type="approveRejectClicked"/></a>`;
        else if (params.value === this.actionCancel)
          template = `<a routerLink="" class="data-grid-padding-right"><img alt="Cancel Request"  title="Cancel Request"  src="../../../assets/images/cancel.png" class="data-grid-image-medium" data-action-type="approveRejectClicked"/></a>`;
        else if (params.value === this.actionViewComments)
          template = `<i title="View Comments" class="icon material-icons cursor text-primary" data-action-type="approveRejectClicked">visibility</i>`;

        return template;

      }
    };
    this.viewModelDataGrid.columnDefs.push(action);
  }

  // data-table configurations for project management
  public setDataTableConfigurations(): void {
    this.viewModelDataGrid.dataTableConfiguration = {
      destroy: true,
      columnDefs: [{ targets: [0], visible: false, searchable: false }],
      order: [[1, "asc"]]
    };
  }

  public clearFilter(): void {
    this.filterInfo.filterStatus = "";
    this.filterInfo.filterType = "";
    this.getUtilitiesList();
  }

  public getUtilitiesList(): void {
    const isUserHasApproverAccess = (this.userRolePermission && this.userRolePermission?.canExecute) ? true : false;
    this.utilitiesService.getUtilities(isUserHasApproverAccess).subscribe(
      data => {
        if (data) {
          this.utilitiesOriginalData = data;
          this.utilities = [];
          if (this.filterInfo.filterStatus || this.filterInfo.filterType) {
            this.utilitiesOriginalData = _.filter(this.utilitiesOriginalData, utility => {
              if (utility.currentStatus.name === this.filterInfo.filterStatus) {
                // show only waiting for approval request
                if (this.filterInfo.filterType === UTILITY_FILTER_TYPE.MY_SUBMITTED_FORMS) {
                  // show only my request waiting for approval
                  return utility.requestedBy === this.userId;
                } else {
                  // exclude my submitted request
                  return utility.requestedBy !== this.userId;
                }
              }
              return false;
            });

          }
          _.each(this.utilitiesOriginalData, (utility) => {
            let objUtility: any = {};
            objUtility.rowDetails = utility;
            objUtility.name = utility.name;
            objUtility.description = utility.description;
            objUtility.availableForAllUsers = utility.isPublic ? "Yes" : "No";
            objUtility.utilityFileName = utility.utilityFile.originalFileName;
            objUtility.helperFileName = (utility.helperFile) ? utility.helperFile.originalFileName : null;
            objUtility.status = (utility.currentStatus.name == this.autoRejected) ? this.statusRejected : utility.currentStatus.name;
            objUtility.requestedBy = utility.requestedByNavigation.firstName.concat(" ", utility.requestedByNavigation.lastName);
            objUtility.requestedOn = utility.requestedDateTime;
            const canShowModifiedDetails = this.canShowModifiedDetails(utility);
            objUtility.reviewedBy = canShowModifiedDetails ? utility.lastModifiedByNavigation.firstName.concat(" ", utility.lastModifiedByNavigation.lastName) : null;
            objUtility.reviewedOn = canShowModifiedDetails ? utility.lastModifiedDate : null;

            if (this.userRolePermission?.canUpdate && (utility.currentStatus.name === this.statusPendingApproval || utility.currentStatus.name === this.submittedScanInProgress) && utility.userId === this.userId) {
              objUtility.action = this.actionCancel; //Show the cancel option for User who requested the Utility
            } else if (this.userRolePermission?.canExecute && utility.currentStatus.name === this.statusPendingApproval && utility.userId !== this.userId) {
              objUtility.action = this.actionApproveReject; //Showing the Approve/Reject option for User who has execute access and the request should not be created by own
            } else if (utility.currentStatus.name && utility.currentStatus.name !== this.statusPendingApproval && utility.currentStatus.name !== this.submittedScanInProgress) {
              objUtility.action = this.actionViewComments; //Showing the View comment option for the Approved/Rejected records
            }
            this.utilities.push(objUtility);
          });

          this.viewModelDataGrid.setRowsData(this.utilities);
          this.bindChartData();
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public subscribeToCreateSuccessMessage(): void {
    this.utilitiesService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      this.alertService.success(message, toStable);
    });
  }


  public submitUtilityRequest(): Promise<void> {
    if (!this.utilitiesRequestModelForm.valid) {
      this.updateMessageData("error", ValidationMessages.formValidError, true);
      return;
    }
    const requestData = this.utilitiesRequestModelForm.value.name;
    this.utilitiesService.checkUtilityRequestNameUnique(requestData).subscribe(data => {
      if (data && data.isSuccess) {
        this.uploadUtilityAndHelperFile();
      }
    }, (error) => {
      if (error.error && error.error.errorList.length > 0) {
        this.updateMessageData("error", error.error.message, true);
      } else {
        this.updateMessageData("error", ValidationMessages.utilityRequestCreateError, true);
      }
    });
  }

  public openRequestUtilityModel(): void {
    this.modalType = "Create";
    this.utilitiesRequestModelForm.patchValue({
      name: "",
      description: "",
      isPublic: true,
      utilityFileID: null,
      helperFileID: null,
    });
    this.utilityFile = null;
    this.helpFile = null;
    this.isShowMessage = false;
    this.messageData = undefined;
    this.ngxSmartModalService.getModal(this.requestUtilityModel).open();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
  }

  public openApproveRejectModal(): void {
    this.utilitiesApproveRejectForm.patchValue({
      comments: "",
    });
    this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
    this.ngxSmartModalService.getModal(this.approveRejectModal).open();
  }

  public closeApproveRejectModal(): void {
    this.utilitiesApproveRejectForm.reset();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
    this.ngxSmartModalService.getModal(this.approveRejectModal).close();
  }

  public closeRequestUtilityModel(): void {
    if (this.utilitiesRequestModelForm.dirty) {
      this.ngxSmartModalService.getModal(this.discardUtilityModel).open();
    } else {
      this.utilitiesRequestModelForm.reset();
      this.clearUtilityOrHelpFileUploadControl();
      this.ngxSmartModalService.getModal(this.requestUtilityModel).close();
    }
  }

  public discardConfirmed(): void {
    this.utilitiesRequestModelForm.reset();
    this.clearUtilityOrHelpFileUploadControl();
    this.ngxSmartModalService.getModal(this.discardUtilityModel).close();
    this.ngxSmartModalService.getModal(this.requestUtilityModel).close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }

  public closeDiscardConfirmation(): void {
    this.ngxSmartModalService.getModal(this.discardUtilityModel).close();
  }

  public fileChanged(event: EventTarget, isUtilityFile: boolean): void {
    const eventObj: MSInputMethodContext = <MSInputMethodContext>event;
    const target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    const files: FileList = target.files;
    const fileData = files[0];
    const fileSize = Math.round((fileData.size / 1024));
    if (fileSize > UTILITY_MAX_FILE_SIZE) {
      this.updateMessageData("error", ValidationMessages.fileSizeError, true);
      if (isUtilityFile) {
        (<HTMLInputElement>window.document.getElementById('utilityFileUpload')).value = "";
      } else {
        (<HTMLInputElement>window.document.getElementById('helpFileUpload')).value = "";
      }
      return;
    }
    const fileType = fileData.name.split('.').pop();
    if (isUtilityFile) {
      if (this.notAcceptedUtilityFiles.includes(fileType)) {
        const constructedMsg = (ValidationMessages.utilityFileTypeInvalidError + this.notAcceptedUtilityFiles);
        this.updateMessageData("error", constructedMsg, false);
        (<HTMLInputElement>window.document.getElementById('utilityFileUpload')).value = "";
        return;
      }
      this.utilityFile = fileData
    } else {
      if (!this.acceptedHelperFiles.includes(fileType)) {
        const constructedMsg = (ValidationMessages.fileTypeInvalidError + this.acceptedHelperFiles);
        this.updateMessageData("error", constructedMsg, false);
        (<HTMLInputElement>window.document.getElementById('helpFileUpload')).value = "";
        return;
      }
      this.helpFile = fileData;
    }
  }

  private uploadUtilityAndHelperFile(): void {
    this.updateMessageData("info", ValidationMessages.utilityReqCreationProgress, false);
    const tdHeaderInformation = this.transformToDataModel();
    this.utilitiesService.uploadUtilityOrHelpFile(tdHeaderInformation, this.utilityFile, this.helpFile).subscribe(data => {
      if (data && data.isSuccess) {
        if (data.payload) {
          this.utilitiesRequestModelForm.patchValue({
            utilityFileID: data.payload.utilityFileId
          });
          if(this.helpFile) {
            this.utilitiesRequestModelForm.patchValue({
              helperFileID: data.payload.helperFileId
            });
          }
        }
        const requestData = this.utilitiesRequestModelForm.value;
        this.utilitiesService.submitUtilityRequest(requestData).subscribe(data => {
          if (data) {
            this.isShowMessage = false;
            this.messageData = undefined;
            this.getUtilitiesList();
            this.discardConfirmed();
            this.alertService.success(data.message, true);
          }
        }, (error) => {
          if (error.error && error.error.errorList.length > 0) {
            this.updateMessageData("error", error.error.message, true);
          } else {
            this.updateMessageData("error", ValidationMessages.utilityRequestCreateError, true);
          }
        });
      } else {
        this.updateMessageData("error", ValidationMessages.fileUploadError, true);
      }
    }, (error) => {
      if (error.error && error.error.error) {
        this.updateMessageData("error", error.error.error, true);
      } else {
        this.updateMessageData("error", ValidationMessages.fileUploadError, true);
      }
    });
  }

  public transformToDataModel(): any {
    const data = [];
    if(this.utilityFile) {
     const utilityFileObj = {
        id: undefined,
        file_name: this.utilityFile.name,
        fileUploadId: undefined,
        file_type: this.utilityFile.type,
        lastModifiedBy: this.userId,
        isUtilityFileUpload: true,
        bucketNameKey: this.aWSUtilityManagementBucketName,
        uploadedFileDataType: this.utilityFileDataType,
        folderPathKey: this.aWSUtilityManagementUtilityFolder,
        utilityFile: this.utilityFile,
        isUtilityFile: true
      };
      data.push(utilityFileObj);
    }
    if(this.helpFile) {
      const helpFileObj = {
         id: undefined,
         file_name: this.helpFile.name,
         fileUploadId: undefined,
         file_type: this.helpFile.type,
         lastModifiedBy: this.userId,
         isUtilityFileUpload: true,
         bucketNameKey: this.aWSUtilityManagementBucketName,
         uploadedFileDataType: this.helperFileDataType,
         folderPathKey: this.aWSUtilityManagementHelperFolder,
         helpFile: this.helpFile,
         isUtilityFile: false
       };
       data.push(helpFileObj);
     }
    return data;
  }

  public approveOrRejectOrCancel(event: any): void {
    const requestData = {
      "utilityRequestId": this.utilityId,
      "status": event,
      "actionComments": this.utilitiesApproveRejectForm.value.comments
    };

    this.utilitiesService.approveOrRejectOrCancel(requestData).subscribe(data => {
      if (data) {
        this.getUtilitiesList();
        this.closeApproveRejectModal();
        this.alertService.success(data.message, true);
      }
    }, (error) => {
      if (error.error && error.error.error) {
        this.updateMessageData("error", error.error.error, true);
      } else {
        this.updateMessageData("error", ValidationMessages.utilityRequestCreateError, true);
      }
    });

  }

  private updateMessageData(type: string, message: string, toStable: boolean): void {
    this.isShowMessage = true;
    this.messageData = {
      type: type,
      message: message
    };
    if (toStable) {
      setTimeout(() => {
        this.isShowMessage = false;
      }, 10000);
    }
  }

  private clearUtilityOrHelpFileUploadControl(): void {
    (<HTMLInputElement>window.document.getElementById('utilityFileUpload')).value = "";
    (<HTMLInputElement>window.document.getElementById('helpFileUpload')).value = "";
    this.utilityFile = null;
    this.helpFile = null;
  }

  private subscribers(): void {
    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.columnClicked:
          this.utilityId = e.eventData.rowDetails.rowDetails.id;
          break;
        case GridConfig.downloadCleanFileClicked:
          if (this.isDownloadInProgress) {
            this.alertService.error(ValidationMessages.fileDownloadProgress, true);
            return;
          }
          this.utilityId = e.eventData.rowDetails.rowDetails.id;
          this.alertService.info(ValidationMessages.fileDownloadProgress, true);
          let uploadedFileId = "";
          let fileName = "";
          let fileType = "";
          if (e.eventData.columnId && e.eventData.columnId == "utilityFileName") {
            uploadedFileId = e.eventData.rowDetails.rowDetails.utilityFile.id;
            fileName = e.eventData.rowDetails.rowDetails.utilityFile.originalFileName;
            fileType = e.eventData.rowDetails.rowDetails.utilityFile.originalFileName.split('.').pop();
          } else if (e.eventData.columnId && e.eventData.columnId == "helperFileName") {
            uploadedFileId = e.eventData.rowDetails.rowDetails.helperFile.id;
            fileName = e.eventData.rowDetails.rowDetails.helperFile.originalFileName;
            fileType = e.eventData.rowDetails.rowDetails.helperFile.originalFileName.split('.').pop();
          }
          this.downloadFile(uploadedFileId, fileType, fileName);
          break;
        case GridConfig.approveRejectClicked:
          this.utilityId = e.eventData.rowDetails.rowDetails.id;
          this.isApproveOrReject = false;
          this.isViewComment = false;
          if (e.eventData.rowDetails && e.eventData.rowDetails.action == this.actionApproveReject) {
            this.modalTitle = this.modalTitleForApproveReject
            this.isApproveOrReject = true;
          }
          if (e.eventData.rowDetails && e.eventData.rowDetails.action == this.actionCancel) {
            this.modalTitle = this.modalTitleForCancel
            this.isApproveOrReject = false;
          }
          if (e.eventData.rowDetails && e.eventData.rowDetails.action == this.actionViewComments) {
            const currentStatusId = e.eventData.rowDetails.rowDetails.currentStatusId;
            const utilityRequestStatusAudits = e.eventData.rowDetails.rowDetails.utilityRequestStatusAudits.find(element => element.statusId === currentStatusId);
            this.bindComments(utilityRequestStatusAudits.actionComments);
          }
          this.openApproveRejectModal();
          break;

        default:
      }
    }, error => {
      this.alertService.error(error);
    });
  }

  private downloadFile(uploadedFileId: string, fileType: string, fileName: string): void {
    this.utilitiesService.downloadFile(uploadedFileId, fileType)
      .subscribe(
        blob => {
          const a = window.document.createElement("a");
          a.href = window.URL.createObjectURL(blob);
          a.download = fileName;
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
          this.isDownloadInProgress = false;
        },
        error => {
          this.alertService.error(error);
        });
  }

  private bindChartData(): void {
    let grouped_data = _.groupBy(this.utilities, "status");
    this.xAxisValues = [];
    this.chartData = [];
    _.each(grouped_data, (value, key) => {
      let objChartData: any = {};
      objChartData.name = key;
      objChartData.y = value.length;
      switch (key) {
        case this.statusApproved:
          objChartData.color = UTILITY_CHART_COLORS.APPROVED;
          break;
        case this.statusCancelled:
          objChartData.color = UTILITY_CHART_COLORS.CANCELLED;
          break;
        case this.statusRejected:
          objChartData.color = UTILITY_CHART_COLORS.REJECTED;
          break;
        case this.autoRejected:
          objChartData.color = UTILITY_CHART_COLORS.AUTO_REJECTED;
          break;
        case this.submittedScanInProgress:
          objChartData.color = UTILITY_CHART_COLORS.SUBMITTED_SCAN_IN_PROGRESS;
          break;
        default:
          break;
      }
      this.xAxisValues.push(key);
      this.chartData.push(objChartData);
    });
    this.chartOptions = {
      chart: {
        type: "column"
      },
      title: {
        text: ""
      },
      xAxis: {
        categories: this.xAxisValues
      },
      yAxis: {
        title: {
          text: "Count"
        }
      },
      plotOptions: {
        column: {
          dataLabels: {
            enabled: true
          }
        }
      },
      series: [{ name: "Utilities", data: this.chartData, showInLegend: false }],

    };
  }

  private bindComments(comments: string): void {
    this.approveOrRejectComment = comments;
    this.modalTitle = this.modalTitleForComments
    this.isViewComment = true;
  }

  private canShowModifiedDetails(utilityDetails: any): boolean {
    let canShowModifiedDetails = false;
    if (utilityDetails.currentStatus.name === this.statusApproved || utilityDetails.currentStatus.name === this.statusRejected || utilityDetails.currentStatus.name === this.autoRejected) {
      canShowModifiedDetails = true;
    }
    return canShowModifiedDetails;
  }

  public onPopUpClose(modalType: string): void {
    this.ngxSmartModalService.getModal(modalType).close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }


}
