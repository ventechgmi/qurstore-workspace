
import { map } from "rxjs/operators";
import { EntityBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { Observable, Subject, of } from "rxjs";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ActivatedRoute } from "@angular/router";
import { ROUTES_CONFIG_MAP, MODULE_BASE_URL_MODULE_ID_MAP } from "src/app/utils/config";
import { AbstractControl, ValidatorFn, FormGroup } from "@angular/forms";
import { UtilitiesService } from "../../services/utilities.service";
import { AuthService } from 'src/app/utils/auth.service';

export class UtilitiesViewModel extends EntityBaseViewModel<"", UtilitiesService> {
    public moduleId: string;
    public moduleAccess;
    public moduleInfo;
    public updateUserListSubject = new Subject<any>();
    public userDetailSelection = new Subject<any>();
    private errorDictionary: Map<string, any[]> = new Map();
    

    public constructor(
        public utilitiesService: UtilitiesService,
        public viewModelType: ViewModelType,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute,
        public authService: AuthService)
    {
        super(utilitiesService, viewModelType);
        this.entityName = "utility";
        this.errorDictionary.set("name", [
            { key: "required", message: "Name is required" },
            { key: "minlength", message: "Name should be between 3 and 100 characters" },
            { key: "maxlength", message: "Name should be between 3 and 100 characters" },
            { key: "nameTaken", message: "Name already exists" }]);

        this.errorDictionary.set("description", [
            { key: "required", message: "Description is required" },
            { key: "hasOnlyWhiteSpace", message: "Description is required" },
            { key: "minlength", message: "Description should be between 3 and 1000 characters" },
            { key: "maxlength", message: "Description should be between 3 and 1000 characters" }]);
    }


    public getUserClaims() {
        const baseUrl = ROUTES_CONFIG_MAP.UTILITY_MANAGEMENT.BASE_URL;
        this.moduleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
        this.moduleInfo = this.authService.getModuleAccess(this.moduleId) || null;
        this.moduleAccess = this.moduleInfo ? this.moduleInfo.RolePermissions : {};
    
        return this.moduleAccess;
    }

    public noWhitespaceValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            const isWhitespace = (control.value || "").trim().length === 0;
            const isValid = !isWhitespace;
            return isValid ? null : { hasOnlyWhiteSpace: true };
        };
    }

    /*
    This method validates the below
    1. validates the control has only white spaces since most of the name fields should not allow just white spaces
    2. validates the control whether it has only special characters.

    */
    public validateSpecialCharactersandWhiteSpace(lengthMin: number, lengthMax: number): ValidatorFn {
        // tslint:disable-next-line: cyclomatic-complexity
        return (control: AbstractControl): { [key: string]: any } => {
            const pattern = "[A-Za-z0-9]";
            if ((control.value || "").length === 0) {
                return null;
            } else if ((control.value || "").trim().length === 0) {
                return { required: true };
            } else if (((control.value || "").trim().length < lengthMin) && ((control.value || "").trim().length !== 0)) {
                return { minlength: true };
            } else if (((control.value || "").trim().length > lengthMax)) {
                return { maxlength: true };
            } else if (!control.value.match(pattern)) { //check if the input doest not have any alphabets or in other words input has only special characters
                //return "Don't enter only special characters";
                return { hasOnlySpecialCharaters: true };
            }
        };
    }

    public errorFor(form: FormGroup, propertyName: string): string | void {
        if (propertyName) {
            if (form.controls[propertyName] && form.controls[propertyName].errors) {
                return this.makeErrorMessageFor(propertyName, Object.keys(form.controls[propertyName].errors)[0]);
            }
        }
    }

    private makeErrorMessageFor(propertyName: string, errorKey: string): string {
        return this.errorDictionary.get(propertyName).filter(data => data.key === errorKey)[0].message;
    }

}
