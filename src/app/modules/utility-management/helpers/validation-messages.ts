export class ValidationMessages {
    public static readonly RequiredMessage = " is required.";
    public static readonly MinMaxLengthMessage = " must be between 3 and 20 characters..";
    public static readonly InvalidMessage = " is invalid.";
    public static readonly EmailMatchMessage = "Email and ConfirmEmail must be matched.";

    public static readonly emailalreadyExists = "Email address is already in use. Please try with another one.";
    public static readonly saveSuccessMessage = "User saved successfully.";
    public static readonly createSuccessMessage = "User created successfully.";
    public static readonly updateSuccessMessage = "User updated successfully.";
    public static readonly deleteSuccessMessage = "User deleted successfully.";
    public static readonly atLeastOneRoleRequiredMsg = "Please select at least one role for this user";
    public static readonly somethingWentWrong = "Something went wrong. Please try again.";
    public static readonly enterUsernameError = "Please enter username";

    // regex patter
    public static readonly EmailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    public static readonly PhoneNumberPattern = "^(\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$";
    public static readonly zipCodePattern = "^[0-9]{5}(?:-[0-9]{4})?$";
    public static readonly fileSizeError = "File size should not exceed 12MB";
    public static readonly fileUploadProgress = "File upload is in progress. Please wait...";
    public static readonly utilityReqCreationProgress = "Utility request creation is in progress. Please wait...";
    public static readonly fileUploadSuccess = "File has been uploaded successfully";
    public static readonly fileUploadError = "Error with uploading the file";
    public static readonly fileTypeInvalidError = "File type is not valid. Supported format(s) are ";
    public static readonly utilityFileTypeInvalidError = "File type is not valid.";
    public static readonly utilityRequestCreateError = "Error with creating utility request";
    public static readonly formValidError = "Form is not valid. Kindly fill all the required fields";
    public static readonly fileDownloadProgress = "File download is in progress. Please wait...";
}
