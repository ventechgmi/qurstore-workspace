import { NgModule } from "@angular/core";
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { DataGridModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';

import { UtilitiesService } from "./services/utilities.service";
import { UtilitiesListViewComponent } from "./views/utilities/utilities-list/utilities-list.component";

import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatButtonModule } from "@angular/material/button";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";
import { utilitiesManagementRoutingModule } from './utility-management-routing-module';
import { HighchartsChartModule } from 'highcharts-angular';
@NgModule(
    {
        declarations: [
            UtilitiesListViewComponent,
        ],
        imports: [
            SharedModules,
            NgSelectModule,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            utilitiesManagementRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatButtonModule,
            DataGridModule,
            MatCheckboxModule,
            MatSlideToggleModule,
            NgxSmartModalModule.forRoot(),
            RecaptchaModule,
            RecaptchaFormsModule,
            HighchartsChartModule
        ],
        exports: [
            UtilitiesListViewComponent
        ],
        providers: [
            AlertService,
            UtilitiesService,
            NgxSmartModalService
        ]
    }
)
export class UtilitiesManagementModule {

}
