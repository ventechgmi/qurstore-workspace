
export interface CostEventType {
    type: CostEventTypes;
    tagTypes?: any[];
    tagTypeIds?: string[];
    tagTypeValues?: any[];
    data?: any;
    range?: any;
    parentTag?: any;
    tags?: any;
    IsAllProjectTags?: boolean;
    IsOnlyUserTag?: boolean;
}

export enum CostEventTypes {
    TAGS_LOADED = "TAGS_LOADED",
    TAGS_TYPES_VALUES_LOADED = "TAGS_TYPES_VALUES_LOADED",
    LOAD_COST_DATA = "LOAD_COST_DATA",
    FETCH_COST_DATA = "FETCH_COST_DATA",
    LOAD_USER_COST_DATA = "LOAD_USER_COST_DATA",
    FETCH_USER_COST_DATA = "FETCH_USER_COST_DATA",
    DRILL_DOWN = "DRILL_DOWN",
    PERIOD_CHANGED = "PERIOD_CHANGED",
    TAG_CHANGE = "TAG_CHANGE",
}

export enum CostPeriodTypes {
    THIS_WEEK = "THIS_WEEK",
    THIS_MONTH = "THIS_MONTH",
    THIS_QUARTER = "THIS_QUARTER",
    THIS_YEAR = "THIS_YEAR"
}

export const periods = [
    { name: "This Week", id: "THIS_WEEK" },
    { name: "This Month", id: "THIS_MONTH" },
    { name: "This Quarter", id: "THIS_QUARTER" },
    { name: "This Year", id: "THIS_YEAR" },
]