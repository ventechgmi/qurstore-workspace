import { AfterViewInit, Component, ElementRef, HostListener, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AlertService } from "../../../../utils/alert/alert.service";
import { CostService } from "../../../cost-management/services/cost.service";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AuthService } from 'src/app/utils/auth.service';
import { Subject, Subscription } from 'rxjs';
import { EventEmitter } from 'events';
import { CostWidgetViewModel } from '../viewmodels/cost-widget.viewmodel';
import { DashboardService } from 'src/app/modules/dashboard/services/dashboard.service';
import { CostEventType } from '../helper/cost-widget.model';

@Component({
  selector: 'app-cost-widget-view',
  templateUrl: './cost-widget-view.component.html',
  styleUrls: ['./cost-widget-view.component.scss'],
  providers: [AlertService, CostWidgetViewModel]
})
export class CostWidgetViewComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild("serviceChartParent", { static: false }) serviceChartParent: ElementRef;
  @Input() costDataEvent: Subject<CostEventType>;
  @Input() isUserCost: boolean;
  @Input() headerText: string;

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    this.costWidgetViewModel.updateChartsWidth();
  }

  public tagList: any;
  public dataTable: any;
  public costRange: any;
  public tagCreateModalRef: any;
  private subscriptions = new Subscription();
  constructor(
    public costWidgetViewModel: CostWidgetViewModel,
    public dashboardService: DashboardService,
    public alertService: AlertService,
    public authService: AuthService,
    private route: ActivatedRoute,
    public ngxSmartModalService: NgxSmartModalService) { }

  ngAfterViewInit(): void {
    this.costWidgetViewModel.serviceChartParent = this.serviceChartParent;
    this.costWidgetViewModel.updateChartsWidth();
  }

  public ngOnInit(): void {
    this.route.params
      .subscribe(() => {
        const userId = this.authService.getUserId();
        this.costWidgetViewModel.alertService = this.alertService;
        this.costWidgetViewModel.init({
          userId,
          costDataEvent: this.costDataEvent,
          isUserCost: this.isUserCost
        });
      });
  }

  public ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
    this.dashboardService.sendDashboardEvent("");
  }



}

