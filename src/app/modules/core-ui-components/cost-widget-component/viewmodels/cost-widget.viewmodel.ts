import { from as observableFrom, Observable, Subject, forkJoin } from "rxjs";
import { Injectable, OnInit, ChangeDetectorRef, ElementRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import lodash from "lodash";
import moment from "moment";
import { CostViewModel } from "./cost.viewmodel";
import { Router, ActivatedRoute } from "@angular/router";
import config from "../../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import PieChartConfig from "../../../../utils/chart-config-json/pie-chart.json";
import { FormValidationMessages } from "../helper/validation-messages";
import { v4 as uuid } from "uuid";
import * as Highcharts from "highcharts";
import Drilldown from "highcharts/modules/drilldown";
import { Constance } from "../helper/constance";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { DataGridViewModel } from "../../data-grid";
import { AlertService } from "src/app/utils/alert/alert.service";
import { CostEventType, CostEventTypes, CostPeriodTypes, periods } from "../helper/cost-widget.model";
import { DataComparatorHelper } from "src/app/utils/helper/date-comparator.helper";
import { FormBuilder, FormGroup } from "@angular/forms";
import { FormatterHelper } from "src/app/utils/helper/formatter.helper";
const noData = require('highcharts/modules/no-data-to-display')
noData(Highcharts)
Drilldown(Highcharts);

@Injectable()
export class CostWidgetViewModel extends CostViewModel {

  public serviceChartParent: ElementRef;
  public maxDate = moment().format(config.DB_DATE_FORMAT);
  public Highcharts = Highcharts;
  public costTypeCostDetails: any;
  public serviceTypeList: any;
  public periods = periods;
  public selectedPeriod: string;
  // number with precision ex: 5646.65
  public activeDrillDownPoint: any;
  public totalServiceTypeCost: string;
  public serviceTypeChartConfig;
  public serviceTypeViewModelDataGrid: DataGridViewModel;
  public serviceChart: any;
  public ValidationMessages = FormValidationMessages;
  public alertService: AlertService;
  public drillDownStateStack = [];
  public drillDownTotalAmountStack = [];
  public selectedTagName = "";
  public selectedTagValueName = "";
  public isShowGrid = false;
  public range = {
    startDate: moment().startOf('week').add("days", 1),
    endDate: moment()
  };
  public settings = {
    text: "--Select--",
    enableSearchFilter: true,
    enableFilterSelectAll: false,
    enableCheckAll: true,
    maxHeight: 250,
    badgeShowLimit: 5,
    singleSelection: false
  };
  public serviceChartCallback = (chart) => {
    this.serviceChart = chart;
  }
  public isUserCost: boolean;
  public tagValueList: any;
  public costRangeText: string;
  public tagTypes: any[] = [];
  public tagTypeValues: any[];
  public selectedTags: any[];
  public costOutputEventObs: Observable<any>;
  public tagTypeForm: FormGroup;

  private costOutputEventSubject = new Subject();
  protected costId: string;
  protected userId: string;
  protected serviceTypeCostDetailsMap: any;
  private errorDictionary: Map<string, any[]> = new Map();
  private costDataEvent: Subject<CostEventType>;
  protected tagTypeIdValueMap: any;
  protected activeDrillDownServiceTypeList: any;
  protected pristineTagValues: string[];
  protected serviceTypeHeader = [
    { headerName: "Service Type", field: "type" },
    { headerName: "Date", field: "startDate", isDate: true },
    {
      headerName: "Amount", field: "roundedAmount",
      valueFormatter: params => FormatterHelper.currencyFormatter(params.data.roundedAmount, config.CURRENCY_DOLLAR_SYMBOL),
      filter: 'agNumberColumnFilter'
    },
  ];
  protected organizationId: string;


  public constructor(
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    super();
    this.costOutputEventObs = this.costOutputEventSubject.asObservable();
    this.tagTypeForm = this.fb.group({
      selectedTags: []
    });
  }

  public init(param?): Observable<any> {
    this.isUserCost = param.isUserCost;
    this.costDataEvent = param.costDataEvent;
    this.costRangeText = Constance.COST_DATE_RANGE
      .replace("{startDate}", this.range.startDate.format(config.FULL_DATE_FORMAT))
      .replace("{endDate}", this.range.endDate.format(config.FULL_DATE_FORMAT));
    this.serviceTypeViewModelDataGrid = new DataGridViewModel();
    this.serviceTypeViewModelDataGrid.setPagination(true);
    this.serviceTypeViewModelDataGrid.setHeader(this.serviceTypeHeader, false, { canUpdate: false });
    this.serviceTypeViewModelDataGrid.defaultSortModel = [
      {
        colId: 'startDate',
        sort: 'desc',
      }
    ];
    this.initSubscribe(param.costDataEvent);
    this.setDataTableConfigurations();
    this.initCharts();

    this.tagTypeForm.controls.selectedTags.valueChanges
      .subscribe(change => {
        this.costDataEvent.next({
          type: CostEventTypes.TAG_CHANGE,
          parentTag: null,
          range: this.range,
          IsAllProjectTags: false,
          tagTypeIds: _.map(change, "id")
        });
        this.initGrid([]);
      })
    return observableFrom([]);
  }

  public onFilterChange(event: any): void {
    if (!event) { return; }
    this.range = {
      startDate: null,
      endDate: moment()
    }
    switch (event.id) {
      case CostPeriodTypes.THIS_WEEK: {
        this.range.startDate = moment().startOf('week').add("days", 1);
        break;
      }
      case CostPeriodTypes.THIS_MONTH: {
        this.range.startDate = moment().startOf('month').add("days", 1);
        break;
      }
      case CostPeriodTypes.THIS_QUARTER: {
        this.range.startDate = moment().quarter(moment().quarter()).startOf('quarter').add("days", 1);
        break;
      }
      case CostPeriodTypes.THIS_YEAR: {
        this.range.startDate = moment().startOf('year').add("days", 1);
        break;
      }
      default: { }
    }
    this.costRangeText = Constance.COST_DATE_RANGE
      .replace("{startDate}", this.range.startDate.format(config.FULL_DATE_FORMAT))
      .replace("{endDate}", this.range.endDate.format(config.FULL_DATE_FORMAT));
    this.costDataEvent.next({
      type: CostEventTypes.FETCH_COST_DATA,
      range: this.range,
      IsAllProjectTags: true,
      IsOnlyUserTag: this.isUserCost,
      tagTypeIds: _.map(this.tagTypeForm.controls.selectedTags.value, "id")
    });
    this.drillDownTotalAmountStack = [];
    this.initGrid([]);
  }

  public onDeSelectTagAll(): void {
    this.costDataEvent.next({
      type: CostEventTypes.TAG_CHANGE,
      parentTag: null,
      range: this.range,
      IsAllProjectTags: true,
      tagTypeIds: []
    });
  }

  // private methods
  private initSubscribe(dateEvent: Subject<CostEventType>): void {
    dateEvent.subscribe((event: CostEventType) => {
      switch (event.type) {
        case CostEventTypes.LOAD_COST_DATA: {
          this.onCostFetchSuccess(event.data, event.parentTag, event.tags);
          break;
        }
        case CostEventTypes.TAGS_LOADED: {
          this.tagTypes = event.tagTypes;
          this.tagTypeForm.controls.selectedTags.setValue(this.tagTypes);
          break;
        }
        default: { }
      }
    });
  }

  // data-table configurations for project management
  private setDataTableConfigurations(): void {
    this.serviceTypeViewModelDataGrid.dataTableConfiguration = {
      destroy: true,
      columnDefs: [{ targets: [0], visible: false, searchable: false }, { type: "num-html", targets: [2] }, { orderable: false, targets: 2, searchable: false }],
      order: [[1, "asc"]]
    };
  }

  private serviceChartDrillDown(event: any): void {
    const tag = {
      tag: event.point.options.tagName,
      tagValues: [event.point.options.name]
    }
    this.selectedTagName = event.point.options.tagName;
    this.selectedTagValueName = event.point.options.tagValue;
    if (event.point.options.sectionType === Constance.TAGGED_COST_NAME) {
      this.initGrid([]);
      this.costDataEvent.next({
        type: CostEventTypes.DRILL_DOWN,
        parentTag: tag,
        range: this.range,
        tagTypeIds: [],
        IsAllProjectTags: false
      });
      this.activeDrillDownPoint = event.point;
      this.totalServiceTypeCost = event.point.options.y;
      this.drillDownTotalAmountStack.push(event.point.options.y);
    } else if (event.point.options.sectionType === Constance.COMMON_COST_NAME || event.point.options.sectionType === Constance.AWS_USER_CRED_NAME) {
      const serviceTypes = this.isUserCost ? this.serviceTypeList : this.activeDrillDownServiceTypeList;
      let filteredServiceTypeList = _.filter(serviceTypes, (instanceType) => {
        return instanceType.sectionType === event.point.options.sectionType;
      });
      this.updateTableHeader(event.point.options.sectionType === Constance.AWS_USER_CRED_NAME);
      this.initGrid(filteredServiceTypeList);
      this.drillDownStateStack.push(filteredServiceTypeList);
    } else {
      this.costDataEvent.next({
        type: CostEventTypes.DRILL_DOWN,
        tagTypeIds: [],
        parentTag: tag,
        range: this.range,
        IsAllProjectTags: false
      });
      this.activeDrillDownPoint = event.point;
      this.totalServiceTypeCost = event.point.options.y;
      this.drillDownTotalAmountStack.push(event.point.options.y);
    }
  }

  private serviceChartDrillUp(): void {
    this.initGrid([]);
    this.drillDownTotalAmountStack.pop();
    this.totalServiceTypeCost = this.drillDownTotalAmountStack[this.drillDownTotalAmountStack.length - 1];
  }

  private initGrid(serviceTypeList: any[]): void {
    if (serviceTypeList) {
      this.serviceTypeViewModelDataGrid.setRowsData(_.map(serviceTypeList, serviceType => ({ ...serviceType, roundedAmount: serviceType.roundedAmount })) || [], false);
    }
    this.isShowGrid = !!serviceTypeList.length;
  }

  private calculateTotal(tagWithValueKeyServiceTypeCostDetailsMap: any): void {
    this.totalServiceTypeCost = this.getTotalCost(tagWithValueKeyServiceTypeCostDetailsMap).toFixed(2);
    this.drillDownTotalAmountStack.push(this.totalServiceTypeCost);
  }

  private getTotalCost(tagWithValueKeyCostDetailsMap: any): number {
    let total = 0;
    _.each(tagWithValueKeyCostDetailsMap, (costDetails) => {
      _.each(costDetails, costDetail => {
        total += costDetail.amount
      });
    });
    return total;
  }

  private updateTableHeader(isUserLevelData: boolean): void {
    if (isUserLevelData) {
      const serviceTypeHeader = [
        { headerName: "Service Type", field: "type" },
        { headerName: "User Name", field: "userName" },
        { headerName: "Date", field: "startDate", isDate: true },
        {
          headerName: "Amount",
          field: "roundedAmount",
          valueFormatter: params => FormatterHelper.currencyFormatter(params.data.roundedAmount, config.CURRENCY_DOLLAR_SYMBOL),
          filter: 'agNumberColumnFilter'
        },
      ];
      this.serviceTypeViewModelDataGrid.setHeader(serviceTypeHeader, false, { canUpdate: false });
      this.serviceTypeViewModelDataGrid.gridApi.refreshView();
    } else {
      const serviceTypeHeader = [
        { headerName: "Service Type", field: "type" },
        { headerName: "Date", field: "startDate", isDate: true },
        {
          headerName: "Amount",
          field: "roundedAmount",
          valueFormatter: params => FormatterHelper.currencyFormatter(params.data.roundedAmount, config.CURRENCY_DOLLAR_SYMBOL),
          filter: 'agNumberColumnFilter'
        },
      ];
      this.serviceTypeViewModelDataGrid.setHeader(serviceTypeHeader, false, { canUpdate: false });
      this.serviceTypeViewModelDataGrid.gridApi.refreshView();
    }

  }

  private onCostFetchSuccess(data: any, parentTag: any, tags: any): void {
    if (data && data.isSuccess) {
      if (!parentTag) {
        this.costTypeCostDetails = data.payload.costTypeNameTagValueModelMap;
        this.serviceTypeCostDetailsMap = this.costTypeCostDetails.SERVICE.tagWithValueNameCostDetailsMap;
        this.calculateTotal(this.serviceTypeCostDetailsMap);
        this.serviceTypeList = this.buildCostDetail(this.serviceTypeCostDetailsMap);
        this.format(this.serviceTypeList);
        this.initChartData(false);
        if (!this.serviceTypeList || (this.serviceTypeList && !this.serviceTypeList.length)) {
          this.alertService.info(FormValidationMessages.NO_COST_DATA_FOUND, true);
        }
      } else {
        const costTypeCostDetails = data.payload.costTypeNameTagValueModelMap;
        const serviceTypeCostDetailsMap = costTypeCostDetails.SERVICE.tagWithValueNameCostDetailsMap;
        this.activeDrillDownServiceTypeList = this.buildCostDetail(serviceTypeCostDetailsMap, parentTag);
        this.format(this.activeDrillDownServiceTypeList);
        this.updateTableHeader(true);
        this.initGrid([]);
        this.drillDownStateStack.push(this.activeDrillDownServiceTypeList);
        this.initChartDrillDown(this.activeDrillDownServiceTypeList, this.activeDrillDownPoint);
      }
    } else {
      this.alertService.error(data.message, true);
    }
  }

  private initChartDrillDown(serviceTypeList: any[], drillDownPoint: any): void {
    let totalCommonCost = 0;
    let totalUserCost = 0;

    _.each(serviceTypeList, instanceTypeDetail => {
      if (instanceTypeDetail.sectionType === Constance.COMMON_COST_NAME) {
        totalCommonCost += parseFloat(instanceTypeDetail.amount);
      } else {
        totalUserCost += parseFloat(instanceTypeDetail.amount);
      }
    });

    const drillDownSeries = {
      name: drillDownPoint.options.name,
      id: drillDownPoint.options.drilldown,
      data: [
        {
          sectionType: Constance.COMMON_COST_NAME,
          name: Constance.COMMON_COST_NAME,
          tagName: drillDownPoint.options.tagName,
          tagValue: drillDownPoint.options.name,
          drilldown: uuid(),
          y: Number(totalCommonCost.toFixed(2))
        },
        {
          sectionType: Constance.AWS_USER_CRED_NAME,
          name: Constance.USERS_NAME,
          tagName: drillDownPoint.options.tagName,
          tagValue: drillDownPoint.options.name,
          drilldown: uuid(),
          y: Number(totalUserCost.toFixed(2))
        }
      ]
    }

    if (this.serviceChart) {
      this.serviceChart.addSingleSeriesAsDrilldown(drillDownPoint, drillDownSeries);
      this.serviceChart.applyDrilldown();
    }
  }

  private buildCostDetail(costTypeCostDetailsMap: any, parentTag?: any): any[] {
    const costDetailList = [];
    _.each(costTypeCostDetailsMap, (costDetails, tagWithValueNameKey) => {
      _.each(costDetails, (costTypeCostDetail) => {
        const names = tagWithValueNameKey.split("_");
        let costDetail = {
          ...costTypeCostDetail,
          sectionType: "",
          tagName: tagWithValueNameKey === Constance.NO_TAG_NAME || tagWithValueNameKey === Constance.COMMON_COST_NAME
            ? tagWithValueNameKey : names[0],
          tagValueName: names[1] || ""
        }

        switch (costDetail.tagName) {
          case Constance.COMMON_COST_NAME: {
            costDetail.sectionType = Constance.COMMON_COST_NAME;
            costDetail.tagName = parentTag.tag;
            costDetail.tagValueName = parentTag.tagValues[0];
            break;
          }
          case Constance.NO_TAG_NAME: {
            costDetail.sectionType = Constance.NO_TAG_NAME;
            break;
          }
          case Constance.AWS_USER_CRED_NAME: {
            costDetail.sectionType = Constance.AWS_USER_CRED_NAME;
            costDetail.tagName = parentTag && parentTag.tag;
            costDetail.tagValueName = parentTag && parentTag.tagValues[0];
            break;
          }
          default: { }
        }

        costDetailList.push(costDetail);
      });
    });
    return costDetailList;
  }

  private initCharts(): void {
    this.serviceTypeChartConfig = lodash.cloneDeep(PieChartConfig);
    const serviceTypeChart: any = this.serviceTypeChartConfig;
    serviceTypeChart.chart.events = {
      drilldown: this.serviceChartDrillDown.bind(this),
      drillupall: this.serviceChartDrillUp.bind(this)
    };
    serviceTypeChart.xAxis = {
      type: "category"
    }
    serviceTypeChart.yAxis = {
      title: {
        text: "Amount in $"
      }
    }
  }

  private initChartData(isTagSelected: boolean): void {
    let serviceTypeChartData;
    if (isTagSelected) {
      serviceTypeChartData = this.buildSelectedTagsChartData(this.serviceTypeList, Constance.TAG_VALUE);
    } else {
      serviceTypeChartData = this.buildChartData(this.serviceTypeList, Constance.MAIN_SECTION);
    }
    this.serviceTypeChartConfig.series = serviceTypeChartData.series;
    this.serviceTypeChartConfig.drilldown.series = serviceTypeChartData.drilldownData;
    if (this.serviceChart) {
      if (this.serviceChart.series && this.serviceChart.series.length) {
        _.each(this.serviceChart.series, seriesInfo => seriesInfo.remove());
      }
      if (this.serviceChart.drilldownLevels) {
        this.serviceChart.drilldownLevels = [];
      }
      this.serviceChart.update({
        series: serviceTypeChartData.series,
        drilldown: {
          ...this.serviceTypeChartConfig.drilldown,
          series: serviceTypeChartData.drilldownData
        }
      }, true, true);
      this.serviceChart.redraw();
    }
  }

  private buildChartData(costDetails: any, chartTitle: string): any {
    const tagTypeTotalCostMap = {};

    _.each(costDetails, instanceTypeDetail => {
      const key = instanceTypeDetail.tagValueName ? instanceTypeDetail.tagName + "_" + instanceTypeDetail.tagValueName : instanceTypeDetail.tagName || instanceTypeDetail.type;
      if (tagTypeTotalCostMap[key]) {
        tagTypeTotalCostMap[key] += parseFloat(instanceTypeDetail.amount);
      } else {
        tagTypeTotalCostMap[key] = parseFloat(instanceTypeDetail.amount);
      }
    });

    _.each(tagTypeTotalCostMap, (amount, key) => {
      tagTypeTotalCostMap[key] = Number(amount.toFixed(2));
    });

    const series = [{
      id: uuid(),
      name: chartTitle,
      colorByPoint: true,
      data: []
    }]

    _.each(tagTypeTotalCostMap, (amount, tagTypeKey) => {
      let tagParts = tagTypeKey.indexOf('_') != -1 ? tagTypeKey.split("_") : null;
      let tagValueName = tagParts ? tagParts[1] : tagTypeKey;
      let tagName = "";
      let tagValue = "";
      if (tagParts) {
        tagName = tagParts[0];
        tagValue = tagParts[1];
      }
      const seriesData = {
        tagName,
        tagValue,
        name: tagValueName,
        id: uuid(),
        y: amount,
        drilldown: uuid(),
        sectionType: this.isUserCost ? Constance.AWS_USER_CRED_NAME : Constance.TAGGED_COST_NAME
      }
      series[0].data.push(seriesData);
    });
    return {
      series,
      drilldownData: []
    }
  }

  private buildSelectedTagsChartData(costDetails: any, chartTitle: string): any {
    const tagTypeWithTagNameTotalCostMap = {};

    _.each(costDetails, instanceTypeDetail => {
      const key = instanceTypeDetail.tagValueName ? instanceTypeDetail.tagName + "_" + instanceTypeDetail.tagValueName : instanceTypeDetail.tagName;
      if (tagTypeWithTagNameTotalCostMap[key]) {
        tagTypeWithTagNameTotalCostMap[key] += parseFloat(instanceTypeDetail.amount);
      } else {
        tagTypeWithTagNameTotalCostMap[key] = parseFloat(instanceTypeDetail.amount);
      }
    });
    const series = [{
      id: uuid(),
      name: chartTitle,
      colorByPoint: true,
      data: []
    }]

    _.each(tagTypeWithTagNameTotalCostMap, (amount, instanceKey) => {
      tagTypeWithTagNameTotalCostMap[instanceKey] = Number(amount.toFixed(2));
    });

    _.each(tagTypeWithTagNameTotalCostMap, (amount, tagTypeKey) => {
      let tagParts = tagTypeKey.split("_");
      let tagName = tagParts[0];
      let tagValueName = tagParts[1];
      const seriesData = {
        tagName,
        name: tagValueName,
        y: amount,
        drilldown: uuid()
      }
      series[0].data.push({ ...seriesData, y: Number(seriesData.y.toFixed(2)) });
    });
    return {
      series,
      drilldownData: []
    }
  }

  private format(costTypeResourcesMapList: any[]): void {
    _.each(costTypeResourcesMapList, (costInfo) => {
      // roundOf
      costInfo.roundedAmount = costInfo.amount.toFixed(2);
      // format date
      costInfo.startDate = moment(costInfo.startDate).format(config.FULL_DATE_FORMAT);
    });
  }

  public updateChartsWidth(): void {
    if (this.serviceChartParent) {
      const serviceChartWidth = this.serviceChartParent.nativeElement.clientWidth -
        (parseInt(window.getComputedStyle(this.serviceChartParent.nativeElement).paddingLeft.replace("px", ""))
          + parseInt(window.getComputedStyle(this.serviceChartParent.nativeElement).paddingRight.replace("px", "")));
      if (this.serviceChart) {
        this.serviceChart.update({ chart: { width: serviceChartWidth } });
      }
    }
  }
}
