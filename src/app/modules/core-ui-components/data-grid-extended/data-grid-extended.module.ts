import { NgModule } from "@angular/core";
import { DataGridExtendedComponent } from "./views/data-grid-extended.component";
import { CommonModule } from "@angular/common";
import { NgxSmartModalModule } from "ngx-smart-modal";
import { NgxUiLoaderModule } from "ngx-ui-loader";
import { ngxUiLoaderConfig } from "src/app/utils/config";
import { ModulesFactoryRegister } from "src/app/modules-factory/modules-factory.module";
import { DataGridExtendedRefreshViewModel } from "./view-models/data-grid-extended-refresh.viewmodel";
import { AgGridModule } from "ag-grid-angular";
import { FormsModule } from "@angular/forms";
@NgModule({
  declarations: [DataGridExtendedComponent],
  imports: [
    CommonModule, NgxSmartModalModule,
    NgxSmartModalModule.forRoot(),
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    ModulesFactoryRegister.FOR_CHILD(DataGridExtendedComponent),
    AgGridModule.withComponents([]),
    FormsModule
  ],
  exports: [DataGridExtendedComponent],
  providers: [DataGridExtendedRefreshViewModel]
})
export class DataGridExtendedModule { }
