import { Subject } from "rxjs";
import { UserEvent } from "../../helpers/user.event";
import { GridConfig, DataTypeCollection } from "../../../../utils/config";
import { rowClickEnabledDataTablesIds } from "../../../../utils/config";
import { GridOptions } from "ag-grid-community";
import _ from "lodash";
// import { AuthService } from "src/app/utils/auth.service";

/**
 * A Generic grid to consume through out the application.
 */
export class DataGridExtendedViewModel {
    pristineHeader: any;
    public constructor() {
        this.events = new Subject<UserEvent>();
        this.rowClickEnabledDataTablesIds = rowClickEnabledDataTablesIds;
    }
    rowClickEnabledDataTablesIds: any;
    modalTitle: string;
    ngxSmartModalService;
    historyAccess: any;
    isEditIconClaimList: any;
    isDeleteIconClaimList: any;
    isPreviewTargetDefinitionIconClaimList: any;
    // isDownloadTargetDefinitionIconClaimList: any;
    public title: string;
    public events: Subject<UserEvent>;
    public data: [string, string][];
    public dataTable: any;
    public claims: any;
    public dataTableConfiguration: any;
    public clickableColumnIndices = [];
    public dataTableId = "dataTable";
    public selectedRow = "";
    public isRemoveEnabled: boolean;
    canAccessEditDelete: false;
    canAccessPreviewTargetDefinition: false;
    canAccessDownloadTargetDefinition: false;
    public specialEditDeleteAccess;
    public gridApi: any;
    public gridColumnApi: any;
    public rowData = [];
    columnDefs = [];
    searchText = "";
    paginationPageSize = 10;
    public dataTypeValueList: any[];
    public defaultSortModel: any[];
    public iconDetails: string = "iconDetails";
    public dataActionType: string ="data-action-type";
    // tslint:disable-next-line:no-angle-bracket-type-assertion
    public gridOptions = <GridOptions>{
        columnDefs: [],
        headerHeight: 35,
        rowHeight: 35,
        enableColResize: true,
        enableGroupEdit: false,
        pagination: true,
        accentedSort: true,
        suppressDragLeaveHidesColumns: true,
        paginationPageSize: this.paginationPageSize,
        autoGroupColumnDef: false,
        sideBar: false,
        defaultColDef: {
            flex: 1,
            filter: true,
            editable: true
        },
        getRowNodeId(data) {
            return data.id;
        },
        rowSelection: "single",
    };

    public deselectAll() {
        this.gridApi.deselectAll();
    }

    /**
     * set rows to the datatable
     * @param data data for the datatable in array
     * @param claims should the columns include remove action
     * TODO need to remove isClaims optional parameter
     */
    public setRowsData(data) {
        if (data) {
            this.rowData = data;
            if (this.gridApi) {
                this.gridApi.sizeColumnsToFit();
                this.gridApi.setColumnDefs(this.columnDefs);
            }
            this.events.next({ eventData: null, eventType: "DataChanged" });
        } else {
            return "Please provide the data to assign";
        }
    }

    public onRowClicked(event) {
        if (event.event.target !== undefined) {
            const actionType = event.event.target.getAttribute("data-action-type");
            const eventData = { rowDetails: event.data, columnId: event.colDef.field };
            switch (actionType) {
                case "acceptedValueIconClicked":
                    this.events.next({ eventData, eventType: GridConfig.acceptedValueIconClicked });
                    break;
                case "columnClicked":
                    this.events.next({ eventData, eventType: GridConfig.columnClicked });
                    break;
                case "checkBoxClicked":
                    this.events.next({ eventData, eventType: GridConfig.checkBoxClicked });
                    break;
                case "splitClicked":
                    this.onSplitMergeClick(event);
                    break;
                case "mergeClicked":
                    this.onSplitMergeClick(event);
                    break;
                case "mappingClicked":
                    this.events.next({ eventData, eventType: GridConfig.mappingClicked });
                    break;
                case "editClicked":
                    this.events.next({ eventData, eventType: GridConfig.editClicked });
                    break;
                case "deleteClicked":
                    this.events.next({ eventData, eventType: GridConfig.deleteClicked });
                    break;
                case "downloadCleanFileClicked":
                    this.events.next({ eventData, eventType: GridConfig.downloadCleanFileClicked });
                    break;
                case "approveRejectClicked":
                    this.events.next({ eventData, eventType: GridConfig.approveRejectClicked });
                    break;
                case "roleCheckBoxClicked":
                    this.events.next({ eventData: eventData, eventType: GridConfig.roleCheckBoxClicked });
                    break;
                case "copyClicked":
                    this.events.next({ eventData, eventType: GridConfig.copyClicked });
                case "cancelRequestClicked":
                    this.events.next({ eventData, eventType: GridConfig.cancelRequestClicked });
                    break;
                case "requestMoreInfoClicked":
                    this.events.next({ eventData, eventType: GridConfig.requestMoreInfoClicked });
                    break;
                case "viewCommentsClicked":
                    this.events.next({ eventData, eventType: GridConfig.viewCommentsClicked });
                    break;
                default:
                    break;
            }
        }
    }

    public onSplitMergeClick(event: any): void {
        const colId = event.column.getId();
        if (colId === this.iconDetails) {
            const actionType = event.event.target.getAttribute(this.dataActionType);
            const eventData = { rowDetails: event.data, columnId: event.colDef.field };
            this.events.next({ eventData, eventType: actionType });
        }
    }

    public onCellValueChanged(params: any): void {
        const colId = params.column.getId();
        const eventData = { rowDetails: params.data, columnId: params.colDef.field };
        switch (colId) {
            case "refColumnDataTypeId":
                const dataType = params.data.refColumnDataTypeId;
                if (dataType.toUpperCase() !== DataTypeCollection.Varchar && dataType.toUpperCase() !== DataTypeCollection.NVarchar && dataType.toUpperCase() !== DataTypeCollection.Char && dataType.toUpperCase() !== DataTypeCollection.Int) {
                    params.node.setDataValue('maxLength', null);
                }
                if (dataType.toUpperCase() !== DataTypeCollection.Date) {
                    params.node.setDataValue('format', null);
                }
                break;
            case "targetColumnId":
                this.events.next({ eventData, eventType: GridConfig.dropDownChanged });
                break;
            default:
                break
        }
    }

    public onRowSelected(event: any): void {
        if (event.type) {
            const eventData = { rowDetails: event.data, rowIndex: event.rowIndex };
            this.events.next({ eventData, eventType: event.type });
        }
    }

    public gridInit(): void {
        this.events.next({ eventData: null, eventType: GridConfig.gridInit });
    }

    public includes(container: any, value: any): boolean {
        let returnValue = false;
        const result = container.indexOf(value);
        if (result >= 0) {
            returnValue = true;
        }
        return returnValue;
    }

    public hasEdit(id: string):any {
        return this.isEditIconClaimList ? this.isEditIconClaimList.get(id) : true;
    }

    /// Ag Grid Implementation
    public onGridReady(event: any) {
        this.gridApi = event.api;
        this.gridColumnApi = event.columnApi;
        this.gridApi.setColumnDefs(this.columnDefs);
        this.gridApi.closeToolPanel();
        if (window.innerWidth > 768) {
            event.api.sizeColumnsToFit();
        }
        if (this.defaultSortModel) {
            event.api.setSortModel(this.defaultSortModel);
        }
        this.gridApi.setDomLayout("autoHeight");
    }
    // public moveColumnToSpecificIndex() {
    //     this.gridColumnApi.moveColumn('columnName', 0);
    // }

    public onPageSizeChanged() {
        this.gridApi.paginationSetPageSize(Number(this.paginationPageSize));
    }

    public onSearch() {
        this.gridApi.setQuickFilter(this.searchText);
        if (this.gridApi.getDisplayedRowCount() === 0) {
            this.gridApi.showNoRowsOverlay();
        } else {
            this.gridApi.hideOverlay();
        }
    }

    /**
     * generating array of values clicked on table
     * for that using headerProperties to get the array of values
     * @param clickedRowData as particular clicked row data
     * @return array of value form clicked row data
     */
    private generateClickEventData(clickedRowData) {
        const headerProperties = _.map(this.pristineHeader, "field");
        const rowData = [];

        rowData.push(clickedRowData.id);
        headerProperties.forEach(header => {
            rowData.push(header, clickedRowData[header]);
        });

        return rowData;
    }

    public clearSearch() {
        this.searchText = "";
        this.onSearch();

    }

    public customSort(param1: any, param2: any): number {
        if (typeof param1 === "number") {
            return param1 - param2;
        } else {
            const paramA = param1 ? param1.toLowerCase() : param1;
            const paramB = param2 ? param2.toLowerCase() : param2;

            if (paramA > paramB) {
                return 1;
            } else if (paramA < paramB) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    public customDateSort(param1, param2) {
        return new Date(param1).getTime() - new Date(param2).getTime();
    }

    public setColumnDefinition(columnDef) {
        this.columnDefs = columnDef;
    }

    /**
     * Sets header information
     * @param header - Header array
     * @param claims - should the header include actions
     */
    public setHeader1(header, isRemoveEnabled, claims?) {
        if (header) {
            this.pristineHeader = header.slice();
            this.isRemoveEnabled = isRemoveEnabled;
            this.dataTable = { headerRow: header, dataRows: undefined };
            const columnDefs = [];
            this.dataTable.headerRow.forEach((element, index) => {
                let isSorting = true;
                if (element.sortable === false) {
                    isSorting = false;
                }
                const column: any = {
                    headerName: element.headerName,
                    field: element.field,
                    colId: element.field,
                    sortable: isSorting,
                    suppressMenu: true,
                    comparator: element.isDate ? this.customDateSort : this.customSort,
                    minWidth: element.minWidth,
                    width: element.width,
                    headerCheckboxSelection: element.headerCheckboxSelection,
                    headerCheckboxSelectionFilteredOnly: element.headerCheckboxSelectionFilteredOnly,
                    checkboxSelection: element.checkboxSelection,
                    icons: element.icons
                };
                if (element.sortingOrder) {
                    column.sortingOrder = element.sortingOrder;
                }
                if (index === 0) {
                    column.sort = element.sort ? element.sort : "asc";
                }
                if (element.noFilter) {
                    column.filter = false;
                }
                if (element.isHide) {
                    column.hide = true;
                }
                if (element.isNumber) {
                    delete element.comparator;
                }
                if (element.isClickable) {
                    column.cellRenderer = (params: any) => {
                        if (params.value) {
                            return `<u class="hyperlink" data-action-type="columnClicked">` + params.value + `</u>`;
                        } else {
                            return `<span  data-action-type="nullValueColumnClicked">` + params.value + `</span>`;
                        }
                    };
                }
                if (element.isCustomIconToShow) {
                    column.cellRenderer = (params: any) => {
                        if (params.value) {
                            return params.value;
                        };
                    }
                }
                columnDefs.push(column);
            });
            this.columnDefs = columnDefs;
            if (this.gridApi) {
                this.gridApi.setColumnDefs(this.columnDefs);
            }
        } else {
            return "Please provide the headers to assign";
        }
    }

    public gridOptionRowSelectionToMultiple() {
        this.gridOptions.rowSelection = "multiple";
    }
}
