import { Subject } from "rxjs";
import { DataTable } from "./datatable.viewmodel";
import { UserEvent } from "../../helpers/user.event";
import { GridConfig } from "../../../../utils/config";
import { Injectable } from "@angular/core";


/**
 * A Generic grid to consume through out the application.
 */
@Injectable()
export class DataGridExtendedRefreshViewModel {
    events = new Subject<UserEvent>();
    public constructor() {}

}
