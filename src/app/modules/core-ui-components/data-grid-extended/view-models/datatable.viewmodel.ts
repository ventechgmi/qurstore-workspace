
export interface DataTable {
    headerRow: string[];
    dataRows: string[][];
}
