import { Directive, ElementRef, Output, EventEmitter, HostListener, Input, OnInit, OnChanges } from "@angular/core";

@Directive({
    selector: "[libClickOutside]"
})
export class ClickOutsideDirective {
    constructor(private _elementRef: ElementRef) {
    }

    @Output()
    public libClickOutside = new EventEmitter<MouseEvent>();

    @HostListener("document:click", ["$event", "$event.target"])
    @HostListener("document:touchstart", ["$event", "$event.target"])
    public onClick(event: MouseEvent, targetElement: HTMLElement): void {
        if (!targetElement) {
            return;
        }

        const clickedInside = this._elementRef.nativeElement.contains(targetElement);
        if (!clickedInside) {
            this.libClickOutside.emit(event);
        }
    }
}

@Directive({
    selector: "[libScroll]"
})
export class ScrollDirective {
    constructor(private _elementRef: ElementRef) {
    }

    @Output()
    public libScroll = new EventEmitter<MouseEvent>();

    @HostListener("libScroll", ["$event"])
    public onClick(event: MouseEvent, targetElement: HTMLElement): void {
        this.libScroll.emit(event);
    }
}