import { Component, OnInit, OnDestroy, NgModule, TemplateRef, AfterContentInit, ContentChild, EmbeddedViewRef, OnChanges, ViewContainerRef, ViewEncapsulation, Input, Output, EventEmitter, ElementRef, AfterViewInit, Pipe, PipeTransform, Directive } from "@angular/core";
import { SafeResourceUrl, DomSanitizer } from "@angular/platform-browser";
import { CommonModule }       from "@angular/common";

@Component({
  selector: "lib-item",
  template: ``
})

export class ItemComponent { 

    @ContentChild(TemplateRef, {static: true}) template: TemplateRef<any>;
    constructor() {   
    }

}

@Component({
  selector: "lib-badge",
  template: ``
})

export class BadgeComponent { 

    @ContentChild(TemplateRef, {static: true}) template: TemplateRef<any>;
    constructor() {   
    }

}