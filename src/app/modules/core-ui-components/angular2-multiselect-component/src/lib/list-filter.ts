import { Pipe, PipeTransform } from "@angular/core";
import { DataService } from "./multiselect.service";


@Pipe({
    name: "listFilter",
    pure: true
})
export class ListFilterPipe implements PipeTransform {
    public found:boolean;
    public filteredList: any = [];
    constructor(private ds: DataService) {

    }

    transform(items: any[], filter: any, searchBy: any): any[] {
        if (!items || !filter) {
            this.ds.setData(items);
            return items;
        }
        this.filteredList = items.filter((item: any) => this.applyFilter(item, filter, searchBy));
        this.ds.setData(this.filteredList);
        return this.filteredList;
    }

    applyFilter(item: any, filter: any, searchBy: any): boolean {
        this.found = false;
        if (searchBy.length > 0) {
            if (item.grpTitle) {
                this.found = true;
            } else {
                this.searchByTitle(item, filter, searchBy);
                // console.log("Expected True "+ this.found);
            }
        } else {
            if (item.grpTitle) {
                this.found = true;
            } else {
                for (const prop in item) {
                    if (filter && item[prop]) {
                        if (item[prop].toString().toLowerCase().indexOf(filter.toLowerCase()) >= 0) {
                            this.found = true;
                        }
                    }
                }
            }
        }
        // console.log("Found "+ this.found);
        return this.found;
    }

    searchByTitle(item: any, filter: any, searchBy: any) {
        // console.log("Expected False "+ this.found);
        let t: number;
        for (t = 0; t < searchBy.length; t++) {
            if (filter && item[searchBy[t]] && item[searchBy[t]] !== "") {
                if (item[searchBy[t]].toString().toLowerCase().indexOf(filter.toLowerCase()) >= 0) {
                    this.found = true;
                }
            }
        }
    }
}