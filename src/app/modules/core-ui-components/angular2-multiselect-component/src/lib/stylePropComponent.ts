import { Directive, ElementRef, Input, OnInit, OnChanges } from "@angular/core";


@Directive({
    selector: "[libStyleProp]"
})
export class StyleDirective implements OnInit, OnChanges {

    constructor(private el: ElementRef) {

    }

    @Input("libStyleProp") styleVal: number;

    ngOnInit() {
        this.el.nativeElement.style.top = this.styleVal;
    }
    ngOnChanges(): void {
        this.el.nativeElement.style.top = this.styleVal;
    }
}


@Directive({
    selector: "[libSetPosition]"
})
export class SetPositionDirective implements OnInit, OnChanges {

    @Input("libSetPosition") height: number;

    constructor(public el: ElementRef) {

    }
    ngOnInit() {
        if (this.height) {
            this.el.nativeElement.style.bottom = parseInt(this.height + 15 + "", 10) + "px";
        }
    }
    ngOnChanges(): void {
        if (this.height) {
            this.el.nativeElement.style.bottom = parseInt(this.height + 15 + "", 10) + "px";
        }
    }
}