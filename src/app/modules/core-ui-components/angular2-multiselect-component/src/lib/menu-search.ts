import { Component, OnInit, OnDestroy, NgModule, TemplateRef, AfterContentInit, ContentChild, EmbeddedViewRef, OnChanges, ViewContainerRef, ViewEncapsulation, Input, Output, EventEmitter, ElementRef, AfterViewInit, Pipe, PipeTransform, Directive } from "@angular/core";
import { SafeResourceUrl, DomSanitizer } from "@angular/platform-browser";
import { CommonModule }       from "@angular/common";


@Component({
selector: "lib-search",
template: ``
})

export class SearchComponent { 

    @ContentChild(TemplateRef, {static: true}) template: TemplateRef<any>;
    constructor() {   
    }

}
@Component({
selector: "lib-template-renderer",
template: ``
})

export class TemplateRendererComponent implements OnInit, OnDestroy { 

    @Input() data: any;
    @Input() item: any;
    view: EmbeddedViewRef<any>;

    constructor(public viewContainer: ViewContainerRef) {   
    }
    ngOnInit() {
        this.view = this.viewContainer.createEmbeddedView(this.data.template, {
            "\$implicit": this.data,
            "item":this.item
        });
    }
    
    ngOnDestroy() {
        this.view.destroy();
    }

}