import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { HighchartsChartModule } from "highcharts-angular";
import { SharedModules } from "src/app/utils/shared-module/shared.modules";
import { AngularMultiSelectModule } from "./angular2-multiselect-component/src/lib";
import { CostWidgetViewComponent } from "./cost-widget-component";
@NgModule(
    {
        declarations: [
            CostWidgetViewComponent
        ],
        imports: [
            AngularMultiSelectModule,
            SharedModules,
            HighchartsChartModule
        ],
        exports: [
            CostWidgetViewComponent
        ],
        providers: []
    }
)
export class CoreUiModule {

}
