export class WorkspaceRequestStatus {
    public static readonly APPROVED_STATUS = "Approved";
    public static readonly PENDING_FOR_APPROVAL_STATUS = "Pending for approval";
    public static readonly TERMINATED_STATUS = "Terminated";
    public static readonly REJECTED_STATUS = "Rejected";
}

export class MappingStatus {
  public static readonly MAPPED = "Mapped";
  public static readonly NOT_MAPPED = "Not Mapped";
}

export class ICON_HTML {
    public static readonly MAPPING_ICON = `<a class="data-grid-padding-right" routerLink=""><img src="../../../assets/images/data.png" class="data-grid-image" alt="Create new Mapping Definition" data-action-type="mappingClicked" title="Create new Mapping Definition"></a>`;
    public static readonly ACTIVE_TOGGLE_BUTTON = `<a class="data-grid-padding-right" routerLink=""><img src="../../../assets/images/green-active-slider.png" class="data-grid-image" alt="active" data-action-type="toggleClicked" title="Deactivate" style="width:20%"></a>`;
    public static readonly IN_ACTIVE_TOGGLE_BUTTON = `<a class="data-grid-padding-right" routerLink=""><img src="../../../assets/images/green-inactive-slider.png" class="data-grid-image" alt="active" data-action-type="toggleClicked" title="Deactivate" style="width:20%"></a>`
    public static readonly REFRESH_ICON = `<a routerLink="" class="data-grid-padding-right"><img alt="Refresh" title="Refresh" src="../../../assets/images/refresh.png"  class="data-grid-image refresh-icon"  data-action-type="refreshClicked"/></a>`;
    public static readonly EDIT_ICON = `<a routerLink="" class="data-grid-padding-right"><img alt="Edit" title="Edit" src="../../../assets/images/edit.png"  class="data-grid-image"  data-action-type="editClicked"/></a>`;
    public static readonly DELETE_ICON = `<a routerLink="" class="data-grid-padding-right"><img alt="Delete"  title="Delete"  src="../../../assets/images/trash.png" class="data-grid-image-small" data-action-type="deleteClicked"/></a>`;
    public static readonly PREVIEW_TARGET_DEFINITION = `<a routerLink="" class="data-grid-padding-right"><img alt="Preview"  title="Preview"  src="../../../assets/images/eye-active.png" class="data-grid-image-small" data-action-type="previewTargetDefinitionClicked"/></a>`;
    public static readonly DOWNLOAD_TARGET_DEFINITION = `<a routerLink="" class="data-grid-padding-right"><img alt="Download Original Version"  title="Download Original Version"  src="../../../assets/images/download.png" class="data-grid-image-small" data-action-type="downloadFileClicked"/></a>`;
    public static readonly DOWNLOAD_CLEAN_FILE_ICON = `<a routerLink="" class="data-grid-padding-right"><img alt="Download Clean Version"  title="Download Clean Version"  src="../../../assets/images/download6.png" class="data-grid-image-small" data-action-type="downloadCleanFileClicked"/></a>`;
    public static readonly VIEW_DATA_USER_AGREEMENT = `<a routerLink="" class="data-grid-padding-right"><img alt="Download Data Usage Agreement"  title="Download Data Usage Agreement"  src="../../../assets/images/pdf-download.png" class="data-grid-image-small" data-action-type="viewDataUsageAgreement"/></a>`;
    public static readonly TERMINATE_REQUEST_ICON = `<a routerLink="" class="data-grid-padding-right"><img alt="Terminate Request"  title="Terminate Request"  src="../../../assets/images/terminate.jpg" class="data-grid-image-small" data-action-type="terminateRequestClicked"/></a>`;
    public static readonly CANCEL_REQUEST_ICON = `<a routerLink="" class="data-grid-padding-right"><img alt="Cancel Request"  title="Cancel Request"  src="../../../assets/images/cancel.png" class="data-grid-image-small" data-action-type="cancelRequestClicked"/></a>`;
    public static readonly APPROVE_REJECT_RECORD = `<a routerLink="" class="data-grid-padding-right"><img alt="Approve/Reject"  title="Approve/Reject"  src="../../../assets/images/icons8-clipboard-approve-32.png"   class="data-grid-image-small" data-action-type="approveRejectClicked"/></a>`;
    public static readonly VIEW_ERROR_LINK = `<a routerLink="" class="data-grid-padding-right"><img alt="View Errors"  title="View Errors"  src="../../../assets/images/icons8-error-64.png" height="29" width="29" data-action-type="viewErrorsClicked"/></a>`;
    public static readonly AUDIT_ICON = ` <i title="Audit Log History" class="icon material-icons cursor " data-action-type="auditLogClicked">history</i>`;
    public static readonly ENABLED_DELETE_ICON = `<i title="Delete" class="icon material-icons cursor text-info" data-action-type="deleteClicked">delete</i>`;
}

export enum PageModeType {
    Approve = "Approve",
    RequestMoreUtility = "RequestMoreUtility",
    Resubmit = "Resubmit",
    Detail = "Detail",
    Create = "Create"
}

export enum AWSKeyValues {
    AdditionalPriceKey = "Bundle Storage License User Volume Usage Custom",
}

export enum PricingModel {
    Monthly = "Monthly",
    Hourly = "Hourly"
}

export enum BundleOption {
    Windows = "Windows Bundle Options",
    Linux = "Linux Bundle Options"
}

export let TabIndexKeys = {};
TabIndexKeys[0] = 'Environment';
TabIndexKeys[1] = 'Hardware'
TabIndexKeys[2] = 'Software';
TabIndexKeys[3] = 'Utilities';
TabIndexKeys[4] = 'Datasets';
TabIndexKeys[5] = 'History';
