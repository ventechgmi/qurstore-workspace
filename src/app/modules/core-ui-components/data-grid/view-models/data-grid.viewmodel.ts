import { Subject } from "rxjs";
import { UserEvent } from "../../helpers/user.event";
import { GridConfig } from "../../../../utils/config";
import { rowClickEnabledDataTablesIds } from "../../../../utils/config";
import { GridOptions } from "ag-grid-community";
import 'ag-grid-enterprise';
// ag-grid
import _ from "lodash";
import { ICON_HTML, MappingStatus } from '../helper/constants';
// import { AuthService } from "src/app/utils/auth.service";

/**
 * A Generic grid to consume through out the application.
 */
export class DataGridViewModel {
    pristineHeader: any;
    public constructor() {
        this.events = new Subject<UserEvent>();
        this.rowClickEnabledDataTablesIds = rowClickEnabledDataTablesIds;
    }
    public customPagination: boolean;
    public rowClickEnabledDataTablesIds: any;
    public modalTitle: string;
    public ngxSmartModalService;
    public historyAccess: any;
    public isEditIconClaimList: any;
    public isDeleteIconClaimList: any;
    public isTerminatedIconClaimList: any;
    public isCancelIconClaimList: any;
    public isPreviewTargetDefinitionIconClaimList: any;
    public isViewDataUsageAgreementIconClaimList: any;
    public actionColumnSize = 150;
    public isDownloadTargetDefinitionIconClaimList: any;
    public title: string;
    public events: Subject<UserEvent>;
    public data: [string, string][];
    public dataTable: any;
    public claims: any;
    public dataTableConfiguration: any;
    public clickableColumnIndices = [];
    public dataTableId = "dataTable";
    public selectedRow = "";
    public actionColumnName = "Action";
    public isRemoveEnabled: boolean;
    public canAccessEdit: false;
    public canMappingDefinition: false;
    public canToggle: false;
    public canAccessDelete = false;
    public canTerminate = false;
    public canCancelRequest = false;
    public canAccessPreviewTargetDefinition = false;
    public canViewDataUsageAgreement = false;
    public canAccessDownloadTargetDefinition = false;
    public canAccessDownloadCleanFile = false;
    public canAccessRefresh = false;
    public canApproveReject = false;
    public deleteTerminateTooltip: string;
    public specialEditDeleteAccess;
    public gridApi: any;
    public gridColumnApi: any;
    public rowData = [];
    public sideBar;
    public columnDefs = [];
    public searchText = "";
    public paginationPageSize = 10;
    public defaultSortModel: any[];
    // tslint:disable-next-line:no-angle-bracket-type-assertion
    public gridOptions = <GridOptions>{
        columnDefs: [],
        headerHeight: 35,
        rowHeight: 35,
        enableColResize: true,
        enableGroupEdit: false,
        pagination: true,
        suppressDragLeaveHidesColumns: true,
        paginationPageSize: this.paginationPageSize,
        autoGroupColumnDef: false,
        defaultColDef: {
            enableRowGroup: false,
            enableValue: false,
            enablePivot: false,
            enableCellChangeFlash: false
        },
        getRowNodeId(data) {
            return data.id;
        },
        sideBar: false,
    };

    public setPagination(isPagination: boolean): void {
        this.gridOptions.pagination = isPagination;
        this.customPagination = isPagination;
    }

    public gridSideBarOption(): void {
        this.sideBar = {
            toolPanels: [
                {
                    id: 'columns',
                    labelDefault: 'Columns',
                    labelKey: 'columns',
                    iconKey: 'columns',
                    toolPanel: 'agColumnsToolPanel',
                    toolPanelParams: {
                        suppressRowGroups: true,
                        suppressValues: true,
                        suppressPivots: true,
                        suppressPivotMode: true,
                        suppressSideButtons: true,
                        suppressColumnFilter: true,
                        suppressColumnSelectAll: true,
                        suppressColumnExpandAll: true,
                    },
                },
            ],
            defaultToolPanel: 'columns',
        };
    }
    public gridOptionRowSelectionToMultiple(): void {
        this.gridOptions.rowSelection = "multiple";
    }
    /**
     * Sets header information
     * @param header - Header array
     * @param claims - should the header include actions
     */
    // tslint:disable-next-line:cyclomatic-complexity
    // tslint:disable-next-line: max-func-body-length
    public setHeader(header: any, isRemoveEnabled: any, claims?: any): string {
        if (header) {
            this.pristineHeader = header.slice();
            if (claims) {
                this.canAccessEdit = claims.canUpdate;
                this.canAccessDelete = claims.canDelete;
                this.canTerminate = claims.canTerminate;
                this.canCancelRequest = claims.canCancelRequest;
                this.canAccessPreviewTargetDefinition = claims.canPreviewTargetDefinition;
                this.canViewDataUsageAgreement = claims.canViewDataUsageAgreement;
                this.canAccessDownloadTargetDefinition = claims.canDownloadTargetDefinition;
                this.canAccessDownloadCleanFile = claims.canDownloadCleanFile;
                this.canApproveReject = claims.canApproveReject;
                this.canMappingDefinition = claims.canMappingDefinition;
                this.canToggle = claims.canToggle;


                this.canApproveReject = claims.canApproveReject;
                this.canAccessRefresh = claims.canAccessRefresh;
                if (claims.historyAccess) {
                    this.historyAccess = claims.historyAccess;
                }
            }
            this.isRemoveEnabled = isRemoveEnabled;
            this.dataTable = { headerRow: header, dataRows: undefined };
            const columnDefs = [];
            this.dataTable.headerRow.forEach((element, index) => {
                let isSorting = true;
                if (element.sortable === false) {
                    isSorting = false;
                }
                const column: any = {
                    headerName: element.headerName,
                    field: element.field,
                    colId: element.field,
                    sortable: isSorting,
                    suppressMenu: true,
                    comparator: element.isDate ? this.customDateSort : this.customSort,
                    minWidth: element.minWidth,
                    width: element.width,
                    headerCheckboxSelection: element.headerCheckboxSelection,
                    headerCheckboxSelectionFilteredOnly: element.headerCheckboxSelectionFilteredOnly,
                    checkboxSelection: element.checkboxSelection,
                    icons: element.icons,
                    valueFormatter: element.valueFormatter
                };
                if (element.sortingOrder) {
                    column.sortingOrder = element.sortingOrder;
                }
                if (index === 0) {
                    column.sort = element.sort ? element.sort : "asc";
                }
                if (element.noFilter) {
                    column.filter = false;
                }
                if (element.isHide) {
                    column.hide = true;
                }
                if (element.isNumber) {
                    delete element.comparator;
                }
                if (element.isClickable) {
                    column.cellRenderer = (params: any) => {
                        if (params.value) {
                            return `<u class="hyperlink" data-action-type="columnClicked">` + params.value + `</u>`;
                        }
                    };
                }
                if (element.onMouseHover) {
                    column.cellRenderer = (params: any) => {
                        if (params.value) {
                            return `<label class="label-control" data-action-type="onMouseHover">` + params.value + `</label>`;
                        }
                    };
                }
                if (element.isCustomIconToShow) {
                    column.cellRenderer = (params: any) => {
                        if (params.value) {
                            return params.value;
                        };
                    }
                }
                if (element.isDropDown) {
                    column.cellRenderer = (params: any) => {
                        if (params.value) {
                            return params.value;
                        };
                    }
                }
                if (element.isCheckBox) {
                    column.width = 50;
                    column.cellRenderer = (params: any) => {
                        if (element.isParentCheckBox) {
                            if (params.value) {
                                return `<input type="checkbox" checked data-action-type="checkBoxClicked" value="parent">`;
                            } else {
                                return `<input type="checkbox" data-action-type="checkBoxClicked" value="parent">`;
                            }
                        } else {
                            if (params.value) {
                                return `<input type="checkbox" checked data-action-type="checkBoxClicked" value="child">`;
                            } else {
                                return `<input type="checkbox" data-action-type="checkBoxClicked" value="child">`;
                            }
                        }

                    };
                }
                if (element.isRadioButton) {
                    column.cellRenderer = (params: any) => {
                        if (params.value) {
                            return `<div class="ngSelectionCell"><input name="radioAV" checked data-action-type="columnClicked"   type="radio"></div>`;
                        } else {
                            return `<div class="ngSelectionCell"><input name="radioAV" data-action-type="columnClicked" type="radio"></div>`;
                        }
                    };
                }
                columnDefs.push(column);
            });

            if (this.canAccessEdit ||
                this.canAccessDelete ||
                this.canTerminate ||
                this.canCancelRequest ||
                this.historyAccess ||
                this.isRemoveEnabled ||
                this.canAccessPreviewTargetDefinition ||
                this.canViewDataUsageAgreement ||
                this.canAccessDownloadTargetDefinition ||
                this.canAccessDownloadCleanFile ||
                this.canApproveReject ||
                this.canMappingDefinition ||
                this.canToggle) {

                columnDefs.push({
                    headerName: this.actionColumnName,
                    field: "",
                    width: this.actionColumnSize,
                    suppressMenu: true,
                    filter: false,
                    sortable: false,

                    // tslint:disable-next-line: cyclomatic-complexity
                    cellRenderer: (params: any) => {
                        let finalIcon = "";
                        const mappingIcon = ICON_HTML.MAPPING_ICON;
                        const activeToggleButton = ICON_HTML.ACTIVE_TOGGLE_BUTTON;
                        const inActiveToggleButton = ICON_HTML.IN_ACTIVE_TOGGLE_BUTTON;
                        const refreshIcon = ICON_HTML.REFRESH_ICON;
                        const editIcon = ICON_HTML.EDIT_ICON;
                        const deleteIcon = ICON_HTML.DELETE_ICON;
                        const previewTargetDefinitionIcon = ICON_HTML.PREVIEW_TARGET_DEFINITION;
                        // tslint:disable-next-line:max-line-length
                        const downloadTargetDefinitionIcon = ICON_HTML.DOWNLOAD_TARGET_DEFINITION;
                        let downloadCleanFileIcon = "";
                        if (params.data.processedFilePath) {
                            downloadCleanFileIcon = ICON_HTML.DOWNLOAD_CLEAN_FILE_ICON;
                        }
                        let viewDataUsageAgreement = "";
                        if (params.data.dataUsageAgreementId) {
                            viewDataUsageAgreement = ICON_HTML.VIEW_DATA_USER_AGREEMENT;
                        }
                        let terminateRequestIcon = "";
                        if (params.data.isCompleted
                        ) {
                            terminateRequestIcon = ICON_HTML.TERMINATE_REQUEST_ICON;
                        }

                        let cancelRequestIcon = "";
                        if (params.data.isOpen
                        ) {
                            cancelRequestIcon = ICON_HTML.CANCEL_REQUEST_ICON;
                        }

                        let approveRejectRecord = "";
                        if (((params.data.approvalStatus && params.data.approvalStatus === "Pending Approval") ||
                            (params.data.isOpen))
                            && this.canApproveReject
                        ) {
                            approveRejectRecord = ICON_HTML.APPROVE_REJECT_RECORD;
                        }

                        let viewErrorLink = "";
                        if (params.data.errorRecordCount && params.data.errorRecordCount > 0) {
                            viewErrorLink = ICON_HTML.VIEW_ERROR_LINK;
                        }

                        const auditIcon = ICON_HTML.AUDIT_ICON;
                        const enabledDeleteIcon = ICON_HTML.ENABLED_DELETE_ICON;
                        if (
                            params.data.canAccessEdit ||
                            params.data.canAccessDelete ||
                            params.data.canTerminate ||
                            params.data.canCancelRequest ||
                            params.data.historyAccess ||
                            params.data.canAccessPreviewTargetDefinition ||
                            params.data.canViewDataUsageAgreement ||
                            params.data.canAccessDownloadTargetDefinition ||
                            params.data.canAccessDownloadCleanFile ||
                            params.data.canApproveReject ||
                            params.data.canMappingDefinition ||
                            params.data.canToggle ||
                            params.data.canAccessRefresh
                        ) {

                            if (params.data.canTerminate && params.data.isTerminatedIconClaimList) {
                                finalIcon += terminateRequestIcon;
                            }
                            if (params.data.canCancelRequest && params.data.isCancelIconClaimList) {
                                finalIcon += cancelRequestIcon;
                            }

                            if (params.data.canAccessDownloadCleanFile && !params.data.isDeleted && params.data.isViewErrorLinkClaimList) {
                                finalIcon += viewErrorLink;
                            }
                            if (params.data.canAccessPreviewTargetDefinition && !params.data.isDeleted && params.data.isPreviewTargetDefinitionIconClaimList) {
                                finalIcon += previewTargetDefinitionIcon;
                            }

                            if (params.data.canViewDataUsageAgreement && params.data.isViewDataUsageAgreementIconClaimList) {
                                finalIcon += viewDataUsageAgreement;
                            }

                            if (params.data.canAccessRefresh) {
                                finalIcon += refreshIcon;
                            }
                            if (params.data.canAccessEdit && !params.data.isDeleted && params.data.isEditIconClaimList) {
                                finalIcon += editIcon;
                            }
                            if (params.data.canAccessDelete && !params.data.isDeleted && params.data.isDeleteIconClaimList) {
                                finalIcon += deleteIcon;
                            }
                            if (params.data.historyAccess) {
                                finalIcon += auditIcon;
                            }
                            if (params.data.canAccessDownloadTargetDefinition && !params.data.isDeleted && params.data.isDownloadTargetDefinitionIconClaimList) {
                                finalIcon += downloadTargetDefinitionIcon;
                            }
                            if (params.data.canAccessDownloadCleanFile && !params.data.isDeleted && params.data.isDownloadCleanFileIconClaimList) {
                                finalIcon += downloadCleanFileIcon;
                            }
                            if (params.data.canApproveReject && !params.data.isDeleted && params.data.isApproveRejectRecordClaimList) {
                                finalIcon += approveRejectRecord;
                            }
                            if (params.data.canMappingDefinition) {
                                finalIcon += mappingIcon;
                            }

                            // enable/disable icon
                            if (params.data.canToggle != undefined && params.data.status == MappingStatus.MAPPED) {
                                if (params.data.canToggle) {
                                    finalIcon += inActiveToggleButton;
                                }
                                else if (!params.data.canToggle) {
                                    finalIcon += activeToggleButton;
                                }
                            }
                        }
                        if (params.data.isRemoveEnabled) {
                            finalIcon += enabledDeleteIcon;
                        }
                        return finalIcon;
                    }
                });
            }
            this.columnDefs = columnDefs;
            if (this.gridApi) {
                this.gridApi.setColumnDefs(this.columnDefs);

            }
        } else {
            return "Please provide the headers to assign";
        }
    }

    /**
     * set rows to the datatable
     * @param data data for the datatable in array
     * @param claims should the columns include remove action
     * TODO need to remove isClaims optional parameter
     */
    public setRowsData(data, isRemoveEnabled?, isClaims?, isEditIconClaimList?, isDeleteIconClaimList?, isTerminatedIconClaimList?, isCancelIconClaimList?,
        isPreviewTargetDefinitionIconClaimList?, isDownloadTargetDefinitionIconClaimList?, isViewDataUsageAgreementIconClaimList?, isDownloadCleanFileIconClaimList?,
        isViewErrorLinkClaimList?, isApproveRejectRecordClaimList?) { // isDownloadTargetDefinitionIconClaimList?,
        if (data) {
            data.forEach(row => {
                if (row.canAccessEdit === undefined)
                    row.canAccessEdit = this.canAccessEdit;
                if (row.canAccessDelete === undefined) {
                    if (row.isOpen !== undefined) {
                        row.canAccessDelete = (this.canAccessDelete && (row.isOpen || row.isCompleted)) ? true : false;
                    } else {
                        row.canAccessDelete = this.canAccessDelete;
                    }
                }


                row.canTerminate = (this.canTerminate && (row.isCompleted)) ? true : false;
                row.canCancelRequest = (this.canCancelRequest && (row.isOpen)) ? true : false;
                row.canApproveReject = (row.isOpen) ? true : false;
                row.canAccessPreviewTargetDefinition = this.canAccessPreviewTargetDefinition;
                row.canAccessDownloadTargetDefinition = this.canAccessDownloadTargetDefinition;
                row.canAccessDownloadCleanFile = this.canAccessDownloadCleanFile;
                row.canViewDataUsageAgreement = this.canViewDataUsageAgreement;
                row.canAccessRefresh = this.canAccessRefresh;
                row.isEditIconClaimList = isEditIconClaimList ? isEditIconClaimList.get(row.id) : true;
                row.isDeleteIconClaimList = (isDeleteIconClaimList && this.canAccessDelete) ? isDeleteIconClaimList.get(row.id) : true;
                row.isTerminatedIconClaimList = isTerminatedIconClaimList ? isTerminatedIconClaimList.get(row.id) : true;
                row.isCancelIconClaimList = isCancelIconClaimList ? isCancelIconClaimList.get(row.id) : true;
                row.isPreviewTargetDefinitionIconClaimList = isPreviewTargetDefinitionIconClaimList ? isPreviewTargetDefinitionIconClaimList.get(row.id) : true;
                row.isViewDataUsageAgreementIconClaimList = isViewDataUsageAgreementIconClaimList ? isViewDataUsageAgreementIconClaimList.get(row.id) : true;
                row.isDownloadTargetDefinitionIconClaimList = isDownloadTargetDefinitionIconClaimList ? isDownloadTargetDefinitionIconClaimList.get(row.id) : true;
                row.isDownloadCleanFileIconClaimList = isDownloadCleanFileIconClaimList ? isDownloadCleanFileIconClaimList.get(row.id) : true;
                row.isViewErrorLinkClaimList = isViewErrorLinkClaimList ? isViewErrorLinkClaimList.get(row.id) : true;
                row.isApproveRejectRecordClaimList = isApproveRejectRecordClaimList ? isApproveRejectRecordClaimList.get(row.id) : true;
                row.isRemoveEnabled = this.isRemoveEnabled;
            });

            this.rowData = data;
            // this.claims = claims;
            this.isRemoveEnabled = isRemoveEnabled;
            this.isEditIconClaimList = isEditIconClaimList; //  isEditIconClaimList;
            this.isDeleteIconClaimList = isDeleteIconClaimList; //  isDeleteIconClaimList;
            this.isTerminatedIconClaimList = isTerminatedIconClaimList;
            this.isCancelIconClaimList = isCancelIconClaimList;
            this.isPreviewTargetDefinitionIconClaimList = isPreviewTargetDefinitionIconClaimList; //  isPreviewTargetDefinitionIconClaimList;
            this.isViewDataUsageAgreementIconClaimList = isViewDataUsageAgreementIconClaimList;

            if (this.gridApi) {
                this.gridApi.sizeColumnsToFit();
                this.gridApi.setColumnDefs(this.columnDefs);
            }
            this.events.next({ eventData: null, eventType: "DataChanged" });
        } else {
            return "Please provide the data to assign";
        }
    }
    public getSelectedNodes(): any {
        return this.gridApi.getSelectedNodes();
    }
    public onRemoveSelected(selectedData: any): void {
        this.gridApi.applyTransaction({ remove: selectedData });
    }
    public onAddSelected(selectedData: any): void {
        this.gridApi.applyTransaction({ add: selectedData });
    }
    public getSelectedRows(): any {
        return this.gridApi.getSelectedRows();
    }
    public getAllRowsId(): any {
        let selectedRows: any[] = [];
        // iterate through every node in the grid
        this.gridApi.forEachNode(function (rowNode, index) {
            selectedRows.push(rowNode.data.id);
        });
        return selectedRows;
    }

    public onRowClicked(event: any): void {
        if (event.event.target !== undefined) {
            const actionType = event.event.target.getAttribute("data-action-type");
            const eventData = { rowDetails: event.data, columnId: event.colDef.field };
            switch (actionType) {
                case "refreshClicked":
                    const refreshEventData = { rowDetails: event.data, columnId: event.colDef.field };
                    this.events.next({ eventData: refreshEventData, eventType: GridConfig.refreshClicked });
                    break;
                case "editClicked":
                    this.events.next({ eventData, eventType: GridConfig.editClicked });
                    break;
                case "deleteClicked":
                    this.events.next({ eventData, eventType: GridConfig.deleteClicked });
                    break;
                case "auditLogClicked":
                    this.events.next({ eventData, eventType: GridConfig.auditLogClicked });
                    break;
                case "columnClicked":
                    this.events.next({ eventData, eventType: GridConfig.columnClicked });
                    break;
                case "previewTargetDefinitionClicked":
                    this.events.next({ eventData, eventType: GridConfig.previewTargetDefinitionClicked });
                    break;
                case "downloadFileClicked":
                    this.events.next({ eventData, eventType: GridConfig.downloadFileClicked });
                    break;
                case "downloadCleanFileClicked":
                    this.events.next({ eventData, eventType: GridConfig.downloadCleanFileClicked });
                    break;
                case "viewDataUsageAgreement":
                    this.events.next({ eventData, eventType: GridConfig.viewDataUsageAgreement });
                    break;
                case "viewErrorsClicked":
                    this.events.next({ eventData, eventType: GridConfig.viewErrorsClicked });
                    break;
                case "approveRejectClicked":
                    this.events.next({ eventData, eventType: GridConfig.approveRejectClicked });
                    break;
                case "cancelRequestClicked":
                    this.events.next({ eventData, eventType: GridConfig.cancelRequestClicked });
                    break;
                case "terminateRequestClicked":
                    this.events.next({ eventData, eventType: GridConfig.terminateRequestClicked });
                    break;
                case "checkBoxClicked":
                    const isParentCheckBox = event.event.target.value;
                    if (isParentCheckBox?.toLowerCase() === "parent".toLowerCase()) {
                        this.events.next({ eventData, eventType: GridConfig.parentCheckBoxClicked });
                    } else {
                        this.events.next({ eventData, eventType: GridConfig.checkBoxClicked });
                    }
                    break;
                case "mappingClicked":
                    this.events.next({ eventData, eventType: GridConfig.mappingClicked });
                    break;
                case "toggleClicked":
                    this.events.next({ eventData, eventType: GridConfig.toggleClicked });
                    break;
                case "beneficiaryClicked":
                    this.events.next({ eventData, eventType: GridConfig.beneficiaryClicked });
                    break;
                case "requestMoreUtilityOrDataSetClicked":
                    this.events.next({ eventData, eventType: GridConfig.requestMoreUtilityOrDataSetClicked });
                    break;
                case "resubmitClicked":
                    this.events.next({ eventData, eventType: GridConfig.resubmitClicked });
                    break;
                default:
            }
        }
    }

    public onCellMouseOver(event: any): void {
        if (event.event.target !== undefined) {
            const actionType = event.event.target.getAttribute("data-action-type");
            const eventData = { rowDetails: this.generateClickEventData(event.data), columnId: event.colDef.field };
            switch (actionType) {
                case "onMouseHover":
                    this.events.next({ eventData, eventType: GridConfig.onMouseHover });
                    break;
                default:
            }
        }
    }

    public gridInit(): void {
        this.events.next({ eventData: null, eventType: GridConfig.gridInit });
    }

    public includes(container: any, value: any): boolean {
        let returnValue = false;
        const result = container.indexOf(value);
        if (result >= 0) {
            returnValue = true;
        }
        return returnValue;
    }

    public hasEdit(id: any): any {
        return this.isEditIconClaimList ? this.isEditIconClaimList.get(id) : true;
    }

    public deselectAll(): void {
        this.gridApi.deselectAll();
    }
    /// Ag Grid Implementation
    public sizeToFit(): void {
        if (this.gridApi) {
            this.gridApi.sizeColumnsToFit();
        }
    }
    public onGridReady(event: any): void {
        this.gridApi = event.api;
        this.gridColumnApi = event.columnApi;
        this.gridApi.setColumnDefs(this.columnDefs);
        this.gridApi.closeToolPanel();
        if (window.innerWidth > 768) {
            event.api.sizeColumnsToFit();
        }
        if (this.defaultSortModel) {
            event.api.setSortModel(this.defaultSortModel);
        }
        this.gridApi.setDomLayout("autoHeight");
    }

    public onPageSizeChanged(): void {
        this.gridApi.paginationSetPageSize(Number(this.paginationPageSize));
    }

    public onSearch(): void {
        this.gridApi.setQuickFilter(this.searchText);
        if (this.gridApi.getDisplayedRowCount() === 0) {
            this.gridApi.showNoRowsOverlay();
        } else {
            this.gridApi.hideOverlay();
        }
    }

    /**
     * generating array of values clicked on table
     * for that using headerProperties to get the array of values
     * @param clickedRowData as particular clicked row data
     * @return array of value form clicked row data
     */
    private generateClickEventData(clickedRowData: any): any {
        const headerProperties = _.map(this.pristineHeader, "field");
        const rowData = [];

        rowData.push(clickedRowData.id);
        headerProperties.forEach(header => {
            rowData.push(header, clickedRowData[header]);
        });

        return rowData;
    }

    public clearSearch(): void {
        this.searchText = "";
        this.onSearch();

    }

    public customSort(param1: any, param2: any): number {
        if (typeof param1 === "number") {
            return param1 - param2;
        } else {
            const paramA = param1 ? param1.toLowerCase() : param1;
            const paramB = param2 ? param2.toLowerCase() : param2;

            if (paramA > paramB) {
                return 1;
            } else if (paramA < paramB) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    public customDateSort(param1: any, param2: any): any {
        return new Date(param1).getTime() - new Date(param2).getTime();
    }

}
