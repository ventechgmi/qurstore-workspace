import { Component, OnInit, AfterViewInit, Input, ChangeDetectorRef, ViewChild, ElementRef, OnChanges, OnDestroy } from "@angular/core";
import { filter } from "rxjs/operators";
import { DataGridViewModel } from "../view-models/data-grid.viewmodel";
import { Subscription } from "rxjs";
import { NgxSmartModalService } from "ngx-smart-modal";
import { DataGridRefreshViewModel } from "../view-models/data-grid-refresh.viewmodel";

declare const $: any;
@Component({
    selector: "app-data-table-cmp",
    templateUrl: "./data-grid.component.html",
    styleUrls: ["./data-grid.component.scss"]

})

/**
 * the generic datagrid which can be consumed throught the application.
 */
export class DataGridComponent implements OnInit, OnDestroy {
    @Input() viewModel: DataGridViewModel;
    @Input() refreshViewModel: DataGridRefreshViewModel;
    subscribers = new Subscription();
    dataTable;
    constructor(
        private chRef: ChangeDetectorRef,
        private ngxSmartModalService: NgxSmartModalService
    ) { }

    /**
     * Angular initializers
     */
    ngOnInit() {
        this.initGrid();
    }

    /**
     * Initialize the varible and subscribers
     */
    initGrid() {
        this.subscribers.add(this.viewModel.events.pipe(filter(e => e.eventType === "Refresh")).subscribe(e => this.dataTableReInit()));
        this.subscribers.add(this.viewModel.events.pipe(filter(e => e.eventType === "HeaderChanged")).subscribe(e => this.dataTableReInit()));
        this.subscribers.add(this.viewModel.events.pipe(filter(e => e.eventType === "DataChanged")).subscribe(e => this.dataTableReInit()));
        this.subscribers.add(this.viewModel.events.pipe(filter(e => e.eventType === "ClearSearch")).subscribe(e => this.viewModel.clearSearch()));
        // for set grid row data during grid init life cycle
        this.viewModel.gridInit();
    }

    /**
     * To avoid self removing from the system when logged in as admin
     * @param row - current data row processed by the ngif
     * @Enhancement - Add isDeleteDisplay, isEditDisplay flags from server payload to avoid multiple custom list looping scenarios
     */
    checkSelfDestruct(row: any[]) {
        const targetId = row[0];
       // const currentUserId = this.authService.getUserId();
        // if (targetId === currentUserId) {
          //  return true;
        // }
        return true;
    }

    isUserDeactivated(row: any[]) {
        const targetStatus = row[5];
        const statusFlag = "Deleted";
        if (targetStatus === statusFlag) {
            return true;
        }
        return false;
    }

    dataTableReInit() {
    }

    /**
     * Destroy events - Unsubsciptions made.
     */
    ngOnDestroy() {
        this.subscribers.unsubscribe();
    }
}
