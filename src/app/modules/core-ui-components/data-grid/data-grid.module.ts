import { NgModule } from "@angular/core";
import { DataGridComponent } from "./views/data-grid.component";
import { CommonModule } from "@angular/common";
import { NgxSmartModalModule } from "ngx-smart-modal";
import { NgxUiLoaderModule } from "ngx-ui-loader";
import { ngxUiLoaderConfig } from "src/app/utils/config";
import { ModulesFactoryRegister } from "src/app/modules-factory/modules-factory.module";
import { DataGridRefreshViewModel } from "./view-models/data-grid-refresh.viewmodel";
import { AgGridModule } from "ag-grid-angular";
import { FormsModule } from "@angular/forms";
@NgModule({
  declarations: [DataGridComponent],
  imports: [
    CommonModule, NgxSmartModalModule,
    NgxSmartModalModule.forRoot(),
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    ModulesFactoryRegister.FOR_CHILD(DataGridComponent),
    AgGridModule.withComponents([]),
    FormsModule
  ],
  exports: [DataGridComponent],
  providers: [DataGridRefreshViewModel]
})
export class DataGridModule { }
