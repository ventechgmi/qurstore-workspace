import { NgModule } from "@angular/core";
import { ProgressBarComponent } from "./views/progress-bar.component";
import { CommonModule } from "@angular/common";
import { NgxSmartModalModule } from "ngx-smart-modal";
import { NgxUiLoaderModule } from "ngx-ui-loader";
import { ngxUiLoaderConfig } from "src/app/utils/config";
import { ModulesFactoryRegister } from "src/app/modules-factory/modules-factory.module";
import { ProgressBarRefreshViewModel } from "./view-models/progress-bar-refresh.viewmodel";
import { AgGridModule } from "ag-grid-angular";
import { FormsModule } from "@angular/forms";
@NgModule({
  declarations: [ProgressBarComponent],
  imports: [
    CommonModule, NgxSmartModalModule,
    NgxSmartModalModule.forRoot(),
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    ModulesFactoryRegister.FOR_CHILD(ProgressBarComponent),
    AgGridModule.withComponents([]),
    FormsModule
  ],
  exports: [ProgressBarComponent],
  providers: [ProgressBarRefreshViewModel]
})
export class ProgressBarModule { }
