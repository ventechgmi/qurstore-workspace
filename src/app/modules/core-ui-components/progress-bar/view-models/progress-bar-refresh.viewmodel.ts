import { Subject } from "rxjs";
import { UserEvent } from "../../helpers/user.event";
import { Injectable } from "@angular/core";


/**
 * A Generic grid to consume through out the application.
 */
@Injectable()
export class ProgressBarRefreshViewModel {
    events = new Subject<UserEvent>();
    public constructor() {}

}
