import { Component, Input } from '@angular/core';

@Component({
  selector: 'progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent {
  @Input() progress: any ;
  public barProperties = {
    barType: 'linear',
    color: "#00cc00",
    secondColor: "#D3D3D3",
    textAlignment:"center",
    linear: {
      depth: 22,
      stripped: true,
      active: true,
      label: {
        enable: true,
        value: "",
        color: "#fff",
        fontSize: 15,
        showPercentage: true,
      }
    },
    radial: {
      depth: 3,
      size: 9,
      label: {
        enable: true,
        color: "#09608c",
      }
    }
  }
  constructor() {
   }

}