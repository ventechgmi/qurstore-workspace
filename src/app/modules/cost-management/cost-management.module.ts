import { NgModule } from "@angular/core";
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { CostListViewComponent } from "./views/cost-list-view/cost-list-view.component";
import { DataGridModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import { CostManagementRoutingModule } from "./cost-management-routing-module";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatButtonModule } from "@angular/material/button";
import { MatRadioModule } from '@angular/material/radio';
import { AlertService } from "src/app/utils/alert/alert.service";
import { CostService } from './services/cost.service';
import { SharedModules } from 'src/app/utils/shared-module/shared.modules';
import { MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns';
import { HighchartsChartModule } from 'highcharts-angular';
import { AngularMultiSelectModule } from "../core-ui-components/angular2-multiselect-component/src/lib";
@NgModule(
    {
        declarations: [
            CostListViewComponent
        ],
        imports: [
            AngularMultiSelectModule,
            SharedModules,
            MultiSelectModule,
            NgSelectModule,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            CostManagementRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatButtonModule,
            MatRadioModule,
            DataGridModule,
            MatCheckboxModule,
            HighchartsChartModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [CostListViewComponent],
        providers: [
            AlertService,
            CostService,
            NgxSmartModalService
        ]
    }
)
export class CostManagementModule {

}
