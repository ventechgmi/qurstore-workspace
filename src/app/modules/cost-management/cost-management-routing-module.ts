import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CostListViewComponent } from "./views/cost-list-view/cost-list-view.component";

export const routes: Routes = [
  {
    path: "list",
    component: CostListViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CostManagementRoutingModule { }
