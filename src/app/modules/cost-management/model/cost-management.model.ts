
export class CostManagementConstance {
    public static readonly USERS_NAME = "Users";
    public static readonly AWS_USER_CRED_NAME = "Cred";
    public static readonly NO_TAG_NAME = "Untagged";
    public static readonly TAGGED_COST_NAME = "Tagged";
    public static readonly COMMON_COST_NAME = "Shared";
    public static readonly TAG_VALUE = "Tag Values";
    public static readonly MAIN_SECTION = "Main";
    public static readonly INSTANCE_TYPE = "Instance Types";
    public static readonly SERVICE_TYPE = "Service Types";
    public static readonly NO_INSTANCE_TYPE = "NoInstanceType";
    public static readonly NO_OF_DAYS = "noOfDays";
    public static readonly DATE_RANGE = "dateRange";
    public static readonly CURRENCY_DOLLAR_SYMBOL = "$";
}