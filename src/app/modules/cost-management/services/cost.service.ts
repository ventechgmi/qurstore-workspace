import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
    providedIn: "root"
})
export class CostService extends EntityService {
    appConfig: AppConfig;

    constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
        super(http, "dashboard", appConfig);
        this.appConfig = appConfig;
    }

    private getRawApi(): string {
        return this.appConfig.apiEndPoint;
    }

    public getCostData(filterCriteria): Observable<any> {
        return this.http.post(this.getApi() + "/get-cost-data", filterCriteria).pipe(map((res: Response) => res));
    }

    public getAllTagTypes(): Observable<any> {
        return this.generateGetAPI("/tag-type/list", "");
    }

    public getTagValuesByTagTypeIds(tagTypeIds): Observable<any> {
        return this.generatePostAPI("/tag-type/list/values", tagTypeIds);
    }

    private generateGetAPI(name, req): Observable<any> {
        return this.http
            .get(this.getRawApi() + name + req).pipe(
                map(res => {
                    return res;
                }));
    }

    private generatePostAPI(name, data): Observable<any> {
        return this.http
            .post(this.getRawApi() + name, data).pipe(
                map((res: Response) => res));
    }
}

