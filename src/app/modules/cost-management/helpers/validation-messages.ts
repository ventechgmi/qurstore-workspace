export class FormValidationMessages {
    public static readonly TAG_IS_REQUIRED = "Tag is required";
    public static readonly TAG_VALUE_REQUIRED = "Tag Value is required";

    public static readonly START_DATE_IS_REQUIRED = "From date is required";
    public static readonly END_DATE_IS_REQUIRED = "To date is required";

    public static readonly COST_TYPE_IS_REQUIRED = "Cost Type is required";

    public static readonly TAG_VALUES_REQUIRED = "Please select at least one tag value";
    public static readonly TAG_REQUIRED = "Please select at least one tag";

    public static readonly SELECT_MISSING_TAGS = "Please select listed all tags";
    public static readonly SELECT_MISSING_VALUES = "Please select at least one tag values from all tags";

    public static readonly DAYS_IS_REQUIRED = "Number of days is required";
    public static readonly DAYS_MIN_MAX = "Days should not exceed 365 days.";

    public static readonly NO_COST_DATA_FOUND = "No cost data found";

    public static readonly AMORTIZED_COST_NOTE = `Amortized costs are estimated by combining unblended costs with the amortized portion of your upfront and recurring reservation fees. The unused portion of your upfront reservation fees and recurring reservation charges ae shown on the first of the month.`;
}
