import { AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { AlertService } from "../../../../utils/alert/alert.service";
import { CostListViewModel } from "../../viewmodels/cost-list.viewmodel";
import { CostService } from "../../services/cost.service";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AuthService } from 'src/app/utils/auth.service';

@Component({
  selector: 'app-cost-list-view',
  templateUrl: './cost-list-view.component.html',
  styleUrls: ['./cost-list-view.component.scss'],
  providers: [AlertService, CostListViewModel]
})
export class CostListViewComponent implements OnInit, AfterViewInit {

  @ViewChild("serviceChartParent", { static: false }) serviceChartParent: ElementRef;


  public tagList: any;
  public dataTable: any;
  public tagCreateModalRef: any;
  projectId: any;
  constructor(private router: Router,
    private costService: CostService,
    public costListViewModel: CostListViewModel,
    public alertService: AlertService,
    public authService: AuthService,
    private route: ActivatedRoute,
    public ngxSmartModalService: NgxSmartModalService) { }

  ngAfterViewInit(): void {
    this.costListViewModel.serviceChartParent = this.serviceChartParent;
  }

  public ngOnInit(): void {
    this.route.params
      .subscribe(() => {
        const userId = this.authService.getUserId();
        this.costListViewModel.initServices(this.alertService);
        this.costListViewModel.init({ userId });
        this.costListViewModel.initServices(this.alertService);
      });

    this.route.queryParams.subscribe(res => {
      this.projectId = res.id;
    });
  }
}

