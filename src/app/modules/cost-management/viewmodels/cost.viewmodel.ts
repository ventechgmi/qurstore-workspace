import { EntityBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { CostService } from "../services/cost.service";
import { NgxSmartModalService } from "ngx-smart-modal";
// import { AuthService } from "src/app/utils/auth.service";
import { ActivatedRoute } from "@angular/router";

export class CostViewModel extends EntityBaseViewModel<"", CostService> {
    public moduleId: string;
    public moduleAccess;
    public moduleInfo;
    auditLogPermission: any = {};

    public constructor(
        public costService: CostService,
        public viewModelType: ViewModelType,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute
    ) {
        super(costService, viewModelType);
    }


}
