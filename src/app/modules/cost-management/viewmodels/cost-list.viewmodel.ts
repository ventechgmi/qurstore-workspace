import { from as observableFrom, Observable, Subject, forkJoin } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { CostService } from "../services/cost.service";
import { Injectable, OnInit, ChangeDetectorRef, ElementRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import lodash from "lodash";
import moment from "moment";
import { CostViewModel } from "./cost.viewmodel";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { TagtypeService } from "../../tag-type-management/services/tag-type.service";
import { AuthService } from "src/app/utils/auth.service";
import PieChartConfig from "../../../utils/chart-config-json/pie-chart.json";
import { FormValidationMessages } from "../helpers/validation-messages";
import { v4 as uuid } from "uuid";
import * as Highcharts from "highcharts";
import Drilldown from "highcharts/modules/drilldown";
import { CostManagementConstance } from "../model/cost-management.model";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { FormatterHelper } from "src/app/utils/helper/formatter.helper";
Drilldown(Highcharts);

@Injectable()
export class CostListViewModel extends CostViewModel {
  public serviceChartParent: ElementRef;
  public maxDate = moment().format(config.DB_DATE_FORMAT);
  public Highcharts = Highcharts;
  public costTypeCostDetails: any;
  public serviceTypeList: any;
  public costListViewModelForm: FormGroup;
  public isFirstDrillDown = true;
  public tagTypeList: any;
  // number with precision ex: 5646.65
  public totalServiceTypeCost: string;
  public alertService: AlertService;
  public serviceTypeChartConfig;
  public serviceTypeViewModelDataGrid: DataGridViewModel;
  public serviceChart: any;
  public selectedTags = [];
  public selectedTagValues = [];
  public isTagValueAdded = true;
  public missingValues;
  public selectedTagName = "";
  public selectedTagValueName = "";
  public ValidationMessages = FormValidationMessages;
  public drillDownStateStack = [];
  public drillDownTotalAmountStack = [];
  public isShowGrid = false;
  public settings = {
    text: "--Select--",
    enableSearchFilter: true,
    enableFilterSelectAll: false,
    enableCheckAll: true,
    maxHeight: 250,
    badgeShowLimit: 5,
    singleSelection: false
  };
  public costTypeSettings = {
    text: "--Select--",
    enableSearchFilter: false,
    enableFilterSelectAll: false,
    enableCheckAll: false,
    maxHeight: 250,
    badgeShowLimit: 5,
    singleSelection: true
  };
  public serviceChartCallback = (chart) => {
    this.serviceChart = chart;
  }
  public tagValueList: any;


  protected costId: string;
  protected userId: string;
  protected serviceTypeCostDetailsMap: any;
  private errorDictionary: Map<string, any[]> = new Map();
  protected tagTypeIdValueMap: any;
  protected activeDrillDownServiceTypeList: any;
  protected pristineTagValues: string[];
  protected serviceTypeHeader = [
    { headerName: "Service Type", field: "type" },
    { headerName: "Date", field: "startDate", isDate: true },
    {
      headerName: "Amount",
      field: "roundedAmount",
      valueFormatter: params => FormatterHelper.currencyFormatter(params.data.roundedAmount, config.CURRENCY_DOLLAR_SYMBOL),
      filter: 'agNumberColumnFilter'
    }
  ];
  protected organizationId: string;


  public constructor(
    public costService: CostService,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    private authService: AuthService,
    private ngxService: NgxUiLoaderService
  ) {
    super(costService, ViewModelType.Edit, ngxSmartModalService, route);
    this.costListViewModelForm = this.fb.group({
      selectedTags: [],
      selectedTagValues: [],
      startDate: ["", []],
      endDate: ["", []],
      filterType: [CostManagementConstance.NO_OF_DAYS, []],
      noOfDays: [null, [Validators.required, Validators.max(365)]],
    });

    this.errorDictionary.set(CostManagementConstance.NO_OF_DAYS, [
      { key: "required", message: this.ValidationMessages.DAYS_IS_REQUIRED },
      { key: "max", message: this.ValidationMessages.DAYS_MIN_MAX }]);

    this.errorDictionary.set("startDate", [
      { key: "required", message: this.ValidationMessages.START_DATE_IS_REQUIRED }
    ]);
    this.errorDictionary.set("endDate", [
      { key: "required", message: this.ValidationMessages.END_DATE_IS_REQUIRED }
    ]);
  }

  public init(param?): Observable<any> {
    this.userId = param.userId;
    this.organizationId = this.authService.getOrganizationId();
    this.serviceTypeViewModelDataGrid = new DataGridViewModel();
    this.serviceTypeViewModelDataGrid.setPagination(true);
    this.serviceTypeViewModelDataGrid.setHeader(this.serviceTypeHeader, false, { canUpdate: false });
    this.serviceTypeViewModelDataGrid.defaultSortModel = [
      {
        colId: 'startDate',
        sort: 'desc',
      }
    ];
    this.setDataTableConfigurations();
    this.initTags();
    this.initSubscription();
    this.initCharts();
    return observableFrom([]);
  }


  public errorFor(propertyName: string): string | void {
    if (propertyName) {
      if (this.costListViewModelForm.controls[propertyName] && this.costListViewModelForm.controls[propertyName].errors) {
        return this.makeErrorMessageFor(propertyName, Object.keys(this.costListViewModelForm.controls[propertyName].errors)[0]);
      }
    }
  }

  public initServices(alertService: AlertService): void {
    this.alertService = alertService;
  }

  // data-table configurations for project management
  private setDataTableConfigurations(): void {
    this.serviceTypeViewModelDataGrid.dataTableConfiguration = {
      destroy: true,
      columnDefs: [{ targets: [0], visible: false, searchable: false }, { type: "num-html", targets: [2] }, { orderable: false, targets: 2, searchable: false }],
      order: [[1, "asc"]]
    };
  }

  public get costListViewModelFormControls(): { [key: string]: AbstractControl; } {
    return this.costListViewModelForm.controls;
  }

  public dateComparator(date1: string, date2: string): number {
    var date1Number = this.monthToComparableNumber(date1);
    var date2Number = this.monthToComparableNumber(date2);

    if (date1Number === null && date2Number === null) {
      return 0;
    }
    if (date1Number === null) {
      return -1;
    }
    if (date2Number === null) {
      return 1;
    }

    return date1Number - date2Number;
  }

  public monthToComparableNumber(date: string): number {
    if (date === undefined || date === null || date.length !== 10) {
      return null;
    }

    var yearNumber = Number(date.substring(6, 10));
    var monthNumber = Number(date.substring(3, 5));
    var dayNumber = Number(date.substring(0, 2));

    var result = yearNumber * 10000 + monthNumber * 100 + dayNumber;
    return result;
  }

  public getCost(parentTag?: any, drillDownPoint?: any): void {
    if (this.costListViewModelForm.invalid) {
      return;
    }
    if (this.costListViewModelFormControls.selectedTags.value && this.costListViewModelFormControls.selectedTags.value.length
      && this.costListViewModelFormControls.selectedTagValues.value && !this.costListViewModelFormControls.selectedTagValues.value.length) {
      return this.alertService.error(this.ValidationMessages.TAG_VALUES_REQUIRED, true);
    }
    this.missingValues = "";
    const { tags = [], missingValues } = this.buildTagAndValues();
    const dbObj = {
      parentTag,
      startDate: "",
      endDate: "",
      noOfDays: "",
      organizationId: this.organizationId,
      costType: config.AWS_AMORTIZED_COST,
      tags,
      IsAllTags: false
    }
    if (missingValues) {
      this.missingValues = missingValues;
      return;
    }

    if (this.costListViewModelFormControls.filterType.value === CostManagementConstance.NO_OF_DAYS) {
      dbObj.noOfDays = this.costListViewModelFormControls.noOfDays.value;
    } else {
      dbObj.startDate = this.costListViewModelFormControls.startDate.value;
      dbObj.endDate = this.costListViewModelFormControls.endDate.value;
    }
    if (!dbObj.tags || dbObj.tags && !dbObj.tags.length) {
      dbObj.IsAllTags = true;
    }
    if (dbObj.parentTag) {
      dbObj.IsAllTags = false;
    }
    this.ngxService.start();
    this.costService.getCostData(dbObj).subscribe(
      data => {
        this.onCostFetchSuccess(data, parentTag, drillDownPoint, tags.length)
        this.ngxService.stop();
      },
      error => {
        this.alertService.error(error, true);
        this.ngxService.stop();
      }
    );
  }

  public onSelectTags(event: any): void {
    this.fetchTagValuesByTagTypeId([event.id]);
  }

  public onRemoveTag(event: any): void {
    lodash.remove(this.tagValueList, { tagTypeId: event.id });
    lodash.remove(this.costListViewModelFormControls.selectedTagValues.value, { tagTypeId: event.id });
    const existingTagValues = this.costListViewModelFormControls.selectedTagValues.value || [];
    this.costListViewModelFormControls.selectedTagValues.setValue([...existingTagValues]);
  }

  public selectedAllTags(): void {
    const tagIds = _.map(this.costListViewModelFormControls.selectedTags.value, "id");
    this.fetchTagValuesByTagTypeId(tagIds);
  }

  public deSelectedAllTags(): void {
    this.tagValueList = [];
    this.costListViewModelFormControls.selectedTagValues.setValue([]);
    this.costListViewModelFormControls.selectedTags.setValue([]);
  }

  public deSelectedAllTagsValues(): void {
    this.costListViewModelFormControls.selectedTagValues.setValue([]);
  }

  /// private methods
  private initSubscription(): void {
    this.costListViewModelFormControls.filterType.valueChanges.subscribe(data => {
      if (data === CostManagementConstance.NO_OF_DAYS) {
        this.costListViewModelFormControls.startDate.clearValidators();
        this.costListViewModelFormControls.endDate.clearValidators();
        this.costListViewModelFormControls.noOfDays.setValidators([Validators.required, Validators.max(365)]);
      } else if (data === CostManagementConstance.DATE_RANGE) {
        this.costListViewModelFormControls.noOfDays.clearValidators();
        this.costListViewModelFormControls.startDate.setValidators([Validators.required]);
        this.costListViewModelFormControls.endDate.setValidators([Validators.required]);
      }
      this.costListViewModelFormControls.noOfDays.updateValueAndValidity();
      this.costListViewModelFormControls.endDate.updateValueAndValidity();
      this.costListViewModelFormControls.startDate.updateValueAndValidity();
      this.costListViewModelForm.updateValueAndValidity();
    });
  }

  private serviceChartDrillDown(event: any): void {
    const tag = {
      tag: event.point.options.tagName,
      tagValues: [event.point.options.name]
    }
    this.selectedTagName = event.point.options.tagName;
    this.selectedTagValueName = event.point.options.tagValue;
    this.isFirstDrillDown = false;
    if (event.point.options.sectionType === CostManagementConstance.NO_TAG_NAME) {
      let filteredServiceTypeList = _.filter(this.serviceTypeList, (instanceType) => {
        return instanceType.sectionType === event.point.options.sectionType;
      });
      this.updateTableHeader(false);
      this.initGrid(filteredServiceTypeList);
      this.drillDownStateStack.push(filteredServiceTypeList);
    } else if (event.point.options.sectionType === CostManagementConstance.TAGGED_COST_NAME) {
      this.initGrid([]);
      this.totalServiceTypeCost = event.point.options.y;
      this.drillDownTotalAmountStack.push(event.point.options.y);
    } else if (event.point.options.sectionType === CostManagementConstance.COMMON_COST_NAME || event.point.options.sectionType === CostManagementConstance.AWS_USER_CRED_NAME) {
      let filteredServiceTypeList = _.filter(this.activeDrillDownServiceTypeList, (instanceType) => {
        return instanceType.sectionType === event.point.options.sectionType;
      });
      this.updateTableHeader(event.point.options.sectionType === CostManagementConstance.AWS_USER_CRED_NAME);
      this.initGrid(filteredServiceTypeList);
      this.drillDownStateStack.push(filteredServiceTypeList);
    } else {
      this.getCost(tag, event.point);
      this.totalServiceTypeCost = event.point.options.y;
      this.drillDownTotalAmountStack.push(event.point.options.y);
    }
  }

  private serviceChartDrillUp(): void {
    this.initGrid([]);
    if (!this.isFirstDrillDown) {
      this.drillDownTotalAmountStack.pop();
      this.totalServiceTypeCost = this.drillDownTotalAmountStack[this.drillDownTotalAmountStack.length - 1];
    }
  }

  private initGrid(serviceTypeList: any[]): void {
    if (serviceTypeList) {
      this.serviceTypeViewModelDataGrid.setRowsData(_.map(serviceTypeList, serviceType => ({ ...serviceType, roundedAmount: serviceType.roundedAmount })) || [], false);
    }
    this.isShowGrid = !!serviceTypeList.length;
  }

  private calculateTotal(tagWithValueKeyServiceTypeCostDetailsMap: any): void {
    this.totalServiceTypeCost = this.getTotalCost(tagWithValueKeyServiceTypeCostDetailsMap).toFixed(2);
    this.drillDownTotalAmountStack.push(this.totalServiceTypeCost);
  }

  private getTotalCost(tagWithValueKeyCostDetailsMap: any): number {
    let total = 0;
    _.each(tagWithValueKeyCostDetailsMap, (costDetails) => {
      _.each(costDetails, costDetail => {
        total += costDetail.amount
      });
    });
    return total;
  }

  private makeErrorMessageFor(propertyName: string, errorKey: string): string {
    return this.errorDictionary.get(propertyName).filter(data => data.key === errorKey)[0].message;
  }

  private updateTableHeader(isUserLevelData: boolean) {
    if (isUserLevelData) {
      const serviceTypeHeader = [
        { headerName: "Service Type", field: "type" },
        { headerName: "User Name", field: "userName" },
        { headerName: "Date", field: "startDate", isDate: true },
        { headerName: "Amount", field: "roundedAmount",  valueFormatter: params => FormatterHelper.currencyFormatter(params.data.roundedAmount, '$'),
        filter: 'agNumberColumnFilter' },
      ];
      this.serviceTypeViewModelDataGrid.setHeader(serviceTypeHeader, false, { canUpdate: false });
      this.serviceTypeViewModelDataGrid.gridApi.refreshView();
    } else {
      const serviceTypeHeader = [
        { headerName: "Service Type", field: "type" },
        { headerName: "Date", field: "startDate", isDate: true },
        { headerName: "Amount", field: "roundedAmount",  valueFormatter: params => FormatterHelper.currencyFormatter(params.data.roundedAmount, '$'),
        filter: 'agNumberColumnFilter' },
      ];
      this.serviceTypeViewModelDataGrid.setHeader(serviceTypeHeader, false, { canUpdate: false });
      this.serviceTypeViewModelDataGrid.gridApi.refreshView();
    }

  }

  private onCostFetchSuccess(data: any, parentTag: any, drillDownPoint: any, tags: any): void {
    if (data && data.isSuccess) {
      if (!parentTag) {
        this.drillDownTotalAmountStack = [];
        this.costTypeCostDetails = data.payload.costTypeNameTagValueModelMap;
        this.serviceTypeCostDetailsMap = this.costTypeCostDetails.SERVICE.tagWithValueNameCostDetailsMap;
        this.calculateTotal(this.serviceTypeCostDetailsMap);
        this.serviceTypeList = this.buildCostDetail(this.serviceTypeCostDetailsMap);
        this.format(this.serviceTypeList);
        this.initChartData(!!tags);
        if (!this.serviceTypeList || (this.serviceTypeList && !this.serviceTypeList.length)) {
          this.alertService.info(FormValidationMessages.NO_COST_DATA_FOUND, true);
        }
        this.isFirstDrillDown = true;
      } else {
        const costTypeCostDetails = data.payload.costTypeNameTagValueModelMap;
        const serviceTypeCostDetailsMap = costTypeCostDetails.SERVICE.tagWithValueNameCostDetailsMap;
        this.activeDrillDownServiceTypeList = this.buildCostDetail(serviceTypeCostDetailsMap, parentTag);
        this.format(this.activeDrillDownServiceTypeList);
        this.updateTableHeader(true);
        this.initGrid([]);
        this.drillDownStateStack.push(this.activeDrillDownServiceTypeList);
        this.initChartDrillDown(this.activeDrillDownServiceTypeList, drillDownPoint);
      }
    } else {
      this.alertService.error(data.message, true);
    }
  }

  private initChartDrillDown(serviceTypeList: any[], drillDownPoint: any): void {
    let totalCommonCost = 0;
    let totalUserCost = 0;

    _.each(serviceTypeList, instanceTypeDetail => {
      if (instanceTypeDetail.sectionType === CostManagementConstance.COMMON_COST_NAME) {
        totalCommonCost += parseFloat(instanceTypeDetail.amount);
      } else {
        totalUserCost += parseFloat(instanceTypeDetail.amount);
      }
    });

    const drillDownSeries = {
      name: drillDownPoint.options.name,
      id: drillDownPoint.options.drilldown,
      data: [
        {
          sectionType: CostManagementConstance.COMMON_COST_NAME,
          name: CostManagementConstance.COMMON_COST_NAME,
          tagName: drillDownPoint.options.tagName,
          tagValue: drillDownPoint.options.name,
          drilldown: uuid(),
          y: Number(totalCommonCost.toFixed(2))
        },
        {
          sectionType: CostManagementConstance.AWS_USER_CRED_NAME,
          name: CostManagementConstance.USERS_NAME,
          tagName: drillDownPoint.options.tagName,
          tagValue: drillDownPoint.options.name,
          drilldown: uuid(),
          y: Number(totalUserCost.toFixed(2))
        }
      ]
    }

    if (this.serviceChart) {
      this.serviceChart.addSingleSeriesAsDrilldown(drillDownPoint, drillDownSeries);
      this.serviceChart.applyDrilldown();
    }
  }

  private buildCostDetail(costTypeCostDetailsMap: any, parentTag?: any): any[] {
    const costDetailList = [];
    _.each(costTypeCostDetailsMap, (costDetails, tagWithValueNameKey) => {
      _.each(costDetails, (costTypeCostDetail) => {
        const names = tagWithValueNameKey.split("_");
        let costDetail = {
          ...costTypeCostDetail,
          sectionType: "",
          tagName: tagWithValueNameKey === CostManagementConstance.NO_TAG_NAME || tagWithValueNameKey === CostManagementConstance.COMMON_COST_NAME
            ? tagWithValueNameKey : names[0],
          tagValueName: names[1] || ""
        }

        switch (costDetail.tagName) {
          case CostManagementConstance.COMMON_COST_NAME: {
            costDetail.sectionType = CostManagementConstance.COMMON_COST_NAME;
            costDetail.tagName = parentTag.tag;
            costDetail.tagValueName = parentTag.tagValues[0];
            break;
          }
          case CostManagementConstance.NO_TAG_NAME: {
            costDetail.sectionType = CostManagementConstance.NO_TAG_NAME;
            break;
          }
          case CostManagementConstance.AWS_USER_CRED_NAME: {
            costDetail.sectionType = CostManagementConstance.AWS_USER_CRED_NAME;
            costDetail.tagName = parentTag.tag;
            costDetail.tagValueName = parentTag.tagValues[0];
            break;
          }
          default: { }
        }

        costDetailList.push(costDetail);
      });
    });
    return costDetailList;
  }

  private buildTagAndValues(): any {
    const tagIdInfoMap = {};
    const selectedTagIdInfoMap = {};
    _.each(this.tagTypeList, tag => {
      tagIdInfoMap[tag.id] = tag;
    });
    _.each(this.costListViewModelFormControls.selectedTags.value, tag => {
      selectedTagIdInfoMap[tag.id] = { tag: tagIdInfoMap[tag.id].name, tagValues: [] };
    });
    _.each(this.costListViewModelFormControls.selectedTagValues.value, tagValue => {
      selectedTagIdInfoMap[tagValue.tagTypeId]?.tagValues?.push(tagValue.value);
    });
    let missingValues = "";
    _.each(this.costListViewModelFormControls.selectedTags.value, tag => {
      const isExist = lodash.some(this.costListViewModelFormControls.selectedTagValues.value, (selectedTagValue) => {
        return lodash.find(this.tagTypeIdValueMap[tag.id], { id: selectedTagValue.id });
      })
      if (!isExist) {
        missingValues += tagIdInfoMap[tag.id].name + " (" + _.map(this.tagTypeIdValueMap[tag.id], "value").join(",") + "), ";
      }
    });

    return { tags: Object.values(selectedTagIdInfoMap), missingValues };
  }

  private initCharts(): void {
    this.serviceTypeChartConfig = lodash.cloneDeep(PieChartConfig);
    const serviceTypeChart: any = this.serviceTypeChartConfig;
    serviceTypeChart.chart.events = {
      drilldown: this.serviceChartDrillDown.bind(this),
      drillupall: this.serviceChartDrillUp.bind(this)
    };
    serviceTypeChart.xAxis = {
      type: "category"
    }
    serviceTypeChart.yAxis = {
      title: {
        text: "Amount in $"
      }
    }
  }

  private initChartData(isTagSelected: boolean): void {
    let serviceTypeChartData;
    if (isTagSelected) {
      serviceTypeChartData = this.buildSelectedTagsChartData(this.serviceTypeList, CostManagementConstance.TAG_VALUE);
    } else {
      serviceTypeChartData = this.buildChartData(this.serviceTypeList, CostManagementConstance.MAIN_SECTION);
    }
    this.serviceTypeChartConfig.series = serviceTypeChartData.series;
    this.serviceTypeChartConfig.drilldown.series = serviceTypeChartData.drilldownData;
    if (this.serviceChart) {
      if (this.serviceChart.series && this.serviceChart.series.length) {
        _.each(this.serviceChart.series, seriesInfo => seriesInfo.remove());
      }
      if (this.serviceChart.drilldownLevels) {
        this.serviceChart.drilldownLevels = [];
      }
      this.serviceChart.update({
        series: serviceTypeChartData.series,
        drilldown: {
          ...this.serviceTypeChartConfig.drilldown,
          series: serviceTypeChartData.drilldownData
        }
      }, true, true);
      this.serviceChart.redraw();
    }
  }

  private buildChartData(costDetails: any, chartTitle: string): any {
    const tagTypeTotalCostMap = {};

    _.each(costDetails, instanceTypeDetail => {
      const key = instanceTypeDetail.tagValueName ? instanceTypeDetail.tagName + "_" + instanceTypeDetail.tagValueName : instanceTypeDetail.tagName;
      if (tagTypeTotalCostMap[key]) {
        tagTypeTotalCostMap[key] += parseFloat(instanceTypeDetail.amount);
      } else {
        tagTypeTotalCostMap[key] = parseFloat(instanceTypeDetail.amount);
      }
    });

    _.each(tagTypeTotalCostMap, (amount, key) => {
      tagTypeTotalCostMap[key] = Number(amount.toFixed(2));
    });

    const series = [{
      id: uuid(),
      name: chartTitle,
      colorByPoint: true,
      data: []
    }]

    const unTaggedCostDetail = {
      name: CostManagementConstance.NO_TAG_NAME,
      y: tagTypeTotalCostMap[CostManagementConstance.NO_TAG_NAME],
      drilldown: uuid(),
      sectionType: CostManagementConstance.NO_TAG_NAME
    }
    const taggedCostDetail = {
      name: CostManagementConstance.TAGGED_COST_NAME,
      y: 0,
      drilldown: uuid(),
      sectionType: CostManagementConstance.TAGGED_COST_NAME
    }

    series[0].data.push(unTaggedCostDetail);
    series[0].data.push(taggedCostDetail);

    const seriesData = {
      name: taggedCostDetail.name,
      id: taggedCostDetail.drilldown,
      data: []
    }
    _.each(tagTypeTotalCostMap, (amount, tagTypeKey) => {
      if (tagTypeKey !== CostManagementConstance.NO_TAG_NAME) {
        let tagParts = tagTypeKey.indexOf('_') != -1 ? tagTypeKey.split("_") : null;
        let tagValueName = tagParts ? tagParts[1] : tagTypeKey;
        taggedCostDetail.y += amount;
        const drillData = {
          tagName: tagParts[0],
          tagValue: tagParts[1],
          name: tagValueName,
          id: taggedCostDetail.drilldown,
          y: amount,
          drilldown: uuid()
        }
        seriesData.data.push(drillData);
      }
    });
    return {
      series,
      drilldownData: [seriesData]
    }
  }

  private buildSelectedTagsChartData(costDetails: any, chartTitle: string): any {
    const tagTypeWithTagNameTotalCostMap = {};

    _.each(costDetails, instanceTypeDetail => {
      const key = instanceTypeDetail.tagValueName ? instanceTypeDetail.tagName + "_" + instanceTypeDetail.tagValueName : instanceTypeDetail.tagName;
      if (tagTypeWithTagNameTotalCostMap[key]) {
        tagTypeWithTagNameTotalCostMap[key] += parseFloat(instanceTypeDetail.amount);
      } else {
        tagTypeWithTagNameTotalCostMap[key] = parseFloat(instanceTypeDetail.amount);
      }
    });
    const series = [{
      id: uuid(),
      name: chartTitle,
      colorByPoint: true,
      data: []
    }]

    _.each(tagTypeWithTagNameTotalCostMap, (amount, instanceKey) => {
      tagTypeWithTagNameTotalCostMap[instanceKey] = Number(amount.toFixed(2));
    });

    _.each(tagTypeWithTagNameTotalCostMap, (amount, tagTypeKey) => {
      let tagParts = tagTypeKey.split("_");
      let tagName = tagParts[0];
      let tagValueName = tagParts[1];
      const seriesData = {
        tagName,
        name: tagValueName,
        y: amount,
        drilldown: uuid()
      }
      series[0].data.push({ ...seriesData, y: Number(seriesData.y.toFixed(2)) });
    });
    return {
      series,
      drilldownData: []
    }
  }

  private initTags(): void {
    this.costService.getAllTagTypes()
      .subscribe(result => {
        if (result) {
          this.tagTypeList = _.map(result, tag => ({ ...tag, itemName: tag.name }));
        }
      },
        error => {
          this.alertService.error(error);
        }
      );
  }

  private fetchTagValuesByTagTypeId(tagTypeIds: string[]): void {
    this.costService.getTagValuesByTagTypeIds(tagTypeIds)
      .subscribe((response) => {
        this.pristineTagValues = this.formatTagValuesName(response);
        this.tagValueList = [...this.pristineTagValues];
        const newlyAddedTypeValues = [];
        const tagTypeIdValuesMap = lodash.groupBy(response, "tagTypeId");
        const existingValuesTagIds = _.map(this.costListViewModelFormControls.selectedTagValues.value, "tagTypeId");
        _.each(tagTypeIdValuesMap, (values, tagTypeId) => {
          if (!existingValuesTagIds.includes(tagTypeId)) {
            newlyAddedTypeValues.push(...values);
          }
        });
        const existingTagValues = this.costListViewModelFormControls.selectedTagValues.value || [];
        this.costListViewModelFormControls.selectedTagValues.setValue([...newlyAddedTypeValues, ...existingTagValues]);
        this.tagTypeIdValueMap = _.groupBy([...newlyAddedTypeValues, ...existingTagValues], "tagTypeId");
      })
  }

  private formatTagValuesName(tagValueList: any[]): any[] {
    const tagIdInfoMap = {};
    _.each(this.tagTypeList, tag => {
      tagIdInfoMap[tag.id] = tag;
    });
    _.each(tagValueList, tagValue => {
      tagValue.itemName = tagIdInfoMap[tagValue.tagTypeId].name + "(" + tagValue.value + ")";
      tagValue.displayName = tagIdInfoMap[tagValue.tagTypeId].name + "(" + tagValue.value + ")";
    });
    return tagValueList;
  }

  private format(costTypeResourcesMapList: any[]): void {
    _.each(costTypeResourcesMapList, (costInfo) => {
      // roundOf
      costInfo.roundedAmount = costInfo.amount.toFixed(2);
      // format date
      costInfo.startDate = moment(costInfo.startDate).format(config.FULL_DATE_FORMAT);
    });
  }

}
