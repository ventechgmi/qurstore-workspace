import { NgModule } from "@angular/core";
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { DataGridModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';

import { NotificationService } from "./services/notification.service";
import { NotificationListViewComponent } from "./views/notification/notification-list/notification-list.component";

import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatButtonModule } from "@angular/material/button";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";
import { NotificationManagementRoutingModule } from './notification-management-routing-module';
import {MatSliderModule} from '@angular/material/slider';
@NgModule(
    {
        declarations: [
            NotificationListViewComponent,
        ],
        imports: [
            SharedModules,
            NgSelectModule,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            NotificationManagementRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatButtonModule,
            DataGridModule,
            MatCheckboxModule,
            MatSlideToggleModule,
            NgxSmartModalModule.forRoot(),
            RecaptchaModule,
            RecaptchaFormsModule,
            MatSliderModule
        ],
        exports: [
            NotificationListViewComponent
        ],
        providers: [
            AlertService,
            NotificationService,
            NgxSmartModalService
        ]
    }
)
export class NotificationManagementModule {

}
