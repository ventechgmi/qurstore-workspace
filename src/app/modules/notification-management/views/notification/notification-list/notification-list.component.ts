import {
    Component,
    OnInit,
    ContentChild,
    AfterViewInit,
    ViewChild,
    ElementRef,
    AfterContentInit
} from "@angular/core";

import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormBuilder } from "@angular/forms";
import config from "../../../../../utils/config";

import { NotificationListViewModel } from "../../../viewmodels/notification/notification-list.viewmodel";
import { NotificationService } from "../../../services/notification.service";
import { AlertComponent } from "src/app/utils/alert/alert.component";
import { AlertService } from "../../../../../utils/alert/alert.service";
import { AuthService } from 'src/app/utils/auth.service';

@Component({
    selector: "app-notification-list",
    templateUrl: "./notification-list.component.html",
    styleUrls: ["./notification-list.component.scss"],
    providers: [ NotificationListViewModel, AlertService]
})
export class NotificationListViewComponent implements OnInit, AfterContentInit {

    constructor(private router: Router,
                private NotificationService: NotificationService,
                public notificationListViewModel: NotificationListViewModel,
                public alertService: AlertService,
                private route: ActivatedRoute,
                public ngxSmartModalService: NgxSmartModalService,
                public authService: AuthService,
    ) {
        // need to set the afterInitObs to show other screen success or error message in different screen
        alertService.afterInitObs = NotificationService.getAfterIntObs();
    }

    public ngOnInit() {
        this.route.params
            .subscribe(params => {
              const userId = this.authService.getUserId();
               this.notificationListViewModel.initServices(this.alertService);
               this.notificationListViewModel.init({ userId });
            });
      
    }

    public ngAfterContentInit() { }
}
