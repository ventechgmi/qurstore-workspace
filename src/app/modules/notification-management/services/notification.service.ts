
import { map, debounceTime } from 'rxjs/operators';
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { EntityService, EntityQuery, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
import { of as observableOf } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class NotificationService extends EntityService {
    appConfig: AppConfig;
    onUpdateListEvent = new EventEmitter();
    public userIdSubject = new ReplaySubject<any>();
    public alertInfo = { message: "", type: "" };

    // this subscription will help to show error or success message in another screen(details screen delete success message showing in list screen)
    private afterInitSubject = new BehaviorSubject(this.alertInfo);

    constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
        super(http, "notification", appConfig);
        this.appConfig = appConfig;
    }
    private getAPIURL() {
        return this.getApi();
    }


    public showAfterIntMessage(messageInfo) {
        this.afterInitSubject.next(messageInfo);
    }

    public getAfterIntObs(): Observable<any> {
        return this.afterInitSubject.asObservable();
    }

    public getUserIdSubject() {
        return this.userIdSubject.asObservable();
    }


    public getUpdateUserListEvent() {
        return this.onUpdateListEvent;
    }


    private generateGetAPI(name, req): Observable<any> {
        return this.http.get(this.getAPIURL() + name + req).pipe(
            map(res => {
                return res;
            }));
    }

    private generatePostAPI(name, data): Observable<any> {
        return this.http.post(this.getAPIURL() + name, data).pipe(
            map((res: Response) => res));
    }

    public getNotifications(unReadNotification): Observable<any> {
     
        return this.generateGetAPI("/list/",unReadNotification);
    }

    public updateNotificationAsRead(req: any): Observable<any> {
     
        return this.generatePostAPI("/update-status-by-id", req);
    }

    public updateAllUnreadNotificationAsRead(): Observable<any> {
     
        return this.generatePostAPI("/update-status-by-user","");
    }

    public getLimitedNotification(): Observable<any> {
     
        return this.generateGetAPI("/limited-list","");
    }

    public getUnreadNotificationCount(): Observable<any> {
     
        return this.generateGetAPI("/unread-count","");
    }

}
