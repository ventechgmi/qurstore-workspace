
import { EntityBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { Subject } from "rxjs";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ActivatedRoute } from "@angular/router";
import { ROUTES_CONFIG_MAP, MODULE_BASE_URL_MODULE_ID_MAP } from "src/app/utils/config";
import { NotificationService } from "../../services/notification.service";
import { AuthService } from 'src/app/utils/auth.service';

export class NotificationViewModel extends EntityBaseViewModel<"", NotificationService> {
    public moduleId: string;
    public moduleAccess;
    public moduleInfo;
    public updateUserListSubject = new Subject<any>();
    public userDetailSelection = new Subject<any>();
    

    public constructor(
        public NotificationService: NotificationService,
        public viewModelType: ViewModelType,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute,
        public authService: AuthService)
    {
        super(NotificationService, viewModelType);
        this.entityName = "notification";
    }


    public getUserClaims() {
        const baseUrl = ROUTES_CONFIG_MAP.NOTIFICATION_MANAGEMENT.BASE_URL;
        this.moduleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
        this.moduleInfo = this.authService.getModuleAccess(this.moduleId) || null;
        this.moduleAccess = this.moduleInfo ? this.moduleInfo.RolePermissions : {};
    
        return this.moduleAccess;
    }


}
