
import { from as observableFrom } from "rxjs";
import { Injectable } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import { ActivatedRoute } from "@angular/router";
import * as _ from "lodash";
import { DataGridViewModel } from "../../../core-ui-components";
import config, { GridConfig } from "../../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from "../../../../utils/alert/alert.service";
import { NotificationService } from "../../services/notification.service";
import { NotificationViewModel } from "./notification.viewmodel";
import { AuthService } from '../../../../utils/auth.service';
import moment from 'moment';

@Injectable()
export class NotificationListViewModel extends NotificationViewModel {

  public viewModelDataGrid: DataGridViewModel;
  public alertService: AlertService;
  public isToShowUnreadMessage: boolean = true;
  public notificationData: any;

  public constructor(
    public notificationService: NotificationService,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public authService: AuthService
  ) {
    super(notificationService, ViewModelType.Edit, ngxSmartModalService, route, authService);
  }


  public init(param?) {
    this.viewModelDataGrid = new DataGridViewModel();
    this.viewModelDataGrid.defaultSortModel = [
      {
        colId: 'notificationDate',
        sort: 'desc',
      }];
    this.initializeGridColDef();
    this.getNotifications();
    this.subscribers();
    return observableFrom([]);
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }
  public showUnreadMessageChanged(): void {
    this.getNotifications();
  }

  private subscribers(): void {
    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.columnClicked:
          this.markNotificationAsRead(e.eventData.rowDetails);
          this.redirectToDetailsPage(e.eventData.rowDetails.actionUrl);
          break;
      }
    }, error => {
      this.alertService.error(error);
    });
  }

  private redirectToDetailsPage(url: string) {
    window.open(url);
  }

  public initializeGridColDef() {
    const notification = {
      headerName: 'Notification',
      field: 'notificationText',
      width: 650,
      cellRenderer: (param: any) => {
        return `<a routerLink="" class ="hyperLink" data-action-type="columnClicked" aria-hidden="true">` + param.value + `</a>`;

      },
    };
    this.viewModelDataGrid.columnDefs.push(notification);
    const notificationDate = {
      headerName: 'Notified Date',
      field: 'notificationDate',
      width: 200,
      isDate: true
    };
    this.viewModelDataGrid.columnDefs.push(notificationDate);

    const sentby = {
      headerName: 'Sent By',
      field: 'sender',
      width: 150,
    };
    this.viewModelDataGrid.columnDefs.push(sentby);

    const status = {
      headerName: 'Status',
      field: 'status',
      width: 100,
    };
    this.viewModelDataGrid.columnDefs.push(status);

    const readDate = {
      headerName: 'Read Date',
      field: 'readDate',
      width: 200,
    };
    this.viewModelDataGrid.columnDefs.push(readDate);
    this.viewModelDataGrid.setPagination(true);
  }

  private getNotifications(): void {
    this.notificationService.getNotifications(this.isToShowUnreadMessage).subscribe(data => {
      if (data) {
        this.notificationData = _.orderBy(data, ["notificationDate"], ["desc"]);
        _.each(this.notificationData, item => {
          item.status = item.isRead ? "Read" : "Unread";
          item.sender = item.sender.fullName;
          item.notificationDate = moment(item.notificationDate).format(config.APPLICATION_DATE_TIME_FORMAT);
          item.readDate = item.readDate ? moment(item.readDate).format(config.APPLICATION_DATE_TIME_FORMAT) : "";
        });
        this.viewModelDataGrid.setRowsData(this.notificationData);
      }
    }, (error) => {
      this.alertService.error(error);
    });

  }

  private markNotificationAsRead(notificationData: any): void {
    if (notificationData.status === "Unread") {
      const request = { Id: notificationData.id }
      this.notificationService.updateNotificationAsRead(request).subscribe(data => {
        if (data.isSuccess) {
          this.getNotifications();
        }
      });
    }
  }

}
