import { Routes, RouterModule } from "@angular/router";
import { NotificationListViewComponent } from "./views/notification/notification-list/notification-list.component";

import { NgModule } from "@angular/core";

export const routes: Routes = [
  {
    path: "list",
    component: NotificationListViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationManagementRoutingModule { }
