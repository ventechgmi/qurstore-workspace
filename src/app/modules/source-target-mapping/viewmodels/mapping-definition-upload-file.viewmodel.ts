import { from as observableFrom, interval, Observable } from "rxjs";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControlName } from "@angular/forms";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ValidationMessages } from '../helpers/validation-messages';
import { AlertService } from "../../../utils/alert/alert.service";
import { MappingDefinitionService } from '../services/mapping-definition.service';
import { LocalStorageManager } from 'src/app/utils/local-storage-manager';
import config, { LocalStorageKeyCollection } from 'src/app/utils/config';
import { AuthService } from 'src/app/utils/auth.service';
import { MappingDefinitionViewModel } from './mapping-definition.viewmodel';
import { MappingDefinitionUploadFileViewModelConfig, Constant, FileType, FormControlNameConstant } from "../helpers/config";
import { NgxUiLoaderService } from "ngx-ui-loader";

@Injectable()
export class MappingDefinitionUploadFileViewModel extends MappingDefinitionViewModel {
  public sourceDefinitionInProgressGuid = MappingDefinitionUploadFileViewModelConfig.sourceDefinitionInProgressGuid;
  public refMapDefFlowStatusId = MappingDefinitionUploadFileViewModelConfig.refMapDefFlowStatusId;
  public refMapDefStatusId = MappingDefinitionUploadFileViewModelConfig.refMapDefStatusId;
  public mappingDefinitionId: string;
  public mappingDefinitionName: string;
  public targetDefinitionId: string;
  public fileUploadId: any;
  public fileName: string;
  public mappingDefinitionDetailsForm: FormGroup;
  public mappingDefinitionUploadForm: FormGroup;
  public mappingDetailEntity: any = {};
  public submitted = false;
  public modalType = Constant.edit || Constant.create || Constant.detail;
  public isExist: boolean;
  public file: any;
  public organizationId: string;
  public barOptions = {
    barType: 'linear',
    color: "#00cc00",
    secondColor: "#D3D3D3",
    progress: 0,
    textAlignment: "center",
    linear: {
      depth: 22,
      stripped: true,
      active: true,
      label: {
        enable: true,
        value: "",
        color: "#fff",
        fontSize: 15,
        showPercentage: true,
      }
    },
    radial: {
      depth: 3,
      size: 9,
      label: {
        enable: true,
        color: "#09608c",
      }
    }
  }
  public uploadNewFile: string ="uploadNewFile";
  private mappingDefinitionCancelConfirmModal: string  ="mappingDefinitionCancelConfirmModal";
  public fileTypes = [{id:"CSV",name:"CSV"},{id:"XLSX",name:"EXCEL"},{id:"TEXT",name:"Text"}];
  public delimiterInformation = [{id:",",name:","},{id:"|",name:"|"},{id:"~",name:"~"},{id:"none",name:"None"}];
  public isHeaderInformation = [{id:"1",name:"Yes"},{id:"0",name:"No"}];
  public isTheDataFileBeUsedForDataIngestion: boolean= false;
  public isDataIngestion: boolean = true;
  public dataIngestionOrganizationProperty: string ="Data_Ingestion";
  public progressValue: number = 33;
  public subscription: any;
  private sourceMappingDefinitionS3Folder = "SourceMappingDefinitionS3Folder";
  private sourceMappingS3bucketNameKey = "AWSStagingLandingZoneBucketName";
  public loaderText: string = "";
  public constructor(
    public mappingDefinitionService: MappingDefinitionService,
    private router: Router,
    private fb: FormBuilder,
    private UploadFb: FormBuilder,
    public alertService: AlertService,
    public ngxSmartModalService: NgxSmartModalService,
    private chRef: ChangeDetectorRef,
    private mappingColumnsLocalStorageManager: LocalStorageManager,
    public authService: AuthService,
    public route: ActivatedRoute,
    private ngxService: NgxUiLoaderService
  ) {
    super(mappingDefinitionService, ViewModelType.ReadOnly, ngxSmartModalService, route,authService);
  }

  public init(params?) {
    this.modalType = params.action;
    this.targetDefinitionId = params.targetDefinitionId;
    this.organizationId = this.authService.getOrganizationId();
    this.getOrganizationPropertyByName(this.organizationId,this.dataIngestionOrganizationProperty)
    this.mappingDefinitionDetailsForm = this.fb.group({     
      mappingDefinitionName: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(50), super.validateSpecialCharactersAndWhiteSpace(3)]],
      filePattern:"",
      isTheDataFileBeUsedForDataIngestion:false

    });
    this.mappingDefinitionUploadForm = this.UploadFb.group({
      file_type: [null, [Validators.required, , ,]],
      fileUpload: [null, [Validators.required, , ,]],
      isHeaderInformationAvailable: [null, [Validators.required, , ,]],
      delimiterInformation: [null, [Validators.required, , ,]],
    });
    this.disableFormControls();
    return observableFrom([]);
  }

  private disableFormControls(): void {
    this.mappingDefinitionDetailsForm.get(FormControlNameConstant.mappingDefinitionName).disable();
    this.mappingDefinitionDetailsForm.get(FormControlNameConstant.filePattern).disable();
    this.mappingDefinitionDetailsForm.get(FormControlNameConstant.isTheDataFileBeUsedForDataIngestion).disable();
  }

  private enableFormControls(): void {
    this.mappingDefinitionDetailsForm.get(FormControlNameConstant.mappingDefinitionName).enable();
    this.mappingDefinitionDetailsForm.get(FormControlNameConstant.filePattern).enable();
    this.mappingDefinitionDetailsForm.get(FormControlNameConstant.isTheDataFileBeUsedForDataIngestion).enable();
  }

  public transformToEntityModel() {
    const { file_type, fileUpload, isHeaderInformationAvailable, delimiterInformation} = this.mappingDefinitionUploadFormControls;
    const { mappingDefinitionName,filePattern,isTheDataFileBeUsedForDataIngestion } = this.mappingDefinitionDetailsFormControls;

      this.mappingDetailEntity.id= this.mappingDefinitionId;
      this.mappingDetailEntity.fileUploadId= this.fileUploadId;
      this.mappingDetailEntity.file_type= file_type.value;
      this.mappingDetailEntity.file_header_row_indicator=  '1';
      this.mappingDetailEntity.delimiter= ',';
      this.mappingDetailEntity.mappingDefinitionName= mappingDefinitionName.value;
      this.mappingDetailEntity.lastModifiedBy= this.authService.getUserId();
      this.mappingDetailEntity.uploadedFileDataType= "SRC";
      this.mappingDetailEntity.isSourceMappingDefinitionFileUpload = true;
      this.mappingDetailEntity.filePattern = filePattern.value;
      this.mappingDetailEntity.isDataIngestionRequired = this.isTheDataFileBeUsedForDataIngestion
    if (this.file) {
      this.mappingDetailEntity.file_name = this.file.name;
    }
    return this.mappingDetailEntity;
  }

  public async saveChanges() {
    this.submitted = true;
    if (this.mappingDefinitionDetailsForm.invalid) { return; }
    if (!this.isValidInput()) {
      return;
    }
    this.transformToEntityModel();
    if (this.modalType === Constant.create) {    
      this.saveTargetDefinition(this.mappingDetailEntity);
    } else if (this.modalType === Constant.edit && this.file) {
      this.ngxSmartModalService.getModal(this.uploadNewFile).open();
    }
    else if (this.modalType === Constant.edit) {
      this.mappingDetailEntity.fileChanges = false;
      this.saveTargetDefinition(this.mappingDetailEntity);
    }    
  }

  public isUploadedFileEnabledOrNot() {
    this.loaderText = Constant.loaderScanningInProgressText;
    this.mappingDefinitionService.isUploadedFileEnabledOrNot(this.fileUploadId).subscribe(data => {
      if (data.message === Constant.fileUploadSuccessMessage) {
        this.alertService.success(data.message);
        this.subscription.unsubscribe();
        this.enableFormControls();
        this.mappingDefinitionDetailsForm.markAsDirty();
        this.ngxService.stop();
      }
      else if(data.message === Constant.maliciousFileUploadMessage) {
        this.alertService.error(data.message);
        this.subscription.unsubscribe();
        this.ngxService.stop();
      }
    }, error => {
      this.subscription.unsubscribe();
      this.ngxService.stop();
      if (error?.error?.errorList && error.error.errorList.length > 0) {
        this.alertService.error(error.error.errorList[0], false);
      } else if (error.error) {
        this.alertService.error(error.error, false);
      }
      else if (error.message) {
        this.alertService.error(error.message, false);
      }
      else {
        this.alertService.error(ValidationMessages.ErrorOccurred, false);
      }
    });
  }

  public uploadFileToActivity(): void {
    this.ngxService.start();
    this.loaderText = Constant.loaderUploadingText;
    this.transformToEntityModel();
    this.mappingDetailEntity.fileChanges = true;
    this.mappingDetailEntity.filePattern = "";
    this.mappingDetailEntity.folderPathKey = this.sourceMappingDefinitionS3Folder;
    this.mappingDetailEntity.bucketNameKey = this.sourceMappingS3bucketNameKey;
    this.masterFile = this.file;
    this.masterMappingDetailEntity = this.mappingDetailEntity;
    this.mappingDefinitionService.saveFileContent(this.mappingDetailEntity, this.file).subscribe(data => {
      if (data?.id && data.isSuccess) {
        this.fileUploadId = data.id;
        this.mappingDetailEntity.uploadId = data.id;
        this.mappingDetailEntity.fileUploadId = data.id;
        this.mappingDetailEntity.columnList = data.columnList;
        this.mappingDetailEntity.sourceDefinitionName = this.mappingDetailEntity.mappingDefinitionName;
        this.mappingDetailEntity.targetDefinitionId = this.targetDefinitionId;
        this.mappingDetailEntity.organizationId = this.organizationId;
        this.mappingDetailEntity.sourceDefinitionInProgressGuid = this.sourceDefinitionInProgressGuid;
        this.mappingDetailEntity.refMapDefFlowStatusId = this.refMapDefFlowStatusId;
        this.mappingDetailEntity.refMapDefStatusId = this.refMapDefStatusId;
        
        this.subscription= interval(config.FILE_UPLOAD_INTERVAL).subscribe(() => {
            this.isUploadedFileEnabledOrNot();
        });
      } else {
        this.alertService.error(ValidationMessages.ErrorOccurred, false);
        this.ngxService.stop();
      }
    }, error => {
      this.ngxService.stop();
      if (error?.error?.errorList && error.error.errorList.length > 0) {
        this.alertService.error(error.error.errorList[0], false);
      }else if (error.error) {
        this.alertService.error(error.error, false);
      }
      else if (error.message) {
        this.alertService.error(error.message, false);
      }
      else {
        this.alertService.error(ValidationMessages.ErrorOccurred, false);
      }
    });
  }

  public saveTargetDefinition(data: any): void {
    this.addMappingFileDataToLocalStorage(this.mappingDetailEntity);
    this.transformDataForSaveSourceDefinition(); // to change some Keys in this.mappingDetailEntity  
    this.mappingDefinitionService.saveSourceDefinition(this.mappingDetailEntity).subscribe(tdData => {
      if (tdData?.id && tdData.isSuccess) {
        this.mappingDefinitionId = tdData.mappingDefinitionId;
        this.mappingDetailEntity.sourceDefinitionId = tdData.sourceDefinitionId;
        this.mappingDetailEntity.mappingDefinitionId = tdData.mappingDefinitionId;
      }
      this.redirectToDefineData(this.modalType, this.targetDefinitionId, tdData.sourceDefinitionId, tdData.mappingDefinitionId,this.fileUploadId);
    }, error => {
      this.alertService.error(error, false);
    });
  }

  public redirectToDefineData(action: string, targetDefinitionId: string, sourceDefinitionId: string, mappingDefinitionId: string,uploadedFileId:string): any {
    return this.router.navigate([`define-data/${action}/${targetDefinitionId}/${sourceDefinitionId}/${mappingDefinitionId}/${uploadedFileId}`]);
  }

  public fileChanged(e: any): void {
    this.file = e.target.files[0];
    this.changeFileTypeDropdown();
  }

  public get mappingDefinitionDetailsFormControls(): any {
    return this.mappingDefinitionDetailsForm.controls;
  }
  public get mappingDefinitionUploadFormControls(): any {
    return this.mappingDefinitionUploadForm.controls;
  }

  public onCancel(): void {
    if (this.modalType === Constant.edit || this.modalType === Constant.create) {
      this.mappingDefinitionDetailsForm.dirty ? this.ngxSmartModalService.getModal(this.mappingDefinitionCancelConfirmModal).open() : this.navigateToList().then().catch();
    } else {
      this.navigateToList().then().catch();
    }
  }

  public async navigateToList(): Promise<any> {
    return this.router.navigate(["source-target-mapping/mapping-definition-list"]);
  }

  private transformDataForSaveSourceDefinition(): void {
    this.mappingDetailEntity.fileHeaderRowIndicator = this.mappingDetailEntity.file_header_row_indicator;
    this.mappingDetailEntity.fileName = this.mappingDetailEntity.file_name;
    this.mappingDetailEntity.fileType = this.mappingDetailEntity.file_type;
    delete (this.mappingDetailEntity.file_header_row_indicator);
    delete (this.mappingDetailEntity.file_name);
    delete (this.mappingDetailEntity.file_type);
    delete (this.mappingDetailEntity.isSourceMappingDefinitionFileUpload);
    delete (this.mappingDetailEntity.uploadedFileDataType);
  }

  private changeFileTypeDropdown(): void {
    const fileNameArray = this.file.name.split(".");
    const extensionName = fileNameArray[fileNameArray.length - 1];
    if (extensionName.toUpperCase() === FileType.CSV) {
      this.mappingDefinitionUploadForm.patchValue({
        file_type: FileType.CSV,
        delimiterInformation: ","
      });
      this.mappingDefinitionUploadForm.get(FormControlNameConstant.fileType).disable();
    } else if (extensionName.toUpperCase() === FileType.XLSX) {
      this.mappingDefinitionUploadForm.patchValue({
        file_type: FileType.XLSX,
        delimiterInformation: "none"
      });
      this.mappingDefinitionUploadForm.get(FormControlNameConstant.fileType).disable();
    }
    
  }

  private addMappingFileDataToLocalStorage(data: any): void {
    this.mappingColumnsLocalStorageManager.setLocalStorageItem(LocalStorageKeyCollection.MappingColumnList, data);
  }

  private isValidInput(): boolean {
    let isValid = true;
    if (!!!this.mappingDefinitionId && !!!this.file) {
      isValid = false;
      this.alertService.error(ValidationMessages.FileNotUploaded, false);
    }
    return isValid;
  }

  public getOrganizationPropertyByName(organizationId: string, names: string) {
    this.mappingDefinitionService.getOrganizationPropertyByName(organizationId, names).subscribe(data => {
      if (data && data.isSuccess) {
        if (data.payload && data.payload.length > 0 && data.payload[0].name === names) {
          this.isDataIngestion = data.payload[0].value == "true"? true :false;
        }
      }
    }, error => {
      this.alertService.error(error, false);
    });
  }

}
