
import { map } from "rxjs/operators";
import { EntityBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { Observable, Subject, of } from "rxjs";
import { AbstractControl, ValidatorFn } from "@angular/forms";
import { NgxSmartModalService } from "ngx-smart-modal";
// import { AuthService } from "src/app/utils/auth.service";
import { ActivatedRoute } from "@angular/router";
import { MappingDefinitionService } from '../services/mapping-definition.service';
import { AlertService } from 'src/app/utils/alert/alert.service';
import { MODULE_BASE_URL_MODULE_ID_MAP, ROUTES_CONFIG_MAP } from "src/app/utils/config";
import { AuthService } from "src/app/utils/auth.service";
export class MappingDefinitionViewModel extends EntityBaseViewModel<"", MappingDefinitionService> {
    public moduleId: string;
    public moduleAccess;
    public moduleInfo;
    public updateVendorListSubject = new Subject<any>();
    public vendorDetailSelection = new Subject<any>();
    public auditLogPermission: any = {};
    public alertService: AlertService;
    public masterMappingDetailEntity: any;
    public masterFile: Blob;
    public constructor(
        public mappingDefinitionService: MappingDefinitionService,
        public viewModelType: ViewModelType,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute,
        public authService: AuthService
    ) {
        super(mappingDefinitionService, viewModelType);
        this.entityName = "DataSets";
    }

    public initServices(alertService: AlertService) {
        this.alertService = alertService;
    }
    
    public getUserClaims(): any {
        const baseUrl = ROUTES_CONFIG_MAP.SOURCE_TARGET_MAPPING.BASE_URL;
        this.moduleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
        this.moduleInfo = this.authService.getModuleAccess(this.moduleId) || null;
        this.moduleAccess = this.moduleInfo ? this.moduleInfo.RolePermissions : {};
        return this.moduleAccess;
    }

    public validateSpecialCharactersAndWhiteSpace(lengthMin: number): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            const pattern = "[A-Za-z0-9]";
            const whiteSpace = /^\s*$/;
            if ((control.value || "").length === 0) {
                return null;
            } else if ((control.value || "").trim().length < lengthMin) {
                return { minlength: true };
            } else if (control.value.match(whiteSpace)) {
                return { hasOnlyWhiteSpace: true };
            } else if (!control.value.match(pattern)) { // check if the input doest not have any alphabets or in other words input has only special characters
                // return "Don't enter only special characters";
                return { hasOnlySpecialCharacters: true };
            }
        };
    }

}
