import { from as observableFrom } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel,DataGridExtendedViewModel } from "../../core-ui-components";
import { NgxSmartModalService } from "ngx-smart-modal";
import { TargetDefinitionService } from "../../target-definition/services/target-definition.service";
import { GridConfig } from 'src/app/utils/config';
import { AuthService } from 'src/app/utils/auth.service';
import { MappingDefinitionService } from "../services/mapping-definition.service";
import { ValidationMessages } from '../helpers/validation-messages';
import { Constant, PreviewSourceTargetMappingDefinitionViewModelConfig } from "../helpers/config";

@Injectable()
export class PreviewSourceTargetMappingDefinitionViewModel {
  public refFlowStatusId = PreviewSourceTargetMappingDefinitionViewModelConfig.refFlowStatusId;
  public refMapDefStatusId = PreviewSourceTargetMappingDefinitionViewModelConfig.refMapDefStatusId;
  public alertService: AlertService;
  public modalType = "Edit" || "Create" || "Detail";
  public mappingDataGridViewModel: DataGridExtendedViewModel;
  public targetDefinitionId: string;
  public sourceDefinitionId: string;
  public mappingDefinitionId: string;
  public organizationId: string;
  public mappingFileData: any;
  public sourceTargetMappingData: any;
  
  public progressValue: number = 100;

  public constructor(
    public mappingDefinitionService: MappingDefinitionService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public authService: AuthService,
  ) { }

  public init(params?) {
    this.modalType = params.action;
    this.targetDefinitionId = params.targetDefinitionId;
    this.sourceDefinitionId = params.sourceDefinitionId;
    this.mappingDefinitionId = params.mappingDefinitionId;
    this.organizationId = this.authService.getOrganizationId();
    this.mappingDataGridViewModel = new DataGridExtendedViewModel();
    this.initializeGridColDef();
    this.subscribers();
    this.getMappingDefinitionColumnList(params);
    return observableFrom([]);
  }

  public initializeGridColDef(): void {
    const sourceColumnName = {
      headerName: 'Source Columns',
      field: 'sourceColumnName',
      sortable: true,
      editable: false,
      sort: 'asc',
      cellRenderer: (params: any) => {
        if (params) {
          return `<div>`+this.getMergedSourceColumns(params)+`</div>`;
        }
      }
    };
    this.mappingDataGridViewModel.columnDefs.push(sourceColumnName);

    const splitOrMergeColumn = {
      headerName: 'Split/Merge',
      editable: false,
      field: 'splitOrMerge',
    };
    this.mappingDataGridViewModel.columnDefs.push(splitOrMergeColumn);

    const dataMappingColumn = {
      headerName: 'Data Mapping',
      editable: false,
      field: 'mappingDefinition',
    };
    this.mappingDataGridViewModel.columnDefs.push(dataMappingColumn);

    const transformationColumn = {
      headerName: 'Transformation',
      editable: false,
      field: 'transformation',
    };
    this.mappingDataGridViewModel.columnDefs.push(transformationColumn);

    const targetColumns = {
      headerName: 'Target Columns',
      field: 'targetColumnId',
      cellRenderer: (params: any) => {
        if (params) {
          return `<div>`+this.getSplitedTargetColumns(params)+`</div>`;
        }
      }
     
    };
    this.mappingDataGridViewModel.columnDefs.push(targetColumns);

    const format = {
      headerName: 'Format',
      field: 'format',
      sortable: true,
      editable: false
    };
    this.mappingDataGridViewModel.columnDefs.push(format);
    const acceptedValueColumn = {
      headerName: 'Accepted Values',
      field: 'acceptedValue',
      sortable: true,
      editable: false
    };
    this.mappingDataGridViewModel.columnDefs.push(acceptedValueColumn);

    const defaultValue = {
      headerName: 'Default Value',
      field: 'defaultValue',
      sortable: true,
      editable: false
    };

    this.mappingDataGridViewModel.columnDefs.push(defaultValue);

    this.mappingDataGridViewModel.defaultSortModel = [
      {
        colId: 'sourceColumnName',
        sort: 'asc',
      }
    ];
  }
  private getSplitedTargetColumns(data: any): any {
    let splitedData = [];
    if (data.data.splitOrMergeData && data.data.splitOrMergeData.length > 0 && data.data.splitOrMerge == Constant.split) {
      data.data.splitOrMergeData.forEach(element => {
        splitedData.push('<label class="data-grid-padding-right">' + element.targetColumnName + '</label>');
      });
      data.node.selectable = false;
      return splitedData;
    }
    else {
        splitedData.push('<label class="data-grid-padding-right">' + data.data.targetColumn.columnName + '</label>');
        return splitedData;
    }
  }

  private getMergedSourceColumns(data: any): any {
    let splitedData = [];
    if (data.data.splitOrMergeData && data.data.splitOrMergeData.length > 0 && data.data.splitOrMerge == Constant.merge) {
      data.data.splitOrMergeData.forEach(element => {
        splitedData.push('<label class="data-grid-padding-right">' + element.sourceColumnName + '</label>');
      });
      data.node.selectable = false;
      return splitedData;
    }
    else {
        splitedData.push('<label class="data-grid-padding-right">' + data.data.sourceColumnName + '</label>');
        return splitedData;
    }
  }
  public subscribers(): void {
    this.mappingDataGridViewModel.events.subscribe(e => {
      switch (e.eventType) {
      }
    });
  }


  public getMappingDefinitionColumnList(params: any): void {
    this.mappingDefinitionService.getSourceTargetMappingDetails(params.sourceDefinitionId).subscribe(data => {
      if (data) {
        this.mappingFileData = data;
        this.initGridSupplierMapping();
      }
    }, error => {
      if (error?.error?.errorList && error.error.errorList.length > 0) {
        this.alertService.error(error.error.errorList[0], false);
      } else if (error.error) {
        this.alertService.error(error.error, false);
      }
      else {
        this.alertService.error(error, false);
      }
    });
  }
  public initGridSupplierMapping(): void {
    const mappingData = [];
    let splitOrMergeData = [];
    let exsitingSplitedData = [];
    let exsitingMergedData = [];

    let data = _.sortBy(this.mappingFileData, (o)=> { return o.sequence; });
    data = data.reverse();
    data.forEach((item) => {
      splitOrMergeData = [];
      if (exsitingSplitedData.some((e) => e == item.sourceColumnId))
        return;
      if (exsitingMergedData.some((e) => e == item.targetColumnId))
        return;

      if (item.splitOrMerge === Constant.split || item.splitOrMerge === Constant.merge) {
        if (item.splitOrMerge === Constant.split) {
          exsitingSplitedData.push(item.sourceColumnId);
        }
        if (item.splitOrMerge === Constant.merge) {
          exsitingMergedData.push(item.targetColumnId);
        }
        splitOrMergeData = this.formatSplitOrMergeDataForGridView(item);
      }

      const obj: any = {};
      obj.rowData = item;
      obj.sourceColumnId = item.sourceColumnId;
      obj.sourceColumnName = item.sourceColumn.columnName;
      obj.targetColumnId = item.targetColumnId ? item.targetColumnId : "";
      obj.format = item.targetColumn ? item.targetColumn.format : "";
      obj.acceptedValue = item.targetColumn ? item.targetColumn.refDataLookupName : "";
      obj.sequenceNumber = item.sequence;
      obj.defaultValue = item.targetColumn ? item.targetColumn.defaultValue : "";
      obj.isMandatory = item.targetColumn ? item.targetColumn.isMandatory : false;
      obj.targetColumn = item.targetColumn ? item.targetColumn : null;
      obj.splitOrMerge = item.splitOrMerge;
      obj.splitOrMergeData = splitOrMergeData;
      mappingData.push(obj);
    });
    this.sourceTargetMappingData = mappingData;
    this.mappingDataGridViewModel.setRowsData(mappingData);
  }


  private formatSplitOrMergeDataForGridView(data: any): any {
    let splitOrMergeData = [];
    if (data.splitOrMerge === Constant.split) {
      const splitData = this.mappingFileData.filter(item => item.sourceColumnId == data.sourceColumnId && item.splitOrMerge === Constant.split);
      splitData.forEach(element => {
        const obj: any = {};
        if (element.targetColumn) {
          obj.targetColumnId = element.targetColumn.id;
          obj.targetColumnName = element.targetColumn.columnName;
          splitOrMergeData.push(obj);
        }

      });

    }
    if (data.splitOrMerge === Constant.merge) {
      const MergeData = this.mappingFileData.filter(item => item.targetColumnId === data.targetColumnId && item.splitOrMerge === Constant.merge);
      MergeData.forEach(element => {
        const obj: any = {};
        if (element.sourceColumn) {
          obj.sourceColumnId = element.sourceColumn.id;
          obj.sourceColumnName = element.sourceColumn.columnName;
          splitOrMergeData.push(obj);
        }
      });

    }
    return splitOrMergeData;

  }

  public submitSourceTargetMapping(): any {
    const request = {
      "id": this.mappingDefinitionId,
      "refFlowStatusId": this.refFlowStatusId,
      "refMapDefStatusId": this.refMapDefStatusId,
      "sourceTargetColumnMapping": [],
      "requireEmailApproval" :true
    };
    this.mappingDefinitionService.saveMappingDefinition(request).subscribe(tdData => {
      if (tdData?.id && tdData.isSuccess) {
        this.router.navigate([`mapping-definition-list/`]);
        this.alertService.success(ValidationMessages.SourceTargetSaveSuccess);
      }
    }, error => {
      this.alertService.error(error, false);
    });
   

  }

  public redirectToDefineData(): any {
    return this.router.navigate([`define-data/${this.modalType}/${this.targetDefinitionId}/${this.sourceDefinitionId}/${this.mappingDefinitionId}`]);
  }
}