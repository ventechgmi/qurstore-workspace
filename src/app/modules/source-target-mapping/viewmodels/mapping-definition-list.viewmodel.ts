import { from as observableFrom, Observable, Subject, forkJoin } from "rxjs";
import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from 'src/app/utils/alert/alert.service';
import { MappingDefinitionService } from '../services/mapping-definition.service';
import { MappingDefinitionViewModel } from './mapping-definition.viewmodel';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '../../../utils/auth.service';
import moment from "moment";

@Injectable()
export class MappingDefinitionListViewModel extends MappingDefinitionViewModel {
  public datasetRequestId: any;
  public alertService: AlertService;
  public viewModelDataGrid: DataGridViewModel;
  public mappingDefinitionList: any;
  public modalType = "Edit" || "Detail";
  public header = [
    { headerName: "Target Name", field: "targetName", isClickable: true },
    { headerName: "Mapping Id", field: "mappingDefinitionId", isClickable: false, isHide: true },
    { headerName: "Mapped Definition", field: "mappedName" },
    { headerName: "Updated On", field: "formattedDate" },
    { headerName: "Status", field: "status" },
    { headerName: "Active", field: "canToggle", isClickable: false, isHide: true },
  ];
  public mappingDefinitionId: string;
  public isEnable: boolean;
  public appliedFilters: any[];
  public vendorWithoutUserList: any;
  userRolePermission: any;

  public constructor(
    public mappingDefinitionService: MappingDefinitionService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    private fb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public authService: AuthService,
  ) {
    super(mappingDefinitionService, ViewModelType.Edit, ngxSmartModalService, route,authService);
  }

  public init(param?) {
    this.viewModelDataGrid = new DataGridViewModel();
    this.viewModelDataGrid.setHeader(this.header, false, { canUpdate: true });
    this.viewModelDataGrid.clickableColumnIndices = [1];
    this.userRolePermission = this.getUserClaims() || {};
    this.getMappingDefinitionList();
    this.subscribers();
    return observableFrom([]);
  }
  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  public subscribers(): void {
    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.mappingClicked:
          this.getClickedRowData(e.eventData);
          break;
        case GridConfig.editClicked:
          this.getClickedRowData(e.eventData); // TODO Remove it After Implementing Edit Functionality
          break;
        case GridConfig.deleteClicked:
          this.mappingDefinitionId = e.eventData.rowDetails[4];
          this.ngxSmartModalService.getModal("deleteConfirmModalFromList").open();
          break;
        case GridConfig.toggleClicked:
          this.mappingDefinitionId = e.eventData.rowDetails[4];
          this.isEnable = e.eventData.rowDetails[12];
          this.enableDisableMappingDefinition();
          break;
        default:
          break;
      }
    }, error => {
      this.alertService.error(error);
    });
  }

  public getMappingDefinitionList(): void {
    this.mappingDefinitionService.getMappingDefinitionList().subscribe(
      data => {
        if (data) {
          this.mappingDefinitionList = data;
          this.mappingDefinitionList.map(element => {
            element.formattedDate = (element.mappedName != "" && element.updatedOn) ? moment(element.updatedOn).format(config.FULL_DATE_FORMAT) : "";
          })
          this.viewModelDataGrid.setRowsData(this.mappingDefinitionList, false);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public getClickedRowData(data: any): void {
    const targetDefinitionId = data.rowDetails.id;
    const sourceDefinitionId = data.rowDetails.sourceDefinitionId;
    const mappingDefinitionId = data.rowDetails.mappingDefinitionId;
    const uploadedFileId = data.rowDetails.uploadedFileId;
    this.router.navigate(["/source-target-mapping/upload/Create/" + targetDefinitionId])
    .then().catch();
    // http://localhost:4200/#/define-data/Create/79651b0e-abc0-4f0a-b81b-655a680d35c9/7f4952ff-ffa9-42ae-a7cc-ba327fd0181e/d515b6b2-98dc-4b82-b0ad-ac0dc6631b4f
    // this.router.navigate(["/define-data/Create/"+ targetDefinitionId +"/"+sourceDefinitionId+"/"+mappingDefinitionId+"/"+uploadedFileId])
    //   .then().catch();
  }

  public async deleteMappingDefinition(): Promise<void> {
    await this.mappingDefinitionService.deleteMappingDefinition(this.mappingDefinitionId).subscribe(data => {
      if (data.isSuccess) {
        this.alertService.success(data.message, true);
        this.init();
      } else {
        this.alertService.error(data.message, true);
      }
    }, (requestFailed) => {
      (requestFailed.error && requestFailed.error.validationErrors) ?
        this.alertService.error(requestFailed.error.validationErrors.message, true) :
        this.alertService.error(requestFailed.error.message, true);
      this.ngxSmartModalService.getModal("deleteConfirmModalFromList").close();
    }, () => {
      this.ngxSmartModalService.getModal("deleteConfirmModalFromList").close();
    });
  }

  public async enableDisableMappingDefinition(): Promise<void>  {
    await this.mappingDefinitionService.enableDisableMappingDefinition(this.mappingDefinitionId, !this.isEnable).subscribe(data => {
      if (data.isSuccess) {
        this.alertService.success(data.message, true);
        this.init();
      } else {
        this.alertService.error(data.message, true);
      }
    }, (requestFailed) => {
      (requestFailed.error && requestFailed.error.validationErrors) ?
        this.alertService.error(requestFailed.error.validationErrors.message, true) :
        this.alertService.error(requestFailed.error.message, true);
    });
  }
}
