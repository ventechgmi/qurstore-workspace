import { from as observableFrom, Observable, Subject, forkJoin, Subscription } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { Injectable, OnInit, ChangeDetectorRef, ElementRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel, DataGridExtendedViewModel } from "../../core-ui-components";
import config, { GridConfig } from "../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ValidationMessages } from '../helpers/validation-messages';
import { Component, ViewChild } from '@angular/core';
import * as HighCharts from 'highcharts';
import { LocalStorageManager } from 'src/app/utils/local-storage-manager';
import { LocalStorageKeyCollection, DelimiterMapping } from 'src/app/utils/config';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from 'src/app/utils/auth.service';
import { MappingDefinitionViewModel } from './mapping-definition.viewmodel';
import { MappingDefinitionService } from '../services/mapping-definition.service';
import { MappingDefinitionUploadFileViewModel } from "./mapping-definition-upload-file.viewmodel";
import { ISplitMergeData } from "../helpers/interface";
import { Constant, MappingDefinitionDefineDataViewModelConfig } from '../helpers/config';

@Injectable()
export class MappingDefinitionDefineDataViewModel extends MappingDefinitionViewModel {
  public refFlowStatusId = MappingDefinitionDefineDataViewModelConfig.refFlowStatusId;
  public refMapDefStatusId = MappingDefinitionDefineDataViewModelConfig.refMapDefStatusId;
  public userId: string;
  public organizationId: string;
  public alertService: AlertService;
  public mappingFileData: any;
  public targetColumnList: any;
  public sourceColumnList: any;
  public targetColumnOptionMapList: any;
  public mappingDataGridViewModel: DataGridExtendedViewModel;
  public currentRowData = { id: "", columnName: "", columnId: "" };
  public sourceDefinitionId: string;
  public targetDefinitionId: string;
  public mappingDefinitionId: string;
  public sourceTargetMappingData: any;
  public delimiterForSplit: string = MappingDefinitionDefineDataViewModelConfig.delimiterForSplit;
  public delimiterForMerge: string = MappingDefinitionDefineDataViewModelConfig.delimiterForMerge;
  public splitArrayData: ISplitMergeData[] = [];
  public margeArrayData: ISplitMergeData[] = [];
  public delimiterMapping = DelimiterMapping;
  public selectedTargetColumnForMerge = { id: "0", columnName: "" };
  public currentNoOfRowsWhileMerging = 2;
  public selectedRowData: any = {};
  public targetColumnExistingSelectionId: string = "";
  public modalType = "Edit" || "Create" || "Detail";
  public initParams: any;
  public barOptions = {
    barType: 'linear',
    color: "#00cc00",
    secondColor: "#D3D3D3",
    progress: 33,
    textAlignment: "center",
    linear: {
      depth: 22,
      stripped: true,
      active: true,
      label: {
        enable: true,
        value: "",
        color: "#fff",
        fontSize: 15,
        showPercentage: true,
      }
    },
    radial: {
      depth: 3,
      size: 9,
      label: {
        enable: true,
        color: "#09608c",
      }
    }
  }
  dataMaapingSourceColumn: any;
  dataMaapingTargetColumn: any;
  dataMaapingAcceptedValues: any;
  referenceValuesList: any;
  public split: string = "Split";
  public merge: string = "Merge";
  public duplicateTargetConfirm: string = "duplicateTargetConfirm";
  public progressValue: number = 66;
  public fileContent: any;
  public subscription = new Subscription();
  public sourceColumnData: any;
  public uploadedFileId: any;
  currentSplitingRow: any;
  currentMergingRow: any;
  public constructor(
    public mappingDefinitionService: MappingDefinitionService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public authService: AuthService,
    private mappingColumnsLocalStorageManager: LocalStorageManager
  ) {
    super(mappingDefinitionService, ViewModelType.ReadOnly, ngxSmartModalService, route, authService);
  }

  public init(params?) {
    this.initParams = params;
    this.modalType = params.action;
    this.targetDefinitionId = params.targetDefinitionId;
    this.sourceDefinitionId = params.sourceDefinitionId;
    this.mappingDefinitionId = params.mappingDefinitionId;
    this.uploadedFileId = params.uploadedFileId;
    this.organizationId = this.authService.getOrganizationId();
    this.mappingDataGridViewModel = new DataGridExtendedViewModel();
    this.getColumnList(params);
    this.getSourceColumnList();
    this.subscribers();
    return observableFrom([]);
  }

  public initServices(alertService: AlertService): void {
    this.alertService = alertService;
  }

  public initializeGridColDef(): void {
    this.selectedRowData = {};
    this.targetColumnExistingSelectionId = "";
    const sourceColumnName = {
      headerName: 'Source Columns',
      field: 'sourceColumnName',
      sortable: true,
      editable: false,
      sort: 'asc',
      cellRenderer: (params: any) => {
        if (params) {
          return `<div>` + this.getMergedSourceColumns(params) + `</div>`;
        }
      }
    };
    this.mappingDataGridViewModel.columnDefs.push(sourceColumnName);

    const splitOrMergeColumn = {
      headerName: 'Split/Merge',
      editable: false,
      field: 'iconDetails',
      cellRenderer: (params: any) => {
        if (params) {
          return this.getSplitMergeIcons();
        }
      }
    };
    this.mappingDataGridViewModel.columnDefs.push(splitOrMergeColumn);

    const dataMappingColumn = {
      headerName: 'Data Mapping',
      editable: false,
      field: 'mappingDefinition',
      cellRenderer: (params: any) => {
        if (params) {
          return this.getMappingDefinitionIcons(params);
        }
      }
    };
    this.mappingDataGridViewModel.columnDefs.push(dataMappingColumn);

    const transformationColumn = {
      headerName: 'Transformation',
      editable: false,
      field: 'transformation',
      cellRenderer: (params: any) => {
        if (params) {
          return this.getTransformationData(params);
        }
      }
    };
    this.mappingDataGridViewModel.columnDefs.push(transformationColumn);

    const targetColumns = {
      headerName: 'Target Columns',
      field: 'targetColumnId',
      cellEditor: 'agSelectCellEditor',
      cellEditorParams: {
        values: this.extractValues(this.targetColumnOptionMapList)
      },
      refData: this.targetColumnOptionMapList
      // cellRenderer: (params: any) => {
      //   if (params) {
      //     return `<div>` + this.getSplitedTargetColumns(params) + `</div>`;
      //   }
      // }

    };
    this.mappingDataGridViewModel.columnDefs.push(targetColumns);

    const format = {
      headerName: 'Format',
      field: 'format',
      sortable: true,
      editable: false
    };
    this.mappingDataGridViewModel.columnDefs.push(format);
    const acceptedValueColumn = {
      headerName: 'Accepted Values',
      field: 'acceptedValue',
      sortable: true,
      editable: false,
      cellRenderer: (params: any) => {
        if (params) {
          return ( params.value)? `<u class="hyperlink" data-action-type="columnClicked">` + params.value + `</u>`:null;
        }
      }
    };
    this.mappingDataGridViewModel.columnDefs.push(acceptedValueColumn);

    const defaultValue = {
      headerName: 'Default Value',
      field: 'defaultValue',
      sortable: true,
      editable: false
    };

    this.mappingDataGridViewModel.columnDefs.push(defaultValue);

    this.mappingDataGridViewModel.defaultSortModel = [
      {
        colId: 'sourceColumnName',
        sort: 'asc',
      }
    ];
  }

  public extractValues(mappings: any): any {
    if (mappings) {
      return Object.keys(mappings);
    }
  }
  public async getSourceColumnList(): Promise<any> {
    await this.mappingDefinitionService.getSourceDefinitionColumnList(this.sourceDefinitionId).subscribe(data => {
      this.sourceColumnList = data;
    });
  }

  public async getColumnList(params: any): Promise<any> {
    await this.mappingDefinitionService.getTargetDefinitionColumnList(params.targetDefinitionId).subscribe(data => {
      if (data) {
        this.targetColumnList = data;
        let dict: any = [];
        dict[0] = "--Select--";
        this.targetColumnList.forEach(element => {
          dict[element.id] = element.columnName;
        });
        this.targetColumnOptionMapList = dict;
        this.initializeGridColDef();
        this.getMappingDefinitionColumnList(params);
      } else {
        this.targetColumnList = [];
      }
    }, error => {
      if (error?.error?.errorList && error.error.errorList.length > 0) {
        this.alertService.error(error.error.errorList[0], false);
        this.targetColumnList = [];
      } else if (error.error) {
        this.alertService.error(error.error, false);
        this.targetColumnList = [];
      }
      else {
        this.alertService.error("ValidationMessages.ErrorOccurred", false);
        this.targetColumnList = [];
      }
    });
  }

  public getMappingDefinitionColumnList(params: any): void {
    this.mappingDefinitionService.getSourceTargetMappingDetails(params.sourceDefinitionId).subscribe(data => {
      if (data) {
        this.mappingFileData = data;
        this.initGridSupplierMapping();
      }
    }, error => {
      if (error?.error?.errorList && error.error.errorList.length > 0) {
        this.alertService.error(error.error.errorList[0], false);
        this.clearMappingFileData();
      } else if (error.error) {
        this.alertService.error(error.error, false);
        this.clearMappingFileData();
      }
      else {
        this.alertService.error(error, false);
        this.clearMappingFileData();
      }
    });
  }

  public initGridSupplierMapping(): void {
    const mappingData = [];
    let splitOrMergeData = [];
    let exsitingSplitedData = [];
    let exsitingMergedData = [];

    let data = _.sortBy(this.mappingFileData, (o) => { return o.sequence; });
    data = data.reverse();
    data.forEach((item) => {
      splitOrMergeData = [];
      if (!item.targetColumnId) {
        this.getTargetColumnDropDownData(item);
      }
      if (exsitingSplitedData.some((e) => e == item.sourceColumnId))
        return;
      if (exsitingMergedData.some((e) => e == item.targetColumnId))
        return;

      if (item.splitOrMerge === this.split || item.splitOrMerge === this.merge) {
        if (item.splitOrMerge === this.split) {
          exsitingSplitedData.push(item.sourceColumnId);
        }
        if (item.splitOrMerge === this.merge) {
          exsitingMergedData.push(item.targetColumnId);
        }
        splitOrMergeData = this.formatSplitOrMergeDataForGridView(item);
      }

      const obj: any = {};
      obj.rowData = item;
      obj.sourceColumnId = item.sourceColumnId;
      obj.sourceColumnName = item.sourceColumn.columnName;
      obj.targetColumnId = 0;
      obj.format = item.targetColumn ? item.targetColumn.format : "";
      obj.acceptedValue = item.targetColumn ? item.targetColumn.refDataLookupName : "";
      obj.sequenceNumber = item.sequence;
      obj.defaultValue = item.targetColumn ? item.targetColumn.defaultValue : "";
      obj.isMandatory = item.targetColumn ? item.targetColumn.isMandatory : false;
      obj.targetColumn = item.targetColumn ? item.targetColumn : null;
      obj.splitOrMerge = item.splitOrMerge;
      obj.splitOrMergeData = splitOrMergeData;
      mappingData.push(obj);
    });
    this.sourceTargetMappingData = mappingData;
    this.mappingDataGridViewModel.setRowsData(mappingData);
  }

  private formatSplitOrMergeDataForGridView(data: any): any {
    let splitOrMergeData = [];
    switch (data.splitOrMerge) {
      case (this.split):
        const splitData = this.mappingFileData.filter(item => item.sourceColumnId == data.sourceColumnId && item.splitOrMerge === this.split);
        splitData.forEach(element => {
          const obj: any = {};
          if (element.targetColumn) {
            obj.targetColumnId = element.targetColumn.id;
            obj.targetColumnName = element.targetColumn.columnName;
            splitOrMergeData.push(obj);
          }

        });
        break;
      case (data.splitOrMerge === this.merge):
        const mergeData = this.mappingFileData.filter(item => item.targetColumnId === data.targetColumnId && item.splitOrMerge === this.merge);
        mergeData.forEach(element => {
          const obj: any = {};
          if (element.sourceColumn) {
            obj.sourceColumnId = element.sourceColumn.id;
            obj.sourceColumnName = element.sourceColumn.columnName;
            splitOrMergeData.push(obj);
          }
        });
        break;
      default:
        break;
    }
    return splitOrMergeData;
  }

  public subscribers(): void {
    this.mappingDataGridViewModel.events.subscribe(e => {
      if (e.eventData && e.eventData.rowDetails.rowData) {
        this.currentRowData.id = e.eventData.rowDetails.rowData.id;
        this.currentRowData.columnId = e.eventData.rowDetails.sourceColumnId;
        this.currentRowData.columnName = e.eventData.rowDetails.sourceColumnName;
      }
      switch (e.eventType) {
        case GridConfig.splitClicked:
          this.openSplitColumnPopUp(e.eventData.rowDetails);
          break;
        case GridConfig.mergeClicked:
          this.openMergeColumnPopUp(e.eventData.rowDetails);
          break;
        case GridConfig.mappingClicked:
          this.openDataMappingColumn(e.eventData.rowDetails);
          break;
        case GridConfig.dropDownChanged:
          this.checkIsDuplicateSelection(e.eventData.rowDetails);
          break;
        case GridConfig.columnClicked:
          this.openAcceptedValuesPopup(e.eventData.rowDetails);
          break;
        default:
          break;
      }
    }, error => {
      this.alertService.error(error, false);
    });

  }

  public getFileContent(): void {
    this.subscription.add(
      this.mappingDefinitionService.fileContent.subscribe(data => {
        this.fileContent = data;
      })
    )
  }
  private openSplitColumnPopUp(data: any): void {
    this.currentSplitingRow = data;
    this.ngxSmartModalService.getModal("splitColumnPopUp").open();
  }

  public closeSplitColumnPopUp(): void {
    this.splitArrayData = [];
    this.ngxSmartModalService.getModal("splitColumnPopUp").close();
  }

  private openMergeColumnPopUp(data: any): void {
    this.currentMergingRow = data;
    this.ngxSmartModalService.getModal("mergeColumnPopUp").open();
  }

  public closeMergeColumnPopUp(): void {
    this.margeArrayData = [];
    this.selectedTargetColumnForMerge = { id: "0", columnName: "" };
    this.ngxSmartModalService.getModal("mergeColumnPopUp").close();
  }
  public openAcceptedValuesPopup(data): void {
    this.dataMaapingSourceColumn = data.sourceColumnName;
    this.dataMaapingTargetColumn = data.targetColumn.columnName;
    this.dataMaapingAcceptedValues = data.acceptedValue;
    this.referenceValuesList = data.targetColumn.refDataLookup.referenceValues;
    this.ngxSmartModalService.getModal("dataAcceptedValues").open();
  }
  public closeDataAcceptedValuePopup():void {
   
    this.ngxSmartModalService.getModal("dataAcceptedValues").close();
    this.dataMaapingSourceColumn = null;
    this.dataMaapingTargetColumn = null;
    this.dataMaapingAcceptedValues = null;
    this.referenceValuesList = null;
  }
  public onNumberOfSplitsSelected(value: any): void {
    this.splitArrayData = [];
    const numericValue = parseInt(value);
    if (numericValue != 0) {
      for (let i = 0; i < numericValue; i++) {
        const tempObj: ISplitMergeData = {
          sequence: i + 1,
          sourceColumnName: this.currentRowData.columnName,
          sourceColumnId: this.currentRowData.columnId,
          targetColumnId: "",
          targetColumnName: ""
        }
        this.splitArrayData.push(tempObj);
      }
    }
  }

  public onTargetOptionsSelected(data: string, index: number): void {
    const targetColumnObj = this.targetColumnList.find(e => e.id === data);
    this.splitArrayData[index].targetColumnName = targetColumnObj.columnName;
    this.splitArrayData[index].targetColumnId = data;
  }

  public onTargetSelectedInMerge(value: any): void {
    this.margeArrayData = [];
    if (value !== "0") {
      this.selectedTargetColumnForMerge = this.targetColumnList.find(e => e.id === value);
      for (let i = 0; i < this.currentNoOfRowsWhileMerging; i++) {
        const obj: ISplitMergeData = {
          sequence: i + 1,
          targetColumnId: this.selectedTargetColumnForMerge.id,
          targetColumnName: this.selectedTargetColumnForMerge.columnName,
          sourceColumnId: ""
        }
        this.margeArrayData.push(obj);
      }
    } else {
      this.margeArrayData = [];
    }
  }

  public onSourceColumnSelectedWhileMerge(data: string, index: number): void {
    const sourceDataObj = this.mappingFileData.find(e => e.sourceColumnId === data);
    this.margeArrayData[index].sourceColumnName = sourceDataObj.sourceColumn.columnName;
    this.margeArrayData[index].sourceColumnId = data;
  }

  public onSourceColumnSelectedWhileDataMapping(data: string, index: number): void {
    const sourceDataObj = this.mappingFileData.find(e => e.sourceColumnId === data);
    this.margeArrayData[index].sourceColumnName = sourceDataObj.sourceColumn.columnName;
    this.margeArrayData[index].sourceColumnId = data;
  }
  public addColumnWhileMerge(): void {
    const arrLength = this.margeArrayData.length;
    const obj: ISplitMergeData = {
      sequence: arrLength + 1,
      targetColumnId: this.selectedTargetColumnForMerge.id,
      targetColumnName: this.selectedTargetColumnForMerge.columnName,
      sourceColumnId: "",
      sourceColumnName: ""
    }
    this.margeArrayData.push(obj);
  }

  public removeColumnWhileMerge(): void {
    const arrLength = this.margeArrayData.length;
    if (arrLength > 2) {
      this.margeArrayData.pop();
    }
  }
  
  public saveSplitMergeMapping(dataToSave: any, splitOrMerge: string): void {
    const formattedData = this.formatSplitMergeData(dataToSave, splitOrMerge);
    this.mappingDefinitionService.saveMappingDefinition(formattedData).subscribe(tdData => {
      if (tdData?.id && tdData.isSuccess) {
        if (splitOrMerge === this.split) {
          this.closeSplitColumnPopUp();
        }
        if (splitOrMerge === this.merge) {
          this.closeMergeColumnPopUp();
        }
        // this.getMappingDefinitionColumnList(this.initParams);
        this.alertService.success(ValidationMessages.SplitSaveSuccess);
      }
    }, error => {
      this.alertService.error(error, false);
    });
  }
  public saveDataMapping(dataToSave: any): void {
    const formattedData = this.formatSplitMergeData(dataToSave);
    this.mappingDefinitionService.saveMappingDefinition(formattedData).subscribe(tdData => {
      if (tdData?.id && tdData.isSuccess) {
      
        this.alertService.success(ValidationMessages.SplitSaveSuccess);
      }
    }, error => {
      this.alertService.error(error, false);
    });
  }
  public swapTargetColumns(): void {
    if (this.selectedRowData && this.targetColumnExistingSelectionId) {
      const currentSourceColumn = _.filter(this.sourceTargetMappingData, sourceTargetColumn => sourceTargetColumn.sourceColumnId == this.selectedRowData.sourceColumnId);
      const toSwapSourceColumn = _.filter(this.sourceTargetMappingData, sourceTargetColumn => sourceTargetColumn.rowData.targetColumnId == this.targetColumnExistingSelectionId);
      const toSwapTargetColumnId = currentSourceColumn[0].rowData.targetColumnId;
      if (currentSourceColumn && currentSourceColumn.length) {
        const filteredTargetItem = _.filter(this.targetColumnList, targetColumn => targetColumn.id == this.targetColumnExistingSelectionId);
        _.each(this.sourceTargetMappingData, sourceTargetRow => {
          if (currentSourceColumn[0].sourceColumnId === sourceTargetRow.sourceColumnId && filteredTargetItem && filteredTargetItem.length) {
            sourceTargetRow.targetColumnId = filteredTargetItem[0].id;
            sourceTargetRow.targetColumn = filteredTargetItem[0];
            sourceTargetRow.format = filteredTargetItem[0].format;
            sourceTargetRow.acceptedValue = filteredTargetItem[0].refDataLookupName;
            sourceTargetRow.defaultValue = filteredTargetItem[0].defaultValue;
            sourceTargetRow.isMandatory = filteredTargetItem[0].isMandatory;
            sourceTargetRow.rowData.targetColumnId = sourceTargetRow.targetColumnId;
            sourceTargetRow.rowData.targetColumn = sourceTargetRow.targetColumn;
            return false;
          }
        });
      }

      if (toSwapSourceColumn && toSwapSourceColumn.length && currentSourceColumn && currentSourceColumn.length) {
        const filteredTargetItem = _.filter(this.targetColumnList, targetColumn => targetColumn.id == toSwapTargetColumnId);
        _.each(this.sourceTargetMappingData, sourceTargetRow => {
          if (toSwapSourceColumn[0].sourceColumnId === sourceTargetRow.sourceColumnId && filteredTargetItem && filteredTargetItem.length) {
            sourceTargetRow.targetColumnId = filteredTargetItem[0].id;
            sourceTargetRow.targetColumn = filteredTargetItem[0];
            sourceTargetRow.format = filteredTargetItem[0].format;
            sourceTargetRow.acceptedValue = filteredTargetItem[0].refDataLookupName;
            sourceTargetRow.defaultValue = filteredTargetItem[0].defaultValue;
            sourceTargetRow.isMandatory = filteredTargetItem[0].isMandatory;
            sourceTargetRow.rowData.targetColumnId = sourceTargetRow.targetColumnId;
            sourceTargetRow.rowData.targetColumn = sourceTargetRow.targetColumn;
            return false;
          }
        });
      }

      this.mappingDataGridViewModel.setRowsData(this.sourceTargetMappingData);
      this.selectedRowData = {};
      this.targetColumnExistingSelectionId = "";
      this.ngxSmartModalService.getModal(this.duplicateTargetConfirm).close();
    }
  }

  public keepPreviousSelectedTargetColumn(): void {
    if (this.selectedRowData && this.selectedRowData.rowData.targetColumnId) {
      _.each(this.sourceTargetMappingData, sourceTargetRow => {
        if (sourceTargetRow.sourceColumnId === this.selectedRowData.sourceColumnId) {
          sourceTargetRow.targetColumnId = this.selectedRowData.targetColumn.id;
          sourceTargetRow.targetColumn = this.selectedRowData.targetColumn;
          sourceTargetRow.format = this.selectedRowData.targetColumn ? this.selectedRowData.targetColumn.format : "";
          sourceTargetRow.acceptedValue = this.selectedRowData.targetColumn ? this.selectedRowData.targetColumn.refDataLookupName : "";
          sourceTargetRow.defaultValue = this.selectedRowData.targetColumn ? this.selectedRowData.targetColumn.defaultValue : "";
          sourceTargetRow.isMandatory = this.selectedRowData.targetColumn ? this.selectedRowData.targetColumn.isMandatory : false;
          sourceTargetRow.rowData.targetColumnId = sourceTargetRow.targetColumnId;
          sourceTargetRow.rowData.targetColumn = sourceTargetRow.targetColumn;
        }
      });
    }
    if (this.selectedRowData.rowData.targetColumnId == null) {
      this.selectedRowData.targetColumnId = null;
    }
    this.mappingDataGridViewModel.setRowsData(this.sourceTargetMappingData);
    this.selectedRowData = {};
    this.targetColumnExistingSelectionId = "";
    this.ngxSmartModalService.getModal(this.duplicateTargetConfirm).close();
  }

  public saveSourceTargetMappingMapping(): void {
    const formattedData = this.formatSourceTargetMappingData();
    var anyValidation = false;
    var sourceColumnNames = '';

    if (formattedData.sourceTargetColumnMapping.length > 0) {
      _.each(this.targetColumnList, columnRow => {
        var matchedColumn = _.find(formattedData.sourceTargetColumnMapping, function (o) {
          return o.targetColumnId == columnRow.id;
        });
        if (!matchedColumn && columnRow.isMandatory) {
          anyValidation = true;
          sourceColumnNames += columnRow.columnName + ', ';
        }
      });

      // _.each(formattedData.sourceTargetColumnMapping, sourceTargetRow => {
      //   //if(!sourceTargetRow.targetColumnId) {
      //     anyValidation = true;
      //     sourceColumnNames +=  sourceTargetRow.sourceColumnName + ', ';
      //     return;
      //   //}        
      // });
    }

    if (anyValidation) {
      sourceColumnNames = sourceColumnNames.replace(/,\s*$/, "");
      this.alertService.error(ValidationMessages.NoMappingAvailableFor + sourceColumnNames + ValidationMessages.PleaseFillUpThoseRequiredData);
      return;
    }

    this.mappingDefinitionService.saveMappingDefinition(formattedData).subscribe(tdData => {
      if (tdData?.id && tdData.isSuccess) {
        if (!this.sourceDefinitionId && tdData.mappingDefinitions) {
          this.sourceDefinitionId = tdData.mappingDefinitions.sourceDefinitionId;
        }
        if (!this.mappingDefinitionId && tdData.mappingDefinitions) {
          this.mappingDefinitionId = tdData.mappingDefinitions.id;
        }
        this.redirectToPreviewData(this.modalType, this.targetDefinitionId, this.sourceDefinitionId, this.mappingDefinitionId);
        this.alertService.success(ValidationMessages.SourceTargetSaveSuccess);
      }
    }, error => {
      this.alertService.error(error, false);
    });
  }

  public redirectToDefineData(action: string, targetDefinitionId: string, sourceDefinitionId: string, mappingDefinitionId: string): any {
    return this.router.navigate([`define-data/${action}/${targetDefinitionId}/${sourceDefinitionId}/${mappingDefinitionId}`]);
  }

  public redirectToPreviewData(action: string, targetDefinitionId: string, sourceDefinitionId: string, mappingDefinitionId: string): any {
    return this.router.navigate([`preview-data/${action}/${targetDefinitionId}/${sourceDefinitionId}/${mappingDefinitionId}`]);
  }
  public openDataMappingColumn(data): void {
    this.dataMaapingSourceColumn = data.sourceColumnName;
    this.dataMaapingTargetColumn = data.targetColumn.columnName;
    this.dataMaapingAcceptedValues = data.acceptedValue;
    this.referenceValuesList = data.targetColumn.refDataLookup.referenceValues;

    this.mappingDefinitionService.getFileContent(this.uploadedFileId, this.dataMaapingSourceColumn).subscribe(data => {
      if (data?.id && data.isSuccess) {
        this.sourceColumnData = data.columnList;
        this.ngxSmartModalService.getModal("dataMappingPopUp").open();
      } else {

      }
    });
  }
  public closeDataMappingPopUp(): void {
    this.dataMaapingSourceColumn = null;
    this.dataMaapingTargetColumn = null;
    this.dataMaapingAcceptedValues = null;
    this.ngxSmartModalService.getModal("dataMappingPopUp").close();
  }
  private formatSplitMergeData(unformattedData: any, splitOrMerge?: string): any {
    const response = {
      "id": this.mappingDefinitionId,
      "refFlowStatusId": this.refFlowStatusId,
      "refMapDefStatusId": this.refMapDefStatusId,
      "sourceTargetColumnMapping": []
    };
    unformattedData.forEach((element: ISplitMergeData) => {
      const obj: any = {
        "id": element.id ? element.id : undefined,
        "mappingDefinitionId": this.mappingDefinitionId,
        "sourceColumnId": element.sourceColumnId,
        "sourceColumnName": element.sourceColumnName,
        "targetColumnId": element.targetColumnId,
        "targetColumnName": element.targetColumnName,
        "refDelimiterId": this.delimiterForSplit,
        "splitOrMerge": splitOrMerge,
        "isDataMapping": false,
        "sequence": element.sequence,
        "defaultValue": ""
      };
      response.sourceTargetColumnMapping.push(obj);
    });
    return response;
  }

  private formatSourceTargetMappingData(): any {
    const response = {
      "id": this.mappingDefinitionId,
      "refFlowStatusId": this.refFlowStatusId,
      "refMapDefStatusId": this.refMapDefStatusId,
      "sourceTargetColumnMapping": []
    };
    this.mappingFileData.forEach(data => {
      const obj: any = {
        "id": data.id ? data.id : undefined,
        "mappingDefinitionId": this.mappingDefinitionId,
        "sourceColumnId": data.sourceColumnId,
        "targetColumnId": data.targetColumnId,
        "refDelimiterId": data.refDelimiterId,
        "splitOrMerge": data.splitOrMerge,
        "isDataMapping": data.isDataMapping,
        "sequence": data.sequence,
        "defaultValue": data.defaultValue,
        "sourceColumnName": data.sourceColumn.columnName
      };
      response.sourceTargetColumnMapping.push(obj);
    });
    return response;
  }

  private getSplitMergeIcons(): string {
    const splitIcon = `<i title="Split" class="icon material-icons cursor text-primary rotate-90-deg" data-action-type="splitClicked">call_split</i>`;
    const mergeIcon = ` <i title="Merge" class="icon material-icons cursor text-primary action-icon-delete rotate-90-deg" data-action-type="mergeClicked">call_merge</i>`;
    return splitIcon + mergeIcon;
  }

  private getMappingDefinitionIcons(data: any): string {
    if (data.data.acceptedValue) {
      const mappingIcon = `<img src="../../../assets/images/data.png" class="data-grid-image mapping-definition" alt="Mapping Definition" data-action-type="mappingClicked" title="Mapping Definition">`;
      return mappingIcon;
    }
    return null;
  }

  private getSplitedTargetColumns(data: any): any {
    let splitedData = [];
    if (data.data.splitOrMergeData && data.data.splitOrMergeData.length > 0 && data.data.splitOrMerge == "Split") {
      data.data.splitOrMergeData.forEach(element => {
        splitedData.push(Constant.labelClassDataGridPaddingRight + element.targetColumnName + Constant.labelClose);
      });
      data.node.selectable = false;
      return splitedData;
    }
    else {
      splitedData.push(Constant.labelClassDataGridPaddingRight + data.data.targetColumn.columnName + Constant.labelClose);
      return splitedData;
    }
  }

  private getMergedSourceColumns(data: any): any {
    let splitedData = [];
    if (data.data.splitOrMergeData && data.data.splitOrMergeData.length > 0 && data.data.splitOrMerge == this.merge) {
      data.data.splitOrMergeData.forEach(element => {
        splitedData.push(Constant.labelClassDataGridPaddingRight + element.sourceColumnName + Constant.labelClose);
      });
      data.node.selectable = false;
      return splitedData;
    }
    else {
      splitedData.push(Constant.labelClassDataGridPaddingRight + data.data.sourceColumnName + Constant.labelClose);
      return splitedData;
    }
  }

  private getTransformationData(data: any): string {
    if (data.transformation) {
      const transformationFormattedData = `<p>Delimiter: Space, Delimiter: Space</p>`;
      return transformationFormattedData;
    }
    return null;
  }

  private getTargetColumnDropDownData(data: any): void {
    if (!data.targetColumnId) {
      const filteredData = _.filter(this.targetColumnList, targetColumn => {
        if (data.sourceColumn && targetColumn.columnName.toLowerCase() === data.sourceColumn.columnName.toLowerCase()) {
          return targetColumn;
        }
      });
      if (filteredData && filteredData.length) {
        for (let i = 0; i < this.targetColumnList.length; i++) {
          if (this.targetColumnList[i].columnName.toLowerCase() === data.sourceColumn.columnName.toLowerCase()) {
            data.targetColumn = this.targetColumnList[i];
            data.targetColumnId = this.targetColumnList[i].id;
            break;
          }
        }
      }
    }
  }

  private clearMappingFileData(): void {
    this.mappingFileData = [];
  }

  private checkIsDuplicateSelection(data: any): void {
    this.selectedRowData = data ? data : {};
    let isDuplicateColumnSelected = false;
    this.targetColumnExistingSelectionId = "";
    if (data.rowData && data.rowData.targetColumnId !== data.targetColumnId) {
      for (let i = 0; i < this.mappingFileData.length; i++) {
        const mappingFileData = this.mappingFileData[i];
        if (mappingFileData.sourceColumnId !== data.rowData.sourceColumnId && mappingFileData.targetColumnId === data.targetColumnId) {
          this.targetColumnExistingSelectionId = mappingFileData.targetColumnId;
          this.ngxSmartModalService.getModal(this.duplicateTargetConfirm).open();
          isDuplicateColumnSelected = true;
          break;
        }
      }
    }

    if (!isDuplicateColumnSelected) {
      const filteredTargetItem = _.filter(this.targetColumnList, targetColumn => targetColumn.id == data.targetColumnId);
      if (filteredTargetItem && filteredTargetItem.length) {
        data.targetColumnId = filteredTargetItem[0].id;
        data.targetColumn = filteredTargetItem[0];
        data.format = filteredTargetItem[0].format;
        data.acceptedValue = filteredTargetItem[0].refDataLookupName;
        data.defaultValue = filteredTargetItem[0].defaultValue;
        data.isMandatory = filteredTargetItem[0].isMandatory;
        data.rowData.targetColumnId = data.targetColumnId;
        data.rowData.targetColumn = data.targetColumn;
        this.mappingDataGridViewModel.setRowsData(this.sourceTargetMappingData);
      }
    }
  }
}
