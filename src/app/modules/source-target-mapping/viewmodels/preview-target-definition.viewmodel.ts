import { from as observableFrom, Observable, Subject, forkJoin } from "rxjs";
import { AlertService } from "../../../utils/alert/alert.service";
import { Injectable, OnInit, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import * as _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { DataGridViewModel } from "../../core-ui-components";
import { NgxSmartModalService } from "ngx-smart-modal";
import { TargetDefinitionService } from "../../target-definition/services/target-definition.service";
import { LocalStorageManager } from 'src/app/utils/local-storage-manager';
import { LocalStorageKeyCollection, GridConfig } from 'src/app/utils/config';
import { Constant } from "../helpers/config";

@Injectable()
export class PreviewTargetDefinitionViewModel {
    public alertService: AlertService;
    public targetDefinitionId: string;
    public targetColumnsList: any;
    public targetDefinitionsListGrid: any;
    public isPreviewSelected: boolean;
    viewModelDataGrid: DataGridViewModel;
    targetDefinitionName: any;

    header = [
        { headerName: "Data Element Name", field: "columnName", isClickable: false },
        { headerName: "Type", field: "refColumnDataTypeName", isClickable: false },
        { headerName: "Required", field: "isMandatoryDisplay", isClickable: false },
        { headerName: "Max Length", field: "maxLength", isClickable: false },
        { headerName: "Format", field: "format", isClickable: false },
        { headerName: "Data Masking", field: "refDataMaskTypeName", isClickable: false },
        { headerName: "Reference Values", field: "refDataLookupName", isClickable: false, onMouseHover: true },
        { headerName: "Default Values", field: "defaultValue", isClickable: false }
    ];

    public constructor(
        public targetDefinitionService: TargetDefinitionService,
        private router: Router,
        private chRef: ChangeDetectorRef,
        public ngxSmartModalService: NgxSmartModalService,
        private targetColumnsLocalStorageManager: LocalStorageManager,
        public route: ActivatedRoute
    ) { }

    public init(params?) {
        this.getTargetDefinitionsList();
        this.viewModelDataGrid = new DataGridViewModel();
        this.viewModelDataGrid.setHeader(this.header, false);
        this.subscribers();
    }

    public initGrid(data) {
        const targetDefinitions = data ? data : [];
        targetDefinitions.map(targetDefinition => {
            targetDefinition.isEnabledDisplay = targetDefinition.isEnabled === true ? "Yes" : "No";
            targetDefinition.isMandatoryDisplay = targetDefinition.isMandatory === true ? "Yes" : "No";
        });
        this.viewModelDataGrid.setRowsData(targetDefinitions, false);
    }

    public subscribers() {
        this.viewModelDataGrid.events.subscribe(e => {
            switch (e.eventType) {
                case GridConfig.onMouseHover:
                    this.onReferenceValueHover(e.eventData);
                    break;
            }
        }, error => {
            this.alertService.error(error);
        });
    }

    public getTargetDefinitionsList() {
        this.targetDefinitionService.getAllTargetDefinitions().subscribe(
            data => {
                if (data) {
                    this.targetDefinitionsListGrid = _.filter(data, targetDefinition => {
                        if (targetDefinition.status === Constant.completed) {
                          return targetDefinition;
                        }
                      });
                }
            },
            error => {
                this.alertService.error(error);
            });
    }

    public targetDefinitionsChange(targetDefinitionId) {
        this.isPreviewSelected = false;
        if (targetDefinitionId) {
            this.isPreviewSelected = true;
            this.targetDefinitionService.getColumnList(targetDefinitionId).subscribe(
                data => {
                    if (data) {
                        this.targetColumnsList = data;
                        this.initGrid(this.targetColumnsList);
                    }
                },
                error => {
                    this.alertService.error(error);
                    return;
                }
            );
        } else {
            this.isPreviewSelected = false;
            this.initGrid(null);
        }
    }

    public onReferenceValueHover(eventData) {
       // alert("Mouse hovered");
    }
}