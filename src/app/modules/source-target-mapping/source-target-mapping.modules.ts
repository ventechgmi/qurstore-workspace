import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DataGridModule } from "../core-ui-components";
import { DataGridExtendedModule,ProgressBarModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { MappingDefinitionService } from "./services/mapping-definition.service";
import { MappingDefinitionListViewComponent } from "./views/mapping-definition-list-view/mapping-definition-list-view.component";
import { SourceTargetMappingRoutingModule } from "./source-target-mapping-routing.module";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldControl } from "@angular/material/form-field";
import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";
import { LocalStorageManager } from "../../utils/local-storage-manager";
import { MappingDefinitionDefineDataViewComponent } from './views/mapping-definition-define-data-view/mapping-definition-define-data-view.component';
import { MappingDefinitionUploadFileComponent } from './views/mapping-definition-upload-file-view/mapping-definition-upload-file-view.component';
import { MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns';
import { PreviewTargetDefinitionComponent } from '../source-target-mapping/views/preview-target-definition/preview-target-definition.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { PreviewSourceTargetMappingDefinitionComponent } from "./views/preview-source-target-mapping-definition/preview-source-target-mapping-definition.component";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';


const modules = [MatSelectModule];

@NgModule(
    {
        declarations: [
            MappingDefinitionListViewComponent,
            MappingDefinitionUploadFileComponent,
            MappingDefinitionDefineDataViewComponent,
            PreviewSourceTargetMappingDefinitionComponent,
            PreviewTargetDefinitionComponent
        ],
        imports: [
            SharedModules,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            SourceTargetMappingRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatSelectModule,
            MatFormFieldModule,
            MatSelectModule,
            MatButtonModule,
            DataGridModule,
            DataGridExtendedModule,
            ProgressBarModule,
            MatCheckboxModule,
            MultiSelectModule,
            NgSelectModule,
            MatSlideToggleModule,
            NgxSmartModalModule.forRoot()
        ],
        exports: [
            MappingDefinitionListViewComponent,
            MappingDefinitionUploadFileComponent,
            MappingDefinitionDefineDataViewComponent,
            PreviewSourceTargetMappingDefinitionComponent,
            PreviewTargetDefinitionComponent
        ],
        providers: [
            AlertService,
            NgxSmartModalService,
            LocalStorageManager,
            MappingDefinitionListViewComponent,
            MappingDefinitionUploadFileComponent
        ]
    }
)
export class SourceTargetMappingModule {
}
