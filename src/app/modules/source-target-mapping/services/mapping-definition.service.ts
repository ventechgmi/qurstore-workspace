import { map } from "rxjs/operators";
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EntityService, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
const appConfigLocal = require("../../../app-config.json");
@Injectable({
    providedIn: "root"
})
export class MappingDefinitionService extends EntityService {
    appConfig: AppConfig;
    onUpdateListEvent = new EventEmitter();
    public vendorIdSubject = new ReplaySubject<any>();
    public showSuccessMessage = new Subject<any>();
    public alertInfo = { message: "", type: "" };
    private afterInitSubject = new BehaviorSubject(this.alertInfo);
    public fileContent = new BehaviorSubject({ masterFile: "", masterMappingDetailEntity: "" });
    constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
        super(http, "TargetDefinition", appConfig);
        this.appConfig = appConfig;
    }

    public showAfterIntMessage(messageInfo) {
        this.afterInitSubject.next(messageInfo);
    }

    public getAfterIntObs(): Observable<any> {
        return this.afterInitSubject.asObservable();
    }
    
    public getMappingDataInformation(orgId: string): Observable<any> {
        return this.generateGetAPI("/getMappingDefinition/" + orgId, "");
    }
    public getTargetDefinitionColumnList(targetDefinitionId: any): Observable<any> {
        return this.generateGetAPI("/column-list/" + targetDefinitionId, "");
    }

    public getSourceDefinitionColumnList(sourceDefinitionId: any): Observable<any> {
        return this.generateGetAPI("/get-source-column-list/" + sourceDefinitionId, "");
    }

    public getSourceTargetMappingDetails(sourceDefinitionId: any): Observable<any> {
        return this.generateGetAPI("/get-source-target-column-mapping/" + sourceDefinitionId, "");
    }

    public getMappingDefinitionList(): Observable<any> {
        return this.generateGetAPI("/mapping-definition-list", "");
    }

    public isUploadedFileEnabledOrNot(uploadedFileId): Observable<any>  {
        const endpoint = this.getFileServiceAPIURL() + "/check-uploaded-file-status/" + uploadedFileId;
        return this.http.get(endpoint).pipe(map(res => {
            return res;
        }));
    }

    public saveFileContent(tdHeaderInformation: any, fileToUpload: Blob): Observable<any> {
        const formData: FormData = new FormData();
        const fileInformationJsonObject: string = JSON.stringify(tdHeaderInformation);
        const endpoint = this.getFileServiceAPIURL() + "/saveUploadedFile";
        formData.set('fileInformationJsonObject', fileInformationJsonObject);
        formData.set('formFile', fileToUpload);
        return this.http
            .post(endpoint, formData).pipe(
                map((res: Response) => res));
    }
    public getFileContent(uploadedFileId: string, columnName: string): Observable<any> {
        const formData: FormData = new FormData();
        const endpoint = this.getFileServiceAPIURL() + "/get-file-source-column-data";
        formData.set('uploadedFileId', uploadedFileId);
        formData.set('columnName', columnName);
        return this.http
            .post(endpoint, formData).pipe(
                map((res: Response) => res));
    }
    public saveSourceDefinition(sourceDefinitionData: any): Observable<any> {
        return this.generatePostAPI("/save-source-definition", sourceDefinitionData);
    }

    public saveMappingDefinition(mappingDefinitionData: any): Observable<any> {
        return this.generatePostAPI("/save-mapping-definition", mappingDefinitionData);
    }

    public saveSourceTargetMappingDefinition(mappingDefinitionData: any): Observable<any> {
        return this.generatePostAPI("/save-mapping-definition", mappingDefinitionData);
    }

    public deleteMappingDefinition(mappingDefinitionId: any): Observable<any> {
        return this.generateDeleteAPI("/delete-mapping-definition/" + mappingDefinitionId, mappingDefinitionId);
    }

    public enableDisableMappingDefinition(mappingDefinitionId: any, isEnable: any): Observable<any> {
        return this.generateGetAPI("/enable-disable-mapping-definition/" + mappingDefinitionId + "/" + isEnable, "");
    }

    public getFileInformation(uploadedFileId: any): Observable<any> {
        const endpoint = this.getFileServiceAPIURL() + "/getFileInformation/" + uploadedFileId;
        return this.http.get(endpoint).pipe(map(res => {
            return res;
        }));
    }

    private getFileServiceAPIURL(): any {
        return this.getRawApi() + "/FileService";        
    }
    private getConfigurationServiceAPIURL(): any {
        return this.getRawApi() + "/configuration";        
    }

    public getOrganizationPropertyByName(organizationId: string, names: string): Observable<any>  {

        const endpoint = this.getConfigurationServiceAPIURL() + "/organization-property/list/" + organizationId +"/"+names;
        return this.http.get(endpoint).pipe(map(res => {
            return res;
        }));
    }

    private getRawApi(): string {
        return this.appConfig.apiEndPoint;
    }

    private generateGetAPI(name: any, req: any): Observable<any> {
        return this.http
            .get(this.getApi() + name + req).pipe(
                map(res => {
                    return res;
                }));
    }

    private generatePostAPI(name: any, data: any): Observable<any> {
        return this.http
            .post(this.getApi() + name, data).pipe(
                map((res: Response) => res));
    }

    private generateDeleteAPI(name: any, data: any): Observable<any> {
        return this.http
            .delete(this.getApi() + name, data).pipe(
                map(res => {
                    return res;
                }));
    }

}
