import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { MappingDefinitionListViewComponent } from "./views/mapping-definition-list-view/mapping-definition-list-view.component";
import { MappingDefinitionDefineDataViewComponent } from './views/mapping-definition-define-data-view/mapping-definition-define-data-view.component';
import { MappingDefinitionUploadFileComponent } from './views/mapping-definition-upload-file-view/mapping-definition-upload-file-view.component';
import { PreviewTargetDefinitionComponent } from '../source-target-mapping/views/preview-target-definition/preview-target-definition.component';
import { PreviewSourceTargetMappingDefinitionComponent } from "./views/preview-source-target-mapping-definition/preview-source-target-mapping-definition.component";

export const routes: Routes = [
  {
    path: "mapping-definition-list",
    component: MappingDefinitionListViewComponent
  },
  {
    path: "upload/:action/:targetDefinitionId",
    component: MappingDefinitionUploadFileComponent
  },
  {
    path: "define-data/:action/:targetDefinitionId/:sourceDefinitionId/:mappingDefinitionId/:uploadedFileId",
    component: MappingDefinitionDefineDataViewComponent
  },
  {
    path: "preview-data/:action/:targetDefinitionId/:sourceDefinitionId/:mappingDefinitionId",
    component: PreviewSourceTargetMappingDefinitionComponent
  },
  {
    path: "preview-target-definition",
    component: PreviewTargetDefinitionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SourceTargetMappingRoutingModule { }
