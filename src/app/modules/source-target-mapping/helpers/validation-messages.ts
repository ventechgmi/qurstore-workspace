export class ValidationMessages {
  public static readonly ErrorOccurred = "Error occurred: please navigate back to List screen. ";
  public static readonly FileNotUploaded = "Invalid File Type: Please upload either XLSX or CSV file.";
  public static readonly SplitSaveSuccess = "Split Mapping Saved Successfully";
  public static readonly SourceTargetSaveSuccess = "Source Target Mapping data Saved Successfully";
  public static readonly SourceTargetSaveValidation = "Please fill up required data";
  public static readonly NoMappingAvailableFor = "No mapping available for ";
  public static readonly PleaseFillUpThoseRequiredData = " field(s). Please fill up those required data.";

}
