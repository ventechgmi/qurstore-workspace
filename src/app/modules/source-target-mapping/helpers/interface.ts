export interface ISplitMergeData {
    id?: string,
    sequence: number,
    sourceColumnId: string,
    sourceColumnName?: string
    targetColumnId: string,
    targetColumnName?: string,
  }