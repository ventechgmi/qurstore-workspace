export class MappingDefinitionUploadFileViewModelConfig {
    public static readonly sourceDefinitionInProgressGuid = "EF40287A-F299-4B9B-81B2-739994825C90";
    public static readonly refMapDefFlowStatusId = "B5ADFD4B-35C6-4A01-A5D9-BE481FA3438B";
    public static readonly refMapDefStatusId = "66102DEE-5749-48F8-85B2-8AD9A8C89EFF";

}

export class PreviewSourceTargetMappingDefinitionViewModelConfig {
    public static readonly refFlowStatusId = "C086BDE5-9DAE-4F55-A577-F5A5DCCC505E";
    public static readonly refMapDefStatusId = "CF5B457E-38D7-4801-AE61-37E156548EF9";

}

export class MappingDefinitionDefineDataViewModelConfig {
  public static readonly refFlowStatusId = "62FAE82A-F7C1-4171-9E10-2BEC5E1B8F8F";
  public static readonly refMapDefStatusId = "66102DEE-5749-48F8-85B2-8AD9A8C89EFF";
  public static readonly delimiterForSplit = "C19180B0-499D-40F0-B7C2-31A790B3979A";
  public static readonly delimiterForMerge = "C19180B0-499D-40F0-B7C2-31A790B3979A"
}

export class Constant {
    public static readonly create = "Create";
    public static readonly edit = "Edit";
    public static readonly detail = "Detail";
    public static readonly split = "Split";    
    public static readonly merge = "Merge";
    public static readonly completed = "Completed";
    public static readonly labelClassDataGridPaddingRight = "<label class='data-grid-padding-right'>";
    public static readonly labelClose = "</label>";
    public static readonly fileUploadSuccessMessage = "File has been uploaded successfully";
    public static readonly maliciousFileUploadMessage = "you have uploaded malicious file. Please try to upload other file";
    public static readonly loaderUploadingText = "Uploading..";
    public static readonly loaderScanningInProgressText = "Scanning In Progress..";

}

export class FormControlNameConstant {
    public static readonly fileType = "file_type";
    public static readonly delimiterInformation = "delimiterInformation";
    public static readonly isHeaderInformationAvailable = "isHeaderInformationAvailable";
    public static readonly mappingDefinitionName = "mappingDefinitionName";
    public static readonly filePattern = "filePattern";
    public static readonly isTheDataFileBeUsedForDataIngestion = "isTheDataFileBeUsedForDataIngestion";
}

export enum FileType {
    CSV = "CSV",
    XLSX = "XLSX"
}
