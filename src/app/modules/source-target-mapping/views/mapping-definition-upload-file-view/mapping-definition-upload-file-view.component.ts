import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AlertService } from "../../../../utils/alert/alert.service";
import { MappingDefinitionUploadFileViewModel } from '../../viewmodels/mapping-definition-upload-file.viewmodel';
import { MappingDefinitionService } from '../../services/mapping-definition.service';

@Component({
  selector: 'mapping-definition-upload-file-view',
  templateUrl: './mapping-definition-upload-file-view.component.html',
  styleUrls: ['./mapping-definition-upload-file-view.component.scss'],
  providers: [MappingDefinitionUploadFileViewModel, AlertService]
})
export class MappingDefinitionUploadFileComponent implements OnInit {
  constructor(
    public mappingDefinitionService: MappingDefinitionService,
    public mappingDefinitionUploadFileViewModel: MappingDefinitionUploadFileViewModel,
   
    private router: ActivatedRoute,
    private alertService: AlertService
  ) { }

  public ngOnInit() {
    this.router.params
      .subscribe(params => {
        this.mappingDefinitionUploadFileViewModel.init(params);
      });
  }
}
