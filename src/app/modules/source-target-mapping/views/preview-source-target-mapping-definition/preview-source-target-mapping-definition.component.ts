import { Component, OnInit } from '@angular/core';
import { AlertService } from "../../../../utils/alert/alert.service";
import { PreviewSourceTargetMappingDefinitionViewModel } from '../../viewmodels/preview-source-target-mapping-definition.viewmodel';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-preview-source-target-mapping-definition',
  templateUrl: './preview-source-target-mapping-definition.component.html',
  styleUrls: ['./preview-source-target-mapping-definition.component.css'],
  providers: [PreviewSourceTargetMappingDefinitionViewModel, AlertService]
})
export class PreviewSourceTargetMappingDefinitionComponent implements OnInit {

  constructor(private router: Router,
    public previewSourceTargetMappingDefinitionViewModel: PreviewSourceTargetMappingDefinitionViewModel,
    public alertService: AlertService,
    private route: ActivatedRoute,
    public ngxSmartModalService: NgxSmartModalService
  ) { }

  ngOnInit(): void {
    this.route.params
      .subscribe(params => {
        this.initViewModels(params);
      });
  }

  public initViewModels(params: any) {
    this.previewSourceTargetMappingDefinitionViewModel.init(params);
  }

}
