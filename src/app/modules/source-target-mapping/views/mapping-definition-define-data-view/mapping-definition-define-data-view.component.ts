import {
    Component,
    OnInit,
    ContentChild,
    AfterViewInit,
    ViewChild,
    ElementRef,
    AfterContentInit,
    OnDestroy
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormBuilder } from "@angular/forms";
// import { AuthService } from "src/app/utils/auth.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { MappingDefinitionService } from '../../services/mapping-definition.service';
import { MappingDefinitionDefineDataViewModel } from '../../viewmodels/mapping-definition-define-data.viewmodel';

@Component({
    selector: "mapping-definition-define-data",
    templateUrl: "./mapping-definition-define-data-view.component.html",
    styleUrls: ["./mapping-definition-define-data-view.component.scss"],
    providers: [MappingDefinitionDefineDataViewModel, AlertService]
})
export class MappingDefinitionDefineDataViewComponent implements OnInit, OnDestroy {

    constructor(
        public mappingDefinitionService: MappingDefinitionService,
        public mappingDefinitionDefineDataViewModel: MappingDefinitionDefineDataViewModel,

        private router: ActivatedRoute,
        private alertService: AlertService
    ) {
        // need to set the afterInitObs to show other screen success or error message in different screen
        alertService.afterInitObs = mappingDefinitionService.getAfterIntObs();
    }

    public ngOnInit() {

        this.router.params
            .subscribe(params => {
                this.mappingDefinitionDefineDataViewModel.init(params);
                this.mappingDefinitionDefineDataViewModel.initServices(this.alertService);
            });

    }

    public ngAfterContentInit() { }

    public ngOnDestroy() {
        this.mappingDefinitionDefineDataViewModel.subscription.unsubscribe();
    }
}
