import {
    Component,
    OnInit,
    AfterContentInit
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from 'src/app/utils/alert/alert.service';
import { MappingDefinitionService } from '../../services/mapping-definition.service';
import { MappingDefinitionListViewModel } from '../../viewmodels/mapping-definition-list.viewmodel';
@Component({
    selector: "app-mapping-definition-list-view",
    templateUrl: "./mapping-definition-list-view.component.html",
    styleUrls: ["./mapping-definition-list-view.component.scss"],
    providers: [ MappingDefinitionListViewModel, AlertService] // need separate alert instance
})
export class MappingDefinitionListViewComponent implements OnInit, AfterContentInit {
 
    constructor(private router: Router,
                private route: ActivatedRoute,
                private mappingDefinitionService: MappingDefinitionService,
                public mappingDefinitionListViewModel: MappingDefinitionListViewModel,
                public alertService: AlertService,
                public ngxSmartModalService: NgxSmartModalService
    ) {
        alertService.afterInitObs = mappingDefinitionService.getAfterIntObs();
    }

    public  ngOnInit() {
        this.route.params
            .subscribe(params => {
               // const userId = this.authService.getUserId();
               const userId = "";
               this.mappingDefinitionListViewModel.init({ userId });
               this.mappingDefinitionListViewModel.initServices(this.alertService);
            });

        this.route.queryParams.subscribe(res => {
          //  this.projectId = res.id;
        });
    }
    public ngAfterContentInit() { }
}