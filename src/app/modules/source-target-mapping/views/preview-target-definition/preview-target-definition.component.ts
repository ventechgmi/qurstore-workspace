import { Component, OnInit } from '@angular/core';
import { AlertService } from "../../../../utils/alert/alert.service";
import { PreviewTargetDefinitionViewModel } from '../../viewmodels/preview-target-definition.viewmodel';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-preview-target-definition',
  templateUrl: './preview-target-definition.component.html',
  styleUrls: ['./preview-target-definition.component.css'],
  providers: [PreviewTargetDefinitionViewModel, AlertService]
})
export class PreviewTargetDefinitionComponent implements OnInit {

  constructor(private router: Router,
    public previewTargetDefinitionViewModel: PreviewTargetDefinitionViewModel,
    public alertService: AlertService,
    private route: ActivatedRoute,
    public ngxSmartModalService: NgxSmartModalService
  ) { }

  ngOnInit(): void {
    this.route.params
      .subscribe(params => {
        this.initViewModels(params);
      });
  }

  public initViewModels(params: any) {
    this.previewTargetDefinitionViewModel.init(params);
  }

}
