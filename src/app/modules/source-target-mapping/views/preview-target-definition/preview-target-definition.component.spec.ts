import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewTargetDefinitionComponent } from './preview-target-definition.component';

describe('PreviewTargetDefinitionComponent', () => {
  let component: PreviewTargetDefinitionComponent;
  let fixture: ComponentFixture<PreviewTargetDefinitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewTargetDefinitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewTargetDefinitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
