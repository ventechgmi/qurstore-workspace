import { NgModule } from "@angular/core";
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { DataGridModule } from "../core-ui-components";
import { NgxSmartModalModule, NgxSmartModalService } from "ngx-smart-modal";
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';

import { UserService } from "./services/user.service";
import { UserListViewComponent } from "./views/user/user-list-view/user-list-view.component";
import { RegisterComponent } from "./views/user/register/register.component";
import { UserEditDetailViewComponent } from './views/user/user-edit-detail-view/user-edit-detail-view.component';
import { UserManagementRoutingModule } from "./user-management-routing-module";

import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatButtonModule } from "@angular/material/button";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { SharedModules } from "../../utils/shared-module/shared.modules";
import { AlertService } from "src/app/utils/alert/alert.service";

@NgModule(
    {
        declarations: [
            UserListViewComponent,
            UserEditDetailViewComponent,
            RegisterComponent
        ],
        imports: [
            SharedModules,
            NgSelectModule,
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            UserManagementRoutingModule,
            MatAutocompleteModule,
            MatInputModule,
            MatFormFieldModule,
            MatButtonModule,
            DataGridModule,
            MatCheckboxModule,
            MatSlideToggleModule,
            NgxSmartModalModule.forRoot(),
            RecaptchaModule,
            RecaptchaFormsModule
        ],
        exports: [
            UserListViewComponent,
            UserEditDetailViewComponent
        ],
        providers: [
            AlertService,
            UserService,
            NgxSmartModalService
        ]
    }
)
export class UserManagementModule {

}
