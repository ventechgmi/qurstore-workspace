import { from as observableFrom } from "rxjs";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { ViewModelType } from "@fastgen/ui-framework";
import { Router, ActivatedRoute } from "@angular/router";
import * as _ from "underscore";
import { DataGridViewModel } from "../../../core-ui-components";
import config, { GridConfig } from "../../../../utils/config";
import { NgxSmartModalService } from "ngx-smart-modal";
import { AlertService } from "../../../../utils/alert/alert.service";
import { UserService } from "../../services/user.service";
import { UserViewModel } from "./user.viewmodel";
import { AuthService } from 'src/app/utils/auth.service';
import { environment } from "src/environments/environment";
import { ValidationMessages } from '../../helpers/validation-messages';
import { ExternalAuthService } from '../../services/external.authentication.service';
@Injectable()
export class UserListViewModel extends UserViewModel {
  public userId: string;
  public userList: any;
  public alertService: AlertService;
  public viewModelDataGrid: DataGridViewModel;
  public deleteConfirmModalFromList: string = "deleteConfirmModalFromList"
  public header: any = [{
    headerName: "First Name",
    field: "firstName",
  }, {
    headerName: "Last Name",
    field: "lastName",
  }, {
    headerName: "Email",
    field: "email"
  }, {
    headerName: "Number of Role(s)",
    field: "roleCount",
    isNumber: true
  }, {
    headerName: "Status",
    field: "isEnableDisplay"
  }];
  public userRolePermission: any = {};
  public isSSOLogin: boolean = false;

  public constructor(
    public userService: UserService,
    private router: Router,
    private chRef: ChangeDetectorRef,
    public ngxSmartModalService: NgxSmartModalService,
    public route: ActivatedRoute,
    public authService: AuthService,
    public externalAuthService: ExternalAuthService
  ) {
    super(userService, ViewModelType.Edit, ngxSmartModalService, route, authService);
  }

  public init() {
    this.userId = this.authService.getUserId();
    this.viewModelDataGrid = new DataGridViewModel();
    this.viewModelDataGrid.setPagination(true);
    this.userRolePermission = this.getUserClaims() || {};
    this.checkIsSSOLogin();
    this.subscribeToCreateSuccessMessage();
    this.subscribers();
    if (this.isSSOLogin) {
      this.header.unshift({
        headerName: "User Name",
        field: "userName"
      })
    }
    this.viewModelDataGrid.setHeader(this.header, false, { canAccessRefresh: this.isSSOLogin, canUpdate: this.userRolePermission.canUpdate, canDelete: this.userRolePermission.canDelete });
    this.setDataTableConfigurations();
    this.getUsersList();
    return observableFrom([]);
  }

  public initServices(alertService: AlertService) {
    this.alertService = alertService;
  }

  private subscribers() {
    this.viewModelDataGrid.events.subscribe(e => {
      switch (e.eventType) {
        case GridConfig.editClicked:
          this.userService.isUserEditFromList = true;
          this.userId = e.eventData.rowDetails.id;
          this.redirectToDetail();
          break;
        case GridConfig.deleteClicked:
          this.userId = e.eventData.rowDetails.id;
          this.deleteUserFromList();
          break;
        case GridConfig.refreshClicked:
          const rowDetails = e.eventData.rowDetails;
          this.refreshADUserDetails(rowDetails);
          break;
        default:
          this.userService.isUserEditFromList = false;
          if (e.eventData && e.eventData.rowDetails) {
            this.userId = e.eventData.rowDetails.id;
            this.redirectToDetail();
          }
      }
    }, error => { });
  }

  private redirectToDetail() {
    this.userService.userIdSubject.next(this.userId);
    this.router
      .navigate(["/user-management/user-detail/" + this.userId])
      .then()
      .catch();
  }

  public redirectToCreate() {
    this.userService.isUserEditFromList = false;
    this.userService.userIdSubject.next(this.userId);
    this.router
      .navigate(["/user-management/user-detail/new-user"])
      .then()
      .catch();
  }

  public deleteUserFromList() {
    this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
    this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).open();
  }

  public closeADConfirmation() {
    this.ngxSmartModalService.getModal("activeDirectoryRefreshConfirmFromList").close();
  }

  public openADConfirmation() {
    this.ngxSmartModalService.getModal("activeDirectoryRefreshConfirmFromList").open();
  }

  public async deleteUser() {
    await this.userService.deleteUser(this.userId).subscribe(data => {
      if (data.isSuccess) {
        this.getUsersList();
        this.userService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
      }
      else {
        this.alertService.error(data.message, true);
        this.userService.showAfterIntMessage({ text: data.message, type: "error", toStable: true });
      }
    }, (err) => {
      ((err.error && err.error.validationErrors) ?
        this.alertService.error(err.error.errorList[0], true) :
        this.userService.showAfterIntMessage({ text: err.error.errorList[0], type: "error", toStable: true }));
      this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).close();
    }, () => {
      this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).close();
    });
  }

  // data-table configurations for project management
  public setDataTableConfigurations() {
    this.viewModelDataGrid.dataTableConfiguration = {
      destroy: true,
      columnDefs: [{ targets: [0], visible: false, searchable: false }, { type: "num-html", targets: [2] }],
      order: [[1, "asc"]]
    };
  }

  public getUsersList() {
    this.userService.getAllUsers().subscribe(
      data => {
        if (data) {
          this.userList = data;
          this.initGrid(this.userList);
        }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  public initGrid(data) {
    const users = data ? data : [];
    users.map(user => {
      user.isEnableDisplay = user.isEnabled ? "Active" : "Inactive";
      user.roleCount = user.userOrganizationRolesUser === undefined ? "0" : user.userOrganizationRolesUser.length;
    });
    this.viewModelDataGrid.setRowsData(users, false);
  }

  public subscribeToCreateSuccessMessage() {
    this.userService.showSuccessMessage.subscribe(data => {
      const { message, toStable } = data;
      this.alertService.success(message, toStable);
    });
  }

  public refreshActiveDirectoryUsers(): void {
    this.ngxSmartModalService.getModal("activeDirectoryRefreshConfirmFromList").close();
    this.alertService.info(ValidationMessages.userDetailRefreshInfo, true);
    this.userService.refreshAllADUserDetails().subscribe((data) => {
      if (data && data.isSuccess) {
        this.getUsersList();
        this.alertService.success(ValidationMessages.userDetailRefreshedSuccess, true);
      } else {
        this.alertService.error(data.message, true);
      }
    }, (error) => {
      this.alertService.error(error.error.errorList[0], true);
    });
  }

  private checkIsSSOLogin(): void {
    if (environment.enableSSO) {
      this.isSSOLogin = true;
    }
  }

  private refreshADUserDetails(userDetails: any): void {
    if (userDetails) {
      if (!userDetails.isEnabled) {
        const errorMessage = ValidationMessages.userNotActiveUserError.replace("{emailAddress}", userDetails.email);
        this.alertService.error(errorMessage, true, true, true);
        return;
      }
      if (!userDetails.authenticationProviderId || !userDetails.userName) {
        const errorMessage = ValidationMessages.userNotADUserError.replace("{emailAddress}", userDetails.email);
        this.alertService.error(errorMessage, true, true, true);
        return;
      } else {
        this.alertService.info(ValidationMessages.userDetailRefreshInfo, true, true, true);
        const req = {
          providerId: userDetails.authenticationProviderId,
          userName: userDetails.userName
        };
        let isUserNotExists = false;
        this.externalAuthService.GetUserNameFromProvider(req).subscribe(
          data => {
            let requestParam: any;
            if (data && data.isSuccess) {
              if (data.message == ValidationMessages.NoUserFoundMessage) {
                isUserNotExists = true;
                requestParam = {
                  firstName: userDetails.firstName,
                  middleName: userDetails.middleName,
                  lastName: userDetails.lastName,
                  email: userDetails.email,
                  isEnabled: false,
                  isDeleted: false,
                  authenticationProviderId: userDetails.authenticationProviderId,
                  userName: userDetails.userName,
                  id: userDetails.id,
                  userOrganizationRolesUser: userDetails.userOrganizationRolesUser
                }
              } else {
                const completeUserDetails = data.payload;
                const userMiddleName = userDetails.middleName ? userDetails.middleName : null;
                if (userDetails.firstName != completeUserDetails.givenName || userMiddleName != completeUserDetails.middleName || userDetails.lastName != completeUserDetails.surname || userDetails.email != completeUserDetails.emailAddress || userDetails.isEnabled != completeUserDetails.enabled) {
                  requestParam = {
                    firstName: completeUserDetails.givenName,
                    middleName: completeUserDetails.middleName,
                    lastName: completeUserDetails.surname,
                    email: completeUserDetails.emailAddress,
                    isEnabled: completeUserDetails.enabled,
                    isDeleted: false,
                    authenticationProviderId: userDetails.authenticationProviderId,
                    userName: userDetails.userName,
                    id: userDetails.id,
                    userOrganizationRolesUser: userDetails.userOrganizationRolesUser
                  }
                } else {
                  this.alertService.success(ValidationMessages.userDetailRefreshedSuccess, true, true, true);
                  return;
                }
              }
              this.userService.saveUser(requestParam).subscribe((data) => {
                if (data) {
                  this.getUsersList();
                  if (isUserNotExists) {
                    this.alertService.error(ValidationMessages.userDetailRemoved, true, true, true);
                  } else {
                    this.alertService.success(ValidationMessages.userDetailRefreshedSuccess, true, true, true);
                  }
                }
              }, (error) => {
                this.alertService.error(error, true, true, true);
              });
            }
          },
          error => {
            this.alertService.error(error, true, true, true);
          });
      }
    }
  }


  public onPopUpClose(): void {
    this.ngxSmartModalService.getModal(this.deleteConfirmModalFromList).close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }


}
