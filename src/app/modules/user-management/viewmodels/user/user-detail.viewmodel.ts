import { from as observableFrom } from "rxjs";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ViewModelType } from "@fastgen/ui-framework";
import _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { UserService } from "../../services/user.service";
import { RoleService } from "../../../role-management/services/role.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { UserViewModel } from "./user.viewmodel";
import { ValidationMessages } from "../../helpers/validation-messages";
import { AuthService } from "../../../../utils/auth.service";
import { ExternalAuthService } from "../../services/external.authentication.service";
import { ModalType } from "../../../../utils/config"
@Injectable()
export class UserDetailViewModel extends UserViewModel {
  userId: any;
  isDeleteButtonEnable = true;
  userDetailsForm: FormGroup;
  submitted = false;
  modalType = "Edit" || "Create";
  isFormValueChange = false;
  userRolesCount = 0;
  roleList: any;
  organizationListGrid: any;
  organizationId: any;
  providerList: any;
  userDetail: any = {
    id: "",
    firstName: "",
    middleName: "",
    lastName: "",
    email: "",
    isEnabled: true,
    userOrganizationRoles: [],
    organizationList: ""
  };
  isExist: boolean;
  isProviderUser: boolean;
  activeDirectoryOption = false;
  activeDirectoryUser: boolean;
  userData;
  activeDirectoryType;
  authenticationProviderId: string = "";
  isDisabled: boolean;
  public isSystemAdmin = false;
  public userRolePermission: any = {};
  public isSameUserToEdit: boolean = false;
  public currentUserId: string = "";
  private userCancelConfirmModal : string = "userCancelConfirmModal";

  public constructor(
    public userService: UserService,
    public roleService: RoleService,
    private router: Router,
    private fb: FormBuilder,
    private alertService: AlertService,
    public ngxSmartModalService: NgxSmartModalService,
    private chRef: ChangeDetectorRef,
    public route: ActivatedRoute,
    public authService: AuthService,
    public externalAuthService: ExternalAuthService
  ) {
    super(userService, ViewModelType.ReadOnly, ngxSmartModalService, route, authService);
  }

  public init(params?) {
    this.isSystemAdmin = this.authService.isSysAdminBoolean();
    this.currentUserId = this.authService.getUserId();
    this.modalType = params.modalType;
    this.organizationId = this.authService.getOrganizationId();
    this.userRolePermission = this.getUserClaims() || {};
    // <! Commented by MS(28/12/20) - Since this logics are not needed, might be used in future !>
    // if(!this.isSystemAdmin) {
    //     this.getProviderList();
    // }
    // this.getOrganizationList();
    this.getProviderList();
    this.getRolesList();
    this.initUserDetailsForm();
    this.userService.showAfterIntMessage({ text: "", type: "success", toStable: true });
    super.init(params)
      .subscribe((data: any) => {
        this.isProviderUser = (data.authenticationProviderId) ? true : false;
        this.authenticationProviderId = data.authenticationProviderId;
        this.userDetail = data;
        if (data.userOrganizationRolesUser.length > 0) {
          this.GetProviderListByOrganizationId(data.userOrganizationRolesUser[0].organizationId);
        }
        this.updateFormFields(this.userDetail);
      });
    if (params.id) {
      this.userId = params.id;
    }
    this.initSubscribers();
    return observableFrom([]);
  }

  private initUserDetailsForm() {
    this.userDetailsForm = this.fb.group({
      userName: [""],
      firstName: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20), super.validateSpecialCharactersandWhiteSpace(3)]],
      middleName: ["", [Validators.minLength(3), Validators.maxLength(20), super.validateSpecialCharactersandWhiteSpace(3)]],
      lastName: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20), super.validateSpecialCharactersandWhiteSpace(3)]],
      email: ["", [Validators.required, super.validEmail()]],
      isEnabled: [true, [Validators.required]],
      userOrganizationRoles: [[], [Validators.required]]
      // activeDirectory: [""],  <! Commented by MS(28/12/20) - Since this logics are not needed, might be used in future !>
      // organizationList: [""]
    });
  }

  private updateFormFields(userDetail) {
    const roleIds = [];
    const organizationIds = [];
    if (userDetail.userOrganizationRolesUser) {
      userDetail.userOrganizationRolesUser.forEach(role => {
        if (role.roleId) {
          roleIds.push(role.roleId);
        }

        if (role.organizationId) {
          organizationIds.push(role.organizationId);
        }
      });
      const organizationIdList = organizationIds[0];
      this.userDetailsForm.patchValue({
        userOrganizationRoles: roleIds
        // organizationList: organizationIdList  <! Commented by MS(28/12/20) - Since this logics are not needed, might be used in future !>
      });
    }

    this.userDetailsForm.patchValue({
      userName: userDetail.userName,
      firstName: userDetail.firstName,
      middleName: userDetail.middleName,
      lastName: userDetail.lastName,
      email: userDetail.email,
      isEnabled: userDetail.isEnabled
    });

    if (this.userId === this.currentUserId && !this.isSystemAdmin) {
      this.isSameUserToEdit = true;
    }

    // <! Commented by MS(28/12/20) - Since this logics are not needed, might be used in future !>
    // if (this.isProviderUser) {
    //   this.userDetailsForm.patchValue({
    //     activeDirectory: this.authenticationProviderId
    //   });
    // }
  }

  private get userDetailsFormControls() {
    return this.userDetailsForm.controls;
  }

  public get detailForm() {
    return this.userDetailsForm.controls;
  }

  public async saveChanges() {
    this.submitted = true;
    this.isExist = false;
    if (this.userDetailsForm.invalid) { return; }
    this.userDetailsForm.controls.firstName.setValue(this.userDetailsForm.controls.firstName.value.trim());
    if (this.userDetailsForm.controls.middleName.value != null) {
      this.userDetailsForm.controls.middleName.setValue(this.userDetailsForm.controls.middleName.value.trim());
    }
    this.userDetailsForm.controls.lastName.setValue(this.userDetailsForm.controls.lastName.value.trim());
    this.userDetailsForm.controls.email.setValue(this.userDetailsForm.controls.email.value.trim());
    this.userDetailsForm.controls.isEnabled.setValue(this.userDetailsForm.controls.isEnabled.value);
    this.userDetailsForm.controls.userOrganizationRoles.setValue(this.userDetailsForm.controls.userOrganizationRoles.value);
    if (this.userDetailsForm.controls.userName.value != "") {
      this.userDetailsForm.controls.userName.setValue(this.userDetailsForm.controls.userName.value.toLowerCase());
    }
    // <! Commented by MS(28/12/20) - Since this logics are not needed, might be used in future !>
    // if(this.userDetailsForm.controls.organizationList.value) {
    //   this.userDetailsForm.controls.organizationList.setValue(this.userDetailsForm.controls.organizationList.value);
    // } else {
    //   this.userDetailsForm.controls.organizationList.setValue(this.organizationId);
    // }
    // this.userDetailsForm.controls.activeDirectory.setValue(this.userDetailsForm.controls.activeDirectory.value);
    this.updateUser().catch();
  }

  public async updateUser() {
    const userDetails = this.transformToEntityModel();
    return this.userService.saveUser(userDetails)
      .subscribe(
        (data) => {
          this.submitted = false;
          this.alertService.success(data.message, true);
          this.userService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
          this.navigateToList().then().catch();
        }, (err) => {
          this.submitted = false;
          this.alertService.error(err.error.errorList[0], true);
          this.userService.showAfterIntMessage({ text: err.error.errorList[0], type: "error", toStable: true });
        },
        () => {
          this.navigateToList().then().catch();
        });
  }

  public transformToEntityModel() {
    const {
      firstName,
      middleName,
      lastName,
      email,
      isEnabled,
      userOrganizationRoles,
      userName
    } = this.userDetailsFormControls;

    const userDetailEntity: any = {
      firstName: firstName.value,
      middleName: middleName.value,
      lastName: lastName.value,
      email: email.value,
      isEnabled: isEnabled.value,
      isDeleted: false,
      userOrganizationRolesUser: [],
      lastModifiedBy: this.authService.getUserId(),
      organizationId: this.organizationId,
      authenticationProviderId: (this.authenticationProviderId) ? this.authenticationProviderId : null,
      userName: userName.value
    };

    if (userOrganizationRoles.value) {
      userOrganizationRoles.value.forEach(roleIdEle => {
        const userRole: any = {
          roleId: roleIdEle,
          organizationId: this.organizationId,
          lastModifiedBy: this.authService.getUserId()
        };
        userDetailEntity.userOrganizationRolesUser.push(userRole);
      });
    }

    if (this.userDetail.id) {
      userDetailEntity.id = this.userDetail.id;
    }

    return userDetailEntity;
  }

  public isFormValueChanged() {
    const { firstName, middleName, lastName, email, isEnabled, userOrganizationRoles, organizationList } = this.userDetailsFormControls;

    const refRole: any = {
      id: this.userDetail.id,
      firstName: firstName.value,
      middleName: middleName.value,
      lastName: lastName.value,
      email: email.value,
      isEnabled: isEnabled.value,
      userOrganizationRoles: this.userDetail.userOrganizationRoles,
      organizationList: this.userDetail.organizationList
    };
    return _.isEqual(refRole, this.userDetail);
  }

  public onCancel() {
    if (this.modalType === "Edit" || this.modalType === "Create") {
      if(this.userDetailsForm.dirty){
        this.authService.popUpStatusEvent.next({ modalPoppedUp: true });
        this.ngxSmartModalService.getModal(this.userCancelConfirmModal).open()
      }else{
        this.navigateToList().then().catch()
      }
    } else {
      this.navigateToList().then().catch();
    }
  }

  public async navigateToList() {
    return this.router.navigate(["user-management/user-list"]);
  }

  public onConfirm() {
    this.ngxSmartModalService.getModal(this.userCancelConfirmModal).close();
    this.userService.showAfterIntMessage({ text: "" });
    this.navigateToList();
  }

  public initSubscribers() {
    this.userService.getUserRolesCountObs()
      .subscribe((count) => {
        this.userRolesCount = count || 0;
      });

    this.userService.isEditModeSubject.subscribe((isEditMode) => {
      this.modalType = isEditMode;
    });

    this.userDetailsForm.valueChanges.subscribe(() => {
      this.isFormValueChange = !this.isFormValueChanged();
    });
  }

  private getRolesList() {
    this.roleService.getAllRolesForUserModule().subscribe(
      data => {
        if (data) {
          this.roleList = data;
        }
      },
      error => {
        this.alertService.error(error);
      });
  }

  public errorFor(propertyName: string): string | void {
    if (propertyName) {
      const property = propertyName.replace(/\w+/g, (w) => w[0].toUpperCase() + w.slice(1).toLowerCase());

      if (this.userDetailsForm.controls[propertyName]) {
        if (this.userDetailsForm.controls[propertyName].hasError("required")) {
          return property.concat(ValidationMessages.RequiredMessage);
        }
        else if (this.userDetailsForm.controls[propertyName].hasError("minlength")
          || this.userDetailsForm.controls[propertyName].hasError("maxlength")) {
          return property.concat(ValidationMessages.MinMaxLengthMessage);
        }
        else if (this.userDetailsForm.controls[propertyName].hasError("email")) {
          return property.concat(ValidationMessages.InvalidMessage);
        }
        else if (this.userDetailsForm.controls[propertyName].hasError("pattern")) {
          return property.concat(ValidationMessages.InvalidMessage);
        }
        else {
          return null;
        }
      }
    }
  }

  private getProviderList() {
    this.isProviderUser = false;
    this.externalAuthService.GetProviderListByOrganization(this.organizationId).subscribe(
      data => {
        if (data) {
          this.providerList = data;
          if (data && data.length) {
            this.isProviderUser = true;
            this.authenticationProviderId = data[0].id;
          } else {
            this.isProviderUser = false;
            this.authenticationProviderId = null;
          }
        }
      },
      error => {
        this.alertService.error(error);
      });
  }

  public searchUser(): boolean | void {
    if (!this.userDetailsForm.controls.userName.value) {
      this.alertService.error(ValidationMessages.enterUsernameError);
      return false;
    } else {
      this.userDetailsForm.controls.userName.setValue(this.userDetailsForm.controls.userName.value.toLowerCase());
    }
    const req = {
      providerId: this.authenticationProviderId,
      userName: this.userDetailsForm.controls.userName.value
    };
    this.isDisabled = true;
    this.externalAuthService.GetUserNameFromProvider(req).subscribe(
      data => {
        if (data && data.isSuccess) {
          if (data.message == ValidationMessages.NoUserFoundMessage) {
            this.userDetailsForm.patchValue({
              isEnabled: false
            });
            if (this.modalType === ModalType.CREATE) {
              this.alertService.error(ValidationMessages.NoUserFoundMessage, true);
            } else {
              this.alertService.error(ValidationMessages.userDetailRemoved, true);
            }

          } else {
            const completeUserDetails = data.payload;
            const adUserDetail: any = {
              firstName: completeUserDetails.givenName,
              middleName: completeUserDetails.middleName ? completeUserDetails.middleName : "",
              lastName: completeUserDetails.surname,
              email: completeUserDetails.emailAddress,
              userName: this.userDetailsForm.controls.userName.value,
              isEnabled: completeUserDetails.enabled
            };
            this.userData = adUserDetail;
            this.isProviderUser = true;
            this.updateFormFields(this.userData);
            this.alertService.success(data.message, true);
          }
          this.isDisabled = false;
          this.userDetailsForm.markAsDirty();
        }
      },
      error => {
        this.isDisabled = false;
        this.resetControls();
        this.alertService.error(error);
      });
  }
  private resetControlsWhenActiveDirectoryChange(): void {
    this.userDetailsForm.controls.firstName.reset();
    this.userDetailsForm.controls.middleName.reset();
    this.userDetailsForm.controls.lastName.reset();
    this.userDetailsForm.controls.email.reset();
    this.userDetailsForm.controls.isEnabled.setValue(true);
    this.userDetailsForm.controls.userName.reset();
  }

  private resetControls(): void {
    this.userDetailsForm.controls.firstName.reset();
    this.userDetailsForm.controls.middleName.reset();
    this.userDetailsForm.controls.lastName.reset();
    this.userDetailsForm.controls.email.reset();
    this.userDetailsForm.controls.isEnabled.setValue(true);
    this.userDetailsForm.controls.userOrganizationRoles.reset();
    this.userDetailsForm.controls.userName.reset();
  }

  private async GetProviderListByOrganizationId(id) {
    await this.externalAuthService.GetProviderListByOrganization(id).subscribe(
      data => {
        if (data) {
          this.providerList = data;
        }
      },
      error => {
        this.alertService.error(error);
        this.providerList = null;
      });
  }

  // <! Commented by MS(28/12/20) - Since this logics are not needed, might be used in future !>
  // public selectOrganizationChange(event): void {
  //   if (event.id) {
  //     this.GetProviderListByOrganizationId(event.id);
  //   } else {
  //     this.providerList = null;
  //   }
  // }

  // public selectChangeHandler(event: any) {
  //   this.resetControlsWhenActiveDirectoryChange();
  //   if (event !== undefined && event !== 0) {
  //     this.activeDirectoryUser = true;
  //     this.isProviderUser = true;
  //     const selectedActiveDirectory = this.providerList.filter(t => t.id === event);
  //     this.activeDirectoryType = (selectedActiveDirectory && selectedActiveDirectory.length) ? selectedActiveDirectory[0].type : undefined;
  //   } else {
  //     this.activeDirectoryUser = true;
  //     this.isProviderUser = false;
  //   }
  // }

  public onPopUpClose(): void {
    this.ngxSmartModalService.getModal(this.userCancelConfirmModal).close();
    this.authService.popUpStatusEvent.next({ modalPoppedUp: false });
  }


}
