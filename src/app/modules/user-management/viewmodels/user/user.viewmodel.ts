
import { map } from "rxjs/operators";
import { EntityBaseViewModel, ViewModelType } from "@fastgen/ui-framework";
import { Observable, Subject ,  of } from "rxjs";
import { AbstractControl, ValidatorFn } from "@angular/forms";
import { NgxSmartModalService } from "ngx-smart-modal";
import { ActivatedRoute } from "@angular/router";
import { ROUTES_CONFIG_MAP, MODULE_BASE_URL_MODULE_ID_MAP } from "src/app/utils/config";
import { AuthService } from 'src/app/utils/auth.service';
import { UserService } from "../../services/user.service";

export class UserViewModel extends EntityBaseViewModel<"", UserService> {
    public moduleId: string;
    public moduleAccess;
    public moduleInfo;
    public updateUserListSubject = new Subject<any>();
    public userDetailSelection = new Subject<any>();

    public constructor(
        public userService: UserService,
        public viewModelType: ViewModelType,
        public ngxSmartModalService: NgxSmartModalService,
        public route: ActivatedRoute,
        public authService: AuthService)
    {
        super(userService, viewModelType);
        this.entityName = "user";
    }

    public getUserClaims(): any {
        const baseUrl = ROUTES_CONFIG_MAP.USER_MANAGEMENT.BASE_URL;
        this.moduleId = MODULE_BASE_URL_MODULE_ID_MAP[baseUrl];
        this.moduleInfo = this.authService.getModuleAccess(this.moduleId) || null;
        this.moduleAccess = this.moduleInfo ? this.moduleInfo.RolePermissions : {};  

        return this.moduleAccess;
    }

    public noWhitespaceValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            const isWhitespace = (control.value || "").trim().length === 0;
            const isValid = !isWhitespace;
            return isValid ? null : { hasOnlyWhiteSpace: true };
        };
    }

    /*
    This method validates the below
    1. validates the control has only white spaces since most of the name fields should not allow just white spaces
    2. validates the control whether it has only special characters.

    */
    public validateSpecialCharactersandWhiteSpace(lengthMin: number): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            const pattern = "[A-Za-z0-9]";
            const whiteSpace = /^\s*$/;
            if ((control.value || "").length === 0) {
                return null;
            } else if ((control.value || "").trim().length < lengthMin) {
                return { minlength: true };
            } else if (control.value.match(whiteSpace)) {
                return { hasOnlyWhiteSpace: true };
            } else if (!control.value.match(pattern)) {
                // check if the input doest not have any alphabets or in other words input has only special characters
                // return "Don't enter only special characters";
                return { hasOnlySpecialCharaters: true };
            }
        };
    }

    /* 
    Validate email address since Validators.email accepting abc@abc value and it is not validating the domain. 
    */
   public validEmail(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        const pattern = "[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,3}$";
        const whiteSpace = /^\s*$/;
        if ((control.value || "").match(whiteSpace)) {
            return { invalidEmail: true };
        } else if (!control.value.match(pattern)) {
            return { invalidEmail: true };
        }
    };
}
}
