import { from as observableFrom,  Observable } from "rxjs";
import { Injectable, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ViewModelType } from "@fastgen/ui-framework";
import _ from "underscore";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { UserService } from "../../services/user.service";
import { AlertService } from "../../../../utils/alert/alert.service";
import { UserViewModel } from "./user.viewmodel";
import { ValidationMessages } from "../../helpers/validation-messages";
import { ConfirmedValidator } from 'src/app/utils/validators/confirmed.validator';
import CountryData from '../../CountryList.json';
import { reCaptchaSiteKey, placeHolderImageSrc } from "../../../../utils/config";
import { AuthService } from 'src/app/utils/auth.service';
@Injectable()
export class RegisterViewModel extends UserViewModel {
  registerForm: FormGroup;
  submitted = false;
  isFormValueChange = false;
  imageSrc: any;
  isExist: boolean;
  countryList: any;
  siteKey : string;
  reCaptchaToken : any;

  public constructor(
    public userService: UserService,
    private router: Router,
    private fb: FormBuilder,
    private alertService: AlertService,
    public ngxSmartModalService: NgxSmartModalService,
    private chRef: ChangeDetectorRef,
    public route: ActivatedRoute,
    public authService: AuthService)
  {
    super(userService, ViewModelType.ReadOnly, ngxSmartModalService, route, authService);
  }

  public init() {
    this.imageSrc = placeHolderImageSrc;
    this.siteKey = reCaptchaSiteKey;
    this.getCountryList();
    this.initRegistrationDetailsForm();
    return observableFrom([]);
  }

  private initRegistrationDetailsForm() {
    this.registerForm = this.fb.group({
      FirstName: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20), super.validateSpecialCharactersandWhiteSpace(3)]],
      MiddleName: ["", [Validators.minLength(3), Validators.maxLength(20), super.validateSpecialCharactersandWhiteSpace(3)]],
      LastName: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20), super.validateSpecialCharactersandWhiteSpace(3)]],
      Email: ["", [Validators.required, Validators.pattern(ValidationMessages.EmailPattern)]],
      ConfirmEmail: ["", [Validators.required, Validators.pattern(ValidationMessages.EmailPattern)]],
      PhoneNumber : ["", [Validators.pattern(ValidationMessages.PhoneNumberPattern), super.validateSpecialCharactersandWhiteSpace(3)]],
      Agreed: [null, [Validators.required]],
      ReCaptcha: [null, [Validators.required]],
      Organizations : [[], []],
      Organization: this.fb.group({
        Name: ["", [Validators.required, super.validateSpecialCharactersandWhiteSpace(3)]],
        WebUrl: ["", [super.validateSpecialCharactersandWhiteSpace(3)]],
        Logo : ["", []],
        OrgLogoFile: ["", []],
        OrganizationAddress : [[], []]
      }),
      OrganizationAdd: this.fb.group({
        AddressLine1: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(200), super.validateSpecialCharactersandWhiteSpace(3)]],
        AddressLine2: ["", [Validators.minLength(3), Validators.maxLength(500), super.validateSpecialCharactersandWhiteSpace(3)]],
        City: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20), super.validateSpecialCharactersandWhiteSpace(3)]],
        State: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20), super.validateSpecialCharactersandWhiteSpace(3)]],
        Zip: ["", [Validators.required, Validators.pattern(ValidationMessages.zipCodePattern), super.validateSpecialCharactersandWhiteSpace(3)]],
        Country: [null, [Validators.required]],
      })
    }, {
      validator: ConfirmedValidator('Email', 'ConfirmEmail')
    });
  }

  public get detailForm() {
    return this.registerForm.controls;
  }

  public async saveChanges() {
    console.log("Inside save changes");
    this.submitted = true;
    this.isExist = false;
    if (!this.reCaptchaToken) {
      this.registerForm.controls.ReCaptcha.markAsTouched();
      return;
    }
    this.registerUser().catch();
  }

  public async registerUser() {
    const userDetails = this.transformToEntityModel();
    return this.userService.registerUser(userDetails)
      .subscribe (
        (data) => {
          this.submitted = false;
          this.alertService.success(data.message, true);
          this.userService.showAfterIntMessage({ text: data.message, type: "success", toStable: true });
          this.resetForm();
        }, (err) => {
          this.submitted = false;
          this.registerForm.controls.ReCaptcha.setValue('');
          this.alertService.error(err.error.errorList[0], true);
          this.userService.showAfterIntMessage({ text: err.error.errorList[0], type: "error", toStable: true });
        },
        () => {
          this.navigateToRegister().then().catch();
        });
  }

  public transformToEntityModel() {
    const registerationDetails = this.registerForm.value;
    registerationDetails.ReCaptchaToken = this.reCaptchaToken;
    registerationDetails.Organizations = [];
    registerationDetails.Organization.OrganizationAddress = [];
    registerationDetails.Organizations.push(registerationDetails.Organization);
    registerationDetails.Organization.OrganizationAddress.push(registerationDetails.OrganizationAdd);

    return registerationDetails;
  }

  public resetForm() {
    this.registerForm.reset();
    this.registerForm.markAsPristine();
    this.registerForm.markAsUntouched();
    this.imageSrc = placeHolderImageSrc;
  }

  public onCancel() {
    this.initRegistrationDetailsForm();
  }

  public async onFileChange(ev) {
    const [file] = ev.target.files;
    if (!file) {
      this.imageSrc = placeHolderImageSrc;
      this.registerForm.patchValue({
        Organization : {
          Logo: null
        },
      });
      return;
    }

    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      alert("Only images are supported.");
      return;
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event) => {
      this.imageSrc = reader.result;
      this.registerForm.patchValue({
        Organization : {
          Logo: reader.result as string
        },
      });
    };
  }

  public async navigateToList() {
    return this.router.navigate(["user-management/user-list"]);
  }

  public async navigateToRegister() {
    return this.router.navigate(["user-management/register"]);
  }

  public errorFor(propertyName: string): string | void {
    if (propertyName) {
      const property = propertyName.replace(/\w+/g, (w) => w[0].toUpperCase() + w.slice(1).toLowerCase());
      const splittedProperty = property.split('.');
      const fieldProperty = splittedProperty.length >= 2 ? splittedProperty[1] : splittedProperty[0];

      const control = this.registerForm.get(propertyName);

      if (control) {
        if (control.hasError("required")) {
          return fieldProperty.concat(ValidationMessages.RequiredMessage);
        }
        else if (control.hasError("minlength")
                  || control.hasError("maxlength")) {
          return fieldProperty.concat(ValidationMessages.MinMaxLengthMessage);
        }
        else if (control.hasError("email") || control.hasError("pattern")) {
          return fieldProperty.concat(ValidationMessages.InvalidMessage);
        }
        else if (control.hasError("confirmedValidator")) {
          return ValidationMessages.EmailMatchMessage;
        }
        else {
          return null;
        }
      }
    }
  }

  private getCountryList() {
    this.countryList = CountryData;
  }

  public resolved(captchaResponse: string) {
    this.reCaptchaToken = captchaResponse;
  }
 }
