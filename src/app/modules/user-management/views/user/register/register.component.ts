import { FormGroup, Validators } from "@angular/forms";
import { Component, OnInit, OnDestroy, OnChanges } from "@angular/core";
import { Observable, Subject, Subscription } from "rxjs";
import { ActivatedRoute } from "@angular/router";

import { RegisterViewModel } from "../../../viewmodels/user/registration.viewmodel";
import config from "../../../../../utils/config";
import { AlertService } from "../../../../../utils/alert/alert.service";

export interface User {
  name: string;
}

@Component({
  selector: "app-user-registration",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
  providers: [RegisterViewModel, AlertService]
})

export class RegisterComponent implements OnInit {
  constructor(public registerViewModel: RegisterViewModel, private router: ActivatedRoute, private alertService: AlertService) { }
  public ngOnInit() {
        this.initViewModels();
  }
  public displayFn(user?: User): string | undefined {
    return user ? user.name : undefined;
  }
  public initViewModels() {
    this.registerViewModel.init();
  }
}
