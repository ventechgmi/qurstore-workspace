import {
    Component,
    OnInit,
    ContentChild,
    AfterViewInit,
    ViewChild,
    ElementRef,
    AfterContentInit
} from "@angular/core";

import { Router, ActivatedRoute } from "@angular/router";
import { NgxSmartModalService } from "ngx-smart-modal";
import { FormBuilder } from "@angular/forms";
import config from "../../../../../utils/config";

import { UserListViewModel } from "../../../viewmodels/user/user-list.viewmodel";
import { UserService } from "../../../services/user.service";
import { AlertComponent } from "src/app/utils/alert/alert.component";
import { AlertService } from "../../../../../utils/alert/alert.service";

@Component({
    selector: "app-user-list-view",
    templateUrl: "./user-list-view.component.html",
    styleUrls: ["./user-list-view.component.scss"],
    providers: [ UserListViewModel, AlertService]
})
export class UserListViewComponent implements OnInit, AfterContentInit {

    public roleList: any;
    public dataTable: any;
    public userCreateModalRef: any;
    projectId: any;

    constructor(private router: Router,
                private userService: UserService,
                public userListViewModel: UserListViewModel,
                public alertService: AlertService,
                private route: ActivatedRoute,
                public ngxSmartModalService: NgxSmartModalService
    ) {
        // need to set the afterInitObs to show other screen success or error message in different screen
        alertService.afterInitObs = userService.getAfterIntObs();
    }

    public ngOnInit() {
        this.route.params
            .subscribe(params => {
               this.userListViewModel.initServices(this.alertService);
               this.userListViewModel.init();
            });

        this.route.queryParams.subscribe(res => {
            this.projectId = res.id;
        });
    }

    public ngOnDestroy() {
      this.userService.showAfterIntMessage({ text:"", type: "success", toStable: true});
    }

    public ngAfterContentInit() { }
}
