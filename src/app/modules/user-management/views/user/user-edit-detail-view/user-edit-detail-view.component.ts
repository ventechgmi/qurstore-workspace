import { FormGroup, Validators } from "@angular/forms";
import { Component, OnInit, OnDestroy, OnChanges } from "@angular/core";
import { Observable, Subject, Subscription } from "rxjs";
import { ActivatedRoute } from "@angular/router";

import { UserDetailViewModel } from "../../../viewmodels/user/user-detail.viewmodel";
import config from "../../../../../utils/config";
import { AlertService } from "../../../../../utils/alert/alert.service";

export interface User {
  name: string;
}

@Component({
  selector: "app-user-edit-detail-view",
  templateUrl: "./user-edit-detail-view.component.html",
  styleUrls: ["./user-edit-detail-view.component.scss"],
  providers: [UserDetailViewModel, AlertService]
})
export class UserEditDetailViewComponent implements OnInit {
  constructor(
    public userViewModel: UserDetailViewModel,
    private router: ActivatedRoute,
    private alertService: AlertService,
  ) { }
  selectedProviderName = 'None';
  public ngOnInit() {
    this.router.params
      .subscribe(params => {
        this.initViewModels(params.userId, params);
      });
  }

  public displayFn(user?: User): string | undefined {
    return user ? user.name : undefined;
  }

  public initViewModels(userId: string, params: any) {
    const userDetailParams = {
      id: userId,
      modalType: (params.userId === "new-user") ? "Create" : "Edit",
    };
    this.userViewModel.init(userDetailParams);
  }
}
