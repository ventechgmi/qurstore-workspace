import { Routes, RouterModule } from "@angular/router";
import { UserListViewComponent } from "./views/user/user-list-view/user-list-view.component";
import { UserEditDetailViewComponent } from "./views/user/user-edit-detail-view/user-edit-detail-view.component";
import { RegisterComponent } from "./views/user/register/register.component";

import { NgModule } from "@angular/core";

export const routes: Routes = [
  {
    path: "user-list",
    component: UserListViewComponent
  }
  , {
    path: "user-detail/:userId",
    component: UserEditDetailViewComponent
  }, {
    path: "user-detail/new-role",
    component: UserEditDetailViewComponent
  }, {
    path: "register",
    component: RegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserManagementRoutingModule { }
