
import { map, debounceTime } from 'rxjs/operators';
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { EntityService, EntityQuery, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
import { of as observableOf } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class ExternalAuthService extends EntityService {
    appConfig: AppConfig;

    constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
        super(http, "authentication-provider", appConfig);
        this.appConfig = appConfig;
    }

    private getAPIURL() {
        return this.getApi();
    }


    private generateGetAPI(name, req): Observable<any> {
        return this.http.get(this.getAPIURL() + name + req).pipe(
            map(res => {
                return res;
            }));
    }

    private generatePostAPI(name, data): Observable<any> {
        return this.http.post(this.getAPIURL() + name, data).pipe(
            map((res: Response) => res));
    }

    private generateDeleteAPI(name, req): Observable<any> {
        return this.http.delete(this.getAPIURL() + name + req).pipe(
            map(res => {
                return res;
            }));
    }

    public getProviderList(): Observable<any> {
        return this.generateGetAPI("/list", "");
    }

    public GetProviderListByOrganization(data): Observable<any> {
        return this.generateGetAPI("/list-by-Organization/", data);
    }

    public GetADUsersAndGroups(authProviderId: string): Observable<any> {
        return this.generateGetAPI("/get-ad-groups-users/", authProviderId);
    }

    public GetUserNameFromProvider(req): Observable<any> {
        return this.generatePostAPI("/user-by-name", req);
    }

}
