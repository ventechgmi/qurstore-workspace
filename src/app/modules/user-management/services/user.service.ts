
import { map, debounceTime } from 'rxjs/operators';
import { Injectable, Inject, EventEmitter } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { EntityService, EntityQuery, APP_CONFIG, AppConfig } from "@fastgen/ui-framework";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
import { of as observableOf } from "rxjs";

const appConfigLocal = require("../../../app-config.json");
@Injectable({
    providedIn: "root"
})
export class UserService extends EntityService {
    appConfig: AppConfig;
    onUpdateListEvent = new EventEmitter();
    public userIdSubject = new ReplaySubject<any>();
    public showSuccessMessage = new Subject<any>();
    public deleteSuccessMessage = new Subject<any>();
    public userRolesCountSubject = new Subject<any>();
    public isEditModeSubject = new Subject<any>();
    public alertInfo = { message: "", type: "" };
    public isUserEditFromList = false;

    // this subscription will help to show error or success message in another screen(details screen delete success message showing in list screen)
    private afterInitSubject = new BehaviorSubject(this.alertInfo);

    constructor(http: HttpClient, @Inject(APP_CONFIG) appConfig: AppConfig) {
        super(http, "user", appConfig);
        this.appConfig = appConfig;
    }
    private getAPIURL() {
        return this.getApi();
        let getAPIURL = "";
        const serverDeployment = appConfigLocal.serverDeployment;
        if (serverDeployment === "0") {
            getAPIURL = "https://localhost:6001";
        } else {
            getAPIURL = "https://api.qurstore-qa-userregistartion.vsolgmi.com";
        }
        return getAPIURL;
    }


    public showAfterIntMessage(messageInfo) {
        this.afterInitSubject.next(messageInfo);
    }

    public getAfterIntObs(): Observable<any> {
        return this.afterInitSubject.asObservable();
    }

    public getUserIdSubject() {
        return this.userIdSubject.asObservable();
    }

    public getShowSuccessMessage() {
        return this.showSuccessMessage.asObservable();
    }

    public getUpdateUserListEvent() {
        return this.onUpdateListEvent;
    }

    public getUserRolesCountObs() {
        return this.userRolesCountSubject;
    }

    private generateGetAPI(name, req): Observable<any> {
        return this.http.get(this.getAPIURL() + name + req).pipe(
            map(res => {
                return res;
            }));
    }

    private generatePostAPI(name, data): Observable<any> {
        return this.http.post(this.getAPIURL() + name, data).pipe(
            map((res: Response) => res));
    }

    private generateDeleteAPI(name, req): Observable<any> {
        return this.http.delete(this.getAPIURL() + name + req).pipe(
            map(res => {
                return res;
            }));
    }

    public getAllUsers(): Observable<any> {
        return this.generateGetAPI("/list", "");
    }
    public getOrganizationList(): Observable<any> {
        return this.generateGetAPI("/getOrganizationList", "");
    }

    public saveUser(user): Observable<any> {
        return this.generatePostAPI("/save", user);
    }

    public refreshAllADUserDetails(): Observable<any> {
        return this.generatePostAPI("/refreshAllADUsers", '');
    }

    public registerUser(user): Observable<any> {
        return this.generatePostAPI("/register", user);
    }

    public deleteUser(userId): Observable<any> {
        return this.generateDeleteAPI("/delete/", userId);
    }

    public getUserById(req): Observable<any> {
        return this.generateGetAPI("/", req);
    }

}
