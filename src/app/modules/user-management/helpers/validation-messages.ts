export class ValidationMessages {
    public static readonly RequiredMessage = " is required.";
    public static readonly MinMaxLengthMessage = " must be between 3 and 20 characters..";
    public static readonly InvalidMessage = " is invalid.";
    public static readonly EmailMatchMessage = "Email and ConfirmEmail must be matched.";

    public static readonly emailalreadyExists = "Email address is already in use. Please try with another one.";
    public static readonly saveSuccessMessage = "User saved successfully.";
    public static readonly createSuccessMessage = "User created successfully.";
    public static readonly updateSuccessMessage = "User updated successfully.";
    public static readonly deleteSuccessMessage = "User deleted successfully.";
    public static readonly atLeastOneRoleRequiredMsg = "Please select at least one role for this user";
    public static readonly somethingWentWrong = "Something went wrong. Please try again.";
    public static readonly enterUsernameError = "Please enter username";

    // regex patter
    public static readonly EmailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    public static readonly PhoneNumberPattern = "^(\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$";
    public static readonly zipCodePattern = "^[0-9]{5}(?:-[0-9]{4})?$";

    public static readonly userNotADUserError = `User with email '{emailAddress}' is not a Active Directory user`;
    public static readonly userNotActiveUserError = `User with email '{emailAddress}' is a Inactive User`;
    public static readonly userDetailRefreshInfo = "Refreshing the User details. Please wait...";
    public static readonly userDetailRefreshedSuccess = "User details refreshed successfully";
    public static readonly NoUserFoundMessage= "No user detail found";
    public static readonly userDetailRemoved = "User has been disabled/deleted from active directory";
}
