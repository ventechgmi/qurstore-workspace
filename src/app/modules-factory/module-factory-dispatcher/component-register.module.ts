import { NgModule } from "@angular/core";
import { DynamicComponentManifest } from "../dynamic-component-manifest";
import { ModulesFactoryRegister } from "../modules-factory.module";
import { manifests } from "./component-manifest";
import { FactoryComponentDispatcher } from "./factory-component-dispatcher";



@NgModule({
    imports: [ModulesFactoryRegister.FOR_ROOT(manifests)],
    providers: [FactoryComponentDispatcher]

})
export class ComponentRegisterModule { }