import { Injectable } from "@angular/core";
import { Subject } from "rxjs";


@Injectable()
export class GlobalDataShareService {

    //subject for data sharing across modules component 
    public dataDispatcher = new Subject<any>();

    public dataResponseDispatcher = new Subject<any>();

}