import { Observable } from "rxjs";
import { ComponentFactory } from "@angular/core";

export interface FactoryComponent {

    /** Unique identifier, used in the application to retrieve a ComponentFactory. */
    getComponentInstance<T>(componentId: string, params?: any): Observable<ComponentFactory<T>>;

}
 