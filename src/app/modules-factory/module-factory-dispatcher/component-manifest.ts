import { DynamicComponentManifest } from "../dynamic-component-manifest";

const manifests: DynamicComponentManifest[] = [
    {
        componentId: "common-grid-component",
        path: "common-grid-component", // some globally-unique identifier, used internally by the router
        loadChildren: "../../modules/core-ui-components/data-grid/data-grid.module#DataGridModule"
    }, 
    {
        componentId: "common-extended-grid-component",
        path: "common-extended-grid-component", // some globally-unique identifier, used internally by the router
        loadChildren: "../../modules/core-ui-components/data-grid/data-grid-extended.module#DataGridExtendedModule"
    }
];

export { manifests };
