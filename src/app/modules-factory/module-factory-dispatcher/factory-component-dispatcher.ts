import { Injectable, ComponentFactory } from "@angular/core";
import { FactoryComponent } from "./factory-component.interface";
import { ModulesFactory } from "../modules-factory";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class FactoryComponentDispatcher implements FactoryComponent {

    constructor(private modulesFactory: ModulesFactory) {

    }
    getComponentInstance<T>(componentId: string, params?: any): Observable<ComponentFactory<T>> {
        return this.modulesFactory.getComponentFactory<T>(componentId);
    }



}