import {
  ANALYZE_FOR_ENTRY_COMPONENTS,
  ModuleWithProviders,
  NgModule,
  NgModuleFactoryLoader,
  SystemJsNgModuleLoader,
  Type
} from "@angular/core";
import { ROUTES } from "@angular/router";

import { ModulesFactory } from "./modules-factory";
import {
  DYNAMIC_COMPONENT,
  DYNAMIC_COMPONENT_MANIFESTS,
  DYNAMIC_MODULE,
  DynamicComponentManifest
} from "./dynamic-component-manifest";

@NgModule()
export class ModulesFactoryRegister {
  static FOR_ROOT(manifests: DynamicComponentManifest[]): ModuleWithProviders {
    return {
      ngModule: ModulesFactoryRegister,
      providers: [
        ModulesFactory,
        { provide: NgModuleFactoryLoader, useClass: SystemJsNgModuleLoader },
        // provider for Angular CLI to analyze
        { provide: ROUTES, useValue: manifests, multi: true },
        // provider for ModulesFactory to analyze
        { provide: DYNAMIC_COMPONENT_MANIFESTS, useValue: manifests }
      ]
    };
  }
  static FOR_MODULE(manifest: DynamicComponentManifest): ModuleWithProviders {
    return {
      ngModule: ModulesFactoryRegister,
      providers: [
        { provide: ANALYZE_FOR_ENTRY_COMPONENTS, useValue: manifest, multi: true },
        // provider for @angular/router to parse
        { provide: ROUTES, useValue: manifest, multi: true },
        // provider for ModulesFactory to analyze
        { provide: DYNAMIC_MODULE, useValue: manifest }]
    };
  }
  static FOR_CHILD(component: Type<any>): ModuleWithProviders {
    return {
      ngModule: ModulesFactoryRegister,
      providers: [
        { provide: ANALYZE_FOR_ENTRY_COMPONENTS, useValue: component, multi: true },
        // provider for @angular/router to parse
        { provide: ROUTES, useValue: [], multi: true },
        // provider for ModulesFactory to analyze
        { provide: DYNAMIC_COMPONENT, useValue: component }
      ]
    };
  }
}

export { DynamicComponentManifest } from "./dynamic-component-manifest";
