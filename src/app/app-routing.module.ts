import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule,
  PreloadAllModules
} from '@angular/router';

// import { AdminLayoutComponent } from "./modules/main/views/layouts/admin/admin-layout.component";
// import { DashboardLayoutComponent } from "./modules/main/views/layouts/dashboard/dashboard-layout.component";
import {
  ROUTES_CONFIG_MAP,
  MODULE_BASE_URL_MODULE_ID_MAP,
  MODULE_NAME_MAP
} from './utils/config';
// import { LoginComponent } from './modules/authentication/views/authentication/login/login.component';
// import { AppComponent } from './app.component';
// import { AuthGuardService } from "./utils/auth-guard.service";
const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: '',
    // component: AppComponent,
    // component: AdminLayoutComponent,
    //  component: DashboardLayoutComponent,
    children: [
      {
        // canActivate: [AuthGuardService],
        path: ROUTES_CONFIG_MAP.ROLE_MANAGEMENT.BASE_URL,
        loadChildren:
          './modules/role-management/role-management.module#RoleManagementModule',
        data: {
          title: 'Role Management',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[
            ROUTES_CONFIG_MAP.ROLE_MANAGEMENT.BASE_URL
            ]
        }
      },
      {
        path: ROUTES_CONFIG_MAP.SUPPLIER_LANDING.BASE_URL,
        loadChildren:
          './modules/supplier-landing/supplier-landing.module#SupplierLandingModule',
        data: {
          title: 'Supplier Landing',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[
            ROUTES_CONFIG_MAP.SUPPLIER_LANDING.BASE_URL
            ]
        }
      },
      {
        path: ROUTES_CONFIG_MAP.SUPPLIER_LANDING.BASE_URL,
        loadChildren:
          './modules/employee-dashboard/employee-dashboard.module#EmployeeDashboardModule',
        data: {
          title: 'Employee Dashboard',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[
            ROUTES_CONFIG_MAP.EMPLOYEE_DASHBOARD.BASE_URL
            ]
        }
      },
      {
       // canActivate: [AuthGuardService],
      path: ROUTES_CONFIG_MAP.TARGET_DEFINITION.BASE_URL,
      loadChildren: () => import('./modules/target-definition/target-definition.module').then(m => m.TargetDefinitionModule),
      data: {
        title: 'Target Definition',
        moduleId:
          MODULE_BASE_URL_MODULE_ID_MAP[
          ROUTES_CONFIG_MAP.TARGET_DEFINITION.BASE_URL
          ]
      }
     },
      {
        path: ROUTES_CONFIG_MAP.WORKSPACE_MANAGEMENT.BASE_URL,
        loadChildren:
          './modules/workspace/workspace.module#WorkspaceModule',
        data: {
          title: 'Workspace Management',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[
            ROUTES_CONFIG_MAP.WORKSPACE_MANAGEMENT.BASE_URL
            ]
        }
      },
      {
        path: ROUTES_CONFIG_MAP.DATASET_TYPE_MANAGEMENT.BASE_URL,
        loadChildren: () => import('./modules/dataset-type-management/dataset-type-management.module').then(m => m.DatasetTypeManagementModule),
        data: {
          title: 'Dataset Type Management',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[
            ROUTES_CONFIG_MAP.DATASET_TYPE_MANAGEMENT.BASE_URL
            ]
        }
      },
      {
        path: ROUTES_CONFIG_MAP.REPORTS.BASE_URL,
        loadChildren:
          './modules/reports/reports.module#ReportsModule',
        data: {
          title: 'Reports',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[
            ROUTES_CONFIG_MAP.REPORTS.BASE_URL
            ]
        }
      }
      , {
        path: ROUTES_CONFIG_MAP.USER_MANAGEMENT.BASE_URL,
        loadChildren: () => import('./modules/user-management/user-management.module').then(m => m.UserManagementModule),
        data: {
          title: 'User Management',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[
            ROUTES_CONFIG_MAP.USER_MANAGEMENT.BASE_URL
            ]
        }
      },
      {
        path: ROUTES_CONFIG_MAP.SOURCE_TARGET_MAPPING.BASE_URL,
        loadChildren:
          './modules/source-target-mapping/source-target-mapping.modules#SourceTargetMappingModule',
        data: {
          title: 'Mapping Definition',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[
            ROUTES_CONFIG_MAP.SOURCE_TARGET_MAPPING.BASE_URL
            ]
        }
      },
      {
        path: ROUTES_CONFIG_MAP.TAG_TYPE_MANAGEMENT.BASE_URL,
        loadChildren:
          './modules/tag-type-management/tag-type-management.module#TagtypeManagementModule',
        data: {
          title: 'Tag Type Management',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[
            ROUTES_CONFIG_MAP.TAG_TYPE_MANAGEMENT.BASE_URL
            ]
        }
      },
      {
        path: ROUTES_CONFIG_MAP.COST_MANAGEMENT.BASE_URL,
        loadChildren:
          './modules/cost-management/cost-management.module#CostManagementModule',
        data: {
          title: 'Cost Reports',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[
            ROUTES_CONFIG_MAP.COST_MANAGEMENT.BASE_URL
            ]
        }
      },
      {
        path: ROUTES_CONFIG_MAP.UTILITY_MANAGEMENT.BASE_URL,
        loadChildren:
          './modules/utility-management/utility-management.module#UtilitiesManagementModule',
        data: {
          title: 'Utility Management',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[
            ROUTES_CONFIG_MAP.UTILITY_MANAGEMENT.BASE_URL
            ]
        }
      }, {
        path: ROUTES_CONFIG_MAP.ENVIRONMENT.BASE_URL,
        loadChildren:
          './modules/environment/environment.module#EnvironmentModule',
        data: {
          title: 'Environment',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[
            ROUTES_CONFIG_MAP.ENVIRONMENT.BASE_URL
            ]
        }
      },
      {
        path: ROUTES_CONFIG_MAP.DATA_UPLOAD.BASE_URL,
        loadChildren:
          './modules/data-upload-management/data-upload-management.module#DataFileUploadManagementModule',
        data: {
          title: 'Data Upload',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[ROUTES_CONFIG_MAP.DATA_UPLOAD.BASE_URL]
        }
      },
      {
        path: ROUTES_CONFIG_MAP.DATASET_EXTRACTION.BASE_URL,
        loadChildren:
          './modules/dataset-extraction/dataset-extraction.module#DatasetExtractionModule',
        data: {
          title: 'Dataset Extraction',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[ROUTES_CONFIG_MAP.DATASET_EXTRACTION.BASE_URL]
        }
      },
      {
        path: ROUTES_CONFIG_MAP.UTILITY_MANAGEMENT.BASE_URL,
        loadChildren:
          './modules/utility-management/utility-management.module#UtilitiesManagementModule',
        data: {
          title: 'Utility Management',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[
            ROUTES_CONFIG_MAP.UTILITY_MANAGEMENT.BASE_URL
            ]
        }
      }
      ,
      {
        path: ROUTES_CONFIG_MAP.NOTIFICATION_MANAGEMENT.BASE_URL,
        loadChildren:
          './modules/notification-management/notification-management.module#NotificationManagementModule',
        data: {
          title: 'Data Upload',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[ROUTES_CONFIG_MAP.NOTIFICATION_MANAGEMENT.BASE_URL]
        }
      },
      {
        path: ROUTES_CONFIG_MAP.DASHBOARD.BASE_URL,
        loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule),
        data: {
          title: 'Dashboard',
          moduleId:
            MODULE_BASE_URL_MODULE_ID_MAP[
            ROUTES_CONFIG_MAP.EMPLOYEE_DASHBOARD.BASE_URL
            ]
        }
      }
    ]
  },
  {
    path: ROUTES_CONFIG_MAP.AUTHENTICATION.BASE_URL,
    loadChildren:
      './modules/authentication/authentication.module#AuthenticationModule',
    data: {
      title: 'Login Page'
    }
  }
  // ,
  // {
  //   path: 'dashboard',
  //  // canActivate: [AuthGuardService],
  //   data: { title: 'Home Page' },
  //   loadChildren:
  //   "./modules/main/views/dashboard/dashboard.module#DashboardModule"
  // }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor() { }
}
