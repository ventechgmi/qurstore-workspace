import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { LicenseManager } from "ag-grid-enterprise";
  import "ag-grid-enterprise";


// if (environment.production) {
//   enableProdMode();
// }
if (environment) {
  LicenseManager.setLicenseKey("Ventech_Solutions,_Inc._MultiApp_2Devs_1Deployment_18_September_2020__MTYwMDM4MzYwMDAwMA==ae0fc7b597e3097c2bb0caf0b20683b6");
  enableProdMode();
} else {
  LicenseManager.setLicenseKey("Evaluation_License-_Not_For_Production_Valid_Until_25_May_2019__MTU1ODczODgwMDAwMA==156057ec2a5212d3fc17b2c425718067");
}
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
